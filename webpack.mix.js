let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.babel([
    'public/js/lib/jquery2.1.1.js',
    'public/js/lib/framework.min.js',
    'public/js/generals.js',
    'public/js/axios.min.js',
    'public/js/jquery.magnific-popup.js',
    'public/js/combodate.js',
    'public/js/moment.js',
    'public/js/sweetalert2.all.min.js',
    'public/js/jquery.maskMoney.min.js',
    'public/js/sweetalert2.all.min.js',
    'public/js/es.js'
], 'public/js/librerias.js').options({
     uglify: {
         uglifyOptions: {
           mangle: false
         }
     },
     compact:false
});
mix.babel([
    'public/js/registro.js',
    'public/js/validacionesFront.js',
    'public/js/cuestionario_dinamico.js',
    'public/js/status_oferta.js',
    'public/js/login.js'
], 'public/js/all.js').options({
     uglify: {
         uglifyOptions: {
           mangle: false
         }
     },
     compact:false
});
mix.babel([
    'public/js/webapp/jquery-3.2.1.slim.min.js',
    'public/js/webapp/popper.min.js',
    'public/js/webapp/bootstrap4.0.min.js',
    'public/js/webapp/axios.min.js',
    'public/js/jquery.magnific-popup.js',
    'public/js/jquery.magnific-popup.js',
    'public/js/sweetalert2.all.min.js',
    'public/js/proceso_webapp.js'
], 'public/js/all_facematch.js').options({
     uglify: {
         uglifyOptions: {
           mangle: false
         }
     },
     compact:false
});
mix.js('resources/assets/js/app_backoffice.js', 'public/js').options({
     uglify: {
         uglifyOptions: {
           mangle: true
         }
     },
     compact:true
});
mix.js('resources/assets/js/webapp.js', 'public/js');
mix.js('resources/assets/js/app.js', 'public/js');
mix.styles([
    'public/css/magnific-popup.css',
    'public/css/sweetalert2.min.css',
    'public/css/modals.css',
    'public/css/general.css',
], 'public/css/style.css');
