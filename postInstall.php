<?php

exec('php artisan storage:link');

if (!file_exists(__DIR__.'/scripts/bc_score')) {
    mkdir(__DIR__.'/scripts/bc_score', 0755, true);
}

if (!file_exists(__DIR__.'/scripts/full_report')) {
    mkdir(__DIR__.'/scripts/full_report', 0755, true);
}

if (!file_exists(__DIR__.'/storage/app/public/exports')) {
    mkdir(__DIR__.'/storage/app/public/exports', 0775, true);
}

if (!file_exists(__DIR__.'/storage/app/public/proceso_simplificado')) {
    mkdir(__DIR__.'/storage/app/public/proceso_simplificado', 0775, true);
    chown(__DIR__.'/storage/app/public/proceso_simplificado', get_current_user());
    chgrp(__DIR__.'/storage/app/public/proceso_simplificado', 'www-data');
}
