<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBcDatosPersonalesTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bc_datos_personales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->string('apellido_p', 50)->nullable();
            $table->string('apellido_m', 50)->nullable();
            $table->string('apellido_adl', 50)->nullable();
            $table->string('nombre', 50)->nullable();
            $table->string('segundo_nombre', 50)->nullable();
            $table->string('fecha_nacimiento', 10)->nullable();
            $table->string('rfc', 13)->nullable();
            $table->string('prefijo_personal', 4)->nullable();
            $table->string('sufijo_personal', 4)->nullable();
            $table->string('nacionalidad', 2)->nullable();
            $table->string('nacionalidad_curp', 2)->nullable();
            $table->string('pais_nacimiento_curp', 2)->nullable();
            $table->string('tipo_residencia', 1)->nullable();
            $table->string('lic_conducir', 20)->nullable();
            $table->string('estado_civil', 1)->nullable();
            $table->string('sexo', 1)->nullable();
            $table->string('cedula_profesional', 20)->nullable();
            $table->string('ife', 20)->nullable();
            $table->string('curp', 20)->nullable();
            $table->string('clave_pais', 2)->nullable();
            $table->string('num_dependientes', 2)->nullable();
            $table->string('edades_dependientes', 30)->nullable();
            $table->string('fecha_recep_dependientes', 10)->nullable();
            $table->string('fecha_defuncion', 8)->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_datos_personales');
    }
}
