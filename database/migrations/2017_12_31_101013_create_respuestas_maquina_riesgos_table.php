<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasMaquinaRiesgosTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->boolean('ejecucion_sp')->nullable();
            $table->mediumText('status_ejecucion_sp')->nullable();
            $table->string('decision', 255)->nullable();
            $table->integer('plantilla_comunicacion')->unsigned()->nullable();
            $table->boolean('pantallas_extra')->nullable();
            $table->string('situaciones',255)->nullable();
            $table->boolean('cuestionario_dinamico_guardado')->nullable();
            $table->mediumText('status_guardado')->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });

    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repuestas_maquina_riesgos');
    }
}
