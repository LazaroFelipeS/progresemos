<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSimplificadoToClientesAltaTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->boolean('simplificado')->after('fecha_alta')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->dropColumn([
                'simplificado'
            ]);
        });
    }
}
