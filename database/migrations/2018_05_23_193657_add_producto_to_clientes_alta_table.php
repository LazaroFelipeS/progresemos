<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductoToClientesAltaTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->string('IDTYPE', 50)->nullable()->after('INCSNDSOURCE');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->dropColumn([
                'IDTYPE'
            ]);
        });
    }
}
