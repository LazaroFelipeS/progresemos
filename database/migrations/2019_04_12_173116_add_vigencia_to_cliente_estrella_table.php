<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVigenciaToClienteEstrellaTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cliente_estrella', function (Blueprint $table) {
            $table->date('vigencia')->after('fecha_campana')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cliente_estrella', function (Blueprint $table) {
            $table->dropColumn([
                'vigencia'
            ]);
        });
    }
}
