<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoConsultaToConsultasBuro extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultas_buro', function (Blueprint $table) {
            $table->integer('no_consulta')->default(0)->after('fecha_consulta');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultas_buro', function (Blueprint $table) {
            $table->dropColumn([
                'no_consulta'
            ]);
        });
    }
}
