<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospectos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('nombres');
            $table->longText('apellido_paterno');
            $table->longText('apellido_materno');
            $table->string('email', 100)->unique();
            $table->string('celular', 15);
            $table->string('password', 255);
            $table->boolean('usuario_ldap');
            $table->string('sms_verificacion', 10);
            $table->boolean('usuario_confirmado');
            $table->string('referencia', 100);
            $table->longText('encryptd');
            $table->timestamps();

            $table->index(['email'], 'email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prospectos');
    }
}
