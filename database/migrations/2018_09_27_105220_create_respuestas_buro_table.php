<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasBuroTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultas_buro', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->enum('orden', ['Primera', 'Segunda']);
            $table->enum('tipo', ['Llamada', 'Respuesta']);
            $table->longText('cadena_original')->nullable();
            $table->longText('folio_consulta')->nullable();
            $table->longText('INTL')->nullable();
            $table->longText('PN')->nullable();
            $table->longText('PA')->nullable();
            $table->longText('PE')->nullable();
            $table->longText('TL')->nullable();
            $table->longText('IQ')->nullable();
            $table->longText('RS')->nullable();
            $table->longText('HI')->nullable();
            $table->longText('HR')->nullable();
            $table->longText('CR')->nullable();
            $table->longText('SC')->nullable();
            $table->longText('ERRR')->nullable();
            $table->longText('ES')->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Reveierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultas_buro');
    }
}
