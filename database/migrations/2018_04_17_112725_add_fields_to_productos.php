<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToProductos extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->boolean('consulta_alp')->nullable()->default(1)->after('campo_cobertura');
            $table->boolean('consulta_buro')->nullable()->default(1)->after('consulta_alp');
            $table->boolean('garantia')->nullable()->default(1)->after('consulta_buro');
            $table->boolean('seguro')->nullable()->default(1)->after('garantia');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->dropColumn([
                'consulta_alp',
                'consulta_buro',
                'garantia',
                'seguro'
            ]);
        });
    }
}
