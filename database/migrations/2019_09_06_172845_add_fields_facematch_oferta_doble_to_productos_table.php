<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsFacematchOfertaDobleToProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->boolean('doble_oferta')->after('stored_procedure')->nullable();
            $table->string('tipo_monto_do', 20)->after('doble_oferta')->nullable();
            $table->double('do_monto_maximo')->after('tipo_monto_do')->nullable();

            $table->boolean('proceso_simplificado')->after('do_monto_maximo')->nullable();
            $table->string('tipo_monto_simplificado', 20)->after('proceso_simplificado')->nullable();
            $table->double('simplificado_monto_minimo')->after('tipo_monto_simplificado')->nullable();
            $table->double('simplificado_monto_maximo')->after('simplificado_monto_minimo')->nullable();

            $table->boolean('facematch')->after('simplificado_monto_maximo')->nullable();
            $table->boolean('carga_identificacion_selfie')->after('facematch')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->dropColumn([
                'doble_oferta',
                'tipo_monto_do',
                'do_monto_maximo',
                'proceso_simplificado',
                'tipo_monto_simplificado',
                'simplificado_monto_maximo',
                'simplificado_monto_minimo',
                'facematch',
            ]);
        });
    }
}
