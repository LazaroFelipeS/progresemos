<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleosTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleo_solicitud', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->unsignedBigInteger('prospecto_id');
           $table->unsignedBigInteger('solicitud_id');
           $table->string('nombre_empresa', 50);
           $table->string('antiguedad_empleo', 5);
           $table->date('fecha_ingreso')->nullable();
           $table->string('telefono_empleo', 10);
           $table->string('calle', 50);
           $table->string('num_exterior', 5);
           $table->string('num_interior', 5)->nullable();
           $table->string('codigo_postal', 5)->nullable();
           $table->string('colonia', 150);
           $table->string('id_colonia', 20)->nullable();
           $table->string('delegacion_municipio', 150);
           $table->string('id_delegacion', 10)->nullable();
           $table->string('ciudad', 150)->nullable();
           $table->string('id_ciudad', 10)->nullable();
           $table->string('estado', 150);
           $table->string('id_estado', 10)->nullable();
           $table->timestamps();

           $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
           $table->index(['solicitud_id'], 'solicitud');
       });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('domicilio_solicitud');
    }
}
