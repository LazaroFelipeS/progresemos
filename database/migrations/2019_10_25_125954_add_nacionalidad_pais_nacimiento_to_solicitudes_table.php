<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNacionalidadPaisNacimientoToSolicitudesTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->string('nacionalidad', 2)->after('curp')->nullable();
            $table->string('pais_nacimiento', 2)->after('nacionalidad')->nullable();
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->dropColumn([
                'nacionalidad',
                'pais_nacimiento'
            ]);
        });
    }
}
