<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlazoProductoTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plazo_producto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('plazo_id')->unsigned();
            $table->integer('producto_id')->unsigned();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('plazo_producto', function (Blueprint $table) {
            Schema::dropIfExists('plazo_producto');
        });
    }
}
