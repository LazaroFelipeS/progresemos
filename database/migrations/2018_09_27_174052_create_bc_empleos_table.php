<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBcEmpleosTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bc_empleos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->string('emp_razon_social', 40)->nullable();
            $table->string('emp_dir1', 40)->nullable();
            $table->string('emp_dir2', 40)->nullable();
            $table->string('emp_colonia', 40)->nullable();
            $table->string('emp_deleg', 40)->nullable();
            $table->string('emp_ciudad', 40)->nullable();
            $table->string('emp_estado', 4)->nullable();
            $table->string('emp_cp', 5)->nullable();
            $table->string('emp_num_tel', 11)->nullable();
            $table->string('emp_tel_ext', 8)->nullable();
            $table->string('emp_fax', 11)->nullable();
            $table->string('emp_cargo', 30)->nullable();
            $table->string('emp_fecha_contrat', 8)->nullable();
            $table->string('emp_clave_moneda', 2)->nullable();
            $table->string('emp_monto_sueldo', 9)->nullable();
            $table->string('emp_periodo_pago', 1)->nullable();
            $table->string('emp_num_empleado', 15)->nullable();
            $table->string('emp_fecha_ult_dia', 8)->nullable();
            $table->string('emp_fecha_reporte', 8)->nullable();
            $table->string('emp_fecha_verificacion', 8)->nullable();
            $table->string('emp_modo_verificacion', 1)->nullable();
            $table->string('emp_origen_razon_social', 2)->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Reveierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_empleos');
    }
}
