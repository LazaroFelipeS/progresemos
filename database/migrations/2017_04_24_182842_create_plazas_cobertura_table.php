<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlazasCoberturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plazas_cobertura', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ciudad',145);
            $table->string('estado',145);
            $table->string('estado_abrev',45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plazas_cobertura');
    }
}
