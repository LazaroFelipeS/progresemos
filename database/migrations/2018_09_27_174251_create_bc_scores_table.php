<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBcScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bc_scores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->string('bc_score', 5)->nullable();
            $table->string('exclusion', 255)->nullable();
            $table->string('bc_razon1', 255)->nullable();
            $table->string('bc_razon2', 255)->nullable();
            $table->string('bc_razon3', 255)->nullable();
            $table->string('bc_error', 255)->nullable();
            $table->string('icc_score', 5)->nullable();
            $table->string('icc_exclusion', 255)->nullable();
            $table->string('icc_razon1', 255)->nullable();
            $table->string('icc_razon2', 255)->nullable();
            $table->string('icc_razon3', 255)->nullable();
            $table->string('icc_error', 255)->nullable();
            $table->string('micro_valor', 5)->nullable();
            $table->string('micro_razon1', 255)->nullable();
            $table->string('micro_razon2', 255)->nullable();
            $table->string('micro_razon3', 255)->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_scores');
    }
}
