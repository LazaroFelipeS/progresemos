<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoOfertaPoblacionToRespuestasMaquinaRiesgosTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::table('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->string('tipo_poblacion', 50)->after('situaciones')->nullable();
            $table->string('tipo_oferta', 50)->after('tipo_poblacion')->nullable();
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->dropColumn([
                'tipo_poblacion',
                'tipo_oferta'
            ]);
        });
    }
}
