<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosBuroTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_buro', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->longText('str_request')->nullable();
            $table->longText('str_response')->nullable();
            $table->longText('bc_score')->nullable();
            $table->longText('exclusion')->nullable();
            $table->longText('bc_razon1')->nullable();
            $table->longText('bc_razon2')->nullable();
            $table->longText('bc_razon3')->nullable();
            $table->longText('bc_error')->nullable();
            $table->longText('report_request')->nullable();
            $table->longText('icc_score')->nullable();
            $table->longText('icc_exclusion')->nullable();
            $table->longText('icc_razon1')->nullable();
            $table->longText('icc_razon2')->nullable();
            $table->longText('icc_razon3')->nullable();
            $table->longText('icc_error')->nullable();
            $table->longText('report_response')->nullable();
            $table->longText('micro_valor')->nullable();
            $table->longText('micro_razon1')->nullable();
            $table->longText('micro_razon2')->nullable();
            $table->longText('micro_razon3')->nullable();
            $table->boolean('actualizado')->nullable();
            $table->boolean('rechazado')->default(0)->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Reveierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_buro');
    }
}
