<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CodigosEnviados extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codigos_enviados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id')->nullable();
            $table->string('nombre', 255)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('celular', 20)->nullable();
            $table->mediumInteger('codigo');
            $table->boolean('enviado')->nullable();
            $table->string('descripcion', 50)->nullable();
            $table->boolean('verificado');
            $table->timestamps();

            $table->index(['prospecto_id'], 'prospecto');
        });
    }

    /**
     * Reveierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codigos_enviados');
    }
}
