<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientesAltaTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes_alta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('SHORTNAME', 100)->nullable();
            $table->string('NAME1', 100)->nullable();
            $table->string('NAME2', 100)->nullable();
            $table->string('FECNACIMIENTO', 10)->nullable();
            $table->string('GENDERNC', 1)->nullable();
            $table->string('MARITALSTSNC', 2)->nullable();
            $table->string('STREET', 250)->nullable();
            $table->string('DIRNUMEXT', 5)->nullable();
            $table->string('DIRCDEDO', 20)->nullable();
            $table->string('DIRDELMUNI', 20)->nullable();
            $table->string('DIRCOLONIA', 20)->nullable();
            $table->string('DIRPAIS', 2)->nullable();
            $table->string('TELDOM', 15)->nullable();
            $table->string('TELOFI', 15)->nullable();
            $table->string('TELCEL', 15)->nullable();
            $table->string('EMAIL', 100)->nullable();
            $table->string('MNEMONIC', 15)->nullable();
            $table->string('RFCCTE', 15)->nullable();
            $table->string('FORMERNAME', 100)->nullable();
            $table->string('LUGNAC', 20)->nullable();
            $table->string('VALCURP', 25)->nullable();
            $table->string('DIRNUMINT', 5)->nullable();
            $table->string('DIRCIUDAD', 100)->nullable();
            $table->string('DIRCODPOS', 5)->nullable();
            $table->string('ESTUDIOS', 100)->nullable();
            $table->string('OCUPACION', 10)->nullable();
            $table->string('MAININCOME', 10)->nullable();
            $table->string('DOMANOS', 10)->nullable();
            $table->string('EGRORDMEN', 20)->nullable();
            $table->string('TIPODOM', 5)->nullable();
            $table->string('NOOFDEPEND', 5)->nullable();
            $table->boolean('alta')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes_alta');
    }
}
