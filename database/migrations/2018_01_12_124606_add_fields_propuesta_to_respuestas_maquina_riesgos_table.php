<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsPropuestaToRespuestasMaquinaRiesgosTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::table('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->string('monto', 100)->nullable()->after('situaciones');
            $table->string('plazo', 100)->nullable()->after('monto');
            $table->string('pago', 100)->nullable()->after('plazo');
            $table->string('tasa', 100)->nullable()->after('pago');
            $table->string('status_oferta', 25)->nullable()->after('decision');
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->dropColumn('monto');
            $table->dropColumn('plazo');
            $table->dropColumn('pago');
            $table->dropColumn('tasa');
            $table->dropColumn('status_oferta');
        });
    }
}
