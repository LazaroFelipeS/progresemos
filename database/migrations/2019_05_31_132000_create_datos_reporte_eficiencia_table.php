<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosReporteEficienciaTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_reporte_eficiencia', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->string('Origen')->nullable();
            $table->string('Anio')->nullable();
            $table->string('Mes')->nullable();
            $table->string('Dia')->nullable();
            $table->string('TipoSolicitud')->nullable();
            $table->string('RangoBCScore')->nullable();
            $table->string('RangoBCMicro')->nullable();
            $table->string('RangoBCMicro_V3')->nullable();
            $table->string('TipoCasosDetallado')->nullable();
            $table->string('TipoCasosRepEC')->nullable();
            $table->string('AntesRelease25May')->nullable();
            $table->string('MontoF2_Mod')->nullable();
            $table->string('Fec_Actualizacion')->nullable();
            $table->datetime('Creado')->nullable();
            $table->datetime('Actualizado')->nullable();
            $table->timestamps();
            
            $table->index(['solicitud_id', 'prospecto_id']);
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('datos_reporte_eficiencia');
    }
}
