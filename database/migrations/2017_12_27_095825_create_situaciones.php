<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSituaciones extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create('situaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('situacion', 2)->nullable();
            $table->longText('encabezado')->nullable();
            $table->longText('introduccion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cuestionarios_dinamicos');
    }
}
