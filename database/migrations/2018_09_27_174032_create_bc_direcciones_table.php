<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBcDireccionesTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bc_direcciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->string('dom_calle', 40)->nullable();
            $table->string('dom_calle_segunda_linea', 40)->nullable();
            $table->string('dom_colonia', 40)->nullable();
            $table->string('dom_deleg', 40)->nullable();
            $table->string('dom_ciudad', 40)->nullable();
            $table->string('dom_estado', 4)->nullable();
            $table->string('dom_cp', 5)->nullable();
            $table->string('dom_fecha', 8)->nullable();
            $table->string('dom_tel_casa', 11)->nullable();
            $table->string('dom_tel_casa_ext', 8)->nullable();
            $table->string('dom_fax', 11)->nullable();
            $table->string('dom_tipo', 1)->nullable();
            $table->string('dom_indicador_espec', 1)->nullable();
            $table->string('dom_fecha_reporte', 8)->nullable();
            $table->string('dom_origen', 2)->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Reveierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_direcciones');
    }
}
