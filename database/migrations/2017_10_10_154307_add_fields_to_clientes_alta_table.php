<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToClientesAltaTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->string('LOANAMOUNT', 10)->after('NOOFDEPEND')->nullable();
            $table->string('TERM', 20)->after('LOANAMOUNT')->nullable();
            $table->string('LOANPURPOSE', 255)->after('TERM')->nullable();
            $table->string('CUREMPMTEXPNCYR', 2)->after('LOANPURPOSE')->nullable();
            $table->string('HLDMORTGAGE', 2)->after('CUREMPMTEXPNCYR')->nullable();
            $table->string('HLDAUTOLOAN', 2)->after('HLDMORTGAGE')->nullable();
            $table->string('HLDTDC', 2)->after('HLDAUTOLOAN')->nullable();
            $table->string('TDCCODE', 5)->after('HLDTDC')->nullable();
            $table->string('INCSNDSOURCE', 50)->after('TDCCODE')->nullable();
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_alta', function (Blueprint $table) {
            $table->dropColumn([
                'LOANAMOUNT',
                'TERM',
                'LOANPURPOSE',
                'CUREMPMTEXPNCYR',
                'HLDMORTGAGE',
                'HLDAUTOLOAN',
                'HLDTDC',
                'TDCCODE',
                'INCSNDSOURCE'
            ]);
        });
    }
}
