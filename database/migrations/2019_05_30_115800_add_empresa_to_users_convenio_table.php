<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpresaToUsersConvenioTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_convenio', function (Blueprint $table) {
            $table->string('empresa', 255)->after('id')->nullable();
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_convenio', function (Blueprint $table) {
            $table->dropColumn([
                'empresa'
            ]);
        });
    }
}
