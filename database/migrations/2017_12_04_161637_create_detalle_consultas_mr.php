<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleConsultasMr extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_consultas_mr', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ID_PROSPECT');
            $table->unsignedBigInteger('ID_SOLIC');
            $table->integer('NO_SOLICITUD');
            $table->string('CONSULTA_FECHA', 8)->nullable();
            $table->string('CONSULTA_CLAVE_MEMBER_CODE', 10)->nullable();
            $table->string('CONSULTA_NOMBRE_USUARIO', 16)->nullable();
            $table->string('CONSULTA_NUM_TEL', 11)->nullable();
            $table->string('CONSULTA_CONTRATO_PRODUCTO', 2)->nullable();
            $table->string('CONSULTA_MONEDA', 2)->nullable();
            $table->integer('CONSULTA_IMPORTE')->nullable();
            $table->string('CONSULTA_REPONSABILIDAD', 1)->nullable();
            $table->string('CONSULTA_INDICADOR_CLIENTE', 1)->nullable();
            $table->integer('CONSULTA_RESERVADO2')->nullable();
            $table->timestamps();

            $table->index(['ID_PROSPECT', 'ID_SOLIC'], 'prospecto_solicitud');
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_consultas_mr');
    }
}
