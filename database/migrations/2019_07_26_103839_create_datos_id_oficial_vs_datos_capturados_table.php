<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosIdOficialVsDatosCapturadosTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_id_oficial_vs_datos_capturados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->string('tipo', 255);
            $table->mediumText('dato_capturado');
            $table->mediumText('dato_buro');
            $table->mediumText('dato_facematch');
            $table->double('facematch_vs_capturado')->nullable();
            $table->double('facematch_vs_buro')->nullable();
            $table->double('capturado_vs_buro')->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id', 'tipo'], 'prospecto_solicitud_tipo');
            $table->index(['prospecto_id'], 'prospecto');
            $table->index(['solicitud_id'], 'solicitud');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_id_oficial_vs_datos_capturados');
    }
}
