<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFechaConsultaToConsultasBuro extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultas_buro', function (Blueprint $table) {
            $table->dateTime('fecha_consulta')->nullable()->after('solicitud_id');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultas_buro', function (Blueprint $table) {
            $table->dropColumn([
                'fecha_consulta'
            ]);
        });
    }
}
