<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFacematchCargaineselfieSimplificadoToProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->boolean('facematch_simplificado')->after('simplificado_monto_maximo')->nullable();
            $table->boolean('carga_identificacion_selfie_simplificado')->after('facematch_simplificado')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->dropColumn([
                'carga_identificacion_selfie',
            ]);
        });
    }
}
