<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultaBcTest extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consulta_bc_test', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rfc', 20)->nullable();
            $table->string('status_sitio', 20)->nullable();
            $table->boolean('corte')->nullable();
            $table->longText('primera_llamada')->nullable();
            $table->longText('segunda_llamada')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('consulta_bc_test');
    }
}
