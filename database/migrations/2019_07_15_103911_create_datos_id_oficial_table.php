<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosIdOficialTable extends Migration
{
    /**
     * Ejecuta las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_id_oficial', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->mediumText('solicitudT24');
            $table->bigInteger('index');
            $table->string('type', 15);
            $table->string('code', 150);
            $table->string('value', 255);
            $table->string('description', 150);
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Revierte las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_id_oficial');
    }
}
