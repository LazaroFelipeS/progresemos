<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigoEstadoToDomicilioSolicitudTable extends Migration
{
    /**
     * Ejecuta las migraciones
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domicilio_solicitud', function (Blueprint $table) {
            $table->text('codigo_estado')->after('id_estado')->nullable();
        });
    }

    /**
     * Revierte las migraciones
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domicilio_solicitud', function (Blueprint $table) {
            $table->dropColumn('codigo_estado');
        });
    }
}
