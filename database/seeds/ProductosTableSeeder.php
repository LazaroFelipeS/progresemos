<?php

use Illuminate\Database\Seeder;
use App\Producto;
use Illuminate\Database\Query\Builder;

class ProductosTableSeeder extends Seeder
{
    /**
     * Ejecuta los seeds de la base de datos
     *
     * @return void
     */
    public function run()
    {
        $productos = [];

        $producto = Producto::create([
            'id'                                        => 1,
            'nombre_producto'                           => 'PRO-Mercado abierto',
            'alias'                                     => 'mercado-abierto',
            'tipo'                                      => 'Mercado abierto',
            'vista'                                     => null,
            'empresa'                                   => '',
            'monto_minimo'                              => 4000,
            'monto_maximo'                              => 50000,
            'bc_score'                                  => 560,
            'condicion'                                 => 'AND',
            'micro_score'                               => 600,
            'sin_historial'                             => 0,
            'sin_cuentas_recientes'                     => 0,
            'edad_minima'                               => 19,
            'edad_maxima'                               => 70,
            'cat'                                       => 72.13,
            'tasa_minima'                               => 30.00,
            'tasa_fija'                                 => 0,
            'tasa_maxima'                               => 70.00,
            'comision_apertura'                         => 0.00,
            'stored_procedure'                          => 'MaquinaRiesgos_MercadoAbierto',
            'doble_oferta'                              => 1,
            'tipo_monto_do'                             => 'definido',
            'do_monto_maximo'                           => 70000,
            'proceso_simplificado'                      => 0,
            'tipo_monto_simplificado'                   => null,
            'simplificado_monto_minimo'                 => null,
            'simplificado_monto_maximo'                 => null,
            'carga_identificacion_selfie_simplificado'  => 0,
            'facematch_simplificado'                    => 0,
            'carga_identificacion_selfie'               => 1,
            'facematch'                                 => 1,
            'logo'                                      => 'financiera_monte_de_piedad_logo.png',
            'campo_cobertura'                           => 'cobertura',
            'consulta_alp'                              => 0,
            'consulta_buro'                             => 1,
            'garantia'                                  => 0,
            'seguro'                                    => 0,
            'vigencia_de'                               => date('Y-m-d H:i:s'),
            'vigente'                                   => 1,
            'vigencia_hasta'                            => null,
            'asignar_nueva_solicitud'                   => 0,
            'default'                                   => 1,
            'created_at'                                => date('Y-m-d H:i:s'),
            'updated_at'                                => date('Y-m-d H:i:s'),
        ]);

        $productos[$producto->id] = $producto;

        $productos[1]->plazos()->sync([1, 2, 3, 4, 5, 6]);
        $productos[1]->finalidades()->sync([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);
        $productos[1]->ocupaciones()->sync([1, 2, 3, 4]);

    }
}
