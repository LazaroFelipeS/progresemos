<?php

use Illuminate\Database\Seeder;

class SituacionesTableSeeder extends Seeder
{
    /**
     * Ejecuta los seeds de la base de datos
     *
     * @return void
     */
    public function run()
    {
        $situaciones = [
            [
                'situacion'     => 'A',
                'encabezado'    => 'ATRASO EN CUENTA REPORTADO EN BURO',
                'introduccion'  => 'Vemos que en tu reporte de Buró de Crédito se muestra una o más cuentas con un atraso importante de pago.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'situacion'     => 'B',
                'encabezado'    => 'ACLARACIÓN SOBRE INGRESO Y/O GASTO FAMILIAR',
                'introduccion'  => 'En tu solicitud indicas gastos familiares elevados para el ingreso que nos informas.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'situacion'     => 'C',
                'encabezado'    => 'ACLARACIÓN SOBRE DEUDA RECIENTE',
                'introduccion'  => 'En tu reporte de Buró de Crédito observamos que recientemente has adquirido deuda significativa.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'situacion'     => 'D',
                'encabezado'    => 'PAGOS A PRÉSTAMOS ACTUALES',
                'introduccion'  => 'En tu reporte de Buró de Crédito se ven pagos elevados en relación con el ingreso que nos informas.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'situacion'     => 'E',
                'encabezado'    => 'FUENTE ACTUAL DE INGRESOS',
                'introduccion'  => null,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'situacion'     => 'F',
                'encabezado'    => 'PROPÓSITO DEL PRÉSTAMO',
                'introduccion'  => null,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'situacion'     => 'G',
                'encabezado'    => 'NOS GUSTARÍA ENTENDER TU NEGOCIO',
                'introduccion'  => null,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'situacion'     => 'H',
                'encabezado'    => 'TELEFONO FIJO',
                'introduccion'  => 'El teléfono del domicilio que proporcionaste es el mismo que el de tu celular.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'situacion'     => 'BD',
                'encabezado'    => 'ACLARACIÓN SOBRE INGRESO Y/O GASTO FAMILIAR Y PAGOS A PRÉSTAMOS ACTUALES',
                'introduccion'  => 'En tu solicitud indicas gastos familiares elevados para el ingreso que nos informas',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'situacion'     => 'I',
                'encabezado'    => 'REFERENCIA LABORAL',
                'introduccion'  => 'En tu solicitud indicas gastos familiares elevados para el ingreso que nos informas',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
        ];

        DB::table('situaciones')->insert($situaciones);


    }
}
