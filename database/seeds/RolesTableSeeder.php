<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Ejecuta los seeds de la base de datos
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name'          => 'administrador',
                'display_name'  => 'Administrador',
                'area'          => '',
                'description'   => 'Perfil para el Perfil para el Administrador del Panel',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'gerente de cobranza.cobranza',
                'display_name'  => 'Gerente de Cobranza',
                'area'          => 'Cobranza',
                'description'   => 'Perfil para el Gerente de Cobranza',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'ejecutivo telefónico.cobranza',
                'display_name'  => 'Ejecutivo telefónico',
                'area'          => 'Cobranza',
                'description'   => 'Perfil para el Ejecutivo telefónico',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'analista de dictámen.credito y riesgos',
                'display_name'  => 'Analista de Dictamen',
                'area'          => 'Credito y Riesgos',
                'description'   => 'Perfil para el Analista de Dictamen',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'becario.credito y riesgos',
                'display_name'  => 'Becario',
                'area'          => 'Credito y Riesgos',
                'description'   => 'Perfil para el Becario',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'analista de riesgos.credito y riesgos',
                'display_name'  => 'Analista de Riesgos',
                'area'          => 'Credito y Riesgos',
                'description'   => 'Perfil para el Analista de Riesgos',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'jefe de administración de riesgo de fraude.credito y riesgos',
                'display_name'  => 'Jefe de Riesgos',
                'area'          => 'Credito y Riesgos',
                'description'   => 'Perfil para el Jefe de Riesgos',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'director de riesgos.credito y riesgos',
                'display_name'  => 'Director de Riesgos',
                'area'          => 'Credito y Riesgos',
                'description'   => 'Perfil para el Director de Riesgos',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'jefe de análisis y dictámen.credito y riesgos',
                'display_name'  => 'Jefe de Análisis y Dictamén',
                'area'          => 'Credito y Riesgos',
                'description'   => 'Perfil para el Jefe de Análisis y Dictamén',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'director de crm & analytics.crm',
                'display_name'  => 'Director de CRM & Analytics',
                'area'          => 'CRM',
                'description'   => 'Perfil para el Director de CRM & Analytics',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'dirección.dirección',
                'display_name'  => 'Dirección',
                'area'          => 'Dirección',
                'description'   => 'Dirección',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'director de operaciones.operaciones',
                'display_name'  => 'Director de Operaciones',
                'area'          => 'Operaciones',
                'description'   => 'Perfil para el Director de Operaciones',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'asesor de atención a clientes.operaciones',
                'display_name'  => 'Asesor de Atención a clientes',
                'area'          => 'Operaciones',
                'description'   => 'Perfil para el Asesor de Atención a clientes',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'analista de métodos y procedimientos.operaciones',
                'display_name'  => 'Analista de Métodos y Procedimientos',
                'area'          => 'Operaciones',
                'description'   => 'Perfil para el Analista de Métodos y Procedimientos',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'jefe de operación de ingresos.operaciones',
                'display_name'  => 'Jefe de operación de ingresos',
                'area'          => 'Operaciones',
                'description'   => 'Perfil para el Jefe de operación de ingresos',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'gerente de desarrollo it.sistemas',
                'display_name'  => 'Gerente de Desarrollo',
                'area'          => 'Sistemas',
                'description'   => 'Perfil para el Gerente de Desarrollo',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'gerente de proyectos it.sistemas',
                'display_name'  => 'Gerente de Proyectos',
                'area'          => 'Sistemas',
                'description'   => 'Perfil para el Gerente de Proyectos',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'desarrollador senior.sistemas',
                'display_name'  => 'Desarrollador senior',
                'area'          => 'Sistemas',
                'description'   => 'Perfil para el Desarrollador senior',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'especialista en t24.sistemas',
                'display_name'  => 'Especialista en T24',
                'area'          => 'Sistemas',
                'description'   => 'Perfil para el Especialista en T24',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'gerente de ventas corporativas.ventas',
                'display_name'  => 'Gerente de Ventas Corporativas',
                'area'          => 'Ventas',
                'description'   => 'Perfil para el Gerente de Ventas Corporativas',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
        ];

        DB::table('roles')->insert($roles);

    }
}
