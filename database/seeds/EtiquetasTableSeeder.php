<?php

use Illuminate\Database\Seeder;

class EtiquetasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Etiquetas
    	$etiquetas=[
    		'INTL'=>[
    			'0'=>[
    				'etiqueta' => 'IN',
    				'descripcion' => 'etiqueta',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'1'=>[
    				'etiqueta' => 'VR',
    				'descripcion' => 'version',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'2'=>[
    				'etiqueta' => 'RO',
    				'descripcion' => 'referencia',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'3'=>[
    				'etiqueta' => 'PR',
    				'descripcion' => 'producto_requerido',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'4'=>[
    				'etiqueta' => 'CP',
    				'descripcion' => 'clave_pais',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'5'=>[
    				'etiqueta' => 'R4',
    				'descripcion' => 'r4',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'6'=>[
    				'etiqueta' => 'CU',
    				'descripcion' => 'member_code',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'7'=>[
    				'etiqueta' => 'PW',
    				'descripcion' => 'password',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'8'=>[
    				'etiqueta' => 'TR',
    				'descripcion' => 'tipo_responsabilidad',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'9'=>[
    				'etiqueta' => 'TC',
    				'descripcion' => 'tipo_contrato',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'10'=>[
    				'etiqueta' => 'MC',
    				'descripcion' => 'moneda_credito',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'11'=>[
    				'etiqueta' => 'IC',
    				'descripcion' => 'importe_credito',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'12'=>[
    				'etiqueta' => 'ID',
    				'descripcion' => 'idioma',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'13'=>[
    				'etiqueta' => 'TS',
    				'descripcion' => 'tipo_salida',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'14'=>[
    				'etiqueta' => 'TB',
    				'descripcion' => 'tamaño_bloque',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'15'=>[
    				'etiqueta' => 'II',
    				'descripcion' => 'identificador_impresora',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    			'16'=>[
    				'etiqueta' => 'R7',
    				'descripcion' => 'r7',
    				'tipo' => 'Envio',
    				'segmento' => 'INTL',
    				'formulario' => 'No'
    			],
    		],
			'PN_RES'=>[
    			'0'=>[
    				'etiqueta' => 'PN',
    				'descripcion' => 'apellido_p',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'1'=>[
    				'etiqueta' => '00',
    				'descripcion' => 'apellido_m',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'2'=>[
    				'etiqueta' => '01',
    				'descripcion' => 'apellido_adl',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'3'=>[
    				'etiqueta' => '02',
    				'descripcion' => 'nombre',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'4'=>[
    				'etiqueta' => '03',
    				'descripcion' => 'segundo_nombre',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'5'=>[
    				'etiqueta' => '04',
    				'descripcion' => 'fecha_nacimiento',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'6'=>[
    				'etiqueta' => '05',
    				'descripcion' => 'rfc',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'7'=>[
    				'etiqueta' => '06',
    				'descripcion' => 'prefijo_personal',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_9'
    			],
    			'8'=>[
    				'etiqueta' => '07',
    				'descripcion' => 'sufijo_personal',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_SP'
    			],
    			'9'=>[
    				'etiqueta' => '08',
    				'descripcion' => 'nacionalidad',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_10'
    			],
    			'10'=>[
    				'etiqueta' => '09',
    				'descripcion' => 'tipo_residencia',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_TR'
    			],
    			'11'=>[
    				'etiqueta' => '10',
    				'descripcion' => 'lic_conducir',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'12'=>[
    				'etiqueta' => '11',
    				'descripcion' => 'estado_civil',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_EC'
    			],
    			'13'=>[
    				'etiqueta' => '12',
    				'descripcion' => 'sexo',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_Genero'
    			],
    			'14'=>[
    				'etiqueta' => '13',
    				'descripcion' => 'cedula_profesional',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'15'=>[
    				'etiqueta' => '14',
    				'descripcion' => 'ife',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'16'=>[
    				'etiqueta' => '15',
    				'descripcion' => 'curp',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'17'=>[
    				'etiqueta' => '16',
    				'descripcion' => 'clave_pais',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_10'
    			],
    			'18'=>[
    				'etiqueta' => '17',
    				'descripcion' => 'num_dependientes',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'19'=>[
    				'etiqueta' => '18',
    				'descripcion' => 'edades_dependientes',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'20'=>[
    				'etiqueta' => '19',
    				'descripcion' => 'fecha_recep_dependientes',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			],
    			'21'=>[
    				'etiqueta' => '20',
    				'descripcion' => 'fecha_defuncion',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PN',
    				'formulario' => ''
    			]
    		],
    		'PA_RES'=>[
    			'0'=>[
    				'etiqueta' => 'PA',
    				'descripcion' => 'dom_calle',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => ''
    			],
    			'1'=>[
    				'etiqueta' => '00',
    				'descripcion' => 'dom_calle_segunda_linea',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => ''
    			],
    			'2'=>[
    				'etiqueta' => '01',
    				'descripcion' => 'dom_colonia',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => ''
    			],
    			'3'=>[
    				'etiqueta' => '02',
    				'descripcion' => 'dom_deleg',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => ''
    			],
    			'4'=>[
    				'etiqueta' => '03',
    				'descripcion' => 'dom_ciudad',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => ''
    			],
    			'5'=>[
    				'etiqueta' => '04',
    				'descripcion' => 'dom_estado',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_11'
    			],
    			'6'=>[
    				'etiqueta' => '05',
    				'descripcion' => 'dom_cp',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => ''
    			],
    			'7'=>[
    				'etiqueta' => '06',
    				'descripcion' => 'dom_fecha',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => ''
    			],
    			'8'=>[
    				'etiqueta' => '07',
    				'descripcion' => 'dom_tel_casa',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => ''
    			],
    			'9'=>[
    				'etiqueta' => '08',
    				'descripcion' => 'dom_tel_casa_ext',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => ''
    			],
    			'10'=>[
    				'etiqueta' => '09',
    				'descripcion' => 'dom_fax',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => ''
    			],
    			'11'=>[
    				'etiqueta' => '10',
    				'descripcion' => 'dom_tipo',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_D'
    			],
    			'12'=>[
    				'etiqueta' => '11',
    				'descripcion' => 'dom_indicador_espec',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_ED'
    			],
    			'13'=>[
    				'etiqueta' => '12',
    				'descripcion' => 'dom_fecha_reporte',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => ''
    			],
    			'14'=>[
    				'etiqueta' => '13',
    				'descripcion' => 'dom_origen',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PA',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_10'
    			],
			],
			'PE_RES'=>[
    			'0'=>[
    				'etiqueta' => 'PE',
    				'descripcion' => 'emp_razon_social',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'1'=>[
    				'etiqueta' => '00',
    				'descripcion' => 'emp_dir1',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'2'=>[
    				'etiqueta' => '01',
    				'descripcion' => 'emp_dir2',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'3'=>[
    				'etiqueta' => '02',
    				'descripcion' => 'emp_colonia',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'4'=>[
    				'etiqueta' => '03',
    				'descripcion' => 'emp_deleg',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'5'=>[
    				'etiqueta' => '04',
    				'descripcion' => 'emp_ciudad',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'6'=>[
    				'etiqueta' => '05',
    				'descripcion' => 'emp_estado',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_11'
    			],
    			'7'=>[
    				'etiqueta' => '06',
    				'descripcion' => 'emp_cp',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'8'=>[
    				'etiqueta' => '07',
    				'descripcion' => 'emp_num_tel',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'9'=>[
    				'etiqueta' => '08',
    				'descripcion' => 'emp_tel_ext',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'10'=>[
    				'etiqueta' => '09',
    				'descripcion' => 'emp_fax',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'11'=>[
    				'etiqueta' => '10',
    				'descripcion' => 'emp_cargo',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'12'=>[
    				'etiqueta' => '11',
    				'descripcion' => 'emp_fecha_contrat',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'13'=>[
    				'etiqueta' => '12',
    				'descripcion' => 'emp_clave_moneda',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_TM'
    			],
    			'14'=>[
    				'etiqueta' => '13',
    				'descripcion' => 'emp_monto_sueldo',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'15'=>[
    				'etiqueta' => '14',
    				'descripcion' => 'emp_periodo_pago',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_PP'
    			],
    			'16'=>[
    				'etiqueta' => '15',
    				'descripcion' => 'emp_num_empleado',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'17'=>[
    				'etiqueta' => '16',
    				'descripcion' => 'emp_fecha_ult_dia',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'18'=>[
    				'etiqueta' => '17',
    				'descripcion' => 'emp_fecha_reporte',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'19'=>[
    				'etiqueta' => '18',
    				'descripcion' => 'emp_fecha_verificacion',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => ''
    			],
    			'20'=>[
    				'etiqueta' => '19',
    				'descripcion' => 'emp_modo_verificacion',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_MV'
    			],
    			'21'=>[
    				'etiqueta' => '20',
    				'descripcion' => 'emp_origen_razon_social',
    				'tipo' => 'Respuesta',
    				'segmento' => 'PE',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_10'
    			]
			],
			'TL_RES'=>[
    			'0'=>[
    				'etiqueta' => 'TL',
    				'descripcion' => 'cuenta_fecha_actualizacion',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
    			],
    			'1'=>[
    				'etiqueta' => '00',
    				'descripcion' => 'cuenta_impugnadoo',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
    			],
    			'2'=>[
    				'etiqueta' => '01',
    				'descripcion' => 'cuenta_clave_member_code', //Solo se buscan las dos Primeras Posiciones, Los Siguientes 4 Identifican al Usuario, Los Ultimos 4 pueden Identificar: Producto y Sucursales o Area Interna
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_1'
    			],
    			'3'=>[
    				'etiqueta' => '02',
    				'descripcion' => 'cuenta_nombre_usuario',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'4'=>[
    				'etiqueta' => '03',
    				'descripcion' => 'cuenta_num_tel',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'5'=>[
    				'etiqueta' => '04',
    				'descripcion' => 'cuenta_num_cuenta',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'6'=>[
    				'etiqueta' => '05',
    				'descripcion' => 'cuenta_responsabilidad',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_TRC'
       			],
       			'7'=>[
    				'etiqueta' => '06',
    				'descripcion' => 'cuenta_tipo',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_TC'
       			],
       			'8'=>[
    				'etiqueta' => '07',
    				'descripcion' => 'cuenta_contrato_producto',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_2'
       			],
       			'9'=>[
    				'etiqueta' => '08',
    				'descripcion' => 'cuenta_moneda',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_MC'
       			],
       			'10'=>[
    				'etiqueta' => '09',
    				'descripcion' => 'cuenta_importe_evaluo',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'11'=>[
    				'etiqueta' => '10',
    				'descripcion' => 'cuenta_num_pagos',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'12'=>[
    				'etiqueta' => '11',
    				'descripcion' => 'cuenta_frecuencia_pagos',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_FP'
       			],
       			'13'=>[
    				'etiqueta' => '12',
    				'descripcion' => 'cuenta_monto_pagar',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'14'=>[
    				'etiqueta' => '13',
    				'descripcion' => 'cuenta_fecha_apertura',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'15'=>[
    				'etiqueta' => '14',
    				'descripcion' => 'cuenta_fecha_ult_pago',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'16'=>[
    				'etiqueta' => '15',
    				'descripcion' => 'cuenta_fecha_ult_compra',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'17'=>[
    				'etiqueta' => '16',
    				'descripcion' => 'cuenta_fecha_cierre',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'18'=>[
    				'etiqueta' => '17',
    				'descripcion' => 'cuenta_fecha_reporte',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'19'=>[
    				'etiqueta' => '18',
    				'descripcion' => 'cuenta_modo_reporte',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_MR'
       			],
       			'20'=>[
    				'etiqueta' => '19',
    				'descripcion' => 'cuenta_ult_fecha_cero',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'21'=>[
    				'etiqueta' => '20',
    				'descripcion' => 'cuenta_garantia',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'22'=>[
    				'etiqueta' => '21',
    				'descripcion' => 'cuenta_cred_max_aut',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'23'=>[
    				'etiqueta' => '22',
    				'descripcion' => 'cuenta_saldo_actual',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'24'=>[
    				'etiqueta' => '23',
    				'descripcion' => 'cuenta_limite_credito',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'25'=>[
    				'etiqueta' => '24',
    				'descripcion' => 'cuenta_saldo_vencido',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'26'=>[
    				'etiqueta' => '25',
    				'descripcion' => 'cuenta_num_pagos_vencidos',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'27'=>[
    				'etiqueta' => '26',
    				'descripcion' => 'cuenta_mop',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_3'
       			],
       			'28'=>[
    				'etiqueta' => '27',
    				'descripcion' => 'cuenta_hist_pagos',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_4'
       			],
       			'29'=>[
    				'etiqueta' => '28',
    				'descripcion' => 'cuenta_hist_pagos_fecha_reciente',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'30'=>[
    				'etiqueta' => '29',
    				'descripcion' => 'cuenta_hist_pagos_fecha_antigua',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'31'=>[
    				'etiqueta' => '30',
    				'descripcion' => 'cuenta_clave_observacion',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_5'
       			],
       			'32'=>[
    				'etiqueta' => '31',
    				'descripcion' => 'cuenta_total_pagos',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'33'=>[
    				'etiqueta' => '32',
    				'descripcion' => 'cuenta_total_pagos_mop2',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'34'=>[
    				'etiqueta' => '33',
    				'descripcion' => 'cuenta_total_pagos_mop3',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'35'=>[
    				'etiqueta' => '34',
    				'descripcion' => 'cuenta_total_pagos_mop4',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'36'=>[
    				'etiqueta' => '35',
    				'descripcion' => 'cuenta_total_pagos_mop5_plus',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'37'=>[
    				'etiqueta' => '36',
    				'descripcion' => 'cuenta_saldo_morosidad_mas_alta',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'38'=>[
    				'etiqueta' => '37',
    				'descripcion' => 'cuenta_fecha_morosidad_mas_alta',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'39'=>[
    				'etiqueta' => '38',
    				'descripcion' => 'cuenta_clasif_puntualidad_de_pago',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_3'
       			],
       			'40'=>[
    				'etiqueta' => '42',
    				'descripcion' => 'cuenta_fecha_inicio_reestructura',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
       			'41'=>[
    				'etiqueta' => '45',
    				'descripcion' => 'cuenta_monto_ultimo_pago',
    				'tipo' => 'Respuesta',
    				'segmento' => 'TL',
    				'formulario' => ''
       			],
			],
			'IQ_RES'=>[
    			'0'=>[
    				'etiqueta' => 'IQ',
    				'descripcion' => 'consulta_fecha',
    				'tipo' => 'Respuesta',
    				'segmento' => 'IQ',
    				'formulario' => ''
    			],
    			'1'=>[
    				'etiqueta' => '00',
    				'descripcion' => 'consulta_reservado1',
    				'tipo' => 'Respuesta',
    				'segmento' => 'IQ',
    				'formulario' => ''
    			],
    			'2'=>[
    				'etiqueta' => '01',
    				'descripcion' => 'consulta_clave_member_code',
    				'tipo' => 'Respuesta',
    				'segmento' => 'IQ',
    				'formulario' => '',
                    'buscar_valor' => 'Anexo_1'
    			],
    			'3'=>[
    				'etiqueta' => '02',
    				'descripcion' => 'consulta_nombre_usuario',
    				'tipo' => 'Respuesta',
    				'segmento' => 'IQ',
    				'formulario' => ''
    			],
    			'4'=>[
    				'etiqueta' => '03',
    				'descripcion' => 'consulta_num_tel',
    				'tipo' => 'Respuesta',
    				'segmento' => 'IQ',
    				'formulario' => ''
    			],
    			'5'=>[
    				'etiqueta' => '04',
    				'descripcion' => 'consulta_contrato_producto',
    				'tipo' => 'Respuesta',
    				'segmento' => 'IQ',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_2'
    			],
    			'6'=>[
    				'etiqueta' => '05',
    				'descripcion' => 'consulta_moneda',
    				'tipo' => 'Respuesta',
    				'segmento' => 'IQ',
    				'formulario' => '',
    				'buscar_valor' => 'Anexo_MC'
    			],
    			'7'=>[
    				'etiqueta' => '06',
    				'descripcion' => 'consulta_importe',
    				'tipo' => 'Respuesta',
    				'segmento' => 'IQ',
    				'formulario' => ''
    			],
                '8'=>[
                    'etiqueta' => '07',
                    'descripcion' => 'consulta_responsabilidad',
                    'tipo' => 'Respuesta',
                    'segmento' => 'IQ',
                    'formulario' => '',
                    'buscar_valor' => 'Anexo_TRC'
                ],
                '9'=>[
                    'etiqueta' => '08',
                    'descripcion' => 'consulta_indicador_cliente',
                    'tipo' => 'Respuesta',
                    'segmento' => 'IQ',
                    'formulario' => ''
                ],
                '10'=>[
                    'etiqueta' => '09',
                    'descripcion' => 'consulta_reservado2',
                    'tipo' => 'Respuesta',
                    'segmento' => 'IQ',
                    'formulario' => ''
                ],
			],
            'RS_RES'=>[
                '0'=>[
                    'etiqueta' => 'RS',
                    'descripcion' => 'resumen_fecha_integracion',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '1'=>[
                    'etiqueta' => '00',
                    'descripcion' => 'resumen_cuentas_mop7',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '2'=>[
                    'etiqueta' => '01',
                    'descripcion' => 'resumen_cuentas_mop6',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '3'=>[
                    'etiqueta' => '02',
                    'descripcion' => 'resumen_cuentas_mop5',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '4'=>[
                    'etiqueta' => '03',
                    'descripcion' => 'resumen_cuentas_mop4',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '5'=>[
                    'etiqueta' => '04',
                    'descripcion' => 'resumen_cuentas_mop3',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '6'=>[
                    'etiqueta' => '05',
                    'descripcion' => 'resumen_cuentas_mop2',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '7'=>[
                    'etiqueta' => '06',
                    'descripcion' => 'resumen_cuentas_mop1',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '8'=>[
                    'etiqueta' => '07',
                    'descripcion' => 'resumen_cuentas_mop0',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '9'=>[
                    'etiqueta' => '08',
                    'descripcion' => 'resumen_cuentas_mop_ur',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '10'=>[
                    'etiqueta' => '09',
                    'descripcion' => 'resumen_numero_de_cuentas',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '11'=>[
                    'etiqueta' => '10',
                    'descripcion' => 'resumen_cuentas_pagos_fijos_hipo',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '12'=>[
                    'etiqueta' => '11',
                    'descripcion' => 'resumen_cuentas_revolventes_sin_limite',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '13'=>[
                    'etiqueta' => '12',
                    'descripcion' => 'resumen_cuentas_cerradas',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '14'=>[
                    'etiqueta' => '13',
                    'descripcion' => 'resumen_cuentas_con_morosidad_actual',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '15'=>[
                    'etiqueta' => '14',
                    'descripcion' => 'resumen_cuentas_con_historial_de_morosidad',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '16'=>[
                    'etiqueta' => '15',
                    'descripcion' => 'resumen_cuentas_en_aclaracion',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '17'=>[
                    'etiqueta' => '16',
                    'descripcion' => 'resumen_solicitudes_de_consulta',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '18'=>[
                    'etiqueta' => '17',
                    'descripcion' => 'resumen_nueva_direccion_ult_60_dias',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => '',
                    'buscar_valor' => 'Anexo_ND'
                ],
                '19'=>[
                    'etiqueta' => '18',
                    'descripcion' => 'resumen_mensaje_de_alerta',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '20'=>[
                    'etiqueta' => '19',
                    'descripcion' => 'resumen_declarativa',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => '',
                    'buscar_valor' => 'Anexo_DE'
                ],
                '21'=>[
                    'etiqueta' => '20',
                    'descripcion' => 'resumen_moneda_del_credito',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => '',
                    'buscar_valor' => 'Anexo_MC'
                ],
                '22'=>[
                    'etiqueta' => '21',
                    'descripcion' => 'resumen_total_creditos_max_rev_sinlim',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '23'=>[
                    'etiqueta' => '22',
                    'descripcion' => 'resumen_total_limites_de_credito_rev_sinlim',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '24'=>[
                    'etiqueta' => '23',
                    'descripcion' => 'resumen_total_saldos_actuales_rev_sinlim',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '25'=>[
                    'etiqueta' => '24',
                    'descripcion' => 'resumen_total_saldos_vencidos_rev_sinlim',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '26'=>[
                    'etiqueta' => '25',
                    'descripcion' => 'resumen_total_importe_de_pago_rev_sinlim',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '27'=>[
                    'etiqueta' => '26',
                    'descripcion' => 'resumen_porcentaje_limite_de_credito_utilizado_rev_sinlim',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '28'=>[
                    'etiqueta' => '27',
                    'descripcion' => 'resumen_total_creditos_max_fijos_hipo',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '29'=>[
                    'etiqueta' => '28',
                    'descripcion' => 'resumen_total_saldos_actuales_fijos_hipo',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '30'=>[
                    'etiqueta' => '29',
                    'descripcion' => 'resumen_total_saldos_vencidos_fijos_hipo',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '31'=>[
                    'etiqueta' => '30',
                    'descripcion' => 'resumen_total_importe_de_pago_fijos_hipo',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '32'=>[
                    'etiqueta' => '31',
                    'descripcion' => 'resumen_cuentas_mop96',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '33'=>[
                    'etiqueta' => '32',
                    'descripcion' => 'resumen_cuentas_mop97',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '34'=>[
                    'etiqueta' => '33',
                    'descripcion' => 'resumen_cuentas_mop99',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '35'=>[
                    'etiqueta' => '34',
                    'descripcion' => 'resumen_fecha_apertura_cuenta_antigua',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '36'=>[
                    'etiqueta' => '35',
                    'descripcion' => 'resumen_fecha_apertura_cuenta_reciente',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '37'=>[
                    'etiqueta' => '36',
                    'descripcion' => 'resumen_num_solicitudes_informe_buro',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '38'=>[
                    'etiqueta' => '37',
                    'descripcion' => 'resumen_fecha_consulta_reciente',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '39'=>[
                    'etiqueta' => '38',
                    'descripcion' => 'resumen_num_cuentas_despacho_cobranza',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '40'=>[
                    'etiqueta' => '39',
                    'descripcion' => 'resumen_fecha_apertura_cuenta_en_despacho_reciente',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '41'=>[
                    'etiqueta' => '40',
                    'descripcion' => 'resumen_num_solicitudes_informe_por_despachos',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
                '42'=>[
                    'etiqueta' => '41',
                    'descripcion' => 'resumen_fecha_consulta_por_despacho_reciente',
                    'tipo' => 'Respuesta',
                    'segmento' => 'RS',
                    'formulario' => ''
                ],
            ],
            'HI_RES'=>[
                '0'=>[
                    'etiqueta' => 'HI',
                    'descripcion' => 'hawk_fecha_reporte',
                    'tipo' => 'Respuesta',
                    'segmento' => 'HI',
                    'formulario' => ''
                ],
                '1'=>[
                    'etiqueta' => '00',
                    'descripcion' => 'hawk_codigo_prevencion',
                    'tipo' => 'Respuesta',
                    'segmento' => 'HI',
                    'formulario' => ''
                ],
                '2'=>[
                    'etiqueta' => '01',
                    'descripcion' => 'hawk_tipo_usuario',
                    'tipo' => 'Respuesta',
                    'segmento' => 'HI',
                    'formulario' => ''
                ],
                '3'=>[
                    'etiqueta' => '02',
                    'descripcion' => 'hawk_mensaje',
                    'tipo' => 'Respuesta',
                    'segmento' => 'HI',
                    'formulario' => ''
                ],
            ],
            'HR_RES'=>[
                '0'=>[
                    'etiqueta' => 'HR',
                    'descripcion' => 'hawk_fecha_reporte',
                    'tipo' => 'Respuesta',
                    'segmento' => 'HR',
                    'formulario' => ''
                ],
                '1'=>[
                    'etiqueta' => '00',
                    'descripcion' => 'hawk_codigo_prevencion',
                    'tipo' => 'Respuesta',
                    'segmento' => 'HR',
                    'formulario' => ''
                ],
                '2'=>[
                    'etiqueta' => '01',
                    'descripcion' => 'hawk_tipo_usuario',
                    'tipo' => 'Respuesta',
                    'segmento' => 'HR',
                    'formulario' => ''
                ],
                '3'=>[
                    'etiqueta' => '02',
                    'descripcion' => 'hawk_mensaje',
                    'tipo' => 'Respuesta',
                    'segmento' => 'HR',
                    'formulario' => ''
                ],
            ],
            'CR_RES'=>[
                '0'=>[
                    'etiqueta' => 'CR',
                    'descripcion' => 'tipo_segmento',
                    'tipo' => 'Respuesta',
                    'segmento' => 'CR',
                    'formulario' => ''
                ],
                '1'=>[
                    'etiqueta' => '00',
                    'descripcion' => 'declarativa_cliente',
                    'tipo' => 'Respuesta',
                    'segmento' => 'CR',
                    'formulario' => ''
                ],
            ],
            'SC_RES'=>[
                '0'=>[
                    'etiqueta' => 'SC',
                    'descripcion' => 'micro_nombre',
                    'tipo' => 'Respuesta',
                    'segmento' => 'SC',
                    'formulario' => ''
                ],
                '1'=>[
                    'etiqueta' => '00',
                    'descripcion' => 'micro_codigo_prod',
                    'tipo' => 'Respuesta',
                    'segmento' => 'SC',
                    'formulario' => ''
                ],
                '2'=>[
                    'etiqueta' => '01',
                    'descripcion' => 'micro_valor',
                    'tipo' => 'Respuesta',
                    'segmento' => 'SC',
                    'formulario' => '',
                    'buscar_valor' => 'Anexo_CS'
                ],
                '3'=>[
                    'etiqueta' => '02',
                    'descripcion' => 'micro_razon1',
                    'tipo' => 'Respuesta',
                    'segmento' => 'SC',
                    'formulario' => '',
                    'buscar_valor' => 'Anexo_6'
                ],
                '4'=>[
                    'etiqueta' => '03',
                    'descripcion' => 'micro_razon2',
                    'tipo' => 'Respuesta',
                    'segmento' => 'SC',
                    'formulario' => '',
                    'buscar_valor' => 'Anexo_6'
                ],
                '5'=>[
                    'etiqueta' => '04',
                    'descripcion' => 'micro_razon3',
                    'tipo' => 'Respuesta',
                    'segmento' => 'SC',
                    'formulario' => '',
                    'buscar_valor' => 'Anexo_6'
                ],
                '6'=>[
                    'etiqueta' => '06',
                    'descripcion' => 'micro_codigo_error',
                    'tipo' => 'Respuesta',
                    'segmento' => 'SC',
                    'formulario' => '',
                    'buscar_valor' => 'Anexo_6'
                ],
            ],
            'CL_RES'=>[
                '0'=>[
                    'etiqueta' => 'CL',
                    'descripcion' => 'plantilla_solicitada',
                    'tipo' => 'Respuesta',
                    'segmento' => 'CL',
                    'formulario' => ''
                ],
                '1'=>[
                    'etiqueta' => '00',
                    'descripcion' => 'indentificador_plantilla',
                    'tipo' => 'Respuesta',
                    'segmento' => 'CL',
                    'formulario' => ''
                ],
                '2'=>[
                    'etiqueta' => '01',
                    'descripcion' => 'numero_caracteristica',
                    'tipo' => 'Respuesta',
                    'segmento' => 'CL',
                    'formulario' => ''
                ],
                '3'=>[
                    'etiqueta' => '02',
                    'descripcion' => 'valor_caracteristica',
                    'tipo' => 'Respuesta',
                    'segmento' => 'CL',
                    'formulario' => ''
                ],
                '4'=>[
                    'etiqueta' => '03',
                    'descripcion' => 'codigo_error',
                    'tipo' => 'Respuesta',
                    'segmento' => 'CL',
                    'formulario' => ''
                ],
            ],
            'ERRR_RES'=>[
                '0'=>[
                    'etiqueta' => 'ER',
                    'descripcion' => 'Inicio de registro de Error',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '1'=>[
                    'etiqueta' => 'UR',
                    'descripcion' => 'Referencia del operador',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '2'=>[
                    'etiqueta' => '00',
                    'descripcion' => 'Solicitud del cliente erronea',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '3'=>[
                    'etiqueta' => '01',
                    'descripcion' => 'Version proporcionada erronea',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '4'=>[
                    'etiqueta' => '02',
                    'descripcion' => 'Producto solicitado erroneo',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '5'=>[
                    'etiqueta' => '03',
                    'descripcion' => 'Clave de Usuario o Contraseña de accesso erronea',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '6'=>[
                    'etiqueta' => '04',
                    'descripcion' => 'Segmento requerido no proporcionado',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '7'=>[
                    'etiqueta' => '05',
                    'descripcion' => 'Ultima información valida del Cliente',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '8'=>[
                    'etiqueta' => '06',
                    'descripcion' => 'Informacion erronea para consulta',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '9'=>[
                    'etiqueta' => '07',
                    'descripcion' => 'Valor erroneo de un campo relacionado',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '10'=>[
                    'etiqueta' => '11',
                    'descripcion' => 'Error en el sistema de buro de credito',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '11'=>[
                    'etiqueta' => '12',
                    'descripcion' => 'Etiqueta de segmento erronea',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '12'=>[
                    'etiqueta' => '13',
                    'descripcion' => 'Orden erroneo del segmento',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '13'=>[
                    'etiqueta' => '14',
                    'descripcion' => 'Numero erroneo de segmentos',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '14'=>[
                    'etiqueta' => '16',
                    'descripcion' => 'Falta campo requerido',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '15'=>[
                    'etiqueta' => '20',
                    'descripcion' => 'Expediente bloqueado para consulta',
                    'tipo' => 'Respuesta',
                    'segmento' => 'UR',
                    'formulario' => ''
                ],
                '16'=>[
                    'etiqueta' => 'ER',
                    'descripcion' => 'Inicio de registro de Error',
                    'tipo' => 'Respuesta',
                    'segmento' => 'AR',
                    'formulario' => ''
                ],
                '17'=>[
                    'etiqueta' => 'AR',
                    'descripcion' => 'Referencia del operador',
                    'tipo' => 'Respuesta',
                    'segmento' => 'AR',
                    'formulario' => ''
                ],
                '18'=>[
                    'etiqueta' => '00',
                    'descripcion' => 'Sujeto no Autenticado',
                    'tipo' => 'Respuesta',
                    'segmento' => 'AR',
                    'formulario' => ''
                ],
                '19'=>[
                    'etiqueta' => '03',
                    'descripcion' => 'Clave de Usuario o Contraseña de accesso erronea',
                    'tipo' => 'Respuesta',
                    'segmento' => 'AR',
                    'formulario' => ''
                ],
                '20'=>[
                    'etiqueta' => '11',
                    'descripcion' => 'Error en el sistema de buro de credito',
                    'tipo' => 'Respuesta',
                    'segmento' => 'AR',
                    'formulario' => ''
                ],
                '21'=>[
                    'etiqueta' => '12',
                    'descripcion' => 'Etiqueta de segmento erronea',
                    'tipo' => 'Respuesta',
                    'segmento' => 'AR',
                    'formulario' => ''
                ],
                '22'=>[
                    'etiqueta' => '16',
                    'descripcion' => 'Falta campo requerido',
                    'tipo' => 'Respuesta',
                    'segmento' => 'AR',
                    'formulario' => ''
                ],
                '23'=>[
                    'etiqueta' => '18',
                    'descripcion' => 'Expediente bloqueado',
                    'tipo' => 'Respuesta',
                    'segmento' => 'AR',
                    'formulario' => ''
                ],
            ],
            'ES_RES'=>[
                '0'=>[
                    'etiqueta' => 'ES',
                    'descripcion' => 'cierre_longitud_resp',
                    'tipo' => 'Respuesta',
                    'segmento' => 'ES',
                    'formulario' => ''
                ],
                '1'=>[
                    'etiqueta' => '00',
                    'descripcion' => 'cierre_num_control',
                    'tipo' => 'Respuesta',
                    'segmento' => 'ES',
                    'formulario' => ''
                ],
                '2'=>[
                    'etiqueta' => '01',
                    'descripcion' => 'cierre_registro_resp',
                    'tipo' => 'Respuesta',
                    'segmento' => 'ES',
                    'formulario' => ''
                ],
            ]
    	];

    	foreach ($etiquetas['INTL'] as $etiqueta) {
    		DB::table('etiquetas')->insert([
				'etiqueta'=>$etiqueta['etiqueta'],
				'descripcion'=>$etiqueta['descripcion'],
				'tipo'=>$etiqueta['tipo'],
				'segmento'=>$etiqueta['segmento'],
				'formulario'=>$etiqueta['formulario']
			]);
    	}

    	foreach ($etiquetas['PN_RES'] as $etiqueta) {
    		if (isset($etiqueta['buscar_valor'])){
	    		DB::table('etiquetas')->insert([
					'etiqueta'=>$etiqueta['etiqueta'],
					'descripcion'=>$etiqueta['descripcion'],
					'tipo'=>$etiqueta['tipo'],
					'segmento'=>$etiqueta['segmento'],
					'formulario'=>$etiqueta['formulario'],
					'buscar_valor'=>$etiqueta['buscar_valor']
				]);
	    	}else{
	    		DB::table('etiquetas')->insert([
					'etiqueta'=>$etiqueta['etiqueta'],
					'descripcion'=>$etiqueta['descripcion'],
					'tipo'=>$etiqueta['tipo'],
					'segmento'=>$etiqueta['segmento'],
					'formulario'=>$etiqueta['formulario']
				]);
	    	}
    	}

    	foreach ($etiquetas['PA_RES'] as $etiqueta) {
    		if (isset($etiqueta['buscar_valor'])){
	    		DB::table('etiquetas')->insert([
					'etiqueta'=>$etiqueta['etiqueta'],
					'descripcion'=>$etiqueta['descripcion'],
					'tipo'=>$etiqueta['tipo'],
					'segmento'=>$etiqueta['segmento'],
					'formulario'=>$etiqueta['formulario'],
					'buscar_valor'=>$etiqueta['buscar_valor']
				]);
	    	}else{
	    		DB::table('etiquetas')->insert([
					'etiqueta'=>$etiqueta['etiqueta'],
					'descripcion'=>$etiqueta['descripcion'],
					'tipo'=>$etiqueta['tipo'],
					'segmento'=>$etiqueta['segmento'],
					'formulario'=>$etiqueta['formulario']
				]);
	    	}
    	}

    	foreach ($etiquetas['PE_RES'] as $etiqueta) {
    		if (isset($etiqueta['buscar_valor'])){
	    		DB::table('etiquetas')->insert([
					'etiqueta'=>$etiqueta['etiqueta'],
					'descripcion'=>$etiqueta['descripcion'],
					'tipo'=>$etiqueta['tipo'],
					'segmento'=>$etiqueta['segmento'],
					'formulario'=>$etiqueta['formulario'],
					'buscar_valor'=>$etiqueta['buscar_valor']
				]);
	    	}else{
	    		DB::table('etiquetas')->insert([
					'etiqueta'=>$etiqueta['etiqueta'],
					'descripcion'=>$etiqueta['descripcion'],
					'tipo'=>$etiqueta['tipo'],
					'segmento'=>$etiqueta['segmento'],
					'formulario'=>$etiqueta['formulario']
				]);
	    	}
    	}

    	foreach ($etiquetas['TL_RES'] as $etiqueta) {
    		if (isset($etiqueta['buscar_valor'])){
	    		DB::table('etiquetas')->insert([
					'etiqueta'=>$etiqueta['etiqueta'],
					'descripcion'=>$etiqueta['descripcion'],
					'tipo'=>$etiqueta['tipo'],
					'segmento'=>$etiqueta['segmento'],
					'formulario'=>$etiqueta['formulario'],
					'buscar_valor'=>$etiqueta['buscar_valor']
				]);
	    	}else{
	    		DB::table('etiquetas')->insert([
					'etiqueta'=>$etiqueta['etiqueta'],
					'descripcion'=>$etiqueta['descripcion'],
					'tipo'=>$etiqueta['tipo'],
					'segmento'=>$etiqueta['segmento'],
					'formulario'=>$etiqueta['formulario']
				]);
	    	}
    	}

        foreach ($etiquetas['IQ_RES'] as $etiqueta) {
            if (isset($etiqueta['buscar_valor'])){
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario'],
                    'buscar_valor'=>$etiqueta['buscar_valor']
                ]);
            }else{
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario']
                ]);
            }
        }

        foreach ($etiquetas['RS_RES'] as $etiqueta) {
            if (isset($etiqueta['buscar_valor'])){
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario'],
                    'buscar_valor'=>$etiqueta['buscar_valor']
                ]);
            }else{
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario']
                ]);
            }
        }

        foreach ($etiquetas['HI_RES'] as $etiqueta) {
            if (isset($etiqueta['buscar_valor'])){
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario'],
                    'buscar_valor'=>$etiqueta['buscar_valor']
                ]);
            }else{
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario']
                ]);
            }
        }

        foreach ($etiquetas['HR_RES'] as $etiqueta) {
            if (isset($etiqueta['buscar_valor'])){
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario'],
                    'buscar_valor'=>$etiqueta['buscar_valor']
                ]);
            }else{
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario']
                ]);
            }
        }

        foreach ($etiquetas['CR_RES'] as $etiqueta) {
            if (isset($etiqueta['buscar_valor'])){
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario'],
                    'buscar_valor'=>$etiqueta['buscar_valor']
                ]);
            }else{
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario']
                ]);
            }
        }

        foreach ($etiquetas['SC_RES'] as $etiqueta) {
            if (isset($etiqueta['buscar_valor'])){
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario'],
                    'buscar_valor'=>$etiqueta['buscar_valor']
                ]);
            }else{
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario']
                ]);
            }
        }

        foreach ($etiquetas['ERRR_RES'] as $etiqueta) {
            if (isset($etiqueta['buscar_valor'])){
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario'],
                    'buscar_valor'=>$etiqueta['buscar_valor']
                ]);
            }else{
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario']
                ]);
            }
        }

        foreach ($etiquetas['ES_RES'] as $etiqueta) {
            if (isset($etiqueta['buscar_valor'])){
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario'],
                    'buscar_valor'=>$etiqueta['buscar_valor']
                ]);
            }else{
                DB::table('etiquetas')->insert([
                    'etiqueta'=>$etiqueta['etiqueta'],
                    'descripcion'=>$etiqueta['descripcion'],
                    'tipo'=>$etiqueta['tipo'],
                    'segmento'=>$etiqueta['segmento'],
                    'formulario'=>$etiqueta['formulario']
                ]);
            }
        }

	}
}
