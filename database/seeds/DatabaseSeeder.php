<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PlantillasComunicacionesTableSeeder::class);
        $this->call(SituacionesTableSeeder::class);
        $this->call(CuestionariosDinamicosTableSeeder::class);
        $this->call(PlazosTableSeeder::class);
        $this->call(FinalidadesTableSeeder::class);
        $this->call(OcupacionesTableSeeder::class);
        $this->call(ProductosTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        $this->call(EtiquetasTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MotivosRechazoTableSeeder::class);
    }
}
