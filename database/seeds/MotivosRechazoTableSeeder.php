<?php

use Illuminate\Database\Seeder;

class MotivosRechazoTableSeeder extends Seeder
{
    /**
     * Ejecuta los seeds de la base de datos
     *
     * @return void
     */
    public function run()
    {
        $motivos = [
            [
                'motivo'     => 'El monto de la oferta es inferior a lo que solicité',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'motivo'     => 'El pago es mayor que el que puedo manejar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'motivo'     => 'La tasa de interés es mayor que la que esperaba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'motivo'     => 'No tengo comprobante de ingreso',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'motivo'     => 'No tengo otros documentos que se solicitan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'motivo'     => 'Otro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];

        DB::table('motivos_rechazo')->insert($motivos);

    }
}
