<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Ejecuta los seeds de la base de datos
     *
     * @return void
     */
    public function run()
    {
        $permisos = [
            [
                'name'          => 'prospectos',
                'display_name'  => 'Prospectos',
                'description'   => 'Puede visualizar el listado de prospectos del menú Dashboard.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'solicitudes',
                'display_name'  => 'Solicitudes',
                'description'   => 'Puede visualizar las solicitudes relacionadas a un prospecto.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'detalle-solicitud',
                'display_name'  => 'Detalle solicitud',
                'description'   => 'Pueve visualizar el detalle de la solicitud.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'calendario-citas',
                'display_name'  => 'Calendario Citas',
                'description'   => 'Puede visualizar el menú que muestra el calendario de citas.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'reportes',
                'display_name'  => 'Reportes',
                'description'   => 'Puede visualizar el menú para generar Reportes.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'tabla-local',
                'display_name'  => 'Tabla local',
                'description'   => 'Puede visualizar el menú para ver los registros de prueba de Buró de crédito.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'logs-registro',
                'display_name'  => 'Logs de registro',
                'description'   => 'Puede visualizar el menú Log de Registro para ver los logs de errores en el proceso de registro del sitio.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'codigos-postales',
                'display_name'  => 'Códigos postales',
                'description'   => 'Puede visualizar el menú Códigos postales para ver la cobertura de plazas por código postal y actualizar el catálogo de códigos postales.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'alta-automatica',
                'display_name'  => 'Alta automática de clientes',
                'description'   => 'Permite visualizar el panel de alta autómatica de clientes en T24.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'administrador-productos',
                'display_name'  => 'Administrador de productos',
                'description'   => 'Permite visualizar el administrador de productos del sitio.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'prueba-registro',
                'display_name'  => 'Prueba registro',
                'description'   => 'Permite generar una prueba de registro, conectandose a tabla local o buró de crédito. Actualmente esta sin funcionamiento.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'administracion-usuarios',
                'display_name'  => 'Administración de usuarios y perfiles',
                'description'   => 'Permite administrar los usuarios y perfiles del panel.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'cliente-estrella',
                'display_name'  => 'Alta cliente estrella',
                'description'   => 'Permite enviar la campaña de cliente estrella.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'name'          => 'captura',
                'display_name'  => 'Captura de solitudes',
                'description'   => 'Permite mostrar el menu para captura de solicitudes.',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
        ];

        DB::table('permissions')->insert($permisos);

    }
}
