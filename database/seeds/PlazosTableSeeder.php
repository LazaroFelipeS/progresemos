<?php

use Illuminate\Database\Seeder;

class PlazosTableSeeder extends Seeder
{
    /**
     * Ejecuta los seeds de la base de datos
     *
     * @return void
     */
    public function run()
    {
        $plazos = [
            [
                'id'                => 1,
                'clave'             => 1,
                'duracion'          => '6',
                'plazo'             => 'Meses',
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ],
            [
                'id'                => 2,
                'clave'             => 2,
                'duracion'          => '12',
                'plazo'             => 'Meses',
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ],
            [
                'id'                => 3,
                'clave'             => 3,
                'duracion'          => '18',
                'plazo'             => 'Meses',
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ],
            [
                'id'                => 4,
                'clave'             => 4,
                'duracion'          => '6',
                'plazo'             => 'Quincenas',
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ],
            [
                'id'                => 5,
                'clave'             => 5,
                'duracion'          => '12',
                'plazo'             => 'Quincenas',
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ],
            [
                'id'                => 6,
                'clave'             => 6,
                'duracion'          => '24',
                'plazo'             => 'Quincenas',
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ],
            [
                'id'                => 7,
                'clave'             => 7,
                'duracion'          => '36',
                'plazo'             => 'Quincenas',
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ],
        ];

        DB::table('plazos')->insert($plazos);
    }
}
