<?php

use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Ejecuta los seeds de la base de datos
     *
     * @return void
     */
    public function run()
    {
        $permission_role = [
            [
                'permission_id'     => '1',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '4',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '5',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '6',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '7',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '8',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '9',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '10',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '11',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '12',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '13',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '14',
                'role_id'           => '1',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '2',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '3',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '4',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '4',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '4',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '5',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '5',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '6',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '6',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '6',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '7',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '7',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '7',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '8',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '8',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '8',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '9',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '9',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '9',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '10',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '10',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '10',
            ],
            [
                'permission_id'     => '5',
                'role_id'           => '10',
            ],
            [
                'permission_id'     => '8',
                'role_id'           => '10',
            ],
            [
                'permission_id'     => '10',
                'role_id'           => '10',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '11',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '11',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '11',
            ],
            [
                'permission_id'     => '5',
                'role_id'           => '11',
            ],
            [
                'permission_id'     => '8',
                'role_id'           => '11',
            ],
            [
                'permission_id'     => '10',
                'role_id'           => '11',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '12',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '12',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '13',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '13',
            ],
            [
                'permission_id'     => '4',
                'role_id'           => '13',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '14',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '15',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '15',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '15',
            ],
            [
                'permission_id'     => '4',
                'role_id'           => '15',
            ],
            [
                'permission_id'     => '8',
                'role_id'           => '15',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '16',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '16',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '16',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '17',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '17',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '17',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '18',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '18',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '18',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '19',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '19',
            ],
            [
                'permission_id'     => '3',
                'role_id'           => '19',
            ],
            [
                'permission_id'     => '9',
                'role_id'           => '19',
            ],
            [
                'permission_id'     => '1',
                'role_id'           => '20',
            ],
            [
                'permission_id'     => '2',
                'role_id'           => '20',
            ],
        ];


        DB::table('permission_role')->insert($permission_role);

    }
}
