<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Ejecuta los seeds de la base de datos
     *
     * @return void
     */
    public function run()
    {
        $users = [];

        $user = User::create([
            'name'          => 'Administrador Panel',
            'email'         => 'administrador@prestanomico.com',
            'area'          => '',
            'puesto'        => 'Administrador Panel',
            'password'      => bcrypt('Adm1n2018'),
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s'),
        ]);
        $users[] = $user;

        $user = User::create([
            'name'          => 'Prestanomico',
            'email'         => 'pruebas@prestanomico.com',
            'area'          => '',
            'puesto'        => 'Pruebas',
            'password'      => bcrypt('Pr357@n0m!c02018'),
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s'),
        ]);
        $users[] = $user;

        $users[0]->roles()->sync([1]);

    }
}
