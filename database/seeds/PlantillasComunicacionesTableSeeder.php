<?php

use Illuminate\Database\Seeder;

class PlantillasComunicacionesTableSeeder extends Seeder
{
    /**
     * Ejecuta los seeds de la base de datos
     *
     * @return void
     */
    public function run()
    {
        $plantillas = [
            [
                'plantilla_id' => 1,
                'modal_encabezado' => '¡Gracias por tu solicitud!',
                'modal_img' => '<img src="/images/brand/progresemos_logo.png" alt="icono_financiera" class="logo">',
                'modal_cuerpo' => '<p>¡Lo sentimos!</p> <p>Luego de un cuidadoso análisis de tu solicitud, lamentamos informarte que no nos será posible otorgar el préstamo que pediste.</p> <p>Esto puede deberse a tu historial crediticio (p.ej., atrasos recientes o frecuentes), o a que tu situación no parece propicia para una deuda adicional (p.ej., un alto nivel de endeudamiento actual).</p>',
                'sms' => null,
                'email_asunto' => null,
                'email_cuerpo' => null
            ],
            [
                'plantilla_id' => 2,
                'modal_encabezado' => 'Invitación a volver en unos meses',
                'modal_img' => '<img src="/images/brand/progresemos_logo.png" alt="icono_financiera" class="logo">',
                'modal_cuerpo' => '<p><b>¡Gracias por tu solicitud!</b></p> <p>Vemos que has tomado un monto importante de deuda reciente.&nbsp; Te recomendamos &nbsp;esperar un poco, terminar de pagar bien la deuda recientemente adquirida, y posteriormente buscar un pr&eacute;stamo con mejores condiciones (menor tasa, mayor plazo) para los proyectos que requieras.</p> <p>Basado en tu historial de cumplimiento, consideramos que eres un buen candidato para pedir un pr&eacute;stamo - una vez que tus finanzas se hayan adaptado al nivel de obligaciones que implica la deuda que has tomado.&nbsp; Te invitamos a volver a contactarnos en unos meses, y si nos das la oportunidad poder servirte en un futuro.</p>',
                'sms' => null,
                'email_asunto' => null,
                'email_cuerpo' => null
            ],
            [
                'plantilla_id' => 3,
                'modal_encabezado' => 'Y para completar tu trámite...',
                'modal_img' => '<img src="/images/brand/progresemos_logo.png" alt="icono_financiera" class="logo">',
                'modal_cuerpo' => '<p>¡Ya estás muy cerca! Continúa con tu solicitud</p> <p>En breve recibirás un email explicando cómo completar tu solicitud. Es muy fácil y sólo lleva diez minutos.</p> <p>Ten a la mano los siguientes documentos:</p> <ul style="list-style: decimal; text-align:  justify;""> <li>Identificación oficial vigente</li> <li>Comprobante de domicilio con antigüedad no mayor a 3 meses</li> <li>Comprobantes de ingresos</li> <li>Estado de cuenta donde depositaremos tu préstamo.</li> </ul> <p><strong>¡Gracias por elegirnos!</strong></p> <p>Te compartimos nuestro Whatsapp de contacto: (55) 2559 2413</p>',
                'sms' => 'Progresemos: Ya estás más cerca de obtener tu préstamo. Para continuar, contáctanos dando click en el siguiente enlace: https://bit.ly/325mCE5',
                'email_asunto' => 'Tu crédito en Progresemos está cada vez más cerca, continúa con tu solicitud.',
                'email_cuerpo' => 'Email_A_ProcesoNormal'
            ],
            [
                'plantilla_id' => 89, // Proceso Carga de Documentos
                'modal_encabezado' => 'Y para completar tu trámite...',
                'modal_img' => '<img src="/images/brand/progresemos_logo.png" alt="icono_financiera" class="logo">',
                'modal_cuerpo' => '<p>¡Ya estás muy cerca! Continúa con tu solicitud</p> <p>En breve recibirás un email explicando cómo completar tu solicitud. Es muy fácil y sólo lleva diez minutos.</p> <p>Ten a la mano los siguientes documentos:</p> <ul style="list-style: decimal; text-align:  justify;""> <li>Comprobante de domicilio con antigüedad no mayor a 3 meses</li> <li>Comprobantes de ingresos</li> <li>Estado de cuenta donde depositaremos tu préstamo.</li> </ul> <p><strong>¡Gracias por elegirnos!</strong></p> <p>Te compartimos nuestro Whatsapp de contacto: (55) 2559 2413</p>',
                'sms' => 'Progresemos: Ya estás más cerca de obtener tu préstamo. Para continuar, contáctanos dando click en el siguiente enlace: https://bit.ly/325mCE5',
                'email_asunto' => 'Tu crédito en Progresemos está cada vez más cerca, continúa con tu solicitud.',
                'email_cuerpo' => 'Email_A_CargaDocumentos'
            ],
            [
                'plantilla_id' => 99, // Proceso Simplificado
                'modal_encabezado' => 'Y para completar tu trámite...',
                'modal_img' => '<img src="/images/brand/progresemos_logo.png" alt="icono_financiera" class="logo">',
                'modal_cuerpo' => '<p>¡Ya estás muy cerca! Continúa con tu solicitud</p> <p>En breve recibirás un email explicando cómo completar tu solicitud. Es muy fácil y sólo lleva diez minutos.</p> <p>Ten a la mano los siguientes documentos:</p> <ul style="list-style: decimal; text-align:  justify;""> <li>Comprobante de domicilio con antigüedad no mayor a 3 meses</li> <li>Comprobantes de ingresos</li> <li>Estado de cuenta donde depositaremos tu préstamo.</li> </ul> <p><strong>¡Gracias por elegirnos!</strong></p> <p>Te compartimos nuestro Whatsapp de contacto: (55) 2559 2413</p>',
                'sms' => 'Progresemos: Ya estás más cerca de obtener tu préstamo. Para continuar, contáctanos dando click en el siguiente enlace: https://bit.ly/325mCE5',
                'email_asunto' => 'Tu crédito en Progresemos está cada vez más cerca, continúa con tu solicitud.',
                'email_cuerpo' => 'Email_A_ProcesoSimplificado'
            ],

        ];

        DB::table('plantillas_comunicaciones')->insert($plantillas);


    }
}
