<?php

return [
    'sesion_expirada' => 'Sesión expirada por inactividad. Inicia sesión de nuevo para continuar con tu solicitud.',
];
