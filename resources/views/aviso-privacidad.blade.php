@extends('layouts.app')
@section('content')
<div class="container-fluid" style="background-color: #FFD359;">
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="padding:6px">
                <div class="vticker text-center center-block" style="position: relative; height: 62px; overflow: hidden; display: table;">
                    <p style="font-weight: bold; font-size: 1.1em; display: table-cell; vertical-align: middle;">Si tienes dudas respecto a tu crédito LANAVE comunícate al: <a href="tel:5552695201" style="text-decoration: none; color: black;">(55) 5269 5201</a></p>
                </div>
            </div>
        </div>
    </div>
</div><br>
<section class="page-view">
    <div class="page-view-image container">
        <img src="https://financieramontedepiedad.com.mx/wp-content/uploads/2018/08/aviso_privacidad.png" alt="Aviso de Privacidad">
        <h1 class="page-view-title">Aviso de Privacidad</h1>
    </div>

    <div class="min-container container">
        <p><strong>Cláusula I.- Identidad y domicilio del responsable.</strong></p>
        <p><strong>Financiera Monte de Piedad, S.A. de C.V., S.F.P.,</strong> en los sucesivo <strong>FMP</strong>, con domicilio en Boulevard Manuel Ávila Camacho Número 32, Pisos del 17 al 20, Colonia Lomas de Chapultepec III Sección, C.P 11000, Miguel
            Hidalgo, Ciudad de México, es responsable de recabar los datos personales y sensibles del titular, del manejo de los mismos y de su protección.</p>
        <p><strong>Cláusula II.- Datos personales para realizar el tratamiento.</strong></p>
        <p>Para llevar a cabo las finalidades descritas en el presente aviso de privacidad, utilizaremos datos personales de: identificación, contacto y ubicación, referencias laborales y personales, biométricos, patrimoniales, financieros, de salud,
            así como, imágenes y sonidos para la seguridad en nuestras instalaciones.</p>
        <p><strong>Cláusula III.- Finalidades del tratamiento.</strong></p>
        <p>Los datos personales que le proporcione a <strong>FMP</strong>, serán utilizados para la realización que pudieran dar origen a relaciones jurídicas derivadas de operaciones de créditos: conformar el historial crediticio, fines administrativos,
            estadísticos, de análisis crediticio, contable, de recuperación de cartera, para ser trasferidos a las sociedades de información crediticias a las entidades regulatorias y calificadoras gubernamentales y privadas, o de las que se deriven
            o sean accesorias de esta; así como, para aquellas actividades secundarias, de fines mercadotécnicos, publicitarios, de prospección comercial de nuevos productos y servicios, seguridad en nuestras instalaciones y para fomentar la actualización
            de datos personales que nos ayude a mejorar los servicios proporcionados tanto propios como de las personas que ejercen el control accionario.</p>
        <p><strong>Cláusula IV.- Negativa al tratamiento de datos personales.</strong></p>
        <p>Si el titular desea que sus datos personales no sean tratados para los fines secundarios antes mencionados, puede presentar desde este momento un escrito en las oficinas de <strong>FMP</strong>, manifestando lo anterior. Solicite el formato correspondiente
            en nuestras oficinas o al correo electrónico <strong>une@financieramontedepiedad.com.mx</strong></p>
        <p>Los medios de contacto que utilizaremos para las finalidades descritas en el presente aviso son: presencial en sucursal, domicilio o trabajo, telefónico, correo electrónico, SMS, correo tradicional (red postal), redes sociales o cualquier otro
            que usted nos indique.<br /> El titular tiene un plazo de cinco días hábiles para que, de ser el caso, manifieste su negativa para el tratamiento de sus datos personales con respecto a las finalidades que no son necesarias, ni dieron origen
            a la relación jurídica.</p>
        <p><strong>Cláusula V. Datos personales sensibles</strong></p>
        <p>Además de los datos personales mencionados anteriormente, para las finalidades informadas en el presente aviso de privacidad utilizaremos los siguientes datos personales considerados como sensibles, que requieren de especial protección: Datos
            de salud.</p>
        <p><strong>Cláusula Vl.- Aceptación u oposición de transferencias o negativa al tratamiento de datos personales.</strong><br /> Le informamos que sus datos personales podrán ser compartidos con las siguientes personas, empresas, organizaciones
            y autoridades distintas a nosotros, para los siguientes fines:</p>
        <div class="tableSpace">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col" width="50%">Destinatario de los datos personales</th>
                            <th scope="col">Finalidad</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Autoridades fiscales mexicanas</td>
                            <td>Cumplimiento de disposiciones oficiales</td>
                        </tr>
                        <tr>
                            <td>Asesores externos</td>
                            <td>Cumplimiento regulatorio y de control interno de la operación y del negocio</td>
                        </tr>
                        <tr>
                            <td>Aseguradoras</td>
                            <td>Seguimiento a pólizas de seguros</td>
                        </tr>
                        <tr>
                            <td>Servicios informáticos externos</td>
                            <td>Soporte técnico</td>
                        </tr>
                        <tr>
                            <td>Centros de llamadas</td>
                            <td>Cumplimiento a las notificaciones o recordatorios estipulados en el contrato y prospección comercial</td>
                        </tr>
                        <tr>
                            <td>Servicios de mercadotecnia externos</td>
                            <td>Desarrollo de estrategias de mercado y de prospección comercial*</td>
                        </tr>
                        <tr>
                            <td>Promoción y publicidad externa</td>
                            <td>Actividades de promoción de marca, informar nuevos productos y servicios y para efectos de calidad en el servicio*<br /> Promover y otorgar productos y/o servicios de crédito*</td>
                        </tr>
                        <tr>
                            <td>Sociedades de información crediticia</td>
                            <td>Obtención de historial crediticio</td>
                        </tr>
                        <tr>
                            <td>Personas que ejercen el control accionario</td>
                            <td>Actividades de promoción de marca, informar nuevos productos y servicios y para efectos de calidad en el servicio*<br /> Promover y otorgar productos y/o servicios de crédito*</td>
                        </tr>
                        <tr>
                            <td>Administración de cartera de crédito externa</td>
                            <td>Administración, gestión y cobranza de cartera de crédito</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <p>Nos comprometemos a no transferir su información personal a terceros sin su consentimiento, salvo las excepciones previstas en el art. 37 de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares.</p>
        </div>
        <p><strong>Cláusula VII.- Aceptación u oposición de transferencias o negativa al tratamiento de datos personales.</strong></p>
        <p>Le informamos que para las trasferencias indicadas con un (*) en la Cláusula VI, requerimos obtener su consentimiento. Si usted no manifiesta su negativa para dichas transferencias, entenderemos que nos lo ha otorgado.</p>
        <p>El titular puede solicitar en nuestras oficinas o por medio del correo electrónico <strong>une@financieramontedepiedad.com.mx</strong> el formato “Oposición de trasferencia o negativa de tratamiento o trasferencias de datos personales”, y entregarlo
            en nuestras oficinas o en el lugar donde se recaben sus datos.<br /> Se entenderá que ha otorgado su consentimiento para el tratamiento de sus datos personales descritos en el presente aviso, para ello con el registro de su huella dactilar
            física o electrónica, a través de su firma autógrafa en la solicitud de crédito, solicitud de apertura de cuenta o contrato de crédito, a través de su aceptación en las casillas electrónicas en nuestra página de internet o sitios
            electrónicos, firma autógrafa en las diferentes modalidades del aviso de privacidad , cuando no exista negativa expresa para el tratamiento de sus datos, consentimiento verbal de manera presencial o mediante el uso de tecnología, firma
            electrónica, o cualquier mecanismo de autenticación que al efecto se establezca.</p>
        <p><strong>Cláusula VIII.- Medios y procedimiento para ejercer los derechos ARCO y para revocar el consentimiento del tratamiento de datos personales.</strong></p>
        <p>El titular tiene el derecho de acceder, rectificar, cancelar sus datos personales, oponerse o en su caso, revocar el consentimiento al tratamiento de dichos datos que para tal fin haya otorgado previamente a <strong>FMP</strong>.</p>
        <p>Para la salvaguarda de sus derechos ARCO y para revocar el consentimiento al tratamiento de datos personales, el titular podrá utilizar los procedimientos implementados a su favor, publicados en <strong>www.financieramontedepiedad.com.mx</strong>            de igual manera puede solicitarlos en nuestras oficinas, en el lugar donde se recabaron sus datos o al correo electrónico <strong>une@financieramontedepiedad.com.mx</strong></p>
        <p>Es importante que tenga en cuenta que para revocar el consentimiento del tratamiento de datos personales, no en todos los casos podremos atender su solicitud o concluir el uso de forma inmediata, ya que es posible que por alguna obligación legal
            requiramos seguir tratando sus datos personales. Asimismo, usted deberá considerar que para ciertos fines, la revocación de su consentimiento implicará que no le podamos seguir prestando el servicio que nos solicitó, o la conclusión de
            su relación con nosotros.</p>
        <p>Para dar cumplimiento a la obligación de acceso <strong>FMP</strong>, pondrá a disposición los datos personales mediante copia simple de sus datos, el titular lo tendrá que solicitar por escrito y le serán entregados de manera gratuita, dentro
            del plazo de 20 días naturales, a partir de la fecha de la solicitud.<br /> La respuesta a la solicitud de revocación del consentimiento para el tratamiento de datos personales y demás derechos ARCO, será de 20 días hábiles a partir
            de la fecha de solicitud.</p>
        <p><strong>Cláusula IX.- Opciones y medios para limitar el uso o divulgación de sus datos personales.</strong></p>
        <p>Con objeto de que usted pueda limitar el uso y divulgación de su información personal, le ofrecemos los siguientes medios:<br /> Su inscripción en el Registro Público de Usuarios, que está a cargo de la Comisión Nacional para la Protección
            y Defensa de los Usuarios de Servicios Financieros (CONDUSEF), con la finalidad de que sus datos personales no sean utilizados para recibir publicidad o promociones de las Instituciones financieras . Para mayor información sobre este registro,
            usted puede consultar el portal de Internet de la CONDUSEF, o bien ponerse en contacto directo con ésta. Su registro en el listado de exclusión del <strong>FMP</strong>, a fin de que sus datos personales no sean tratados para fines mercadotécnicos,
            publicitarios o de prospección comercial por nuestra parte. Para mayor información, enviar un correo electrónico a la siguiente dirección <strong>une@financieramontedepiedad.com.mx</strong></p>
        <p><strong>Cláusula X.- Uso de tecnología en Internet.</strong></p>
        <p>Le informamos que en nuestro sitio web utilizamos cookies, rastreadores GIF, etiquetas de píxel y Google Analytics a través de las cuales es posible monitorear su comportamiento como usuario de Internet, así como brindarle un mejor servicio
            y experiencia de usuario al navegar en nuestra página. Los datos personales que obtenemos de estas tecnologías de rastreo son los siguientes: Dirección IP o http cookie, el sistema operativo del usuario, URL, horario y tiempo de navegación,
            Hit, página, archivo, visita, host y navegador. Adicional a lo anterior, usted nos puede proporcionar de manera directa en nuestro sitio web, datos de identificación, contacto y ubicación, así como fechas de cumpleaños, mismos que utilizaremos
            para: Fines de identificación y verificación de datos, informativos, promociones y publicidad, atención y seguimiento de quejas, estadísticos y las demás finalidades descritas en el presente aviso.<br /> Asimismo, le informamos que los
            datos recabados por <strong>FMP</strong> mediante el uso de las tecnologías antes mencionadas (cookies), no se transfieren a un tercero.<br /> Para mayor información sobre el uso de estas tecnologías, así como la manera de deshabilitarlas,
            puede consultar nuestra sitio web <strong>www.financieramontedepiedad.com.mx</strong> en nuestra sección Aviso de Privacidad, apartado “Mecanismos para deshabilitar tecnologías de rastreo”.</p>
        <p><strong>Cláusula XI.- Videograbación en zonas de vigilancia.</strong></p>
        <p>uestras instalaciones cuentan con zonas de video vigilancia, las imágenes y sonidos captados por las cámaras serán utilizados para su seguridad y de las personas que nos visitan, mismas que serán almacenadas y resguardadas bajo los términos
            de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares.</p>
        <p><strong>Cláusula XII.- Cambios al Aviso de Privacidad.</strong></p>
        <p>Cualquier modificación al presente aviso le será notificada a través de nuestro sitio web <strong>www.financieramontedepiedad.com.mx</strong> y de forma impresa en nuestras oficinas, por tal motivo, recomendamos consultar regularmente este
            Aviso de Privacidad ya que puede sufrir cambios en cualquier momento.<br /> Estas medidas operan en el marco de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares.</p>
        <p>Atentamente<br /> Financiera Monte de Piedad, S.A. de C.V., S.F.P.</p>
    </div>
</section>
@endsection
