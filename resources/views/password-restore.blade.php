@extends('layouts.app')
@section('head')
      <title>Prestanómico | Checa si calificas</title>
      <meta name="description" content=" Solicita tu préstamo o refinancia tu deuda con menos intereses. Checa si calificas.">
      <meta name="keywords" content="Prestamos personales, Préstamos personales, Creditos, Créditos, prestamos uber, préstamos uber, transferencia de deuda, refinanciamiento, checa si calificas, registro.">
      <!-- Facebook Open Graph meta tags -->
      <meta property="og:type" content="website"/>
      <meta property="og:title" content="Prestanómico | Checa si calificas"/>
      <meta property="og:image" content="https://prestanomico.com/images/thumb_prestanomico_calificas.jpg"/>
      <meta property="og:url" content="https://prestanomico.com/registro"/>
      <meta property="og:description" content="Solicita tu préstamo o refinancia tu deuda con menos intereses, aquí."/>
      <meta property="og:site_name" content="Prestanómico"/>
      <!-- Twitter Card meta tags -->
      <meta name="twitter:card" content="summary_large_image">
      <meta name="twitter:title" content="Prestanómico | Checa si calificas"/>
      <meta name="twitter:image" content="https://prestanomico.com/images/thumb_prestanomico_calificas.jpg"/>
      <meta name="twitter:site" content="https://prestanomico.com/registro"/>
      <meta name="twitter:description" content="Solicita tu préstamo o refinancia tu deuda con menos intereses, aquí."/>
      <meta name="twitter:creator" content="@Prestanómico"/>
      <link rel="stylesheet" href="/back/css/AdminLTE.min.css">
      <link rel="stylesheet" href="/back/css/_all-skins.min.css">
@endsection
@section('content')
<style>
.my-input {
    width: 100%;
}
.my-input input {
    width: 100%;
    top: 18px;
    left: 3px;
    z-index: 1;
    font-size: 14px;
}
.linea-dato {
    height: 20px;
    border-top: none;
    z-index: 0;
}
.inner {
    padding-left: 0px !important;
}
.box {
    box-shadow: 0 1px 1px rgba(0,0,0,0.5);
}
h4 {
    font-family: 'Montserrat-Regular';
}
.box.box-danger {
    border-top-color: #F7962C;
}
.form-registro__buttons {
    margin-top: 1em;
    margin-bottom: 1em;
}
.help-block {
    color: #dd4b39;
    font-size: 14px;
}
.password {
    text-transform: none !important;
}
.resend_email {
    color: #3c8dbc !important;
}
</style>
<section class="container registro col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10 offset-1 offset-sm-2 offset-md-2 offset-lg-3 offset-xl-3">
    <div class="header-form" style="min-height: 67px;">
    </div>
    <div class="registro__first-block-footer" id="password-restore" style="display:block">
        <div class="inner">
            <div class="box box-danger col-11 col-sm-11 col-md-10 col-lg-8 col-xl-8">
                <div class="box-header with-border">
                  <h4>¿Olvidaste tu contraseña?</h4>
                  <p>No te preocupes, reestablecer tu contraseña es muy fácil.<br>Solo ingresa el correo electrónico con el que estás registrado en <b>Prestanómico</b>.</p>
                </div>
                <div class="box-body">
                    <form id="form-password-restore">
                        {!! csrf_field() !!}
                        <div class="my-input col-11 col-sm-11 col-md-10 col-lg-7 col-xl-7" style="margin-bottom: 15px;">
                            <input type="text" placeholder="E-MAIL" id="correo" name="correo" class="required"/>
                            <div class="linea-dato"></div>
                        </div>
                        <div class="row col-11 col-sm-11 col-md-10 col-lg-7 col-xl-7">
                            <span id="error_correo" class="help"></span>
                            <span id="error_ldap_paso1" class="help"></span>
                        </div>
                        <div class="form-registro__buttons">
                            <button type="button" class="green-white-btn" onclick="emailPasswordRestore()" style="width: 200px">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="registro__first-block-footer" id="temporary-password" style="display:none">
        <div class="inner">
            <div class="box box-danger col-11 col-sm-11 col-md-10 col-lg-8 col-xl-8">
                <div class="box-header with-border">
                  <h4>Reestablece tu contraseña</h4>
                  <p>Te hemos enviado un e-mail a <b id="correo_enviado"></b> con una contraseña temporal.<br>Ingresa esa contraseña y da click en verificar para establecer una nueva contraseña.</p>
                </div>
                <div class="box-body">
                    <form id="form-temporary-password">
                		{!! csrf_field() !!}
                		<div class="my-input col-11 col-sm-11 col-md-10 col-lg-7 col-xl-7">
                            <input type="hidden" id="correo_pt" name="correo_pt">
                		    <input type="password" placeholder="CONTRASEÑA TEMPORAL" id="password_temporal" name="password_temporal" minlength="8" maxlength="8" name="password_temporal" class="required password"/>
                		    <div class="linea-dato"></div>
                		</div>
                        <div class="row col-11 col-sm-11 col-md-10 col-lg-7 col-xl-7">
                            <input type="checkbox" onclick="showPassword()">Mostrar
                        </div>
                        <div class="row col-11 col-sm-11 col-md-10 col-lg-7 col-xl-7">
                            <span id="error_password_temporal" class="help-block"></span>
                            <span id="error_ldap_paso2" class="help-block"></span>
                        </div>
                        <div class="form-registro__buttons">
                    		<button type="button" class="green-white-btn" onclick="validateTemporaryPassword()" style="width: 200px">Verificar</button>
                        </div>
                	</form>
                </div>
                <div class="box-footer">
                    <p>¿No has recibido el e-mail?.<br>Por favor verifica tu carpeta de spam, si ya ha pasado tiempo da click <a href="#" onclick="emailPasswordRestore()" class="resend_email">aqui</a> para reenviar el e-mail.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="registro__first-block-footer" id="password-update" style="display:none">
        <div class="inner">
            <div class="box box-danger col-11 col-sm-11 col-md-10 col-lg-8 col-xl-8">
                <div class="box-header with-border">
                  <h4>Actualizar contraseña</h4>
                  <p>Introduce tu nueva contraseña.</p>
                </div>
                <div class="box-body">
                    <form id="form-password-update">
                		{!! csrf_field() !!}
                        <input type="hidden" id="idUser" name="idUser">
                        <input type="hidden" id="idUserT24" name="idUserT24">
                        <input type="hidden" id="fullName" name="fullName">
                        <input type="hidden" id="lastName" name="lastName">
                        <input type="hidden" id="name" name="name">
                        <input type="hidden" id="phoneNumber" name="phoneNumber">
                        <input type="hidden" id="token" name="token">
                        <input type="hidden" id="timeWhenGenerated" name="timeWhenGenerated">
                		<div class="my-input col-11 col-sm-11 col-md-10 col-lg-7 col-xl-7" style="margin-bottom: 15px;">
                		    <input type="password" placeholder="CREA TU NUEVA CONTRASEÑA" id="reg_password" maxlength="50" name="new_password" class="required password" onfocus="helperPassword('On')"/>
                		    <div class="linea-dato"></div>
                		</div>
                        <div class="row col-11 col-sm-11 col-md-10 col-lg-7 col-xl-7">
                            <span id="error_new_password" class="help-block"></span>
                        </div>
                        <div class="col-11 col-sm-11 col-md-10 col-lg-7 col-xl-7 row" id="helper-password" style="font-size: 12px; margin-top: 3px; display: none">
        					<div class="col-md-12"><span id="ucase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra mayúscula</div>
        					<div class="col-md-12"><span id="lcase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra minúscula</div>
        					<div class="col-md-12"><span id="num" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Un número</div>
        					<div class="col-md-12"><span id="8char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Minímo 8 caracteres</div>
        				</div>
                        <div class="my-input col-11 col-sm-11 col-md-10 col-lg-7 col-xl-7" style="margin-bottom: 15px;">
                		    <input type="password" placeholder="CONFIRMA TU NUEVA CONTRASEÑA" id="confirm_password" maxlength="50" name="confirm_password" class="required password"/>
                		    <div class="linea-dato"></div>
                		</div>
                        <div class="row col-11 col-sm-11 col-md-10 col-lg-7 col-xl-7">
                            <span id="error_confirm_password" class="help-block"></span>
                            <span id="error_ldap_paso3" class="help-block"></span>
                        </div>
                        <div class="form-registro__buttons">
                            <button type="button" class="green-white-btn" onclick="passwordUpdate()" style="width: 250px">Actualizar contraseña</button>
                        </div>
                	</form>
                </div>
            </div>
        </div>
    </div>
    <div class="registro__first-block-footer" id="password-update-ok" style="display:none">
        <div class="inner">
            <div class="box box-danger col-11 col-sm-11 col-md-10 col-lg-8 col-xl-8">
                <div class="box-header with-border">
                  <h4>Contraseña actualizada</h4>
                </div>
                <div class="box-body">
                    <p>La contraseña se ha actualizado con éxito.</p>
                    <p>Ya puedes ingresar con tu nueva contraseña.</p>
                    <p>Asegurate de memorizarla o anotarla en un lugar seguro.</p>
                    <div class="form-registro__buttons">
                        <button type="button" class="green-white-btn" style="width: 250px" onclick="iniciarSesion()">Iniciar sesión</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('extra_footer')
<script src='/js/password_restore.js?v=<?php echo microtime(); ?>' type="text/javascript" charset="utf-8"></script>
@endsection
