<div id="confianza">
    <div class="modal__body">
        <div class="modal__emoji" style="padding:0px; height:70px;">
            <img src="/images/brand/financiera_monte_de_piedad_logo.png" alt="icono_financiera" class="logo">
        </div>
        <div class="modal__text">
            <p>Financiera Monte de Piedad <b>No</b> pide depósitos ni cualquier tipo de pago anticipado para otorgar préstamos.</p>
            <p>Al comunicarte con nosotros y enviar documentos utiliza nuestros medios oficiales:</p>
            <ul>
                <li><b><i class="far fa-envelope"></i> documentaciondigital@financieramontepiedad.com.mx</b></li>
                <li><b><i class="fab fa-whatsapp"></i> (55) 2559 2413</b></li>
            </ul>
        </div>
    </div>
</div>
