<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <!-- NAME: 1:3 COLUMN - FULL WIDTH -->
    <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $nombre }}, ¡En Prestanómico recompensamos tu compromiso!</title>

    <style type="text/css">
        p {
            margin: 10px 0;
            padding: 0;
        }

        table {
            border-collapse: collapse;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            display: block;
            margin: 0;
            padding: 0;
        }

        img,
        a img {
            border: 0;
            height: auto;
            outline: none;
            text-decoration: none;
        }

        body,
        #bodyTable,
        #bodyCell {
            height: 100%;
            margin: 0;
            padding: 0;
            width: 100%;
        }

        .mcnPreviewText {
            display: none !important;
        }

        #outlook a {
            padding: 0;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        p,
        a,
        li,
        td,
        blockquote {
            mso-line-height-rule: exactly;
        }

        a[href^=tel],
        a[href^=sms] {
            color: inherit;
            cursor: default;
            text-decoration: none;
        }

        p,
        a,
        li,
        td,
        body,
        table,
        blockquote {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass td,
        .ExternalClass div,
        .ExternalClass span,
        .ExternalClass font {
            line-height: 100%;
        }

        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        .templateContainer {
            max-width: 600px !important;
        }

        a.mcnButton {
            display: block;
        }

        .mcnImage,
        .mcnRetinaImage {
            vertical-align: bottom;
        }

        .mcnTextContent {
            word-break: break-word;
        }

        .mcnTextContent img {
            height: auto !important;
        }

        .mcnDividerBlock {
            table-layout: fixed !important;
        }

        body,
        #bodyTable {
            background-color: #e8e8e8;
        }

        #bodyCell {
            border-top: 0;
        }

        h1 {
            color: #202020;
            font-family: Helvetica;
            font-size: 26px;
            font-style: normal;
            font-weight: bold;
            line-height: 125%;
            letter-spacing: normal;
            text-align: left;
        }

        h2 {
            color: #202020;
            font-family: Helvetica;
            font-size: 22px;
            font-style: normal;
            font-weight: bold;
            line-height: 125%;
            letter-spacing: normal;
            text-align: left;
        }

        h3 {
            color: #202020;
            font-family: Helvetica;
            font-size: 20px;
            font-style: normal;
            font-weight: bold;
            line-height: 125%;
            letter-spacing: normal;
            text-align: left;
        }

        h4 {
            color: #202020;
            font-family: Helvetica;
            font-size: 18px;
            font-style: normal;
            font-weight: bold;
            line-height: 125%;
            letter-spacing: normal;
            text-align: left;
        }

        #templatePreheader {
            background-color: #fafafa;
            background-image: none;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            border-top: 0;
            border-bottom: 0;
            padding-top: 0px;
            padding-bottom: 0px;
        }

        #templatePreheader .mcnTextContent,
        #templatePreheader .mcnTextContent p {
            color: #656565;
            font-family: Helvetica;
            font-size: 12px;
            line-height: 100%;
            text-align: left;
        }

        #templatePreheader .mcnTextContent a,
        #templatePreheader .mcnTextContent p a {
            color: #656565;
            font-weight: normal;
            text-decoration: underline;
        }

        #templateHeader {
            background-color: #e8e8e8;
            background-image: none;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            border-top: 0;
            border-bottom: 0;
            padding-top: 9px;
            padding-bottom: 0;
        }

        #templateHeader .mcnTextContent,
        #templateHeader .mcnTextContent p {
            color: #202020;
            font-family: Helvetica;
            font-size: 16px;
            line-height: 150%;
            text-align: left;
        }

        #templateHeader .mcnTextContent a,
        #templateHeader .mcnTextContent p a {
            color: #e8e8e8;
            font-weight: normal;
            text-decoration: underline;
        }

        #templateBody {
            background-color: #e8e8e8;
            background-image: none;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            border-top: 0;
            border-bottom: 0;
            padding-top: 0px;
            padding-bottom: 0px;
        }

        #templateBody .mcnTextContent,
        #templateBody .mcnTextContent p {
            color: #515151;
            font-family: Helvetica;
            font-size: 16px;
            line-height: 100%;
            text-align: left;
        }

        #templateBody .mcnTextContent a,
        #templateBody .mcnTextContent p a {
            color: #f89021;
            font-weight: normal;
            text-decoration: underline;
        }

        #templateColumns {
            background-color: #e8e8e8;
            background-image: none;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            border-top: 0;
            border-bottom: 0;
            padding-top: 0;
            padding-bottom: 9px;
        }

        #templateColumns .columnContainer .mcnTextContent,
        #templateColumns .columnContainer .mcnTextContent p {
            color: #202020;
            font-family: Helvetica;
            font-size: 16px;
            line-height: 150%;
            text-align: left;
        }

        #templateColumns .columnContainer .mcnTextContent a,
        #templateColumns .columnContainer .mcnTextContent p a {
            color: #007C89;
            font-weight: normal;
            text-decoration: underline;
        }

        #templateFooter {
            background-color: #e8e8e8;
            background-image: none;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            border-top: 0;
            border-bottom: 0;
            padding-top: 9px;
            padding-bottom: 9px;
        }

        #templateFooter .mcnTextContent,
        #templateFooter .mcnTextContent p color: #656565 font-family: Helvetica font-size: 12px line-height: 150% text-align: center;
        }

        #templateFooter .mcnTextContent a,
        #templateFooter .mcnTextContent p a color: #656565 font-weight: normal text-decoration: underline;
        }

        @media only screen and (min-width:768px) {
            .templateContainer {
                width: 600px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            body,
            table,
            td,
            p,
            a,
            li,
            blockquote {
                -webkit-text-size-adjust: none !important;
            }

        }

        @media only screen and (max-width: 480px) {
            body {
                width: 100% !important;
                min-width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            #bodyCell {
                padding-top: 10px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .columnWrapper {
                max-width: 100% !important;
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnRetinaImage {
                max-width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImage {
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnCartContainer,
            .mcnCaptionTopContent,
            .mcnRecContentContainer,
            .mcnCaptionBottomContent,
            .mcnTextContentContainer,
            .mcnBoxedTextContentContainer,
            .mcnImageGroupContentContainer,
            .mcnCaptionLeftTextContentContainer,
            .mcnCaptionRightTextContentContainer,
            .mcnCaptionLeftImageContentContainer,
            .mcnCaptionRightImageContentContainer,
            .mcnImageCardLeftTextContentContainer,
            .mcnImageCardRightTextContentContainer,
            .mcnImageCardLeftImageContentContainer,
            .mcnImageCardRightImageContentContainer {
                max-width: 100% !important;
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnBoxedTextContentContainer {
                min-width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImageGroupContent {
                padding: 9px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnCaptionLeftContentOuter .mcnTextContent,
            .mcnCaptionRightContentOuter .mcnTextContent {
                padding-top: 9px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImageCardTopImageContent,
            .mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,
            .mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
                padding-top: 18px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImageCardBottomImageContent {
                padding-bottom: 9px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImageGroupBlockInner {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImageGroupBlockOuter {
                padding-top: 9px !important;
                padding-bottom: 9px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnTextContent,
            .mcnBoxedTextContentColumn {
                padding-right: 18px !important;
                padding-left: 18px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImageCardLeftImageContent,
            .mcnImageCardRightImageContent {
                padding-right: 18px !important;
                padding-bottom: 0 !important;
                padding-left: 18px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcpreview-image-uploader {
                display: none !important;
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            h1 {
                font-size: 22px !important;
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {

            h2 {
                font-size: 20px !important;
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {

            h3 {
                font-size: 18px !important;
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {

            h4 {
                font-size: 16px !important;
                line-height: 150% !important;
            }

        }

        @media only screen and (max-width: 480px) {

            .mcnBoxedTextContentContainer .mcnTextContent,
            .mcnBoxedTextContentContainer .mcnTextContent p {
                font-size: 14px !important;
                line-height: 150% !important;
            }

        }

        @media only screen and (max-width: 480px) {

            #templatePreheader {
                display: block !important;
            }

        }

        @media only screen and (max-width: 480px) {

            #templatePreheader .mcnTextContent,
            #templatePreheader .mcnTextContent p {
                font-size: 14px !important;
                line-height: 150% !important;
            }

        }

        @media only screen and (max-width: 480px) {

            #templateHeader .mcnTextContent,
            #templateHeader .mcnTextContent p {
                font-size: 16px !important;
                line-height: 150% !important;
            }

        }

        @media only screen and (max-width: 480px) {

            #templateBody .mcnTextContent,
            #templateBody .mcnTextContent p {
                font-size: 16px !important;
                line-height: 150% !important;
            }

        }

        @media only screen and (max-width: 480px) {

            #templateColumns .columnContainer .mcnTextContent,
            #templateColumns .columnContainer .mcnTextContent p {
                font-size: 16px !important;
                line-height: 150% !important;
            }

        }

        @media only screen and (max-width: 480px) {

            #templateFooter .mcnTextContent,
            #templateFooter .mcnTextContent p {
                font-size: 14px !important;
                line-height: 150% !important;
            }

        }
    </style>
</head>

<body>
    <center>
        <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
            <tr>
                <td align="center" valign="top" id="bodyCell">
                    <!-- BEGIN TEMPLATE // -->
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="center" valign="top" id="templatePreheader">
                                <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                    <tr>
                                        <td valign="top" class="preheaderContainer">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">

                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" id="templateHeader">
                                <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                    <tr>
                                        <td valign="top" class="headerContainer"></td>
                                    </tr>
                                </table>
                                <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" id="templateBody">
                                <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                    <tr>
                                        <td valign="top" class="bodyContainer">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
                                                <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                                                <tbody class="mcnBoxedTextBlockOuter">
                                                    <tr>
                                                        <td valign="top" class="mcnBoxedTextBlockInner">

                                                            <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                                                                <tbody>
                                                                    <tr>

                                                                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                                                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td valign="top" class="mcnTextContent" style="padding: 18px;color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;">
                                                                                            <img data-file-id="838015" height="272" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/750d2462-cd67-4324-b6c1-7311512b9b92.jpg" style="border: 0px  ; width: 600px; height: 272px; margin: 0px;" width="600">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <!--[if gte mso 9]>
				</td>
				<![endif]-->

                                                            <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
                                                <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                                                <tbody class="mcnBoxedTextBlockOuter">
                                                    <tr>
                                                        <td valign="top" class="mcnBoxedTextBlockInner">

                                                            <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                                                                <tbody>
                                                                    <tr>

                                                                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                                                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #2790CE;border: 1px none;">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td valign="top" class="mcnTextContent" style="padding: 18px;color: #FFFFFF;font-family: Helvetica;font-size: 14px;font-weight: normal;line-height: 100%;text-align: center;">
                                                                                            <span style="font-size:24px"><span>¡Hola {{ $nombre }}!</span></span><br> &nbsp;
                                                                                            <div style="text-align: center;"><br>
                                                                                                <span style="font-size:19px"><strong>Porque nos gusta consentirte</strong></span></div>
                                                                                            <br>
                                                                                            <br>
                                                                                            <font face="arial, helvetica neue, helvetica, sans-serif" size="4">Prestanómico reconoce&nbsp;el excelente comportamiento de pagos que tuviste con nosotros, por este motivo queremos consentirte con la siguiente oferta.<br>
                                                                                                <br> Te ofrecemos un crédito <strong>Pre-aprobado</strong> con las siguientes características:</font><br> &nbsp;
                                                                                            <div style="text-align: left;"><br>
                                                                                                <br>
                                                                                                <span style="font-size:24px"><span style="color:#ccccc"><strong>Monto</strong>:</span>&nbsp;<span>{{ $monto }}</span><br>
                                                                                                <br>
                                                                                                <span style="color:#ccccc"><strong>Tasa</strong>:</span>&nbsp;<span>{{ $tasa }}</span><br>
                                                                                                <br>
                                                                                                <span style="color:#ccccc"><strong>Plazo</strong>:</span>&nbsp;<span>{{ $plazo }}</span><br>
                                                                                                <br>
                                                                                                <strong>Pago estimado</strong><span>:</span>&nbsp;<span>{{ $pago_estimado }}</span></span> &nbsp;</div>

                                                                                            <table class="egt">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <br>
                                                                                            <br>
                                                                                            <span style="font-size:18px">¡Da clic en aceptar y conoce la manera mas sencilla de obtener un préstamo como cliente estrella!</span><br>
                                                                                            <br>
                                                                                            <br>
                                                                                            <br>
                                                                                            <span style="font-size:13px"><a href="/enviaInvitacion/uuid={{$uuid}}" target="_blank"><img data-file-id="834667" height="44" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/7bf5c225-b57e-4517-98ae-97aad67099f8.png" style="border: 0px  ; width: 123px; height: 44px; margin: 0px;" width="123"></a>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<a href="https://es.surveymonkey.com/r/52DTQHJ" target="_blank"><img data-file-id="834663" height="44" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/0e4ba1f0-c350-4911-9b0f-ff58fa5b0f04.png" style="border: 0px  ; width: 123px; height: 44px; margin: 0px;" width="123"></a></span><br>                                                                                            &nbsp;
                                                                                            <div style="text-align: right;"><span style="font-size:14px">Oferta vigente hasta el {{ $vigencia }}</span></div>

                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <!--[if gte mso 9]>
				</td>
				<![endif]-->

                                                            <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
                                                <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                                                <tbody class="mcnBoxedTextBlockOuter">
                                                    <tr>
                                                        <td valign="top" class="mcnBoxedTextBlockInner">

                                                            <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                                                                <tbody>
                                                                    <tr>

                                                                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                                                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td valign="top" class="mcnTextContent" style="padding: 18px;color: #515151;font-family: Helvetica;font-size: 14px;font-weight: normal;">
                                                                                            <div style="text-align: center;"><img data-file-id="879211" height="53" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/d3393633-e0bf-4aff-a38a-ad7f783efae8.jpg" style="border: 0px  ; width: 600px; height: 53px; margin: 0px;"
                                                                                                    width="600"></div>
                                                                                            <br> 1. Ingresa a tu solicitud dando clic en el botón <strong>Aceptar</strong><br> 2. Elige la finalidad de tu crédito. Puede ser Préstamo Personal o Pago de Tarjeta
                                                                                            de Crédito.<br> 3. Revisa que tus datos personales estén correcto y autoriza una consulta en Buró.<br> 4. Acepta tu oferta y&nbsp;envía tu último comprobante de
                                                                                            ingresos.
                                                                                            <br> 5. Firma tu contrato y recibe tu dinero.&nbsp;<span style="text-align:center">&nbsp; &nbsp;</span><br>
                                                                                            <img data-file-id="659955" height="3" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/fb57b25d-ba47-485c-b318-209be2666ba8.png" width="600">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <!--[if gte mso 9]>
				</td>
				<![endif]-->

                                                            <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
                                                <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                                                <tbody class="mcnBoxedTextBlockOuter">
                                                    <tr>
                                                        <td valign="top" class="mcnBoxedTextBlockInner">

                                                            <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                                                                <tbody>
                                                                    <tr>

                                                                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                                                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td valign="top" class="mcnTextContent" style="padding: 18px;color: #515151;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;">
                                                                                            <div style="text-align: center;"><strong>¿Tienes dudas? Con gusto te apoyomos:</strong><br>
                                                                                                <img data-file-id="659975" height="20" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/8065cb83-b720-4733-8049-26dfd7369c90.png" width="20">&nbsp;(55)
                                                                                                7822-8820
                                                                                                <br>
                                                                                                <img data-file-id="659979" height="20" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/e1b09ac0-c76e-463c-9e85-27b7ccbe7d7c.png" width="20">&nbsp;(55)
                                                                                                5502-8500&nbsp;/ (55)&nbsp;8128-9583<br>
                                                                                                <a href="http://prestamos@prestanomico.com"><img data-file-id="659971" height="15" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/a4dbc336-b7a9-4888-b424-750d2d5edcac.png" width="20"></a>&nbsp;prestamos@prestanomico.com<br>
                                                                                                <br>
                                                                                                <img data-file-id="659955" height="3" src="https://gallery.mailchimp.com/b160d258cd8c88c82337fe17d/images/fb57b25d-ba47-485c-b318-209be2666ba8.png" width="600"></div>

                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <!--[if gte mso 9]>
				</td>
				<![endif]-->

                                                            <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
                                                <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                                                <tbody class="mcnBoxedTextBlockOuter">
                                                    <tr>
                                                        <td valign="top" class="mcnBoxedTextBlockInner">

                                                            <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                                                                <tbody>
                                                                    <tr>

                                                                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                                                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #515151;border: 1px none;">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td valign="top" class="mcnTextContent" style="padding: 18px;color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;">
                                                                                            <div style="text-align: left;">¿Aún con dudas?<br>
                                                                                                <br> Prestanómico en ningún momento te pedirá que realices algún pago anticipado por nuestro servicio.<br>
                                                                                                <br> Toda la comunicación se realizará a través de nuestro dominio de correo “@ prestanomico . com” Si te llega información por otra cuenta, favor de reportarlo.<br>
                                                                                                <br> Tu información está protegida por nuestros sistemas de seguridad. Para nuestra política de datos personales revisa nuestro <a href="https://prestanomico.com/aviso-de-privacidad"
                                                                                                    target="_blank"><span style="color:#FF8C00"><u>aviso de privacidad</u></span></a>.<br>
                                                                                                <br> Tu crédito es flexible - en cualquier momento puedes realizar adelantos de pagos o liquidar tu préstamo sin ninguna penalización.<br>
                                                                                                <br> El pago se realiza de manera automática con cargo a la cuenta donde se te depositó. &nbsp; ¡olvídate de ir al banco! Solo asegúrate que en las fechas
                                                                                                de pago (normalmente los días quince y último de cada mes) haya suficiente dinero en esa misma cuenta.</div>

                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <!--[if gte mso 9]>
				</td>
				<![endif]-->

                                                            <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" id="templateColumns">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                    <tr>
                                        <td valign="top">
                                            <!--[if (gte mso 9)|(IE)]>
                                                <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                                <tr>
                                                <td align="center" valign="top" width="200" style="width:200px;">
                                                <![endif]-->
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper">
                                                <tr>
                                                    <td valign="top" class="columnContainer"></td>
                                                </tr>
                                            </table>
                                            <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                <td align="center" valign="top" width="200" style="width:200px;">
                                                <![endif]-->
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper">
                                                <tr>
                                                    <td valign="top" class="columnContainer"></td>
                                                </tr>
                                            </table>
                                            <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                <td align="center" valign="top" width="200" style="width:200px;">
                                                <![endif]-->
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper">
                                                <tr>
                                                    <td valign="top" class="columnContainer"></td>
                                                </tr>
                                            </table>
                                            <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                </tr>
                                                </table>
                                                <![endif]-->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" id="templateFooter">
                                <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                    <tr>
                                        <td valign="top" class="footerContainer">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
                                                <tbody class="mcnFollowBlockOuter">
                                                    <tr>
                                                        <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" style="padding-left:9px;padding-right:9px;">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                                                                                            <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="center" valign="top">
                                                                                                            <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->

                                                                                                            <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                                                                                <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>

                                                                                                                                                        <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                                                                                            <a href="https://www.facebook.com/Prestanomico/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                                                                                        </td>


                                                                                                                                                        <td align="left" valign="middle" class="mcnFollowTextContent" style="padding-left:5px;">
                                                                                                                                                            <a href="https://www.facebook.com/Prestanomico/" target="" style="font-family: Helvetica;font-size: 12px;text-decoration: none;color: #656565;">Facebook</a>
                                                                                                                                                        </td>

                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>

                                                                                                            <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                                                                                            <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td valign="top" style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                                                                                <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>

                                                                                                                                                        <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                                                                                            <a href="https://prestanomico.com/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                                                                                        </td>


                                                                                                                                                        <td align="left" valign="middle" class="mcnFollowTextContent" style="padding-left:5px;">
                                                                                                                                                            <a href="https://prestanomico.com/" target="" style="font-family: Helvetica;font-size: 12px;text-decoration: none;color: #656565;">Prestanómico</a>
                                                                                                                                                        </td>

                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>

                                                                                                            <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                                                                                            <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
                                                <tbody class="mcnDividerBlockOuter">
                                                    <tr>
                                                        <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 10px 18px 25px;">
                                                            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EEEEEE;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <span></span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                            </td>
                        </tr>
                    </table>
                    <!-- // END TEMPLATE -->
                </td>
            </tr>
        </table>
    </center>
</body>

</html>
