@extends('layouts.webapp')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12" id="navigator">
				<ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
					<li role="presentation"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i class="icon-id-front-1"></i>
						<p>Paso 1</p>
					</a></li>
					<li role="presentation"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><i class="icon-id-back-1"></i>
						<p>Paso 2</p>
					</a></li>
					<li role="presentation" class="active"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i class="icon-selfie-camera" aria-hidden="true"></i>
						<p>Paso 3</p>
					</a></li>
				</ul>
			</div>
		</div>
		<div class="row">
          	<div class="col-12 col-lg-12">
				<div class="card">
	              <div class="card-header">
	                <div class="align-items-center">
	                  <div class="col" style="text-align: center">
	                    <h5 class="card-header-title">
							TÓMATE UNA SELFIE <i class="fas fa-info-circle" style="font-size: 15px; color: #3085D6" onclick="ayuda('tip2')"></i>
	                    </h5>
						<hr>
						<span class="span-card">
							<ul>
								<li>Sonríe y parpadea para tomarte la foto. Con el rostro descubierto, sin lentes ni gorra.</li>
							</ul>
						</span>
	                  </div>
	                </div>
	              </div>
				  <hr>
	              <div class="card-body" id="tomar-foto">
					  <div id="cargando" style="display:none">
					    <center>
					      <img src="/images/ajax-loader.gif" class="loading-gif">
					      <p> Cargando camara... </p>
					    </center>
    				  </div>
					  @if($desktop === true)
				  		<div class="videoDiv desktop" id="videoDiv">
				  	    	<video id="videoInput" autoplay="true" class="desktop"></video>
				  	    </div>
						@else
						<div class="videoDiv mobile" id="videoDiv">
							<video id="videoInput" autoplay="true" class="mobile"></video>
				  	    </div>
						@endif
			  		<div>
			  			<input id="ChangeCamera" type="button" value="Change Camera"  style="visibility: hidden; padding: 10px 20px" onclick="askForChangeCameraFunction();"  />
			  		</div>
	              </div>
				  <div class="card-body" id="resultado">
					  	<canvas id="resultImage"></canvas>
					  	<hr>
  						<div class="row align-items-center justify-content-between">
							<div class="col-xs-12 calculadoras-productos txt-center">
  								<button id="repetirFoto" type="button" class="general-button" onclick="repetirPasoSelfie()">
  									Repetir Foto
  								</button>
  								<button id="siguientePaso1" type="button" class="general-button" onclick="saveImage('identificacion_oficial', 'photo')">
  									Siguiente
  								</button>
  						  	</div>
  						</div>
						<hr>
				  	</div>
	            </div>
          	</div>
        </div>
	</div>
@endsection
<script>
	window.onload = function () {
		iniciarProcesoSelfie();
	}
</script>
