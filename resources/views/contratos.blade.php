@extends('layouts.app')
@section('content')
<div class="container-fluid" style="background-color: #FFD359;">
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="padding:6px">
                <div class="vticker text-center center-block" style="position: relative; height: 62px; overflow: hidden; display: table;">
                    <p style="font-weight: bold; font-size: 1.1em; display: table-cell; vertical-align: middle;">Si tienes dudas respecto a tu crédito LANAVE comunícate al: <a href="tel:5552695201" style="text-decoration: none; color: black;">(55) 5269 5201</a></p>
                </div>
            </div>
        </div>
    </div>
</div><br>
<section class="page-view">
    <div class="page-view-image container">
        <img src="https://financieramontedepiedad.com.mx/wp-content/uploads/2018/08/contratos.png" alt="Contratos">
        <h1 class="page-view-title">Contratos</h1>
    </div>

    <div class="min-container container">
        <div class="col-lg-6 col-md-6 col-md-offset-3 col-sm-12 col-xs-12 generalPadding txt-center"><img class="fixImageWidth imageFullContainer" src="https://financieramontedepiedad.com.mx//wp-content/uploads/2018/08/contratos-prestamo-personal.png" alt="UNE">&nbsp;&nbsp;</p>
            <p class="secondary-title"><b>Préstamo Personal</b></p>
            <p><a class="general-button" href="/pdf/contrato_prestamo_personal.pdf" target="_blank" rel="noopener noreferrer">Conócelo</a></p>
        </div>

    </div>
</section>
@endsection
