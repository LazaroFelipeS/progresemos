@extends('crm.app')
@section('content')
<link rel="stylesheet" href="/back/css/font-awesome.min.css">
<link rel="stylesheet" href="/back/css/AdminLTE.min.css">
<link rel="stylesheet" href="/back/css/_all-skins.min.css">
<link rel="stylesheet" href="/back/css/multi-select.css">
<link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<style>
.box.box-primary {
	border-top-color: #f79020;
}
label {
	text-transform: none;
}

.file-input {
    position: relative;
	overflow: hidden;
	margin: 0px;
    color: #333;
    background-color: #fff;
    border-color: #ccc;
}
.file-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.file-input-title {
    margin-left:2px;
}
.popover-content {
	background-color: #eaeaea;
}
.help-block {
    color: #dd4b39;
}

</style>

<div class="container" style="width:100%">
	<div id="dashboard-container" class="content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="box box-primary">
				<div class="box-header with-border" style="text-align:center;">
	            	<h3 class="box-title" style="font-weight: bold;">Plazas Cobertura {{ $producto->nombre_producto }}</h3>
	            </div>
				<div class="box-body">
					<form role="form" id="cobertura_producto">

						<div class="row">
							<input type="hidden" id="producto_id" name="producto_id" value="{{ $producto->id }}">
							<input type="hidden" id="campo_cobertura" name="campo_cobertura" value="{{ $producto->campo_cobertura }}">
							<div class="form-group col-lg-9 col-md-9 col-sm-9 col-xs-9">
								<label class="control-label">Archivo C.P.</label>

								<div class="input-group">
									<input type="text" class="form-control file-filename" disabled="disabled">
									<span class="input-group-btn">

										<button type="button" class="btn btn-default file-clear" style="display:none;">
											<span class="glyphicon glyphicon-remove"></span> Limpiar
										</button>

										<div class="btn btn-default file-input">
											<span class="glyphicon glyphicon-folder-open"></span>
											<span class="file-input-title">Buscar</span>
											<input type="file" id="csv" name="csv"/>
										</div>
									</span>
								</div>

								<span id="error_csv" class="help-block"></span>
							</div>

		  				</div>

						<div class="row">
							<div class="footer" style="text-align: center">
								<button type="button" class="btn btn-primary" onclick="coberturaProducto()">Cargar Archivo</button>
								<a href="{{ URL::route('productos') }}" class="btn btn-primary"> Regresar </a>
							</div>
						</div>

					</form>
	    		</div>
			</div>

		</div>
	</div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="/js/backoffice/multiselect.min.js"></script>
<script type="text/javascript" src="/js/backoffice/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="/js/backoffice/datetimepicker.min.js"></script>
<script type="text/javascript" src="/js/backoffice/datetimepicker.es.js"></script>
<script type="text/javascript" src="/js/backoffice/jquery.inputmask.bundle.js"></script>
<script type="text/javascript" src="/js/productos.js?v=<?php echo microtime(); ?>"></script>
<script>

</script>
@endsection
