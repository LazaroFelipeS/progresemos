@extends('crm.app')
@section('content')
<link href="/css/font-awesome.css" rel="stylesheet" >
<link href="/css/bootstrap-social.css" rel="stylesheet" >
<style>
    label {
        text-transform: none;
        font-weight: normal;
    }
</style>
<div id="login-panel" class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Login Captura Solicitud</h3>
    </div>
    <div class="panel-body">
        <form method="POST" action="/login/captura">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="email">Correo Electrónico</label>
                <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}" autocomplete="new-email">
            </div>
            <div class="form-group">
                <label for="password">Contraseña</label>
                <input type="password" id="password" name="password" class="form-control">
                <input type="hidden" id="producto" name="producto" class="form-control" value="{{ $producto }}">
            </div>
            <center><button class="btn btn-default" type="submit">Iniciar sesión</button></center>
            <hr>

        </form>
    </div>
</div>
@endsection
