@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                @php
                use App\CatalogoSepomex;
                $estados = CatalogoSepomex::selectRAW('distinct(estado), id_estado')
                    ->orderBy('estado')
                    ->pluck('estado', 'id_estado');
                $estados = (json_encode($estados))
            @endphp
            <datos_personales :estados='{{ $estados }}'></datos_personales>
            </div>
        </div>
    </div>
</div>
@endsection