<div id="loginModal" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-dialog-centered modal-login modal-sm ">
		<div class="modal-content">
			<div class="modal-headers">
				<h2 class="titulo-dinamico">Iniciar Sesión</h2>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<form id="formLogin">
					<div class="calculadoras-productos">
						<i class="fa fa-user"></i>
						<div class="my-input" style="margin-bottom:30px; width:110% !important">
							<input id="email_login" type="text" class="pre-registro-input" placeholder="Correo Electrónico" autocomplete="new-email" name="email" required="required">
							<small id="email_login-help" class="help"></small>
							<div class="linea-dato"></div>
						</div>
					</div>
					<div class="calculadoras-productos">
						<i class="fa fa-lock"></i>
						<div class="my-input" style="width:110% !important;">
							<input id="password_login" type="password" class="pre-registro-input" placeholder="Contraseña" autocomplete="new-password" name="password" required="required">
							<small id="password_login-help" class="help"></small>
							<div class="linea-dato"></div>
						</div>
					</div><br>
					<div class="form-group">
						<input id="buttonLogin" type="button" onclick="iniciarSesion()" class="green-white-btn" value="Iniciar Sesión" style="margin-top:30px;">
					</div>
				</form>
				<span id="loginError" class="error" style="border-bottom: none !important; font-size: 13px;"></span>
			</div>
			<div class="modal-footer">
				<a href="/password-restore" id="olvidePassword"><b>Olvide mi contraseña</b></a>
			</div>
		</div>
	</div>
</div> 
  <footer class="footer">
	  <div class="col-xs-12 col-md-12 col-12" style="margin-left:15px !important;">
		<div class="row justify-content-center">
				<h5>FINANCIAMIENTO PROGRESEMOS S.A. DE C.V. SOFOM, E.N.R</h5>
			</div>
			<div class="row row-footer justify-content-center">
				<p><i class="fa fa-phone" style="color:#53a13c;"></i>+(52) 55 75 20 09 <i class="fa fa-map-marker" style="color:#53a13c;"></i> Picacho Ajusco 130, Despacho 203, Col. Jardínes en la Montaña, CDMX, C.P. 14210
					Ciudad de México</p>
			</div>
			<div class="row row-footer justify-content-center">
				<script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=fJzgGrLcXSBlMzn2cT5aU0SRFMGfCr7Tpo8P8xkf7gqUdaSDbIjWU7eUFfZ0"></script>
				<img style="cursor:pointer;cursor:hand;height: 25px;margin-bottom: 20px;" src="https://seal.godaddy.com/images/3/es/siteseal_gd_3_h_l_m.gif" onclick="verifySeal();" alt="SSL site seal - click to verify">
			</div>
			<div class="row row-footer justify-content-center">
				<a href="https://www.gob.mx/condusef" tabindex="-1">
					<img src="/images/icons/logo-condusef.png" alt="Condusef">
				</a>
				<a href="https://financieramontedepiedad.com.mx/quejas-y-aclaraciones/" tabindex="-1">
					<img src="/images/icons/logo-une.png" alt="Unidad especializada de atención a cliente">
				</a>
				<a href="https://www.gob.mx/cnbv" tabindex="-1">
					<img src="/images/icons/logo-cnbb.png" alt="Comisión Nacional Bancaria de Valores">
				</a>
				<a href="https://financieramontedepiedad.com.mx/buro-de-entidades-financieras" tabindex="-1">
					<img src="/images/icons/logo-buro.png" alt="Condusef">
				</a>
				<a href=" http://www.banxico.org.mx/CAT/" target="_blank" tabindex="-1">
					<img src="/images/icons/logo-banco-mexico.jpg" alt="Banco de México">
				</a>
			</div>
		</div>
	

	<script type="text/javascript" src='/js/librerias.js'></script>
	<script type="text/javascript" src='/js/status_oferta.js?v=<?php echo microtime(); ?>'></script>
	<script type="text/javascript" src='/js/combodate.js?v=<?php echo microtime(); ?>'></script>
	 <script type="text/javascript" src="/js/app.js?v=<?php echo microtime(); ?>"></script>
	 <script type="text/javascript" src="/js/login.js?v=<?php echo microtime(); ?>"></script>
	 <script type="text/javascript" src="/js/registro.js?v=<?php echo microtime(); ?>"></script>
	 <script src='/js/password_restore.js?v=<?php echo microtime(); ?>' type="text/javascript" charset="utf-8"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
	{{-- <script type="text/javascript" src="js/calculadora-prestamo.js"></script> --}}

</footer>