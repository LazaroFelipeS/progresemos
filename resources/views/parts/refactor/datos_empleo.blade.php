<div id="datos_empleo" style="display:none">
    <form id="formDatosEmpleo">
        <div class="col-xs-12">
            <h2 class="secondary-title txt-center">Solicitud de crédito</h2>
            <div class="calculadoras-productos"><small>Estás muy cerca de terminar, este es el último paso de tu solicitud.</small></div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="empresa" name="empresa" type="text" placeholder="Nombre de empresa/lugar de trabajo*" maxlength="100" class="required uppercase">
                    <small id="empresa-help" class="help"></small>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="antiguedad_empleo" name="antiguedad_empleo" aria-required="true" aria-invalid="false" required="required">
                                    <option value="" disabled="" selected="selected">Antigüedad en empleo*</option>
                                    <option value="0">Menos de 1 año</option>
        		                    <option value="1">1 año</option>
        		                    <option value="2">2 años</option>
        		                    <option value="3">3 años</option>
        		                    <option value="4">4 años</option>
        		                    <option value="5">5 años</option>
        		                    <option value="6">6 años</option>
        		                    <option value="7">7 años</option>
        		                    <option value="8">8 años</option>
        		                    <option value="9">9 años</option>
        		                    <option value="10">10 años</option>
        		                    <option value="11">Más de 10 años</option>
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                    <small id="antiguedad_empleo-help" class="help"></small>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 calculadora-personal fecha_nacimiento" style="margin-top: 0px !important;">
                    <input id="fecha_ingreso" name="fecha_ingreso" type="text" placeholder="Fecha de Ingreso" data-format="DD-MM-YYYY" data-template="YYYY-MMM-DD" class="required">
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="telefono_empleo" name="telefono_empleo" type="text" placeholder="Teléfono del empleo actual" maxlength="10" class="required">
                    <small id="telefono_empleo-help" class="help"></small>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="calle_empleo" name="calle_empleo" type="text" placeholder="Calle" maxlength="50" class="required uppercase">
                    <small id="calle_empleo-help" class="help"></small>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="no_exterior_empleo" name="no_exterior_empleo" type="text" placeholder="No. Ext." maxlength="5" class="required uppercase">
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="no_interior_empleo" name="no_interior_empleo" type="text" placeholder="No. Int." maxlength="5" class="uppercase">
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="codigo_postal_empleo" name="codigo_postal_empleo" type="text" placeholder="C.P." maxlength="5" onkeyup="getCP(this, true)" class="required">
                    <small id="codigo_postal_empleo-help" class="help"></small>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 calculadora-personal">
                    <div class="calculadoras-productos">
                        <div class="withDecoration min-select">
                            <span>
                                <select class="pre-registro-input required" id="select_colonia_empleo" name="select_colonia_empleo" aria-required="true" aria-invalid="false">
                                    <option value="" disabled="" selected="selected">Ingrese Código Postal</option>
                                </select>
                            </span>
                            <div class="decoration"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="delegacion_empleo" name="delegacion_empleo" type="text" placeholder="Delegación / Municipio" disabled="disabled" class="uppercase">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="ciudad_empleo" name="ciudad_empleo" type="text" placeholder="Ciudad" disabled="disabled" class="uppercase">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 calculadora-personal">
                    <input id="estado_empleo" name="estado_empleo" type="text" placeholder="Estado" disabled="disabled" class="uppercase">
                </div>
                <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
                    <label id="validacionesDatosEmpleo"></label>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 calculadora-personal mt-0">
                    <div class="text-right">
                        <a class="general-button" onclick="datos_empleo()"><span>Finalizar</span></a>
                    </div>
                </div>
                <input type="hidden" id="colonia_empleo" name="colonia_empleo" class="">
                <input type="hidden" id="id_colonia_empleo" name="id_colonia" class="">
                <input type="hidden" id="id_delegacion_municipio_empleo" name="id_delegacion_municipio_empleo" class="">
                <input type="hidden" id="id_estado_empleo" name="id_estado_empleo" class="">
                <input type="hidden" id="codigo_estado_empleo" name="codigo_estado_empleo" class="">
                <input type="hidden" id="id_ciudad_empleo" name="id_ciudad_empleo" class="">
            </div>
        </div>
    </form>
</div>
<div id="cuestionarioDinamico">
</div>
