<div id="verificar_codigo" style="display:none">
    <form id="formVerificarSMS">
        <div class="col-xs-12">
            <h2 class="secondary-title txt-center">Verifica tu código</h2>
            @if(Auth::guard('prospecto')->check())
                <div class="calculadoras-productos"><small>Te hemos enviado un código de verificación por SMS al número: <b> {{ Auth::guard('prospecto')->user()->celular }}</b>.</small></div>
            @else
                <div class="calculadoras-productos"><small>Te hemos enviado un código de verificación por SMS al número: <b id="show_celular"></b>.</small></div>
            @endif
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-lg-4 col-lg-offset-6 col-md-4 col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-4 calculadora-personal">
                    <input type="text" id="conf_code" name="conf_code" class="required only_numbers" maxlength="5" style="text-align: center;"/>
                    <input type="hidden" id="login" name="login"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div class="col-lg-10 col-lg-offset-1 col-xs-12 mt-0">
                    <div class="text-center">
                        <button id="reenviarSMS" type="button" class="general-button disabled" onclick="reenviar_sms()"><span>Reenviar SMS <b id="timerResend"></b></span></button>
                        <a class="general-button" onclick="valida_sms()"><span>Validar código</span></a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
