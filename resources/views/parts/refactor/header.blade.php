<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<!-- Facebook tagging -->
	<meta property="fb:app_id" content=""/>
	<meta property="og:type"   content="website" />
	<meta property="og:url"    content="https://progresemos.com" />
	<meta property="og:title"  content="Progresemos"/>
	<meta property="og:image"  content="" />
	<meta property="og:description"  content="Un préstamo diseñado a tu medida con pagos fijos. Cumple tus objetivos con las mejores facilidades."/>
	<!-- End of facebook tagging -->
	<link rel="shortcut icon" href="/images/brand/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta name="description" content="Un préstamo diseñado a tu medida con pagos fijos. Cumple tus objetivos con las mejores facilidades.">
	<meta name="keywords" content="">
	<style>
	:focus {
		/* outline: 0 !important; */
	}
	</style>
	<link type="text/css" rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> --}}
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.1/css/bootstrap-slider.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.1/bootstrap-slider.js"></script>
	<title>Progresemos</title>
	<link rel="icon" type="image/png" href="/images/brand/favicon.png">
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<header id="header">
	<nav class="navbar">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="/">
					<img src="{{ asset('images/brand/Logo_Final_Blanco_50px.png') }}" alt="Progresemos" title="">
				</a>
			</div>
			<div id="navbar" class="">
				@if (Auth::guard('prospecto')->check())
				<ul class="nav navbar-nav navbar-right loggeo">
					{{-- <li><a href="/">Inicio</a></li> --}}
					<li class="nav-item dropdown">
				        <a class="white-green-btn" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::guard('prospecto')->user()->nombres }} {{ Auth::guard('prospecto')->user()->apellido_paterno }} {{ Auth::guard('prospecto')->user()->apellido_materno }}</a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a>
				        </div>
			      	</li>
				</ul>
                @else
					<ul class="nav navbar-nav navbar-right loggeo">
						{{-- <li><a href="/">Inicio</a></li> --}}
						<li id="linkNoSesion"><a class="white-green-btn" href="#" data-toggle="modal" data-target="#loginModal">Iniciar sesión</a></li>
						<li id="linkSesion" style="display:none" class="nav-item dropdown">
					        <a id="nombreProspecto" class="white-green-btn" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          <a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a>
					        </div>
				      	</li>
					</ul>
                @endif

			</div>
		</div>
	</nav>

</header>
