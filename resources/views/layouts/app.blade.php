<!DOCTYPE html>
<html lang='en'>
    <head>
        @include('parts.refactor.header', $configuracion)
    </head>
    <style type="text/css">
        .modal-login {
            color: #636363;
            width: 350px;
        }
        .modal-login .modal-content {
            border-radius: 5px;
            border: none;
            max-width: 95vw;
        }
        .modal-login .modal-header {
            border-bottom: none;
            position: relative;
            justify-content: center;
        }
        .modal-login h4 {
            text-align: center;
            font-size: 26px;
        }
        .modal-login  .form-group {
            position: relative;
        }
        .modal-login i {
            position: absolute;
            left: 13px;
            top: 11px;
            font-size: 18px;
        }
        .modal-login .form-control {
            padding-left: 40px;
        }
        .modal-login .form-control:focus {
            border-color: #00ce81;
        }
        .modal-login .form-control, .modal-login .btn {
            min-height: 40px;
            border-radius: 3px;
        }
        .modal-login .hint-text {
            text-align: center;
            padding-top: 10px;
        }
        .modal-login .btn {
            background: #00ce81;
            border: none;
            line-height: normal;
        }
        .modal-login .btn:hover, .modal-login .btn:focus {
            background: #00bf78;
        }
        .modal-login .modal-footer {
            background: #cdcdc0;
            border-color: #dee4e7;
            text-align: center;
            /*margin: 0 -20px -20px;*/
            border-radius: 5px;
            font-size: 13px;
            justify-content: center;
        }
        .modal-login .modal-footer a {
            color: #999;
        }
        .navbar-nav>li>a:hover {
            color: #999;
            background-color: transparent;
        }
        .nav>li>a:hover, .nav>li>a:active, .nav>li>a:focus {
            color: #999;
            background-color: transparent;
        }
        .display-field {
            display: inline-block;
            vertical-align: middle;
        }
        .field_oculto {
            display: none;
        }
        legend {
            font-size: 1rem;
        }
        @font-face {
        font-family: LeckerliOne-Regular;
            src: url('/fonts/vendor/bootstrap-sass/bootstrap/LeckerliOne-Regular.otf');
        }
        @font-face {
        font-family: Montserrat-Regular;
            src: url('/fonts/vendor/bootstrap-sass/bootstrap/Montserrat/Montserrat-Regular.ttf');
            font-weight: normal;
        }
        @font-face {
            font-family: Montserrat-Regular;
            src: url('/fonts/vendor/bootstrap-sass/bootstrap/Montserrat/Montserrat-Bold.ttf');
            font-weight: bold;
        }
        .cursive {
            font-family: 'LeckerliOne-Regular';
            text-transform: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
        }
        body {
            font-family: 'Montserrat-Regular' !important;
        }
        .lsolicitudTitulo {
            display: block;
            font-weight: bold !important;
        }
        .lsolicitud {
            display: block;
            font-weight: normal !important;
            font-family: 'Tahoma';
        }
        .row-table {
            border-top: 1px solid #f4f4f4;
        }
        .title {
            font-size: 14px !important;
            font-family: 'Montserrat-Regular';
        }
        .pagoTarjeta-unchecked, .pagoPP-uncheckedcheck {
        }
        .pagoTarjeta-check, .pagoPP-check {
            color: #fff;
            background-color: #3490dc;
            border-color: #3490dc;
        }
    }
    </style>
    <body id="laPaz">
        {{-- @yield('head') --}}
        <link rel="stylesheet" href="/css/_all-skins.min.css">
        <link rel="stylesheet" href="/css/AdminLTE.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        @include('googletagmanager::head')
         <!-- Google Analytics -->
         <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            @if(env('APP_ENV') == 'local' || env('APP_ENV') == 'desarrollo' || env('APP_ENV') == 'desarrollos')
                @verbatim
                    ga('create', 'UA-80377318-3', 'auto', {
                        'cookieDomain': 'none'
                    });
                @endverbatim
            @else
                @verbatim
                    ga('create', 'UA-80377318-2', 'auto', {
                        'cookieDomain': 'none'
                    });
                @endverbatim
            @endif
            ga(function(tracker) {
                var clientId = tracker.get('clientId');
                tracker.set('dimension1', tracker.get('clientId'));
            });
            ga('send', 'pageview');
         </script>
        <!-- popup login-->
        @include('parts.refactor.login-form')
        <!-- End popup login-->
        <section class="page-view page-product container">
            <div id="app">
                @yield('content')
            </div>
        </section>
        @include('parts.refactor.footer')
        @include('googletagmanager::body')
   </body>
</html>
