<!DOCTYPE html>
<html lang='es'>
<head>
    <meta charset="utf-8">
    <title>Prestanómico - Carga de documentos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="/images/favicon_prestanomico_negro.png">
    <!--Google Search Console-->
    <meta name="google-site-verification" content="RQBze-kf4yQjVFtw5YCyCvSMNbMvGoia5IYCcOjBcao" /><head>
    <script src="/js/webapp.js"></script>
    
    <!-- <script src="https://kit.fontawesome.com/0d0c3b6669.js"></script> -->
    <script async defer src="/js/jquery/icarSDK.js"></script>
    <link rel="stylesheet" href="/css/theme.min.css">
    <link rel="stylesheet" href="/css/font-awesome.css">
	<link rel='stylesheet' href='/icon-webapp2/styles.css'>
	<link rel="stylesheet" href="/css/bootstrap3.7.css">
	<link rel="stylesheet" href="/css/webapp.css">
    <link rel="stylesheet" href="/css/magnific-popup.css">
    @if(env('APP_ENV') == 'local' || env('APP_ENV') == 'desarrollo' || env('APP_ENV') == 'desarrollos')
        @verbatim
        <!-- Google Tag Manager -->
        <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-K82D3GD');
        </script>
        <!-- End Google Tag Manager -->
        @endverbatim
    @else
        @verbatim
        <!-- Google Tag Manager -->
        <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5B4DM8C');
        </script>
        <!-- End Google Tag Manager -->
        @endverbatim
    @endif
    <!-- Google Analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    @if(env('APP_ENV') == 'local' || env('APP_ENV') == 'desarrollo' || env('APP_ENV') == 'desarrollos')
        @verbatim
        ga('create', 'UA-80377318-3', 'auto', {
            'cookieDomain': 'none'
        });
        @endverbatim
    @else
        @verbatim
        ga('create', 'UA-80377318-2', 'auto', {
            'cookieDomain': 'none'
        });
        @endverbatim
    @endif
    ga(function(tracker) {
        var clientId = tracker.get('clientId');
        tracker.set('dimension1', tracker.get('clientId'));
    });
    ga('send', 'pageview');
    </script>

</head>
<body>
    <header class="header">
        @include('webapp.header')
        @yield('head')
    </header>
    <main class="container">
        @include('parts.refactor.modalsWebapp')
        @yield('content')
    </main>
</body>
</html>
