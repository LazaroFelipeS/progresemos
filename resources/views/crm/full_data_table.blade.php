@extends('crm.app')
@section('content')
<div class="container" style="margin-right:150px;">
	<h2>Tabla Completa</h2>
	<p>
		<div class="row">
			<div class="col-sm-1">
				<div class="form-group">
					<form action="/panel/report-completo-csv/" method="POST">
					{!! csrf_field() !!}
					<label for="full_report_month">Mes</label>
					<select class="form-control" id="full_report_month" name="full_report_month">
						<option value="Mes">Mes</option>
						<option value="01" {{ ($url_month == "01")? 'selected' : '' }}>01</option>
						<option value="02" {{ ($url_month == "02")? 'selected' : '' }}>02</option>
						<option value="03" {{ ($url_month == "03")? 'selected' : '' }}>03</option>
						<option value="04" {{ ($url_month == "04")? 'selected' : '' }}>04</option>
						<option value="05" {{ ($url_month == "05")? 'selected' : '' }}>05</option>
						<option value="06" {{ ($url_month == "06")? 'selected' : '' }}>06</option>
						<option value="07" {{ ($url_month == "07")? 'selected' : '' }}>07</option>
						<option value="08" {{ ($url_month == "08")? 'selected' : '' }}>08</option>
						<option value="09" {{ ($url_month == "09")? 'selected' : '' }}>09</option>
						<option value="10" {{ ($url_month == "10")? 'selected' : '' }}>10</option>
						<option value="11" {{ ($url_month == "11")? 'selected' : '' }}>11</option>
						<option value="12" {{ ($url_month == "12")? 'selected' : '' }}>12</option>
					</select> 
				</div>
			</div>
			<div class="col-sm-2"> 
				<div class="form-group">
					<label for="full_report_month">Año</label>
					<select class="form-control" id="full_report_year" name="full_report_year">
						<option value="Año">Año</option>
						<option value="2016" {{ ($url_year == "2016")? 'selected' : '' }}>2016</option>
						<option value="2017" {{ ($url_year == "2017")? 'selected' : '' }}>2017</option>
						<option value="2018" {{ ($url_year == "2018")? 'selected' : '' }}>2018</option>
						<option value="2019" {{ ($url_year == "2019")? 'selected' : '' }}>2019</option>
						<option value="2020" {{ ($url_year == "2020")? 'selected' : '' }}>2021</option>
						<option value="2020" {{ ($url_year == "2020")? 'selected' : '' }}>2022</option>
						<option value="2020" {{ ($url_year == "2020")? 'selected' : '' }}>2023</option>
						<option value="2020" {{ ($url_year == "2020")? 'selected' : '' }}>2024</option>
					</select>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label for="full_report_month">&nbsp;&nbsp;</label><br>
					<input type="submit" class="btn btn-primary" value="Filtrar">
					</form>
				</div>
			</div>
			<div class="col-sm-2">
				<form id="download_solics_form" action="" method="POST">
					{!! csrf_field() !!}
					<input type="hidden" name="full_report_month" value="{{$url_month}}">
					<input type="hidden" name="full_report_year" value="{{$url_year}}">
					<input type="hidden" name="full_report_type" value="solics">
					<label for="">&nbsp;&nbsp;</label><br>
					<button id="download_solics_btn" class="btn btn-success">Descargar Solicitudes</button>
				</form>
			</div>
			<div class="col-sm-2">
				<form id="download_cuentas_form" action="" method="POST">
					{!! csrf_field() !!}
					<input type="hidden" name="full_report_month" value="{{$url_month}}">
					<input type="hidden" name="full_report_year" value="{{$url_year}}">
					<input type="hidden" name="full_report_type" value="cuentas">
					<label for="">&nbsp;&nbsp;</label><br>
					<button id="download_cuentas_btn" class="btn btn-success">Descargar Cuentas &nbsp;&nbsp;</button>
				</form>
			</div>
			<div class="col-sm-2">
				<form id="download_consultas_form" action="" method="POST">
					{!! csrf_field() !!}
					<input type="hidden" name="full_report_month" value="{{$url_month}}">
					<input type="hidden" name="full_report_year" value="{{$url_year}}">
					<input type="hidden" name="full_report_type" value="consultas">
					<label for="">&nbsp;&nbsp;</label><br>
					<button id="download_consultas_btn" class="btn btn-success">Descargar Consultas</button>
				</form>
				</div>
			</div>
		</div>
	</p>
	<table class="table table-striped table-bordered table-hover table-responsive" style="margin-left:20px;">
    @foreach($data as $row_key=>$row)
    	<tr>
	    	@foreach($row as $col_key=>$col)
	    		@if($row_key == 0)
	  				<th>
	  					<!-- {{ '('.$col_key.')'}}<br> -->
	  					{{ $col }}
	  				</th>
	    		@else
	    			@if(in_array($col_key,['70','71','72','73','74','75']))
	    				<!-- ingore TL, consultas, MOP(1,2,3) data, and Pago Mensual Data -->
	    			@else
		    			<td>
		    				<a href="#" data-toggle="modal" data-target="#solic_modal_{{$row_key}}">
				    			@if($col_key == 02)
				    				<!-- Fecha Registro Format -->
				    				{{ date('d/m/Y', strtotime($col)) }}
				    			@else
				    				{{ mb_strtoupper($col) }}
				    			@endif
			    			</a>
		    			</td>
		    		@endif
	    		@endif
	    	@endforeach
    	</tr>
    @endforeach
	</table>

	<!-- Modals -->
	@foreach($data as $row_key=>$row)
		<div class="modal fade" id="solic_modal_{{$row_key}}" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Cálculos, Cuentas y Consultas de la Solicitud # {{ $row[0] }} (Prospecto # {{ $row[1] }})</h4>
		      </div>
		      <div class="modal-body">
		      	
		      	<?php 
		      		//===================================== MOP 1 DEBUG REPORT ===================================
		      			//check if mop1 report exists and is not false
			        	if(isset($row[72])){
			        		if($row[72]){
				        		$mop1_report = json_decode($row[72]);
				        		$mop1_total_all_cuentas = $mop1_report->mop1_total_all_cuentas;
				      			$meses_total = $mop1_report->meses_total;
				      			$porcentaje_mop1_a_meses = number_format($mop1_report->porcentaje_mop1_a_meses,2).'%';
				      			$porcentaje_mop1_pasa = ($mop1_report->porcentaje_mop1_pasa)? 'Si' : 'No';
				      			$minimo_para_pasar = '(minimo para pasar: '.$mop1_report->minimo_para_pasar.'%)';
				        		echo '
				        			<strong>Cálculo MOP1:</strong> '.$minimo_para_pasar.'<br>
							      	<table class="table table-striped table-bordered table-hover table-responsive">
							      		<tr> 
							      			<td><strong>MOP1 total:</strong> '.$mop1_total_all_cuentas.'</td> 
							      			<td><strong>Meses total:</strong> '.$meses_total.' </td> 
							      		</tr>
							      		<tr> 
							      			<td><strong>Porcentaje MOP1:</strong> '.$porcentaje_mop1_a_meses.' </td> 
							      			<td><strong>MOP1 Pasa:</strong> '.$porcentaje_mop1_pasa.' </td> 
							      		</tr>
							      	</table><br>
				        		';
				        	}
			        	}
			      ?>
			      


		      	<?php 
		      		//===================================== MOP 2 DEBUG REPORT ===================================

		      			//set defaults
		      			$minimo_para_pasar = '';
		      			$mop2_report = [];
		      			//check if mop2 report exists and is not false
			        	if(isset($row[73])){
			        		if($row[73]){
				        		$mop2_report = json_decode($row[73]);
				        		$minimo_para_pasar = '(menos de 3 cuentas con 2 o más Mop2 (o mayor) en los ultimos 12 meses)';
				        		
				        		echo '
				        			<strong>Cálculo MOP2:</strong> '. $minimo_para_pasar .'<br>
							      	<table class="table table-striped table-bordered table-hover table-responsive">
							      		<tr> 
							      			<th>Historial</th>
													<th>Pago Reciente</th>
													<th>Fecha Solicitud</th>
													<th>Diferencia</th>
													<th>Meses a contar</th>
													<th>Meses contados</th>
													<th>Mop2+ contados</th>
													<th>TL Califica?<br><span style="font-size:9px;">(2 mop2+ o más)</span></th>
							      		</tr>
							      ';
				      			$cuentas_mop2_calificadas = 0;
				      			foreach($mop2_report as $tl){
				      				$califica = 'No';//default
				      				if($tl[6] >= 2){
				      					$califica = 'Si';
				      					$cuentas_mop2_calificadas ++; 
				      				}
				      				echo '
						      				<tr> 
								      			<td>'.$tl[0].'</td>
														<td>'.$tl[1].'</td>
														<td>'.$tl[2].'</td>
														<td>'.$tl[3].'</td>
														<td>'.$tl[4].'</td>
														<td>'.$tl[5].'</td>
														<td>'.$tl[6].'</td>
														<td>'.$califica.'</td>
								      		</tr>
								      ';
				      			}
				      			$mop2_pasa = ($cuentas_mop2_calificadas > 2)? 'No' : 'Si';

						      	echo '<tr> 
								      			<td colspan="1"><strong>Total de Cuentas</strong><br>
								      				'. count($mop2_report) .' 
								      			</td> 
								      			<td colspan="3"><strong>Total de Cuentas Calificadas</strong> <br>
								      				'. $cuentas_mop2_calificadas .'
								      			</td> 
								      			<td colspan="4"><strong>MOP2 Pasa:</strong> <br>
								      				'. $mop2_pasa .'
								      			</td> 
								      		</tr>
								      	</table><br>
				        		';
				        	}
			        	}
			      ?>


			      <?php 
		      		//===================================== MOP 3 DEBUG REPORT ===================================

		      			//set defaults
		      			$minimo_para_pasar = '';
		      			$mop3_report = [];
		      			//check if mop3 report exists and is not false
			        	if(isset($row[74])){
			        		if($row[74]){
				        		$mop3_report = json_decode($row[74]);
				        		$minimo_para_pasar = '(No más de una cuenta con más de 1 mop3 o mayor en los ultimos 18 meses)';
				        		
				        		echo '
				        			<strong>Cálculo MOP3:</strong> '. $minimo_para_pasar .'<br>
							      	<table class="table table-striped table-bordered table-hover table-responsive">
							      		<tr> 
							      			<th>Historial</th>
													<th>Pago Reciente</th>
													<th>Fecha Solicitud</th>
													<th>Diferencia</th>
													<th>Meses a contar</th>
													<th>Meses contados</th>
													<th>Mop3+ contados</th>
													<th>TL Califica?<br><span style="font-size:9px;">(1 mop3+ o más)</span></th>
							      		</tr>
							      ';
				      			$cuentas_mop3_calificadas = 0;
				      			foreach($mop3_report as $tl){
				      				$califica = 'No';//default
				      				if($tl[6] >= 1){
				      					$califica = 'Si';
				      					$cuentas_mop3_calificadas ++; 
				      				}
				      				echo '
						      				<tr> 
								      			<td>'.$tl[0].'</td>
														<td>'.$tl[1].'</td>
														<td>'.$tl[2].'</td>
														<td>'.$tl[3].'</td>
														<td>'.$tl[4].'</td>
														<td>'.$tl[5].'</td>
														<td>'.$tl[6].'</td>
														<td>'.$califica.'</td>
								      		</tr>
								      ';
				      			}
				      			$mop3_pasa = ($cuentas_mop3_calificadas > 1)? 'No' : 'Si';

						      	echo '<tr> 
								      			<td colspan="1"><strong>Total de Cuentas</strong><br>
								      				'.count($mop3_report) .' 
								      			</td> 
								      			<td colspan="3"><strong>Total de Cuentas Calificadas</strong> <br>
								      				'. $cuentas_mop3_calificadas .'
								      			</td> 
								      			<td colspan="4"><strong>MOP3 Pasa:</strong> <br>
								      				'. $mop3_pasa .'
								      			</td> 
								      		</tr>
								      	</table><br>
				        		';
				        	}
			        	}
			      ?>

			      <?php 
		      		//===================================== PAGO MENSUAL DE DEUDA REPORT ===================================

		      			//set defaults
		      			
		      			$pago_mensual_report = [];
		      			//check if pago_mensual_report exists and is not false
			        	if(isset($row[75])){
			        		if($row[75]){
				        		$pago_mensual_report = json_decode($row[75]);
				        		$minimo_para_pasar = '(Se utiliza para determinar DEUDA A INGRESO, INGRESO DISPONIBLE, y ATP)';
				        		$pago_mensual_total_de_deuda = 0;
				        		echo '
				        			<strong>Cálculo PAGO MENSUAL TOTAL DE DEUDA BC:</strong> '. $minimo_para_pasar .'<br>
							      	<table class="table table-striped table-bordered table-hover table-responsive">
							      		<tr> 
							      			<th>Num.</th>
													<th>Fecha de Cierre</th>
													<th>Saldo Actual</th>
													<th>Monto Vencido</th>
													<th>6%</th>
													<th>Monto a Pagar</th>
													<th>Contrato Producto</th>
													<th>Tipo</th>
													<th>Frequencia</th>
													<th>Monto Ajustado</th>
							      		</tr>
							      ';
				      			
				      			foreach($pago_mensual_report as $tl){
				      				echo '
						      				<tr> 
								      			<td>'.$tl->num.'</td>
													';
													//check for fecha de cierra and ignore the rest of the fields if exists
													if($tl->fecha_cierre != ''){
														echo '
															<td style="color:red">'.$tl->fecha_cierre.'</td>
															<td colspan="7" style="background-color:#999;"></td>
															<td style="background-color:#999;color:#666">'.$tl->monto_ajustado.'</td>
									      		</tr>
											      ';
													}else{//continue
														echo '
															<td style="color:green">OK</td>
														';
														//check if saldo actual is set and > 0																	
														if($tl->saldo_actual == '0' || $tl->saldo_status == '-'){
																echo '
																		<td style="color:red">'.$tl->saldo_actual.$tl->saldo_status.'</td>
																		<td colspan="7" style="background-color:#999;"></td>
												      		</tr>
												      	';
														}else{//continue
															echo '<td style="color:green">'.$tl->saldo_actual.$tl->saldo_status.'</td>';
															//check if saldo vencido will be used
															if($tl->saldo_vencido != 'NO'){
																echo '<td style="color:orange;">'.$tl->saldo_vencido.'</td>
																			<td style="background-color:#999;color:#666">'.$tl->est_min.'</td>
																			<td style="background-color:#999;color:#666">'.$tl->monto_pagar.'</td>
																';
															}else{//continue
																echo '<td style="color:green;">'.$tl->saldo_vencido.'</td>';
																//check if 6% will be used
																if($tl->monto_pagar <= $tl->est_min){
																	echo '
																			<td style="color:green">'.$tl->est_min.'</td>
																			<td style="background-color:#999;color:#666">'.$tl->monto_pagar.'</td>
																	';
																	//check if the contrato producto is in accepted array
																	if(in_array($tl->contrato_producto,['CC', 'CL', 'LR', 'SC', 'TE'])){
																		echo '<td style="color:green">'.$tl->contrato_producto.'</td>
																					<td style="background-color:#999;color:#666">'.$tl->tipo.'</td>
																					<td style="background-color:#999;color:#666">'.$tl->frequencia.'</td>
																					<td>'.$tl->monto_ajustado.'</td>
									      								</tr>';//done
																	}else{
																		echo '<td style="color:red">'.$tl->contrato_producto.'</td>
																					<td colspan="2" style="background-color:#999;"></td>
																					<td style="background-color:#999;color:#666">'.$tl->monto_ajustado.'</td>
																				</tr>';//done
																	}
																}else{//normal monto pagar will be used
																	echo '
																			<td style="background-color:#999;color:#666">'.$tl->est_min.'</td>
																			<td style="color:green">'.$tl->monto_pagar.'</td>
																			<td style="background-color:#999;color:#666">'.$tl->contrato_producto.'</td>
																			<td>'.$tl->tipo.'</td>
																			<td>'.$tl->frequencia.'</td>
																			<td>'.$tl->monto_ajustado.'</td>
									      						</tr>';//done
																}
															}
													  }
													}
													
																	
								      $pago_mensual_total_de_deuda += $tl->monto_ajustado;
				      			}
				      			
						      	echo '<tr> 
								      			<td colspan="9" class="text-right"><strong>Pago Mensual Total de Deuda BC:</strong></td> 
								      			<td>'. $pago_mensual_total_de_deuda .'</td> 
								      		</tr>
								      	</table><br>
				        		';
				        	}
			        	}
			      ?>
			      
		      	<strong>Cuentas:</strong>
		      	<table class="table table-striped table-bordered table-hover table-responsive">
			        <?php 
			        	if(isset($row[70])){
			        		$accounts = json_decode($row[70]);
			        		$acc_counter = 1;
			        		foreach($accounts->accounts_collection as $tl){
			        			echo '<tr><td><br>'.$acc_counter.':</td><td></td><tr>';
	        					foreach($tl as $prop=>$val){
	        						echo '<tr><td>'.$prop.':</td><td>'.$val.'</td></tr>';
	        					}
	        					$acc_counter++;
			        		}
			        	}
			        ?>
		      	</table><br>
		      	<strong>Consultas:</strong>
		      	<table class="table table-striped table-bordered table-hover table-responsive">
		      		<?php 
			        	if(isset($row[71])){
			        		$consultas = json_decode($row[71]);
			        		$con_counter = 1;
			        		foreach($consultas->consultas_collection as $con){
			        			echo '<tr><td><br>'.$con_counter.':</td><td></td><tr>';
	        					foreach($con as $prop=>$val){
	        						echo '<tr><td>'.$prop.':</td><td>'.$val.'</td></tr>';
	        					}
	        					$con_counter++;
			        		}
			        	}
			        ?>
		      	</table>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
	@endforeach
</div>

@endsection