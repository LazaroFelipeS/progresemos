@extends('crm.app')
@section('content')
<style>
	@font-face {
		font-family: Montserrat-Regular;
		src:url("../Fonts/Montserrat/Montserrat-Regular.ttf")
	}
	.swal2-title {
		font-family: 'Montserrat-Regular' !important;
		font-size: 18px !important;
	}
	.swal2-content {
		font-family: 'Montserrat-Regular' !important;
		font-size: 15px !important;
		text-align: left !important;
	}
</style>
<div class="container" style="width:100%">
	<div id="dashboard-container" class="content">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<div class="col-xs-12">
						<center>
							<h3 class="box-title">
								Calendario de Citas
							</h3>
						</center>
					</div>
					<br><br>
				</div>
				@if(Auth::user()->can('calendario-citas'))
				<div class="box-body" style="font-size: 12px;">
					<div class="col-sm-10 col-md-offset-1">
						<div id='calendar'></div>
					</div>
				</div>
				@else
					<center>
						<br>
						<h4>No tienes privilegios para realizar esta acción</h4>
					</center>
				@endif
			</div>

		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="/back/js/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="/back/js/fullcalendar.es.js"></script>
<script>
	$(document).ready(function() {
		$.ajax({
	        method: "GET",
	        url: "/obtener-citas",
	        dataType: "json",

	    }).done(function(resultado) {

			$('#calendar').fullCalendar({
				header: {
				  left: 'prev,next today',
				  center: 'title',
				  right: 'month,agendaWeek,agendaDay,listWeek'
				},
				defaultView: 'listWeek',
				locale: 'es',
				events: resultado,
				eventClick: function(event) {
					swal({
						title: event.title,
						html: event.description,
						showCloseButton: true,
						showCancelButton: false
					});
			    }
			})

	    }).fail(function(resultado, textStatus, errorThrow) {


	    });

	});
</script>
@endsection
