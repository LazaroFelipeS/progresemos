<!DOCTYPE html>
<html ng-app="prestaburo">
	<head>
		<title>Prestanómico Buró de Crédito</title>
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
		<link href="/back/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="/back/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
		<link href="/back/css/AdminLTE.min.css" rel="stylesheet" type="text/css">
		<link href="/back/css/main.css" rel="stylesheet" type="text/css">
		<link href="/back/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
		<link href="/back/css/fullcalendar.min.css" rel="stylesheet" type="text/css">
		<meta name="csrf-token" content="{{ csrf_token() }}">
	</head>
	<style>
		.nav>li>a {
    		padding-right: 10px;
    		padding-left: 10px;
		}
	</style>
	<body>
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#admin-main-nav" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/">
		      	<img src="/back/img/logo_prestanomico.png">
		      </a>
		    </div>

		    <div class="collapse navbar-collapse" id="admin-main-nav">
		      @if(Auth::check())
			      <ul class="nav navbar-nav">
			      	<!-- <li><a>{{ $_SERVER['REQUEST_URI'] }}</a></li> -->
			        <li class="{{ ($_SERVER['REQUEST_URI'] == '/panel')? 'active' : '' }}"><a href="/panel">Dashboard</a></li>
					@if(Auth::user()->can('calendario-citas'))
						<li class="{{ ($_SERVER['REQUEST_URI'] == '/calendario-citas')? 'active' : '' }}">
							<a href="/calendario-citas">Calendario Citas</a>
						</li>
				 	@endif
					@if(Auth::user()->can('reportes'))
						<li class="dropdown {{ (strpos($_SERVER['REQUEST_URI'],'report-completo-csv') !== false)? 'active' : '' }}">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes <span class="caret"></span></a>
				          <ul class="dropdown-menu">
				            <li><a href="/panel/report-completo-csv/">Tabla Completa</a></li>
				          </ul>
				        </li>
					@endif
					@if(Auth::user()->can('logs-registro'))
				        <li class="{{ ($_SERVER['REQUEST_URI'] == '/panel/registro-logs')? 'active' : '' }}">
				        	<a href="/panel/registro-logs">Logs de Registro</a>
				        </li>
					@endif
					@if(Auth::user()->can('codigos-postales'))
						<li class="dropdown {{ ($_SERVER['REQUEST_URI'] == '/panel/cp-console')? 'active' : '' }}">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Códigos Postales<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li>
									<a href="/panel/cp-console">Mantenimento códigos postales</a>
								</li>
								<li>
									<a href="/panel/cobertura">Cobertura códigos postales</a>
								</li>
							</ul>
						</li>
					@endif
					@if(Auth::user()->can('alta-automatica') || Auth::user()->can('administrador-productos') || Auth::user()->can('administracion-usuarios') || Auth::user()->can('cliente-estrella'))
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Desarrollos<span class="caret"></span></a>
						<ul class="dropdown-menu">
							@if(Auth::user()->can('alta-automatica'))
							<li class="{{ ($_SERVER['REQUEST_URI'] == '/alta-clientes')? 'active' : '' }}">
								<a href="/alta-clientes">Alta automática T24</a>
							</li>
							@endif
							@if(Auth::user()->can('administrador-productos'))
							<li class="{{ ($_SERVER['REQUEST_URI'] == '/productos')? 'active' : '' }}">
								<a href="/productos">Administrador de productos</a>
							</li>
							@endif
							@if(Auth::user()->can('administracion-usuarios'))
							<li class="{{ ($_SERVER['REQUEST_URI'] == '/panel/usuarios')? 'active' : '' }}">
								<a href="/panel/usuarios">Usuarios</a>
							</li>
							@endif
							@if(Auth::user()->can('cliente-estrella'))
							<li class="{{ ($_SERVER['REQUEST_URI'] == '/panel/clienteEstrella')? 'active' : '' }}">
								<a href="/panel/clienteEstrella">Cliente estrella</a>
							</li>
							@endif
						</ul>
					</li>
					@endif
					@if(Auth::user()->can('captura'))
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Captura<span class="caret"></span></a>
						<ul class="dropdown-menu">
							@if(Auth::user()->can('captura-dentalia'))
							<li class="{{ ($_SERVER['REQUEST_URI'] == '/panel/producto/captura/dentalia')? 'active' : '' }}">
								<a href="/panel/producto/captura/dentalia">Solicitud Dentalia</a>
							</li>
							@endif
						</ul>
					</li>
					@endif
			      </ul>
			      <ul class="nav navbar-nav navbar-right">
			        <li><a href="/logout">Cerrar Sessión</a></li>
			      </ul>
			  	@endif
				@if(Auth::guard('captura_convenio')->check())
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
					  	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::guard('captura_convenio')->user()->sucursal }} - {{ Auth::guard('captura_convenio')->user()->embajador }} <span class="caret"></span></a>
					  	<ul class="dropdown-menu">
						  	<li><a href="/logout/captura/{{ request()->segment(4) }}">Cerrar Sessión</a></li>
					  	</ul>
					</li>
				</ul>
				@endif
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		@yield('content')
		<script type="text/javascript" src="/back/js/jquery.min.js"></script>
		<script type="text/javascript" src="/back/js/moment.min.js"></script>
		<script type="text/javascript" src="/back/js/fullcalendar.js"></script>
		<script type="text/javascript" src="/back/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="/back/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/back/js/angular.min.js"></script>
		<script type="text/javascript" src="/back/js/app.js"></script>
		<script type="text/javascript" src="/back/js/main.js"></script>
		@yield('scripts')
		@yield('app_backoffice')
	</body>
</html>
