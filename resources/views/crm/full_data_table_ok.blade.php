@extends('crm.app')
@section('content')
<div class="container" style="margin-right:150px;">
	<h2>Reporte Tabla Completa</h2>
	@if(Auth::user()->can('reportes'))
	<p>
		<div class="row">
			<div class="col-sm-1">
				<div class="form-group">
					<form action="/panel/report-completo-csv/" method="POST">
					{!! csrf_field() !!}
					<label for="full_report_month">Mes</label>
					<select class="form-control" id="full_report_month" name="full_report_month">
						<option value="Mes">Mes</option>
						<option value="01" {{ ($url_month == "01")? 'selected' : '' }}>01</option>
						<option value="02" {{ ($url_month == "02")? 'selected' : '' }}>02</option>
						<option value="03" {{ ($url_month == "03")? 'selected' : '' }}>03</option>
						<option value="04" {{ ($url_month == "04")? 'selected' : '' }}>04</option>
						<option value="05" {{ ($url_month == "05")? 'selected' : '' }}>05</option>
						<option value="06" {{ ($url_month == "06")? 'selected' : '' }}>06</option>
						<option value="07" {{ ($url_month == "07")? 'selected' : '' }}>07</option>
						<option value="08" {{ ($url_month == "08")? 'selected' : '' }}>08</option>
						<option value="09" {{ ($url_month == "09")? 'selected' : '' }}>09</option>
						<option value="10" {{ ($url_month == "10")? 'selected' : '' }}>10</option>
						<option value="11" {{ ($url_month == "11")? 'selected' : '' }}>11</option>
						<option value="12" {{ ($url_month == "12")? 'selected' : '' }}>12</option>
					</select>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label for="full_report_month">Año</label>
					<select class="form-control" id="full_report_year" name="full_report_year">
						<option value="Año">Año</option>
						<option value="2016" {{ ($url_year == "2016")? 'selected' : '' }}>2016</option>
						<option value="2017" {{ ($url_year == "2017")? 'selected' : '' }}>2017</option>
						<option value="2018" {{ ($url_year == "2018")? 'selected' : '' }}>2018</option>
						<option value="2019" {{ ($url_year == "2019")? 'selected' : '' }}>2019</option>
						<option value="2020" {{ ($url_year == "2020")? 'selected' : '' }}>2021</option>
						<option value="2020" {{ ($url_year == "2020")? 'selected' : '' }}>2022</option>
						<option value="2020" {{ ($url_year == "2020")? 'selected' : '' }}>2023</option>
						<option value="2020" {{ ($url_year == "2020")? 'selected' : '' }}>2024</option>
					</select>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label for="full_report_month">&nbsp;&nbsp;</label><br>
					<input type="submit" class="btn btn-primary" value="Filtrar">
					</form>
				</div>
			</div>
			<div class="col-sm-2">
				<form id="download_solics_form" action="" method="POST">
					{!! csrf_field() !!}
					<input type="hidden" name="full_report_month" value="{{$url_month}}">
					<input type="hidden" name="full_report_year" value="{{$url_year}}">
					<input type="hidden" name="full_report_type" value="solics">
					<label for="">&nbsp;&nbsp;</label><br>
					<button id="download_solics_btn" class="btn btn-success">Descargar Solicitudes</button>
				</form>
			</div>
			<div class="col-sm-2">
				<form id="download_cuentas_form" action="" method="POST">
					{!! csrf_field() !!}
					<input type="hidden" name="full_report_month" value="{{$url_month}}">
					<input type="hidden" name="full_report_year" value="{{$url_year}}">
					<input type="hidden" name="full_report_type" value="cuentas">
					<label for="">&nbsp;&nbsp;</label><br>
					<button id="download_cuentas_btn" class="btn btn-success">Descargar Cuentas &nbsp;&nbsp;</button>
				</form>
			</div>
			<div class="col-sm-2">
				<form id="download_consultas_form" action="" method="POST">
					{!! csrf_field() !!}
					<input type="hidden" name="full_report_month" value="{{$url_month}}">
					<input type="hidden" name="full_report_year" value="{{$url_year}}">
					<input type="hidden" name="full_report_type" value="consultas">
					<label for="">&nbsp;&nbsp;</label><br>
					<button id="download_consultas_btn" class="btn btn-success">Descargar Consultas</button>
				</form>
				</div>
			</div>
		</div>
	</p>
	@else
		<center>
			<br>
			<h4>No tienes privilegios para realizar esta acción</h4>
		</center>
	@endif
</div>

@endsection
