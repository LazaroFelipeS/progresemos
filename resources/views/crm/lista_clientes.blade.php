<style>
td {
    word-wrap:break-word
}
</style>
<div class="col-xs-12">
	<center><h4 class="box-title">Listado de Altas</h4></center>
	<div class="box-body table-responsive no-padding">
		<table class="table table-hover" style="overflow: auto;">
			<tbody>
				<tr>
					<th width="15%">Fecha</th>
					<th width="8%">Id Prospecto</th>
					<th width="8%">Id Solicitud</th>
					<th width="8%">No. Cliente T24</th>
					<th width="10%">Nombre del Cliente</th>
					<th width="10%">Email</th>
					<th width="8%">Alta cliente</th>
					<th width="8%">Alta solicitud</th>
					<th width="9%">Ligue de usuario</th>
                    <th width="9%">FaceMatch</th>
                    <th width="8%">Envío de email</th>
                    <th width="5%">Detalles</th>
				</tr>
            </tbody>
				@foreach ($altas as $alta)
                <tbody>
				<tr class="parent">
					<td>{{ $alta->created_at->format('Y-m-d H:i') }}</td>
					<td>{{ $alta->prospecto_id }}</td>
					<td>{{ $alta->solicitud_id }}</td>
					<td>{{ $alta->no_cliente_t24 }}</td>
					<td>{{ $alta->SHORTNAME.' '.$alta->NAME1.' '.$alta->NAME2 }}</td>
					<td>{{ $alta->EMAIL }}</td>

					<!-- Determinando Status del alta del cliente -->
					@if ($alta->status_alta == 'Alta de cliente en proceso')
						<td><span class="label label-primary">En Proceso</span></td>
					@elseif ($alta->status_alta == 'Alta de cliente erronea')
						<td><span class="label label-danger">Erronea</span></td>
					@elseif ($alta->status_alta == 'Alta de cliente exitosa')
						<td><span class="label label-success">Exitosa</span></td>
					@else
						<td><span class="label label-warning">NA</span></td>
					@endif

					<!-- Determinando Status del alta de la solicitud -->
					@if ($alta->status_solicitud == 'Alta de solicitud en proceso')
                        <td><span class="label label-primary">En Proceso</span></td>
                    @elseif ($alta->status_solicitud == 'Alta de solicitud por procesar')
						<td><span class="label label-primary">Por Procesar</span></td>
                    @elseif ($alta->status_solicitud == 'Alta de solicitud no procesada')
    						<td><span class="label label-danger">No Procesada</span></td>
					@elseif ($alta->status_solicitud == 'Alta de solicitud erronea')
						<td><span class="label label-danger">Erronea</span></td>
					@elseif ($alta->status_solicitud == 'Alta de solicitud exitosa')
						<td><span class="label label-success">Exitosa</span></td>
					@else
						<td><span class="label label-warning">NA</span></td>
					@endif

					<!-- Determinando Status del ligue de usuario -->
					@if ($alta->status_ligue == 'Ligue de usuario en proceso')
						<td><span class="label label-primary">En Proceso</span></td>
                    @elseif ($alta->status_ligue == 'Ligue de usuario por procesar')
						<td><span class="label label-primary">Por Procesar</span></td>
					@elseif ($alta->status_ligue == 'Ligue de usuario erroneo')
						<td><span class="label label-danger">Erroneo</span></td>
                    @elseif ($alta->status_ligue == 'Ligue de usuario no procesado')
						<td><span class="label label-danger">No Procesado</span></td>
					@elseif ($alta->status_ligue == 'Ligue de usuario exitoso')
						<td><span class="label label-success">Exitoso</span></td>
					@else
						<td><span class="label label-warning">NA</span></td>
					@endif

                    <!-- Determinando Status del facematch solicitud -->
					@if ($alta->fachematch == 'Facematch pendiente')
                        <td><span class="label label-primary">Pendiente</span></td>
                    @elseif ($alta->fachematch == 'Facematch no procesado')
    						<td><span class="label label-danger">No Procesado</span></td>
					@elseif ($alta->fachematch == 'Facematch erroneo')
						<td><span class="label label-danger">Erroneo</span></td>
                    @elseif ($alta->fachematch == 'Facematch erroneo-Procesos exitos'
                        || $alta->fachematch == 'Facematch erroneo-Registro exitoso'
                        || $alta->fachematch == 'Facematch erroneo-Documentos exitoso')
						<td><span class="label label-danger">{{ $alta->fachematch }}</span></td>
					@elseif ($alta->fachematch == 'Facematch exitoso')
						<td><span class="label label-success">Exitoso</span></td>
                    @elseif ($alta->fachematch == 'Facematch exitoso-Error Registro'
                        || $alta->fachematch == 'Facematch exitoso-Error Documentos'
                        || $alta->fachematch == 'Identificación/Selfie-Documentos exitoso'
                        || $alta->fachematch == 'Identificación/Selfie-Registro exitoso')
						<td><span class="label label-warning">{{ $alta->fachematch }}</span></td>
                    @elseif ($alta->fachematch == 'Identificación/Selfie pendiente')
						<td><span class="label label-primary">{{ $alta->fachematch }}</span></td>
                    @elseif ($alta->fachematch == 'Identificación/Selfie exitoso')
						<td><span class="label label-success">{{ $alta->fachematch }}</span></td>
                    @elseif ($alta->fachematch == 'Identificación/Selfie erroneo')
						<td><span class="label label-danger">{{ $alta->fachematch }}</span></td>
					@else
						<td><span class="label label-warning">NA</span></td>
					@endif

                    <!-- Determinando Status del envio email -->
					@if ($alta->status_email == 'Envio de email por procesar')
						<td><span class="label label-primary">Por procesar</span></td>
                    @elseif ($alta->status_email == 'Envio de email en proceso')
						<td><span class="label label-primary">En proceso</span></td>
                    @elseif ($alta->status_email == 'Envio de email no procesado')
						<td><span class="label label-danger">No Procesado</span></td>
                    @elseif ($alta->status_email == 'Envio de email erroneo')
						<td><span class="label label-danger">Erroneo</span></td>
                    @elseif ($alta->status_email == 'Envio de email exitoso')
						<td><span class="label label-success">Exitoso</span></td>
                    @else
						<td><span class="label label-warning">NA</span></td>
                    @endif


                    <td><i class="fa fa-chevron-down"></i></td>
				</tr>
                <tr class="cchild">
                    @php
                        $rfc_calculado = ($alta->rfc_calculado == 1) ? 'Si' : 'No';
                    @endphp
                    <td> <b>Error: </b> </td>
                    <td colspan="4"> <b>Error Proceso Alta:</b><br>{!! $alta->error !!} <br><b>Error Facematch:<br></b> {!! nl2br($alta->facematch_error) !!} </td>
                    <td>
                        <b>RFC: </b> {!! $alta->MNEMONIC !!} <br> <b>RFC calculado</b>: {!! $rfc_calculado !!}
                    </td>
                    <td>
                        @if ($alta->error != '' && ($alta->aplica_cliente == 1 && $alta->alta_cliente == 0))
                        <button type="button" class="btn btn-primary" style="font-size: 10px" onclick="altaCliente({{ $alta->id }})">Alta cliente</button>
                        @endif
                    </td>
                    <td>
                        @if ($alta->error != '' && ($alta->aplica_solicitud == 1 && $alta->alta_solicitud == 0))
                        <button type="button" class="btn btn-success" style="font-size: 10px" onclick="altaSolicitud({{ $alta->id }})">Alta solicitud</button>
                        @endif
                    </td>
                    <td>
                        @if ($alta->error != '' && ($alta->aplica_ligue == 1 && $alta->alta_usuario_ligado == 0))
                        <button type="button" class="btn btn-primary" style="font-size: 10px" onclick="ligarUsuario({{ $alta->id }})">Ligar usuario</button>
                        @endif
                    </td>
                    <td>
                        @if ($alta->facematch_error != '' && ($alta->aplica_facematch == 1 && ($alta->facematch == 0 || $alta->registro_facematch == 0 || $alta->documentos_facematch == 0)))
                        <button type="button" class="btn btn-primary" style="font-size: 10px" onclick="reprocesarFacematch({{ $alta->id }})">Reprocesar</button>
                        @endif
                        @if ($alta->facematch_error != '' && ($alta->aplica_facematch == 0 && $alta->solo_carga_identificacion_selfie == 1 && ($alta->registro_facematch == 0 || $alta->documentos_facematch == 0)))
                        <button type="button" class="btn btn-primary" style="font-size: 10px" onclick="reprocesarFacematch({{ $alta->id }})">Reprocesar</button>
                        @endif
                    </td>
                    <td>
                        @if ($alta->error != '' && ($alta->aplica_email == 1 && $alta->email_enviado == 0))
                        <button type="button" class="btn btn-success" style="font-size: 10px" onclick="enviarEmail({{ $alta->id }})">Enviar email</button>
                        @endif
                    </td>
                    <td>
                    </td>
				</tr>

                </tbody>
				@endforeach
		</table>
		{{ $altas->links() }}
	</div>
</div>
