@extends('crm.app')
@section('content')
	<div class="container" ng-controller="MainController as main">
		<div class="content">
			<header>
				<div class="title">
					<h2>Generador de Consulta a Buró de Crédito (<strong class="kamikaze-brand">PROSPECTO BC SCORE</strong>)</lab></h2>
				</div>
			</header>
			<form id="gen_bcscore_request_form" method="POST">
				@include('crm.buro.segments_prospector.seg_autenticacion')
				@include('crm.buro.segments_prospector.seg_encabezado')
				@include('crm.buro.segments_prospector.seg_nombre')
				@include('crm.buro.segments_prospector.seg_direccion')
				@include('crm.buro.segments_prospector.seg_empleo')
				@include('crm.buro.segments_prospector.seg_referencias')
				@include('crm.buro.segments_prospector.seg_cierre')
				<div class="row">
					<div class="col-sm-12 text-center">
						<a id="gen_bcscore_request_submit" class="btn btn-primary">Enviar Solicitud</a>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection