<!-- ================================================================================== -->
<!-- ================================= SEGMENTO REFERENCIAS =========================== -->
<!-- ================================================================================== -->
<div class="panel panel-default">
  <div class="panel-heading">
  	<h2 class="panel-title"><strong>Segmento de Referencias Crediticias - PI</strong>  <span class="badge tip-btn" data-toggle="collapse" data-target="#desc_referencias">?</span></h2>
  	<div id="desc_referencias" class="collapse">
  		<p>En este Segmento se reporta la información de la(s) Cuenta(s) o Crédito(s) (Referencias Crediticias) que el Cliente manifiesta tener con otros Usuarios. Esta información proporciona la localización más exacta del Cliente</p>
			<p>Este Segmento es OPCIONAL, sin embargo se puede registrar hasta 4 referencias crediticias.</p>
			<p>De igual manera que el Segmento anterior, cada campo se le debe integrar la Etiqueta o Nombre del campo antes del dato al que se refiere</p>
  	</div>
  	<button class="btn btn-default" data-toggle="collapse" data-target="#segmento-referencias">Formulario</button>
  </div>
  <div id="segmento-referencias" class="panel-body collapse">
  	<section id="referencias_preview">
  		Preview: 
			<ul id="segmento_referencias" class="preview_segmento_list">
				<span ng-show="segs.ref.numero_de_cuenta">
					<li class="seg-label" id="seg-etiqueta_numero_de_cuenta">PI</li>
					<li class="seg-count" id="seg-len_numero_de_cuenta"><% segs.ref.numero_de_cuenta | stringLength %></li>
					<li id="seg-numero_de_cuenta"><% segs.ref.numero_de_cuenta %></li>
				</span>

				<span ng-show="segs.ref.clave_usuario">
					<li class="seg-label" id="seg-etiqueta_clave_usuario">00</li>
					<li class="seg-count" id="seg-len_clave_usuario"><% segs.ref.clave_usuario | stringLength %></li>
					<li id="seg-clave_usuario"><% segs.ref.clave_usuario %></li>
				</span>

				<span ng-show="segs.ref.nombre_usuario">
					<li class="seg-label" id="seg-etiqueta_nombre_usuario">01</li>
					<li class="seg-count" id="seg-len_nombre_usuario"><% segs.ref.nombre_usuario | stringLength %></li>
					<li id="seg-nombre_usuario"><% segs.ref.nombre_usuario %></li>
				</span>
			</ul>
		</section>
		<section id="referencias_settings">
			<div class="form-group">
				<label for="ref_numero_de_cuenta">PI - NÚMERO DE CUENTA <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_ref_numero_de_cuenta">?</span></label>
				<div id="tip_ref_numero_de_cuenta" class="collapse">
					<p>
						Ingresar el número de cuenta o crédito.
						<ul>
							<li>Los datos de las Referencias Crediticias del Cliente o prospecto, deberán ser solo las que se encuentren a su nombre como titular del crédito.</li>
							<li>No se deben considerar: tarjetas adicionales, tarjetas de débito, cuentas de inversión o cuentas de cheques u otro instrumento que no sea de crédito y el titular de la misma.</li>
						</ul>
					</p>
				</div>
				<input type="text" id="ref_numero_de_cuenta" name="ref_numero_de_cuenta" class="form-control" ng-model="segs.ref.numero_de_cuenta" maxlength="25">
				<small>Opcional | Etiqueta: PI | Logitud Variable | 25 caracteres max | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="ref_clave_usuario">00 - CLAVE DEL USUARIO o MEMBER CODE <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_ref_clave_usuario">?</span></label>
				<div id="tip_ref_clave_usuario" class="collapse">
					<p>
						Contiene la clave única o “Member Code” del Usuario que otorgó el Crédito mencionado en el campo PI.
						<ul>
							<li>Incluir si se tiene disponible.</li>
						</ul>
					</p>
				</div>
				<input type="text" id="ref_clave_usuario" name="ref_clave_usuario" class="form-control" ng-model="segs.ref.clave_usuario" maxlength="10">
				<small>Opcional | Etiqueta: 00 | Logitud Fija | 10 caracteres | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="ref_nombre_usuario">01 - NOMBRE DEL USUARIO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_ref_nombre_usuario">?</span></label>
				<div id="tip_ref_nombre_usuario" class="collapse">
					<p>
						Contiene el nombre del Usuario que otorgó el Crédito mencionado en el campo PI.
						<ul>
							<li>Incluir si se tiene disponible.</li>
						</ul>
					</p>
				</div>
				<input type="text" id="ref_nombre_usuario" name="ref_nombre_usuario" class="form-control" ng-model="segs.ref.nombre_usuario" maxlength="16">
				<small>Opcional | Etiqueta: 01 | Logitud Variable | 16 caracteres | Alfanumérico</small>
			</div>
		</section>
  </div>
</div>