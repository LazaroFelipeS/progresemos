<!-- ================================================================================== -->
<!-- ================================= SEGMENTO CIERRE ================================ -->
<!-- ================================================================================== -->
<div class="panel panel-default">
  <div class="panel-heading">
  	<h2 class="panel-title"><strong>Segmento de Cierre – ES</strong>  <span class="badge tip-btn" data-toggle="collapse" data-target="#desc_cierre">?</span></h2>
  	<div id="desc_cierre" class="collapse">
  		<p>Este segmento indica el fin del registro a consultar. Es un segmento REQUERIDO y solo se reporta una sola vez al final de cada registro.</p> 
  	</div>
  	<button class="btn btn-default" data-toggle="collapse" data-target="#segmento-cierre">Formulario</button>
  </div>
  <div id="segmento-cierre" class="panel-body collapse">
  	<section id="cierre_preview">
  		Preview: 
			<ul id="segmento_cierre" class="preview_segmento_list">
				<li class="seg-label" id="seg-etiqueta_longitud">ES</li>
				<li class="seg-count" id="seg-len_longitud"><% segs.cierre.longitud | stringLength %></li>
				<li id="seg-longitud"><% segs.cierre.longitud %></li>

				<li class="seg-label" id="seg-etiqueta_marca_fin">00</li>
				<li class="seg-count" id="seg-len_marca_fin">2</li>
				<li id="seg-marca_fin">**</li>
			</ul>
		</section>
		<section id="cierre_settings">
			<div class="form-group">
				<label for="cierre_longitud">ES - LONGITUD DEL REGISTRO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_cierre_longitud">?</span></label>
				<div id="tip_cierre_longitud" class="collapse">
					<p>
						Indica la longitud en bytes del registro de consulta que se transmitirá, incluyendo este segmento.
						<ul>
							<li>Se deberá rellenar con ceros a la izquierda para cumplir con las 5 posiciones.</li>
							<li>Si no se incluye un dato válido, se rechazará el proceso de la consulta.</li>
						</ul>
					</p>
				</div>
				<input type="text" id="cierre_longitud" name="cierre_longitud" class="form-control" maxlength="5" value="" disabled>
				<small><strong class="req-label">Requerido</strong> | Etiqueta: ES | Logitud Fija | 5 caracteres | Numérico</small>
			</div>

			<div class="form-group">
				<label for="cierre_clave_usuario">00 - MARCA DE FIN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip_cierre_marca_fin">?</span></label>
				<div id="tip_cierre_marca_fin" class="collapse">
					<p>
						Debe colocarse lo siguiente: **
						<ul>
							<li>Si no se incluye un dato válido se rechazará el proceso de consulta.</li>
						</ul>
					</p>
				</div>
				<input type="text" id="cierre_marca_fin" name="cierre_marca_fin" class="form-control" value="**" disabled>
				<small><strong class="req-label">Requerido</strong> | Etiqueta: 00 | Logitud Fija | 2 caracteres | Alfabético</small>
			</div>
		</section>
  </div>
</div>