<!-- ================================================================================== -->
<!-- ================================= SEGMENTO ENCABEZADO ============================ -->
<!-- ================================================================================== -->
<div class="panel panel-default">
  <div class="panel-heading">
  	<h2 class="panel-title"><strong>Segmento de Encabezado - INTL</strong> <span class="badge tip-btn" data-toggle="collapse" data-target="#desc_encabezado">?</span></h2>
  	<div id="desc_encabezado" class="collapse">
  		<p>El Segmento de Encabezado o de Inicio debe ser el primer segmento por cada registro a consultar, es un segmento requerido y solo debe reportarse por única vez por cada registro por consultar.</p>
  	</div>
  	<button class="btn btn-default" data-toggle="collapse" data-target="#segmento-encabezado">Formulario</button>
  </div>
  <div id="segmento-encabezado" class="panel-body collapse">
  	<section id="encabezado_preview">
	  	Preview: 
			<ul id="segmento_nombre_cliente" class="preview_segmento_list">
				<li class="seg-label" id="seg-etiqueta">INTL</li>
				<li id="seg-version">13</li>
				<li id="seg-referencia"><% segs.encabezado.referencia %></li>
				<li id="seg-producto"><% segs.encabezado.producto %></li>
				<li id="seg-pais"><% segs.encabezado.pais %></li>
				<li id="seg-reservado">0000</li><!-- 4 ceros -->
				<li id="seg-clave-usuario">**********</li><!-- 10 chars -->
				<li id="seg-password-usuario">********</li><!-- 8 chars -->
				<li id="seg-responsabilidad"><% segs.encabezado.responsabilidad %></li>
				<li id="seg-tipo_contrato"><% segs.encabezado.tipo_contrato %></li>
				<li id="seg-tipo_moneda"><% segs.encabezado.tipo_moneda %></li>
				<li id="seg-importe">000000000</li>
				<li id="seg-idioma"><% segs.encabezado.idioma %></li>
				<li id="seg-tipo_salida"><% segs.encabezado.tipo_salida %></li>
				<li id="seg-tamanio-bloque"> </li><!-- 1 espacio -->
				<li id="seg-impresora">    </li><!-- 4 espacios -->
				<li id="seg-reservado">0000000</li><!-- 7 ceros -->
			</ul>
			<hr>
		</section>
		<section id="encabezado_settings">
			<div class="form-group">
				<label for="etiqueta_seg_encabezado">INTL - ETIQUETA DEL SEGMENTO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-etiqueta-encabezado">?</span></label>
				<div id="tip-etiqueta-encabezado" class="collapse">
					<p>Debe contener las letras INTL</p>
				</div>
				<input type="text" id="etiqueta_seg_encabezado" name="etiqueta_seg_encabezado" class="form-control" value="INTL" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 4 caracteres | Alfabético | Posición 1-4</small>
			</div>

			<div class="form-group">
				<label for="encabezado_version">VERSIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-encabezado-version">?</span></label>
				<div id="tip-encabezado-version" class="collapse">
					<p>Indica la versión del formato de registro de consulta. INTL13.</p>
				</div>
				<input type="text" id="encabezado_version" name="encabezado_version" class="form-control" value="13" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 2 caracteres | Numérico | Posición 5-6</small>
			</div>

			<div class="form-group">
				<label for="num_referencia">Referencia Interna <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-ref-interna">?</span></label>
				<div id="tip-ref-interna" class="collapse">
					<p>
						Reportar una referencia para identificar la consulta, si la referencia es menor de 25
						caracteres, incluir espacios o blancos a la izquierda hasta tener 25 caracteres.
						Si no requiere indicar una referencia, colocar 25 espacios o blancos.
					</p>
				</div>
				<input type="text" id="num_referencia" name="num_referencia" class="form-control" ng-model="segs.encabezado.referencia" maxlength="25">
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 25 caracteres | Numérico | Posición 7-31</small>
			</div>

			<div class="form-group">
				<label for="encabezado_producto">Clave del Producto <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-clave-producto">?</span></label>
				<div id="tip-clave-producto" class="collapse">
					<p>
						Se debe indicar la Clave del producto que se requiere, puede ser solo el “Informe
						Buró” o puede agregarse la calificación de “BC-Score, Score Buró Tarjeta de
						Crédito” o “Score Buró Microfinancieras”.
						Para solicitar alguno de los productos Score, la “Clave del Usuario” debe tener el
						privilegio previamente contratado.<br>
						<strong>Para solicitar BC Score, la clave de acceso debe contar con el privilegio 
						previamente contratado por el Usuario.</strong>
					</p>
				</div>
				<select id="encabezado_producto" name="encabezado_producto" class="form-control" ng-model="segs.encabezado.producto">
					<option value="107">107 - BC Score</option>
					<option value="501">501 - Informe Buró</option>
					<option value="504">504 - Informe Buró con Índice de Capacidad Crediticia (ICC)</option>
					<option value="507">507 - Informe Buró con BC-Score recalibrado (2008)</option>
					<option value="509">509 - Informe Buró con Score Buró MICRO</option>
					<option value="510">510 - Informe Buró con Score Buró TDC</option>
					<option value="511">511 - Informe Buró con Score Buró TDC más Índice de Capacidad Crediticia ICC</option>
					<option value="512">512 - Informe Buró con Score Buró TDC en caso de exclusión devuelve BC Score</option>
					<option value="513">513 - Informe Buró con Score Buró TDC con ICC en caso de Exclusión devuelve BC Score con ICC</option>
					<option value="540">540 - Informe Buró con BC Score con Score Buró TDC</option>
					<option value="541">541 - Informe Buró con BC Score con Score Buró MICRO</option>
					<option value="542">542 - Informe Buró con Score Buró TDC con Score Buró MICRO</option>
					<option value="545">545 - Informe Buró con BC Score más Score Buró TDC con Índice de Capacidad Crediticia ICC</option>
					<option value="546">546 - Informe Buró con Score Buró TDC con Índice de Capacidad Crediticia ICC más Score Buró MICRO</option>
					<option value="548">548 - Informe Buró con BC Score con Índice de Capacidad Crediticia más Score Buró MICRO</option>
				</select>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 25 caracteres | Numérico | Posición 32-34</small>
			</div>

			<div class="form-group">
				<label for="encabezado_pais">Clave del País <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-clave-pais">?</span></label>
				<div id="tip-clave-pais" class="collapse">
					<p>Deberá contener las letras MX</p>
				</div>
				<select id="encabezado_pais" name="encabezado_pais" class="form-control" ng-model="segs.encabezado.pais">
					<option value="MX">MX</option>
				</select>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 2 caracteres | Alfabético | Posición 35-36</small>
			</div>

			<div class="form-group">
				<label for="encabezado_reservado">RESERVADO</label>
				<input type="text" id="encabezado_reservado" name="encabezado_reservado" class="form-control" value="0000" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 4 caracteres | Numbers | posición 37-40</small>
			</div>

			<div class="form-group">
				<label for="encabezado_clave_usuario">CLAVE DEL USUARIO o MEMBER CODE <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-clave-usuario">?</span></label>
				<div id="tip-clave-usuario" class="collapse">
					<p>
						Contiene la clave única del Usuario para consulta, la cual fue asignada por Buró de Crédito.
						Esta clave está formada como sigue:
						<ul>
							<li>
									Las primeras 2 posiciones son alfabéticas y corresponden a la
									clave de Tipo de Negocio o KOB (Kind of Business) del
									Usuario
							</li>
							<li>
									Los siguientes 4 números identifica al Usuario.
							</li>
							<li>
									Los últimos 4 números puede identificar productos, sucursales o área interna del Usuario.
							</li>
						</ul>
					</p>
				</div>
				<input type="text" id="encabezado_clave_usuario" name="encabezado_clave_usuario" class="form-control" value="" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 10 caracteres | Alfanumérico | Posición 41-50</small>
			</div>
			<div class="form-group">
				<label for="encabezado_password_usuario">CONTRASEÑA O PASSWORD DE ACCESO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-password-usuario">?</span></label>
				<div id="tip-password-usuario" class="collapse">
					<p>
						Incluir la “Contraseña” o “Password” cifrada o encriptada.
						Para obtener y dar mantenimiento a esta contraseña o password, seguir las
						indicaciones del ANEXO 13 “MANTENIMIENTO A CONTRASEÑA O
						PASSWORD”.
					</p>
				</div>
				<input type="text" id="encabezado_password_usuario" name="encabezado_password_usuario" class="form-control" value="" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 8 caracteres | Alfanumérico | Posición 51-58</small>
			</div>

			<div class="form-group">
				<label for="responsabilidad">Tipo de Responsabilidad <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-responsabilidad">?</span></label>
				<div id="tip-responsabilidad" class="collapse">
					<p>Indicar si la solicitud del Informe Buró es para un crédito individual o mancomunado.</p>
				</div>
				<select id="responsabilidad" name="responsabilidad" class="form-control" ng-model="segs.encabezado.responsabilidad">
					<option value="A">A - Usuario Autorizado (Adicional)</option>
					<option value="I">I - Individual</option>
					<option value="J">J - Mancomunado</option>
				</select>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 1 caracter | Alfabético | Posición 59</small>
			</div>

			<div class="form-group">
				<label for="tipo_contrato">Tipo de Contrato o Producto <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-tipo-contrato">?</span></label>
				<div id="tip-tipo-contrato" class="collapse">
					<p>Indicar el producto de la solicitud del Cliente.</p>
				</div>
				<select id="tipo_contrato" name="tipo_contrato" class="form-control" ng-model="segs.encabezado.tipo_contrato">
					<option value="AF">AF - Aparatos / Muebles</option>
					<option value="AG">AG - Agropecuario (PFAE)</option>
					<option value="AL">AL - Arrendamiento Automotriz</option>
					<option value="AP">AP - Aviación</option>
					<option value="AU">AU - Compra de Automóvil</option>
					<option value="BD">BD - Fianza</option>
					<option value="BT">BT - Bote / Lancha</option>
					<option value="CC">CC - Tarjeta de Crédito</option>
					<option value="CE">CE - Cartas de Crédito (PFAE)</option>
					<option value="CF">CF - Crédito Fiscal</option>
					<option value="CL">CL - Linea de Crédito</option>
					<option value="CO">CO - Consolidación</option>
					<option value="CS">CS - Crédito Simple (PFAE)</option>
					<option value="CT">CT - Con Colateral (PFAE)</option>
					<option value="DE">DE - Descuentos (PFAE)</option>
					<option value="EQ">EQ - Equipo</option>
					<option value="FI">FI - Fideicomiso (PFAE)</option>
					<option value="FT">FT - Factoraje</option>
					<option value="HA">HA - Habilitación o Avio (PFAE)</option>
					<option value="HE">HE - Préstamo tipo "Home Equity"</option>
					<option value="HI">HI - Mejoras a la casa</option>
					<option value="LS">LS - Arrendamiento</option>
					<option value="LR">LR - Línea de Crédito Reinstalable</option>
					<option value="MI">MI - Otros</option>
					<option value="OA">OA - Otros adeudos vencidos (PFAE)</option>
					<option value="PA">PA - Préstamo para Personas Físicas con Actividades Empresariales (PFAE)</option>
					<option value="PB">PB - Editorial</option>
					<option value="PG">PG - PGUE - Préstamo como garantía de unidades industriales para PFAE</option>
					<option value="PL">PL - Préstamo Personal</option>
					<option value="PN">PN - Préstamo de nómina</option>
					<option value="PQ">PQ - Quirografario (PFAE)</option>
					<option value="PR">PR - Prendario (PFAE)</option>
					<option value="PS">PS - Pago de Servicios</option>
					<option value="RC">RC - Reestructurado (PFAE)</option>
					<option value="RD">RD - Redescuento (PFAE)</option>
					<option value="RE">RE - Bienes Raíces</option>
					<option value="RF">RF - Refaccionario (PFAE)</option>
					<option value="RN">RN - Renovado (PFAE)</option>
					<option value="RV">RV - Vehículo Recreativo</option>
					<option value="SC">SC - Tarjeta Garantizada</option>
					<option value="SE">SE - Préstamo Garantizado</option>
					<option value="SG">SG - Seguros</option>
					<option value="SM">SM - Segunda Hipoteca</option>
					<option value="ST">ST - Préstamo para estudiante</option>
					<option value="TE">TE - Tarjeta de Crédito Empresarial</option>
					<option value="UK">UK - Desconocido</option>
					<option value="US">US - Préstamo no garantizado</option>
				</select>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 2 caracteres | Alfabético | Posición 60-61</small>
			</div>

			<div class="form-group">
				<label for="tipo_moneda">Moneda del crédito <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-tipo-moneda">?</span></label>
				<div id="tip-tipo-moneda" class="collapse">
					<p>Indica la moneda del crédito que solicita el Cliente.</p>
				</div>
				<select id="tipo_moneda" name="tipo_moneda" class="form-control" ng-model="segs.encabezado.tipo_moneda">
					<option value="MX">MX - Pesos Mexicanos</option>
					<option value="N$">N$ - Pesos Mexicanos</option>
					<option value="UD">UD - Unidades de Inversión (UDI’s)</option>
					<option value="US">US - Dólares Americanos</option>
				</select>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 2 caracteres | Alfabético | Posición 62-63</small>
			</div>

			<div class="form-group">
				<label for="encabezado_importe_contrato">IMPORTE DEL CONTRATO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-encabezado-importe_contrato">?</span></label>
				<div id="tip-encabezado-importe_contrato" class="collapse">
					<p>Reportar 9 ceros.</p>
				</div>
				<input type="text" id="encabezado_importe_contrato" name="encabezado_importe_contrato" class="form-control" value="000000000" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 9 caracteres | Numérico | Posición 64-72</small>
			</div>
			
			<div class="form-group">
				<label for="idioma">Idioma <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-idioma">?</span></label>
				<div id="tip-idioma" class="collapse">
					<p>Indica el idioma en que deberá emitirse los informes de Crédito del archivo</p>
				</div>
				<select id="idioma" name="idioma" class="form-control" ng-model="segs.encabezado.idioma">
					<option value="SP">SP - Español</option>
					<option value="EN">EN - Inglés</option>
				</select>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 2 caracteres | Alfabético | Posición 73-74</small>
			</div>

			<div class="form-group">
				<label for="tipo_salida">Tipo de salida <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-tipo-salida">?</span></label>
				<div id="tip-tipo-salida" class="collapse">
					<p>Indica el tipo de formato en el que se entregarán los Informes Buró</p>
				</div>
				<select id="tipo_salida" name="tipo_salida" class="form-control" ng-model="segs.encabezado.tipo_salida">
					<option value="01">01 - Archivo de cadena de datos</option>
					<option value="02">02 - Formato para Impresión</option>
					<option value="03">03 - Ambos</option>
				</select>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 2 caracteres | Numérico | Posición 75-76</small>
			</div>

			<div class="form-group">
				<label for="encabezado_bloque_registro">TAMAÑO DEL BLOQUE DEL REGISTRO DE RESPUESTA <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-bloque-registro">?</span></label>
				<div id="tip-bloque-registro" class="collapse">
					<p>Reportar 1 blanco o espacio.</p>
				</div>
				<input type="text" id="encabezado_bloque_registro" name="encabezado_bloque_registro" class="form-control" value=" " disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 1 caracter | Alfabético | Posición 77</small>
			</div>

			<div class="form-group">
				<label for="encabezado_id_impresora">IDENTIFICACIÓN DE LA IMPRESORA <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-id-impresora">?</span></label>
				<div id="tip-id-impresora" class="collapse">
					<p>Deberá reportarse 4 espacios o blancos.</p>
				</div>
				<input type="text" id="encabezado_id_impresora" name="encabezado_id_impresora" class="form-control" value="    " disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 4 caracteres | Alfanumérico | Posición 78-81</small>
			</div>
			
			<div class="form-group">
				<label for="encabezado_uso_futuro">RESERVADO PARA USO FUTURO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-uso-futuro">?</span></label>
				<div id="tip-uso-futuro" class="collapse">
					<p>Reportar siete ceros.</p>
				</div>
				<input type="text" id="encabezado_uso_futuro" name="encabezado_uso_futuro" class="form-control" value="0000000" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 7 caracteres | Numérico | Posición 82-88</small>
			</div>
		</section>
  </div>
</div>
