@extends('crm.app')
@section('content')
<div class="container" style="width:100%"> 
	<div id="cp-console-container" class="content">
		<div class="row">
			<div class="col-sm-4">
				<div class="title">
					<h2>CP CONSOLE</h2>
				</div>
			</div>
			<div class="col-sm-4">
				<form id="update-cp-form" class="form">
					{!! csrf_field() !!}
					<div class="form-group">
						<label for="update-estado">Estado</label>
						<select id="update-estado" name="estado" class="form-control">
							<option value=""></option>
							<option value="Aguascalientes">Aguascalientes</option>
							<option value="Baja California Sur">Baja California Sur</option>
							<option value="Baja California">Baja California</option>
							<option value="Campeche">Campeche</option>
							<option value="Chiapas">Chiapas</option>
							<option value="Chihuahua">Chihuahua</option>
							<option value="Distrito Federal">Distrito Federal</option>
							<option value="Coahuila de Zaragoza">Coahuila de Zaragoza</option>
							<option value="Colima">Colima</option>
							<option value="Durango">Durango</option>
							<option value="Guanajuato">Guanajuato</option>
							<option value="Guerrero">Guerrero</option>
							<option value="Hidalgo">Hidalgo</option>
							<option value="Jalisco">Jalisco</option>
							<option value="México">México</option>
							<option value="Michoacán de Ocampo">Michoacán de Ocampo</option>
							<option value="Morelos">Morelos</option>
							<option value="Nayarit">Nayarit</option>
							<option value="Nuevo León">Nuevo León</option>
							<option value="Oaxaca">Oaxaca</option>
							<option value="Puebla">Puebla</option>
							<option value="Querétaro">Querétaro</option>
							<option value="Quintana Roo">Quintana Roo</option>
							<option value="San Luis Potosí">San Luis Potosí</option>
							<option value="Sinaloa">Sinaloa</option>
							<option value="Sonora">Sonora</option>
							<option value="Tabasco">Tabasco</option>
							<option value="Tamaulipas">Tamaulipas</option>
							<option value="Tlaxcala">Tlaxcala</option>
							<option value="Veracruz de Ignacio de la Llave">Veracruz de Ignacio de la Llave</option>
							<option value="Yucatán">Yucatán</option>
							<option value="Zacatecas">Zacatecas</option>
						</select>
					</div>
				</form>
			</div>
			<div class="col-sm-4">
				<button id="update-cp-submit-btn" class="btn btn-default" style="margin-top:23px;">Obtener CPs</button>
				<button id="update-cp-submit-btn-loading" class="btn btn-default hidden" style="margin-top:23px;">Cargando...</button>
			</div>
		</div>
		<div class="panel panel-default">
		  <div class="panel-heading">
		  	<div class="row">
		  		<div class="col-sm-8">
			    	<h3 id="cp-panel-title" style="margin-top:5px;"></h3>
			    </div>
			    <div class="col-sm-4">
			    	<button id="run-cp-update-btn" class="btn btn-warning btn-sm pull-right hidden">Actualizar Todos</button>
			    </div>
			  </div>
		  </div>
		  <div class="panel-body">
		    <table class="table table-striped">
		    	<tbody id="cp-table-body"></tbody>
		    </table>
		  </div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="progress-modal" tabindex="-1" role="dialog" aria-labelledby="progress-modal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="progress-modal-title" style="width:80%;float:left;">Procesando Lista</h3>
        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="progress">
				  <div class="progress-bar" role="progressbar" style="width:0%"></div>
				</div>
				<div id="current_process_description">

				</div>
				<br>
				<p class="text-center">
					Favor de esperar hasta que el proceso termine.
				</p>
				<p>
					<strong>Nuevos Códigos Postales:</strong><br>
				</p>
				<div id="new_cps_added" style="max-height:150px;overflow:auto;">
				</div>
      </div>
      <div class="modal-footer">
        <button id="cancel-process-list" type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      	<button id="process-list-ok" type="button" class="btn btn-primary hidden" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
@endsection