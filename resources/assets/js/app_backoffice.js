
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import SweetAlert from 'sweetalert2'
import VueFormGenerator from "vue-form-generator";

window.Vue = require('vue');
window.swal = require('sweetalert2');

Vue.use(VueFormGenerator);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('panel', require('./components/Panel.vue'));
Vue.component('detalle_prospecto', require('./components/DetalleProspecto.vue'));
Vue.component('registro', require('./components/Registro.vue'));
Vue.component('cliente_estrella', require('./components/ClienteEstrella.vue'));
Vue.component('dentalia', require('./components/Dentalia.vue'));
Vue.component('bedu', require('./components/Bedu.vue'));
Vue.component('detalle_solicitud', require('./components/DetalleSolicitud'));

Vue.component('pagination', require('laravel-vue-pagination'));

const app = new Vue({
    el: '#app',
});
