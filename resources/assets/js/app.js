
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import SweetAlert from 'sweetalert2';
import VuePersist from 'vue-persist';
import VueFormGenerator from "vue-form-generator";
import Vuelidate from 'vuelidate';
import Tooltip from 'vue-directive-tooltip';
import {VMoney} from 'v-money';

window.Vue = require('vue');
window.swal = require('sweetalert2');

Vue.use(VueFormGenerator);
Vue.use(Tooltip);
Vue.use(Vuelidate);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('registro', require('./components/Registro.vue'));
Vue.component('validacion_usuario', require('./components/ValidacionUsuario.vue'));
Vue.component('datos_domicilio', require('./components/DatosDomicilio.vue'));
Vue.component('datos_personales', require('./components/DatosPersonales.vue'));
Vue.component('datos_buro', require('./components/DatosBuro.vue'));
Vue.component('datos_adicionales', require('./components/DatosAdicionales.vue'));
Vue.component('datos_ingreso', require('./components/Datosingreso.vue'));
Vue.component('cuestionario_dinamico', require('./components/CuestionarioDinamico.vue'));
Vue.component('decision', require('./components/Decision.vue'));

Vue.use(VuePersist, {
  name: 'registro',
  expiration: 300000 * 6 // 30 minutos
});

const app = new Vue({
    el: '#app',
    data: {
      registro: false,
      monto: null,
      plazo: null,
      finalidad: null,
      finalidad_custom: null,
      celular: null,
    },
    persist: ['plazo', 'monto', 'finalidad_custom','celular']
});
