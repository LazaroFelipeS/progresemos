
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import LoadScript from 'vue-plugin-load-script';

Vue.use(LoadScript);

Vue.loadScript("/js/jquery/webapp/jquery-3.2.1.slim.min.js")
Vue.loadScript("/js/jquery/webapp/popper.min.js")
Vue.loadScript("/js/jquery/webapp/bootstrap4.0.min.js")
Vue.loadScript("/js/jquery/webapp/axios.min.js")
Vue.loadScript("/js/jquery.magnific-popup.js")
Vue.loadScript("/js/refactor/modals.js")
Vue.component('facematch_paso1', require('./components/FacematchPaso1.vue'));

const app = new Vue({
    el: '#app',
});
