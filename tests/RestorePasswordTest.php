<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Http\Controllers\RestorePasswordController;
use Illuminate\Http\Request;

class ExampleTest extends TestCase
{
    public function setUp() {

        parent::setUp();

        $this->mockCurlCaller = $this->getMockBuilder('App\Repositories\CurlCaller')
            ->getMock();

        $this->mockLDAPRepository = $this->getMockBuilder('App\Repositories\LDAPRepository')
            ->setConstructorArgs([$this->mockCurlCaller])
            ->getMock();
    }

    /**
     * Prueba del servicio de envio de email de restauración de contraseña LDAP
     * El correo se envía exitosamente
     */
    public function testRestorePasswordLDAPOk() {

        $fakeResponse = json_encode([
            'code'    => '00',
            'message' => 'Operacion Exitosa.'
        ]);

        $this->mockCurlCaller->expects($this->any())
            ->method('callCurlLDAP')
            ->willReturn($fakeResponse);

        $request = [
            'correo' => 'maza_urbano@hotmail.com'
        ];

        $LDAPRepository = new App\Repositories\LDAPRepository($this->mockCurlCaller);
        $response = $LDAPRepository->restorePassword($request);
    }

    /**
     * Prueba del servicio de envio de email de restauración de contraseña LDAP
     * No existe un usuario asociado con el correo proporcionado.
     */
    public function testRestorePasswordLDAPNotUserFound() {

        $fakeResponse = json_encode([
            'code'    => '181',
            'message' => 'Error, No existe un usuario asociado al correo proporcionado.'
        ]);

        $this->mockCurlCaller->expects($this->any())
            ->method('callCurlLDAP')
            ->willReturn($fakeResponse);

        $request = [
            'correo' => 'testmailmock@hotmail.com'
        ];

        $LDAPRepository = new App\Repositories\LDAPRepository($this->mockCurlCaller);
        $response = $LDAPRepository->restorePassword($request);
    }

    /**
     * Prueba del servicio de envio de email de restauración de contraseña LDAP.
     * No hay respuesta por parte de LDAP
     */
    public function testRestorePasswordLDAPNotLDAPResponse() {

        $fakeResponse = [];

        $this->mockCurlCaller->expects($this->any())
            ->method('callCurlLDAP')
            ->willReturn($fakeResponse);

        $request = [
            'correo' => 'testmailmock@hotmail.com'
        ];

        $LDAPRepository = new App\Repositories\LDAPRepository($this->mockCurlCaller);
        $response = $LDAPRepository->restorePassword($request);
    }

    /**
     * Prueba de la función de envio de email de restauración de contraseña.
     * Correo de restauración enviado exitosamente.
     */
    public function testEmailPasswordRestoreOk()
    {
        $fakeResponse = json_encode([
            'success'   => true,
            'message'   => 'Email de restauración de contraseña enviado',
            'correo'    => 'maza_urbano@hotmail.com',
            'resultado' => [
                'code'      => '00',
                'message'   => 'Operacion Exitosa.'
            ]
        ]);

        $this->mockLDAPRepository->expects($this->once())
            ->method('restorePassword')
            ->willReturn($fakeResponse);

        $request = new Request();
        $request->merge(['correo' => 'maza_urbano@hotmail.com']);

        $restorePassword = new RestorePasswordController($this->mockLDAPRepository);
        $response = $restorePassword->emailPasswordRestore($request);

    }

    /**
     * Prueba de la función de envio de email de restauración de contraseña.
     * Hubo un problema al enviar el correo de restauración.
     */
    public function testEmailPasswordRestoreFail()
    {
        $fakeResponse = json_encode([
            'success'   => false,
            'resultado' => [
                'code'      => '181',
                'message'   => 'Error, No existe un usuario asociado al correo proporcionado.'
            ]
        ]);

        $this->mockLDAPRepository->expects($this->once())
            ->method('restorePassword')
            ->willReturn($fakeResponse);

        $request = new Request();
        $request->merge(['correo' => 'maza_urbano@hotmail.com']);

        $restorePassword = new RestorePasswordController($this->mockLDAPRepository);
        $response = $restorePassword->emailPasswordRestore($request);

    }

    /**
     * Prueba de la función de envio de email de restauración de contraseña.
     * El correo es inválido.
     */
    public function testEmailPasswordRestoreErrors()
    {

        $request = new Request();
        $request->merge(['correo' => 'testmailmock']);

        $restorePassword = new RestorePasswordController($this->mockLDAPRepository);
        $response = $restorePassword->emailPasswordRestore($request);

    }

    /**
     * Prueba del servicio de login user LDAP
     * El login es correcto
     */
    public function testLoginUserLDAPOk() {

        $fakeResponse = json_encode([
            'code'      => '00',
            'message'   => 'Operacion Exitosa.',
            'user'      => [
                'idUser'        => 'testmailmock@hotmail.com',
                'name'          => 'TEST',
                'lastName'      => 'TEST TEST',
                'fullName'      => 'TEST TEST TEST',
                'idUserT24'     => '0',
                'phoneNumber'   => '5555555555'
            ],
            'token'     => [
                'token'             => 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtYXphX3VyYmFub0Bob3RtYWlsLmNvbTExLzA3LzIwMTggMTQ6MDA6MTguNzIxIn0.xibj2Autj6GqND_bS1gG13_XzDRhH7w1aYaFjwcGeUrDpJQl_mSCEV18XB_Xc5Itq1JjDD6_HgaAdvDEG1av_w',
                'timeWhenGenerated' => '11/07/2018 14:00:18.721'
            ]
        ]);

        $this->mockCurlCaller->expects($this->any())
            ->method('callCurlLDAP')
            ->willReturn($fakeResponse);

        $request = [
            'correo'    => 'testmailmock@hotmail.com',
            'password'  => 'XdFA1290'
        ];

        $LDAPRepository = new App\Repositories\LDAPRepository($this->mockCurlCaller);
        $response = $LDAPRepository->loginUserLDAP($request);
    }


    /**
     * Prueba de la función de validar el password temporal.
     * La longitud no es la correcta
     */
    public function testValidateTemporaryPasswordFail()
    {
        $request = new Request();
        $request->merge([
            'correo_pt' => 'testmailmock@hotmail.com',
            'password_temporal' => 'ABCD12456770'
        ]);

        $restorePassword = new RestorePasswordController($this->mockLDAPRepository);
        $response = $restorePassword->validateTemporaryPassword($request);

    }

    /**
     * Prueba de la función de validar el password temporal.
     * El login en LDAP con la contraseña temporal es correcto
     */
    public function testValidateTemporaryPasswordOk()
    {
        $fakeResponse = json_encode([
            'success'   => true,
            'message'   => 'Usuario logueado',
            'resultado' => [
                'code'      => '00',
                'message'   => 'Operacion Exitosa.',
                'user'      => [
                    'idUser'        => 'testmailmock@hotmail.com',
                    'name'          => 'TEST',
                    'lastName'      => 'TEST TEST',
                    'fullName'      => 'TEST TEST TEST',
                    'idUserT24'     => '0',
                    'phoneNumber'   => '5555555555'
                ],
                'token'     => [
                    'token'             => 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtYXphX3VyYmFub0Bob3RtYWlsLmNvbTExLzA3LzIwMTggMTQ6MDA6MTguNzIxIn0.xibj2Autj6GqND_bS1gG13_XzDRhH7w1aYaFjwcGeUrDpJQl_mSCEV18XB_Xc5Itq1JjDD6_HgaAdvDEG1av_w',
                    'timeWhenGenerated' => '11/07/2018 14:00:18.721'
                ]
            ],
        ]);

        $this->mockLDAPRepository->expects($this->once())
            ->method('loginUserLDAP')
            ->willReturn($fakeResponse);

        $request = new Request();
        $request->merge([
            'correo_pt'         => 'testmailmock@hotmail.com',
            'password_temporal' => 'vZ9GqSlm'
        ]);

        $restorePassword = new RestorePasswordController($this->mockLDAPRepository);
        $response = $restorePassword->validateTemporaryPassword($request);

    }

    /**
     * Prueba de la función de validar el password temporal.
     * El login en LDAP con la contraseña temporal es incorrecto
     */
    public function testValidateTemporaryPasswordNotOk()
    {
        $fakeResponse = json_encode([
            'success'   => false,
            'code'      => 21,
            'message'   => 'Usuario o password son incorrectos.',
        ]);

        $this->mockLDAPRepository->expects($this->once())
            ->method('loginUserLDAP')
            ->willReturn($fakeResponse);

        $request = new Request();
        $request->merge([
            'correo_pt'         => 'testmailmock@hotmail.com',
            'password_temporal' => 'vZ9GqSlm'
        ]);

        $restorePassword = new RestorePasswordController($this->mockLDAPRepository);
        $response = $restorePassword->validateTemporaryPassword($request);

    }

    /**
     * Prueba del servicio de actualizar password LDAP
     * El resultado de actualizar contraseña es correcto
     */
    public function testUpdatePasswordLDAP() {

        $fakeResponse = json_encode([
            'code'    => '00',
            'message' => 'Operacion Exitosa.'
        ]);

        $this->mockCurlCaller->expects($this->any())
            ->method('callCurlLDAP')
            ->willReturn($fakeResponse);

        $request = [
            'idUser'            => 'testmailmock@hotmail.com',
            'name'              => 'TEST',
            'lastName'          => 'TEST TEST',
            'fullName'          => 'TEST TEST TEST',
            'idUserT24'         => '0',
            'phoneNumber'       => '5555555555',
            'token'             => 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtYXphX3VyYmFub0Bob3RtYWlsLmNvbTExLzA3LzIwMTggMTQ6MDA6MTguNzIxIn0.xibj2Autj6GqND_bS1gG13_XzDRhH7w1aYaFjwcGeUrDpJQl_mSCEV18XB_Xc5Itq1JjDD6_HgaAdvDEG1av_w',
            'timeWhenGenerated' => '11/07/2018 14:00:18.721',
            'app'               => false,
            'new_password'       => 'Abcd1234'
        ];

        $LDAPRepository = new App\Repositories\LDAPRepository($this->mockCurlCaller);
        $response = $LDAPRepository->updatePassword($request);
    }

    /**
     * Prueba de la función de actualizar contraseña.
     * El password y confirm password son diferentes
     */
    public function testUpdatePasswordFail()
    {
        $request = new Request();
        $request->merge([
            'idUser'            => 'testmailmock@hotmail.com',
            'name'              => 'TEST',
            'lastName'          => 'TEST TEST',
            'fullName'          => 'TEST TEST TEST',
            'idUserT24'         => '0',
            'phoneNumber'       => '5555555555',
            'token'             => 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtYXphX3VyYmFub0Bob3RtYWlsLmNvbTExLzA3LzIwMTggMTQ6MDA6MTguNzIxIn0.xibj2Autj6GqND_bS1gG13_XzDRhH7w1aYaFjwcGeUrDpJQl_mSCEV18XB_Xc5Itq1JjDD6_HgaAdvDEG1av_w',
            'timeWhenGenerated' => '11/07/2018 14:00:18.721',
            'new_password'      => 'Abcd1234',
            'confirm_password'  => 'abcd1234',
        ]);

        $restorePassword = new RestorePasswordController($this->mockLDAPRepository);
        $response = $restorePassword->passwordUpdate($request);

    }

    /**
     * Prueba de la función de actualizar contraseña.
     * El resultado de actualizar la contraseña es exitoso.
     */
    public function testUpdatePasswordOk()
    {
        $fakeResponse = json_encode([
            'success'   => true,
            'message'   => 'Contraseña actualizada con éxito',
            'resultado' => [
                'code'      => '00',
                'message'   => 'Operacion Exitosa.'
            ]
        ]);

        $this->mockLDAPRepository->expects($this->once())
            ->method('updatePassword')
            ->willReturn($fakeResponse);

        $request = new Request();
        $request->merge([
            'idUser'            => 'maza_urbano@hotmail.com',
            'name'              => 'TEST',
            'lastName'          => 'TEST TEST',
            'fullName'          => 'TEST TEST TEST',
            'idUserT24'         => '0',
            'phoneNumber'       => '5555555555',
            'token'             => 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtYXphX3VyYmFub0Bob3RtYWlsLmNvbTExLzA3LzIwMTggMTQ6MDA6MTguNzIxIn0.xibj2Autj6GqND_bS1gG13_XzDRhH7w1aYaFjwcGeUrDpJQl_mSCEV18XB_Xc5Itq1JjDD6_HgaAdvDEG1av_w',
            'timeWhenGenerated' => '11/07/2018 14:00:18.721',
            'new_password'      => 'Abcd1234',
            'confirm_password'  => 'Abcd1234',
        ]);

        $restorePassword = new RestorePasswordController($this->mockLDAPRepository);
        $response = $restorePassword->passwordUpdate($request);

    }

    /**
     * Prueba de la función de actualizar contraseña.
     * El resultado de actualizar la contraseña no es exitoso.
     */
    public function testUpdatePasswordNotOk()
    {
        $fakeResponse = json_encode([
            'success'   => false,
            'code'      => 27,
            'message'   => 'El token es incorrecto.',
        ]);

        $this->mockLDAPRepository->expects($this->once())
            ->method('updatePassword')
            ->willReturn($fakeResponse);

        $request = new Request();
        $request->merge([
            'idUser'            => 'maza_urbano@hotmail.com',
            'name'              => 'TEST',
            'lastName'          => 'TEST TEST',
            'fullName'          => 'TEST TEST TEST',
            'idUserT24'         => '0',
            'phoneNumber'       => '5555555555',
            'token'             => 'token',
            'timeWhenGenerated' => '11/07/2018 14:00:18.721',
            'new_password'      => 'Abcd1234',
            'confirm_password'  => 'Abcd1234',
        ]);

        $restorePassword = new RestorePasswordController($this->mockLDAPRepository);
        $response = $restorePassword->passwordUpdate($request);

    }

    public function tearDown(){
        parent::tearDown();
        Mockery::close();
    }

}
