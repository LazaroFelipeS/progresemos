function openDropdown(obj) {
   var parent = $(obj).closest('.my-dropdown');
   var ul = parent.find('ul');
   if(parent.hasClass('closed')) {
      parent.removeClass('closed');
      $(obj).closest('.my-dropdown__content').css('background','transparent url(/images/icons/up_arrow.png) 98% center no-repeat');
      $(ul).removeClass('dropdownhidden-scroll');
		$(ul).addClass('dropdownvisible-scroll');
   } else {
      parent.addClass('closed');
      $(obj).closest('.my-dropdown__content').css('background','transparent url(/images/icons/down_arrow.png) 98% center no-repeat');
      $(ul).addClass('dropdownhidden-scroll');
		$(ul).removeClass('dropdownvisible-scroll');
   }
};
