<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BCScores extends Model
{
    protected $table = 'bc_scores';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'bc_score',
        'exclusion',
        'bc_razon1',
        'bc_razon2',
        'bc_razon3',
        'bc_error',
        'icc_score',
        'icc_exclusion',
        'icc_razon1',
        'icc_razon2',
        'icc_razon3',
        'icc_error',
        'micro_valor',
        'micro_razon1',
        'micro_razon2',
        'micro_razon3',
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
