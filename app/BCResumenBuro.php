<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BCResumenBuro extends Model
{
    protected $table = 'bc_resumen';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'resumen_fecha_integracion',
        'resumen_cuentas_mop7',
        'resumen_cuentas_mop6',
        'resumen_cuentas_mop5',
        'resumen_cuentas_mop4',
        'resumen_cuentas_mop3',
        'resumen_cuentas_mop2',
        'resumen_cuentas_mop1',
        'resumen_cuentas_mop0',
        'resumen_cuentas_mop_ur',
        'resumen_numero_de_cuentas',
        'resumen_cuentas_pagos_fijos_hipo',
        'resumen_cuentas_revolventes_sin_limite',
        'resumen_cuentas_cerradas',
        'resumen_cuentas_con_morosidad_actual',
        'resumen_cuentas_con_historial_de_morosidad',
        'resumen_cuentas_en_aclaracion',
        'resumen_solicitudes_de_consulta',
        'resumen_nueva_direccion_ult_60_dias',
        'resumen_mensaje_de_alerta',
        'resumen_declarativa',
        'resumen_moneda_del_credito',
        'resumen_total_creditos_max_rev_sinlim',
        'resumen_total_limites_de_credito_rev_sinlim',
        'resumen_total_saldos_actuales_rev_sinlim',
        'resumen_total_saldos_vencidos_rev_sinlim',
        'resumen_total_importe_de_pago_rev_sinlim',
        'resumen_porcentaje_limite_de_credito_utilizado_rev_sinlim',
        'resumen_total_creditos_max_fijos_hipo',
        'resumen_total_saldos_actuales_fijos_hipo',
        'resumen_total_saldos_vencidos_fijos_hipo',
        'resumen_total_importe_de_pago_fijos_hipo',
        'resumen_cuentas_mop96',
        'resumen_cuentas_mop97',
        'resumen_cuentas_mop99',
        'resumen_fecha_apertura_cuenta_antigua',
        'resumen_fecha_apertura_cuenta_reciente',
        'resumen_num_solicitudes_informe_buro',
        'resumen_fecha_consulta_reciente',
        'resumen_num_cuentas_despacho_cobranza',
        'resumen_fecha_apertura_cuenta_en_despacho_reciente',
        'resumen_num_solicitudes_informe_por_despachos',
        'resumen_fecha_consulta_por_despacho_reciente',
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
