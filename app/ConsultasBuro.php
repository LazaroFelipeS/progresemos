<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsultasBuro extends Model
{
    protected $table = 'consultas_buro';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'orden',
        'tipo',
        'cadena_original',
        'folio_consulta',
        'fecha_consulta',
        'no_consulta',
        'INTL',
        'PN',
        'PA',
        'PE',
        'TL',
        'IQ',
        'RS',
        'HI',
        'HR',
        'CR',
        'SC',
        'ERRR',
        'ES',
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
