<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Situacion extends Model
{
    protected $table = 'situaciones';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Obtiene el cuestionario ligado a la solicitud
     *
     * @return object Datos del cuestionario
     */
    public function cuestionario() {
        return $this->hasMany('App\CuestionarioDinamico');
    }
}
