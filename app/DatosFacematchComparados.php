<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla datos_
 */
class DatosFacematchComparados extends Model
{
    protected $table = 'datos_id_oficial_vs_datos_capturados';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'solicitud_id',
        'prospecto_id',
        'tipo',
        'dato_capturado',
        'dato_buro',
        'dato_facematch',
        'facematch_vs_capturado',
        'facematch_vs_buro',
        'capturado_vs_buro',
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
