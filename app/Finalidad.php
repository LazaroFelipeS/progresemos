<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo que se conecta con la tabla finalidades
 */
class Finalidad extends Model
{
    /**
     * Tabla a la que se conecta
     */
    protected $table = 'finalidades';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'finalidad',
    ];
}
