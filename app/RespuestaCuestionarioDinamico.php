<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestaCuestionarioDinamico extends Model
{
    protected $table = 'respuestas_cuestionario_dinamico';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'situacion_id',
        'pregunta_id',
        'respuesta'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];
}
