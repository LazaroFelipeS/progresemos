<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla analytics
 * Guarda los datos que se obtienen de google analytics
 */
class ClienteEstrella extends Model
{
    protected $table = 'cliente_estrella';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'cuenta_progresemos',
        'prospecto_id',
        'documentoprincipal',
        'fecha_campana',
        'vigencia',
        'nombre',
        'segundonombre',
        'apellidopaterno',
        'apellidomaterno',
        'fechadenacimiento',
        'email',
        'rfc',
        'telefono',
        'genero',
        'estadocivil',
        'estadodenacimiento',
        'tiempoderecidencia',
        'empresa',
        'puestoqueocupa',
        'tiempolaborandoenlaempresa',
        'colonia',
        'delegacion',
        'ciudad',
        'estado',
        'codigopostal',
        'monto',
        'finalidad',
        'pago',
        'tasa',
        'plazo',
        'procesado',
        'uuid',
        'email_enviado',
        'id_email'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
