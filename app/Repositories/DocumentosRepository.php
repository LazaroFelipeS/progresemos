<?php
namespace App\Repositories;
use App\Repositories\CurlCaller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\MB_UPLOADED_DOCUMENT;
use \Exception as Exception;

class DocumentosRepository {

    private $documentUploaded = '';
    /**
     * Constructor de la clase
     *
     * @param CurlCaller $curl Realiza las llamadas curl
     */
    public function __construct(CurlCaller $curl)
    {
        $this->_curl = $curl;
    }

    function subirDocumentoFTP($data) {

        $documentos = $data['documentos'];
        $email = $data['email'];
        $no_solicitud_t24 = $data['no_solcitud_t24'];
        foreach ($documentos as $key => $documento) {

            $ruta = $data['ruta_local'];
            $archivo = $documento['tipo_documento'].'_'.$documento['detalle_documento'].$documento['formato_documento'];
            $nombre_archivo = $documento['tipo_documento'].'_'.$documento['detalle_documento'];
            if (Storage::disk('s3')->exists("{$ruta}_{$archivo}") == true) {
                $image = "{$ruta}_{$archivo}";
                if (Storage::disk('s3')->exists("{$email}/{$no_solicitud_t24}/{$archivo}")) {
                    Storage::disk('s3')->delete("{$email}/{$no_solicitud_t24}/{$archivo}");
                }

                $response = Storage::disk('s3')->move($image, "{$email}/{$no_solicitud_t24}/{$archivo}", 'private');

                if ($response == 1) {
                    $responseDB = MB_UPLOADED_DOCUMENT::updateOrCreate([
                        'idLoanApp'         => $no_solicitud_t24,
                        'email'             => $email,
                        'name'              => $nombre_archivo,
                    ], [
                        'extension'         => '.jpg',
                        'ip'                => '',
                        'source'            => env('AWS_S3_BUCKET')."/{$email}/{$no_solicitud_t24}/{$archivo}",
                        'businessFormat'    => env('BUSINESSFORMAT'),
                        'createdAt'         => Carbon::now(),
                    ]);
                }
            }
        }
    }

    function subirDocumentoServicioNormal($data) {

        foreach ($documentos as $key => $documento) {
            $arregloDocumento = [
                'user' => [
                    'idUser' => $data['email'],
                    'name' => $data['nombre'],
                    'lastName' => $data['apellidos'],
                    'fullName' => $data['nombre_completo'],
                    'idUserT24' => $data['no_cliente_t24'],
                    'phoneNumber' => $data['celular']
                ],
                'token' => [
                    'token' => 'PANELOPERATIVO',
                    'timeWhenGenerated' => Carbon::now()->format('Y-m-d H:i:s'),
                ],
                'imei' => 'PANELOPERATIVO',
                'applicationId' => $data['no_solicitud_t24'],
                'documentType' => $documento['tipo_documento'],
                'documentDetailType' => $documento['detalle_documento'],
                'image' => $documento['url_documento'], // 'https://praesul.serveo.net/storage/INE_0001.jpeg',
                'fileFormat' => $documento['formato_documento'],
            ];
        }

        $response = $this->makeCallCurl($arregloDocumento);

    }

    function subirDocumentoServicioPanelOperativo($data) {

        foreach ($documentos as $key => $documento) {
            $arregloDocumento = [
                'user' => [
                    'idUser' => $data['email'],
                    'name' => $data['nombre'],
                    'lastName' => $data['apellidos'],
                    'fullName' => $data['nombre_completo'],
                    'idUserT24' => $data['no_cliente_t24'],
                    'phoneNumber' => $data['celular']
                ],
                'token' => [
                    'token' => 'PANELOPERATIVO',
                    'timeWhenGenerated' => Carbon::now()->format('Y-m-d H:i:s'),
                ],
                'imei' => 'PANELOPERATIVO',
                'applicationId' => $data['no_solicitud_t24'],
                'documentType' => $documento['tipo_documento'],
                'documentDetailType' => $documento['detalle_documento'],
                'image' => $documento['url_documento'], // 'https://praesul.serveo.net/storage/INE_0001.jpeg',
                'fileFormat' => $documento['formato_documento'],
            ];
        }

        $response = $this->makeCallCurl($arregloDocumento);

    }

    function makeCallCurl($data) {

        $url = env('URL_SUBIR_DOCUMENTOS');
        $response = $this->_curl->callCurlDocumentos($url, $data);
        return $response;

    }



}
