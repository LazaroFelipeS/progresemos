<?php
namespace App\Repositories;
use \Exception as Exception;
use Log;
use App\DomicilioSolicitud;
use App\Prospecto;
use App\Solicitud;
use App\BCDirecciones;
use App\BCDatosPersonales;
use App\DatosFacematchComparados;

class FaceMatchRepository {

    /**
     * Constructor de la clase
     *
     */
    public function __construct()
    {

    }

    function comparaDatos($datosFaceMatch, $prospecto_id, $solicitud_id) {

        $prospecto = Prospecto::find($prospecto_id);

        $solicitud = Solicitud::with('domicilio', 'bc_direcciones', 'bc_datos_personales')
            ->find($solicitud_id);

        // Verificando el % de coincidencia del nombre del prospecto
        // Obteniendo el nombre del FaceMatch
        $nombrePersona = collect($datosFaceMatch)
            ->whereIn('Code', ['NAME', 'SURNAME'])
            ->toArray();

        $nombreFaceMatch = [];
        foreach ($nombrePersona as $key => $value) {
            $nombreFaceMatch[] = $value->Value;
        }
        $nombreFaceMatch = implode(' ', $nombreFaceMatch);

        // Obteniendo el nombre capturado
        $nombreCapturado = "{$prospecto->nombre} {$prospecto->apellido_p} {$prospecto->apellido_m}";

        // Obteniendo el nombre de Buró de crédito
        if (count($solicitud->bc_datos_personales) > 0) {
            if ($solicitud->bc_datos_personales[0]->segundo_nombre == '') {
                $nombreBuro = "{$solicitud->bc_datos_personales[0]->nombre} {$solicitud->bc_datos_personales[0]->apellido_p} {$solicitud->bc_datos_personales[0]->apellido_m}";
            } else {
                $nombreBuro = "{$solicitud->bc_datos_personales[0]->nombre} {$solicitud->bc_datos_personales[0]->segundo_nombre} {$solicitud->bc_datos_personales[0]->apellido_p} {$solicitud->bc_datos_personales[0]->apellido_m}";
            }
        }

        // Verificando el % de coincidencia del CURP
        // Obteniendo el CURP del FaceMatch
        $curpFaceMatch = collect($datosFaceMatch)
            ->whereIn('Code', ['CURP'])
            ->values()
            ->toArray();

        // Obteniendo el CURP de Buro de crédito
        if (count($solicitud->bc_datos_personales) > 0 && count($curpFaceMatch) > 0) {
            if ($solicitud->bc_datos_personales[0]->curp != '') {
                similar_text($solicitud->bc_datos_personales[0]->curp, $curpFaceMatch[0]->Value, $coincidenciaCURP);
                // Log::info($coincidenciaCURP);
                DatosFacematchComparados::updateOrCreate([
                    'prospecto_id'              => $prospecto->id,
                    'solicitud_id'              => $solicitud->id,
                    'tipo'                      => 'CURP',
                ], [
                    'dato_capturado'            => 'NA',
                    'dato_buro'                 => $solicitud->bc_datos_personales[0]->curp,
                    'dato_facematch'            => $curpFaceMatch[0]->Value,
                    'facematch_vs_capturado'    => null,
                    'facematch_vs_buro'         => $coincidenciaCURP,
                    'capturado_vs_buro'         => null,
                ]);
            }
        }

        // Verificando el % de coincidencia del Domicilio
        // Obteniendo el domicilio del FaceMatch
        $domicilio = collect($datosFaceMatch)
            ->whereIn('Code', ['ADDRESS'])
            ->toArray();
        $domicilioFaceMatch = [];
        foreach ($domicilio as $key => $value) {
            $domicilioFaceMatch[] = $value->Value;
        }
        $domicilioFaceMatch = implode(' ', $domicilioFaceMatch);

        // Obteniendo el domicilio capturado
        $domicilio = $solicitud->domicilio;
        $domicilioCapturado = "{$domicilio->calle} {$domicilio->num_exterior} {$domicilio->num_interior} {$domicilio->colonia} {$domicilio->cp} {$domicilio->delegacion}";

        // Obteniendo el domicilio de Buro de crédito
        $domiciliosBuro = [];
        if (count($solicitud->bc_direcciones) > 0) {
            $domicilioBuro = '';
            foreach ($solicitud->bc_direcciones as $key => $value) {
                $domicilioBuro = $value->dom_calle;
                if ($value->dom_calle_segunda_linea != '') {
                    $domicilioBuro = $domicilioBuro.' '.$value->dom_calle_segunda_linea;
                }
                if ($value->dom_colonia != '') {
                    $domicilioBuro = $domicilioBuro.' '.$value->dom_colonia;
                }
                if ($value->dom_cp != '') {
                    $domicilioBuro = $domicilioBuro.' '.$value->dom_cp;
                }
                if ($value->dom_deleg != '') {
                    $domicilioBuro = $domicilioBuro.' '.$value->dom_deleg;
                }
                $domiciliosBuro[]['domicilio'] = $domicilioBuro;
            }
        }

        // Verificando el % de coincidencia de valores del INE/IFE
        // Obteniendo valores del INE/IFE del FaceMatch
        $datosIDFaceMatch = collect($datosFaceMatch)
            ->whereIn('Code', ['CRC_SECTION', 'ELECTOR_ID', 'REGISTRY_FOLIO'])
            ->values()
            ->toArray();

        // Obteniendo valores del INE/IFE de Buró de crédito
        if (count($solicitud->bc_datos_personales)) {
            if ($solicitud->bc_datos_personales[0]->ife != '') {
                foreach ($datosIDFaceMatch as $key => $value) {
                    $valor = $value->Value;
                    $codigo = $value->Code;
                    similar_text($valor, $solicitud->bc_datos_personales[0]->ife, $coincidencia);

                    DatosFacematchComparados::updateOrCreate([
                        'prospecto_id'              => $prospecto->id,
                        'solicitud_id'              => $solicitud->id,
                        'tipo'                      => $codigo,
                    ], [
                        'dato_capturado'            => 'NA',
                        'dato_buro'                 => $solicitud->bc_datos_personales[0]->ife,
                        'dato_facematch'            => $valor,
                        'facematch_vs_capturado'    => null,
                        'facematch_vs_buro'         => $coincidencia,
                        'capturado_vs_buro'         => null,
                    ]);
                }
            }
        }

        similar_text($domicilioCapturado, $domicilioFaceMatch, $domicilioCapvsFM);
        DatosFacematchComparados::updateOrCreate([
            'prospecto_id'              => $prospecto->id,
            'solicitud_id'              => $solicitud->id,
            'tipo'                      => 'Domicilio Solicitud',
        ], [
            'dato_capturado'            => $domicilioCapturado,
            'dato_buro'                 => 'NA',
            'dato_facematch'            => $domicilioFaceMatch,
            'facematch_vs_capturado'    => $domicilioCapvsFM,
            'facematch_vs_buro'         => null,
            'capturado_vs_buro'         => null,
        ]);

        foreach ($domiciliosBuro as $key => $domicilio) {
            similar_text($domicilio['domicilio'], $domicilioFaceMatch, $domicilioBurovsFM);
            similar_text($domicilioCapturado, $domicilioBuro, $domicilioCapvsBuro);

            $numero = $key + 1;
            DatosFacematchComparados::updateOrCreate([
                'prospecto_id'              => $prospecto->id,
                'solicitud_id'              => $solicitud->id,
                'tipo'                      => 'Domicilio Registrado BC '.$numero,
            ], [
                'dato_capturado'            => $domicilioCapturado,
                'dato_buro'                 => $domicilio['domicilio'],
                'dato_facematch'            => $domicilioFaceMatch,
                'facematch_vs_capturado'    => null,
                'facematch_vs_buro'         => $domicilioBurovsFM,
                'capturado_vs_buro'         => $domicilioCapvsBuro,
            ]);

        }

        similar_text($nombreCapturado, $nombreFaceMatch, $nombreCapvsFM);
        similar_text($nombreBuro, $nombreFaceMatch, $nombreBurovsFM);
        similar_text($nombreCapturado, $nombreBuro, $nombreCapvsBuro);

        DatosFacematchComparados::updateOrCreate([
            'prospecto_id'              => $prospecto->id,
            'solicitud_id'              => $solicitud->id,
            'tipo'                      => 'Nombre',
        ], [
            'dato_capturado'            => $nombreCapturado,
            'dato_buro'                 => $nombreBuro,
            'dato_facematch'            => $nombreFaceMatch,
            'facematch_vs_capturado'    => $nombreCapvsFM,
            'facematch_vs_buro'         => $nombreBurovsFM,
            'capturado_vs_buro'         => $nombreCapvsBuro,
        ]);

    }

}
