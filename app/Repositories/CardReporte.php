<?php

namespace App\Repositories;
use Sebbmyr\Teams\AbstractCard as Card;
/**
 * Simple card for microsoft teams
 */
class CardReporte extends Card
{
    public function getMessage()
    {
        return [
            "@context" => "http://schema.org/extensions",
            "@type" => "MessageCard",
            "themeColor" => '01BC36',
            "summary" => "Reportes",
            "title" => $this->data['title'],
            "sections" => [
                [
                    "activityTitle" => "",
                    "activitySubtitle" => "",
                    "activityImage" => "",
                    "facts" => [
                        [
                            "name" => "Reporte 1",
                            "value" => "[Descargar](". $this->data["reporte_1"] .")"
                        ],
                        [
                            "name" => "Reporte 2",
                            "value" => "[Descargar](". $this->data["reporte_2"] .")"
                        ],
                        [
                            "name" => "Reporte 3",
                            "value" => "[Descargar](". $this->data["reporte_3"] .")"
                        ],
                        [
                            "name" => "Reporte 4",
                            "value" => "[Descargar](". $this->data["reporte_4"] .")"
                        ]
                    ],
                    "text" => "Reportes generados con éxito"
                ]
            ]
        ];
    }
}
