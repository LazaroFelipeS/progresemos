<?php
namespace App\Repositories;
use \Exception as Exception;
use JWTFactory;
use JWTAuth;

class CurlCaller {

    public function callCurlLDAP($url, $curl_post_data) {

        $data_string = json_encode($curl_post_data);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json')
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);

        return $curl_response;

    }

    function callCurlCalixta($celular, $msj) {

        $calixta_config = [
            "cte"       =>  env('CTE'),
            "encpwd"    =>  env('ENCPWD'),
            "email"     =>  env('EMAIL'),
            "numtel"    =>  $celular,
            "msg"       =>  $msj,
            "mtipo"     =>  env('MTIPO'),
            "idivr"     =>  env('IDIVR'),
            "auxiliar"  =>  env('AUXILIAR')
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, env('CALIXTA_URL'));
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($calixta_config));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if (!$curl_response) {
            $curl_response = curl_error($curl);
        }
        curl_close($curl);
        return $curl_response;

    }

    public function callCurlLeadSource($url, $curl_post_data, $key = null) {

        $data_string = json_encode($curl_post_data);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        if ($key == null) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json')
            );
        } else {
            curl_setopt($curl, CURLOPT_HTTPHEADER,
                array(
                    'Content-Type: application/json',
                    'key: '.$key
                )
            );
        }
        $curl_response = curl_exec($curl);
        curl_close($curl);

        return $curl_response;

    }

    public function callCurlDocumentos($url, $curl_post_data) {

        $data_string = json_encode($curl_post_data);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "json={$data_string}");
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded')
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);

        return $curl_response;

    }

    public function callCurlPanelOperativo($url, $curl_post_data) {

        $customClaims = ['jti' => env('JWT_JTI')];
        $payload = JWTFactory::make($customClaims);
        $token = JWTAuth::encode($payload);

        $data_string = json_encode($curl_post_data);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-token: Bearer '.$token,
            'x-api-key: '.env('APIKEY_ALTAPO')
            )
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);

        return $curl_response;

    }
}
