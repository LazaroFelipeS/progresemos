<?php
namespace App\Repositories;
use \Exception as Exception;
use Log;
use App\Repositories\CurlCaller;

class PanelOperativoRepository {

    /**
     * Constructor de la clase
     *
     * @param CurlCaller $curl Realiza las llamadas curl
     */
    public function __construct(CurlCaller $curl)
    {
        $this->_curl = $curl;
    }

    function forzarAlta($idClienteAlta) {
        try {
            $url = env('URL_ALTAPO');
            $response = $this->_curl->callCurlPanelOperativo($url, $idClienteAlta);
            $response = json_decode($response);
            return [
                'success'   => true,
                'response'  => $response
            ];
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'response'  => $e->getMessage()
            ];
        }
    }


}
