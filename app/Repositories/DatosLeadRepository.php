<?php
namespace App\Repositories;
use App\Repositories\CurlCaller;
use \Exception as Exception;

class DatosLeadRepository {

    /**
     * Constructor de la clase
     *
     * @param CurlCaller $curl Realiza las llamadas curl
     */
    public function __construct(CurlCaller $curl)
    {
        $this->_curl = $curl;
    }

    /**
     * Obtiene los datos del lead
     *
     * @param  array $data Datos del lead
     *
     * @return json Resultado de la obtencion de datos
     */
    function getDatosUsuario($data) {

        $url = env('URL_DATOS_USUARIO');
        $curl_post_data = $data;
        $response = $this->_curl->callCurlLeadSource($url, $curl_post_data);
        $response = $this->procesaRespuesta($response, 'Obtención de datos');
        return $response;

    }

    /**
     * Guarda los datos del lead
     *
     * @param  array $data Datos del lead
     *
     * @return json Resultado del guardado de datos
     */
    public function guardaLead($data) {

        $url = env('URL_SAVE_LEAD');
        $key = env('KEY_CLIENTE_ESTRELLA');
        $curl_post_data = $data;
        $response = $this->_curl->callCurlLeadSource($url, $curl_post_data, $key);
        $response = $this->procesaRespuesta($response, 'Guardado de datos');
        return $response;

    }

    /**
     * Procesa la respuesta del servicio
     *
     * @param  json   $response Respusta del servicio invocado.
     * @param  string $mensaje  Mensaje de respuesta del servicio invocado.
     *
     * @return json             Resultado de procesar la respusta.
     */
    public function procesaRespuesta($response, $mensaje) {

        $response = json_decode($response);

        if (isset($response->code)) {

            if ($response->code == '200') {
                return [
                    'success'   => true,
                    'message'   => $mensaje,
                    'resultado' => $response,
                ];
            } else {
                return [
                    'success' => false,
                    'code'    => $response->code,
                    'message' => $response->message
                ];
            }

        } else {
            return [
                'success'   => false,
                'code'      => '999',
                'message'   => 'No hubo respuesta por parte del servicio'
            ];
        }
    }

}
