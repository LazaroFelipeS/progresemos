<?php
namespace App\Repositories;
use \Exception as Exception;
use App\LogSolicitud;
use App\TrackingSolicitud;
use Carbon\Carbon;
use App\Solicitud;
use App\RespuestaMaquinaRiesgo;
use App\PlantillaComunicacion;
use App\MotivoRechazo;
use App\Situacion;
use App\Http\Controllers\FuncionesT24;
use App\Http\Controllers\serverMethods;
use App\Http\Controllers\cleanStrMethods;
use DB;
use Validator;

class SolicitudRepository {

    use FuncionesT24, serverMethods, cleanStrMethods;

    /**
     * Genera el log para cada paso del proceso de registro
     * @param  string $nombre     Nombre del prospecto que se esta registrando
     * @param  string $mnsj_str   Mensaje que sera registrado
     * @param  array $user_input  Captura de datos del prospecto
     * @return void
     */
    private function logRegisterError($nombre, $mnsj_str, $user_input)
    {
        $now_str = $date = date('d_m_Y_H_i_s');
        //switch to the logs directory
        chdir('../storage/logs/');
        $new_path = getcwd();
        //create the new register error log
        $lfname = $now_str.'_'.$nombre;
        $logfile = fopen($lfname.".txt", "w") or die("Unable to create file!");
        //remove password and password conf from plain-text input
        $user_input['password'] = '********';
        $user_input['password_conf'] = '********';
        $mnsj_data = json_decode($mnsj_str);
        if ($mnsj_data->message == 'Código Inválido' || $mnsj_data->message == 'Login: Código Inválido') {
            //get the last código sent to the user (saved in prospect as activate_code)
            $prospect = Prospecto::find($mnsj_data->prospect_id);
            if ($prospect) {
                // $last_code = decrypt($prospect->activate_code);
                //add the last code to the user input array
                $user_input['ult_sms'] = ''; // $last_code;
            }
        }
        //add the mnsj_str to the log file
        $message = $mnsj_str.'
		Data :
		'.json_encode($user_input);
        fwrite($logfile, $message);
        fclose($logfile);
    }

    /**
     * Funcion que determina que parte del flujo de la solicitud debe mostrar
     * @param  string $paso         Paso actual en el que se encuentra la solicitud
     * @param  integer $prospecto   Id del prospecto Autenticado
     * @param  boolean $login       Para identificar si el método se esta invocando del login
     *
     * @return array                Arreglo con la información del siguiente paso
     */
    public function siguientePaso($paso = null, $prospecto = null, $login = null) {

        // Si la variable $prospecto esta definida es por que la función se esta invocando cuando la página
        // se actualiza, cuando se invoca de un método interno (Llamada a BC, ALP,
        // Status Oferta) o cuando se hace login.
        if ($prospecto != null) {
            $solicitud = Solicitud::with('tracking', 'log')->where('prospecto_id', $prospecto->id)
                ->orderBy('created_at', 'DESC')
                ->first();

            $status = $solicitud->status;
            $sub_status = $solicitud->sub_status;
            $cuestionario = null;
            $tipoPoblacion = null;
            $redirect = null;
            $formulario = null;
            $mostrarOferta = false;
            $modal = null;
            $nueva_solicitud = false;
            //dd($solicitud->status);
            if ($status == 'Registro' && $prospecto->sms_verificacion == true) {
                $formulario = 'verificar_codigo';
                if ($login == true) {
                    $modal = view("modals.modalSolicitudPendiente")->render();
                }
            } elseif ($status == 'Registro Confirmado' || $status == 'Reporte Completo') {

                if ($login == true) {
                    $modal = view("modals.modalSolicitudPendiente")->render();
                }
                switch ($sub_status) {
                    case 'Validar SMS':
                        $formulario = 'datos_domicilio';
                        break;
                    case 'Domicilio':
                        $formulario = 'datos_personales';
                        break;
                    case 'Datos Personales':
                        $formulario = 'datos_buro';
                        break;
                    case 'Cuentas de Crédito':
                        $formulario = 'datos_buro';
                        break;
                    case 'Reporte Completo':
                        $formulario = 'datos_ingreso';
                        break;
                    case 'Datos Ingresos-Egresos':
                        $formulario = 'datos_empleo';
                        break;
                    default:
                        $formulario = 'simulador';
                        break;
                }

            } elseif($status == 'Hit BC') {

                switch ($sub_status) {
                    case 'Check Bc Score Elegible':
                        $formulario = 'datos_ingreso';
                        break;
                    case 'Elegible':
                        $formulario = 'datos_empleo';
                        break;
                    case 'No Elegible':
                        $formulario = 'simulador';
                        $nueva_solicitud = true;
                        session()->flash('nueva_solicitud', true);
                        break;
                    default:
                        $formulario = 'simulador';
                        $nueva_solicitud = true;
                        session()->flash('nueva_solicitud', true);
                        break;
                }

            } elseif ($status == 'Invitación a Continuar') {
                //dd($status);
                $respuesta = RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto->id)
                    ->where('solicitud_id', $solicitud->id)
                    ->get();

                if ($login == true && $sub_status == 'Oferta Rechazada') {
                    $sub_status = $sub_status.'_LoginOR';
                } elseif ($login == true && ($sub_status == 'Oferta Aceptada' || $sub_status == 'Oferta Aceptada - FaceMatch Completado')) {
                    $sub_status = $sub_status.'_LoginOA';
                    //$modal = view("modals.modalSolicitudPendiente")->render();
                } elseif ($login == true) {
                    $modal = view("modals.modalSolicitudPendiente")->render();
                }
                //dd($sub_status);
                switch ($sub_status) {
                    case 'Invitación a Continuar':
                    case 'Cuestionario Completo':
                        $mostrarOferta = true;
                        $plantillaComunicacion = null;
                        $modal = 'modalOferta';
                        $datosOferta = [];
                        foreach ($respuesta as $oferta) {
                            $datosOferta[] = [
                                'monto'         => $oferta->monto,
                                'plazo'         => $oferta->plazo,
                                'tasa'          => $oferta->tasa,
                                'pago_estimado' => $oferta->pago,
                                'tipo_oferta'   => $oferta->tipo_oferta,
                            ];
                            $tipoPoblacion = $oferta->tipo_poblacion;
                            $plantillaComunicacion = $oferta->plantilla_comunicacion;

                        }
                        if ($tipoPoblacion == 'oferta_doble') {
                            $modal = 'modalDobleOferta';
                        }
                        $motivos = MotivoRechazo::pluck('motivo');
                        $modal = view("modals.{$modal}", ['datosOferta' => $datosOferta, 'motivos' => $motivos])->render();
                        break;
                    case 'Cuestionario Incompleto':
                        $formulario = 'cuestionario_dinamico';
                        $pantallas = $respuesta[0]->situaciones;
                        $pantallas = explode(',', $pantallas);
                        $tipoPoblacion = $respuesta[0]->tipo_poblacion;
                        $cuestionario = Situacion::whereIN('situacion', $pantallas)
                            ->with('cuestionario')
                            ->get()
                            ->toArray();
                        break;
                    case 'Oferta Aceptada - FaceMatch':
                        $redirect = '/carga_documentos/paso1';
                        break;
                    case 'Oferta Rechazada':
                    case 'Oferta Aceptada':
                    case 'Oferta Aceptada - FaceMatch Completado':
                        session()->flash('nueva_solicitud', true);
                        $nueva_solicitud = true;
                        break;
                    case 'Oferta Rechazada_LoginOR':
                        $nueva_solicitud = true;
                        break;
                    case 'Oferta Aceptada_LoginOA':
                    case 'Oferta Aceptada - FaceMatch Completado_LoginOA':
                        $modal = view("modals.modalSolicitudTerminada")->render();
                        $nueva_solicitud = true;
                        break;
                }

            } else {
                // Casos: Oferta Diferida, Rechazado o cualquier otro status no especificado
                // Si el prospecto inicio sesion y fue oferta diferida o rechazado
                // tiene que iniciar una nueva solicitud
                if ($prospecto->login == true) {
                    $nueva_solicitud = true;
                    $modal = null;
                } else {
                     // Si recarga la página y la ultima solicitud es oferta diferida
                     // o rechazado se tiene que redirigir al simulador con una nueva
                     // solicitud
                    session()->flash('nueva_solicitud', true);
                    $nueva_solicitud = true;
                }

            }

            // Si el prospecto tiene el campo login en true es por que o inicio sesion
            // o no ha validado el sms que se envia al hacer login, por lo que
            // se tiene que mostrar el formulario de validacion en true y cualquier
            // redirect se tiene que anular siempre y cuando no se tenga que
            // iniciar con una nueva solicitud
            if ($prospecto->login == true && $nueva_solicitud == false) {
                $formulario = 'verificar_codigo';
                $redirect = null;
            }

            return [
                'formulario'        => $formulario,
                'oculta'            => 'simulador',
                'cuestionario'      => $cuestionario,
                'tipo_poblacion'    => $tipoPoblacion,
                'redirect'          => $redirect,
                'mostrar_oferta'    => $mostrarOferta,
                'modal'             => $modal,
                'nueva_solicitud'   => $nueva_solicitud
            ];

        } elseif ($paso != null) {
            // Si la variable $paso esta definida es por que se esta invocando del botón siguiente
            // que se encuentra en cada paso del flujo a excepción del Cuestionario Dinámico

            $formulario = '';
            switch ($paso) {
                case 'registro':
                    $formulario = 'registro';
                    break;
                case 'verificar_codigo':
                    $formulario = 'verificar_codigo';
                    break;
                case 'datos_domicilio':
                    $formulario = 'datos_domicilio';
                    break;
                case 'datos_personales':
                    $formulario = 'datos_personales';
                    break;
                case 'datos_buro':
                    $formulario = 'datos_buro';
                    break;
                case 'datos_ingreso':
                case 'datos_elegible':
                    $formulario = 'datos_ingreso';
                    break;
                case 'datos_empleo':
                    $formulario = 'datos_empleo';
                    break;
                case 'cuestionario_dinamico':
                    $formulario = 'cuestionarioDinamico';
                    break;
                default:
                    $formulario = 'simulador';
                    break;
            }

            return [
                'formulario'        => $formulario,
                'oculta'            => null,
            ];
        }
    }

    public function setUltMnsjUsr($solicitud, $log) {

        $log = $this->limpiaLog($log);

        if ($solicitud->log()->exists() == false) {

            $ultimoMensaje = [
                'datetime'      => Carbon::now()->format('Y-m-d H:i:s'),
                'console_log'   => json_decode($log),
            ];

            LogSolicitud::updateOrCreate([
                'solicitud_id'              => $solicitud->id,
            ], [
                'ultimo_mensaje_usuario'    => json_encode($ultimoMensaje)
            ]);

        } else {

            $logfile = [];
            $logActual = $solicitud->log()->get()->toArray();
            $logActual = json_decode($logActual[0]['ultimo_mensaje_usuario'], true);

            $ultimoMensaje = [
                'datetime'      => Carbon::now()->format('Y-m-d H:i:s'),
                'console_log'   => json_decode($log),
            ];

            if (isset($logActual[0])) {
                $logfile = $logActual;
                array_push($logfile, $ultimoMensaje);
            } else {
                array_push($logfile, $logActual);
                array_push($logfile, $ultimoMensaje);
            }

            LogSolicitud::updateOrCreate([
                'solicitud_id'              => $solicitud->id,
            ], [
                'ultimo_mensaje_usuario'    => json_encode($logfile, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
            ]);
        }

    }

    public function setUltPuntoReg($solicitud_id, $status, $sub_status, $success, $result, $extra = NULL) {

        $decision = false;
        $tipoOferta = '';
        if ($extra != null) {
            if (isset($extra['tipo_oferta'])) {
                $decision = true;
            }
            $extra = json_encode($extra, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }

        if ($decision == false || ($decision == true && $tipoOferta == 'oferta_normal')) {

            TrackingSolicitud::insert([
                'solicitud_id'  => $solicitud_id,
                'status'        => $status,
                'sub_status'    => $sub_status,
                'success'       => $success,
                'descripcion'   => $result,
                'extra'         => $extra,
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now()
            ]);

        }


    }

    public function limpiaLog($log) {

        $log = json_decode($log);
        $log = json_encode($log,  JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return $log;

    }

    function json_validate($string)
    {
        // decode the JSON data
        $result = json_decode($string);
        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = ''; // JSON is valid // No error has occurred
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }
        if ($error !== '') {
            return $error;
        }
        return true;
    }


    public function updateDataT24 () {

        $soapWrapper = new SoapWrapper();
        $soapWrapper->add('T24', function ($service) {
            $service->wsdl(env('WSDL_T24'))
                ->trace(true)
                ->options([
                    'connection_timeout' => 10,
                    'default_socket_timeout' => 10
                ]);
        });

        $response = $soapWrapper->call('T24.CustomerInput', [
            'body' => [
                'WebRequestCommon' => [
                    'company' => env('COMPANY_T24'),
                    'password' => env('PASSWORD_T24'),
                    'userName' => env('USERNAME_T24')
                ],
                'OfsFunction' => [

                ],
                'CUSTOMERPRSTINPUTWSType' => [
                    'id' => '100152',
                    'gNAME1' => [
                        'NAME1' => 'SARABIAS'
                    ]
                ]
            ]
        ]);

    }

    public function ofertaAceptada($prospecto, $solicitud, $respuestaMR, $tipo_oferta = null) {

        RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto->id)
            ->where('solicitud_id', $solicitud->id)
            ->update(['elegida' => 1, 'status_oferta' => 'Oferta Aceptada']);

        $oferta_minima = $respuestaMR->oferta_minima;
        $simplificado = $respuestaMR->simplificado;
        $carga_identificacion_selfie = $respuestaMR->carga_identificacion_selfie;
        $modal = null;

        if ($respuestaMR->tipo_poblacion == 'oferta_doble') {

            RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto->id)
                ->where('solicitud_id', $solicitud->id)
                ->where('tipo_oferta', 'not like', '%'.$tipo_oferta.'%')
                ->update(['elegida' => 0]);

            try {

                $monto = str_replace('$', '', $respuestaMR->monto);
                $monto = intval(str_replace(',', '', $monto));
                $pago = str_replace('$', '', $respuestaMR->pago);
                $pago = str_replace(',', '', $pago);
                $pago = str_replace(' quincenal', '', $pago);
                $plazo = $respuestaMR->plazo;

                $resultado = DB::connection('mysql_maquina_riesgos')
                    ->table('Tb1_ResFinal_MR')
                    ->where('id_prospect', $prospecto->id)
                    ->where('id_solic', $solicitud->id)
                    ->update([
                        'Monto_Mod' => $monto,
                        'Pago_Mod' => $pago,
                        'Plazo_Mod' => $plazo,
                    ]);

                $plazo = str_replace(' quincenas', '', $plazo);

                $resultado = DB::connection('mysql_maquina_riesgos')
                    ->table('Tb1_Ofertas_MR')
                    ->where('id_prospect', $prospecto->id)
                    ->where('id_solic', $solicitud->id)
                    ->where('tipo_oferta', 'like' , '%'.$tipo_oferta.'%')
                    ->update([
                        'Aceptada' => 1,
                    ]);

                $resultado = DB::connection('mysql_maquina_riesgos')
                    ->table('Tb1_ModMTPP_MR')
                    ->where('id_prospect', $prospecto->id)
                    ->where('id_solic', $solicitud->id)
                    ->update([
                        'MontoF_Mod' => $monto,
                        'PagoF_Mod' => $pago,
                        'PlazoF_Mod' => $plazo,
                    ]);

            } catch (\Exception $e) {

                return $mnsj_str = [
                    'success'   => false,
                    'message'   => $e->getMessage()
                ];
            }
        }

        // Se agrega substatus Oferta Aceptada - FaceMatch para que todos los
        // prospectos que tengan carga_identificacion_selfie como true
        if ($carga_identificacion_selfie == true) {

            $this->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                'Oferta Aceptada - FaceMatch',
                1,
                'Se ha guardado el status de la solicitud con éxito'
            );
            $solicitud->sub_status = 'Oferta Aceptada - FaceMatch';
            $solicitud->save();

        } else {

            $id_plantilla = $respuestaMR->plantilla_comunicacion;
            $plantilla = PlantillaComunicacion::select('id', 'modal_encabezado', 'modal_cuerpo', 'modal_img')
                        ->where('plantilla_id', $id_plantilla)
                        ->get()
                        ->toArray();

            $tituloModal = $plantilla[0]['modal_encabezado'];
            $imgModal = $plantilla[0]['modal_img'];
            $cuerpoModal = $plantilla[0]['modal_cuerpo'];
            $modal = view("modals.modalStatusSolicitud", ['tituloModal' => $tituloModal, 'imgModal' => $imgModal, 'cuerpoModal' => $cuerpoModal, 'continua' => false])->render();

        }

        // Buscando que el prospecto no exista en la tabla de clientes de T24
        // El método se encuentra en serverMethods
        $clienteT24 = self::buscarClienteT24(
            $prospecto->id,
            $solicitud->id
        );

        // Si el cliente es encontrado el proceso comienza desde la creación
        // de la solicitud
        if ($clienteT24['cliente_t24'] == true) {

            $idClienteT24 = $clienteT24['datos'][0]['id_cliente_t24'];
            $email_distinto = $clienteT24['email_distinto'];

            /**
             * Procesa el alta de la solicitud, el método se encuentra en Funciones T24
             * Id Prospecto, Id Solicitud, Id del registro en tabla clientes_alta, Id del Prospecto en T24, Identifica si el mail es diferente
             */
            $altaCliente = self::altaSolicitudT24($prospecto->id, $solicitud->id, null, $idClienteT24, $email_distinto);

        // Si el cliente no existe y no hay un error en el resultado de la
        // búsqeda el proceso comienza desde crear el cliente
        } elseif ($clienteT24['cliente_t24'] == false && $clienteT24['error'] == false) {

            /**
             * Procesa el alta del cliente, el método se encuentra en Funciones T24
             * Id Prospecto, Id Solicitud, Id del registro en tabla alta_clientes, error, mensaje
             */
            $altaCliente = self::altaClienteT24($prospecto->id, $solicitud->id, null, false, null);

        // Si el cliente no existe y hubo un error en la búsqueda el proceso
        // comienza desde crear el cliente
        } else {

            $msj = $clienteT24['msj'];
            /**
             * Procesa el alta del cliente, el método se encuentra en Funciones T24
             * Id Prospecto, Id Solicitud, Id del registro en tabla clientes_alta, error, mensaje
             */
            $altaCliente = self::altaClienteT24($prospecto->id, $solicitud->id, null, true, $msj);

        }

        return [
            'success'                       => true,
            'oferta_minima'                 => $oferta_minima,
            'carga_identificacion_selfie'   => $carga_identificacion_selfie,
            'modal'                         => $modal,
        ];

    }

    function validacionesSolicitud($datosSolicitud, $formulario) {
        $reglas = null;
        $mensajes = [
            'required'                      => 'El campo es obligatorio.',
            'calle.required'                => 'El campo Calle es obligatorio.',
            'no_exterior.required'          => 'El campo Número exterior es obligatorio.',
            'password.regex'                => 'La contraseña debe contener al menos: 1 letra mayúscula, 1 letra minúscula, 1 número y debe ser al menos de 8 dígitos.',
            'confirma_password.same'        => 'El campo contraseña y confirma contraseña deben ser iguales.',
            'numeric'                       => 'El campo debe contener solo números.',
            'email'                         => 'El email no es válido.',
            'digits'                        => 'El campo debe tener :digits números.',
            'ingreso_mensual.min'           => 'El ingreso mensual debe tener al menos 4 dígitos',
            'alpha_numeric_spaces_not_html' => 'El campo solo puede contener caracteres válidos: [A-Z][0-9] o espacios',
            'alpha_spaces_not_html'         => 'El campo solo puede contener caracteres válidos: [A-Z]',
            'max'                           => 'El campo no puede ser mayor a 5 caracteres',
            'min'                           => 'El campo no puede ser menor a 5 caracteres',
            'rfc'                           => 'El RFC esta formado por 4 letras y 6 números (2 del año, 2 del mes y 2 del día de la fecha de nacimiento)'
        ];

        switch ($formulario) {
            case 'simulador':
                $reglas = [
                    'monto_prestamo'    => 'required|numeric|min:'.$datosSolicitud['monto_minimo'].'|max:'.$datosSolicitud['monto_maximo'],
                    'plazo'             => 'required',
                    'finalidad'         => 'required',
                    'finalidad_custom'  => 'required|alpha_numeric_spaces_not_html'
                ];
                $mensajesSimulador = [
                    'monto_prestamo.max' => 'El monto no puede ser mayor a $'.number_format($datosSolicitud['monto_maximo'], 0),
                    'monto_prestamo.min' => 'El monto no puede ser menor a $'.number_format($datosSolicitud['monto_minimo'], 0),
                ];
                $mensajes = array_merge($mensajes, $mensajesSimulador);
            break;
            case 'registro':
                $reglas = [
                    'monto'            => 'required|numeric|min:'.$datosSolicitud['monto'].'|max:'.$datosSolicitud['monto'],
                    'plazo'                     => 'required',
                    'finalidad'                 => 'required',
                    //'finalidad_custom'          => 'required|alpha_numeric_spaces_not_html',
                    'nombre'                   => 'required|alpha_spaces_not_html',
                    'apellidomaterno'          => 'required|alpha_spaces_not_html',
                    'apellidopaterno'          => 'required|alpha_spaces_not_html',
                    'celular'                   => 'required|numeric|digits:10',
                    'email'                     => 'required|email',
                    'password'                => 'required|regex:/(^(?:(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,})$)/u',
                    'confirma_password'   => 'required|same:password',
                ];
            break;
            case 'domicilio':
                $reglas = [
                    'calle'         => 'required|alpha_numeric_spaces_not_html|max:100',
                    'no_exterior'   => 'required|alpha_numeric_spaces_not_html|max:5',
                    'no_interior'   => 'alpha_numeric_spaces_not_html|max:5',
                    'codigo_postal' => 'required|digits:5',
                    'colonia'       => 'required|alpha_numeric_spaces_not_html',
                    'delegacion'    => 'required|alpha_numeric_spaces_not_html',
                    'ciudad'        => 'alpha_numeric_spaces_not_html',
                    'estado'        => 'required|alpha_numeric_spaces_not_html',
                ];
            break;
            case 'datos_personales':
                $reglas = [
                    'sexo'              => 'required|alpha_numeric_spaces_not_html',
                    'fecha_nacimiento'  => 'required|date',
                    'ciudad_nacimiento' => 'required|alpha_numeric_spaces_not_html',
                    'estado_nacimiento' => 'required|alpha_numeric_spaces_not_html',
                    'telefono_casa'     => 'required|digits:10',
                    'estado_civil'      => 'required|alpha_numeric_spaces_not_html',
                    'nivel_estudios'    => 'required|alpha_numeric_spaces_not_html',
                    'rfc'               => 'required|alpha_numeric_spaces_not_html',
                ];
            break;
            case 'datos_empleo':
                $reglas = [
                    'empresa'               => 'required|alpha_numeric_spaces_not_html',
                    'antiguedad_empleo'     => 'required|alpha_numeric_spaces_not_html',
                    'telefono_empleo'       => 'required|digits:10',
                    'fecha_ingreso'         => 'required|date',
                    'calle_empleo'          => 'required|alpha_numeric_spaces_not_html|max:100',
                    'no_exterior_empleo'    => 'required|alpha_numeric_spaces_not_html|max:5',
                    'no_interior_empleo'    => 'alpha_numeric_spaces_not_html|max:5',
                    'codigo_postal_empleo'  => 'required|digits:5',
                    'colonia_empleo'        => 'required|alpha_numeric_spaces_not_html',
                    'delegacion_empleo'     => 'required|alpha_numeric_spaces_not_html',
                    'ciudad_empleo'         => 'alpha_numeric_spaces_not_html',
                    'estado_empleo'         => 'required|alpha_numeric_spaces_not_html',
                ];
            break;
        }



        $validaciones = Validator::make($datosSolicitud, $reglas, $mensajes);
        return $validaciones;

        /*
        $validator = Validator::make($datosSolicitud, [








            'ocupacion' => 'required',
            'fuente_ingresos' => 'required',
            'ingreso_mensual' => 'required|numeric|min:1000',
            'antiguedad_empleo' => 'required',
            'nombre_empresa' => 'required',
            'telefono_empleo' => 'required|numeric|digits:10',
            'residencia' => 'required',
            'antiguedad_domicilio' => 'required',
            'gastos_mensuales' => 'required|numeric',
            'dependientes_economicos' => 'required',
        ], [

        ])->validate();*/


    }

}
