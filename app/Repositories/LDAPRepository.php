<?php
namespace App\Repositories;
use App\Repositories\CurlCaller;
use \Exception as Exception;

class LDAPRepository {

    /**
     * Constructor de la clase
     *
     * @param CurlCaller $curl Realiza las llamadas curl
     */
    public function __construct(CurlCaller $curl)
    {
        $this->_curl = $curl;
    }

    /**
     * Crea un usuario en LDAP
     *
     * @param  array $data Datos del usuario
     *
     * @return json Resultado de la creación de usuario en LDAP
     */
    function createUserLDAP($data) {

        $targetIP = env('TARGET_IP');
        $targetPort = env('TARGET_PORT');
        $methodURL = env('CREATE_USER_URL');

        $url = $targetIP.$targetPort.$methodURL;

        $curl_post_data = [
            'names' =>  $data['nombres'],
            'surnames' => $data['apellido_paterno'].' '.$data['apellido_materno'],
            'user' =>  $data['email'],
            'password' => $data['password'],
            'telephoneNumber' => $data['celular'],
            'idT24' => ''
        ];

        $response = $this->_curl->callCurlLDAP($url, $curl_post_data);
        $response = $this->procesaRespuesta($response, 'Usuario creado');
        return $response;

    }

    /**
     * Login de Usuario en LDAP
     *
     * @param  array $data Datos del usuario
     *
     * @return json Resultado de la creación de usuario en LDAP
     */
    function loginUserLDAP($data) {

        $targetIP = env('TARGET_IP');
        $targetPort = env('TARGET_PORT');
        $methodURL = env('LOGIN_USER');
        $url = $targetIP.$targetPort.$methodURL;

        $curl_post_data = [
            'user'      => $data['email'],
            'password'  => $data['password'],
            'app'       => false
        ];

        $response = $this->_curl->callCurlLDAP($url, $curl_post_data);
        $response = $this->procesaRespuesta($response, 'Usuario logueado');
        return $response;
    }

    /**
     * Envia un correo con una contraseña temporal para poder reestablecer
     * la contraseña.
     *
     * @param  array $data  Datos del usuario.
     *
     * @return json         Resultado del envío del correo.
     */
    public function restorePassword($data) {

        $targetIP = env('TARGET_IP');
        $targetPort = env('TARGET_PORT');
        $methodURL = env('RESTORE_PASSWORD');
        $url = $targetIP.$targetPort.$methodURL;

        $curl_post_data = [
            'email'             => $data['correo'],
            'businessFormat'    => env('LDAP_BUSSINESS')
        ];

        $response = $this->_curl->callCurlLDAP($url, $curl_post_data);
        $response = $this->procesaRespuesta($response, 'Email de restauración de contraseña enviado');
        return $response;
    }

    public function updatePassword($data) {

        $targetIP = env('TARGET_IP');
        $targetPort = env('TARGET_PORT');
        $methodURL = env('UPDATE_PASSWORD');
        $url = $targetIP.$targetPort.$methodURL;

        $curl_post_data = [
            'user'          => [
                'idUser'        => $data['idUser'],
                'name'          => $data['name'],
                'lastName'      => $data['lastName'],
                'fullName'      => $data['fullName'],
                'idUserT24'     => $data['idUserT24'],
                'phoneNumber'   => $data['phoneNumber']
            ],
            'token'         => [
                'token'             => $data['token'],
                'timeWhenGenerated' => $data['timeWhenGenerated']
            ],
            'app'           => false,
            'newPassword'   => $data['new_password']
        ];

        $response = $this->_curl->callCurlLDAP($url, $curl_post_data);
        $response = $this->procesaRespuesta($response, 'Contraseña actualizada con éxito');
        return $response;
    }

    /**
     * Actulizacion de Usuario en LDAP
     *
     * @param  array $data Datos del usuario
     *
     * @return json Resultado de la creación de usuario en LDAP
     */
    function updateUserLDAP($data) {

        $targetIP = env('TARGET_IP');
        $targetPort = env('TARGET_PORT');
        $methodURL = env('UPDATE_USER');

        $url = $targetIP.$targetPort.$methodURL;

        $curl_post_data = [
            'user' => [
                'idUser' => $data['user']['idUser'],
                'name' => $data['user']['name'],
                'lastName' => $data['user']['lastName'],
                'fullName' => $data['user']['fullName'],
                'idUserT24' => $data['user']['idUserT24'],
                'phoneNumber' => $data['user']['phoneNumber']
            ],
            'token' => [
                'token' => $data['token']['token'],
                'timeWhenGenerated' => $data['token']['timeWhenGenerated']
            ],
            'app' => $data['app'],
            'surnames' => $data['surnames'],
            'idT24' => $data['idT24']
        ];

        $response = $this->_curl->callCurlLDAP($url, $curl_post_data);
        $response = $this->procesaRespuesta($response, 'Usuario ligado');
        return $response;
    }


    /**
     * Procesa la respuesta de los servicios LDAP
     *
     * @param  json   $response Respusta del servicio invocado.
     * @param  string $mensaje  Mensaje de respuesta del servicio invocado.
     *
     * @return json             Resultado de procesar la respusta.
     */
    public function procesaRespuesta($response, $mensaje) {

        if ($response) {

            $response = json_decode($response);

            if ($response->code == '00') {
                return json_encode([
                    'success'   => true,
                    'message'   => $mensaje,
                    'resultado' => $response
                ]);
            } else {
                return json_encode([
                    'success' => false,
                    'code'    => $response->code,
                    'message' => $response->message
                ]);
            }

        } else {
            return json_encode([
                'success'   => false,
                'code'      => '999',
                'message'   => 'Error de sistema, No response from the LDAP service'
            ]);
        }
    }

}
