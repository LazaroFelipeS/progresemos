<?php
namespace App\Repositories;
use App\Repositories\CurlCaller;
use Exception as Exception;
use App;

class CalixtaRepository {

    /**
     * Constructor de la clase
     *
     * @param CurlCaller $curl Realiza las llamadas curl
     */
    public function __construct(CurlCaller $curl)
    {
        $this->_curl = $curl;
    }

    /**
     * Envía un SMS al celular
     *
     * @param  integer $celular Número de celular al que se enviará el SMS
     * @param  string  $msj     Mensaje que se enviará
     *
     * @return
     */
    public function sendSMS($celular, $msj)
    {
        $resultado = $this->_curl->callCurlCalixta($celular, $msj);
        $resultado = $this->procesaRespuesta($resultado);
        return $resultado;
    }

    public function procesaRespuesta($resultado) {

        if ($resultado == 'OK|3') {
            return json_encode([
                'success'       => true,
                'message'       => 'El mensaje ha sido enviado.',
                'response_code' => '3'
            ]);
        } else {
            $res_code = 0;
            $res = explode('|', $resultado);
            if (isset($res[1])) {
                $res_code = $res[1];
                $calixta_errors = [
                    '6'     => 'Número no móvil',
                    '10'    => 'Número inválido',
                    '101'   => 'Falta de saldo',
                    '-1'    => 'Error general'
                ];

                if (isset($calixta_errors[$res_code]) == true) {
                    $calixta_msg = $calixta_errors[$res_code];
                } else {
                    $calixta_msg = utf8_encode($res_code);
                }
            } else {
                $calixta_msg = $resultado;
            }

            return json_encode([
                'response'      => 'error',
                'success'       => false,
                'message'       => 'El mensaje no ha sido enviado. '.$calixta_msg,
                'response_code' => $res_code
            ]);
        }

    }

}
