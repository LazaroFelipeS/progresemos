<?php

namespace App\Providers;

use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Queue\Events\JobFailed;
use App\Http\Controllers\FuncionesT24;
use App\Http\Controllers\serverMethods;
use App\Http\Controllers\cleanStrMethods;
use Log;
use Validator;
use Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;
use App\Jobs\ProcesaFaceMatch;
use App\ClientesAlta;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class AppServiceProvider extends ServiceProvider
{
    use FuncionesT24, serverMethods, cleanStrMethods;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Evento que se ejecuta al termino del procesamiento de un Job de la
         * cola.
         */
        Queue::after(function (JobProcessed $event) {

            // Obteniendo el nombre del Job
            $job = $event->job->resolveName();

            if (strpos($job, 'AltaClienteAutomatica') !== false) {

                $payload = $event->job->getRawBody();
                $payload = json_decode($payload);
                $data = $payload->data;
                $command = (array) unserialize($data->command);
                $datosAltaCliente = json_decode($command['' . "\0" . '*' . "\0" . 'clienteAlta']);

                if ($datosAltaCliente->aplica_solicitud == 1) {
                    $altaSolicitud = self::altaSolicitudT24(
                        $datosAltaCliente->prospecto_id,
                        $datosAltaCliente->solicitud_id,
                        $datosAltaCliente->id
                    );
                }

            }

            if (strpos($job, 'AltaSolicitudAutomatica') !== false) {

                $payload = $event->job->getRawBody();
                $payload = json_decode($payload);
                $data = $payload->data;
                $command = (array) unserialize($data->command);
                $datosAltaCliente = json_decode($command['' . "\0" . '*' . "\0" . 'clienteAlta']);

                $altaSolicitud = self::ligueUsuarioLDAP(
                    $datosAltaCliente->id
                );

                if ($datosAltaCliente->aplica_facematch == 1 && $datosAltaCliente->facematch === 0) {

                    $clienteAlta = ClientesAlta::findOrFail($datosAltaCliente->id);
                    $jobProcesaFaceMatch = (new ProcesaFaceMatch($clienteAlta))->delay(10);
                    dispatch($jobProcesaFaceMatch);

                }

                if ($datosAltaCliente->solo_carga_identificacion_selfie == 1 && $datosAltaCliente->facematch === 0) {

                    $clienteAlta = ClientesAlta::findOrFail($datosAltaCliente->id);
                    $jobProcesaFaceMatch = (new ProcesaCargaIdentificacionSelfie($clienteAlta))->delay(10);
                    dispatch($jobProcesaFaceMatch);

                }

            }

            if (strpos($job, 'ProcesaFaceMatch') !== false) {

                $payload = $event->job->getRawBody();
                $payload = json_decode($payload);
                $data = $payload->data;
                $command = (array) unserialize($data->command);
                $datosAltaCliente = json_decode($command['' . "\0" . '*' . "\0" . 'clienteAlta']);

                if ($datosAltaCliente->aplica_email == 1) {
                    $altaSolicitud = self::EnvioEmail(
                        $datosAltaCliente->id
                    );
                }

            }

        });

        // Validación customizada que permite solo letras y espacios
        Validator::extend('alpha_numeric_spaces', function ($attribute, $value) {

            return preg_match('/[\p{L}\p{N} .-]+$/u', $value);

        });

        Validator::extend('alpha_spaces_not_html', function ($attribute, $value) {

            if (preg_match('/(?<=<)\w+(?=[^<]*?>)/', $value) || preg_match('/[#$%^&*()+=\-\[\]\'.\/{}|":<>?~\\\\]/', $value)) {
                return false;
            } else {
                if (preg_match('/[\p{L} ,.-]+$/u', $value)) {
                    return true;
                } else {
                    return false;
                }
            }

        });

        Validator::extend('alpha_numeric_spaces_not_html', function ($attribute, $value) {

            if (preg_match('/(?<=<)\w+(?=[^<]*?>)/', $value) || preg_match('/[#$%^&*()+=\-\[\]\'\/{}|":<>?~\\\\]/', $value)) {
                return false;
            } else {
                if (preg_match('/[\p{L}\p{N} ,.-]+$/u', $value)) {
                    return true;
                } else {
                    return false;
                }
            }

        });

        Validator::extend('alpha_numeric', function ($attribute, $value) {

            if (preg_match('/(?<=<)\w+(?=[^<]*?>)/', $value) || preg_match('/[#$%^&*()+=\-\[\]\'\/{}|":<>?~\\\\]/', $value)) {
                return false;
            } else {
                if (preg_match('/[\p{L}\p{N}]+$/u', $value)) {
                    return true;
                } else {
                    return false;
                }
            }

        });


        // Validación customizada que verifica si existe el plazo
        Validator::extend('unique_duracion_plazo', function ($attribute, $value, $parameters) {

            $plazo = \DB::select("SELECT count(*) as total
            FROM plazos
            WHERE duracion ='$value' AND plazo='$parameters[0]'");

            if ($plazo[0]->total == 0) {
                return true;
            } else {
                return false;
            }

        });

        // Validación customizada que verifica si existe el rol/perfil
        Validator::extend('unique_rol', function ($attribute, $value, $parameters) {

            $rol = \DB::select("SELECT count(*) as total
            FROM roles
            WHERE name LIKE '%$parameters[0]%'");

            if ($rol[0]->total == 0) {
                return true;
            } else {
                return false;
            }

        });

        // Agregando sftp como Storage Driver
        Storage::extend('sftp', function ($app, $config) {
            return new Filesystem(new SftpAdapter($config));
        });

        Collection::macro('paginate', function($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
