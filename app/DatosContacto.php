<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosContacto extends Model
{
    protected $table = 'datos_contacto';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'apellidos',
        'puesto',
        'empresa',
        'telefono',
        'extension',
        'email',
        'comentarios'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];
}
