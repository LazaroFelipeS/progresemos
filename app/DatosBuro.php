<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosBuro extends Model
{
    protected $table = 'datos_buro';
    protected $connection = 'mysql_sitio';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'str_request',
        'str_response',
        'bc_score',
        'exclusion',
        'bc_razon1',
        'bc_razon2',
        'bc_razon3',
        'bc_error',
        'report_request',
        'icc_score',
        'icc_exclusion',
        'icc_razon1',
        'icc_razon2',
        'icc_razon3',
        'icc_error',
        'report_response',
        'micro_valor',
        'micro_razon1',
        'micro_razon2',
        'micro_razon3',
        'actualizado',
        'actualizado',
        'procesado',
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
