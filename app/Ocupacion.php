<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo que se conecta con la tabla ocupaciones
 */
class Ocupacion extends Model
{
    /**
     * Tabla a la que se conecta
     */
    protected $table = 'ocupaciones';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'ocupacion',
    ];
}
