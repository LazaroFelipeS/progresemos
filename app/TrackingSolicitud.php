<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingSolicitud extends Model
{
    protected $table = 'tracking_solicitud';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'solicitud_id',
        'status_id',
        'status',
        'status_id',
        'sub_status',
        'success',
        'descripcion',
        'extra'
    ];
}
