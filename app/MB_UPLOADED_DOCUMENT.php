<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla MB_UPLOADED_DOCUMENT
 * Guarda los datos de la imagen que se subio al repositorio de imagenes
 */
class MB_UPLOADED_DOCUMENT extends Model
{
    protected $connection = 'mysql_app';
    protected $table = 'MB_UPLOADED_DOCUMENT';
    public $timestamps = false;
    protected $primaryKey = 'idMB_UploadedDocument';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'idLoanApp',
        'email',
        'name',
        'extension',
        'ip',
        'source',
        'businessFormat',
        'createdAt'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
