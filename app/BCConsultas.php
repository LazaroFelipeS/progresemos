<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BCConsultas extends Model
{
    protected $table = 'bc_consultas';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'no_consulta',
        'consulta_clave_member_code',
        'consulta_contrato_producto',
        'consulta_fecha',
        'consulta_importe',
        'consulta_indicador_cliente',
        'consulta_moneda',
        'consulta_nombre_usuario',
        'consulta_num_tel',
        'consulta_reservado1',
        'consulta_reservado2',
        'consulta_responsabilidad'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
