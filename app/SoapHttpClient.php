<?php

namespace App;
use SoapClient;

class SoapHttpClient {

    public static function soap($url, $data, $call, $res=null, $encoding='GBK')
    {
        $soap = new SoapClient($url);
        $soap->soap_defencoding = $encoding;
        $soap->decode_utf8 = true;
        $soap->xml_encoding = $encoding;

        $result = $soap->__Call($call, array($data));

        if (is_soap_fault($result)) {
            return false;
        } else {
            if ($res === null) return $result;
            return $result->$res;
        }
    }
}

