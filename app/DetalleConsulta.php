<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleConsulta extends Model
{
    protected $table = 'detalle_consultas_mr';
    protected $connection = 'mysql_maquina_riesgos';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'ID_PROSPECT',
        'ID_SOLIC',
        'NO_CONSULTA',
        'CONSULTA_FECHA',
        'CONSULTA_CLAVE_MEMBER_CODE',
        'CONSULTA_NOMBRE_USUARIO',
        'CONSULTA_NUM_TEL',
        'CONSULTA_CONTRATO_PRODUCTO',
        'CONSULTA_MONEDA',
        'CONSULTA_IMPORTE',
        'CONSULTA_REPONSABILIDAD',
        'CONSULTA_INDICADOR_CLIENTE',
        'CONSULTA_RESERVADO2'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
