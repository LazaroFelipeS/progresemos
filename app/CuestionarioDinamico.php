<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuestionarioDinamico extends Model
{
    protected $table = 'cuestionarios_dinamicos';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];
}
