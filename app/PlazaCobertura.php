<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlazaCobertura extends Model
{
    protected $table = 'plazas_cobertura';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ciudad',
        'estado',
        'estado_abrev',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}