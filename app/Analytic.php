<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla analytics
 * Guarda los datos que se obtienen de google analytics
 */
class Analytic extends Model
{
    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'client_id',
        'email',
        'ip',
        'navegador',
        'navegador_version',
        'sistema_operativo',
        'sistema_version',
        'tipo_dispositivo',
        'marca',
        'modelo',
        'modelo_detalle',
        'idioma',
        'campaña',
        'origen',
        'medio',
        'duracion_sesion',
        'pais',
        'estado',
        'ciudad',
        'proveedor_internet',
        'datos_obtenidos',
        'fecha_hora',
        'contenido_anuncio'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
