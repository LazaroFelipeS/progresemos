<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BCCuentas extends Model
{
    protected $table = 'bc_cuentas';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'no_cuenta',
        'cuenta_clave_observacion',
        'cuenta_clave_member_code',
        'cuenta_clasif_puntualidad_de_pago',
        'cuenta_contrato_producto',
        'cuenta_cred_max_aut',
        'cuenta_fecha_actualizacion',
        'cuenta_fecha_apertura',
        'cuenta_fecha_cierre',
        'cuenta_fecha_inicio_reestructura',
        'cuenta_fecha_morosidad_mas_alta',
        'cuenta_fecha_reporte',
        'cuenta_fecha_ult_compra',
        'cuenta_fecha_ult_pago',
        'cuenta_frecuencia_pagos',
        'cuenta_garantia',
        'cuenta_hist_pagos',
        'cuenta_hist_pagos_fecha_antigua',
        'cuenta_hist_pagos_fecha_reciente',
        'cuenta_impugnado',
        'cuenta_importe_evaluo',
        'cuenta_limite_credito',
        'cuenta_modo_reporte',
        'cuenta_moneda',
        'cuenta_monto_pagar',
        'cuenta_monto_ultimo_pago',
        'cuenta_mop',
        'cuenta_nombre_usuario',
        'cuenta_num_tel',
        'cuenta_num_cuenta',
        'cuenta_num_pagos',
        'cuenta_num_pagos_vencidos',
        'cuenta_responsabilidad',
        'cuenta_saldo_actual',
        'cuenta_saldo_morosidad_mas_alta',
        'cuenta_saldo_vencido',
        'cuenta_tipo',
        'cuenta_total_pagos',
        'cuenta_total_pagos_mop2',
        'cuenta_total_pagos_mop3',
        'cuenta_total_pagos_mop4',
        'cuenta_total_pagos_mop5_plus',
        'cuenta_ult_fecha_cero',
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
