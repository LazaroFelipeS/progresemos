<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla LOAN_APPLICATION_SITIO
 * Guarda los datos de la solicitud
 */
class LOAN_APPLICATION_SITIO extends Model
{
    protected $connection = 'mysql_app';
    protected $table = 'LOAN_APPLICATION_SITIO';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'id',
        'prospecto_id',
        'solicitud_id',
        'loan_id',
        'empresa'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
