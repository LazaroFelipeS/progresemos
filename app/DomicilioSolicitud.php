<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomicilioSolicitud extends Model
{
    protected $table = 'domicilio_solicitud';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'cp',
        'calle',
        'num_exterior',
        'num_interior',
        'colonia',
        'id_colonia',
        'delegacion',
        'id_delegacion',
        'ciudad',
        'id_ciudad',
        'estado',
        'id_estado',
        'codigo_estado',
        'cobertura'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
