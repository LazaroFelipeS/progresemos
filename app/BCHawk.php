<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BCHawk extends Model
{
    protected $table = 'bc_hawk';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'hawk_tipo',
        'hawk_fecha_reporte',
        'hawk_codigo_prevencion',
        'hawk_tipo_usuario',
        'hawk_mensaje',
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [
    ];
}
