<?php

namespace App;

class FicoUtil {
    private $url = 'http://orig.alp-fico.com.br:8080/FICOAlpCloud/services/ScoringEntry?wsdl';
    private $url_test = 'http://orig.alp-fico.com.br:8090/FICOAlpCloudTest/services/ScoringEntry?wsdl';

    private $lender_id = 'prestano';
    private $password = 'HYerC1-5';
    private $key = '#w_uWt3b9&T%TMa_';

    private $iv = '1234567812345678';
    private $encoding = 'GBK';

    private $is_debug;

    public function __construct($is_debug)
    {
        $this->is_debug = $is_debug;
    }

    private function encrypt($xml)
    {
        $xml = \mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->key, $xml,
            MCRYPT_MODE_CBC, $this->iv);
        return base64_encode($xml);
    }

    private function decrypt($res)
    {
        $res = \base64_decode($res);
        $res = \mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->key, $res,
            MCRYPT_MODE_CBC, $this->iv);
        return $res;
    }

    private function parse_data($xml, $is_compressed)
    {
        $xml = $this->password.$xml;

        $xml = iconv('UTF-8', $this->encoding, $xml);
        $xml = $is_compressed ? gzencode($xml, 9, FORCE_DEFLATE) : $xml;

        $xml = $this->encrypt($xml);
        $is_compressed = intval($is_compressed);

        //echo "<br><br>lender_id:<br>".$this->lender_id;
        //echo "<br><br>is_compressed:<br>".$is_compressed;
        //echo "<br><br>xml:<br>".$xml;
        //echo "<br><br>this->password:<br>".$this->password;
        return $this->lender_id.$is_compressed.$xml;
    }

    private function parse_result($res)
    {
        $status_code = substr($res, 0, 2);
        $res = substr($res, 2);
        if ($status_code == '00')
            $res = $this->decrypt($res);
        $res = iconv($this->encoding, 'UTF-8', $res);
        return (object)array(
            'code' => $status_code,
            'result' => $res,
        );

    }

    public function request($xml, $is_compressed=false)
    {
        $data = $this->parse_data($xml, $is_compressed);

        $url = $this->is_debug ? $this->url_test : $this->url;

        //echo "<br><br>this->url:<br>".$url.'<br><br>';

        $data = array('inputXMLStr' => $data);
        $res = SoapHttpClient::soap($url, $data, 'process', 'processReturn', $this->encoding);
        return $this->parse_result($res);
    }

}
