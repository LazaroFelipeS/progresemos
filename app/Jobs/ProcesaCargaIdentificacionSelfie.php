<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Exception;
use SoapClient;
use SimpleXMLElement;
use App;
use Carbon\Carbon;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Illuminate\Support\Facades\Storage;
use App\Notifications\AltaClienteNotification;
use App\ClientesAlta;
use App\DatosIdentificacionOficial;
use App\DatosIdentificacionOficialR;
use App\W_ICAR_VALIDATION;
use App\W_LOAN_APPLICATION;
use App\Repositories\FaceMatchRepository;
use App\Repositories\CurlCaller;
use App\Repositories\DocumentosRepository;
use App\Repositories\PanelOperativoRepository;
use Image;
use DB;

class ProcesaCargaIdentificacionSelfie implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 600;
    public $tries = 1;
    protected $clienteAlta;
    protected $resultadoDocumentos;
    protected $resultadoRegistro;
    protected $curl;
    protected $altaPanel = false;
    protected $idPanelOperativo = false;
    protected $generaProcesos = true;
    protected $soapWrapper;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
        $this->curl = new CurlCaller;
        $this->soapWrapper = new SoapWrapper();
        $this->resultadoDocumentos = $this->clienteAlta->documentos_facematch === null ? false : $this->clienteAlta->documentos_facematch;
        $this->resultadoRegistro = $this->clienteAlta->registro_facematch === null ? false : $this->clienteAlta->registro_facematch;

    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle(FaceMatchRepository $facematch)
    {
        try {
            ini_set('default_socket_timeout', 700);
            $solicitudT24 = $this->clienteAlta->no_solicitud_t24;
            $panel_operativo_id = $this->clienteAlta->panel_operativo_id;
            $this->generaProcesos = true;
            if ($solicitudT24 !== null) {
                if (App::environment(['desarrollo', 'desarrollos', 'production']) && $panel_operativo_id === null) {
                    $panelOperativoRepository = new PanelOperativoRepository($this->curl);
                    $id_clienteAlta = [
                        'idp' => $this->clienteAlta->id,
                    ];
                    $response = $panelOperativoRepository->forzarAlta($id_clienteAlta);

                    if ($response['success'] == true) {
                        if (isset($response['response']->idp[0])) {
                            $this->altaPanel = true;
                            ClientesAlta::where('id', $this->clienteAlta->id)->update([
                                'panel_operativo_id' => $response['response']->idp[0],
                            ]);
                        }
                    } else {
                        $this->generaProcesos = false;
                        throw new Exception("No se pudo dar de alta en el Panel Operativo");
                    }
                }

                $prospecto_id = $this->clienteAlta->prospecto_id;
                $solicitud_id = $this->clienteAlta->solicitud_id;
                $email = $this->clienteAlta->EMAIL;
                $ruta = "/proceso_simplificado/{$prospecto_id}_{$solicitud_id}";
                $datosFaceMatch = [];

                $this->generaRegistro($datosFaceMatch);
                $this->cargaDocumentos();
                $resultadoDocumentos = ($this->resultadoDocumentos == 1 ? 'Ok' : 'Error');
                $resultadoRegistro = ($this->resultadoRegistro == 1 ? 'Ok' : 'Error');


                ClientesAlta::where('id', $this->clienteAlta->id)->update([
                    'facematch'         => 1,
                    'facematch_at'      => Carbon::now(),
                ]);

                $notificacion = new \stdClass;
                $notificacion->type = 'success_cargaineselfie';
                $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                    ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                $notificacion->content = 'Carga de Identificación/Selfie procesada con éxito'.
                    PHP_EOL.'Carga de documentos: '.$resultadoDocumentos.
                    PHP_EOL.'Registro en tablas: '.$resultadoRegistro;
                $notificacion->solicitud = false;
                $this->clienteAlta->notify(new AltaClienteNotification($notificacion));


            } else {

                $this->generaProcesos = false;
                throw new Exception("Carga de Identificación/Selfie fallida: La solicitud en T24 aún no existe");

            }

        } catch (\Exception $e) {
            // Error general del proceso de Facematch
            if ($this->generaProcesos === true) {
                $this->generaRegistro(null, $e->getMessage());
                $this->cargaDocumentos();
            }
            throw new Exception($e->getMessage());

        }

    }

    /**
     * Genera el registro en las tablas necesarias del APP y del Sitio para el control
     * del Facematch
     *
     * @param  array    $datosFaceMatch    Arreglo que contiene el resultado del Facematch
     * @param  string   $exceptionMessage  Cadena que contiene el movito por el cual el Facematch
     * no se proceso con éxito
     */
    public function generaRegistro($datosFaceMatch = null, $exceptionMessage = null) {

        try {

            $loanApp = W_LOAN_APPLICATION::updateOrCreate([
                'ID_LOAN_APP'       => $this->clienteAlta->no_solicitud_t24,
                'ID_USER'           => mb_strtolower($this->clienteAlta->EMAIL),
            ], [
                'ID_LOAN_PURPOSE'   => $this->clienteAlta->LOANPURPOSE,
                'ID_LOAN_STATUS'    => 2,
                'LOAN_AMOUNT'       => $this->clienteAlta->LOANAMOUNT,
                'APPLICATION_DATE'  => Carbon::createFromFormat('Ymd', $this->clienteAlta->fecha_alta),
                'CONTRACT_EXPIRY'   => Carbon::createFromFormat('Ymd', $this->clienteAlta->fecha_alta),
                'CREATE_DATE'       => Carbon::now(),
                'UPDATE_DATE'       => Carbon::now(),
            ]);

            if ($datosFaceMatch !== null) {

                $icarValidation = W_ICAR_VALIDATION::updateOrCreate([
                    'ID_LOAN_APP' => $this->clienteAlta->no_solicitud_t24,
                ], $datosFaceMatch);

            } else {
                $icarValidation = W_ICAR_VALIDATION::updateOrCreate([
                    'ID_LOAN_APP' => $this->clienteAlta->no_solicitud_t24,
                ], [
                    'STATUS_RESPONSE'       => 0,
                    'TEST_FACE_REC_VAL'     => 'TEST_FACE_REC_VAL: null',
                    'TEST_EXPIRY_DATE'      => 'TEST_EXPIRY_DATE: null',
                    'TEST_LOBAL_ATUH_RAT'   => 'TEST_LOBAL_ATUH_RAT: null',
                    'TEST_LOBAL_ATUH_VAL'   => 'TEST_LOBAL_ATUH_VAL: null',
                    'OBSERVATIONS'          => $exceptionMessage,
                    'EXECUTION_DATE'        => Carbon::now(),
                    'CREATED_AT'            => Carbon::now(),
                    'UPDATED_AT'            => Carbon::now()
                ]);
            }

            ClientesAlta::where('id', $this->clienteAlta->id)->update([
                'registro_facematch'    => 1,
            ]);
            $this->resultadoRegistro = true;

        } catch (\Exception $e) {
            ClientesAlta::where('id', $this->clienteAlta->id)->update([
                'registro_facematch'    => 0,
                'facematch_error'       => $this->clienteAlta->error_facematch.
                    PHP_EOL.
                    'Error al realizar el registro: '.$e->getMessage()
            ]);
            $this->resultadoRegistro = false;
        }

    }

    /**
     * Realiza el llamado al método de la clase DocumentosRepository que permite
     * subir los documentos al Panel Operativo/Repositorio de Imagenes
     */
    public function cargaDocumentos() {

        try {
            $documentos = new DocumentosRepository($this->curl);
            $prospecto_id = $this->clienteAlta->prospecto_id;
            $solicitud_id = $this->clienteAlta->solicitud_id;
            $clienteT24 = $this->clienteAlta->no_cliente_t24;
            $solicitudT24 = $this->clienteAlta->no_solicitud_t24;
            $nombre = "{$this->clienteAlta->NAME2} {$this->clienteAlta->FORMERNAME}";
            $nombre = trim(preg_replace('/\s+/',' ', $nombre));
            $apellidos = "{$this->clienteAlta->NAME1} {$this->clienteAlta->SHORTNAME}";
            $nombre_completo = "{$nombre} {$apellidos}";
            $celular = $this->clienteAlta->TELCEL;
            $email = $this->clienteAlta->EMAIL;
            $ruta = "/proceso_simplificado/{$prospecto_id}_{$solicitud_id}";

            $arregloDocumentos = [
                [
                    'tipo_documento'    => 'identificacion_oficial',
                    'detalle_documento' => 'front',
                    'url_documento'     =>  env('APP_URL')."/storage/{$ruta}_identificacion_oficial_front.jpg",
                    'formato_documento' => '.jpg'
                ],
                [
                    'tipo_documento'    => 'identificacion_oficial',
                    'detalle_documento' => 'back',
                    'url_documento'     =>  env('APP_URL')."/storage/{$ruta}_identificacion_oficial_back.jpg",
                    'formato_documento' => '.jpg'
                ],
                [
                    'tipo_documento'    => 'identificacion_oficial',
                    'detalle_documento' => 'photo',
                    'url_documento'     =>  env('APP_URL')."/storage/{$ruta}_identificacion_oficial_photo.jpg",
                    'formato_documento' => '.jpg'
                ],
            ];

            $datosDocumentos = [
                'no_solcitud_t24'   => $solicitudT24,
                'no_cliente_t24'    => $clienteT24,
                'nombre'            => $nombre,
                'apellidos'         => $apellidos,
                'nombre_completo'   => $nombre_completo,
                'celular'           => $celular,
                'email'             => $email,
                'ruta_local'        => $ruta,
                'documentos'        => $arregloDocumentos
            ];

            $documentos->subirDocumentoFTP($datosDocumentos);

            ClientesAlta::where('id', $this->clienteAlta->id)->update([
                'documentos_facematch'  => 1,
            ]);

            $this->resultadoDocumentos = true;

        } catch (\Exception $e) {
            ClientesAlta::where('id', $this->clienteAlta->id)->update([
                'documentos_facematch'  => 0,
                'facematch_error'       => $this->clienteAlta->error_facematch.
                    PHP_EOL.
                    'Error al cargar documentos: '.$e->getMessage()
            ]);

            $this->resultadoDocumentos = false;
        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Facematch fallido: '.$exception);
        $message = $exception->getMessage();
        $idAlta = $this->clienteAlta->id;
        $error = $this->clienteAlta->facematch_error;
        if ($error != '') {
            $error = $error.PHP_EOL;
        }
        ClientesAlta::where('id', $idAlta)->update([
            'facematch'         => 0,
            'facematch_error'   => $error.$message,
        ]);

        $resultadoDocumentos = ($this->resultadoDocumentos == 1 ? 'Ok' : 'Error');
        $resultadoRegistro = ($this->resultadoRegistro == 1 ? 'Ok' : 'Error');

        $notificacion = new \stdClass;
        $notificacion->type = 'error_cargaineselfie';
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
        $notificacion->content = 'Error al procesar Carga de Identificación/Selfie: '.PHP_EOL.$exception->getMessage().
            PHP_EOL.'Carga de documentos: '.$resultadoDocumentos.
            PHP_EOL.'Registro en tablas: '.$resultadoRegistro;

        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
    }
}
