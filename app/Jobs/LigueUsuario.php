<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ClientesAlta;
use App\Prospect;
use Exception;
use Log;
use App\Http\Controllers\LDAP;

class LigueUsuario implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, LDAP;

    public $timeout = 120;
    public $tries = 1;

    protected $clienteAlta;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        Log::info("Inicio de Ligue de usuario");

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];

        $datosProspecto = Prospect::where('id', $datosCliente['prospect_id'])
            ->get()
            ->toArray();

        if (count($datosProspecto) > 0) {

            $data['nombre'] = decrypt($datosProspecto[0]['nombre']);
            $data['apellido_p'] = decrypt($datosProspecto[0]['apellido_p']);
            $data['apellido_m'] = decrypt($datosProspecto[0]['apellido_m']);
            $data['cel'] = decrypt($datosProspecto[0]['cel']);
            $data['password'] = decrypt($datosProspecto[0]['encryptd']);
            $data['email'] = $datosProspecto[0]['email'];
            $data['correo'] = $datosProspecto[0]['email'];

            $login = $this->loginUserLDAP($data);
            $login = json_decode($login);

            if ($login->message == 'Usuario o password son incorrectos.') {

                ClientesAlta::where('id', $idAlta)->update([
                    'usuario_ligado' => 0,
                    'error' => $login->code.': '.$login->message
                ]);
                Log::info("Ligue de usuario fallido");

            } elseif ($login->message == 'Usuario logueado') {

                $dataUpdate['user'] = (array) $login->resultado->user;
                $dataUpdate['token'] = (array) $login->resultado->token;
                $dataUpdate['app'] = false;
                $dataUpdate['surnames'] = $data['apellido_p'].' '.$data['apellido_m'];
                $dataUpdate['idT24'] = $datosCliente['no_cliente_t24'];

                $update = $this->updateUserLDAP($dataUpdate);
                $update = json_decode($update);

                if ($update->message == 'Usuario ligado') {
                    ClientesAlta::where('id', $idAlta)->update([
                        'usuario_ligado' => 1
                    ]);
                    Log::info("Usuario ligado con éxito");
                } else {
                    $msg = '';
                    $error = $datosCliente['error'];
                    if ($error != '') {
                        $msg = $msg.$error.chr(13).'<br/>';
                    }
                    ClientesAlta::where('id', $idAlta)->update([
                        'usuario_ligado' => 0,
                        'error' => $msg.$update->code.': '.$update->message
                    ]);
                    Log::info("Ligue de usuario fallido");
                }

            }
        } else {
            ClientesAlta::where('id', $idAlta)->update([
                'usuario_ligado' => 0,
                'error' => 'No se encontro el prospecto'
            ]);
            Log::info("Ligue de usuario fallido: No se encontro el prospecto");
        }
    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Ligue de usuario fallido: '.$exception);
        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['error'];
        if ($error != '') {
            $error = $error.chr(13).'<br/>';
        }
        ClientesAlta::where('id', $idAlta)->update([
            'usuario_ligado' => 0,
            'error' => $error.$exception->getMessage()
        ]);
    }
}
