<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ClientesAlta;
use App\ClienteT24;
use App\StatusT24;
use Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Exception;
use App\Notifications\AltaClienteNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class AltaClienteAutomaticaT24 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 1;
    protected $clienteAlta;

    /**
     * Crea una nueva instancia del Job
     *
     * @return void
     */
    public function __construct(ClientesAlta $clienteAlta)
    {
        date_default_timezone_set('America/Mexico_City');
        $this->clienteAlta = $clienteAlta;
    }

    /**
     * Ejecuta el Job
     *
     * @return void
     */
    public function handle()
    {
        Log::info("Inicio de Alta de Cliente T24");

        $status_t24 = StatusT24::first();
        $cambio_fecha = false;

        if (date('Y-m-d H:i:s') >= date('Y-m-d 03:00:00') && $status_t24->fecha_alta == null) {
            $cambio_fecha = true;
            $fecha_alta = date('Ymd');
        } else {
            $status = $status_t24->toArray();
            $fecha = $status_t24['fecha_alta'];
            $fecha_alta = $status[$fecha];
        }

        $datosCliente = $this->clienteAlta->toArray();

        $soapWrapper = new SoapWrapper();
        $soapWrapper->add('T24', function ($service) {
            $service->wsdl(env('WSDL_T24'))
                ->trace(true)
                ->options([
                    'connection_timeout'        => 10,
                    'default_socket_timeout'    => 10
                ]);
        });

        $response = $soapWrapper->call('T24.CustomerInput', [
            'body' => [
                'WebRequestCommon' => [
                    'company'   => env('COMPANY_T24'),
                    'password'  => env('PASSWORD_T24'),
                    'userName'  => env('USERNAME_T24')
                ],
                'OfsFunction' => [

                ],
                'CUSTOMERPRSTINPUTWSType' => [
                    'MNEMONIC'          => $datosCliente['MNEMONIC'],
                    'gSHORTNAME'        => [
                        'SHORTNAME'     => $datosCliente['SHORTNAME']
                    ],
                    'gNAME1'            => [
                        'NAME1'         => $datosCliente['NAME1']
                    ],
                    'gNAME2'            => [
                        'NAME2'         => $datosCliente['NAME2']
                    ],
                    'gSTREET'           => [
                        'STREET'        => $datosCliente['STREET'],
                        'STREET'        => $datosCliente['STREET']
                    ],
                    'BIRTHINCORPDATE'   => $fecha_alta,
                    'CUSTOMERTYPE'      => 'ACTIVE',
                    'NOOFDEPEND'        => $datosCliente['NOOFDEPEND'],
                    'FORMERNAME'        => $datosCliente['FORMERNAME'],
                    'MARITALSTSNC'      => $datosCliente['MARITALSTSNC'],
                    'GENDERNC'          => $datosCliente['GENDERNC'],
                    'MAININCOME'        => $datosCliente['MAININCOME'],
                    'FECNACIMIENTO'     => $datosCliente['FECNACIMIENTO'],
                    'LUGNAC'            => $datosCliente['LUGNAC'],
                    'RFCCTE'            => $datosCliente['RFCCTE'],
                    'DIRNUMEXT'         => $datosCliente['DIRNUMEXT'],
                    'DIRNUMINT'         => $datosCliente['DIRNUMINT'],
                    'DIRCOLONIA'        => $datosCliente['DIRCOLONIA'],
                    'DIRDELMUNI'        => $datosCliente['DIRDELMUNI'],
                    'DIRCODPOS'         => $datosCliente['DIRCODPOS'],
                    'DIRCDEDO'          => $datosCliente['DIRCDEDO'],
                    'DIRPAIS'           => 'MX',
                    'TIPODOM'           => $datosCliente['TIPODOM'],
                    'DOMANOS'           => $datosCliente['DOMANOS'],
                    'gTEL.DOM'          => [
                        'TELDOM'        => $datosCliente['TELDOM']
                    ],
                    'gTEL.OFI'          => [
                        'TELOFI'        => $datosCliente['TELOFI']
                    ],
                    'gTEL.CEL'          => [
                        'TELCEL'        => $datosCliente['TELCEL']
                    ],
                    'EMAIL'             => $datosCliente['EMAIL'],
                    'ESTUDIOS'          => $datosCliente['ESTUDIOS'],
                    'OCUPACION'         => $datosCliente['OCUPACION'],
                    'VALCURP'           => $datosCliente['VALCURP'],
                    'DIRCIUDAD'         => $datosCliente['DIRCIUDAD'],
                    'EGRORDMEN'         => $datosCliente['EGRORDMEN'],
                    'TOTING'            => $datosCliente['prospecto_id']
                ]
            ]
        ]);

        $idAlta = $datosCliente['id'];

        Log::info('Status alta:'. $response->Status->successIndicator);

        if ($response) {

            if (isset($response->Status)) {

                if ($response->Status->successIndicator == 'Success') {

                    $idT24 = $response->CUSTOMERType->id;

                    Log::info('Alta de cliente exitosa: '.$idT24);

                    ClientesAlta::where('id', $idAlta)->update([
                        'alta_cliente'      => 1,
                        'alta_cliente_at'   => date('Y-m-d H:i:s'),
                        'fecha_alta'        => $fecha_alta,
                        'no_cliente_t24'    => $idT24
                    ]);

                    ClienteT24::updateOrCreate(
                        [
                            'rfc' => $datosCliente['MNEMONIC'],
                        ],
                        [
                            'id_cliente_t24'    => $idT24,
                            'apellido_paterno'  => $datosCliente['SHORTNAME'],
                            'apellido_materno'  => $datosCliente['NAME1'],
                            'nombre'            => $datosCliente['NAME2'],
                            'segundo_nombre'    => $datosCliente['FORMERNAME'],
                            'email'             => $datosCliente['EMAIL'],
                            'rfc_homoclave'     => $datosCliente['RFCCTE'],
                            'curp'              => $datosCliente['VALCURP'],
                            'fecha_nacimiento'  => $datosCliente['FECNACIMIENTO'],
                            'id_panel'          => $datosCliente['prospecto_id']
                        ]
                    );

                    if ($cambio_fecha == true) {
                        $status_t24->fecha_alta = 'fecha_actual';
                        $status_t24->fecha_t24 = date('Ymd');
                        $status_t24->save();
                    }

                    $notificacion = new \stdClass;
                    $notificacion->type = 'success';
                    $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
                        ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
                    $notificacion->content = 'Alta de cliente exitosa';
                    $notificacion->solicitud = false;
                    $this->clienteAlta->notify(new AltaClienteNotification($notificacion));

                } else {

                    $errores = $response->Status->messages;
                    $msg = '';

                    if (is_array($errores)) {
                        $msg = implode(", ", $errores);
                    } else {
                        $msg = $errores;
                    }

                    if (strpos($msg, 'DIR.COLONIA:1:1=ID IN FILE MISSING') !== false) {
                        $msg = $msg.chr(13).'<br>'.'Id colonia faltante: '.$this->clienteAlta->DIRCOLONIA;
                    }

                    throw new Exception($msg);

                }
            }
        }

    }

    /**
     * El Job fallido en procesar
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info('Alta  de cliente fallida: '.$exception);

        $datosCliente = $this->clienteAlta->toArray();
        $idAlta = $datosCliente['id'];
        $error = $datosCliente['error'];

        if ($error != '') {
            $error = $error.chr(13).'<br/>';
        }

        if (strpos($exception->getMessage(), 'BIRTH.INCORP.DATE:1:1=DATE MUST BE <= TODAY') !== false) {
            $job = (new AltaClienteAutomatica($this->clienteAlta))->delay(Carbon::now()->addMinutes(30));
            dispatch($job);
        }

        ClientesAlta::where('id', $idAlta)->update([
            'alta_cliente'      => 0,
            'alta_cliente_at'   => date('Y-m-d H:i:s'),
            'error'             => $error.$exception->getMessage()
        ]);

        $notificacion = new \stdClass;
        $notificacion->type = 'error';
        $notificacion->title = 'Id Prospecto: '.$this->clienteAlta->prospecto_id.
            ' | Id Solicitud: '.$this->clienteAlta->solicitud_id;
        $notificacion->content = strip_tags('Alta de cliente erronea: '.chr(13).$exception->getMessage());

        $this->clienteAlta->notify(new AltaClienteNotification($notificacion));
    }
}
