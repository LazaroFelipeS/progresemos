<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla W_USER
 * Guarda los datos del usuario
 * del app
 */
class W_USER extends Model
{
    protected $connection = 'mysql_app';
    protected $table = 'W_USER';
    public $timestamps = false;
    protected $primaryKey = 'ID_USER';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'ID_USER',
        'PHONE_NUMBER',
        'USER_T24',
        'PASSWORD_T24',
        'TEMPORARY_PASS',
        'CREATE_DATE',
        'UPDATE_DATE'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
