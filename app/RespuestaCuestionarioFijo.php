<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestaCuestionarioFijo extends Model
{
    protected $table = 'respuestas_cuestionario_fijo';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'telefono_contactar',
        'fecha_hora_contacto',
        'sms',
        'whatsapp',
        'email'
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Obtiene el prospecto de cada solicitud
     *
     * @return object Datos del prospecto
     */
    public function prospecto() {
        return $this->hasOne('App\Prospect', 'id', 'prospecto_id')
            ->select('id', 'email', 'nombre', 'apellido_p', 'apellido_m', 'cel');
    }

    /**
     * Obtiene el prospecto de cada solicitud
     *
     * @return object Datos del prospecto
     */
    public function solicitud() {
        return $this->hasOne('App\Solicitation', 'id', 'solicitud_id')
            ->select('id', 'telefono_empleo', 'tel_casa');
    }
}
