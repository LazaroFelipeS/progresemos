<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'alta-cliente-t24',
        'alta-solicitud-t24',
        'ligar-usuario-ldap',
        'enviar-email',
        'carga_documentos/procesaFaceMatch'
    ];
}
