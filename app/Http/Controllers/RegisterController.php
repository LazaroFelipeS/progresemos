<?php
namespace App\Http\Controllers;

use Artisaninweb\SoapWrapper\Facades\SoapWrapper;
use Illuminate\Http\Request;
use App\Solicitud;
use App\Prospecto;
use App\Analytic;
use App\Finalidad;
use App\Plazo;
use App\DomicilioSolicitud;
use App\DatosAdicionales;
use App\Producto;
use App\CatalogoSepomex;
use App\UserConvenio;
use App\DatosConvenio;
use App\DatosEmpleo;
use App\RespuestaMaquinaRiesgo;
use App\Situacion;
use Illuminate\Contracts\Encryption\DecryptException;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;
use App;
use Cookie;
use RFC\RfcBuilder;
use App\CodigosSMS;
use Validator;
use Hash;
use Auth;
use App\Repositories\LDAPRepository;
use App\Repositories\CalixtaRepository;
use App\Repositories\SolicitudRepository;
use App\Repositories\CognitoRepository;
use App\W_USER;

class RegisterController extends Controller
{
    private $configuracionProducto;
    private $ocupaciones;
    private $solicitudRepository;

    /**
     * Constructor de la clase
     */
    public function __construct(request $request)
    {
        $solicitud_id = null;
        $producto = null;
        $version = null;

        $prospecto = Auth::guard('prospecto')->check();
        if ($prospecto == true) {
            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $solicitud = Solicitation::with('producto')
                ->where('id', $prospecto_id)
                ->get()
                ->toArray();

            if (count($solicitud[0]['producto']) == 1) {
                $producto = $solicitud[0]['producto'][0]['id'];
                $version = $solicitud[0]['producto'][0]['pivot']['version_producto'];
            }

        }

        $this->configuracionProducto = App::call(
            'App\Http\Controllers\ProductoController@obtenerConfiguracion',
            [
                'id_producto' => $producto,
                'version' => $version
            ]
        );
        $this->ocupaciones = collect($this->configuracionProducto['ocupaciones'])->pluck('ocupacion');
        $this->solicitudRepository = new SolicitudRepository;

    }

    public function getForm(Request $request) {

        if (Auth::guard('prospecto')->check() && $request->has('paso') == false) {
            $prospecto = Auth::guard('prospecto')->user();
            $siguientePaso = $this->solicitudRepository->siguientePaso(null, $prospecto);

        } elseif ($request->has('paso')) {

            $siguientePaso = $this->solicitudRepository->siguientePaso($request->paso, null);
            $ultimoStatus = null;
            $modalConfianza = null;
            $nombreProspecto = null;
            if ($prospecto = Auth::guard('prospecto')->user()) {
                $solicitud = Solicitud::where('prospecto_id', $prospecto->id)
                    ->orderBy('created_at', 'DESC')
                    ->first();
                $ultimoStatus = $solicitud->status;
                if ($ultimoStatus == 'Registro') {
                    $modalConfianza = view("modals.modalConfianza")->render();
                    $nombreProspecto = "{$prospecto->nombres} {$prospecto->apellido_paterno} {$prospecto->apellido_materno}";
                    $siguientePaso['ultimo_status'] = $ultimoStatus;
                    $siguientePaso['modal_confianza'] = $modalConfianza;
                    $siguientePaso['nombre_prospecto'] = $nombreProspecto;
                    $siguientePaso['sesionIniciada'] = true;
                }
            }
        } else {
            $siguientePaso = [
                'formulario'    => 'simulador',
                'oculta'        => null
            ];
        }

        return response()->json($siguientePaso);
    }

    public function validaciones(Request $request, $formulario = null) {
       
        if ($request->has('datos_registro')) {

            $datos = $request->datos_registro;
            $formulario = 'registro';
    
            $validaciones = $this->solicitudRepository->validacionesSolicitud($datos, $formulario);
           
            if ($validaciones->fails()) {
                return response()->json([
                    'success'   => false,
                    'errores'   => $validaciones->errors()
                ]);
            } else {
               
            }

        } elseif ($formulario != null) {

            $validaciones = $this->solicitudRepository->validacionesSolicitud($request->toArray(), $formulario);
            if ($validaciones->fails()) {
                return [
                    'success'   => false,
                    'errores'   => $validaciones->errors()
                ];
            }

        } else {

            return response()->json([
                'success' => false,
                'message' => 'Operacion no permitida'
            ]);

        }

    }

    /**
     * Registra el prospecto en la base de datos
     *
     * @param  request $request Contiene los datos enviados por el usuario en el
     * formulario
     *
     * @return  json Respuesta de registrar el prospecto
     */
    public function registroProspecto(Request $request, LDAPRepository $ldap) {

        $reintento = true;
        $no_intentos = 1;
        $intentos = 0;

        $validaciones = $this->validaciones($request, 'registro');
        
        if ($validaciones != '') {
            return response()->json($validaciones);
        }
        
        $datos_registro = $request->datos_registro;
        $cognito_id = null;
        $responseCognito = $this->registroCognito($request);
        
        if ($responseCognito['success'] == false) {
            return response()->json($responseCognito);
        } else {
            $cognito_id = $responseCognito['cognito_id'];
        }

        $prospecto = Prospecto::firstOrCreate(
            [
            'email' => $datos_registro['email']
            ]
        );

        // Si el prospecto fue creado recientemente, se actualizan atributos
        // faltantes y se crea solicitud
        if ($prospecto->wasRecentlyCreated) {

            $prospecto->nombres               = $datos_registro['nombre'];
            $prospecto->apellido_paterno      = $datos_registro['apellidopaterno'];
            $prospecto->apellido_materno      = $datos_registro['apellidomaterno'];
            $prospecto->celular               = $datos_registro['celular'];
            $prospecto->password              = Hash::make($datos_registro['password']);
            $prospecto->encryptd               = encrypt($datos_registro['password']);
            $prospecto->usuario_ldap          = 0;
            $prospecto->sms_verificacion      = 0;
            $prospecto->usuario_confirmado    = 0;
            $prospecto->cognito_id            = $cognito_id;
            $prospecto->save();

            $w_user = W_USER::updateOrCreate([
                'ID_USER'       => mb_strtolower($datos_registro['email'])
            ], [
                'PHONE_NUMBER'  => $datos_registro['celular'],
                'CREATE_DATE'   => Carbon::now(),
                'UPDATE_DATE'   => Carbon::now()
            ]); 

            // Obteniendo la descripcion de la finalidad
            $finalidad = $datos_registro['finalidad'];
            $finalidad = Finalidad::find($finalidad);
            if (count($finalidad) == 1) {
                $finalidad = mb_strtoupper($finalidad->finalidad);
            }

            // Obteniendo la clave del plazo
            $plazo = $datos_registro['plazo'];
            $duracion = null;
            $clave_plazo = null;
            $plazo = Plazo::find($plazo);

            if (count($plazo) == 1) {
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            } else {
                $plazo = Plazo::find(1);
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            }

            $solicitud = Solicitud::create(
                [
                    'status'            => 'Registro',
                    'sub_status'        => 'Crear Usuario',
                    'prospecto_id'      => $prospecto['id'],
                    'prestamo'          => $datos_registro['monto'],
                    'plazo'             => $datos_registro['plazo'],
                    'finalidad'         => $datos_registro['finalidad'],
                    'pago_estimado'     => $datos_registro['pago'],
                    'status_id'         => 1, // Status para crear usuario
                ]
            );

            $cognito = new CognitoRepository;
            $responseCognito = $cognito->setUserAttributes($request->email, ['custom:idProspecto' => 'PRO-'.$prospecto->id]);

            Auth::guard('prospecto')->loginUsingId($prospecto->id);

            $producto_id = $this->configuracionProducto['id'];
            $version_producto = $this->configuracionProducto['version_producto'];

            $solicitud->producto()->attach([
                $producto_id => [
                    'version_producto' => $version_producto,
                    'lead'             => null,
                    'lead_id'          => null,
                ]
            ]);

          
            $agent = new Agent();
            $tipo_dispositivo = null;
            $marca =  null;

            if ($agent->isDesktop()) {
                $tipo_dispositivo = 'desktop';
                $marca = $agent->device();
            }
            if ($agent->isMobile()) {
                $tipo_dispositivo = 'mobile';
                $marca = $agent->device();
            }
            if ($agent->isTablet()) {
                $tipo_dispositivo = 'tablet';
                $marca = $agent->device();
            }

            $analytic = Analytic::insert([
                'prospecto_id'      => $prospecto['id'],
                'solicitud_id'      => $solicitud->id,
                //'client_id'         => $request->input('id'),
                'email'             => $datos_registro['email'],
                'ip'                => self::getUserIP(),
                'telefono'          => $datos_registro['celular'],
                'sistema_operativo' => $agent->platform(),
                'navegador'         => $agent->browser(),
                'tipo_dispositivo'  => $tipo_dispositivo,
                'marca'             => $marca,
                'datos_obtenidos'   => 'Sin datos',
                'fecha_hora'        => date('YmdHi'),
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            ]);

            $mnsj_str = [
                'success'           => true,
                'login'             => false,
                'nueva_solicitud'   => true,
                'plazo'             => $datos_registro['plazo'],
            'prestamo'              => $datos_registro['monto'],
                'msj'               => 'Prospecto y solicitud creados',
                'siguiente_paso'    => 'sms',
                'prospecto_id'      => $prospecto->id
            ];
/*             return response()->json([
                'success'           => true,
                'msj'               => 'Prospecto y solicitud creados',
                'siguiente_paso'    => 'sms',
                'prospecto_id'      => $prospecto->id,
                'solicitud_id'      => $solicitud->id,
            ]); */
            $name_string = $datos_registro['nombre'].' '.$datos_registro['apellidopaterno'].' '.$datos_registro['apellidomaterno'];
            $this->solicitudRepository->setUltPuntoReg($solicitud->id,
                $solicitud->status,
                'Crear Usuario',
                1,
                'Usuario y Solicitud creados con éxito',
                ['prospecto_id' => $prospecto->id, 'solicitud_id' => $solicitud->id, 'nombre' => $name_string]
            );

            $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
            $solicitud->save();

            $eventTM[] = ['event'=> 'RegistroUsuario', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
            $mnsj_str['eventTM'] = $eventTM;

            return response()->json($mnsj_str, 201);

        } elseif($prospecto->id) {

            if ($prospecto->cognito_id == null) {
                $prospecto->cognito_id = $cognito_id;
                $prospecto->save();
            } else {

                return response()->json([
                    'success'           => false,
                    'msg'               => 'El usuario ya esta registrado',
                    'siguiente_paso'    => 'login'
                ]);
            }

        } else {

            return response()->json([
                'success' => false,
                'msg'     => 'Hubo un error al crear el prospecto y la solicitud',
            ]);

        }

    }


    public function registroSolicitud(request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::findOrFail($prospecto_id);

            // Obteniendo la descripcion de la finalidad
            $finalidad = $request->input('finalidad');
            $finalidad = Finalidad::find($finalidad);
            if (count($finalidad) == 1) {
                $finalidad = mb_strtoupper($finalidad->finalidad);
            }

            // Obteniendo la clave del plazo
            $plazo = $request->input('plazo');
            $duracion = null;
            $clave_plazo = null;
            $plazo = Plazo::find($plazo);

            if (count($plazo) == 1) {
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            } else {
                $plazo = Plazo::find(1);
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            }

            $solicitud = Solicitud::updateOrCreate([
                'status'            => 'Registro',
                'sub_status'        => 'Crear Usuario',
                'prospecto_id'      => $prospecto->id,
                'prestamo'          => $request->monto,
                'plazo'             => $clave_plazo,
                'pago_estimado'     => $request->pago_estimado,
                'finalidad'         => $finalidad,
                'finalidad_custom'  => $request->finalidad_custom,
            ]);

            $mnsj_str = [
                'success'           => true,
                'login'             => true,
                'nueva_solicitud'   => true,
                'prospecto_id'      => $prospecto->id,
                'celular'           => $prospecto->celular,
            ];

            if ($solicitud->wasRecentlyCreated) {
                $r_parameter = 'none';
                $campaña = 'none';
                if (Cookie::get('r_parameter') !== null && Cookie::get('campaña') !== null) {
                    $campaña = Cookie::get('campaña') == 'askrobin' ? 'askrobin' : 'none';
                    $r_parameter = Cookie::get('r_parameter');
                }
                $campaña = $campaña == 'none' ? null : $campaña;
                $r_parameter = $r_parameter == 'none' ? null : $r_parameter;
                $producto_id = $this->configuracionProducto['id'];
                $version_producto = $this->configuracionProducto['version_producto'];

                $solicitud->producto()->attach([
                    $producto_id => [
                        'version_producto' => $version_producto,
                        'lead'             => $campaña,
                        'lead_id'          => $r_parameter,
                    ]
                ]);

                Cookie::queue(Cookie::forget('campaña'));
                Cookie::queue(Cookie::forget('r_parameter'));

                if ($request->has('sucursal')) {
                    $sucursal = $request->sucursal;
                    $datosSucursal = UserConvenio::find($sucursal);
                    if (count($datosSucursal) == 1) {
                        $datosConvenio = DatosConvenio::updateOrCreate([
                            'prospecto_id'  => $prospecto->id,
                            'solicitud_id'  => $solicitud->id
                        ], [
                            'empresa'   => $datosSucursal->empresa,
                            'sucursal'  => $datosSucursal->sucursal,
                            'embajador' => $datosSucursal->embajador,
                            'telefono'  => $datosSucursal->telefono,
                            'email'     => $datosSucursal->email
                        ]);
                    }
                }

                $agent = new Agent();
                $tipo_dispositivo = null;
                $marca =  null;

                if ($agent->isDesktop()) {
                    $tipo_dispositivo = 'desktop';
                    $marca = $agent->device();
                }
                if ($agent->isMobile()) {
                    $tipo_dispositivo = 'mobile';
                    $marca = $agent->device();
                }
                if ($agent->isTablet()) {
                    $tipo_dispositivo = 'tablet';
                    $marca = $agent->device();
                }

                $analytic = Analytic::insert([
                    'prospecto_id'      => $prospecto->id,
                    'solicitud_id'      => $solicitud->id,
                    'client_id'         => $request->input('clientId'),
                    'email'             => $prospecto->email,
                    'ip'                => self::getUserIP(),
                    'telefono'          => $prospecto->celular,
                    'sistema_operativo' => $agent->platform(),
                    'navegador'         => $agent->browser(),
                    'tipo_dispositivo'  => $tipo_dispositivo,
                    'marca'             => $marca,
                    'datos_obtenidos'   => 'Sin datos',
                    'fecha_hora'        => date('YmdHi'),
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s')
                ]);

                $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
                $solicitud->save();

                $name_string = $request->input('nombres').' '.$request->input('apellido_paterno').' '.$request->input('apellido_materno');
                $this->solicitudRepository->setUltPuntoReg($solicitud->id,
                    $solicitud->status,
                    'Crear Usuario',
                    1,
                    'Nueva solicitud creada con éxito',
                    ['prospecto_id' => $prospecto->id, 'solicitud_id' => $solicitud->id, 'nombre' => $name_string]
                );

            }

            session()->forget('nueva_solicitud');
            return response()->json($mnsj_str, 201);


        } else {

            return response()->json([
                'message' => 'La sessión expiro',
                'success' => false,
            ]);

        }

    }

    /**
     * Crea el usuario en LDAP
     * @param  Request        $request  Datos del prospecto
     * @param  LDAPRepository $ldap     Repositorio con las funcionalidades de LDAP
     * @return json                     Resultado de crear el usuario en LDAP
     */
    public function registroLDAP(Request $request, LDAPRepository $ldap) {
        
        $datos_prospecto = $request->datos_prospecto;
        //$prospecto_id = $datos_registro['prospecto_id'];

        $datos_usuario = [
            'nombres'               => $datos_prospecto['nombre'],
            'apellido_paterno'      => $datos_prospecto['apellido_paterno'],
            'apellido_materno'      => $datos_prospecto['apellido_materno'],
            'email'                 => $datos_prospecto['email'],
            'celular'               => $datos_prospecto['celular'],
            'password'              => $datos_prospecto['contraseña'],
        ];

        $responseldap = $ldap->createUserLDAP($datos_usuario);
        $responseldap = json_decode($responseldap);

        if ($responseldap->message == 'Error, el proceso de creacion de usuario en LDAP fallo, LDAP:Entry Already Exists') {

            return [
                'success'           => true,
                'message'           => 'Usuario ya existe en LDAP, se registro antes en FISA o Prestanómico',
                'existiaLDAP'       => true,
                'responseLDAP'      => $responseldap
            ];

        } elseif ($responseldap->message == 'Error de sistema, No response from the LDAP service') {

            return [
                'success'           => false,
                'message'           => 'No hubo respuesta en LDAP',
                'existiaLDAP'       => false,
                'responseLDAP'      => $responseldap
            ];


        } elseif ($responseldap->message == 'Usuario creado') {

            return [
                'success'       => true,
                'message'       => 'Usuario creado en LDAP.',
                'existiaLDAP'   => false,
                'responseLDAP'  => $responseldap
            ];

        } else {

            return [
                'success'       => false,
                'message'       => $responseldap->message,
                'existiaLDAP'   => false,
                'responseLDAP'  => $responseldap
            ];

        }

    }

    public function registroCognito(Request $request) {

        $cognito = new CognitoRepository;

        if ($request->datos_registro['apellidomaterno'] != '')  {
            $apellidos = mb_strtoupper("{$request->datos_registro['apellidopaterno']} {$request->datos_registro['apellidomaterno']}");
        } else {
            $apellidos = mb_strtoupper($request->datos_registro['apellidopaterno']);
        }
        $fecha = Carbon::now('UTC');

        $datos_usuario = [
            'name'                  => mb_strtoupper($request->datos_registro['nombre']),
            'middle_name'           => $apellidos,
            'phone_number'          => '+52'.$request->datos_registro['celular'],
        ];

        $cognito = $cognito->createUserCognito(mb_strtolower($request->datos_registro['email']), $request->datos_registro['password'], $datos_usuario);

        return $cognito;

    }

    /**
     * Envia SMS de verificación al prospecto
     * @param  Request           $request  Datos del prospecto
     * @param  CalixtaRepository $calixta  Repositorio con las funcionalidades de Calixta
     * @return json                        Resultado del envío del SMS
     */
    public function envioSMS(Request $request, CalixtaRepository $calixta) {

        $prospecto_id = $request->datos_registro['prospecto_id'];
        $celular = $request->datos_registro['celular'];

        $codigo = mt_rand(10000, 99999);
        $msj = "Código de Verificación: ". $codigo;
        $responsecalixta = $calixta->sendSMS($celular, $msj);
        $responsecalixta = json_decode($responsecalixta);

        if ($responsecalixta->success == true) {

            $prospecto = Prospecto::findOrFail($prospecto_id);

            $codigo_sms = CodigosSMS::create([
                'prospecto_id'  => $prospecto_id,
                'codigo'        => $codigo,
                'celular'       => $celular,
                'nombre'        => $prospecto->nombre.' '.$prospecto->apellidopaterno,
                'email'         => $request->email,
                'enviado'       => $responsecalixta->success,
                'descripcion'   => $responsecalixta->message,
                'verificado'    => false,
            ]);

            $prospecto->sms_verificacion = true;
            $prospecto->save();

            return response()->json([
                'success'           => true,
                'message'           => 'Código de verificacón reenviado con éxito.',
                'celular'           => $celular
            ]);

        } else {

            if ($responsecalixta->response_code == '6' || $responsecalixta->response_code == '10') {
                return response()->json([
                    'success'               => false,
                    'message'               => $responsecalixta->message,
                    'actualizar_celular'    => true,
                    'celular'               => $celular
                ]);
            } else {
                return response()->json([
                    'success'               => false,
                    'message'               => $responsecalixta->message,
                ]);
            }

        }

    }

    public function confSMSCode(request $request, CalixtaRepository $calixta) {

        if (Auth::guard('prospecto')->check()) {
            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $conf_code = $request->input('codigo_confirmacion');
            $is_login = $request->input('is_login');

            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            $codigos = CodigosSMS::select('codigo')
                ->where('prospecto_id', $prospecto_id)
                ->where('verificado', 0)
                ->where('enviado', 1)
                ->latest()
                ->limit(5)
                ->get();

            // Obteniendo el arreglo solo de los códigos enviados
            $existe_codigo = $codigos->where('codigo', $conf_code)->toArray();

            if (count($existe_codigo)) {

                CodigosSMS::select('codigo')
                    ->where('prospecto_id', $prospecto_id)
                    ->where('verificado', 0)
                    ->where('enviado', 1)
                    ->update(['verificado' => 1]);

                $prospecto->usuario_confirmado = 1;
                $prospecto->save();

                if ($solicitud->status == 'Registro') {
                    $codigo_enviado = $calixta->sendSMS(
                        $prospecto->celular,
                        env('SMS_CONFIANZA')
                    );
                }

                if ($prospecto->login == false) {
                    
                    $cognito = new CognitoRepository;
                    $responseCognito = $cognito->setUserAttributes($prospecto->email, ['phone_number_verified' => 'true']);
                    //dd($responseCognito);
                    $solicitud->status = 'Registro Confirmado';
                    $solicitud->sub_status = 'Validar SMS';
                    $solicitud->save();

                    $mnsj_str = [
						'success'         => true,
						'message'         => 'El usuario confirmó su celular con éxito.',
                        'siguiente_paso'  => $this->solicitudRepository->siguientePaso(null, $prospecto)
					];

                    $this->solicitudRepository->setUltPuntoReg(
                        $solicitud->id,
                        'Registro Confirmado',
                        'Validar SMS',
                        1,
                        'El usuario confirmó su celular con éxito.',
                        ['cel' => $prospecto->cel, 'code' => '']
                    );
                    $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));

                } else {

                    $prospecto->login = false;
                    $prospecto->save();

                    if ($solicitud->status == 'Registro') {

                        $solicitud->status = 'Registro Confirmado';
                        $solicitud->sub_status = 'Validar SMS';
                        $solicitud->save();

                        $this->solicitudRepository->setUltPuntoReg(
                            $solicitud->id,
                            'Registro Confirmado',
                            'Validar SMS',
                            1,
                            'El usuario confirmó su celular con éxito al hacer Login',
                            ['cel' => $prospecto->cel, 'code' => '']
                        );

                    }

                    $mnsj_str = [
                        'success'           => true,
                        'message'           => 'Login: El usuario confirmó su celular con éxito.',
                    ];
                    $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
                    
                    $siguiente_paso = $this->solicitudRepository->siguientePaso(null, $prospecto);
                    if (isset($siguiente_paso['cuestionario'])) {
                        $mnsj_str['cuestionario'] = $siguiente_paso['cuestionario'];
                    }
                    $mnsj_str['siguiente_paso'] = $siguiente_paso;

                }

                return response()->json($mnsj_str);

            } else {

                if ($prospecto->login == false) {
                    $mnsj_str = [
                        'success'       => false,
                        'message'       => 'Código Inválido',
                        'prospect_id'   => $prospecto_id
					];

                    $this->solicitudRepository->setUltPuntoReg(
                        $solicitud->id,
                        $solicitud->status,
                        'Validar SMS',
                        0,
                        'El usuario presentó un código inválido.',
                        ['celular' => $prospecto->celular, 'codigo' => $conf_code]
                    );

                } else {

                    $mnsj_str = [
                        'success'   => false,
                        'message'   => 'Código Inválido',
                    ];

                }

                $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
                $solicitud->save();
                return response()->json($mnsj_str);

            }

        } else {
            return response()->json([
                'success'   => false,
                'message' => 'La sesión expiro'
            ], 401);
        }
    }

    /**
     * Reenvia el SMS con el código de activación al usuario
     *
     * @param  request $request Contiene los datos del usuario
     *
     * @return json Resultado del envío del SMS
     */
    public function resendSMSCode(request $request, CalixtaRepository $calixta) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);

            $codigo_activacion = CodigosSMS::where('prospecto_id', $prospecto_id)
                                    ->get()
                                    ->last();

            $msj = "Código de Verificación: ". $codigo_activacion->codigo;
            $responsecalixta = $calixta->sendSMS($prospecto->celular, $msj);
            $responsecalixta = json_decode($responsecalixta);

            $msj_str = [];
            if ($responsecalixta->success == true) {

                return response()->json([
                    'success'               => true,
                    'message'               => 'Código de verificacón reenviado con éxito',
                ]);

            } else {

                if ($responsecalixta->response_code == '6' || $responsecalixta->response_code == '10') {
                    return response()->json([
                        'success'               => false,
                        'message'               => $responsecalixta->message,
                        'actualizar_celular'    => true,
                        'celular'               => $prospecto->celular
                    ]);
                } else {
                    return response()->json([
                        'success'               => false,
                        'message'               => $responsecalixta->message,
                    ]);
                }

            }

        } else {

            return response()->json([
                'success'   => false,
                'message' => 'La sesión expiro'
            ], 401);

        }
    }

    /**
     * Guarda los datos capturados en la sección Domicilio
     *
     * @param  request $request Arreglo que contiene los datos capturados
     *
     * @return json             Resultdo del guardado de datos
     */
    public function regDomicilio(request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();
            //dd($request->datos);
            if ($solicitud) {
                 $validaciones = $this->validaciones($request, 'domicilio');
                if ($validaciones == '') {
                    return response()->json($validaciones);
                }  

                // Guardando los datos de la solicitud desencriptados en la nueva tabla
                $domicilio = DomicilioSolicitud::firstOrNew(
                    [
                    'prospecto_id'  => $prospecto->id,
                    'solicitud_id'  => $solicitud->id
                ]);

                $domicilio->cp              = $request->datos['codigo_postal'];
                $domicilio->calle           = mb_strtoupper($request->datos['calle']);
                $domicilio->num_exterior    = mb_strtoupper($request->datos['no_exterior']);
                $domicilio->num_interior    = mb_strtoupper($request->datos['no_int']);
                $domicilio->colonia         = mb_strtoupper($request->datos['colonia']);
                $domicilio->id_colonia      = $request->datos['id_estado'].$request->datos['id_delegacion_municipio'].$request->datos['codigo_postal'].$request->datos['id_colonia'];
                $domicilio->delegacion      = mb_strtoupper($request->datos['delegacion']);
                $domicilio->id_delegacion   = $request->datos['id_delegacion_municipio'];
                $domicilio->ciudad          = mb_strtoupper($request->datos['ciudad']);
                $domicilio->id_ciudad       = $request->datos['id_ciudad'];
                $domicilio->estado          = mb_strtoupper($request->datos['estado']);
                $domicilio->id_estado       = $request->datos['id_estado'];
                $domicilio->codigo_estado   = $request->datos['codigo_estado'];
                $domicilio->cobertura       = $request->datos['cobertura'];
                $domicilio->save();

                // Se actualiza la solicitud con los datos del domicilio
                $dom_string = $request->datos['calle'].' '
                    .$request->datos['no_exterior'].' '
                    .$request->datos['no_int'].', '
                    .$request->datos['colonia'].', '
                    .$request->datos['delegacion'].', '
                    .$request->datos['codigo_postal'].', '
                    .$request->datos['ciudad'].', '
                    .$request->datos['estado'];

                // Actualizando el campo ult_punto_reg de la solicitud
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Domicilio',
                    1,
                    'Los datos de domicilio se han guardado con éxito',
                    ['domicilio' => $dom_string]
                );

                $mnsj_str = [
                    'success'       => true,
                    'domicilio'     => $dom_string
                ];

                // Guardando el mensaje en el campo ult_mensj_a_usuario
                $this->solicitudRepository->setUltMnsjUsr(
                    $solicitud,
                    json_encode($mnsj_str)
                );
                $solicitud->sub_status = 'Domicilio';
                $solicitud->save();

                return response()->json($mnsj_str);
            } else {
                return response()->json([
                    'success'   => false,
                    'message'   => 'La solicitud no existe'
                ]);
            }

        } else {
            return response()->json([
                'success'   => false,
                'message' => 'La sesión expiro'
            ]);
        }
    }

    /**
     * Guarda los datos capturados en la sección Datos Personales
     *
     * @param  request $request Arreglo que contiene los datos capturados
     *
     * @return json             Resultado del guardado de datos
     */
    public function datosPersonales(request $request)
    {
        if (Auth::guard('prospecto')->check()) {
            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $conf_code = $request->input('conf_code');

            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            if ($solicitud) {

                /* $validaciones = $this->validaciones($request, 'datos_personales');
                if ($validaciones != '') {
                    return response()->json($validaciones);
                } */
                //dd($request);
                $fechaNacimiento = str_replace('.','-',$request->datos['fecha_nacimiento']);
                $fecha_nacimiento = Carbon::createFromFormat('d-m-Y',$fechaNacimiento)->format('Y-m-d');
                $rfc = $request->datos['rfc'];
                $error_rfc = $request->datos['error_rfc'];
                
                if ($rfc == '' || $error_rfc != '') {

                    try {

                        $builder = new RfcBuilder();
                        $dia = $fecha_nacimiento->format('d');
                        $mes = $fecha_nacimiento->format('m');
                        $año = $fecha_nacimiento->format('Y');
                        $nombre = ($solicitud->prospecto->nombres);
                        $apellido_paterno = $solicitud->prospecto->apellido_paterno;
                        $apellido_materno = $solicitud->prospecto->apellido_materno;
                        if ($apellido_materno == 'XXX') {
                            $apellido_materno = '';
                        }

                        $rfc = $builder->name($nombre)
                            ->firstLastName($apellido_paterno)
                            ->secondLastName($apellido_materno)
                            ->birthday($dia, $mes, $año)
                            ->build()
                            ->toString();

                        $rfc = substr($rfc, 0, 10);

                        $mnsj_str = [
                            'success'   => true,
                            'message'   => 'RFC calculado: '.$rfc
                        ];

                        // Guardando el mensaje en el campo ult_mensj_a_usuario
                        $this->solicitudRepository->setUltMnsjUsr(
                            $solicitud,
                            json_encode($mnsj_str)
                        );

                    } catch (\Exception $e) {

                        $mnsj_str = [
                            'success'   => false,
                            'message'   => 'Error al calcular RFC: '. $e->getMessage(),
                        ];

                        // Guardando el mensaje en el campo ult_mensj_a_usuario
                        $this->solicitudRepository->setUltMnsjUsr(
                            $solicitud,
                            json_encode($mnsj_str)
                        );
                        $rfc = '';
                    }

                } else {

                    $mnsj_str = [
                        'success'   => true,
                        'message'   => 'RFC Capturado.',
                    ];

                    // Guardando el mensaje en el campo ult_mensj_a_usuario
                    $this->solicitudRepository->setUltMnsjUsr(
                        $solicitud,
                        json_encode($mnsj_str)
                    );
                    $solicitud->save();
                }

                // Actualiza los datos personales de la solicitud
                $solicitud->sexo                    = $request->datos['sexo'];
                $solicitud->fecha_nacimiento        = $fecha_nacimiento;
                $solicitud->lugar_nacimiento_estado = mb_strtoupper($request->datos['estado_nacimiento']);
                $solicitud->lugar_nacimiento_ciudad = mb_strtoupper($request->datos['ciudad_nacimiento']);
                $solicitud->estado_civil            = $request->datos['estado_civil'];
                $solicitud->nivel_estudios          = $request->datos['nivel_estudios'];
                $solicitud->rfc                     = mb_strtoupper($rfc);
                //$solicitud->curp                    = mb_strtoupper($request->datos['curp']);
                $solicitud->telefono_casa           = $request->datos['telefono_casa'];

                $mnsj_str = [
                    'success'   => true,
                    'message'   => 'Los datos personales se han guardado con éxito.',
                ];

                // Actualizando el campo ult_punto_reg de la solicitud
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Datos Personales',
                    1,
                    'Los datos personales se han guardado con éxito.');

                // Guardando el mensaje en el campo ult_mensj_a_usuario
                $this->solicitudRepository->setUltMnsjUsr(
                    $solicitud,
                    json_encode($mnsj_str)
                );

                $solicitud->sub_status = 'Datos Personales';
                $solicitud->save();

                return response()->json($mnsj_str);
            } else {
                return response()->json([
                    'success'   => false,
                    'message'   => 'La solicitud no existe'
                ]);
            }
        } else {
            return response()->json([
                'success'   => false,
                'message' => 'La sesión expiro'
            ]);
        }
    }

    /**
     * Guarda los datos capturados en la sección Cuentas de crédito
     *
     * @param  request $request Arreglo que contiene los datos capturados
     *
     * @return json             Resultado del guardado de los datos
     */
    public function cuentasDeCredito(request $request)
    {
        $datos_buro = $request['datos_buro'];
        if (Auth::guard('prospecto')->check()) {
            $prospecto_id = Auth::guard('prospecto')->user()->id;

            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            if ($solicitud) {
                // Actualizando los campos de cuentas de crédito para esta solicitud
                $solicitud->credito_hipotecario = ($datos_buro['credito_hipotecario'] == 'Si') ? 1 : 0;
                $solicitud->credito_automotriz  = ($datos_buro['credito_automotriz'] == 'Si') ? 1 : 0;
                $solicitud->credito_bancario    = ($datos_buro['credito_bancario'] == 'Si') ? 1 : 0;
                $solicitud->ultimos_4_digitos   = ($datos_buro['digitos_tarjeta']);
                $solicitud->autorizado          = ($datos_buro['acepto_consulta']) ? 1 : 0;

                if (count($solicitud->producto) > 0) {
                    $lead = $solicitud->producto[0]['pivot']['lead'];
                    $lead_id = $solicitud->producto[0]['pivot']['lead_id'];
                } else {
                    $lead = null;
                    $lead_id = null;
                }

                if ($lead == null && $lead_id == null) {
                    $lead = 'none';
                    $lead_id = 'none';
                }

                $mnsj_str = [
    				'success'           => true,
    				'message'           => 'Los datos de cuentas de crédito se han guardado con éxito',
    				'prospecto_id'      => $prospecto->id,
    				'solicitud_id'      => $solicitud->id,
                    'lead'              => $lead,
                    'lead_id'           => $lead_id,
                    'siguiente_paso'    => 'primera_llamada_bc',
    			];

                // Actualizando el ultimo punto de registro de la solicitud
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Cuentas de Crédito',
                    1,
                    'Los datos de cuentas de crédito se han guardado con éxito'
                );

                // Guardando el mensaje en el campo ult_mensj_a_usuario field
                $this->solicitudRepository->setUltMnsjUsr(
                    $solicitud,
                    json_encode($mnsj_str)
                );

                $solicitud->sub_status = 'Cuentas de Crédito';
                $solicitud->save();

                $eventTM = ['event' => 'AutorizaCB', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                $mnsj_str['eventTM'] = $eventTM;
                return response()->json($mnsj_str);

            } else {

                $mnsj_str = [
    				'success'           => false,
    				'message'           => 'La solicitud no existe',
    			];
                return response()->json($mnsj_str);

            }
        } else {
            return response()->json([
                'success'   => false,
                'message' => 'La sesión expiro'
            ]);
        }
    }

    public function correccionBC(request $request)
    {
        //dd($request[0]['value']);
        if (Auth::guard('prospecto')->check()) {
            $prospecto_id = Auth::guard('prospecto')->user()->id;

            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            if ($solicitud) {

                $fecha_nacimiento = Carbon::createFromFormat('d-m-Y', $request[0]['value']);
                
                $rfc = $request->input('rfc');
                $message = '';

                if ($rfc == '') {

                    try {
                        $builder = new RfcBuilder();
                        $dia = $fecha_nacimiento->format('d');
                        $mes = $fecha_nacimiento->format('m');
                        $año = $fecha_nacimiento->format('Y');
                        $nombre = ($solicitud->prospecto->nombres);
                        $apellido_paterno = $solicitud->prospecto->apellido_paterno;
                        $apellido_materno = $solicitud->prospecto->apellido_materno;
                        if ($apellido_materno == 'XXX') {
                            $apellido_materno = '';
                        }

                        $rfc = $builder->name($nombre)
                            ->firstLastName($apellido_paterno)
                            ->secondLastName($apellido_materno)
                            ->birthday($dia, $mes, $año)
                            ->build()
                            ->toString();

                        $rfc = substr($rfc, 0, 10);
                        $message = ' RFC calculado: '.$rfc;

                    } catch (\Exception $e) {

                        $mnsj_str = [
                            'success'   => false,
                            'message'   => 'Error al calcular RFC: '. $e->getMessage(),
                        ];

                        // Guardando el mensaje en el campo ult_mensj_a_usuario
                        $this->solicitudRepository->setUltMnsjUsr(
                            $solicitud,
                            json_encode($mnsj_str)
                        );
                        $rfc = '';
                    }
                } else {
                    $rfc = $request->input('rfc');
                    if ($request->input('homoclave') != '') {
                        $rfc .= $request->homoclave;
                    }
                }

                $solicitud->fecha_nacimiento = $fecha_nacimiento;
                $solicitud->rfc = $rfc;
                $solicitud->save();

                $mnsj_str = [
    				'success'           => true,
    				'message'           => 'Se actualiza el RFC/Fecha de nacimiento.'.$message,
    				'prospecto_id'      => $prospecto->id,
    				'solicitud_id'      => $solicitud->id,
                    'siguiente_paso'    => 'primera_llamada_bc',
    			];

                // Guardando el mensaje en el campo ult_mensj_a_usuario field
                $this->solicitudRepository->setUltMnsjUsr(
                    $solicitud,
                    json_encode($mnsj_str)
                );

                return response()->json($mnsj_str);

            } else {

                $mnsj_str = [
    				'success'           => false,
    				'message'           => 'La solicitud no existe',
    			];
                return response()->json($mnsj_str);

            }
        } else {
            return response()->json([
                'success'   => false,
                'message' => 'La sesión expiro'
            ]);
        }

    }

    /**
     * Guarda los datos capturados en la sección de Datos Adicionales
     *
     * @param  request $request Arreglo que contiene los datos capturados
     *
     * @return json             Resultado del guardado de datos
     */
    public function datosAdicionales(request $request) {
        //dd($request->datos_adicionales["ocupacion"]);
        if (Auth::guard('prospecto')->check()) {
            $prospecto_id = Auth::guard('prospecto')->user()->id;

            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            if ($solicitud) {

                $solicitud->tipo_residencia       = mb_strtoupper($request->datos_adicionales['residencia']);
                $solicitud->ingreso_mensual       = $request->datos_adicionales['ingreso_mensual'];
                $solicitud->antiguedad_domicilio  = $request->datos_adicionales['antiguedad_domicilio'];
                $solicitud->antiguedad_empleo     = $request->datos_adicionales['antiguedad_empleo'];
                $solicitud->telefono_empleo       = $request->datos_adicionales['telefono_empleo'];    
                $solicitud->gastos_familiares     = $request->datos_adicionales["gastos_mensuales"];
                $solicitud->numero_dependientes   = $request->datos_adicionales["dependientes_economicos"];
                $solicitud->ocupacion             = mb_strtoupper($request->datos_adicionales["ocupacion"]);
                $solicitud->fuente_ingresos       = mb_strtoupper($request->datos_adicionales["fuente_ingresos"]);
                $solicitud->save();

                if ($solicitud->bc_score->bc_score == '-008' || $solicitud->bc_score->bc_score == '-009') {

                    $this->solicitudRepository->setUltPuntoReg(
                        $solicitud->id,
                        $solicitud->status,
                        'Datos Elegible',
                        1,
                        'Los datos de ingresos/egresos se han guardado con éxito.'
                    );
                    $solicitud->sub_status = 'Datos Elegible';
                    $solicitud->save();

                    $ocupacion = $solicitud->ocupacion;
                    $ocupacionElegible = $this->ocupaciones->filter(function ($value, $key) use($ocupacion) {
                        return mb_strtoupper($value) == mb_strtoupper($ocupacion);
                    });

                    if (count($ocupacionElegible) == 1) {

                        $solicitud->sub_status = 'Elegible';
                        $solicitud->save();

                        $mnsj_str = [
                            'stat'          => 'Elegible',
                            'ocupacion'     => mb_strtoupper($solicitud->ocupacion),
    						'message'       => 'El usuario es Elegible. La ocupación es: '. implode(' o ', $this->ocupaciones->toArray()),
    						'elegible'      => 1,
                            'prospect_id'   => $solicitud->prospect_id,
                            'solic_id'      => $solicitud->id,
                        ];

                        $this->solicitudRepository->setUltPuntoReg(
                            $solicitud->id,
                            $solicitud->status,
                            'Elegible',
                            1,
                            'El usuario es Elegible. La ocupación es: '. implode(' o ', $this->ocupaciones->toArray())
                        );
                        $siguiente_paso = 'datos_empleo';

                    } else {

                        $solicitud->sub_status = 'No Elegible';
                        $solicitud->save();

                        $mnsj_str = [
                            'stat'      => 'No Elegible',
                            'ocupacion' => mb_strtoupper($solicitud->ocupacion),
    						'message'   => 'El usuario NO es Elegible. La ocupación no es: '. implode(' o ', $this->ocupaciones->toArray()),
    						'elegible'  => 0,
                        ];

                        $this->solicitudRepository->setUltPuntoReg(
                            $solicitud->id,
                            $solicitud->status,
                            'No Elegible',
                            1,
                            'El usuario NO es Elegible. La ocupación no es: '. implode(' o ', $this->ocupaciones->toArray())
                        );

                        $siguiente_paso = 'modal';
                        Cookie::queue(Cookie::forget('producto'));
                        Cookie::queue(Cookie::forget('logo'));
                        Cookie::queue(Cookie::forget('checa_calificas'));
                    }

                    $eventTM = ['event'=> 'Finalizan4', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                    $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
                    $solicitud->save();
                    $mnsj_str['siguiente_paso'] = $siguiente_paso;
                    if ($siguiente_paso == 'modal') {
                        $mnsj_str['modal'] = view("modals.modalNoCalifica")->render();
                    }
                    $mnsj_str['eventTM'] = $eventTM;
                    return  response()->json($mnsj_str);

                }

                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    "Datos Ingresos-Egresos",
                    1,
                    "Los datos de ingresos/egresos se han guardado con éxito."
                );

                $solicitud->sub_status = 'Datos Ingresos-Egresos';
                $solicitud->save();

                $eventTM = ['event'=> 'Finalizan4', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                $siguiente_paso = 'alp';

                return response()->json([
                    'stat'              => 'Datos Adicionales OK',
                    'prospecto_id'      => $prospecto->id,
                    'solicitud_id'      => $solicitud->id,
                    'siguiente_paso'    => $siguiente_paso,
                    'eventTM'           => $eventTM,
                    'success'           => true
                ]);

            } else {
                return response()->json([
                    'success'   => false,
                    'message'   => 'La solicitud no existe',
                ]);
            }
        } else {
            return response()->json([
                'success'   => false,
                'message' => 'La sesión expiro'
            ]);
        }
    }

    public function datosEmpleo(request $request) {

        if (Auth::guard('prospecto')->check()) {
            $prospecto_id = Auth::guard('prospecto')->user()->id;

            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            if ($solicitud) {

                $validaciones = $this->validaciones($request, 'datos_empleo');
                if ($validaciones != '') {
                    return response()->json($validaciones);
                }

                $solicitud->antiguedad_empleo = $request->input('antiguedad_empleo');
                $solicitud->telefono_empleo   = $request->input("telefono_empleo");
                $solicitud->save();

                $datos_adicionales = DatosAdicionales::updateOrCreate([
                    'prospecto_id'  => $prospecto->id,
                    'solicitud_id'  => $solicitud->id
                ], [
                    'nombre_empresa' => mb_strtoupper($request->input('empresa'))
                ]);

                $datos_empleo = DatosEmpleo::updateOrCreate([
                    'prospecto_id'          => $prospecto->id,
                    'solicitud_id'          => $solicitud->id
                ], [
                    'nombre_empresa'        => mb_strtoupper($request->input('empresa')),
                    'antiguedad_empleo'     => $request->input('antiguedad_empleo'),
                    'telefono_empleo'       => $request->input('telefono_empleo'),
                    'fecha_ingreso'         => $fecha_nacimiento = Carbon::createFromFormat('d-m-Y', $request->input('fecha_ingreso')),
                    'calle'                 => mb_strtoupper($request->input('calle_empleo')),
                    'num_exterior'          => mb_strtoupper($request->input('no_exterior_empleo')),
                    'num_interior'          => mb_strtoupper($request->input('no_interior_empleo')),
                    'codigo_postal'         => $request->input('codigo_postal_empleo'),
                    'colonia'               => mb_strtoupper($request->input('colonia_empleo')),
                    'delegacion_municipio'  => mb_strtoupper($request->input('delegacion_empleo')),
                    'ciudad'                => mb_strtoupper($request->input('ciudad_empleo')),
                    'estado'                => mb_strtoupper($request->input('estado_empleo')),
                ]);

                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    "Datos Empleo",
                    1,
                    "Los datos del empleo se han guardado con éxito."
                );
                $solicitud->sub_status = 'Datos Empleo';
                $solicitud->save();

                if ($solicitud->status == 'Hit BC') {
                    $siguiente_paso = 'segunda_llamada';
                } else {
                    $siguiente_paso = 'alp';
                }

                $eventTM = ['event'=> 'Finalizan5', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                return response()->json([
                    'success'           => true,
                    'prospecto_id'      => $prospecto->id,
                    'solicitud_id'      => $solicitud->id,
                    'eventTM'           => $eventTM,
                    'siguiente_paso'    => $siguiente_paso,
                ]);

            } else {

                return response()->json([
                    'success'   => false,
                    'message'   => 'La solicitud no existe',
                ]);

            }
        } else {
            return response()->json([
                'success'   => false,
                'message' => 'La sesión expiro'
            ]);
        }
    }

    public function solicitudExpress(request $request) {

        $datosSolicitud = $request['datosSolicitud'];
        if(Auth::guard('captura_convenio')->check()) {
            $datosSucursal = [
                'empresa'   => Auth::guard('captura_convenio')->user()->empresa,
                'sucursal' 	=> Auth::guard('captura_convenio')->user()->sucursal,
                'email'		=> Auth::guard('captura_convenio')->user()->email,
                'telefono'	=> Auth::guard('captura_convenio')->user()->telefono,
                'embajador'	=> Auth::guard('captura_convenio')->user()->embajador,
                'email'	    => Auth::guard('captura_convenio')->user()->email
            ];

            $empresa = Auth::guard('captura_convenio')->user()->empresa;
            $curso = '-';
            if (isset($request['datosSolicitud']['curso'])) {
                $curso = $request['datosSolicitud']['curso'];
            }

            switch ($empresa) {
                case 'Dentalia':
                    $finalidad_custom = 'Préstamo para financiamiento de tratamiento dental con Dentalia';
                    break;
                case 'Bedu':
                    $finalidad_custom = 'Préstamo para financiamiento del curso '.$curso.' de Bedu';
                    break;
                default:
                    $finalidad_custom = '';
                    break;
            }

        } else {
            $datosSucursal = [
                'empresa'   => '',
                'sucursal'  => '',
                'email'     => '',
                'telefono'  => '',
                'embajador' => '',
                'email'     => '',
            ];
        }

        $validator = Validator::make($datosSolicitud, [
            'monto'     => 'required|numeric|min:'.$datosSolicitud['monto_minimo'].'|max:'.$datosSolicitud['monto_maximo'],
            'plazo'     => 'required',
            'finalidad' => 'required',

            'nombres' => 'required',
            'apellido_materno' => 'required',
            'apellido_paterno' => 'required',
            'celular' => 'required|numeric|digits:10',
            'email' => 'required|email',
            'password' => 'required|regex:/(^(?:(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,})$)/u',
            'confirma_password' => 'required|same:password',

            'calle' => 'required',
            'no_ext' => 'required',
            'codigo_postal' => 'required|numeric',
            'colonia' => 'required',

            'sexo' => 'required',
            'fecha_nacimiento' => 'required',
            'ciudad_nacimiento' => 'required',
            'estado_nacimiento' => 'required',
            'telefono_casa' => 'required|numeric|digits:10',
            'estado_civil' => 'required',
            'nivel_estudios' => 'required',
            'rfc' => 'required',

            'ocupacion' => 'required',
            'fuente_ingresos' => 'required',
            'ingreso_mensual' => 'required|numeric|min:1000',
            'antiguedad_empleo' => 'required',
            'nombre_empresa' => 'required',
            'telefono_empleo' => 'required|numeric|digits:10',
            'residencia' => 'required',
            'antiguedad_domicilio' => 'required',
            'gastos_mensuales' => 'required|numeric',
            'dependientes_economicos' => 'required',
        ], [
            'required'                  => 'El campo es obligatorio.',
            'calle.required'            => 'El campo Calle es obligatorio.',
            'no_ext.required'           => 'El campo Número exterior es obligatorio.',
            'password.regex'            => 'La contraseña debe contener al menos: 1 letra mayúscula, 1 letra minúscula, 1 número y debe ser al menos de 8 dígitos.',
            'confirma_password.same'    => 'El campo contraseña y confirma contraseña deben ser iguales.',
            'numeric'                   => 'El campo debe contener solo números.',
            'email'                     => 'El email no es válido.',
            'digits'                    => 'El campo debe tener :digits números.',
            'monto.max'                 => 'El monto no puede ser mayor a $'.number_format($datosSolicitud['monto_maximo'], 0),
            'monto.min'                 => 'El monto no puede ser menor a $'.number_format($datosSolicitud['monto_minimo'], 0),
            'ingreso_mensual.min'       => 'El ingreso mensual debe tener al menos 4 dígitos',
        ])->validate();

        $prospecto = Prospecto::updateOrCreate([
            'email' => mb_strtoupper($datosSolicitud['email'])
        ]);

        $datosLDAP = [
            'correo'        => $datosSolicitud['email'],
            'password'      => $datosSolicitud['password'],
            'email'         => mb_strtoupper($datosSolicitud['email']),
            'nombre'        => mb_strtoupper($datosSolicitud['nombres']),
            'apellido_p'    => mb_strtoupper($datosSolicitud['apellido_paterno']),
            'apellido_m'    => mb_strtoupper($datosSolicitud['apellido_materno']),
            'user'          => $datosSolicitud['email'],
            'cel'           => $datosSolicitud['celular'],
            'origen'        => 'SITIO'

        ];

        if ($prospecto->wasRecentlyCreated) {

            $prospecto->nombre        = mb_strtoupper($datosSolicitud['nombres']);
            $prospecto->apellido_p    = mb_strtoupper($datosSolicitud['apellido_paterno']);
            $prospecto->apellido_m    = mb_strtoupper($datosSolicitud['apellido_materno']);
            $prospecto->cel           = $datosSolicitud['celular'];
            $prospecto->activate_code = '';
            $prospecto->confirmed     = 1;
            $prospecto->referencia    = 'SITIO';
            $prospecto->save();

            $responseLDAP = self::createUserLDAP($datosLDAP);
            if ($responseLDAP != '') {
                $responseLDAP = json_decode($responseLDAP);
                if ($responseLDAP->success == true) {

                    $prospecto->password = Hash::make($datosSolicitud['password']);
                    $prospecto->encryptd = encrypt($datosSolicitud['password']);
                    $prospecto->save();

                } else if ($responseLDAP->success == false
                    && $responseLDAP->message == 'Error, el proceso de creacion de usuario en LDAP fallo, LDAP:Entry Already Exists') {

                    $responseLDAP = self::loginUserLDAP($datosLDAP);

                    if ($responseLDAP->success == true) {

                        $prospecto->password = Hash::make($datosSolicitud['password']);
                        $prospecto->encryptd = encrypt($datosSolicitud['password']);
                        $prospecto->save();

                    }
                } else {

                    if ($responseLDAP->success == false
                        && $responseLDAP->message == 'Error de sistema, No response from the LDAP service') {
                        $error = $responseLDAP->message;
                    } else {
                        $error = 'Error desconocido al intentar registrar el usuario';
                    }

                    return response()->json([
                        'success'   => false,
                        'msg'       => $error,
                        'ldap'      => $responseLDAP
                    ]);
                }

            } else {

                return response()->json([
                    'success'   => false,
                    'msg'       => 'Error desconocido al intentar registrar el usuario',
                    'ldap'      => $responseLDAP
                ]);

            }

        } else {

            $responseLDAP = self::loginUserLDAP($datosLDAP);
            if ($responseLDAP != '') {
                $responseLDAP = json_decode($responseLDAP);
                if ($responseLDAP->message == true) {

                    $prospecto->password = Hash::make($datosSolicitud['password']);
                    $prospecto->encryptd = encrypt($datosSolicitud['password']);
                    $prospecto->save();

                }
            }

            $prospecto->nombre        = mb_strtoupper($datosSolicitud['nombres']);
            $prospecto->apellido_p    = mb_strtoupper($datosSolicitud['apellido_paterno']);
            $prospecto->apellido_m    = mb_strtoupper($datosSolicitud['apellido_materno']);
            $prospecto->cel           = $datosSolicitud['celular'];
            $prospecto->save();
        }

        $fecha_nacimiento = Carbon::createFromFormat('Y-m-d', $datosSolicitud['fecha_nacimiento']);
        // Obteniendo la descripcion de la finalidad
        $finalidad = Finalidad::find($datosSolicitud['finalidad']);
        if (count($finalidad) == 1) {
            $finalidad = mb_strtoupper($finalidad->finalidad);
        }

        $solicitud = Solicitud::updateOrCreate([
            'prospecto_id'          => $prospecto->id,
            'status'                => 'Registro Usuario',
        ],[
            'prestamo'              => $datosSolicitud['monto'],
            'plazo'                 => $datosSolicitud['plazo'],
            'finalidad'             => $finalidad,
            'finalidad_custom'      => $finalidad_custom,
            'sexo'                  => $datosSolicitud['sexo'],
            'fecha_nacimiento'      => $fecha_nacimiento->format('Y-m-d'),
            'lugar_nac_estado'      => $datosSolicitud['estado_nacimiento'],
            'lugar_nac_ciudad'      => $datosSolicitud['ciudad_nacimiento'],
            'rfc'                   => $datosSolicitud['rfc'],
            'tel_casa'              => $datosSolicitud['telefono_casa'],
            'estado_civil'          => $datosSolicitud['estado_civil'],
            'nivel_estudios'        => $datosSolicitud['nivel_estudios'],
            'ocupacion'             => $datosSolicitud['ocupacion'],
            'fuente_ingresos'       => $datosSolicitud['fuente_ingresos'],
            'tipo_residencia'       => $datosSolicitud['residencia'],
            'ingreso_mensual'       => $datosSolicitud['ingreso_mensual'],
            'gastos_familiares'     => $datosSolicitud['gastos_mensuales'],
            'antiguedad_empleo'     => $datosSolicitud['antiguedad_empleo'],
            'telefono_empleo'       => $datosSolicitud['telefono_empleo'],
            'antiguedad_domicilio'  => $datosSolicitud['antiguedad_domicilio'],
            'numero_dependientes'   => $datosSolicitud['dependientes_economicos'],
        ]);

        $datosConvenio = DatosConvenio::updateOrCreate([
            'prospecto_id'  => $prospecto->id,
            'solicitud_id'  => $solicitud->id
        ],[
            'empresa'   => $datosSucursal['empresa'],
            'sucursal'  => $datosSucursal['sucursal'],
            'embajador' => $datosSucursal['embajador'],
            'telefono'  => $datosSucursal['telefono'],
            'email'     => $datosSucursal['email']
        ]);

        $datos_domicilio = $this->buscarDatosDomicilio($datosSolicitud);

        // Guardando los datos del domicilio
        $domicilio = DomicilioSolicitud::firstOrNew([
            'prospect_id'     => $prospecto->id,
            'solicitation_id' => $solicitud->id
        ]);

        $domicilio->cp            = $datosSolicitud['codigo_postal'];
        $domicilio->calle         = mb_strtoupper($datosSolicitud['calle']);
        $domicilio->num_exterior  = mb_strtoupper($datosSolicitud['no_ext']);
        if (isset($datosSolicitud['no_int'])) {
            $domicilio->num_interior  = mb_strtoupper($datosSolicitud['no_int']);
        }
        $domicilio->colonia       = mb_strtoupper($datosSolicitud['colonia']);
        $domicilio->delegacion    = mb_strtoupper($datosSolicitud['delegacion_municipio']);
        $domicilio->ciudad        = mb_strtoupper($datosSolicitud['ciudad']);
        $domicilio->estado        = mb_strtoupper($datosSolicitud['estado']);

        if (count($datos_domicilio) > 1) {
            $domicilio->cp            = $datos_domicilio['codigopostal'];
            $domicilio->id_estado     = $datos_domicilio['id_estado'];
            $domicilio->codigo_estado = $datos_domicilio['codigo_estado'];
            $domicilio->id_delegacion = $datos_domicilio['id_delegacion'];
            $domicilio->id_ciudad     = $datos_domicilio['id_ciudad'];
            $domicilio->id_colonia    = $datos_domicilio['id_colonia'];
            $domicilio->cobertura     = $datos_domicilio['cobertura'];
            if ($datosSolicitud['ciudad'] == '' && $datos_domicilio['ciudad'] != '') {
                $domicilio->ciudad = $datos_domicilio['ciudad'];
            }
        }
        $domicilio->save();

        // Guardando el nombre de la empresa
        $datos_adicionales = DatosAdicionales::firstOrNew([
            'prospecto_id'      => $prospecto->id,
            'solicitud_id'      => $solicitud->id,
            'nombre_empresa'    => mb_strtoupper($datosSolicitud['nombre_empresa']),
        ]);
        $datos_adicionales->save();

        // Agregando el producto a la solicitud:
        //  1.- Mercado Abierto
        $producto_id = isset($datosSolicitud['producto_id']) ? $datosSolicitud['producto_id'] : 1;
        $version_producto = Producto::find($producto_id)->getCurrentVersionNo();
        $solicitud->producto()->sync([
            $producto_id => [
                'version_producto'  => $version_producto,
                'lead'              => '',
            ]
        ]);

        return response()->json([
            'success'       => true,
            'prospecto_id'  => $prospecto->id,
            'solicitud_id'  => $solicitud->id,
        ]);
    }

    public function buscarDatosDomicilio($datos_usuario) {

        $cp = $datos_usuario['codigo_postal'];
        $colonia = $datos_usuario['colonia'];
        $msj_colonia = '';
        $colonia_correcta = 0;

        if (strlen($cp) == 4) {
            $cp = '0'.$cp;
        }

        $direccion = CatalogoSepomex::whereRaw('codigo LIKE "'.$cp.'" AND colonia_asentamiento LIKE "%'.$colonia.'%"')
            ->get()
            ->toArray();

        if ( count($direccion) == 0 ) {

            $direccion = CatalogoSepomex::whereRaw('codigo LIKE "'.$cp.'"')
                ->get()
                ->toArray();

            if ( count($direccion) >= 1 ) {

                $id_edo = $direccion[0]['id_estado'];
                $codigo_edo = $direccion[0]['codigo_estado'];

                if ($id_edo == '09') {
                    $id_municipio = '0'.$direccion[0]['id_ciudad'];
                    $id_ciudad = '01';
                } else {
                    $id_municipio = $direccion[0]['id_municipio'];
                    $id_ciudad = $direccion[0]['id_ciudad'];
                }

                $id_colonia = intval($direccion[0]['id_colonia']);

                $ciudad = '';
                if ($id_ciudad != '') {
                    $id_ciudad = $id_ciudad;
                    $ciudad = $direccion[0]['ciudad'];
                }


                return [
                    'codigopostal'  => $cp,
                    'id_estado'     => $id_edo,
                    'codigo_estado' => $codigo_edo,
                    'id_delegacion' => $id_municipio,
                    'id_ciudad'     => $id_ciudad,
                    'ciudad'        => $ciudad,
                    'id_colonia'    => '',
                    'cobertura'     => $direccion[0]['cobertura'],
                ];

            } else {
                return [];
            }

        } elseif (count($direccion) > 1) {

            $direccion_exacta = CatalogoSepomex::whereRaw('codigo LIKE "'.$cp.'" AND colonia_asentamiento LIKE "'.$colonia.'"')
                ->get()
                ->toArray();

            if ( count($direccion_exacta) == 1 ) {

                $direccion = $direccion_exacta;
                $colonia_correcta = 1;

            }

        } elseif ( count($direccion) == 1 ) {

            $colonia_correcta = 1;

        } else {

            $colonia_correcta = 0;

        }

        $id_edo = $direccion[0]['id_estado'];
        $codigo_edo = $direccion[0]['codigo_estado'];

        if ($id_edo == '09') {
            $id_municipio = '0'.$direccion[0]['id_ciudad'];
            $id_ciudad = '01';
        } else {
            $id_municipio = $direccion[0]['id_municipio'];
            $id_ciudad = $direccion[0]['id_ciudad'];
        }

        $id_colonia = '';
        if ($colonia_correcta == 1) {
            $id_colonia = intval($direccion[0]['id_colonia']);
            $id_colonia = $id_edo.$id_municipio.$cp.$id_colonia;
        }

        $ciudad = '';
        if ($id_ciudad != '') {
            $id_ciudad = $id_ciudad;
            $ciudad = $direccion[0]['ciudad'];
        }

        return [
            'codigopostal'  => $cp,
            'id_estado'     => $id_edo,
            'codigo_estado' => $codigo_edo,
            'id_delegacion' => $id_municipio,
            'id_ciudad'     => $id_ciudad,
            'ciudad'        => $ciudad,
            'id_colonia'    => $id_colonia,
            'cobertura'     => $direccion[0]['cobertura'],
        ];
    }

    public function buscarColonias(Request $request) {

        $codigo_postal = $request['codigo_postal'];
        $cp_data = CatalogoSepomex::where('codigo', $codigo_postal)
            ->get()
            ->toArray();

        if (count($cp_data) > 0) {
            $colonias = [];
            $ciudad = "";
            $id_ciudad = "";
            foreach ($cp_data as $data) {

                array_push(
                    $colonias,
                    [
                        'colonia' => addslashes($data['colonia_asentamiento']),
                        'id' => intval($data['id_colonia'])
                    ]
                );
                if ($data['ciudad'] != "") {
                    $ciudad = $data['ciudad'];
                    $id_ciudad = $data['id_ciudad'];
                }
            }
            // Si el id del estado es 09 (Ciudad de México), el Id de municipio
            // es igual al id de la ciudad y el id de la ciudad es 01
            $id_estado = $cp_data[0]['id_estado'];
            if ($id_estado == '09') {
                $id_municipio = '0'.$id_ciudad;
                $id_ciudad = '01';
            } else {
                $id_municipio = $cp_data[0]['id_municipio'];
            }

            $id_colonia = $id_estado.$id_municipio.$codigo_postal;

            return response()->json([
                'success'        => true,
                'cp'             => $cp_data[0]['codigo'],
                'colonias'       => $colonias,
                'deleg_munic'    => addslashes($cp_data[0]['municipio']),
                'id_deleg_munic' => $id_municipio,
                'ciudad'         => addslashes($ciudad),
                'id_ciudad'      => $id_ciudad,
                'estado'         => addslashes($cp_data[0]['estado']),
                'codigo_estado'  => addslashes($cp_data[0]['codigo_estado']),
                'id_estado'      => $id_estado,
                'id_colonia'     => $id_colonia,
                'cobertura'      => true // Agregar validación de cobertura
            ]);
        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'No se encontraron colonias para el codigo postal ingresado',
                'cp'        => $codigo_postal
            ]);
        }
    }

    public function buscarCiudades(Request $request)
    {   
        // Obteniendo los municipios del estado
        $municipios = CatalogoSepomex::selectRAW('distinct(municipio)')
            ->where('estado', $request['estado'])
            ->orderBy('municipio')
            ->pluck('municipio');

        $no_municipios = count($municipios);

        if ($no_municipios > 0) {
            return response()->json([
                'success'   => true,
                'count'      => $no_municipios,
                'municipios' => $municipios
            ]);
        } else {
            return response()->json([
                'success'   => false,
                'message'    => 'Ciudades no encontradas para el estado seleccionados'
            ]);
        }
    }

    public function getCuestionario(Request $request)
    {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        $solicitud_id = $request->solicitud_id;
        $prospecto_id = $request->prospecto_id;

        return view('solicitud.cuestionario_dinamico', compact('solicitud_id', 'prospecto_id','configuracion'));
    }
    
}
