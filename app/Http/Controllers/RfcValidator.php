<?php
namespace App\Http\Controllers;

trait RfcValidator {

    /**
     * Regla de validación para un RFC.
     *
     * @param string $value
     *
     * @return boolean
     */
    public function validate($value)
    {
        if (!$this->correctLenght($value)) {
            return false;
        }
        if (!preg_match($this->pattern($value), $value, $matches)) {
            return false;
        }
        if (!$this->hasValidDate($matches)) {
            return false;
        }
        return true;
    }

    /**
     * Verifica que el RFC tenga una longitud correcta.
     *
     * @param string $rfc
     * @return bool
     */
    private function correctLenght($rfc)
    {
        $length = mb_strlen($rfc);
        // El RFC debe ser de 10 o 13 posiciones para personas
        // físicas, cualquier longitud fuera de ese rango resulta en un RFC
        // inválido.
        return $length == 10 || $length == 13;
    }

    /**
     * Devuelve el patrón que debe ser usado por preg_match() para el RFC en
     * cuestión.
     *
     * @see https://es.wikipedia.org/wiki/Registro_Federal_de_Contribuyentes_(México)
     * @param string $rfc
     * @return string
     */
    private function pattern($rfc)
    {
        $length = mb_strlen($rfc);
        // El RFC debe comenzar con 4 letras
        $lettersLength = 4;
        return '/^[A-ZÑ&]{' . $lettersLength . '}([0-9]{2})([0-1][0-9])([0-3][0-9])[A-Z0-9]{2}[0-9A]$/iu';
    }

    /**
     * Verifica si el RFC tiene una fecha válida a partir de los 'match' del
     * resultado de preg_match().
     *
     * @param array $matches
     * 
     * @return bool
     */
    private function hasValidDate($matches)
    {
        // Extraemos los datos de la fecha para poder validar su integridad.
        $year = (int)$matches[1];
        $month = (int)$matches[2];
        $day = (int)$matches[3];
        // Si el año es menor a 70 se asume que es después del año 2000.
        $year < 70 ? $year += 2000 : $year += 1900;
        return checkdate($month, $day, $year);
    }
}
