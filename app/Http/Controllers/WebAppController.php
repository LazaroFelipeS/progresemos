<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DocumentosRepository;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Jobs\ProcesaFaceMatch;
use App\Jobs\ProcesaCargaIdentificacionSelfie;
use App\Solicitud;
use App\ClientesAlta;
use App\RespuestaMaquinaRiesgo;
use App\MB_UPLOADED_DOCUMENT;
use App\PlantillaComunicacion;
use Log;
use Image;
use App;
use Auth;
use Jenssegers\Agent\Agent;
use App\Repositories\SolicitudRepository;

class WebAppController extends Controller
{
    private $solicitudRepository;

    public function __construct(DocumentosRepository $documentos)
    {
        $this->_documentos = $documentos;
        $this->solicitudRepository = new SolicitudRepository;
    }

    public function capturaIdentificacionFront() {

        $agent = new Agent();
        $prospecto = Auth::guard('prospecto')->user();
        $prospecto_id = $prospecto->id;
        $solicitud = $this->solicitudActual($prospecto->id);
        $solicitud_id = $solicitud->id;

        $respuestaMR = RespuestaMaquinaRiesgo::select('simplificado')
            ->where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->firstOrFail();
            
        return view('webapp.idFront', ['desktop' => $agent->isDesktop(), 'simplificado' => $respuestaMR->simplificado]);
    }

    public function capturaIdentificacionBack() {
        $agent = new Agent();
        return view('webapp.idBack', ['desktop' => $agent->isDesktop()]);
    }

    public function capturaSelfie() {
        $agent = new Agent();
        return view('webapp.selfie', ['desktop' => $agent->isDesktop()]);
    }

    public function subirDocumento(Request $request) {
        $prospecto = Auth::guard('prospecto')->user();
        $prospecto_id = $prospecto->id;
        $solicitud = $this->solicitudActual($prospecto->id);
        $solicitud_id = $solicitud->id;

        $tipo_documento = $request->get('tipo_documento');
        $detalle_documento = $request->get('detalle_documento');
        $image = Image::make($request->get('imgBase64'));
        $root = "proceso_simplificado";

        if (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.jpg")) {
            Storage::disk('s3')->delete("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.jpg");
        }
        Storage::disk('s3')->put("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}.jpg", $image->encode());

        $paso = 1;
        $termina = false;
        if ($tipo_documento == 'identificacion_oficial') {
            if ($detalle_documento == 'front') {
                $paso = 2;
            } elseif ($detalle_documento == 'back') {
                $paso = 3;
            }  elseif ($detalle_documento == 'photo') {
                $paso = 3;
                $termina = true;
            }
        }

        if ($termina == true) {

            $clienteAlta = ClientesAlta::where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->orderBy('id', 'desc')
                ->first();

            if ($clienteAlta->facematch === null) {

                // Actualizando ult_punto_reg solicitud
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Oferta Aceptada - FaceMatch Completado',
                    1,
                    'El FaceMatch se completo con éxito'
                );


                if ($clienteAlta->aplica_facematch == true) {
                    $job = (new ProcesaFaceMatch($clienteAlta))->delay(10);
                    dispatch($job);
                } elseif ($clienteAlta->solo_carga_identificacion_selfie == true) {
                    $job = (new ProcesaCargaIdentificacionSelfie($clienteAlta))->delay(10);
                    dispatch($job);
                }

                $solicitud->sub_status = 'Oferta Aceptada - FaceMatch Completado';
                $solicitud->save();

                if (isset($solicitud->respuesta_maquina_riesgos[0]['plantilla_comunicacion'])) {
                    $id_plantilla = $solicitud->respuesta_maquina_riesgos[0]['plantilla_comunicacion'];
                } else {
                    $id_plantilla = 89;
                }

                $plantilla = PlantillaComunicacion::select('id', 'modal_encabezado', 'modal_cuerpo', 'modal_img')
                            ->where('plantilla_id', $id_plantilla)
                            ->get()
                            ->toArray();
                $tituloModal = $plantilla[0]['modal_encabezado'];
                $imgModal = $plantilla[0]['modal_img'];
                $cuerpoModal = $plantilla[0]['modal_cuerpo'];
                $modal = view("modals.modalStatusSolicitud", ['tituloModal' => $tituloModal, 'imgModal' => $imgModal, 'cuerpoModal' => $cuerpoModal, 'continua' => false])->render();
                Auth::guard('prospecto')->logout();

                return response()->json([
                    'success'           => true,
                    'message'           => 'Imagen guardada correctamente',
                    'modal'             => $modal,
                    'termina_proceso'   => true,
                ]);

            }

        } else {

            return response()->json([
                'success'           => true,
                'message'           => 'Imagen guardada correctamente',
                'termina_proceso'   => false,
                'paso'              => $paso,
            ]);

        }


    }

    public function solicitudActual($prospecto_id) {

        // Obteniendo la última solicitud del prospecto
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('id', 'desc')
            ->first();

        return $solicitud;

    }

    public function procesaFaceMatch(Request $request) {

        $id = $request->idAltaCliente;
        $clienteAlta = ClientesAlta::findOrFail($id);

        if ($clienteAlta->aplica_facematch == true) {
            $job = (new ProcesaFaceMatch($clienteAlta))->delay(10);
            dispatch($job);
        } elseif ($clienteAlta->solo_carga_identificacion_selfie == true) {
            $job = (new ProcesaCargaIdentificacionSelfie($clienteAlta))->delay(10);
            dispatch($job);
        }

        return response()->json([
            'success' => true
        ]);

    }


    function repetirProceso(Request $request) {

        try {

            $prospecto_id = $request->prospecto_id;
            $solicitud_id = $request->solicitud_id;

            $clienteAlta = ClientesAlta::where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->firstOrFail();

            $clienteAlta->facematch = null;
            $clienteAlta->save();

            $solicitud = Solicitud::where('id', $solicitud_id)
                ->orderBy('id', 'desc')
                ->first();

            $solicitud->ult_punto_reg = self::setUltPuntoReg(
                $solicitud->ult_punto_reg,
                'Oferta Aceptada - Repetir FaceMatch',
                1,
                'El FaceMatch necesita ser reprocesado'
            );

            $resultado = $solicitud->save();

            return response()->json([
                'success'   => true,
                'message'   => 'El FaceMatch esta listo para volver a ejecutarse'
            ]);

        } catch (\Exception $e) {

            return response()->json([
                'success'   => false,
                'message'   => $e->getMessage()
            ]);

        }

    }

}
