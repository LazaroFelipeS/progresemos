<?php
namespace App\Http\Controllers;
use App\CatalogosT24;
use App\Sepomex;
use Log;
use App\Prospecto;
use App\Solicitud;
use App\ClientesAlta;
use App\RespuestaMaquinaRiesgo;
use App\Jobs\AltaClienteAutomatica;
use App\Jobs\AltaSolicitudAutomatica;
use App\Jobs\LigueUsuarioAutomatico;
use App\Jobs\EnvioEmail;
use Carbon\Carbon;
use App\Holiday;


trait FuncionesT24 {

    /**
     * Realiza la búsqueda de los valores que se enviaran a T24 en la tabla catalogos_t24
     *
     * @param  string $parametros Parametros especiales del campo
     * @param  string $campo      Valor original del campo
     * @param  string $key        Nombre del campo T24
     *
     * @return string/array       Valor T24 del campo
     */
    function buscar($parametros = null, $campo, $key = null) {
        if ($parametros == 'catalogos_t24') {

            $datos = CatalogosT24::where('campo_webservice', $key)
                ->whereRaw('valor_sitio LIKE "%'.$campo.'%"')
                ->get()
                ->toArray();

            if (count($datos) == 1) {

                return $datos[0]['valor_t24'];

            } elseif (count($datos) > 1) {

                // Si el campo que se esta buscando es el Estado Civil y arroja
                // más de una coincidencia, se regresa siempre el primer valor
                // esto sucedera cuando la opcion que se selecciona es CASADO
                if ($key == 'MARITALSTSNC') {
                    return $datos[0]['valor_t24'];
                } else {
                    return '';
                }

            } elseif (count($datos) == 0 && $key == 'LOANPURPOSE') {

                // Si el campo que se esta buscando es el objetivo del préstamo y no
                // regreso ninguna coincidencia (Cliente eligio Otra y realizó captura abierta)
                // se manda el codigo de Otros
                $datos = CatalogosT24::where('campo_webservice', $key)
                    ->whereRaw('valor_sitio LIKE "OTROS"')
                    ->get()
                    ->toArray();
                return $datos[0]['valor_t24'];

            } elseif (count($datos) == 0 && $key == 'OCUPACION') {

                // Si el campo que se esta buscando es el objetivo del préstamo y no
                // regreso ninguna coincidencia (Cliente eligio Otra y realizó captura abierta)
                // se manda el codigo de Otros
                $datos = CatalogosT24::where('campo_webservice', $key)
                    ->whereRaw('valor_sitio LIKE "OTRO"')
                    ->get()
                    ->toArray();
                return $datos[0]['valor_t24'];

            } else {

                $campo = explode(' ', $campo);
                if (count($campo) > 1) {
                    $ultimo = count($campo)-1;
                    $primero = 0;
                    $datos = CatalogosT24::where('campo_webservice', $key)
                        ->whereRaw('valor_sitio LIKE "%'.$campo[$primero].'" OR valor_sitio LIKE "'.$campo[$ultimo].'%"')
                        ->get()
                        ->toArray();

                    if (count($datos) == 1) {
                        return $datos[0]['valor_t24'];
                    } elseif (count($datos) > 1) {
                        return '';
                    } else {
                        return '';
                    }
                } else {
                    return '';
                }
            }

        } elseif ($parametros == 'direccion') {

            $cp = $campo['DIRCODPOS'];
            $colonia = $campo['DIRCOLONIA'];
            $msj_colonia = '';
            $colonia_correcta = 0;
            if (strlen($cp) == 4) {
                $cp = '0'.$cp;
            }

            $direccion = Sepomex::whereRaw('codigo LIKE "'.$cp.'" AND colonia_asentamiento LIKE "%'.$colonia.'%"')
                ->get()
                ->toArray();

            if ( count($direccion) == 0 ) {
                Log::info($cp.'-'.$colonia.': No existen coincidencias de Colonia.');
                return [
                    'DIRCODPOS' => $cp,
                    'DIRCDEDO' => '',
                    'DIRDELMUNI' => '',
                    'DIRCIUDAD' => '',
                    'DIRCOLONIA' => '',
                    'COLONIACORRECTA' => '',
                    'MSGCOLONIA' => 'No existen coincidencias de Colonia.'
                ];
            } elseif (count($direccion) > 1) {

                $direccion_exacta = Sepomex::whereRaw('codigo LIKE "'.$cp.'" AND colonia_asentamiento LIKE "'.$colonia.'"')
                    ->get()
                    ->toArray();

                if ( count($direccion_exacta) == 1 ) {

                    $direccion = $direccion_exacta;
                    $colonia_correcta = 1;
                    $msj_colonia = 'Coincidencia exacta en la búsqueda de colonia.';
                    Log::info($cp.'-'.$colonia.': '.$msj_colonia);

                }

            } elseif ( count($direccion) == 1 ) {

                $msj_colonia = 'Coincidencia exacta en la búsqueda de colonia.';
                $colonia_correcta = 1;
                Log::info($cp.'-'.$colonia.': '.$msj_colonia);

            } else {

                $msj_colonia = 'No hubo una coincidencia exacta en la búsqueda de Colonia. Se da de alta con: '.$direccion[0]['colonia_asentamiento'].'.';
                $colonia_correcta = 0;
                Log::info($cp.'-'.$colonia.': '.$msj_colonia);

            }

            $id_edo = $direccion[0]['id_estado'];
            if ($id_edo == '09') {
                $id_municipio = '0'.$direccion[0]['id_ciudad'];
                $id_ciudad = '01';
            } else {
                $id_municipio = $direccion[0]['id_municipio'];
                $id_ciudad = $direccion[0]['id_ciudad'];
            }

            $id_colonia = intval($direccion[0]['id_colonia']);
            if ($id_ciudad != '') {
                $id_ciudad = $id_edo.$id_ciudad;
            }

            return [
                'DIRCODPOS'         => $cp,
                'DIRCDEDO'          => $id_edo,
                'DIRDELMUNI'        => $id_edo.$id_municipio,
                'DIRCIUDAD'         => $id_ciudad,
                'DIRCOLONIA'        => $id_edo.$id_municipio.$cp.$id_colonia,
                'COLONIACORRECTA'   => $colonia_correcta,
                'MSGCOLONIA'        => $msj_colonia
            ];

        }

    }

    /**
     * Trunca el valor del campo al numero de caracteres que se establece en los parametros
     *
     * @param  string $parametros Parametros especiales del campo.
     * @param  string $campo      Valor original del campo
     * @param  string $key        Nombre del campo T24
     *
     * @return string             Valor T24 del campo
     */
    function truncar($parametros = null, $campo, $key = null) {
        $limite = $parametros;
        return substr($campo, 0, $limite);
    }

    /**
     * Parte el valor del campo por el caracter establecido en la primer parte de
     * los parametros, y realiza la búsqueda del valor real del campo en la tabla
     * catalogo_sepomex.
     *
     * @param  string $parametros Parametros especiales del campo
     * @param  string $campo      Valor original del campo
     * @param  string $key        Nombre del campo T24
     *
     * @return string             Valor T24 del campo
     */
    function partir_buscar($parametros = null, $campo, $key = null) {
        $parametros = explode('|', $parametros);
        $caracter = $parametros[0];
        $valor_regresado = $parametros[1];
        $campo = explode($caracter, $campo);
        $campo = $campo[$valor_regresado];

        if (strpos($campo,"DISTRITO FEDERAL") !== false) {
            $campo = 'Ciudad de Mexico';
        }

        $valor = Sepomex::select('id_estado')
            ->whereRaw('estado LIKE "%'.$campo.'%"')
            ->get()
            ->first();

        if (count($valor) == 1) {
            return $valor->id_estado;
        } else {
            return 'No se encontro coincidencia';
        }
    }

    /**
     * Divide el valor del campo en un arreglo, y almacena el indice 0 y 1 en
     * diferentes campos
     *
     * @param  string $parametros Parametros especiales del campo
     * @param  string $campo      Valor original del campo
     * @param  string $key        Nombre del campo T24
     *
     * @return array              Valor T24 del campo
     */
    function partir_nombre($parametros = null, $campo, $key = null) {
        $campo = explode(' ', $campo, 2);
        if (count($campo) > 1) {

            return [
                'NAME2' => $campo[0],
                'FORMERNAME' => $campo[1]
            ];

        } else {
            return [
                'NAME2' => $campo[0],
                'FORMERNAME' => ''
            ];
        }

    }

    /**
     * Ordena el valor de los campos fecha en el formato que T24 reconoce
     * AAAAMMDD
     *
     * @param  string $parametros Parametros especiales del campo
     * @param  string $campo      Valor original del campo
     * @param  string $key        Nombre del campo T24
     *
     * @return string             Valor T24 del campo
     */
    function ordenar_fecha($parametros = null, $campo, $key = null) {
        $fecha = explode('/', $campo);
        $dia = $fecha[0];
        $mes = $fecha[1];
        $año = $fecha[2];

        $dia = (strlen($dia) == 1) ? '0'.$dia : $dia;
        $mes = (strlen($mes) == 1) ? '0'.$mes : $mes;

        $fecha = $año.$mes.$dia;
        return $fecha;
    }

    /**
     * Deja el valor del campo en blanco si este no cumple con la longitud necesaria
     *
     * @param  string $parametros Parametros especiales del campo
     * @param  string $campo      Valor original del campo
     * @param  string $key        Nombre del campo T24
     *
     * @return string             Valor T24 del campo
     */
    function borrar($parametros = null, $campo, $key = null) {
        $necesario = $parametros;
        $longitud = strlen($campo);
        if ($longitud == $necesario)
        {
            return $campo;
        } else {
            return '';
        }

    }

    /**
     * Limipia el valor del campo de cualquier carácter especial
     *
     * @param  string $cadena Valor a limpiar
     *
     * @return string         Cadena sin caracteres especiales
     */
    function limpiar_caracteres($cadena) {
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A'),
            $cadena
        );

        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'),
            $cadena
        );

        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('I', 'I', 'I', 'I', 'I', 'I', 'I', 'I'),
            $cadena
        );

        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('O', 'O', 'O', 'O', 'O', 'O', 'O', 'O'),
            $cadena
        );

        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('U', 'U', 'U', 'U', 'U', 'U', 'U', 'U'),
            $cadena
        );

	   return $cadena;
    }

    /**
     * Regresa el valor del campo con la primer letra en mayúscula
     *
     * @param  string $parametros Parametros especiales del campo
     * @param  string $campo      Valor original del campo
     * @param  string $key        Nombre del campo T24
     *
     * @return string             Valor T24 del campo
     */
    function primera_mayuscula($parametros = null, $campo, $key = null) {
        return ucfirst(strtolower($campo));
    }

    /**
     * Convierte el valor del campo a un arreglo para obtener la parte entera
     * y si la parte cadena contiene la palabra QUINCENAS divide el valor entero
     * entre 2 para obtener el valor en meses
     *
     * @param  string $parametros Parametros especiales del campo
     * @param  string $campo      Valor original del campo
     * @param  string $key        Nombre del campo T24
     *
     * @return integer            Valor T24 del campo
     */
    function tiempo_meses($parametros = null, $campo, $key = null) {
        $tiempo = explode(' ', $campo);
        if ($tiempo[1] == 'MESES') {
            return $tiempo[0];
        } elseif ($tiempo[1] == 'QUINCENAS') {
            return ($tiempo[0] / 2);
        } elseif ($tiempo[1] == 'quincenas') {
            return ($tiempo[0] / 2);
        } elseif ($tiempo[1] == 'meses') {
            return $tiempo[0];
        }
    }

    public function buscar_estado($parametros = null, $campo, $key = null) {

        if (strpos($campo, "DISTRITO FEDERAL") !== false) {
            $campo = 'Ciudad de Mexico';
        }

        $valor = Sepomex::select('id_estado')
            ->whereRaw('estado LIKE "%'.trim($campo).'%"')
            ->get()
            ->first();

        if (count($valor) == 1) {
            return $valor->id_estado;
        } else {
            return 'No se encontro coincidencia';
        }

    }

    public function ids_domicilio($parametros = null, $campo, $key = null) {

        $id_edo = $campo['DIRCDEDO'];
        $cp = $campo['DIRCODPOS'];

        if ($id_edo == '09') {
            $id_municipio = $campo['DIRDELMUNI'];
            $id_ciudad = '01';
        } else {
            $id_municipio = $campo['DIRDELMUNI'];
            $id_ciudad = $campo['DIRCIUDAD'];
        }

        if ($id_ciudad != '') {
            $id_ciudad = $id_edo.$id_ciudad;
        } else {
            $id_ciudad = '';
        }

        return [
            'DIRDELMUNI'        => $id_edo.$id_municipio,
            'DIRCIUDAD'         => $id_ciudad
        ];

    }

    /**
     * Ejecuta el job del alta automatica del cliente en T24
     *
     * @param  integer $prospecto_id  Id del prospcecto
     * @param  integer $solicitud_id  Id de la solicitud del prospecto
     * @param  integer $idClienteAlta Id del cliente en tabla clientes_alta
     * @param  boolean $error         Para saber si el alta es un error
     * @param  string  $msj           Mensaje que se mostrara al guardar el registro
     *
     * @return array                  Resultado de procesar el alta del cliente
     */
    public function altaClienteT24($prospecto_id, $solicitud_id, $idClienteAlta = null, $error, $msj)
    {
        // Si existe el $idClienteAlta solo hay que buscar el registro en la tabla
        // clientes_alta, si no hay que generar un nuevo registro
        $procesadaManual = false;
        if ($idClienteAlta != null) {
            $clientesAlta = ClientesAlta::find($idClienteAlta);
            $procesadaManual = true;
        } else {
            $clientesAlta = $this->arregloT24($prospecto_id, $solicitud_id, null, $error, $msj);
        }

        if ($clientesAlta && ($clientesAlta->wasRecentlyCreated == true || $procesadaManual == true)) {
            // Si no existe ningún error se crea el Job para dar de alta el
            // cliente en T24
            if ($error == false) {

                $first = Carbon::createFromTime(3, 0, 0);
                $second = Carbon::createFromTime(4, 0, 0);
                $hora = Carbon::now();
                $dia = Carbon::today();

                // Obteniendo los dias festivos de T24
                $holidays = Holiday::select('fecha')->get()->toArray();
                $holidays_dates = [];

                foreach ($holidays as $holiday) {
                    $holidays_dates[] = strtotime($holiday['fecha']);
                }

                // Verificando si el dia es laborable
                $dia_laborable = true;
                if (in_array($dia->timestamp, $holidays_dates) || $dia->isWeekend() == true) {
                    $dia_laborable = false;
                }

                // Si el día es laborable y la hora de alta esta entre 3am y 4am
                // (cierre de T24), el proceso debe de encolarse para despues de las 4am
                // de lo contrario se procesa 10 segundos después
                if ($hora->between($first, $second) == true && $dia_laborable == true) {
                    $minutos =  $hora->diffInMinutes($second) + 2;
                    $job = (new AltaClienteAutomatica($clientesAlta))->delay(Carbon::now()->addMinutes($minutos));
                } else {
                    $job = (new AltaClienteAutomatica($clientesAlta))->delay(10);
                }

                dispatch($job);
            }

            return [
                'success' => true
            ];

        } else {

            return [
                'success' => false
            ];

        }

    }

    /**
     * Ejecuta el job del alta automatica de la solicitud en T24
     *
     * @param  integer $prospecto_id    Id del prospcecto
     * @param  integer $solicitud_id    Id de la solicitud del prospecto
     * @param  integer $idClienteAlta   Id del cliente en tabla altas_cliente
     * @param  boolean $idClienteT24    Id del cliente en T24
     * @param  string  $email_distinto  Identifica si el email del cliente es distinto
     *
     * @return array                  Resultado de procesar el alta del cliente
     */
    public function altaSolicitudT24($prospecto_id, $solicitud_id, $idClienteAlta = null, $idClienteT24 = null, $email_distinto = null)
    {
        // Si existe el $idClienteAlta solo hay que buscar el registro en la tabla
        // clientes_alta, si no hay que generar un nuevo registro
        $procesadaManual = false;
        if ($idClienteAlta != null) {
            $clienteAlta = ClientesAlta::find($idClienteAlta);
            $procesadaManual = true;
        } else {
            $msj = null;
            $error = false;
            if ($email_distinto == 1) {
                $error = true;
                $msj = 'El cliente se registro con un correo diferente';
            }

            $clienteAlta = $this->arregloT24($prospecto_id, $solicitud_id, $idClienteT24, $error, $msj);
            Log::info('Es recientemente creada: '.$clienteAlta->wasRecentlyCreated);
        }

        if ($clienteAlta && ($clienteAlta->wasRecentlyCreated == true || $procesadaManual == true)) {

            $first = Carbon::createFromTime(3, 0, 0);
            $second = Carbon::createFromTime(4, 0, 0);
            $hora = Carbon::now();
            $dia = Carbon::today();

            // Obteniendo los dias festivos de T24
            $holidays = Holiday::select('fecha')->get()->toArray();
            $holidays_dates = [];

            foreach ($holidays as $holiday) {
                $holidays_dates[] = strtotime($holiday['fecha']);
            }

            // Verificando si el dia es laborable
            $dia_laborable = true;
            if (in_array($dia->timestamp, $holidays_dates) || $dia->isWeekend() == true) {
                $dia_laborable = false;
            }

            // Si el día es laborable y la hora de alta esta entre 3am y 4am
            // (cierre de T24), el proceso debe de encolarse para despues de las 4am
            // de lo contrario se procesa 10 segundos después
            if ($hora->between($first, $second) == true && $dia_laborable == true) {
                $minutos =  $hora->diffInMinutes($second) + 2;
                $job = (new AltaSolicitudAutomatica($clienteAlta))->delay(Carbon::now()->addMinutes($minutos));
            } else {
                $job = (new AltaSolicitudAutomatica($clienteAlta))->delay(10);
            }

            dispatch($job);

        }

    }

    /**
     * Procesa el alta de la solicitud automatica en T24
     *
     * @param  integer $idClienteAlta   Id del cliente en tabla altas_cliente
     *
     * @return void
     */
    public function ligueUsuarioLDAP($idClienteAlta)
    {
        $clienteAlta = ClientesAlta::find($idClienteAlta);

        $job = (new LigueUsuarioAutomatico($clienteAlta))->delay(10);
        dispatch($job);

    }

    /**
     * Envia email a cliente
     *
     * @param  integer $idClienteAlta   Id del cliente en tabla altas_cliente
     *
     * @return void
     */
    public function EnvioEmail($idClienteAlta)
    {

        $clienteAlta = ClientesAlta::find($idClienteAlta);

        $job = (new EnvioEmail($clienteAlta))->delay(10);
        dispatch($job);

    }

    /**
     * Genera el registro para la tabla clientes_alta para procesar el alta
     * automática del cliente
     *
     * @param  integer $prospecto_id  Id del prospecto
     * @param  integer $solicitud_id  Id de la solicitud del prospecto
     * @param  integer $idClienteT24  Id del cliente en T24
     * @param  boolean $error         Identifica si el registro tiene un error
     * @param  string  $msj           Mensaje de error
     *
     * @return array                  Resultado del registro en tabla clientes_alta
     */
    public function arregloT24($prospecto_id, $solicitud_id, $idClienteT24 = null, $error = null, $msj = null)
    {
        $datosAlta = [];

        // Obteniendo los datos del prospecto
        $prospecto = Prospecto::select('nombres', 'apellido_paterno', 'apellido_materno',
                'celular', 'email', 'referencia')
            ->where('id', $prospecto_id)
            ->get();
        $prospecto = $prospecto[0];
        $datosAlta = array_merge($datosAlta, $prospecto->toArray());

        // Obteniendo los datos de la solicitud
        $solicitud = Solicitud::with(['domicilio' => function($query) {
                $query->select('solicitud_id', 'calle', 'num_exterior',
                    'num_interior', 'id_estado', 'id_delegacion', 'id_colonia',
                    'id_ciudad', 'cp'
                );
            }, 'producto' => function($query) {
                $query->select('nombre_producto')->withPivot('lead');
            }, 'log'])
            ->select('id', 'prospecto_id', 'fecha_nacimiento',
                'sexo', 'estado_civil', 'telefono_casa', 'telefono_empleo', 'rfc',
                'lugar_nacimiento_estado', 'lugar_nacimiento_ciudad', 'curp',
                'nacionalidad', 'pais_nacimiento', 'nivel_estudios',
                'ocupacion', 'ingreso_mensual', 'antiguedad_domicilio',
                'gastos_familiares', 'tipo_residencia', 'numero_dependientes',
                'prestamo', 'plazo', 'finalidad', 'antiguedad_empleo',
                'credito_hipotecario', 'credito_automotriz', 'credito_bancario',
                'ultimos_4_digitos')
            ->where('id', $solicitud_id)
            ->get();


        $solicitud = $solicitud[0];
        $solicitud->credito_hipotecario = self::getYesOrNo($solicitud->credito_hipotecario);
        $solicitud->credito_automotriz = self::getYesOrNo($solicitud->credito_automotriz);
        $solicitud->credito_bancario = self::getYesOrNo($solicitud->credito_bancario);
        $solicitud->estado_civil = self::getEstadoCivilDesc($solicitud->estado_civil);

        // Verificando si existe un mensaje a usuario que indique que el RFC fue
        // calculado por la libreria.
        $rfc_calculado = 0;
        /* try {
            $mensajes_usuario = json_decode($solicitud->ult_mensj_a_usuario);
            foreach ($mensajes_usuario as $mensaje) {
                $es_rfc_calculado = strrpos($mensaje->console_log, "RFC calculado:");
                if ($es_rfc_calculado !== false) {
                    $rfc_calculado = 1;
                    break;
                }
            }
        } catch (\Exception $e) {
            $rfc_calculado = 0;
        }*/

        // Obteniendo los datos de la oferta de la respuesta de la máquina de
        // riesgos para mandar los datos de monto y plazo a la solicitud T24

        $respuesta_mr = RespuestaMaquinaRiesgo::select('monto', 'plazo', 'simplificado', 'carga_identificacion_selfie', 'facematch')
            ->where('solicitud_id', $solicitud_id)
            ->where('prospecto_id', $prospecto_id)
            ->where('status_oferta', 'Oferta Aceptada')
            ->get();

        $facematch = 0;
        $carga_identificacion_selfie = 0;
        $simplificado = 0;
        if (count($respuesta_mr) > 0) {

            if ($respuesta_mr[0]->plazo == null) {
                $solicitud->plazo = self::getPlazoLabel($solicitud->plazo);
            } else {
                $monto = $respuesta_mr[0]->monto;
                $plazo = $respuesta_mr[0]->plazo;

                preg_match('/^\$?[\d,]+(\.\d*)?$/', $monto, $matches);
                if (count($matches) > 0) {
                    $monto = str_replace("$", "", $monto);
                    $monto = str_replace(",", "", $monto);
                    $solicitud->prestamo = $monto;
                }
                $solicitud->plazo = $plazo;
            }
            $solicitud->simplificado = $respuesta_mr[0]->simplificado;
            $simplificado = $respuesta_mr[0]->simplificado;
            $facematch = $respuesta_mr[0]->facematch;
            $solo_carga_identificacion_selfie = $respuesta_mr[0]->carga_identificacion_selfie;

        } else {

            $solicitud->plazo = self::getPlazoLabel($solicitud->plazo);

        }

        if (count($solicitud->producto) != 0) {
            $solicitud->nombre_producto = mb_strtoupper($solicitud->producto['0']['nombre_producto']);
            if ($solicitud->producto[0]['pivot']['lead'] != null) {
                $datosAlta['referencia'] = $solicitud->producto[0]['pivot']['lead'];
                if ($datosAlta['referencia'] == 'COMPARA GURU') {
                    $facematch = 0;
                    $solo_carga_identificacion_selfie = 0;
                }
            }
        } else {
            $solicitud->nombre_producto = 'MERCADO ABIERTO';
        }

        if ($facematch == 1) {
            $solo_carga_identificacion_selfie = 0;
        }

        $datosAlta = array_merge($datosAlta, $solicitud->toArray());
        $datosAlta = array_dot($datosAlta);
        // Cambia el nombre de las columnas del arreglo $datosAlta con el
        // de las columnas en tabla clientes_alta
        $arregloAlta = app(T24Controller::class)->arregloDatosAlta($datosAlta);
        // Cambia el valor de los campos al formato que T24 necesita (Solo los
        // que no se encuentran en el formato requerido)
        $arregloAlta = app(T24Controller::class)->procesaDatosAlta($arregloAlta);

        $arregloAlta['rfc_calculado'] = $rfc_calculado;
        $datosUnicos['prospecto_id'] = $arregloAlta['prospecto_id'];
        $datosUnicos['solicitud_id'] = $arregloAlta['solicitud_id'];

        array_forget($arregloAlta, 'prospecto_id');
        array_forget($arregloAlta, 'solicitud_id');

        // Generando el registro en la tala clientes_alta
        $clientesAlta = ClientesAlta::updateOrCreate(
            $datosUnicos,
            $arregloAlta
        );

        if ($clientesAlta->wasRecentlyCreated) {
            // Si el $idClienteT24 es diferente a nulo los campos de control para el
            // procesamiento de Jobs comienzan en la solicitud, de lo contrario
            // comienzan en el cliente
            if ($idClienteT24 != null) {
                $clientesAlta->no_cliente_t24 = $idClienteT24;
                $clientesAlta->aplica_cliente = 0;
                $clientesAlta->aplica_solicitud = 1;
                $clientesAlta->aplica_ligue = 1;
                $clientesAlta->aplica_email = ($simplificado == false ? 1 : 0);
                $clientesAlta->aplica_facematch = $facematch;
                $clientesAlta->solo_carga_identificacion_selfie = $solo_carga_identificacion_selfie;
                $clientesAlta->save();
            } else {
                $clientesAlta->aplica_cliente = 1;
                $clientesAlta->aplica_solicitud = 1;
                $clientesAlta->aplica_ligue = 1;
                $clientesAlta->aplica_email = ($simplificado == false ? 1 : 0);
                $clientesAlta->aplica_facematch = $facematch;
                $clientesAlta->solo_carga_identificacion_selfie = $solo_carga_identificacion_selfie;
                $clientesAlta->save();
            }

            if ($error == true) {
                $arregloAlta['error'] = $msj;
            }
        }

        return $clientesAlta;
    }

}
