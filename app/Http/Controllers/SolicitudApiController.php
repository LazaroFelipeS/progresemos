<?php
namespace App\Http\Controllers;

use DB;
use Response;
use App;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Prospecto;
use App\Solicitud;
use App\MockPerson;
use App\FicoUtil;
use Auth;
use App\PlazaCobertura;
use App\SoapHttpClient;
use App\Sepomex;
use App\DomicilioSolicitud;
use App\ConsultaBcTest;
use App\DetalleCuenta;
use App\DetalleConsulta;
use App\DetalleSolicitud;
use App\PlantillaComunicacion;
use App\Situacion;
use App\RespuestaMaquinaRiesgo;
use App\CatalogoSepomex;
use App\Plazo;
use Cookie;

use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Encryption\DecryptException;

class SolicitudApiController extends Controller
{
    private $configuracionProducto;
    private $bcScore;
    private $edadMinima;
    private $edadMaxima;
    private $campoCobertura;
    private $alp;

    /**
    * Constructor de la clase
    */
    public function __construct(request $request)
    {
        $solicitud_id = null;
        $producto = null;
        $version = null;

        if (isset($request->solic_bc_id)) {
            $solicitud_id = $request->solic_bc_id;
        } elseif (isset($request->solic_hawk_id)) {
            $solicitud_id = $request->solic_hawk_id;
        } elseif (isset($request->solic_alp_id)) {
            $solicitud_id = $request->solic_alp_id;
        }

        if ($solicitud_id != null) {

            $solicitud = Solicitation::with('producto')
            ->where('id', $solicitud_id)
            ->get()
            ->toArray();

            if (count($solicitud[0]['producto']) == 1) {
                $producto = $solicitud[0]['producto'][0]['id'];
                $version = $solicitud[0]['producto'][0]['pivot']['version_producto'];
            }

        }

        $this->configuracionProducto = App::call(
            'App\Http\Controllers\ProductoController@obtenerConfiguracion',
            [
                'id_producto' => $producto,
                'version' => $version
            ]
        );
        $this->bcScore = $this->configuracionProducto['bc_score'];
        $this->condicion = $this->configuracionProducto['condicion'];
        $this->microScore = $this->configuracionProducto['micro_score'];
        $this->edadMinima = $this->configuracionProducto['edad_minima'];
        $this->edadMaxima = $this->configuracionProducto['edad_maxima'];
        $this->campoCobertura = $this->configuracionProducto['campo_cobertura'];
        $this->storedProcedure = $this->configuracionProducto['stored_procedure'];
        $this->alp = $this->configuracionProducto['consulta_alp'];
        $this->ocupaciones = collect($this->configuracionProducto['ocupaciones'])->pluck('ocupacion');
    }

    /**
    * Determina si la solicitud es Elegible o No
    *
    * @param  request $request Arreglo que contiene los datos capturados
    *
    * @return json             Resultado del guardado de datos
    */
    public function solicitudElegible(request $request)
    {
        $prospecto_id = $request->prospecto_id;
        $solicitud_id = $request->solicitud_id;
        // Obtiene la solicitud por id
        $solic = Solicitation::where('id', $solicitud_id)
        ->where('prospect_id', $prospecto_id)
        ->first();

        if ($solic) {

            //update the solics ult_punto_reg
            $solic->ult_punto_reg = self::setUltPuntoReg(
                $solic->ult_punto_reg,
                "Datos Elegible",
                1,
                "Los datos adicionales se han guardado con éxito"
            );
            //have not decrypted so its ok to save
            $solic->save();
            //decrypt the newly added solicitud values to keep working on it
            $solic = self::decryptSolicitud($solic);
            //now we will check the user's ocoupación
            $ocupacion = $solic->ocupacion;
            $ocupacionElegible = $this->ocupaciones->filter(function ($value, $key) use($ocupacion) {
                return mb_strtoupper($value) == mb_strtoupper($ocupacion);
            });

            // in_array($solic->ocupacion, ['Empleado Sector Público',mb_strtoupper('Empleado Sector Público'),'Empleado Sector Privado',mb_strtoupper('Empleado Sector Privado'),'Pensionado',mb_strtoupper('Pensionado'),'Jubilado',mb_strtoupper('Jubilado')])
            if (count($ocupacionElegible) == 1) {
                //is elegible
                $mnsj_str = [
                    'stat'      => 'Elegible',
                    'ocupacion' => mb_strtoupper($solic->ocupacion),
                    'message'   => 'El usuario es Elegible. La ocupación es: '. implode(' o ', $this->ocupaciones->toArray()),
                    'elegible'  => 1,
                ];

                $solic->ult_punto_reg = self::setUltPuntoReg(
                    $solic->ult_punto_reg,
                    "Check Ocupación",
                    1,
                    "El usuario es Elegible. La ocupación es: ". implode(' o ', $this->ocupaciones->toArray())
                );
                //update the solicitud status to Elegible
                $solic->type = "Elegible";

            } else {
                //is not elegible

                $mnsj_str = [
                    'stat'      => 'No Elegible',
                    'ocupacion' => mb_strtoupper($solic->ocupacion),
                    'message'   => 'El usuario NO es Elegible. La ocupación no es: '. implode(' o ', $this->ocupaciones->toArray()),
                    'elegible'  => 0,
                ];
                //update the solicitud status to No Elegible
                $solic->type = "No Elegible";
            }

            $solic->ult_punto_reg = self::setUltPuntoReg($solic->ult_punto_reg, "Check Ocupación", 0, "El usuario NO es Elegible. La ocupación no es: ". implode(' o ', $this->ocupaciones->toArray()));
            //save the message to the ult_mensj_a_usuario field
            $solic->ult_mensj_a_usuario = self::setUltMnsjUsr($solic->ult_mensj_a_usuario, json_encode($mnsj_str));
            //make sure your recrypt all solic values before saving
            $solic = self::recryptSolicitud($solic);
            $solic->save();

            return  response()->json($mnsj_str);
            // echo $mnsj_str;

        } else {
            echo '{
                "response" :  "error",
                "message" : "La solicitud no existe"
            }';
        }
    }

    /**
    * Obtiene el cuestionarioDinamico
    *
    * @param  Request $request Datos para armar el cuestionario dinámico
    *
    * @return json             Cuestionario dinámico
    */
    public function cuestionarioDinamico(Request $request) {
        
        if (Auth::guard('prospecto')->check()) {
            $prospecto_id = Auth::guard('prospecto')->user()->id;

            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();
             // Obteniendo la respuesta de la máquina de riesgos
        $respuesta = RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto_id)
        ->where('solicitud_id', $solicitud->id)
        ->get();

        // Obteniendo la plantilla de comunicación
        $plantilla = PlantillaComunicacion::select('modal_encabezado', 'modal_cuerpo', 'modal_img')
        ->where('plantilla_id', $respuesta[0]->plantilla_comunicacion)
        ->get()
        ->toArray();

        $plantilla = $plantilla[0];

        // Obteniendo las preguntas del cuestionario dinámico
        $pantallas = $respuesta[0]->situaciones;
        $pantallas = explode(',', $pantallas);
        $pantallas = array_filter($pantallas, 'strlen');

        $secciones = Situacion::whereIN('situacion', $pantallas)
        ->with('cuestionario')
        ->get()
        ->toArray();

        $datosSeccion = null;
        $model = null;
        foreach ($secciones as $seccion) {
            $preguntas = null;
            foreach ($seccion['cuestionario'] as $pregunta) {

                $visible = '';
                $model_id = $seccion['situacion'].'_'.$pregunta['id'];
                if ($pregunta['oculto'] == 1) {
                    $visible .= ' field_oculto';
                }

                if ($pregunta['tipo'] == 'text') {

                    $preguntas[] = [
                        'type'          => 'input',
                        'inputType'     => 'text',
                        'label'         => $pregunta['pregunta'],
                        'styleClasses'  => $model_id.' col-md-6 display-field'.$visible,
                        'model'         => $model_id,
                        'id'            => $model_id,
                    ];


                } else if ($pregunta['tipo'] == 'textarea') {

                    $preguntas[] = [
                        'type'          => 'textArea',
                        'label'         => $pregunta['pregunta'],
                        'styleClasses'  => $model_id.' col-md-6 display-field'.$visible,
                        'model'         => $model_id,
                        'id'            => $model_id,
                    ];


                } else if ($pregunta['tipo'] == 'select') {

                    $opciones = explode('|', $pregunta['opciones']);
                    $function = '';
                    if ($pregunta['depende'] != null) {

                        $pregunta_depende = explode('|', $pregunta['depende']);
                        $respuesta_depende = explode('|', $pregunta['respuesta_depende']);
                        foreach ($pregunta_depende as $key => $value) {
                            $condicion_muestra = "if (model.".$seccion['situacion'].'_'.$pregunta['id']." == '".$respuesta_depende[$key]."') {";
                            $muestra = "$('.".$value."').removeClass('field_oculto')}";
                            $condicion_oculta = "else{";
                                $oculta = "$('.".$value."').addClass('field_oculto'); model.".$value." = '' }";
                                $function .= $condicion_muestra.$muestra.$condicion_oculta.$oculta;
                        }
                    }

                    $preguntas[] = [
                        'type'                  => 'select',
                        'label'                 => $pregunta['pregunta'],
                        'styleClasses'          => $model_id.' col-md-6 display-field'.$visible,
                        'model'                 => $model_id,
                        'values'                => $opciones,
                        'id'                    => $model_id,
                        'onChanged'             => $function,
                        'selectOptions'         => [
                            'hideNoneSelectedText'  => true,
                        ]

                    ];

                }

                $model[$seccion['situacion'].'_'.$pregunta['id']] = '';

            }

                $datosSeccion[] = [
                    'legend' => $seccion['encabezado'].'. '.$seccion['introduccion'],
                    'styleClasses' => 'col-md-12',
                    'fields' => $preguntas
                ];

        }

        $formulario = [
            'formulario'  => $datosSeccion,
            'model'       => $model,
            'situaciones' => implode('|', $pantallas),
            'success'      => true,
            'prospecto_id_sitio' => $prospecto->id,
            'solicitud_id_sito' => $solicitud->id
        ];
        
        return response()->json($formulario);

        } else {
            return response()->json([
                'success'   => false,
                'message' => 'La sesión expiro'
            ]);
        }

       
    }

    /**
    * Guarda el status de la oferta (Aceptada/Rechazada), si la oferta es Rechazada
    * tambien guarda el motivo de rechazo.
    *
    * @param  Request $request Datos capturados en el modal oferta
    *
    * @return json             Resultado del guardado del status
    */
    public function statusOferta(Request $request) {

        $msj = 'Se ha guardado el status de la solicitud con éxito';
        $motivo = null;
        $descripcion_otro = null;

        if (isset($request->motivo)) {
            $msj = 'Se ha guardado el motivo de recahzo de la solicitud con éxito';
            $motivo = $request->motivo_rechazo;
            $descripcion_otro = $request->descripcion_otro;
        }

        $status_oferta = $request->status_oferta;
        $resultadoMR = RespuestaMaquinaRiesgo::updateOrCreate([
            'prospecto_id' => $request->prospecto_id,
            'solicitud_id' => $request->solicitud_id
        ], [
            'status_oferta'     => $status_oferta,
            'motivo_rechazo'    => mb_strtoupper($motivo),
            'descripcion_otro'  => mb_strtoupper($descripcion_otro)
        ]);

        $solicitud = Solicitation::find($request->solicitud_id);
        $solicitud->type = 'Invitación a Continuar';
        // Actualizando el campo ult_punto_reg de la solicitud
        $solicitud->ult_punto_reg = self::setUltPuntoReg(
            $solicitud->ult_punto_reg,
            $request->status_oferta,
            1,
            $msj,
            null
        );
        Cookie::queue(Cookie::forget('producto'));
        Cookie::queue(Cookie::forget('logo'));
        Cookie::queue(Cookie::forget('checa_calificas'));
        $solicitud->save();

        if ($status_oferta == 'Oferta Aceptada') {
            // Buscando que el prospecto no exista en la tabla de clientes de T24
            // El método se encuentra en serverMethods
            $clienteT24 = self::buscarClienteT24(
                $request->prospecto_id,
                $request->solicitud_id
            );
            // Si el cliente es encontrado el proceso comienza desde la creación
            // de la solicitud
            if ($clienteT24['cliente_t24'] == true) {
                $idClienteT24 = $clienteT24['datos'][0]['id_cliente_t24'];
                $email_distinto = $clienteT24['email_distinto'];
                // Procesando el alta de la solicitud
                // El método se encuentra en FuncionesT24
                $altaCliente = self::altaSolicitudT24(
                    $request->prospecto_id, // id del prospecto
                    $request->solicitud_id, // id de la solicitud
                    null,                   // id del cliente en tabla alta clientes
                    $idClienteT24,          // id del cliente en t24
                    $email_distinto         // identifica si el email del prospecto es diferente
                );
                // Si el cliente no existe y no hay un error en el resultado de la
                // búsqeda el proceso comienza desde crear el cliente
            } elseif ($clienteT24['cliente_t24'] == false && $clienteT24['error'] == false) {
                // Procesando el alta del cliente.
                // El método se encuentra en FuncionesT24
                $altaCliente = self::altaClienteT24(
                    $request->prospecto_id, // id del prospecto
                    $request->solicitud_id, // id de la solicitud
                    null,                   // id del cliente en tabla alta clientes
                    false,                  // error
                    null                    // msj
                );
                // Si el cliente no existe y hubo un error en la búsqueda el proceso
                // comienza desde crear el cliente
            } else {
                $msj = $clienteT24['msj'];
                // Procesando el alta del cliente.
                // El método se encuentra en FuncionesT24
                $altaCliente = self::altaClienteT24(
                    $request->prospecto_id, // id del prospecto
                    $request->solicitud_id, // id de la solicitud
                    null,                   // id del cliente en tabla alta clientes
                    true,                   // error
                    $msj                    // msj
                );
            }
        }

        return response()->json([
            'success'       => true
        ]);

    }

    /**
    * Obtiene los datos de la oferta
    *
    * @param  Request $request Datos para obtener la oferta
    *
    * @return json             Obtiene la oferta
    */
    public function getDatosOferta(Request $request) {

        if (Auth::guard('prospecto')->check()) {
            $prospecto_id = Auth::guard('prospecto')->user()->id;

            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

        // Obteniendo la respuesta de la máquina de riesgos
        $datos_oferta = RespuestaMaquinaRiesgo::select('decision', 'monto', 'plazo', 'pago', 'tasa', 'plantilla_comunicacion','tipo_poblacion','tipo_oferta')
        ->where('prospecto_id', $prospecto_id)
        ->where('solicitud_id', $solicitud->id)
        ->get();
   
        if (count($datos_oferta) > 0) {

            $plantilla = PlantillaComunicacion::where('plantilla_id',$datos_oferta['0']['plantilla_comunicacion'])->get()
            ->toArray();
            //dd($plantilla);
            $response = [
                'success'      => true,
                'datos_oferta' => $datos_oferta,
                'plantilla'    => $plantilla,
            ];

        } else {

            $response = [
                'success'      => false,
            ];

        }

        return response()->json($response);
        } else {
            return response()->json([
                'success'   => false,
                'message' => 'La sesión expiro'
            ]);
        }
    }

    function datosSolicitud(Request $request) {

        return response()->json([
            'id_solicitud'  =>  $request->id_solicitud,
            'nombre'        => 'Prueba'
        ]);

    }
}
