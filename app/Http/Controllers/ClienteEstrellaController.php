<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DatosLeadRepository;
use Excel;
use Ramsey\Uuid\Uuid;
use App\Jobs\EnvioEmailClienteEstrella;
use App\ClienteEstrella;
use App;
use Carbon\Carbon;

class ClienteEstrellaController extends Controller
{
    private $campos = [
        'nombre'             => 'nombre',
        'apellido_m'         => 'apellidomaterno',
        'apellido_p'         => 'apellidopaterno',
        'email'              => 'email',
        'rfc'                => 'rfc',
        'estado_civil'       => 'estadocivil',
        'genero'             => 'genero',
        'fec_nacimiento'     => 'fechadenacimiento',
        'lugar_nacimiento'   => 'estadodenacimiento',
        'celular'            => 'telefono',
    	'tel_domicilio'      => 'telefonodecasa',
    	'dom_calle'          => 'calle',
    	'dom_ciudad'         => 'ciudad',
    	'cod_postal'         => 'codigopostal',
    	'dom_colonia'        => 'colonia',
    	'del_munic'          => 'delegacion',
        'dom_estado'         => 'estado',
        'num_int'            => 'numerointdedomicilio',
        'num_ext'            => 'numerodedomicilio',
        'dom_anios'          => 'teimpoderecidencia',
        'empresa'            => 'empresa',
        'tel_oficina'        => 'telefonodecompania',
        'antig_empleo'       => 'tiempolaborandoenlaempresa',
        'nivel_estudios'     => 'gradodeestudios',
    	'ingreso'            => 'ingresoreal',
    	'ocupacion'          => 'puestoqueocupa',
        'gastos_familiares'  => 'gastos_familiares',
        'num_dependientes'   => 'num_dependientes',
        'residencia'         => 'tipodevivienda',
    	'cred_automotriz'    => 'tienecreditoautomotriz',
    	'cred_hipotecario'   => 'tienecreditohipotecario',
    	'cred_tdcbancario'   => 'tienetarjeta',
    	'ult4_tdc'           => 'ultimoscuatrodigitosdelatarjeta',
        'encontrado_bc'      => 'encontrado_bc',
        'monto'              => 'monto',
        'tasa'               => 'tasa',
        'plazo'              => 'plazo',
        'pago'               => 'pago',
        'finalidad'          => 'finalidad',
        'prospecto_id'       => 'prospecto_id',
        'numcreditoactual'   => 'numcreditoactual',
        'fecha_campana'      => 'fecha_campana',
        'vigencia'           => 'vigencia',
        'nombre_empresa_trabajo' => 'nombreempresatrabajo',
        'fuente_ingreso'    =>  'fuenteingreso',
        'cuenta_progresemos' => 'cuentaprogresemos',
        'identificador_persona' => 'identificadorpersona', 
    ];

    /**
     * Constructor de la clase
     */
    public function __construct(DatosLeadRepository $datosLead)
    {
        $this->datosLead = $datosLead;
    }

    public function index(request $request)
    {
        return view('crm.cliente_estrella.cliente_estrella');
    }

    public function listaClienteEstrella()
    {
        $clienteEstrella = ClienteEstrella::select('prospecto_id', 'fecha_campana', 'nombre',
            'segundonombre', 'apellidopaterno', 'apellidomaterno', 'email',
            'procesado', 'uuid', 'email_enviado')
            ->paginate(10);

        return response()->json($clienteEstrella);

    }

    public function uploadClienteEstrella(request $request)
    {
        setlocale(LC_TIME, env('LC_TIME'));
        $file = $request->file('file');
        if ($file) {
            $filename = $file->getClientOriginalName();
            $file = $file->move(storage_path().'/app/public', $filename);

            if ($file->getExtension() == 'csv') {
                $location = $file->getPathName();

                Excel::load($location, function ($reader) {
                    $headers = $reader->first()->keys()->toArray();
                    $results = $reader->formatDates(false)->toArray();
                    $campos = $this->campos;

                    foreach ($results as $result) {

                        // Homologando el nombre de las columnas como estan los
                        // espera el sitio de CORU
                        $result = array_combine(
                            array_map(function($el) use ($campos) {
                                return $campos[$el];
                            }, array_keys($result)), array_values($result)
                        );
                        
                        $nombre = $result['nombre'];
                        $arregloNombre = explode(' ', $nombre, 2);
                        $result['nombre'] = $arregloNombre[0];
                        if (count($arregloNombre) > 1) {
                            $result['segundonombre'] = $arregloNombre[1];
                        } else {
                            $result['segundonombre'] = '';
                        }
                        //$result['estadocivil'] = str_replace('/a','',$result);
                        /* $result['empresa'] = '';
                        $result['genero'] = $this->getGenero($result['genero']);
                        $result['tienecreditoautomotriz'] = $this->getYesNo($result['tienecreditoautomotriz']);
                        $result['tienecreditohipotecario'] = $this->getYesNo($result['tienecreditohipotecario']);
                        $result['tienetarjeta'] = $this->getYesNo($result['tienetarjeta']); */
                        $vigencia = Carbon::now()->addDays(20);
                        
                        $result['vigencia'] = $vigencia->format('Y-m-d');
                        
                        $datosUnicos = [
                            'prospecto_id'   => $result['prospecto_id'],
                            'fecha_campana' => $result['fecha_campana']
                        ];

                        if ($result['ultimoscuatrodigitosdelatarjeta'] == 0) {
                            $result['ultimoscuatrodigitosdelatarjeta'] = '';
                        }

                        $datosGenrales = $result;
                        
                        
                        unset($datosGenrales['prospecto_id']);
                        unset($datosGenrales['fecha_campana']);

                        $clienteEstrella = ClienteEstrella::updateOrCreate($datosUnicos, $datosGenrales);

                         if ($clienteEstrella->wasRecentlyCreated) {
                            if ($result['tasa'] == -1) {
                                $tasa = '15 puntos menos de la tasa de la tarjeta que deseas pagar.';
                                $result['finalidad'] = 'pago_tarjeta';
                            } else {
                                $tasa = $result['tasa'].'%';
                                $result['finalidad'] = 'prestamo_personal';
                            }
                            /* $intentos = 0;
                            $exitoso = false;
                            while ($exitoso == false && $intentos <= 3) {
                                $response = $this->datosLead->guardaLead($result);
                                if (isset($response['resultado'])) {
                                    $exitoso = true;
                                }
                                $intentos = $intentos + 1;
                            } */

                            if(env('APP_ENV') == 'local' || env('APP_ENV') == 'desarrollo' || env('APP_ENV') == 'desarrollos') {
                                $datosEmail['email'] = env('CLIENTEESTRELLA_EMAIL_TEST');
                            } else {
                                $datosEmail['email'] = $result['email'];
                            }
                            $datosEmail['nombre'] = $nombre;
                            $datosEmail['nombre_completo'] = $result['nombre'].' '.$result['apellidopaterno'].' '.$result['apellidomaterno'];
                            $datosEmail['monto'] = '$'.number_format($result['monto'], 2, '.', ',');

                            $datosEmail['plazo'] = $result['plazo'];
                            $datosEmail['pago_estimado'] ='$'.number_format($result['pago'], 2, '.', ',');
                            $datosEmail['vigencia'] = $vigencia->formatLocalized('%d de %B de %Y');
                            $uuid = Uuid::uuid1();
                            $datosEmail['uuid'] = $uuid;
                            $datosEmail['tasa'] = $tasa;
                            $clienteEstrella->procesado = 1;
                            $clienteEstrella->email_enviado = 0;
                            $clienteEstrella->uuid = $uuid;
                            $clienteEstrella->save();

                            $job = (new EnvioEmailClienteEstrella($datosEmail, $clienteEstrella->id))->delay(10);
                            dispatch($job); 

                            $clienteEstrella->procesado = 0;
                            $clienteEstrella->save();

                            }
                        } 

                });

            }
        }
    }

    public function getGenero($val) {
        $val = title_case($val);
        $genero = [
            'M' => 'Masculino',
            'F' => 'Femenino',
        ];
        if (isset($genero[$val])) {
            return $genero[$val];
        } else {
            return '';
        }
    }

    public function getYesNo($val) {
        $val = title_case($val);
        $respuesta = [
            'Si' => 'Yes',
            'No' => 'No',
        ];
        if (isset($respuesta[$val])) {
            return $respuesta[$val];
        } else {
            return '';
        }
    }

    public function indexInvitacion($uuid)
    {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        $datos = ClienteEstrella::where('uuid',$uuid)->first();
        //dd($datos);
        $info = json_encode($datos);
        //dd($datos);
        return view('solicitud.index', compact('datos','configuracion'));
        //return view('solicitud.index');
        /* return view('solicitud.index', [
            'uuid'      => $uuid,
            'empresa'   => 'Progresemos',
            'datos'     => $datos,
            'configuracion' => $configuracion,
            //'origen'    => env('ORIGEN_PRESTANOMICO'),
            
        ]); */
    }

    public function getDatosUsuario(Request $request)
    {
        $datos_usuario = ClienteEstrella::where('uuid',$request->uuid)->get();
        
        return response()->json([
            'success'           => true,
            'datos_usuario'     => $datos_usuario,
        ]);
    }

}
