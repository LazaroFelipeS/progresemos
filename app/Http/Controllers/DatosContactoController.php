<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Log;
use Validator;
use App\DatosContacto;
use Artisaninweb\SoapWrapper\SoapWrapper;


class DatosContactoController extends Controller
{

    public function guardarDatos(Request $request) {

        $datos = $request->all();
        // Realizando la validación de los datos
        $reglas = $this->reglasValidacion();
        $mensajes = $this->mensajesValidacion();

        $validacion = Validator::make($datos, $reglas, $mensajes)
            ->validate();

        $datosGuardados = DatosContacto::updateOrCreate([
            'email'         => $request->email
        ], [
            'nombre'        => $request->nombre,
            'apellidos'     => $request->apellidos,
            'puesto'        => $request->puesto,
            'empresa'       => $request->empresa,
            'telefono'      => $request->telefono,
            'extension'     => $request->extension,
            'comentarios'   => $request->comentarios
        ]);

        $view = view('emails.registro_platapresta')->with([
            'email'         => $request->email,
            'nombre'        => $request->nombre,
            'apellidos'     => $request->apellidos,
            'puesto'        => $request->puesto,
            'empresa'       => $request->empresa,
            'telefono'      => $request->telefono,
            'extension'     => $request->extension,
            'comentarios'   => $request->comentarios
        ]);

        $this->soapWrapper = new SoapWrapper();
        $this->soapWrapper->add('Calixta', function ($service) {
            $service->wsdl(env('CALIXTA_EMAIL_WSDL'))
            ->trace(true)
            ->options([
                'connection_timeout'        => 10,
                'default_socket_timeout'    => 10
            ]);
        });
        $response = $this->soapWrapper->call('Calixta.EnviaEmail', [
            'cte'                   => env('CALIXTA_EMAIL_CTE'),
            'email'                 => env('CALIXTA_EMAIL_USUARIO'),
            'password'              => env('CALIXTA_EMAIL_PASSWORD'),
            'nombreCamp'            => 'Registro en PlataPresta',
            'to'                    => env('CONTACTO_PLATAPRESTA'),
            'from'                  => 'plataformadigital@prestanomico.com',
            'fromName'              => 'Plataforma Digital',
            'replyTo'               => $request->email,
            'subject'               => 'Registro en PlataPresta',
            'incrustrarImagen'      => 0,
            'textEmail'             => 'Registro en PlataPresta',
            'htmlEmail'             => $view,
            'seleccionaAdjuntos'    => 0,
            'envioSinArchivo'       => 1,
            'fechaInicio'           => date('d/m/Y'),
        ]);

        return response()->json([
            'success'   => true,
            'message'   => 'Los datos se han guarado con éxito'
        ]);

    }

    /**
     * Generando las reglas de validación de los Datos de Contacto
     *
     * @return array            Arreglo con las reglas de validacion
     */
    public function reglasValidacion()
    {
        $reglas = null;
        $reglas ['nombre']  = [
            'required',
            'min:2',
            'max:100',
            'alpha_numeric_spaces'
        ];
        $reglas ['apellidos']  = [
            'required',
            'min:2',
            'max:100',
            'alpha_numeric_spaces'
        ];
        $reglas ['puesto']  = [
            'required',
            'min:5',
            'max:50',
            'alpha_numeric_spaces'
        ];
        $reglas ['empresa']  = [
            'required',
            'min:5',
            'max:50',
            'alpha_numeric_spaces'
        ];
        $reglas ['telefono']  = [
            'required',
            'digits:10',
            'numeric'
        ];
        $reglas ['extension']  = [
            'digits_between:1,6',
            'numeric'
        ];
        $reglas ['email']  = [
            'required',
            'email',
            'max:100'
        ];
        $reglas ['comentarios']  = [
            'required',
            'alpha_numeric_spaces',
            'min:10',
            'max:255'
        ];

        return $reglas;
    }

    /**
     * Establece los mensajes para cada situación de las validaciones
     *
     * @return array Arreglo con los mensajes
     */
    public function mensajesValidacion()
    {
        return [
            'required'              => ' El campo es requerido',
            'min'                   => ' Al menos :min caracteres',
            'max'                   => ' Menos de :min caracteres',
            'numeric'               => ' El campo solo permite números',
            'digits'                => ' El campo debe tener :digits números',
            'digits_between'        => ' El campo debe tener máximo :max números',
            'email'                 => ' El e-mail no es válido',
            'alpha_numeric_spaces'  => ' El campo solo permite letras, números y espacios'
        ];
    }

}
