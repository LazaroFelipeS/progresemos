<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosEmpleo extends Model
{
    protected $table = 'empleo_solicitud';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'nombre_empresa',
        'antiguedad_empleo',
        'fecha_ingreso',
        'telefono_empleo',
        'fecha_ingreso',
        'calle',
        'num_exterior',
        'num_interior',
        'codigo_postal',
        'colonia',
        'delegacion_municipio',
        'ciudad',
        'estado',
    ];

    /**
     * Atributos que son ocultos
     *
     * @var array
     */
    protected $hidden = [

    ];
}
