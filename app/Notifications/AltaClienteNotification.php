<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;

class AltaClienteNotification extends Notification
{
    use Queueable;

    private $notificacion;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($notificacion)
    {
        $this->notificacion = $notificacion;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    public function toSlack($notifiable)
    {
        $notificacion = $this->notificacion;

        if ($notificacion->type == 'success') {

            if ($notificacion->solicitud == true) {

                $tipoProceso = ($notificacion->simplificado == true) ? 'Proceso Simplificado' : 'Proceso Normal';
                return (new SlackMessage)
                    ->from('Sitio Prestanómico')
                    ->to(env('SLACK_CHANNEL_SUCCESS'))
                    ->success()
                    ->content('Alta de cliente automática <@hector.urbano> <@miriam.cazares> <@benjamin>')
                    ->attachment(function ($attachment) use ($notificacion, $tipoProceso) {
                        $attachment->title('Fecha: '.date('Y-m-d:').PHP_EOL.$notificacion->content.PHP_EOL.$notificacion->title)
                            ->fields([
                                 'Tipo'             => $tipoProceso,
                                 'Producto'         => $notificacion->producto,
                                 'Celular'          => $notificacion->celular,
                                 'Nombre'           => $notificacion->nombre,
                                 'Apellido Paterno' => $notificacion->apellido_paterno,
                                 'Apellido Materno' => $notificacion->apellido_materno,
                             ]);
                    });


            } else {

                return (new SlackMessage)
                    ->from('Sitio Prestanómico')
                    ->to(env('SLACK_CHANNEL_SUCCESS'))
                    ->success()
                    ->content('Alta de cliente automática <@hector.urbano> <@miriam.cazares> <@benjamin>')
                    ->attachment(function ($attachment) use ($notificacion) {
                        $attachment->title('Fecha: '.date('Y-m-d:').PHP_EOL.$notificacion->title)
                                   ->content($notificacion->content);
                    });

            }

        }

        if ($notificacion->type == 'success_fm') {

            return (new SlackMessage)
                ->from('Sitio Prestanómico')
                ->to(env('SLACK_CHANNEL_FM'))
                ->success()
                ->content('Alta de cliente automática <@hector.urbano> <@miriam.cazares> <@benjamin>')
                ->attachment(function ($attachment) use ($notificacion) {
                    $attachment->title('Fecha: '.date('Y-m-d:').PHP_EOL.$notificacion->title)
                               ->content($notificacion->content);
                });
        }

        if ($notificacion->type == 'success_cargaineselfie') {

            return (new SlackMessage)
                ->from('Sitio Prestanómico')
                ->to(env('SLACK_CHANNEL_FM'))
                ->success()
                ->content('Alta de cliente automática <@hector.urbano> <@miriam.cazares> <@benjamin>')
                ->attachment(function ($attachment) use ($notificacion) {
                    $attachment->title('Fecha: '.date('Y-m-d:').PHP_EOL.$notificacion->title)
                               ->content($notificacion->content);
                });
        }

        if ($notificacion->type == 'error') {
            return (new SlackMessage)
                ->from('Sitio Prestanómico')
                ->to(env('SLACK_CHANNEL_ERROR'))
                ->error()
                ->content('Alta de cliente automática <@hector.urbano> <@miriam.cazares>')
                ->attachment(function ($attachment) use ($notificacion) {
                    $attachment->title('Fecha: '.date('Y-m-d:').PHP_EOL.$notificacion->title)
                               ->content($notificacion->content);
                });
        }

        if ($notificacion->type == 'error_fm') {
            return (new SlackMessage)
                ->from('Sitio Prestanómico')
                ->to(env('SLACK_CHANNEL_ERROR_FM'))
                ->error()
                ->content('Alta de cliente automática <@hector.urbano> <@miriam.cazares>')
                ->attachment(function ($attachment) use ($notificacion) {
                    $attachment->title('Fecha: '.date('Y-m-d:').PHP_EOL.$notificacion->title)
                               ->content($notificacion->content);
                });
        }

        if ($notificacion->type == 'error_cargaineselfie') {
            return (new SlackMessage)
                ->from('Sitio Prestanómico')
                ->to(env('SLACK_CHANNEL_ERROR_FM'))
                ->error()
                ->content('Alta de cliente automática <@hector.urbano> <@miriam.cazares>')
                ->attachment(function ($attachment) use ($notificacion) {
                    $attachment->title('Fecha: '.date('Y-m-d:').PHP_EOL.$notificacion->title)
                               ->content($notificacion->content);
                });
        }
    }
}
