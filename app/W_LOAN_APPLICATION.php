<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla W_LOAN_APPLICATION
 * Guarda los datos de la solicitud en la tabla del app
 */
class W_LOAN_APPLICATION extends Model
{
    protected $connection = 'mysql_app';
    protected $table = 'W_LOAN_APPLICATION';
    public $timestamps = false;
    protected $primaryKey = 'ID_LOAN_APP';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'ID_LOAN_APP',
        'ID_USER',
        'ID_LOAN_PURPOSE',
        'ID_LOAN_STATUS',
        'ID_REASON_WITHDRAWN',
        'APPLICATION_DATE',
        'CONTRACT_EXPIRY',
        'DISBURB_LOAN_ID',
        'LOAN_AMOUNT',
        'CONTRACT_DECISION_DATE',
        'CONTRACT_DECISION',
        'ARRANGEMENT_ID',
        'CREATE_DATE',
        'UPDATE_DATE',
        'GENERATED_AT_DOC_CREATION'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
