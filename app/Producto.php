<?php


namespace App;

use Illuminate\Database\Eloquent\Model;
use NexusPoint\Versioned\Versioned;

/**
 * Modelo que se conecta con la tabla productos
 */
class Producto extends Model
{
    use Versioned;

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'nombre_producto',
        'alias',
        'tipo',
        'empresa',
        'monto_minimo',
        'monto_maximo',
        'bc_score',
        'condicion',
        'micro_score',
        'edad_minima',
        'edad_maxima',
        'cat',
        'tasa_minima',
        'tasa_maxima',
        'comision_apertura',
        'stored_procedure',
        'doble_oferta',
        'tipo_monto_do',
        'do_monto_maximo',
        'proceso_simplificado',
        'tipo_monto_simplificado',
        'simplificado_monto_minimo',
        'simplificado_monto_maximo',
        'facematch_simplificado',
        'carga_identificacion_selfie_simplificado',
        'carga_identificacion_selfie',
        'facematch',
        'logo',
        'campo_cobertura',
        'consulta_alp',
        'consulta_buro',
        'garantia',
        'seguro',
        'vigencia_de',
        'vigente',
        'vigencia_hasta',
        'default',
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];


    /**
     * Obtiene las solicitudes del producto
     *
     * @return object Datos de las solicitudes
     */
    public function solicitudes() {
        return $this->belongsToMany('App\Solicitation')->withPivot('lead');
    }

    /**
     * Obtiene los plazos relacionados al producto
     *
     * @return object Datos de los plazos
     */
    public function plazos() {
        return $this->belongsToMany('App\Plazo')->withPivot('default');
    }

    /**
     * Obtiene las finalidades relacionados al producto
     *
     * @return object Datos de las finalidades
     */
    public function finalidades() {
        return $this->belongsToMany('App\Finalidad');
    }

    /**
     * Obtiene las ocupaciones relacionados al producto
     *
     * @return object Datos de las ocupaciones
     */
    public function ocupaciones() {
        return $this->belongsToMany('App\Ocupacion');
    }

}
