<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Prospect;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call('App\Http\Controllers\AnalyticsController@getDataAnalytics')
            ->everyTenMinutes()
            ->name('obtencion_datos_analytics')
            ->withoutOverlapping();

        $schedule->call('App\Http\Controllers\T24Controller@setDatesT24')
            ->dailyAt('03:00')
            ->name('establece_fechas_t24')
            ->withoutOverlapping();

        $schedule->call('App\Http\Controllers\ReporteDiarioController@datosReporteEficiencia')
            ->dailyAt('07:00')
            ->name('datos_reporte_eficiencia')
            ->withoutOverlapping();

    }
}
