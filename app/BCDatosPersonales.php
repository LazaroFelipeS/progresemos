<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo que se conecta con la tabla analytics
 * Guarda los datos que se obtienen de google analytics
 */
class BCDatosPersonales extends Model
{
    protected $table = 'bc_datos_personales';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'solicitud_id',
        'prospecto_id',
        'apellido_p',
        'apellido_m',
        'apellido_adl',
        'nombre',
        'segundo_nombre',
        'fecha_nacimiento',
        'rfc',
        'prefijo_personal',
        'sufijo_personal',
        'nacionalidad',
        'nacionalidad_curp',
        'pais_nacimiento_curp',
        'tipo_residencia',
        'lic_conducir',
        'estado_civil',
        'sexo',
        'cedula_profesional',
        'ife',
        'curp',
        'clave_pais',
        'num_dependientes',
        'edades_dependientes',
        'fecha_recep_dependientes',
        'fecha_defuncion',
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];

}
