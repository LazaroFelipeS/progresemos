function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function validarSimulador() {

    var datos = $('#formSimulador').serializeArray();
    datos = getFormData(datos);
    prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
    prestamoValue = prestamoValue * 1000;
    datos['monto_prestamo'] = prestamoValue;

    var validaciones = validacionesSimulador();
    var nuevaSolicitud = $('#nueva_solicitud').val();
    $('#validacionesSimulador').html(validaciones);

    var datos_faltantes = '';
    if (validaciones == '') {

        axios.post('/validaciones', {
            datos: datos,
            formulario: 'simulador'
        }).then(function (response) {

            if (response.data.success == false) {

                datos_faltantes += '- Verifica los campos en rojo.';
                $.each(response.data.errores, function (key, error) {
                    $('#' + key + '-help').html(error);
                });
            } else {

                if (nuevaSolicitud == 'false' && validaciones == '') {
                    getForm('registro', 'simulador');
                    $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
                } else if (nuevaSolicitud == 'true' && validaciones == '') {
                    registroNuevaSolicitud();
                }
            }
        }).catch(function (error) {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'moda',
                allowOutsideClick: false
            });
        });
    }
}

function registroProspecto() {

    var datos = $('#formSimulador,#formRegistro').serializeArray();
    datos = getFormData(datos);

    prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
    prestamoValue = prestamoValue * 1000;
    datos['monto_prestamo'] = prestamoValue;

    var validaciones = validacionesRegistro();
    $('#validacionesRegistro').html(validaciones);

    if (validaciones == '') {

        var texto = 'Registrando usuario...';
        Swal.fire({
            html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
            customClass: 'modalLoading',
            showCancelButton: false,
            showConfirmButton: false,
            allowOutsideClick: false
        });

        var clientId = 0;
        ga(function (tracker) {
            clientId = tracker.get('clientId');
        });
        datos['clientId'] = clientId;

        axios.post('/solicitud/registro', datos).then(function (response) {
            var _this = this;

            if (response.data.success == true) {

                $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
                datos['prospecto_id'] = response.data.prospecto_id;

                if (response.data.hasOwnProperty('eventTM')) {
                    var eventos = response.data.eventTM;
                    $.each(eventos, function (key, evento) {
                        dataLayer.push(evento);
                    });
                }

                if (response.data.siguiente_paso == 'sms') {
                    envia_sms(datos);
                }
            } else {

                swal.close();
                if (response.data.hasOwnProperty('errores')) {

                    $.each(response.data.errores, function (key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesRegistro').html('- Verifica los campos en rojo.');
                } else if (response.data.hasOwnProperty('siguiente_paso')) {

                    if (response.data.siguiente_paso == 'login') {
                        Swal.fire({
                            title: "Usuario Registrado",
                            text: 'Ya existe un usuario con el email que intentas registrar. Inicia sesión para continuar con tu solicitud',
                            type: 'error',
                            showConfirmButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Iniciar Sesión',
                            customClass: 'moda',
                            cancelButtonText: 'Cancelar',
                            allowOutsideClick: false
                        }).then(function (result) {
                            if (result.value) {
                                $('#loginModal').modal('show');
                            }
                        });
                    }
                } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Reintentar',
                        customClass: 'moda',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then(function (result) {
                        if (result.value) {
                            var texto = 'Registrando usuario...';
                            Swal.fire({
                                html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
                                customClass: 'modalLoading',
                                showCancelButton: false,
                                showConfirmButton: false,
                                allowOutsideClick: false
                            });
                            _this.registroProspecto(datos);
                        }
                    });
                }
            }
        }).catch(function (error) {

            $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'moda',
                allowOutsideClick: false
            });
        });
    }
}

function registroNuevaSolicitud(datos) {

    var texto = 'Registrando nueva solicitud...';
    Swal.fire({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false
    });

    var datos = $('#formSimulador').serializeArray();
    datos = getFormData(datos);

    prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
    prestamoValue = prestamoValue * 1000;
    datos['monto_prestamo'] = prestamoValue;
    var clientId = 0;
    ga(function (tracker) {
        clientId = tracker.get('clientId');
    });
    datos['clientId'] = clientId;

    axios.post('/solicitud/registro/solicitud', datos).then(function (response) {

        $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
        if (response.data.success == true) {

            var datos = [];
            datos['prospecto_id'] = response.data.prospecto_id;
            datos['celular'] = response.data.celular;
            envia_sms_solicitud(datos);
        } else {
            swal.close();
        }
    }).catch(function (error) {

        $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
        var descripcionError = '';
        if (error.hasOwnProperty('response')) {
            descripcionError = error.response.status + ': ' + error.response.responseText;
        } else {
            descripcionError = error;
        }

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: descripcionError,
            type: 'error',
            showConfirmButton: true,
            customClass: 'moda',
            allowOutsideClick: false
        });
    });
}

function getFormData(datos) {
    var unindexed_array = datos;
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function crea_usuario_ldap(datos) {
    axios.post('/solicitud/registro/ldap', datos).then(function (response) {
        var _this2 = this;

        if (response.data.success == true) {
            if (response.data.siguiente_paso == 'sms') {
                envia_sms(datos);
            }
        } else {

            if (response.data.responseLDAP.code == '999') {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: 'Hubo un problema al crear el usuario en LDAP, favor de reintentar.',
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'moda',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false
                    // onOpen: () => swal.getTitle().style.order = -1
                }).then(function (result) {
                    if (result.value) {
                        var texto = 'Registrando usuario...';
                        Swal.fire({
                            html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
                            customClass: 'modalLoading',
                            showCancelButton: false,
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                        _this2.crea_usuario_ldap(datos);
                    }
                });
            } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'moda',
                    allowOutsideClick: false
                });
            }
        }
    }).catch(function (error) {

        var descripcionError = '';
        if (error.hasOwnProperty('response')) {
            descripcionError = error.response.status + ': ' + error.response.responseText;
        } else {
            descripcionError = error;
        }

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: descripcionError,
            type: 'error',
            showConfirmButton: true,
            customClass: 'moda',
            allowOutsideClick: false
        });
    });
}

function envia_sms(datos) {

    axios.post('/solicitud/registro/sms', datos).then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.success == true) {
            getForm('verificar_codigo', 'registro');
            $('#info_prestamo_personal').show();
            $('#show_celular').html(response.data.celular);
            swal.close();
        } else {

            if (response.data.actualizar_celular == true) {
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: 'Por favor ingresa un numero',
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'moda',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        var texto = 'Enviando SMS...';
                        Swal.fire({
                            html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
                            customClass: 'modalLoading',
                            showCancelButton: false,
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                    }
                });
            } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'moda',
                    allowOutsideClick: false
                });
            }
        }
    }).catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'moda',
            allowOutsideClick: false
        });
    });
}

function reenviar_sms() {

    var texto = 'Reenviando SMS...';
    Swal.fire({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false
    });

    axios.post('/solicitud/resend_code').then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.success == true) {
            playinterval();
            Swal.fire({
                title: 'Envío exitoso...',
                text: response.data.message,
                showConfirmButton: true,
                confirmButtonText: 'Aceptar',
                customClass: 'moda',
                allowOutsideClick: false
            });
        } else {

            if (response.data.actualizar_celular == true) {
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: 'Por favor ingresa un numero',
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'moda',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        var texto = 'Enviando SMS...';
                        Swal.fire({
                            html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
                            customClass: 'modalLoading',
                            showCancelButton: false,
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                    }
                });
            } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'moda',
                    allowOutsideClick: false
                });
            }
        }
    }).catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'moda',
            allowOutsideClick: false
        });
    });
}

function envia_sms_solicitud(datos) {

    axios.post('/solicitud/registro/sms', {
        celular: datos['celular'],
        prospecto_id: datos['prospecto_id']
    }).then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.success == true) {
            getForm('verificar_codigo', 'simulador');
            $('#info_prestamo_personal').show();
            swal.close();
        } else {

            if (response.data.actualizar_celular == true) {
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: 'Por favor ingresa un numero',
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'modalError',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        var texto = 'Enviando SMS...';
                        Swal.fire({
                            html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
                            customClass: 'modalLoading',
                            showCancelButton: false,
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                    }
                });
            } else {

                $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
                });
            }
        }
    }).catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
        });
    });
}

function valida_sms() {
    var datos = $('#formVerificarSMS').serializeArray();
    datos = getFormData(datos);

    var texto = 'Validando SMS...';
    Swal.fire({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false
    });

    axios.post('/solicitud/conf_sms_code', datos).then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.success == true) {
            getForm(response.data.siguiente_paso.formulario, 'verificar_codigo');
            swal.close();
        } else {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: response.data.message,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
            });
        }
    }).catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
        });
    });
}

function datos_domicilio() {

    var validaciones = validacionesDomicilio();
    $('#validacionesDomicilio').html(validaciones);

    if (validaciones == '') {

        var disabledSelect = $('#formDatosDomicilio').find('option:disabled').removeAttr('disabled');
        var disabled = $('#formDatosDomicilio').find('input:disabled').removeAttr('disabled');

        var datos = $('#formDatosDomicilio').serializeArray();
        datos = getFormData(datos);
        disabledSelect.attr('disabled', 'disabled');
        disabled.attr('disabled', 'disabled');

        var texto = 'Guardando datos...';
        swal({
            html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
            customClass: 'modalLoading',
            showCancelButton: false,
            showConfirmButton: false,
            allowOutsideClick: false
        });

        axios.post('/solicitud/reg_domicilio', datos).then(function (response) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            if (response.data.success == true) {
                getForm('datos_personales', 'datos_domicilio');
                swal.close();
            } else {

                if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    $.each(response.data.errores, function (key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesDomicilio').html('- Verifica los campos en rojo.');
                } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                    });
                }
            }
        }).catch(function (error) {

            $("html, body").animate({ scrollTop: $("info_prestamo_personal").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
            });
        });
    }
}

function datos_personales() {

    var validaciones = validacionesDatosPersonales();
    $('#validacionesDatosPersonales').html(validaciones);

    if (validaciones == '') {

        var disabled = $('#formDatosPersonales').find('option:disabled').removeAttr('disabled');
        var datos = $('#formDatosPersonales').serializeArray();
        datos = getFormData(datos);
        disabled.attr('disabled', 'disabled');

        var texto = 'Guardando datos...';
        swal({
            html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
            customClass: 'modalLoading',
            showCancelButton: false,
            showConfirmButton: false,
            allowOutsideClick: false
        });

        axios.post('/solicitud/datos_personales', datos).then(function (response) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            if (response.data.success == true) {
                getForm('datos_buro', 'datos_personales');
                swal.close();
            } else {

                if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    $.each(response.data.errores, function (key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesDatosPersonales').html('- Verifica los campos en rojo.');
                } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                    });
                }
            }
        }).catch(function (error) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
            });
        });
    }
}

function datos_buro() {

    var clientId = 0;
    ga(function (tracker) {
        clientId = tracker.get('clientId');
    });

    var validaciones = validacionesDatosBuro();
    $('#validacionesDatosBuro').html(validaciones);

    if (validaciones == '') {

        var datos = $('#formDatosBuro').serializeArray();
        datos = getFormData(datos);
        datos['clientId'] = clientId;

        var texto = 'Guardando datos...';
        swal({
            html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
            customClass: 'modalLoading',
            showCancelButton: false,
            showConfirmButton: false,
            allowOutsideClick: false
        });

        axios.post('/solicitud/cuentas_de_credito', datos).then(function (response) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            if (response.data.success == true) {

                if (response.data.hasOwnProperty('eventTM')) {
                    dataLayer.push(response.data.eventTM);
                }

                if (response.data.siguiente_paso == 'primera_llamada_bc') {

                    var texto = 'Realizando consulta a Buró de Crédito...';
                    swal({
                        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
                        customClass: 'modalLoading',
                        showCancelButton: false,
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                    primeraLlamadaBC(response.data.prospecto_id, response.data.solicitud_id);
                }
            } else {

                $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
                });
            }
        }).catch(function (error) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
            });
        });
    }
}

function BCCorreccion(datos) {

    var texto = 'Actualizando datos...';
    swal({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false
    });

    axios.post('/solicitud/correccionBC', datos).then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);

        if (response.data.success == true) {

            if (response.data.siguiente_paso == 'primera_llamada_bc') {

                var texto = 'Realizando consulta a Buró de Crédito...';
                swal({
                    html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
                    customClass: 'modalLoading',
                    showCancelButton: false,
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
                primeraLlamadaBC(response.data.prospecto_id, response.data.solicitud_id);
            }
        } else {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: response.data.message,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
            });
        }
    }).catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
        });
    });
}

function datos_ingreso() {

    var clientId = 0;
    ga(function (tracker) {
        clientId = tracker.get('clientId');
    });

    var validaciones = validacionesDatosIngreso();
    $('#validacionesDatosIngreso').html(validaciones);

    if (validaciones == '') {

        var texto = 'Guardando datos...';
        swal({
            html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
            customClass: 'modalLoading',
            showCancelButton: false,
            showConfirmButton: false,
            allowOutsideClick: false
        });

        var disabled = $('#formDatosIngreso').find('option:disabled').removeAttr('disabled');
        var datos = $('#formDatosIngreso').serializeArray();
        datos = getFormData(datos);
        disabled.attr('disabled', 'disabled');

        ingreso = $("#ingreso_mensual").maskMoney('unmasked')[0];
        ingreso = ingreso * 1000;
        datos['ingreso_mensual'] = ingreso;

        gastoMensual = $("#gastos_mensuales").maskMoney('unmasked')[0];
        gastoMensual = gastoMensual * 1000;
        datos['gastos_mensuales'] = gastoMensual;
        datos['clientId'] = clientId;

        axios.post('/solicitud/datos_adicionales', datos).then(function (response) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);

            if (response.data.hasOwnProperty('eventTM')) {
                dataLayer.push(response.data.eventTM);
            }
            if (response.data.siguiente_paso == 'datos_empleo') {
                getForm('datos_empleo', 'datos_ingreso');
                swal.close();
            } else if (response.data.siguiente_paso == 'alp') {
                sendALPRequest(response.data.prospecto_id, response.data.solicitud_id, 'datos_ingreso');
            } else if (response.data.siguiente_paso == 'modal') {
                Swal.fire({
                    title: response.data.stat,
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Aceptar',
                    customClass: 'modalError',
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        location.href = '/';
                    }
                });
            }
        }).catch(function (error) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
            });
        });
    }
}

function datos_empleo() {

    var validaciones = validacionesDatosEmpleo();
    $('#validacionesDatosEmpleo').html(validaciones);

    var clientId = 0;
    ga(function (tracker) {
        clientId = tracker.get('clientId');
    });

    if (validaciones == '') {

        var texto = 'Guardando datos...';
        swal({
            html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
            customClass: 'modalLoading',
            showCancelButton: false,
            showConfirmButton: false,
            allowOutsideClick: false
        });

        var disabledSelect = $('#formDatosEmpleo').find('option:disabled').removeAttr('disabled');
        var disabled = $('#formDatosEmpleo').find('input:disabled').removeAttr('disabled');
        var datos = $('#formDatosEmpleo').serializeArray();
        datos = getFormData(datos);
        disabled.attr('disabled', 'disabled');
        disabledSelect.attr('disabled', 'disabled');
        datos['clientId'] = clientId;

        axios.post('/solicitud/datos_empleo', datos).then(function (response) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            if (response.data.success == true) {

                if (response.data.siguiente_paso == 'alp') {
                    sendALPRequest(response.data.prospecto_id, response.data.solicitud_id, 'datos_empleo');
                }

                if (response.data.siguiente_paso == 'segunda_llamada') {
                    segundaLlamadaBC(response.data.prospecto_id, response.data.solicitud_id, true);
                }
            } else {

                if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    $.each(response.data.errores, function (key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesDatosEmpleo').html('- Verifica los campos en rojo.');
                } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                    });
                }
            }
        }).catch(function (error) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
            });
        });
    }
}

function getCP(cp) {
    var empleo = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    var idEmpleo = '';
    if (empleo == true) {
        idEmpleo = '_empleo';
    }
    if (cp.value.length == 5) {
        $.ajax({
            method: "GET",
            url: "/api/geo/" + cp.value
        }).done(function (response) {
            console.log(response);
            $("#colonia" + idEmpleo).val('');
            $("#colonia" + idEmpleo).attr("placeholder", "Ingrese Código Postal");
            $("#colonia_id .trigger-scroll").html('<label style="color: #b7b7b7;">Ingrese Código Postal</label>');
            $("#delegacion" + idEmpleo).val('');
            $("#delegacion" + idEmpleo).attr("placeholder", "Ingrese Código Postal");
            $("#ciudad" + idEmpleo).val('');
            $("#ciudad" + idEmpleo).attr("placeholder", "Ingrese Código Postal");
            $("#estado" + idEmpleo).val('');
            $("#estado" + idEmpleo).attr("placeholder", "Ingrese Código Postal");
            $('#estado_id .trigger-scroll').html('<label style="color: #b7b7b7;">Ingrese Código Postal</label>');

            if (response.success == true) {

                $('#select_colonia' + idEmpleo).empty();
                $('#select_colonia' + idEmpleo).append($('<option>', {
                    value: '',
                    text: 'Seleccionar Colonia',
                    disabled: true,
                    selected: true
                }));

                for (var i = 0; i < response.colonias.length; i++) {

                    $('#select_colonia' + idEmpleo).append($('<option>', {
                        value: response.colonias[i].id,
                        text: response.colonias[i].colonia
                    }));
                }

                $("#id_colonia" + idEmpleo).val(response.id_colonia);
                $("#delegacion" + idEmpleo).val(response.deleg_munic);
                $("#id_deleg_munic" + idEmpleo).val(response.id_deleg_munic);
                $("#ciudad" + idEmpleo).val(response.ciudad);
                $("#id_ciudad" + idEmpleo).val(response.id_ciudad);
                if (response.ciudad == '') {
                    $("#ciudad" + idEmpleo).attr("placeholder", "NO APLICA");
                }
                $("#estado" + idEmpleo).val(response.estado);
                $("#id_estado" + idEmpleo).val(response.id_estado);
                $("#codigo_estado" + idEmpleo).val(response.codigo_estado);
                if (empleo == false) {
                    $("#cobertura").val(response.cobertura);
                }
            } else if (response.response == "error") {

                $("#delegacion" + idEmpleo).attr("readonly", !0);
                $("#ciudad" + idEmpleo).attr("readonly", !0);
                $("#colonia" + idEmpleo).html("Ingrese Código Postal");
                $("#delegacion" + idEmpleo).attr("placeholder", "Ingrese Código Postal");
                $("#ciudad" + idEmpleo).attr("placeholder", "Ingrese Código Postal");
                $("#estado" + idEmpleo).html("Ingrese Código Postal");
                alert(response.message);
            } else if (response.response == "Not Found") {

                $("#cp-error" + idEmpleo).html("C.P. no encontrado").slideDown("slow");
                $("#delegacion" + idEmpleo).val("");
                $("#delegacion" + idEmpleo).attr("placeholder", "Ingrese Código Postal Válido");
                $("#ciudad" + idEmpleo).val("");
                $("#ciudad" + idEmpleo).attr("placeholder", "Ingrese Código Postal Válido");
                $("#estado" + idEmpleo).val("");
                $("#estado" + idEmpleo).attr("placeholder", "Ingrese Código Postal Válido");
                $('#select_colonia' + idEmpleo).empty();
                $('#select_colonia' + idEmpleo).append($('<option>', {
                    value: 0,
                    text: 'Ingrese Código Postal Válido'
                }));
            }
        }).fail(function () {});
    } else {

        $("#colonia" + idEmpleo).val('');
        $("#delegacion" + idEmpleo).val('');
        $("#delegacion" + idEmpleo).attr("placeholder", "Ingrese Código Postal");
        $("#ciudad" + idEmpleo).val('');
        $("#ciudad" + idEmpleo).attr("placeholder", "Ingrese Código Postal");
        $("#estado" + idEmpleo).val('');
        $("#estado" + idEmpleo).attr("placeholder", "Ingrese Código Postal");
    }
}

$("#select_colonia").on("change", function (e) {
    var colonia = $(this).find('option:selected').text();
    $('#colonia').val(colonia);
});

$("#select_colonia_empleo").on("change", function (e) {
    var colonia = $(this).find('option:selected').text();
    $('#colonia_empleo').val(colonia);
});

function cambioEstadoNacimiento() {

    var estado = $('#estado_nacimiento').val();
    if (estado != "0") {
        var onFail = function onFail(xhr, textStatus, errorThrown) {

            if (retries > 0) {
                retries = retries - 1;
                $.ajax(ajaxSettings).done(function (response) {
                    onDone(response);
                }).fail(function (xhr, textStatus, errorThrown) {
                    onFail(xhr, textStatus, errorThrown);
                });
            } else {
                swal({
                    text: "Ooops.. Surgió un problema al buscar las Ciudades, por favor selecciona el Estado en el campo Lugar de Nacimiento de nuevo",
                    showConfirmButton: true
                });
                $('#estado_nacimiento').val("0").change();
            }
        };

        var onDone = function onDone(response) {

            if (response.success == true) {
                $('#ciudad_nacimiento').empty();
                $('#ciudad_nacimiento').append($('<option>', {
                    value: '',
                    text: 'Ciudad de nacimiento*',
                    disabled: true,
                    selected: true
                }));
                for (var i = 0; i < response.count; i++) {

                    $('#ciudad_nacimiento').append($('<option>', {
                        value: response.municipios[i],
                        text: response.municipios[i]
                    }));
                }
            } else {
                $('#ciudad_nacimiento').empty();
                $('#ciudad_nacimiento').append($('<option>', {
                    value: '',
                    text: 'Ciudad de nacimiento*',
                    disabled: true,
                    selected: true
                }));
            }
        };

        var retries = 3;
        var ajaxSettings = {
            cache: false,
            method: "GET",
            url: "/api/ciudades/" + estado,
            dataType: "json",
            timeout: 6000,
            beforeSend: function beforeSend() {}
        };

        $.ajax(ajaxSettings).done(function (response) {
            onDone(response);
        }).fail(function (xhr, textStatus, errorThrown) {
            onFail(xhr, textStatus, errorThrown);
        });
    }
}

function primeraLlamadaBC(prospecto_id, solicitud_id) {
    var clientId = 0;
    ga(function (tracker) {
        clientId = tracker.get('clientId');
    });

    axios.post('/solicitud_bc_score', {
        prospecto_id: prospecto_id,
        solicitud_id: solicitud_id,
        clientId: clientId
    }).then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.hasOwnProperty('eventTM')) {
            var eventos = response.data.eventTM;
            $.each(eventos, function (key, evento) {
                dataLayer.push(evento);
            });
        }

        if (response.data.success == true) {

            if (response.data.stat == "Califica BC Score") {
                if (response.data.lead == 'askrobin') {
                    sendPixelAskRobin('3', response.data.lead_id, prospecto_id);
                }
                segundaLlamadaBC(prospecto_id, solicitud_id);
            } else if (response.data.stat == "Datos Elegible") {
                getForm('datos_elegible', 'datos_buro');
                swal.close();
            } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
                });
            }
        } else {

            if (response.data.hasOwnProperty('maximosReintentos')) {
                Swal.fire({
                    title: 'Falta información de buró',
                    html: response.data.message,
                    showConfirmButton: true,
                    confirmButtonText: 'Aceptar',
                    customClass: 'modalError',
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        location.href = '/';
                    }
                });
            }

            if (response.data.stat == 'Error BC No Response') {
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Reintentar',
                    cancelButtonText: 'Cancelar',
                    customClass: 'modalError',
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        var texto = 'Realizando consulta a Buró de Crédito...';
                        Swal.fire({
                            html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
                            customClass: 'modalLoading',
                            showCancelButton: false,
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                        primeraLlamadaBC(prospecto_id, solicitud_id);
                    }
                });
            }

            if (response.data.stat == 'No Autenticado') {
                Swal.fire({
                    title: response.data.stat,
                    html: 'No pudimos encontrar tu información en Buró de Crédito. <br><br> Revisa la ayuda que aparece en cada una de las preguntas y verifica que la informacíon proporcionada sea la correcta',
                    type: 'error',
                    showConfirmButton: true,
                    confirmButtonText: 'Aceptar',
                    customClass: 'modalError',
                    allowOutsideClick: false
                });
            }

            if (response.data.stat == 'Error en Captura') {
                Swal.fire({
                    title: 'La consulta a Buró de Crédito arrojo un error',
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'modalError',
                    allowOutsideClick: false,
                    onBeforeOpen: function onBeforeOpen() {
                        $('#fecha_nacimientoBC').combodate({
                            maxYear: moment().get('year') - response.data.edad_minima,
                            minYear: moment().get('year') - response.data.edad_maxima,
                            firstItem: 'name',
                            smartDays: true
                        });
                        $('.combodate').css({ "width": "100%" });

                        $("#formCorreccion #year").on("change", function (e) {
                            var valor = $(this).find('option:selected').text();
                            $('#formCorreccion #year').val(valor);
                            $('.combodate').removeClass('error');
                            $('#fecha_nacimiento-help').html('');
                        });

                        $("#formCorreccion #month").on("change", function (e) {
                            var valor = $(this).find('option:selected').val();
                            $('#formCorreccion #month').val(valor);
                            $('.combodate').removeClass('error');
                            $('#fecha_nacimiento-help').html('');
                        });

                        $("#formCorreccion #day").on("change", function (e) {
                            var valor = $(this).find('option:selected').text();
                            valor = parseInt(valor);
                            $('#formCorreccion #day').val(valor);
                            $('.combodate').removeClass('error');
                            $('#fecha_nacimiento-help').html('');
                        });
                    },
                    preConfirm: function preConfirm(e) {
                        var validacion = validacionesBCCorreccion();
                        if (validacion == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }).then(function (result) {
                    if (result.value) {
                        var datos = $('#formCorreccion').serializeArray();
                        datos = getFormData(datos);
                        BCCorreccion(datos);
                    }
                });
            }

            if (response.data.stat == 'No Califica') {
                Swal.fire({
                    title: response.data.stat,
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Aceptar',
                    customClass: 'modalError',
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        location.href = '/';
                    }
                });
            }

            if (response.data.stat == 'No Encontrado') {
                Swal.fire({
                    title: 'Falta información de buró',
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Aceptar',
                    customClass: 'modalError',
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        location.href = '/';
                    }
                });
            }
        }
    }).catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        var descripcionError = '';
        if (error.hasOwnProperty('response')) {
            descripcionError = error.response.status + ': ' + error.response.responseText;
        } else {
            descripcionError = error;
        }

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: descripcionError,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
        });
    });
}

function segundaLlamadaBC(prospecto_id, solicitud_id) {
    var elegible = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    axios.post('/solicitud_hawk', {
        prospecto_id: prospecto_id,
        solicitud_id: solicitud_id,
        elegible: elegible
    }).then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (response.data.stat == "Segunda Llamada BC OK") {
            getForm('datos_ingreso', 'datos_buro');
            swal.close();
        } else if (response.data.stat == "Segunda Llamada BC OK Elegible") {
            sendALPRequest(response.data.prospecto_id, response.data.solicitud_id, 'datos_empleo');
        } else if (response.data.stat == "No Encontrado") {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: response.data.message,
                type: 'error',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Reintentar',
                cancelButtonText: 'Cancelar',
                customClass: 'modalError',
                allowOutsideClick: false
            }).then(function (result) {
                if (result.value) {
                    var texto = 'Realizando consulta a Buró de Crédito...';
                    Swal.fire({
                        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
                        customClass: 'modalLoading',
                        showCancelButton: false,
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                    segundaLlamadaBC(prospecto_id, solicitud_id);
                }
            });
        } else if (response.data.stat == "Error BC") {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: response.data.message,
                type: 'error',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Reintentar',
                cancelButtonText: 'Cancelar',
                customClass: 'modalError',
                allowOutsideClick: false
            }).then(function (result) {
                if (result.value) {
                    var texto = 'Realizando consulta a Buró de Crédito...';
                    Swal.fire({
                        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
                        customClass: 'modalLoading',
                        showCancelButton: false,
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                    segundaLlamadaBC(prospecto_id, solicitud_id);
                }
            });
        }
    }).catch(function (error) {

        var descripcionError = '';
        if (error.hasOwnProperty('response')) {
            descripcionError = error.response.status + ': ' + error.response.responseText;
        } else {
            descripcionError = error;
        }

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: descripcionError,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
        });
    });
}

function sendALPRequest(prospecto_id, solicitud_id, ocultar) {

    var clientId = 0;
    ga(function (tracker) {
        clientId = tracker.get('clientId');
    });

    var texto = 'Estamos procesando tu solicitud...';
    Swal.fire({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false
    });

    axios.post('/solicitud_fico_alp', {
        prospecto_id: prospecto_id,
        solicitud_id: solicitud_id,
        clientId: clientId
    }).then(function (response) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        var respuesta = response.data;
        if (respuesta.success == true) {

            if (response.data.hasOwnProperty('eventTM')) {
                var eventos = response.data.eventTM;
                $.each(eventos, function (key, evento) {
                    dataLayer.push(evento);
                });
            }

            var tipoPoblacion = respuesta.tipo_poblacion;
            var pantallasExtra = 0;

            $('#encabezadoModal').html(respuesta.plantilla.modal_encabezado);
            $('#imgModal').html(respuesta.plantilla.modal_img);
            $('#cuerpoModal').html(respuesta.plantilla.modal_cuerpo);
            var decision = respuesta.decision;

            if (tipoPoblacion == 'oferta_normal' && decision == 3) {
                pantallasExtra = respuesta.resultado.oferta_normal.PantallaExtra;
            } else if (tipoPoblacion == 'oferta_incrementada' && decision == 3) {
                pantallasExtra = respuesta.resultado.oferta_mayor.PantallaExtra;
            } else if (tipoPoblacion == 'oferta_doble' && decision == 3) {
                pantallasExtra = respuesta.resultado.oferta_mayor.PantallaExtra;
            }

            // Si la respuesta a la decision del stored procedure es 3 (Invitación
            // a continuar) y no hay pantallas extras se debe mostrar el modal
            // de oferta
            if (pantallasExtra == 0 && decision == 3) {

                Swal.fire({
                    html: response.data.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modaloferta'
                });
            } else if (pantallasExtra == 1 && decision == 3) {
                // Si la respuesta a la decisión del stored procedure es 3 (Invitación
                // a continuar) y hay pantallas extras se debe mostrar el cuestionario
                // fijo

                // Creando el cuestionario dinámico
                var cuestionarios = respuesta.cuestionario;
                $('#cuestionarioDinamico').append("<form action='#' id='formCuestionarioDinamico'>");
                var formulario = "";
                var situaciones = "";
                formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'><h2 class='secondary-title txt-center'>Falta un paso más</h2>";
                formulario = formulario + "<div class='calculadoras-productos'><small>Ayúdanos con estos últimos datos.</small></div></div>";
                $.each(cuestionarios, function (key, cuestionario) {
                    formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'>";
                    formulario = formulario + "<div class='col-12' style='margin-bottom: 10px; margin-top: 10px;'>";
                    if (cuestionario.introduccion != null) {
                        formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4><label class=''>" + cuestionario.introduccion + "</label></div>";
                    } else {
                        formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4></div>";
                    }

                    $.each(cuestionario.cuestionario, function (index, pregunta) {

                        oculto = '';
                        change = '';
                        largo = 'col-12';
                        width = '100%';
                        if (pregunta.oculto == 1) {
                            oculto = 'display:none';
                        }
                        if (pregunta.grid_estilo != null) {
                            largo = pregunta.grid_estilo;
                        }
                        if (pregunta.ancho_estilo != null) {
                            width = pregunta.ancho_estilo;
                        }
                        id = cuestionario.situacion + '_' + pregunta.id;

                        formulario = formulario + "<div class='" + largo + " ' id='div_" + id + "' style='margin-bottom: 5px; margin-bottom: 5px;padding-left: 0px;padding-right: 0px;" + oculto + "'>";
                        situaciones = situaciones + cuestionario.situacion + '|';
                        if (pregunta.depende != '') {
                            var valores = "'" + pregunta.depende + "','" + pregunta.respuesta_depende + "'";
                            change = 'onChange = "campoVisible(this.value,' + valores + ')"';
                        }

                        if (pregunta.tipo == 'text') {
                            formulario = formulario + '<label id="label_' + id + '" class="labelDinamico">' + pregunta.pregunta + '</label>';
                            var style = 'text-transform: uppercase; text-align: center; width: ' + width;
                            formulario = formulario + '<div class="calculadora-personal"><input type="text" id="' + id + '" name="' + id + '" style="' + style + '"/></div>';
                        }

                        if (pregunta.tipo == 'textarea') {
                            formulario = formulario + '<label id="label_' + id + '" class="labelDinamicoT">' + pregunta.pregunta + '</label>';
                            var style = 'text-transform: uppercase; float: none important!; resize: none;';
                            formulario = formulario + '<div class="calculadora-personal"><textarea id="' + id + '" name="' + id + '" maxlength="255" rows="2" style="' + style + '"></textarea></div>';
                        }

                        if (pregunta.tipo == 'select') {
                            formulario = formulario + '<label id="label_' + id + '" class="labelDinamico">' + pregunta.pregunta + '</label>';
                            formulario = formulario + '<div class="calculadoras-productos"><div class="withDecoration"><span><select id="' + id + '" name="' + id + '" ' + change + ' class="dinamico pre-registro-input">';
                            formulario = formulario + '<option value="" selected>SELECCIONA</option>';
                            opciones = pregunta.opciones.split('|');
                            var opcionesLista = '';
                            $.each(opciones, function (indexO, opcion) {
                                opcionesLista = opcionesLista + '<option value="' + opcion + '">' + opcion + '</option>';
                            });
                            formulario = formulario + opcionesLista + '</select></span><div class="decoration"><i class="fas fa-chevron-down"></i></div></div></div>';
                        }
                        formulario = formulario + '<label id="lerror_' + id + '" class="help"></label>';
                        formulario = formulario + "</div>";
                    });

                    $('#cuestionarioDinamico form').append(formulario);
                    formulario = "";
                });

                $('#cuestionarioDinamico form').append('<div class="row col-lg-10 col-lg-offset-1 col-xs-12 mt-0"><div class="text-right"><button type="button" class="general-button" onclick="guardarCuestionarioDinamico()">Continuar</button></div></div>');
                $('#cuestionarioDinamico form').append('<input type="hidden" id="situaciones" name="situaciones"/>');
                $('#cuestionarioDinamico form').append('<input type="hidden" id="tipo_poblacion" name="tipo_poblacion" value="' + response.data.tipo_poblacion + '"/>');
                $('#situaciones').val(situaciones);

                $('#' + ocultar).hide();
                $('#cuestionarioDinamico').show();
                $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
                swal.close();
            } else if (decision == 2 || decision == 1) {
                // Si la respuesta a la decisión del stored procedure es 2 (Oferta
                // diferida) ó 1 (Rechazado) solo se debe mostrar el modal del status
                // de la solicitud
                Swal.fire({
                    html: respuesta.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modalstatus'
                });
            }
        } else {}
    }).catch(function (error) {
        swal.close();
    });
}

function getForm() {
    var siguientePaso = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var anterior = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;


    axios.get('/getForm', {
        params: {
            paso: siguientePaso
        }
    }).then(function (response) {
        $('#loading-form').hide();
        $('#' + response.data.formulario).show();

        if (anterior != null) {
            $('#' + anterior).hide();
        } else if (response.data.oculta != null && response.data.nueva_solicitud == false) {
            $('#info_prestamo_personal').show();
            $('#' + response.data.oculta).hide();
        }

        if (response.data.formulario == 'verificar_codigo') {
            $('#cuestionarioDinamico').hide();
            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            playinterval();
        }

        if (response.data.nueva_solicitud == true) {
            $('#nueva_solicitud').val('true');
        }

        if (response.data.hasOwnProperty('ultimo_status')) {
            if (response.data.ultimo_status == 'Registro') {
                Swal.fire({
                    title: "Aviso Importante",
                    html: response.data.modal_confianza,
                    showConfirmButton: true,
                    customClass: 'modalConfianza',
                    allowOutsideClick: false
                });
            }
        }

        if (response.data.hasOwnProperty('sesionIniciada')) {
            $('#linkNoSesion').hide();
            $('#linkSesion').show();
            $('#nombreProspecto').html(response.data.nombre_prospecto);
        }

        if (response.data.redirect != null) {
            location.href = response.data.redirect;
        } else if (response.data.cuestionario != null) {

            $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
            var cuestionarios = response.data.cuestionario;

            $('#cuestionarioDinamico').append("<form action='#' id='formCuestionarioDinamico'>");
            var formulario = "";
            var situaciones = "";
            formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'><h2 class='secondary-title txt-center'>Falta un paso más</h2>";
            formulario = formulario + "<div class='calculadoras-productos'><small>Ayúdanos con estos últimos datos.</small></div></div>";
            $.each(cuestionarios, function (key, cuestionario) {
                formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'>";
                formulario = formulario + "<div class='col-12' style='margin-bottom: 10px; margin-top: 10px;'>";
                if (cuestionario.introduccion != null) {
                    formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4><label class=''>" + cuestionario.introduccion + "</label></div>";
                } else {
                    formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4></div>";
                }

                $.each(cuestionario.cuestionario, function (index, pregunta) {

                    oculto = '';
                    change = '';
                    largo = 'col-12';
                    width = '100%';
                    if (pregunta.oculto == 1) {
                        oculto = 'display:none';
                    }
                    if (pregunta.grid_estilo != null) {
                        largo = pregunta.grid_estilo;
                    }
                    if (pregunta.ancho_estilo != null) {
                        width = pregunta.ancho_estilo;
                    }
                    id = cuestionario.situacion + '_' + pregunta.id;

                    formulario = formulario + "<div class='" + largo + " ' id='div_" + id + "' style='margin-bottom: 5px; margin-bottom: 5px;padding-left: 0px;padding-right: 0px;" + oculto + "'>";
                    situaciones = situaciones + cuestionario.situacion + '|';
                    if (pregunta.depende != '') {
                        var valores = "'" + pregunta.depende + "','" + pregunta.respuesta_depende + "'";
                        change = 'onChange = "campoVisible(this.value,' + valores + ')"';
                    }

                    if (pregunta.tipo == 'text') {
                        formulario = formulario + '<label id="label_' + id + '" class="labelDinamico">' + pregunta.pregunta + '</label>';
                        var style = 'text-transform: uppercase; text-align: center; width: ' + width;
                        formulario = formulario + '<div class="calculadora-personal"><input type="text" id="' + id + '" name="' + id + '" style="' + style + '"/></div>';
                    }

                    if (pregunta.tipo == 'textarea') {
                        formulario = formulario + '<label id="label_' + id + '" class="labelDinamicoT">' + pregunta.pregunta + '</label>';
                        var style = 'text-transform: uppercase; float: none important!; resize: none;';
                        formulario = formulario + '<div class="calculadora-personal"><textarea id="' + id + '" name="' + id + '" maxlength="255" rows="2" style="' + style + '"></textarea></div>';
                    }

                    if (pregunta.tipo == 'select') {
                        formulario = formulario + '<label id="label_' + id + '" class="labelDinamico">' + pregunta.pregunta + '</label>';
                        formulario = formulario + '<div class="calculadoras-productos"><div class="withDecoration"><span><select id="' + id + '" name="' + id + '" ' + change + ' class="dinamico pre-registro-input">';
                        formulario = formulario + '<option value="" selected>SELECCIONA</option>';
                        opciones = pregunta.opciones.split('|');
                        var opcionesLista = '';
                        $.each(opciones, function (indexO, opcion) {
                            opcionesLista = opcionesLista + '<option value="' + opcion + '">' + opcion + '</option>';
                        });
                        formulario = formulario + opcionesLista + '</select></span><div class="decoration"><i class="fas fa-chevron-down"></i></div></div></div>';
                    }
                    formulario = formulario + '<label id="lerror_' + id + '" class="help"></label>';
                    formulario = formulario + "</div>";
                });

                $('#cuestionarioDinamico form').append(formulario);
                formulario = "";
            });

            $('#cuestionarioDinamico form').append('<div class="row col-lg-10 col-lg-offset-1 col-xs-12 mt-0"><div class="text-right"><button type="button" class="general-button" onclick="guardarCuestionarioDinamico()">Continuar</button></div></div>');
            $('#cuestionarioDinamico form').append('<input type="hidden" id="situaciones" name="situaciones"/>');
            $('#cuestionarioDinamico form').append('<input type="hidden" id="tipo_poblacion" name="tipo_poblacion" value="' + response.data.tipo_poblacion + '"/>');
            $('#situaciones').val(situaciones);
        } else if (response.data.mostrar_oferta == true) {

            Swal.fire({
                html: response.data.modal,
                showConfirmButton: false,
                allowOutsideClick: false,
                customClass: 'modaloferta'
            });
        }
    }).catch(function (error) {
        console.log(error);
    });
}

function validacionesSimulador(datos) {

    var monto_minimo = parseInt($('#monto_minimo').val());
    var monto_maximo = parseInt($('#monto_maximo').val());
    var datos_faltantes = '';
    $(".error").removeClass("error");

    if ($("#plazo").val() == null) {
        datos_faltantes += '- Selecciona un Plazo. <br>';
        $("#plazo").addClass('error');
    }

    if ($("#finalidad").val() == null) {
        datos_faltantes += '- Selecciona la Finalidad de tu préstamo. <br>';
        $("#finalidad").addClass('error');
    }

    prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
    prestamoValue = prestamoValue * 1000;
    if (prestamoValue == 0 || parseInt(prestamoValue) < monto_minimo || parseInt(prestamoValue) > monto_maximo) {
        datos_faltantes += '- El Monto del préstamo debe ser entre ' + formatoMoneda(monto_minimo) + ' y ' + formatoMoneda(monto_maximo) + '. <br>';
        $("#monto_prestamo").addClass('error');
    }

    if ($("#finalidad_custom").val().length == 0 || $("#finalidad_custom").val().length < 20) {
        datos_faltantes += '- El campo Nos gustaría conocerte debe contener mínimo 20 caracteres y máximo 200. <br>';
        $("#finalidad_custom").addClass('error');
    }

    return datos_faltantes;
}

function validacionesRegistro() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formRegistro .required').each(function () {
        if (this.value == '') {
            campo = this.placeholder;
            $('#' + this.id).addClass('error');
            switch (this.name) {
                case 'celular':
                    campo = 'Telefono celular';
                    break;
                case 'contraseña':
                    campo = 'Contraseña';
                    break;
                case 'confirmacion_contraseña':
                    campo = 'Confirma tu contraseña';
                    break;

            }
            datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
        }

        if (this.name == 'email' && this.value != '') {

            var email = IsEmail(this.value);
            if (email == false) {
                $('#' + this.id).addClass('error');
                $('#' + this.id + '-help').html('Ingresa un correo válido');
                check_especial = true;
            }
        }

        if (this.name == 'celular' && this.value != '' && this.value.length < 10) {
            $('#' + this.id).addClass('error');
            $('#' + this.id + '-help').html('El teléfono celular debe ser de 10 dígitos');
            check_especial = true;
        }

        if (this.name == 'confirmacion_contraseña' && this.value != '' && $('#contraseña').val() != '') {
            if (this.value != $('#contraseña').val()) {
                $('#' + this.id).addClass('error');
                $('#contraseña').addClass('error');
                $('#' + this.id + '-help').html('El campo contraseña y confirmación de contraseña no son iguales');
                $('#contraseña-help').html('El campo contraseña y confirmación de contraseña no son iguales');
                check_especial = true;
            }
        }

        if ((this.name == 'nombres' || this.name == 'apellido_paterno') && this.value != '' && this.value.length < 2) {
            $('#' + this.id).addClass('error');
            $('#' + this.id + '-help').html('El campo debe tener al menos 2 caracteres');
            check_especial = true;
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;
}

function validacionesDomicilio() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosDomicilio .required').each(function () {
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;
            $('#' + this.id).addClass('error');
            switch (this.name) {
                case 'select_colonia':
                    campo = 'Colonia';
                    break;

            }
            if (this.name == 'select_colonia') {
                if ($('#codigo_postal').val() == '') {
                    datos_faltantes += ' - Ingresa un C.P. para selecionar una ' + campo + '. <br>';
                } else {
                    datos_faltantes += ' - Selecciona una ' + campo + '. <br>';
                }
            } else {
                datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
            }
        }

        if (this.name == 'codigo_postal' && this.value != '' && this.value.length < 5) {
            $('#' + this.id).addClass('error');
            $('#' + this.id + '-help').html('El código postal de tener 5 números.');
            check_especial = true;
        }

        if (this.name == 'calle' && this.value != '' && this.value.length < 2) {
            $('#' + this.id).addClass('error');
            $('#' + this.id + '-help').html('La calle debe tener al menos 2 caracteres.');
            check_especial = true;
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;
}

function validacionesDatosPersonales() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosPersonales .required').each(function () {
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;

            if (this.name == 'fecha_nacimiento') {
                $('.combodate').addClass('error');
            } else {
                $('#' + this.id).addClass('error');
            }

            switch (this.name) {
                case 'sexo':
                    campo = 'Sexo';
                    break;
                case 'estado_nacimiento':
                    campo = 'Estado de nacimiento';
                    break;
                case 'ciudad_nacimiento':
                    campo = 'Ciudad de nacimiento';
                    break;
                case 'telefono_casa':
                    campo = 'Telefono de casa';
                    break;
                case 'estado_civil':
                    campo = 'Estado civil';
                    break;
                case 'nivel_estudios':
                    campo = 'Nivel de estudios';
                    break;

            }

            if (this.name == 'fecha_nacimiento') {

                if ($('#day').find('option:selected').val() == '') {
                    datos_faltantes += ' - Selecciona un dia de nacimiento. <br>';
                }
                if ($('#month').find('option:selected').val() == '') {
                    datos_faltantes += ' - Selecciona un mes de nacimiento. <br>';
                }
                if ($('#year').find('option:selected').val() == '') {
                    datos_faltantes += ' - Selecciona un año de nacimiento. <br>';
                }
            } else {
                datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
            }
        }

        if (this.name == 'telefono_casa' && this.value != '' && this.value.length < 10) {
            $('#' + this.id).addClass('error');
            $('#' + this.id + '-help').html('El teléfono de casa debe ser de 10 dígitos.');
            check_especial = true;
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;
}

function validacionesDatosBuro() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosBuro .required').each(function () {

        if (this.checked == false) {
            if (this.name == 'acepto_consulta') {
                $('#label_autorizo').addClass('errorTitle');
                datos_faltantes += '- Se debe autorizar la consulta a Buró de Crédito. <br>';
            } else {

                valor = $('input[name=' + this.name + ']:checked').val();
                if (valor == undefined) {
                    check_especial = true;
                    $('.' + this.name + '-title').addClass('errorTitle');
                    $('#' + this.name + '-help').html('Selecciona una respuesta');
                }

                if (this.name == 'credito_bancario' && valor == 'Si' && $('#digitos_tarjeta').val() == '') {
                    $('#digitos_tarjeta').addClass('error');
                    datos_faltantes += '- Ingresa los ultimos 4 dígitos de alguna tarjeta de crédito.  <br>';
                }
            }
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo';
    }

    return datos_faltantes;
}

function validacionesBCCorreccion() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formCorreccion .required').each(function () {
        console.log(this.name);
        if (this.name == 'rfc' && this.value != '' && this.value.length < 10) {
            $('#' + this.id).addClass('error');
            $('#' + this.id + '-help').css('color', 'red');
            $('#' + this.id + '-help').html('Deben ser 10 caracteres.');
            check_especial = true;
        }

        if (this.name == 'homoclave' && this.value != '' && this.value.length < 3) {
            $('#' + this.id).addClass('error');
            $('#' + this.id + '-help').css('color', 'red');
            $('#' + this.id + '-help').html('Deben ser 2 caracteres.');
            check_especial = true;
        }

        if (this.name == 'fecha_nacimiento') {

            if ($('#formCorreccion #day').find('option:selected').val() == '') {
                datos_faltantes += ' - Selecciona un dia de nacimiento. <br>';
                $('#formCorreccion #day').addClass('error');
            }
            if ($('#formCorreccion #month').find('option:selected').val() == '') {
                datos_faltantes += ' - Selecciona un mes de nacimiento. <br>';
                $('#formCorreccion #month').addClass('error');
            }
            if ($('#formCorreccion #year').find('option:selected').val() == '') {
                datos_faltantes += ' - Selecciona un año de nacimiento. <br>';
                $('#formCorreccion #year').addClass('error');
            }
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo';
    }
    return datos_faltantes;
}

function validacionesDatosIngreso() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosIngreso .required').each(function () {
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;
            $('#' + this.id).addClass('error');
            switch (this.name) {
                case 'ocupacion':
                    campo = 'Ocupacion';
                    break;
                case 'fuente_ingresos':
                    campo = 'Fuente de Ingresos';
                    break;
                case 'ingreso_mensual':
                    campo = 'Ingreso mensual comprobable';
                    break;
                case 'residencia':
                    campo = 'Residencia';
                    break;
                case 'empresa':
                    campo = 'Nombre de empresa/lugar de trabajo';
                    break;
                case 'antiguedad_empleo':
                    campo = 'Antigüedad en empleo';
                    break;
                case 'antiguedad_domicilio':
                    campo = 'Antigüedad en domicilio';
                    break;
                case 'numero_dependientes':
                    campo = 'Dependientes económicos';
                    break;
                case 'gastos_familiares':
                    campo = 'Monto gastos familiares mensuales';
                    break;

            }

            datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
        }

        if (this.name == 'ingreso_mensual' && this.value != '') {
            valor = $("#ingreso_mensual").maskMoney('unmasked')[0];
            valor = valor * 1000;
            if (valor < 1000) {
                $('#' + this.id).addClass('error');
                $('#' + this.id + '-help').html('El ingreso minimo es de $ 1,000.00');
                check_especial = true;
            }
        }

        if (this.name == 'gastos_mensuales' && this.value != '') {
            valor = $("#gastos_mensuales").maskMoney('unmasked')[0];
            valor = valor * 1000;
            if (valor <= 0) {
                $('#' + this.id).addClass('error');
                $('#' + this.id + '-help').html('El montomo debe ser mayor a $ 0');
                check_especial = true;
            }
        }

        if (this.name == 'telefono_empleo' && this.value != '') {
            if (this.value.length < 10) {
                $('#' + this.id).addClass('error');
                $('#' + this.id + '-help').html('El teléfono del empleo debe ser de 10 dígitos');
                check_especial = true;
            }
        }

        if (this.name == 'empresa' && this.value != '') {
            if (this.value.length < 5) {
                $('#' + this.id).addClass('error');
                $('#' + this.id + '-help').html('El nombre de la empresa/lugar de trabajo debe tener mínimo 5 caracteres');
                check_especial = true;
            }
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;
}

function validacionesDatosEmpleo() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosEmpleo .required').each(function () {
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;

            if (this.name == 'fecha_ingreso') {
                $('.combodate').addClass('error');
            } else {
                $('#' + this.id).addClass('error');
            }

            switch (this.name) {
                case 'empresa':
                    campo = 'Nombre de empresa/lugar de trabajo';
                    break;
                case 'antiguedad_empleo':
                    campo = 'Antigüedad en empleo';
                    break;
                case 'select_colonia_empleo':
                    campo = 'Colonia';
                    break;
            }

            if (this.name == 'select_colonia_empleo') {
                if ($('#codigo_postal_empleo').val() == '') {
                    datos_faltantes += ' - Ingresa un C.P. para selecionar una ' + campo + '. <br>';
                } else {
                    datos_faltantes += ' - Selecciona una ' + campo + '. <br>';
                }
            }

            if (this.name == 'fecha_ingreso') {

                if ($('#formDatosEmpleo #day').find('option:selected').val() == '') {
                    datos_faltantes += ' - Selecciona un dia de ingreso. <br>';
                }
                if ($('#formDatosEmpleo #month').find('option:selected').val() == '') {
                    datos_faltantes += ' - Selecciona un mes de ingreso. <br>';
                }
                if ($('#formDatosEmpleo #year').find('option:selected').val() == '') {
                    datos_faltantes += ' - Selecciona un año de ingreso. <br>';
                }
            } else {
                datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
            }
        }

        if (this.name == 'calle_empleo' && this.value != '' && this.value.length < 2) {
            $('#' + this.id).addClass('error');
            $('#' + this.id + '-help').html('La calle debe tener al menos 2 caracteres.');
            check_especial = true;
        }

        if (this.name == 'codigo_postal_empleo' && this.value != '' && this.value.length < 5) {
            $('#' + this.id).addClass('error');
            $('#' + this.id + '-help').html('El código postal de tener 5 números.');
            check_especial = true;
        }

        if (this.name == 'telefono_empleo' && this.value != '') {
            if (this.value.length < 10) {
                $('#' + this.id).addClass('error');
                $('#' + this.id + '-help').html('El teléfono del empleo debe ser de 10 dígitos');
                check_especial = true;
            }
        }

        if (this.name == 'empresa' && this.value != '') {
            if (this.value.length < 5) {
                $('#' + this.id).addClass('error');
                $('#' + this.id + '-help').html('El nombre de la empresa/lugar de trabajo debe tener mínimo 5 caracteres');
                check_especial = true;
            }
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;
}

var formatoMoneda = function formatoMoneda(input) {
    return '$' + numeroComas(input);
};

var numeroComas = function numeroComas(n) {
    var parts = n.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
};

$(':input').on('focus', function () {

    $(this).removeClass('error');
    $('#' + this.id + '-help').html('');

    if (this.name == 'credito_hipotecario' || this.name == 'credito_automotriz' || this.name == 'credito_bancario') {
        $('#' + this.name + '-help').html('');
        $('.' + this.name + '-title').removeClass('errorTitle');
    }

    if (this.name == 'acepto_consulta') {
        $('#label_autorizo').removeClass('errorTitle');
    }
});

$('#contraseña').on('keyup', function () {
    var pass = IsValidPwd(this.value);
});

$('#password_login').on('blur', function () {
    if (this.value != '') {
        var pass = IsValidPwdLogin(this.value);
        if (pass == false) {
            $('#' + this.id).addClass('error');
            $('#' + this.id + '-help').html('La contraseña no cumple con el formato: <br> Al menos 1 letra mayúscula, 1 letra minúscula, 1 número y al menos 8 carateres.');
            $("#buttonLogin").prop("disabled", true);
        } else {
            $("#buttonLogin").prop("disabled", false);
        }
    }
});

$('input[name=credito_hipotecario]').on('change', function () {
    $('#' + this.name + '_descripcion_Si').hide();
    $('#' + this.name + '_descripcion_No').hide();
    $('#' + this.name + '_descripcion_' + this.value).show();
});

$('input[name=credito_automotriz]').on('change', function () {
    $('#' + this.name + '_descripcion_Si').hide();
    $('#' + this.name + '_descripcion_No').hide();
    $('#' + this.name + '_descripcion_' + this.value).show();
});

$('input[name=credito_bancario]').on('change', function () {
    $('#' + this.name + '_descripcion_Si').hide();
    $('#' + this.name + '_descripcion_No').hide();
    $('#' + this.name + '_descripcion_' + this.value).show();

    $('#digitos_tarjeta').val('');
    $('#descripcion_digitos').hide();
    if (this.value == 'Si') {
        $('#div_digitos_tarjeta').show();
    } else {
        $('#div_digitos_tarjeta').hide();
    }
});

$('#digitos_tarjeta').on('change', function () {
    $('#descripcion_digitos').show();
});

$('#email').on('change', function () {
    var email = IsEmail(this.value);
    if (email == false) {
        $('#' + this.id).addClass('error');
        $('#' + this.id + '-help').html('Ingresa un correo válido');
    }
});

$('#email_login').on('blur', function () {
    if (this.value != '') {
        var email = IsEmail(this.value);
        if (email == false) {
            $('#' + this.id).addClass('error');
            $('#' + this.id + '-help').html('Ingresa un correo válido');
            $("#buttonLogin").prop("disabled", true);
        } else {
            $("#buttonLogin").prop("disabled", false);
        }
    }
});

$(document).on('click', "#terminaFlujo", function () {
    location.href = '/';
});

function IsEmail(valor) {
    return (/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+$/.test(valor)
    );
}

/**
 * Verifica que la contraseña que ingresa el prospecto cumpla con la politica
 * de contraseñas
 *
 * @return {boolean} Resultado de la validación de la contraseña
 */
function IsValidPwd(valor) {

    $('.helperPassword').show();
    var ucase = new RegExp("[A-Z]+");
    var lcase = new RegExp("[a-z]+");
    var num = new RegExp("[0-9]+");

    var valid_len = false;
    var valid_ucase = false;
    var valid_lcase = false;
    var valid_num = false;

    if (valor.length >= 8) {
        $("#8char").removeClass("glyphicon-remove");
        $("#8char").addClass("glyphicon-ok");
        $("#8char").css("color", "#00A41E");
        valid_len = true;
    } else {
        $("#8char").removeClass("glyphicon-ok");
        $("#8char").addClass("glyphicon-remove");
        $("#8char").css("color", "#FF0004");
    }

    if (ucase.test(valor)) {
        $("#ucase").removeClass("glyphicon-remove");
        $("#ucase").addClass("glyphicon-ok");
        $("#ucase").css("color", "#00A41E");
        valid_ucase = true;
    } else {
        $("#ucase").removeClass("glyphicon-ok");
        $("#ucase").addClass("glyphicon-remove");
        $("#ucase").css("color", "#FF0004");
    }

    if (lcase.test(valor)) {
        $("#lcase").removeClass("glyphicon-remove");
        $("#lcase").addClass("glyphicon-ok");
        $("#lcase").css("color", "#00A41E");
        valid_lcase = true;
    } else {
        $("#lcase").removeClass("glyphicon-ok");
        $("#lcase").addClass("glyphicon-remove");
        $("#lcase").css("color", "#FF0004");
    }

    if (num.test(valor)) {
        $("#num").removeClass("glyphicon-remove");
        $("#num").addClass("glyphicon-ok");
        $("#num").css("color", "#00A41E");
        valid_num = true;
    } else {
        $("#num").removeClass("glyphicon-ok");
        $("#num").addClass("glyphicon-remove");
        $("#num").css("color", "#FF0004");
    }

    if (valid_len == true && valid_ucase == true && valid_lcase == true && valid_num == true) {
        return true;
    } else {
        return false;
    }
}

function IsValidPwdLogin(valor) {

    $('.helperPassword').show();
    var ucase = new RegExp("[A-Z]+");
    var lcase = new RegExp("[a-z]+");
    var num = new RegExp("[0-9]+");

    var valid_len = false;
    var valid_ucase = false;
    var valid_lcase = false;
    var valid_num = false;

    if (valor.length >= 8) {
        valid_len = true;
    }

    if (ucase.test(valor)) {
        valid_ucase = true;
    }

    if (lcase.test(valor)) {
        valid_lcase = true;
    }

    if (num.test(valor)) {
        valid_num = true;
    }
    if (valid_len == true && valid_ucase == true && valid_lcase == true && valid_num == true) {
        return true;
    } else {
        return false;
    }
}

var oldCursor,
    oldValue = '';
regex = new RegExp(/^\d{0,10}$/g);
unmask = function unmask(value) {
    var output = value.replace(new RegExp(/[^\d]/, 'g'), '');
    return output;
};
keydownHandler = function keydownHandler(e) {
    var el = e.target;
    oldValue = el.value;
    oldCursor = el.selectionEnd;
};
inputHandler = function inputHandler(e) {
    var el = e.target,
        newCursorPosition,
        newValue = unmask(el.value);

    if (newValue.match(regex)) {
        el.value = newValue;
    } else {
        el.value = oldValue;
    }
};

var codigoPostal = document.querySelector('#codigo_postal');
codigoPostal.addEventListener('keydown', keydownHandler);
codigoPostal.addEventListener('input', inputHandler);

var telefonoCasa = document.querySelector('#telefono_casa');
telefonoCasa.addEventListener('keydown', keydownHandler);
telefonoCasa.addEventListener('input', inputHandler);

var digitosTDC = document.querySelector('#digitos_tarjeta');
digitosTDC.addEventListener('keydown', keydownHandler);
digitosTDC.addEventListener('input', inputHandler);

var telefonoEmpleo = document.querySelector('#telefono_empleo');
telefonoEmpleo.addEventListener('keydown', keydownHandler);
telefonoEmpleo.addEventListener('input', inputHandler);

var telefonoCelular = document.querySelector('#celular');
telefonoCelular.addEventListener('keydown', keydownHandler);
telefonoCelular.addEventListener('input', inputHandler);

var telefonoCelular = document.querySelector('#celular');
telefonoCelular.addEventListener('keydown', keydownHandler);
telefonoCelular.addEventListener('input', inputHandler);

var codigoVerificacion = document.querySelector('#conf_code');
codigoVerificacion.addEventListener('keydown', keydownHandler);
codigoVerificacion.addEventListener('input', inputHandler);

$('#monto_prestamo').maskMoney({
    prefix: '$ ',
    precision: 0,
    reverse: false,
    selectAllOnFocus: true,
    formatOnBlur: true,
    decimal: '.',
    thousands: ',',
    allowEmpty: true
});

$('#ingreso_mensual').maskMoney({
    prefix: '$ ',
    precision: 0,
    reverse: false,
    selectAllOnFocus: true,
    formatOnBlur: true,
    decimal: '.',
    thousands: ',',
    allowEmpty: true
});

$('#gastos_mensuales').maskMoney({
    prefix: '$ ',
    precision: 0,
    reverse: false,
    selectAllOnFocus: true,
    formatOnBlur: true,
    decimal: '.',
    thousands: ',',
    allowEmpty: true
});

var interval;
function makeTimer(endTime) {

    endTime = Date.parse(endTime) / 1000;

    var now = new Date();
    now = Date.parse(now) / 1000;

    var timeLeft = endTime - now;
    if (timeLeft <= 0) {
        stopinterval();
    }

    var days = Math.floor(timeLeft / 86400);
    var hours = Math.floor((timeLeft - days * 86400) / 3600);
    var minutes = Math.floor((timeLeft - days * 86400 - hours * 3600) / 60);
    var seconds = Math.floor(timeLeft - days * 86400 - hours * 3600 - minutes * 60);

    if (hours < "10") {
        hours = "0" + hours;
    }
    if (minutes < "10") {
        minutes = "0" + minutes;
    }
    if (seconds < "10") {
        seconds = "0" + seconds;
    }

    $('#timerResend').html(minutes + ':' + seconds);
}

function playinterval() {
    $('#timerResend').show();
    $('#reenviarSMS').removeAttr('onclick', 'reenviar_sms();');
    $('#reenviarSMS').addClass('disabled');
    var newTime = new Date();
    newTime = new Date(newTime.getTime() + 5 * 60000);
    interval = setInterval(function () {
        makeTimer(newTime);
    }, 1000);
    return false;
}

function stopinterval() {
    $('#timerResend').hide();
    $('#reenviarSMS').attr('onclick', 'reenviar_sms();');
    $('#reenviarSMS').removeClass('disabled');
    clearInterval(interval);
    return false;
}

/*
function guardarCuestionarioDinamico() {

    $('.labelDinamico').css('color', '#b7b7b7');
    $('.lerror').text('');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        data: $("#formCuestionarioDinamico").serialize(),
        url: "/saveCuestionarioDinamico",
        dataType: "json",
        beforeSend: function() {
            //OpenModal("#processing");
       }
    }).done(function(resultado) {

        if (resultado.success == true) {

            Swal.fire({
                html: resultado.modal,
                showConfirmButton: false,
                allowOutsideClick: false,
            });

        } else {
            // CloseModal("#processing");
            swal({
                title: "Ooops.. Surgió un problema",
                text: resultado.message,
                showConfirmButton: true
           });
        }

    }).fail(function(resultado, textStatus, errorThrow) {
        $("html, body").animate({ scrollTop: $("#cuestionarioDinamico").offset().top }, 1000);
        // CloseModal("#processing");
        if (resultado.status == 422 && errorThrow == 'Unprocessable Entity') {
            $.each(resultado.responseJSON, function(index, error) {
                $('#label_' + index).css('color', 'red');
                $('#lerror_' + index).text(error);
            });
        } else {
            swal({
                title: "Ooops.. Surgió un problema",
                text: resultado.status + ': ' + errorThrow,
                showConfirmButton: true
           });
        }

    });

}*/

function guardarCuestionarioDinamico() {

    $('.labelDinamico').css('color', '#b7b7b7');
    $('.help').text('');

    var texto = 'Guardando respuestas...';
    Swal.fire({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton: false
    });

    var datos = $("#formCuestionarioDinamico").serialize();
    axios.post('/saveCuestionarioDinamico', datos).then(function (resultado) {

        if (resultado.data.success == true) {

            Swal.fire({
                html: resultado.data.modal,
                showConfirmButton: false,
                allowOutsideClick: false,
                customClass: 'modaloferta'
            });
        } else {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: resultado.data.message,
                showConfirmButton: true
            });
        }
    }).catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (error.response.status == 422) {
            $.each(error.response.data, function (index, error) {
                $('#label_' + index).css('color', 'red');
                $('#lerror_' + index).text(error);
            });
            swal.close();
        } else {
            var _Swal$fire;

            Swal.fire((_Swal$fire = {
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                showConfirmButton: true,
                type: 'error'
            }, _defineProperty(_Swal$fire, 'showConfirmButton', true), _defineProperty(_Swal$fire, 'customClass', 'modalError'), _defineProperty(_Swal$fire, 'allowOutsideClick', false), _Swal$fire));
        }
    });
}

function campoVisible(valor, div_id, respuetas) {

    respuestas = respuetas.split('|');
    div_id = div_id.split('|');

    $.each(respuestas, function (index, respuesta) {

        if (respuesta == valor) {
            $('#div_' + div_id[index]).show();
        } else {
            $('#div_' + div_id[index]).hide();
        }
    });
}

function campoVisible(valor, div_id, respuetas) {

    respuestas = respuetas.split('|');
    div_id = div_id.split('|');

    $.each(respuestas, function (index, respuesta) {

        if (respuesta == valor) {
            $('#div_' + div_id[index]).show();
        } else {
            $('#div_' + div_id[index]).hide();
        }
    });
}

function selectoferta(oferta) {
    $('.btn-oferta').removeClass('btn-oferta-clicked');
    $(oferta).addClass('btn-oferta-clicked');
    $('.datos_oferta').hide();
    $('.propuesta').hide();
    setTimeout(function () {
        $('#datos_' + oferta.id).show();
        $('#oferta_seleccionada').val($('#oferta_' + oferta.id).val());
        $('.datos_oferta').show();
    }, 100);
}

/**
 * Envia la petición para guardar el status de la oferta, cierra el modal oferta
 * y abre el de cuestionario fijo
 */
$(document).on('click', "#aceptarOferta", function () {

    var texto = 'Guardando status de la oferta...';
    Swal.fire({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false
    });

    var clientId = 0;
    ga(function (tracker) {
        clientId = tracker.get('clientId');
    });

    axios.post('/solicitud/status_oferta', {
        'status_oferta': 'Oferta Aceptada',
        'clientId': clientId
    }).then(function (response) {

        if (response.data.hasOwnProperty('eventTM')) {
            var eventos = response.data.eventTM;
            $.each(eventos, function (key, evento) {
                dataLayer.push(evento);
            });
        }

        if (response.data.carga_identificacion_selfie == true) {
            location.href = '/carga_documentos/paso1';
        } else {

            Swal.fire({
                html: response.data.modal,
                showConfirmButton: false,
                allowOutsideClick: false,
                customClass: 'modalstatus'
            });
        }
    }).catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
        });
    });
});

$(document).on('click', "#aceptarOfertaDoble", function () {

    var tipo_oferta = $('#oferta_seleccionada').val();
    var clientId = 0;
    ga(function (tracker) {
        clientId = tracker.get('clientId');
    });

    var texto = 'Guardando status de la oferta...';
    Swal.fire({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false
    });

    axios.post('/solicitud/status_oferta_doble', {
        'status_oferta': 'Oferta Aceptada',
        'tipo_oferta': tipo_oferta,
        'clientId': clientId
    }).then(function (response) {

        if (response.data.success == true) {

            if (response.data.hasOwnProperty('eventTM')) {
                var eventos = response.data.eventTM;
                $.each(eventos, function (key, evento) {
                    dataLayer.push(evento);
                });
            }

            if (response.data.carga_identificacion_selfie == true) {
                location.href = '/carga_documentos/paso1';
            } else {

                Swal.fire({
                    html: response.data.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modalstatus'
                });
            }
        } else {
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
            });
        }
    }).catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
        });
    });
});

$(document).on('click', "#rechazarOferta", function () {
    $('#tituloOferta').hide();
    $('#emojiOferta').hide();
    $('#seccionOferta').hide();
    $('#botonesOferta').hide();
    $('#tituloRechazo').show();
    $('#seccionRechazo').show();
    $('#botonesRechazo').show();
});

function cambioMotivoRechazo(select) {

    if (select.value == 'Otro') {
        $('#textOtroRechazo').show();
        $('#textOtroRechazo').val('');
        $('#textOtroRechazo').focus();
    } else {
        $('#textOtroRechazo').hide();
    }
}

$(document).on('click', "#cancelarRechazarOferta", function () {

    $('#botonesOferta').show();
    $('#seccionOferta').show();
    $('#emojiOferta').show();
    $('#tituloOferta').show();
    $('#botonesRechazo').hide();
    $('#seccionRechazo').hide();
    $('#tituloRechazo').hide();
    $('#textOtroRechazo').hide();
    $("#motivo_rechazo").val('SELECCIONA');
    $('#textOtroRechazo').val('');
});

$(document).on('click', "#confirmaRechazarOferta", function () {

    var motivo_rechazo = $('#motivo_rechazo').val();
    var descripcion_otro = $('#textOtroRechazo').val();
    ga(function (tracker) {
        clientId = tracker.get('clientId');
    });

    var texto = 'Guardando status de la oferta...';
    Swal.fire({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false
    });

    axios.post('/solicitud/rechazar_oferta', {
        'status_oferta': 'Oferta Rechazada',
        'motivo': true,
        'motivo_rechazo': motivo_rechazo,
        'descripcion_otro': descripcion_otro,
        'clientId': clientId
    }).then(function (response) {

        if (response.data.success == true) {
            window.location.href = '/';
        } else {
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
            });
        }
    }).catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
        });
    });
});

function iniciarSesion() {

    $("#loginError").html('');
    var texto = 'Iniciando sesión...';
    Swal.fire({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false
    });

    var datos = $('#formLogin').serializeArray();
    datos = getFormData(datos);

    var clientId = 0;
    ga(function (tracker) {
        clientId = tracker.get('clientId');
    });
    datos['clientId'] = clientId;

    axios.post('/login/prospecto', datos).then(function (response) {

        if (response.data.success == true) {
            console.log(response);
            var scrolTop = '#simulador';
            $('#loginModal').modal('hide');
            $('#formRegistro').trigger("reset");
            $('#formSimulador').trigger("reset");
            $('#prestamoPersonalPagoFIjo').html('$0.00');
            if (response.data.nueva_solicitud == false) {
                $('#' + response.data.formulario).show();
                $('#' + response.data.oculta).hide();
                $('#formRegistro').hide();
                swal.close();
                window.location = '/solicitud/validacion_usuario';
                var scrolTop = '#info_prestamo_personal';
                $('#info_prestamo_personal').show();
                $("#info_general_prestamo").text(response.data.prestamo);
                $("#info_general_plazo").text(response.data.plazo);
                $("#info_general_finalidad").text(response.data.finalidad);

                if (response.data.cuestionario != null) {
                    $('#cuestionarioDinamico').hide();
                    $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
                    var cuestionarios = response.data.cuestionario;

                    $('#cuestionarioDinamico').append("<form action='#' id='formCuestionarioDinamico'>");
                    var formulario = "";
                    var situaciones = "";
                    formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'><h2 class='secondary-title txt-center'>Falta un paso más</h2>";
                    formulario = formulario + "<div class='calculadoras-productos'><small>Ayúdanos con estos últimos datos.</small></div></div>";
                    $.each(cuestionarios, function (key, cuestionario) {
                        formulario = formulario + "<div class='col-lg-10 col-lg-offset-1 col-xs-12'>";
                        formulario = formulario + "<div class='col-12' style='margin-bottom: 10px; margin-top: 10px;'>";
                        if (cuestionario.introduccion != null) {
                            formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4><label class=''>" + cuestionario.introduccion + "</label></div>";
                        } else {
                            formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4></div>";
                        }

                        $.each(cuestionario.cuestionario, function (index, pregunta) {

                            oculto = '';
                            change = '';
                            largo = 'col-12';
                            width = '100%';
                            if (pregunta.oculto == 1) {
                                oculto = 'display:none';
                            }
                            if (pregunta.grid_estilo != null) {
                                largo = pregunta.grid_estilo;
                            }
                            if (pregunta.ancho_estilo != null) {
                                width = pregunta.ancho_estilo;
                            }
                            id = cuestionario.situacion + '_' + pregunta.id;

                            formulario = formulario + "<div class='" + largo + " ' id='div_" + id + "' style='margin-bottom: 5px; margin-bottom: 5px;padding-left: 0px;padding-right: 0px;" + oculto + "'>";
                            situaciones = situaciones + cuestionario.situacion + '|';
                            if (pregunta.depende != '') {
                                var valores = "'" + pregunta.depende + "','" + pregunta.respuesta_depende + "'";
                                change = 'onChange = "campoVisible(this.value,' + valores + ')"';
                            }

                            if (pregunta.tipo == 'text') {
                                formulario = formulario + '<label id="label_' + id + '" class="labelDinamico">' + pregunta.pregunta + '</label>';
                                var style = 'text-transform: uppercase; text-align: center; width: ' + width;
                                formulario = formulario + '<div class="calculadora-personal"><input type="text" id="' + id + '" name="' + id + '" style="' + style + '"/></div>';
                            }

                            if (pregunta.tipo == 'textarea') {
                                formulario = formulario + '<label id="label_' + id + '" class="labelDinamicoT">' + pregunta.pregunta + '</label>';
                                var style = 'text-transform: uppercase; float: none important!; resize: none;';
                                formulario = formulario + '<div class="calculadora-personal"><textarea id="' + id + '" name="' + id + '" maxlength="255" rows="2" style="' + style + '"></textarea></div>';
                            }

                            if (pregunta.tipo == 'select') {
                                formulario = formulario + '<label id="label_' + id + '" class="labelDinamico">' + pregunta.pregunta + '</label>';
                                formulario = formulario + '<div class="calculadoras-productos"><div class="withDecoration"><span><select id="' + id + '" name="' + id + '" ' + change + ' class="dinamico pre-registro-input">';
                                formulario = formulario + '<option value="" selected>SELECCIONA</option>';
                                opciones = pregunta.opciones.split('|');
                                var opcionesLista = '';
                                $.each(opciones, function (indexO, opcion) {
                                    opcionesLista = opcionesLista + '<option value="' + opcion + '">' + opcion + '</option>';
                                });
                                formulario = formulario + opcionesLista + '</select></span><div class="decoration"><i class="fas fa-chevron-down"></i></div></div></div>';
                            }
                            formulario = formulario + '<label id="lerror_' + id + '" class="help"></label>';
                            formulario = formulario + "</div>";
                        });

                        $('#cuestionarioDinamico form').append(formulario);
                        formulario = "";
                    });

                    $('#cuestionarioDinamico form').append('<div class="row col-lg-10 col-lg-offset-1 col-xs-12 mt-0"><div class="text-right"><button type="button" class="general-button" onclick="guardarCuestionarioDinamico()">Continuar</button></div></div>');
                    $('#cuestionarioDinamico form').append('<input type="hidden" id="situaciones" name="situaciones"/>');
                    $('#cuestionarioDinamico form').append('<input type="hidden" id="tipo_poblacion" name="tipo_poblacion" value="' + response.data.tipo_poblacion + '"/>');
                    $('#situaciones').val(situaciones);
                }
                //playinterval();
            } else {
                $('#' + response.data.formulario).show();
                $('#formRegistro').hide();
                swal.close();
                $('#nueva_solicitud').val('true');
            }

            $('#linkNoSesion').hide();
            $('#linkSesion').show();
            $('#nombreProspecto').html(response.data.nombre_prospecto);
            $('#show_celular').html(response.data.celular);

            if (response.data.modal != null) {

                Swal.fire({
                    html: response.data.modal,
                    showConfirmButton: true,
                    allowOutsideClick: false,
                    confirmButtonText: 'Aceptar',
                    customClass: 'modalstatus'
                }).then(function (result) {
                    if (result.value) {
                        $("html, body").animate({ scrollTop: $(scrolTop).offset().top - 50 }, 1000);
                    }
                });
            }
        } else {

            $("#loginError").html(response.data.message);
            swal.close();
        }
    }).catch(function (error) {

        var descripcionError = '';
        if (error.hasOwnProperty('response')) {
            descripcionError = error.response.status + ': ' + error.response.responseText;
        } else {
            descripcionError = error;
        }

        $("#loginError").html(descripcionError);
    });
}