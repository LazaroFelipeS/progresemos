function selectoferta(oferta) {
    $('.btn-oferta').removeClass('btn-oferta-clicked');
    $(oferta).addClass('btn-oferta-clicked');
    $('.datos_oferta').hide();
    $('.propuesta').hide();
    setTimeout(function(){
        $('#datos_' + oferta.id).show();
        $('#oferta_seleccionada').val($('#oferta_' + oferta.id).val());
        $('.datos_oferta').show();
    }, 100);
}

/**
 * Envia la petición para guardar el status de la oferta, cierra el modal oferta
 * y abre el de cuestionario fijo
 */
$(document).on('click', "#aceptarOferta", function() {

    var texto = 'Guardando status de la oferta...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    axios.post('/solicitud/status_oferta', {
        'status_oferta': 'Oferta Aceptada',
        'clientId': clientId
    })
    .then(function (response) {

        if (response.data.hasOwnProperty('eventTM')) {
            var eventos = response.data.eventTM;
            $.each(eventos, function(key, evento) {
                dataLayer.push(evento);
            });
        }

        if (response.data.carga_identificacion_selfie == true) {
            location.href = '/carga_documentos/paso1';
        } else {

            Swal.fire({
                html: response.data.modal,
                showConfirmButton: false,
                allowOutsideClick: false,
                customClass: 'modalstatus'
            });
        }

    })
    .catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

});

$(document).on('click', "#aceptarOfertaDoble", function() {

    var tipo_oferta = $('#oferta_seleccionada').val();
    var clientId = 0;
    ga(function(tracker) {
        clientId = tracker.get('clientId');
    });

    var texto = 'Guardando status de la oferta...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    axios.post('/solicitud/status_oferta_doble', {
        'status_oferta' : 'Oferta Aceptada',
        'tipo_oferta'   : tipo_oferta,
        'clientId': clientId
    })
    .then(function (response) {

        if (response.data.success == true) {

            if (response.data.hasOwnProperty('eventTM')) {
                var eventos = response.data.eventTM;
                $.each(eventos, function(key, evento) {
                    dataLayer.push(evento);
                });
            }

            if (response.data.carga_identificacion_selfie == true) {
                location.href = '/carga_documentos/paso1';
            } else {

                Swal.fire({
                    html: response.data.modal,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: 'modalstatus'
                });

            }

        } else {
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });
        }
    })
    .catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

});

$(document).on('click', "#rechazarOferta", function() {
    $('#tituloOferta').hide();
    $('#emojiOferta').hide();
    $('#seccionOferta').hide();
    $('#botonesOferta').hide();
    $('#tituloRechazo').show();
    $('#seccionRechazo').show();
    $('#botonesRechazo').show();
});

function cambioMotivoRechazo(select) {

    if(select.value == 'Otro') {
        $('#textOtroRechazo').show();
        $('#textOtroRechazo').val('');
        $('#textOtroRechazo').focus();
    } else {
        $('#textOtroRechazo').hide();
    }

}

$(document).on('click', "#cancelarRechazarOferta", function() {

    $('#botonesOferta').show();
    $('#seccionOferta').show();
    $('#emojiOferta').show();
    $('#tituloOferta').show();
    $('#botonesRechazo').hide();
    $('#seccionRechazo').hide();
    $('#tituloRechazo').hide();
    $('#textOtroRechazo').hide();
    $("#motivo_rechazo").val('SELECCIONA');
    $('#textOtroRechazo').val('');

});

$(document).on('click', "#confirmaRechazarOferta", function() {

    var motivo_rechazo = $('#motivo_rechazo').val();
    var descripcion_otro = $('#textOtroRechazo').val();
    ga(function(tracker) {
        clientId = tracker.get('clientId');
    });

    var texto = 'Guardando status de la oferta...';
    Swal.fire({
        html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton:false,
        allowOutsideClick: false
    });

    axios.post('/solicitud/rechazar_oferta', {
        'status_oferta' : 'Oferta Rechazada',
        'motivo' : true,
        'motivo_rechazo' : motivo_rechazo,
        'descripcion_otro' : descripcion_otro,
        'clientId': clientId
    })
    .then(function (response) {

        if (response.data.success == true) {
            window.location.href = '/';
        } else {
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });
        }
    })
    .catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

});
