function IsValidName(e) {
    return !(e.length < 2)
}

function IsValidTel(e) {
    return 10 == e.length
}

function IsEmail(e) {
    return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+$/.test(e)
}

function IsChecked(e) {
    return e.is(':checked');
}


function cambiaFinalidad ($this) {
    var finalidad = $($this).find("option:selected").text();

    if (finalidad == "Otra") {
        $('#other_finality').show();
        $('#finalitys').hide();
    }
}

/**
 * Verifica que la contraseña que ingresa el prospecto cumpla con la politica
 * de contraseñas
 *
 * @return {boolean} Resultado de la validación de la contraseña
 */
function IsValidPwd(id_campo) {

    var ucase = new RegExp("[A-Z]+");
	var lcase = new RegExp("[a-z]+");
	var num = new RegExp("[0-9]+");

    var valid_len = false;
    var valid_ucase = false;
    var valid_lcase = false;
    var valid_num = false;

	if ($(id_campo).val().length >= 8) {
		$("#8char").removeClass("fa-times");
		$("#8char").addClass("fa-check");
		$("#8char").css("color","#00A41E");
        valid_len = true;
	} else {
		$("#8char").removeClass("fa-check");
		$("#8char").addClass("fa-times");
		$("#8char").css("color","#FF0004");
	}

	if (ucase.test($(id_campo).val())) {
		$("#ucase").removeClass("fa-times");
		$("#ucase").addClass("fa-check");
		$("#ucase").css("color","#00A41E");
        valid_ucase = true;
	} else {
		$("#ucase").removeClass("fa-check");
		$("#ucase").addClass("fa-times");
		$("#ucase").css("color","#FF0004");
	}

	if (lcase.test($(id_campo).val())) {
		$("#lcase").removeClass("fa-times");
		$("#lcase").addClass("fa-check");
		$("#lcase").css("color","#00A41E");
        valid_lcase = true;
	} else {
		$("#lcase").removeClass("fa-check");
		$("#lcase").addClass("fa-times");
		$("#lcase").css("color","#FF0004");
	}

	if (num.test($(id_campo).val())) {
		$("#num").removeClass("fa-times");
		$("#num").addClass("fa-check");
		$("#num").css("color","#00A41E");
        valid_num = true;
	} else {
		$("#num").removeClass("fa-check");
		$("#num").addClass("fa-times");
		$("#num").css("color","#FF0004");
	}

    if (valid_len == true && valid_ucase == true && valid_lcase == true && valid_num == true) {
        return true;
    } else {
        return false;
    }

    // return !!(e.length >= 8 && /[A-Z]+/.test(e) && /[a-z]+/.test(e) && /[0-9]+/.test(e))
}

function IsEmpty(e) {
    return 0 === e.length
}

$("#reg_password").keyup(function() {

    var isValid = IsValidPwd('#reg_password');
    return isValid;

});

/**
 * Muestra u oculta la ayuda para la generación de la contraseña
 */
function helperPassword(opcion) {

    if (opcion == 'On') {
        $('#helper-password').show();
    } else if (opcion == 'Off') {
        $('#helper-password').hide();
    }
}

/**
 * Valida que el RFC capturado sea correcto en cuanto a estructura
 *
 * @return {boolean} Resultado de la validación del CURP
 */
function validateRFC() {

    var year = $('.year').val();
    var month = parseInt($('.month').val());
    var day = parseInt($('.day').val());
    var rfc = $('#solic_rfc').val();
    var response = [];

    if (year != '' && month != '' && day != '' && rfc != '') {

        // Validando la expresión regular del RFC
        // 4 Letras, 2 digitos año de nacimiento, 2 digitos mes de nacimiento
        // 2 digitos dia de nacimiento y si se captura la homoclave 2 primeros
        // digos cualquier letra o número y el ultimo cualquier número o letra A (0-9A)
        var matches = rfc.match(/^[A-Z]{4}([0-9]{2})([0-1][0-9])([0-3][0-9])(?:[A-Z0-9]{2}[A-Z0-9])?$/i);

        if (!matches) {
            response['success'] = false;
            response['msj'] = 'La estructura del RFC es inválida';
            return response;
        }

        // Validando fecha de nacimiento
        year_m = rfc.substring(4, 6);
        year_c = year.substring(2, 4);
        month_m = parseInt(rfc.substring(6, 8)) - 1;
        day_m = parseInt(rfc.substring(8, 10));

        if (year_c == year_m && month == month_m && day == day_m) {
            var date = new Date(year_m, month_m, day_m);

            if (date.getFullYear().toString().substr(2, 2) == year_m && date.getMonth() == month_m && date.getDate() == day_m) {
                response['success'] = true;
                response['msj'] = 'RFC correcto';
                return response;
            } else {
                response['success'] = false;
                response['msj'] = 'La fecha de nacimiento en el RFC es inválida';
                return response;
            }

        } else {
            response['success'] = false;
            response['msj'] = 'La fecha de nacimiento en el RFC no coincide con la capturada';
            return response;
        }
    } else {
        response['success'] = true;
        return response;
    }
}

/**
 * Valida que el CURP capturado sea correcto en cuanto a estructura
 *
 * @return {array} Resultado de la validación del CURP
 */
function validateCURP() {

    var year = $('.year').val();
    var month = parseInt($('.month').val());
    var day = parseInt($('.day').val());
    var curp = $('#solic_curp').val();
    var response = [];

    response['success'] = true;
    return response;

    if (year != '' && month != '' && day != '' && curp != '') {

        if (curp.length < 18) {

            response['success'] = false;
            response['msj'] = 'La longitud del CURP debe ser de 18 caracteres';
            return response;

        }

        // Validando la expresión regular del RFC
        // 4 Letras, 2 digitos año de nacimiento, 2 digitos mes de nacimiento
        // 2 digitos dia de nacimiento y si se captura la homoclave 2 primeros
        // digos cualquier letra o número y el ultimo cualquier número o letra A
        var matches = curp.match(/([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)/gi);

        if (!matches) {

            response['success'] = false;
            response['msj'] = 'El formato del CURP es incorrecto';
            return response;

        }

        // Validando fecha de nacimiento
        year_m = curp.substring(4, 6);
        year_c = year.substring(2, 4);
        month_m = parseInt(curp.substring(6, 8) - 1);
        day_m = parseInt(curp.substring(8, 10));

        if (year_c == year_m && month == month_m && day == day_m) {
            var date = new Date(year_m, month_m, day_m);

            if (date.getFullYear().toString().substr(2, 2) == year_m && date.getMonth() == month_m && date.getDate() == day_m) {

                response['success'] = true;
                response['msj'] = 'CURP Correcto';
                return response;

            } else {

                response['success'] = false;
                response['msj'] = 'El formato de la fecha de nacimiento del CURP es incorrecto';
                return response;

            }

        } else {
            response['success'] = false;
            response['msj'] = 'La fecha de nacimiento no coincide con la capturada en el CURP';
            return response;
        }
    } else {
        response['success'] = true;
        return response;
    }

}

/**
 * Realiza el envío de los datos capturados en la pantalla 3 de 4 (Cuentas de crédito)
 * al Backend y la primer llamada a buro de crédito.
 */
function sendBCRequest() {
    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });
    $("#bc-score-result-msg").html("Iniciando consulta BC Score..."), $.ajax({
        method: "POST",
        url: $("#solicitud_bc_form").attr("action"),
        data: $("#solicitud_bc_form").serialize(),
        beforeSend: function() {
            OpenModal("#processing")
        }
    }).done(function(e) {
        var res = e;
        if (res.stat == "Califica BC Score") {
            if (res.lead == 'askrobin') {
                sendPixelAskRobin('3', res.lead_id, res.prospect_id);
            }
        }

        CloseModal("#processing"), res = e, "No Encontrado" == res.stat && (OpenModal("#faltaInfo2")),
        "Error BC" == res.stat && (OpenModal("#issues")),
        "Datos Elegible" == res.stat && (dataLayer.push({'event': 'EncontradoBC', 'userId': res.prospect_id, 'solicId': res.solic_id, 'clientId': clientId}), $("#prospect_adic_id").val(res.prospect_id),
            $("#solic_adic_id").val(res.solic_id), $("#solicitud-three").hide(), $("#solicitud-four").toggle("slide", {
            direction: "right" }, 1e3), $("#pag").text("4 de 4"), ScrollUp("#form-modificar-solicitud"), $("#datos_caso").val("Datos Elegible")),
        "Califica BC Score" == res.stat && (dataLayer.push({'event': 'EncontradoBC', 'userId': res.prospect_id, 'solicId': res.solic_id, 'clientId': clientId}), dataLayer.push({'event': 'PasaBC', 'userId': res.prospect_id, 'solicId': res.solic_id, 'clientId': clientId}), $("#prospect_hawk_id").val(res.prospect_id), $("#solic_hawk_id").val(res.solic_id), sendReportRequest()),
        "No Califica" == res.stat && (dataLayer.push({'event': 'EncontradoBC', 'userId': res.prospect_id, 'solicId': res.solic_id, 'clientId': clientId}), $("#bc-score-result-msg").html('<div class="alert alert-danger"><p>' + res.result + "</p></div>"), OpenModal("#gracias"))

    }).fail(function(e) {
        alert(e);
    })
}

/**
 * Realiza la petición de la segunda llamada a buró de crédito, y dependiendo de
 * la respuesta muestra la pantalla 4 de 4 o un modal con la respuesta
 */
function sendReportRequest() {
    $.ajax({
        method: "POST",
        url: $("#solicitud_hawk_form").attr("action"),
        data: $("#solicitud_hawk_form").serialize(),
        beforeSend: function() {
            OpenModal("#processing")
        }
    }).done(function(e) {
        var res = e;
        // Verificando si la respuesta es JSON o text
        try {
            res = $.parseJSON(e);
        } catch( e ) {
            // Es un JSON bien formado
        } finally {
            res = e;
        }

        CloseModal("#processing"),
        "No Encontrado" == res.stat && (OpenModal("#faltaInfo2")),
        "Error BC" == res.stat && (OpenModal("#issues")),
        "Segunda Llamada BC OK Elegible" == res.stat && ($("#prospect_alp_id").val(res.prospect_id), $("#solic_alp_id").val(res.solic_id), sendALPRequest()),
        "Segunda Llamada BC OK" == res.stat && ($("#prospect_adic_id").val(res.prospect_id), $("#solic_adic_id").val(res.solic_id), $("#solicitud-three").hide(), $("#solicitud-four").toggle("slide", {
            direction: "right"}, 1e3), $("#pag").text("4 de 4"), ScrollUp("#form-modificar-solicitud"), $("#datos_caso").val("Datos Reporte"))

    }).fail(function(e) {
        console.log(e)
    })
}

/**
 * Realiza la pretición a FICO ALP y la respuesta contiene la decisión de la
 * máquina de riesgos la cual puede ser:
 *
 * Invitación a Continuar - Muestra Modal Invitación a Continuar
 * Invitación a Continuar con Cuestionario Dinámico - Muestra pantalla 5 de 5
 * (Cuestionario Dinámico)
 * Oferta Diferida o Rechazado - Muestra Modal con diferente mensaje. El proceso
 * termina para el prospecto
 *
 */
function sendALPRequest() {
    $.ajax({
        method: "POST",
        url: $("#solicitud_fico_alp_form").attr("action"),
        data: $("#solicitud_fico_alp_form").serialize(),
        beforeSend: function() {
            OpenModal("#processing")
        }
    }).done(function(respuesta) {

        if (respuesta.success == true) {

            var tipoPoblacion = respuesta.tipo_poblacion;
            var clientId = 0;

            $('#encabezadoModal').html(respuesta.plantilla.modal_encabezado);
            $('#imgModal').html(respuesta.plantilla.modal_img);
            $('#cuerpoModal').html(respuesta.plantilla.modal_cuerpo);
            var decision = respuesta.decision;

            if (tipoPoblacion == 'oferta_normal' && decision == 3) {
                $('#propuesta-monto').text(respuesta.resultado.oferta_normal.Monto_Mod);
                $('#propuesta-plazo').text(respuesta.resultado.oferta_normal.Plazo_Mod);
                $('#propuesta-pago').text(respuesta.resultado.oferta_normal.Pago_Mod);
                $('#propuesta-tasa').text(respuesta.resultado.oferta_normal.Tasa_Mod);
                var pantallasExtra = respuesta.resultado.oferta_normal.PantallaExtra;
            } else if (tipoPoblacion == 'oferta_incrementada' && decision == 3) {
                $('#propuesta-monto').text(respuesta.resultado.oferta_mayor.Monto_Mod);
                $('#propuesta-plazo').text(respuesta.resultado.oferta_mayor.Plazo_Mod);
                $('#propuesta-pago').text(respuesta.resultado.oferta_mayor.Pago_Mod);
                $('#propuesta-tasa').text(respuesta.resultado.oferta_mayor.Tasa_Mod);
                var pantallasExtra = respuesta.resultado.oferta_mayor.PantallaExtra;
            } else if (tipoPoblacion == 'oferta_doble' && decision == 3) {
                $('#propuesta1').text(respuesta.resultado.oferta_normal.Monto_Mod);
                $('#propuesta1-monto').text(respuesta.resultado.oferta_normal.Monto_Mod);
                $('#propuesta1-plazo').text(respuesta.resultado.oferta_normal.Plazo_Mod);
                $('#propuesta1-pago').text(respuesta.resultado.oferta_normal.Pago_Mod);
                $('#propuesta1-tasa').text(respuesta.resultado.oferta_normal.Tasa_Mod);
                $('#propuesta2').text(respuesta.resultado.oferta_mayor.Monto_Mod);
                $('#propuesta2-monto').text(respuesta.resultado.oferta_mayor.Monto_Mod);
                $('#propuesta2-plazo').text(respuesta.resultado.oferta_mayor.Plazo_Mod);
                $('#propuesta2-pago').text(respuesta.resultado.oferta_mayor.Pago_Mod);
                $('#propuesta2-tasa').text(respuesta.resultado.oferta_mayor.Tasa_Mod);
                var pantallasExtra = respuesta.resultado.oferta_mayor.PantallaExtra;
            }

            ga(function(tracker) {
            	clientId = tracker.get('clientId');
            });

            // Si la respuesta a la decision del stored procedure es 3 (Invitación
            // a continuar) y no hay pantallas extras se debe mostrar el modal
            // de oferta
            if (pantallasExtra == 0 && decision == 3) {

                dataLayer.push({'event': 'LeadContinua', 'userId': respuesta.prospecto_id, 'solicId': respuesta.solicitud_id, 'clientId': clientId});
                $('#prospecto_id_CF').val(respuesta.prospecto_id);
                $('#solicitud_id_CF').val(respuesta.solicitud_id);
                $('#prospecto_id_oferta').val(respuesta.prospecto_id);
                $('#solicitud_id_oferta').val(respuesta.solicitud_id);
                $('#closeModalStatus').show();

                if (tipoPoblacion == 'oferta_normal' || tipoPoblacion == 'oferta_incrementada' || tipoPoblacion == null) {
                    OpenModal("#modalOferta");
                } else if (tipoPoblacion == 'oferta_doble') {
                    OpenModal("#modalOfertaDoble");
                } else {
                    OpenModal("#modalOferta");
                }

            } else if (pantallasExtra == 1 && decision == 3) {
            // Si la respuesta a la decisión del stored procedure es 3 (Invitación
            // a continuar) y hay pantallas extras se debe mostrar el cuestionario
            // fijo
                dataLayer.push({'event': 'LeadContinua', 'userId': respuesta.prospecto_id, 'solicId': respuesta.solicitud_id, 'clientId': clientId});
                // Creando el cuestionario dinámico
                var cuestionarios = respuesta.cuestionario;
                $('#cuestionarioDinamico').append("<form action='#' id='formCuestionarioDinamico'>");
                var formulario = "";
                var situaciones = "";
                $.each(cuestionarios, function(key, cuestionario) {
                    formulario = formulario + "<div class='row'>";
                    formulario = formulario + "<div class='col-12' style='margin-bottom: 10px; margin-top: 10px;'>";
                    if (cuestionario.introduccion != null) {
                        formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4><br><div class='label'><label>" + cuestionario.introduccion + "</label></div></div>";
                    } else {
                        formulario = formulario + "<h4>" + cuestionario.encabezado + "</h4></div>";
                    }

                    $.each(cuestionario.cuestionario, function(index, pregunta) {
                        oculto = '';
                        change = '';
                        largo = 'col-12';
                        width = '100%';
                        if (pregunta.oculto == 1) {
                            oculto = 'display:none';
                        }
                        if (pregunta.grid_estilo != null) {
                            largo = pregunta.grid_estilo;
                        }
                        if (pregunta.ancho_estilo != null) {
                            width = pregunta.ancho_estilo;
                        }
                        id = cuestionario.situacion + '_' + pregunta.id;

                        formulario = formulario + "<div class='" + largo + " ' id='div_" + id + "' style='margin-bottom: 5px; " + oculto +"'>";
                        formulario = formulario + '<div class="label"><label id="label_' + id +'" class="labelDinamico">' + pregunta.pregunta + '</label></div>';

                        situaciones = situaciones + cuestionario.situacion + '|';
                        if (pregunta.depende != '') {
                            var valores = "'" + pregunta.depende + "','" + pregunta.respuesta_depende + "'";
                            change = 'onChange = "campoVisible(this.value,' + valores + ')"';
                        }
                        if (pregunta.tipo == 'text') {
                            var style = 'text-transform: uppercase; border-left: 2px solid; border-right: 2px solid; border-bottom: 2px solid; ';
                            style = style + 'border-image: linear-gradient(to top, #00843D 30%, rgba(0,0,0,0) 30%); ';
                            style = style + 'border-image-slice: 1 !important; padding-top: 22px; text-align: center; width: ' + width;
                            formulario = formulario + '<div><input type="text" id="' + id + '" name="' + id + '" style="' + style + '"/></div>';
                        }

                        if (pregunta.tipo == 'textarea') {
                            var style = 'text-transform: uppercase; padding: 0.5em; border-color: rgb(0, 132, 61); border-width: 0.15em; width: 100%';
                            formulario = formulario + '<div><textarea id="' + id + '" name="' + id + '" maxlength="255" rows="3" style="' + style + '"></textarea></div>';
                        }

                        if (pregunta.tipo == 'select') {
                            formulario = formulario + '<div><select id="' + id + '" name="' + id + '" ' + change + ' class="dinamico">';
                            formulario = formulario + '<option value="" selected>SELECCIONA</option>';
                            opciones = pregunta.opciones.split('|');
                            $.each(opciones, function(indexO, opcion) {
                                formulario = formulario + '<option value="' + opcion + '">' + opcion + '</option>';
                            });
                            formulario = formulario + '</select></div>';
                        }
                        formulario = formulario + '<label id="lerror_' + id + '" class="lerror"></label>';
                        formulario = formulario + "</div>";
                    });
                    $('#cuestionarioDinamico form').append(formulario);
                    formulario = "";
                });
                $('#cuestionarioDinamico form').append('<input type="hidden" id="situaciones" name="situaciones"/>');
                $('#cuestionarioDinamico form').append('<input type="hidden" id="prospecto_id" name="prospecto_id" value="' + respuesta.prospecto_id + '"/>');
                $('#cuestionarioDinamico form').append('<input type="hidden" id="solicitud_id" name="solicitud_id" value="' + respuesta.solicitud_id + '"/>');
                $('#cuestionarioDinamico form').append('<input type="hidden" id="tipo_poblacion" name="tipo_poblacion" value="' + respuesta.tipo_poblacion + '"/>');
                $('#situaciones').val(situaciones);
                $('#cuestionarioDinamico form').append('<br><br><div class="row"><a href="#" class="orange-btn" style="margin: 0 auto;" onclick="guardarCuestionarioDinamico()">Continuar</a></div>');

                CloseModal("#processing");
                $('#solicitud-four').hide();
                $("#pag").text('5');
                $('#cuestionarioDinamico').show();
                $("html, body").animate({ scrollTop: $("#cuestionarioDinamico").offset().top }, 1000);
                $('#closeModalStatus').show();


            } else if (decision == 2 || decision == 1) {
            // Si la respuesta a la decisión del stored procedure es 2 (Oferta
            // diferida) ó 1 (Rechazado) solo se debe mostrar el modal del status
            // de la solicitud
                $('#closeModalStatusRechazado').show();
                OpenModal("#statusSolicitud");
            }

        } else {
            CloseModal("#processing");
        }

    }).fail(function(resultado, textStatus, errorThrow) {
        CloseModal("#processing");
    })
}

/**
 * Ciera el modal de Status Solicitud y muestra el modal con la oferta
 * (Solo para Invitación a Continuar e Invitación a Continuar Cuestionario Completo)
 */
function closeModalStatus() {
    CloseModal("#statusSolicitud");
    OpenModal("#cuestionarioFijo");
}

/**
 *  Cierra el modal de Status de la Solicitud y redirige a la pantalla de Inicio
 *  (Solo para Oferta diferida y Rechazdos)
 */
function closeModalStatusRechazado() {

    CloseModal("#statusSolicitud");
    window.location.href = '/';

}

function cambioMotivoRechazo(select) {

    if(select.value == 'Otro') {
        $('#textOtroRechazo').show();
        $('#textOtroRechazo').val('');
        $('#textOtroRechazo').focus();
    } else {
        $('#textOtroRechazo').hide();
    }

}

function cambioMotivoRechazoOfertaDoble(select) {

    if(select.value == 'Otro') {
        $('#textOtroRechazoOfertaDoble').show();
        $('#textOtroRechazoOfertaDoble').val('');
        $('#textOtroRechazoOfertaDoble').focus();
    } else {
        $('#textOtroRechazoOfertaDoble').hide();
    }

}

function cancelarRechazarOferta() {

    $('#botonesOferta').show();
    $('#seccionOferta').show();
    $('#emojiOferta').show();
    $('#tituloOferta').show();

    $('#botonesRechazo').hide();
    $('#seccionRechazo').hide();
    $('#tituloRechazo').hide();
    $('#textOtroRechazo').hide();
    $("#motivo_rechazo").val('SELECCIONA');
    $('#textOtroRechazo').val('');

}

function cancelarRechazarOfertaDoble() {

    $('#botonesOfertaOfertaDoble').show();
    $('#datos_oferta').show();
    $('#seccionOferta').show();
    $('#emojiOfertaDoble').show();
    $('#tituloOfertaDoble').show();

    $('#botonesRechazoOfertaDoble').hide();
    $('#seccionRechazoOfertaDoble').hide();
    $('#tituloRechazoOfertaDoble').hide();
    $('#textOtroRechazoOfertaDoble').hide();
    $("#motivo_rechazoOfertaDoble").val('SELECCIONA');
    $('#textOtroRechazoOfertaDoble').val('');

}

/**
 * Convierte el input fechaContacto en un datetimepicker
 */
function transforma_fechaHoraContacto() {
    var date = new Date();
    var cday = date.getDay();
    var chour = date.getHours();
    var suma = 0;
    var startDate = 0;
    var endDate = 0;
    var diaExtra = 0;
    var horaInicio = 0;
    switch(cday) {
        case 0:
            startDate = new Date(date.setDate(date.getDate() + 1));
            endDate = new Date(date.setDate(date.getDate() + 1));
            break;
        case 1:
            if (chour >= 19) {
                diaExtra = 1;
                horaInicio = 8;
            } else {
                horaInicio = chour;
                suma = 60;
            }
            startDate = new Date(date.setDate(date.getDate() + diaExtra));
            endDate = new Date(date.setDate(date.getDate() + 1));
            break;
        case 2:
            if (chour >= 19) {
                diaExtra = 1;
                horaInicio = 8;
            } else {
                horaInicio = chour;
                suma = 60;
            }
            startDate = new Date(date.setDate(date.getDate() + diaExtra));
            endDate = new Date(date.setDate(date.getDate() + 1));
            break;
        case 3:
            if (chour >= 19) {
                diaExtra = 1;
                horaInicio = 8;
            } else {
                horaInicio = chour;
                suma = 60;
            }
            startDate = new Date(date.setDate(date.getDate() + diaExtra));
            endDate = new Date(date.setDate(date.getDate() + 1));
            break;
        case 4:
            if (chour >= 19) {
                diaExtra = 1;
                horaInicio = 8;
            } else {
                horaInicio = chour;
                suma = 60;
            }
            startDate = new Date(date.setDate(date.getDate() + diaExtra));
            endDate = new Date(date.setDate(date.getDate() + 1));
            break;
        case 5:
            if (chour >= 19) {
                diaExtra = 1;
                horaInicio = 8;
            } else {
                horaInicio = chour;
                suma = 60;
            }
            startDate = new Date(date.setDate(date.getDate() + diaExtra));
            endDate = new Date(date.setDate(date.getDate() + 2 ));
            break;
        case 6:
            if (chour >= 14) {
                horaInicio = 9;
                startDate = new Date(date.setDate(date.getDate() + 2));
                endDate = new Date(date.setDate(date.getDate() + 1 ));
            } else {
                horaInicio = chour;
                suma = 60;
                startDate = new Date(date.setDate(date.getDate()));
                endDate = new Date(date.setDate(date.getDate() + 2 ));
            }
            break;
    }

    var initialDate = new Date(startDate.getUTCFullYear(), startDate.getUTCMonth(), startDate.getDate(), horaInicio);
    if (suma != 0) {
        initialDate.setMinutes(date.getMinutes() + suma);
    }

    $('.fechaContacto').datetimepicker({
        initialDate: initialDate,
        startDate: initialDate,
        endDate: new Date(endDate.getUTCFullYear(), endDate.getUTCMonth(), endDate.getDate(), 23),
        daysOfWeekDisabled: [0],
        format: 'yyyy-mm-dd hh:ii',
        minView: 0,
        timezone: 'GMT-0600',
        language: 'es',
        minuteStep: 15,
        onRenderHour: function(date) {

            if (date.getDay() == 6) {
                if (date.getUTCHours() < 9 || date.getUTCHours() > 14)
                  return ['disabled'];
            } else if ( date.getDay() == 0) {
                return ['disabled'];
            } else {
                if (date.getUTCHours() < 8 && date.getUTCHours() > 19)
                  return ['disabled'];
            }
        },
        onRenderMinute: function(date) {
          if (date.getUTCHours() <= 7 || (date.getUTCHours() == 19 && date.getUTCMinutes() > 1 ) || date.getUTCHours() > 19)
              return ['disabled'];
          if (date.getDay() == 6) {
              if (date.getUTCHours() <= 8 || (date.getUTCHours() == 14 && date.getUTCMinutes() > 1) || date.getUTCHours() > 14)
                return ['disabled'];
          }
        },

    });

    $('.fechaContacto').datetimepicker('setHoursDisabled', [0,1,2,3,4,5,6,7,20,21,22,23]);
}

/**
 * Envía el pixel a AskRobin
 *
 * @param  {integer} pixel       No. del pixel que se enviará
 * @param  {integer} r_parameter Customer ID AskRobin
 * @param  {integer} t_parameter Id del prospecto prestanomico
 *
 */
function sendPixelAskRobin(pixel, r_parameter, t_parameter) {

    var url = '';
    switch (pixel) {
        case "1":
            url = 'https://robinacademia.com/p.ashx?o=115&e=10&f=pb&r='+ r_parameter + '&t=' + t_parameter;
            break;
        case "2":
            url = 'https://robinacademia.com/p.ashx?o=115&e=11&f=pb&r='+ r_parameter + '&t=' + t_parameter;
            break;
        case "3":
            url = 'https://robinacademia.com/p.ashx?o=115&e=2&f=pb&r='+ r_parameter + '&t=' + t_parameter;
            break;
    }
    $.ajax({
       url: url,
       method: 'GET',
       headers: {  'Access-Control-Allow-Origin': '*' },
       data: {},
       crossDomain: true,
       dataType: 'jsonp',
   });
}

var Forms = function() {
        function e() {
            $("form").bind("keypress", function(e) {
                if(13 == e.keyCode) return e.preventDefault(), !1
            })
        }
        return {
            start: e
        }
    }(),
    ScrollUp = function(e) {
        var o, i;
        "#first-footer" == e || "#home-simulador-header" == e ? (o = $(e).offset(), i = o.top - 70) : "#form-modificar-solicitud" == e ? (o = $(e).offset(), i = o.top - 135) : (o = $(e).offset(), i = o.top - 100), $("html, body").animate({
            scrollTop: i
        }, "slow")
    },
    PathView = function() {
        function e() {
            "/registro" == window.location.pathname ? ($("#btn-compara-deuda").hide(), $("#to-registro").children().removeClass("orange-btn"), $("#to-registro").children().addClass("blue-white-btn"), $("#to-registro").children().text("Siguiente")) : "/" == window.location.pathname && $("#btn-compara-deuda").show()
        }
        return {
            start: e
        }
    }(),
    opcionPlazo = function(e) {
        return "0" === e ? "6 meses" : "1" === e ? "12 meses" : "2" === e ? "18 meses" : "3" === e ? "12 qnas" : "4" === e ? "24 qnas" : "5" === e ? "36 qnas" : void 0
    },
    opcionFinalidad = function(e) {
        return "Bienes para el hogar" === e || "BIENES PARA EL HOGAR" === e ? "Bien hogar" : "Bienes personales" === e || "BIENES PERSONALES" === e ? "Bien personal" : "Pago tarjeta" === e || "PAGO TARJETA" === e ? "Tarjeta crédito" : "Pago préstamo personal" === e || "PAGO PRÉSTAMO PERSONAL" === e ? "Préstamo personal" : "Gastos escolares" === e || "GASTOS ESCOLARES" === e ? "Gasto Escolar" : "Vacaciones y viajes" === e || "VACACIONES Y VIAJES" === e ? "Vacaciones" : "Salud y gastos médicos" === e || "SALUD Y GASTOS MÉDICOS" === e ? "Salud" : "Inversión negocio" === e || "INVERSIÓN NEGOCIO" === e ? "Inversión negocio" : "Gastos de tu negocio" === e || "GASTOS DE TU NEGOCIO" === e ? "Gastos negocio" : "Reparación en casa" === e || "REPARACIÓN EN CASA" === e ? "Reparar casa" : "Emergencia personal" === e || "EMERGENCIA PERSONAL" === e ? "Emergencia" : "otra" === e || "OTRA" === e ? "Otra" : void 0
    },
    go_after_conf = "domicilio",
    ToRegisterSlide = function() {
        function e() {
            $("#to-registro").on("click", function(e) {

                var domains = ['hotmail.com', 'gmail.com', 'aol.com', 'live.com', 'gmail.com', 'yahoo.com', 'yahoo.com.mx', 'hotmail.es'];
            	var topLevelDomains = ["com", "net", "org", "com"];
            	$('#reg_email').on('blur', function(event) {
            	  $(this).mailcheck({
            	    domains: domains,
            	    topLevelDomains: topLevelDomains,
            	    suggested: function(element, suggestion) {
            	      $('#email-error').html("¿Quisiste decir <b><i>" + suggestion.full + "</b></i>?");
            	    },
            	    empty: function(element) {
            	      $('#email-error').html('');
            	    }
            	  });
            	});

                e.preventDefault();
                var o = !1,
                    i = "";

                var monto_minimo = parseInt($('#monto_minimo').val());
                var monto_maximo = parseInt($('#monto_maximo').val());
                var datos_faltantes = '';

                if (0 == $("#plazo").val()) {
                    datos_faltantes += '- Selecciona un Plazo. <br>';
                }

                if (null == $("#finalidad").val()) {
                    datos_faltantes += '- Selecciona la Finalidad de tu préstamo. <br>';
                }

                if ($("#prestamo").val().length == 0 || ( parseInt($("#prestamo").val()) < monto_minimo || parseInt($("#prestamo").val()) > monto_maximo)) {
                    datos_faltantes += '- El Monto del préstamo debe ser entre ' + formatoMoneda(monto_minimo) + ' a ' + formatoMoneda(monto_maximo) + '. <br>';
                }

                if ($("#finalidad_custom").val().length == 0 || $("#finalidad_custom").val().length < 10) {
                    datos_faltantes += '- El campo Nos gustaría conocerte debe contener mínimo 10 caracteres y máximo 200. <br>';
                }

                0 !== $("#prestamo").val().length && 0 != $("#plazo").val() && null != $("#finalidad").val() && 0 !== $("#finalidad_custom").val().length || (o = !0, i = datos_faltantes), parseInt($("#prestamo").val()) >= monto_minimo && parseInt($("#prestamo").val()) <= monto_maximo ? $("#prestamo-error").hide() : (o = !0, $("#prestamo-error").html("Tu préstamo sólo puede ser de " + formatoMoneda(monto_minimo) + " a " + formatoMoneda(monto_maximo) + ".").slideDown("slow"), $("#notificacion-simu").html("debes ingresar los datos.").slideDown("slow")), $("#o_finality").val().length > 0 && $('#finalidad').append( '<option value="'+ $("#o_finality").val() +'" selected>'+ $("#o_finality").val() +'</option>' ), $("#finalidad_custom").val().length >= 10 ? $("#finalidad_custom-error").hide() : (o = !0, $("#finalidad_custom-error").html("Texto de mínimo 10 caracteres y máximo 200.").slideDown("slow"), i = datos_faltantes), o && $("#notificacion-simu").html(i).slideDown("slow"), o || ($("#notificacion-simu").hide(), $("#prestamo-error").hide(), "" == $("#simulador_existing_user").val() ? ("/registro" == window.location.pathname ? $("#first-footer").slideUp() : "/" == window.location.pathname && $("#home-simulador-header").slideUp(), $("#home-simulador").slideUp(), $("#home-registro").slideDown(), openModalInicio("#confianza")):
                $.ajax({
                    method: "POST",
                    url: "/register_prospect",
                    data: $("#form-registro").serialize() + "&prestamo=" + $("#prestamo").val() + "&plazo=" + $("#plazo").val() + "&finalidad=" + $("#finalidad").val() + "&finalidad_custom=" + $("#finalidad_custom").val() + "&existing_user=" + $("#simulador_existing_user").val(),
                    beforeSend: function() {
                        OpenModal("#loading");
                    }
                }).done(function(e) {
                    res = $.parseJSON(e);
                    if (res.hasOwnProperty("nueva_solicitud")) {
                        $('#nueva_solicitud').val(res.nueva_solicitud);
                    }
                    if(CloseModal("#loading"), res = $.parseJSON(e), "OK" == res.response) "/registro" == window.location.pathname ? $("#first-footer").slideUp() : "/" == window.location.pathname && $("#home-simulador-header").slideUp(), $("#prospect_conf_id").val(res.prospect_id), $("#solic_conf_id").val(res.solic_id), $("#home-simulador").hide(), $("#verificar").toggle("slide", {
                        direction: "right"
                    }, 1e3), $("#denegado").hide(), $("#aprovado").hide(), $("#notificacion").hide(), ScrollUp("#form-modificar-solicitud");
                    else {
                        res.code && "LDAP Error code - " + res.code + " : "
                    }
                    var existing_user = $("#simulador_existing_user").val();
                    if (existing_user != "" && "OK" == res.response)  {
                        openModalInicio("#confianza");
                    }

                }).fail(function() {}), $("#form-modificar-solicitud").slideDown(), $("#data-prestamo").val(formatoMoneda($("#prestamo").val())), $("#data-plazo").val($("#plazo").find('option:selected').text()), $("#data-finalidad").html($("#finalidad").find('option:selected').text().toUpperCase()), ScrollUp("#form-modificar-solicitud"))
            })
        }
        return {
            start: e
        }

    }(),
    ReturnToSimulador = function() {
        function e() {
            $("#return-simulador").on("click", function(e) {
                e.preventDefault(), $("#home-registro").slideUp(), $("#form-modificar-solicitud").slideUp(), $("#home-simulador-header").slideDown(), $("#home-simulador").slideDown(), "/registro" == window.location.pathname ? ($("#first-footer").slideDown(), ScrollUp("#first-footer")) : "/" == window.location.pathname && ($("#home-simulador-header").slideDown(), ScrollUp("#home-simulador-header"))
            })
        }
        return {
            start: e
        }
    }(),
    ToValidateCode = function() {
        function e() {
            $("#to-verificar").on("click", function(e) {
                e.preventDefault();
                var o = ($(this), !1),
                    i = !1,
                    a = !1,
                    l = "";
                var clientId = 0;
                ga(function(tracker) {
                    clientId = tracker.get('clientId');
                });
                $('#form-registro input[type="checkbox"].required').each(function() {
                    var e = $(this);
                    "acepto_avisop" === e.attr("id") && (IsChecked(e) ? $("#acepto_avisop-error").hide() : (o = !0, $("#acepto_avisop-error").html("Debes aceptar terminos y condiciones del aviso de privacidad").slideDown("slow")))
                }),
                $('#form-registro input[type="tel"].required').each(function() {
                    var e = $(this);
                    IsEmpty(e.val()) && (a = !0, $("#notificacion").html("Todos los campos son obligatorios.").slideDown("slow")), "reg_cel" === e.attr("id") && (IsValidTel(e.val()) ? $("#tel-error").hide() : (o = !0, $("#tel-error").html("Mínimo 10 dígitos").slideDown("slow")))
                }),
                $('#form-registro input[type="text"].required').each(function() {
                    var e = $(this);
                    IsEmpty(e.val()) && (a = !0, $("#notificacion").html("Todos los campos son obligatorios.").slideDown("slow")), "reg_email" === e.attr("id") && (IsEmail(e.val()) ? $("#email-error").hide() : (o = !0, $("#email-error").html("Escribe un email válido").slideDown("slow"))), "reg_nombre" === e.attr("id") && (IsValidName(e.val()) ? $("#name-error").hide() : (o = !0, $("#name-error").html("Mínimo 2 caracteres").slideDown("slow"))), "reg_cel" === e.attr("id") && (IsValidTel(e.val()) ? $("#tel-error").hide() : (o = !0, $("#tel-error").html("Mínimo 10 dígitos").slideDown("slow")))
                }), $('#form-registro input[type="password"].required').each(function() {
                    var e = $(this);
                    0 === e.val().length && (o = !0, l = "Todos los campos son obligatorios."), "reg_password" !== e.attr("id") && "reg_password_conf" !== e.attr("id") || (IsValidPwd('#reg_password') ? (i = !1, $("#pwd-error").hide()) : (i = !0, $("#pwd-error").html("La contraseña no cumple el formato").slideDown("slow")))
                }), $("#reg_password").val() !== $("#reg_password_conf").val() && (o = !0, $("#pwd-error").html("Las contraseñas no son iguales").slideDown("slow")), o && i && a && $("#notificacion").html(l).slideDown("slow"), o || i || a || ($("#notificacion").hide(), $("#reg_nombre").val($("#reg_nombre").val().toUpperCase()), $("#reg_apellido_p").val($("#reg_apellido_p").val().toUpperCase()), $("#reg_apellido_m").val($("#reg_apellido_m").val().toUpperCase()), $("#reg_email").val($("#reg_email").val().toUpperCase()), void 0 == $.cookie("referencia") || "" == $.cookie("referencia") ? $("#referencia").val("SITIO") : $("#referencia").val($.cookie("referencia")), $.ajax({
                    method: "POST",
                    url: "/register_prospect",
                    data: $("#form-registro").serialize() + "&prestamo=" + $("#prestamo").val() + "&plazo=" + $("#plazo").val() + "&finalidad=" + $("#finalidad").val() + "&finalidad_custom=" + $("#finalidad_custom").val() + "&clientId="+clientId,
                    beforeSend: function() {
                        OpenModal("#loading")
                    }
                }).done(function(e) {

                    var res = $.parseJSON(e);
                    if ("OK" == res.response) {

                        var prospect_id = res.prospect_id;
                        ga('set', 'userId', prospect_id);
                        ga(function(tracker) {
                            tracker.set('dimension2', prospect_id);
                        });
                        var clientId = 0;
                        ga(function(tracker) {
                            clientId = tracker.get('clientId');
                        });
                        ga('send', 'pageview');

                        if (res.campaña == 'askrobin') {
                            sendPixelAskRobin('1', res.r_parameter, res.prospect_id);
                        }

                    }

                    if(CloseModal("#loading"), res = $.parseJSON(e), res.hasOwnProperty("msg")) $("#tel-error").html("Verifica tu teléfono").slideDown("slow");
                    else if("Un usuario con ese email ya existe" == res.message || "11" == res.code) $("#email-error").html("Ya existe un usuario con ese email. <a href='/registro#login' target='_self' class='link-login loginPop'>Inicia sesión</a> para continuar con tu registro.").slideDown("slow"), ScrollUp("#reg_email");
                    else if("El usuario ya existe en LDAP, pero no en el sitio de FINDEP" == res.message) $("#email-error").html("No fue posible registrarte con esta cuenta de correo, por favor intenta con una diferente.").slideDown("slow"), ScrollUp("#reg_email");
                    else if("93" == res.code) $("#email-error").html("Escribe un correo valido").slideDown("slow"), ScrollUp("#reg_email");
                    else if("OK" == res.response) dataLayer.push({'event': 'RegistroUsuario', 'userId': res.prospect_id, 'solicId': res.solic_id, 'clientId': clientId}), $("#prospect_conf_id").val(res.prospect_id), $("#solic_conf_id").val(res.solic_id), $("#numCel").html($("#reg_cel").val()), $("#home-registro").hide(), $("#verificar").toggle("slide", {
                        direction: "right"
                    }, 1e3), $("#denegado").hide(), $("#aprovado").hide(), $("#notificacion").hide(), ScrollUp("#form-modificar-solicitud");
                    else {
                        res.code && "LDAP Error code - " + res.code + " : ", OpenModal("#OtherIssues")
                    }
                }).fail(function() {}))
            })
        }
        return {
            start: e
        }
    }(),
    ValidateCode = function() {
        function e() {
            $("#verifica").on("click", function(e) {
                e.preventDefault(), "" === $("#conf_code").val() || 0 === $("#conf_code").val().length ? ($("#verifica_error").html("Debes ingresar el código").slideDown("slow"), $("#denegado").hide()) : $("#conf_code").val().length < 5 ? ($("#verifica_error").html("Código debe ser de 5 números").slideDown("slow"), $("#denegado").hide()) : $.ajax({
                    method: "POST",
                    url: "/conf_sms_code",
                    data: $("#form-verificar").serialize(),
                    beforeSend: function() {
                        OpenModal("#loading")
                    }
                }).done(function(e) {
                    CloseModal("#loading"), res = $.parseJSON(e), "OK" == res.response ? ($("#prospect_dom_id").val(res.prospect_id), $("#solic_dom_id").val(res.solic_id), $("#denegado").hide(), $("#valida-text").hide(), "domicilio" == go_after_conf ? ($("#prospect_dom_id").val(res.prospect_id), $("#solic_dom_id").val(res.solic_id), $("#verificar").hide(), $("#solicitud").show(), $("#pagination").slideDown(), $("#pag").text("1 de 4"), ScrollUp("#form-modificar-solicitud")) : "datos personales" == go_after_conf ? ($("#prospect_dp_id").val(res.prospect_id), $("#solic_dp_id").val(res.solic_id), $("#verificar").hide(), $("#solicitud-two").show(), $("#pagination").slideDown(), $("#pag").text("2 de 4"), $("#return-solicitud").hide(), ScrollUp("#form-modificar-solicitud")) : "cuentas de credito" == go_after_conf ? ($("#prospect_cc_id").val(res.prospect_id), $("#solic_cc_id").val(res.solic_id), $("#verificar").hide(), $("#solicitud-three").show(), $("#pagination").slideDown(), $("#pag").text("3 de 4"), $("#return-solicitud2").hide(), ScrollUp("#form-modificar-solicitud")) : "datos elegible" == go_after_conf ? ($("#prospect_adic_id").val(res.prospect_id), $("#solic_adic_id").val(res.solic_id), $("#datos_caso").val("Datos Elegible"), $("#verificar").hide(), $("#solicitud-four").show(), $("#pagination").slideDown(), $("#pag").text("4 de 4"), ScrollUp("#form-modificar-solicitud")) : "datos reporte" == go_after_conf ? ($("#prospect_adic_id").val(res.prospect_id), $("#solic_adic_id").val(res.solic_id), $("#datos_caso").val("Datos Reporte"), $("#verificar").hide(), $("#solicitud-four").show(), $("#pagination").slideDown(), $("#pag").text("4 de 4"), ScrollUp("#form-modificar-solicitud")) : "cuestionario dinamico" == go_after_conf ? ($("#verificar").hide(), $("#cuestionarioDinamico").show(), $("#pagination").slideDown(), $("#pag").text("5"), ScrollUp("#form-modificar-solicitud")) : "status solicitud" == go_after_conf ? ($("#verificar").hide(), $("#cuestionarioDinamico").show(), ScrollUp("#form-modificar-solicitud"), OpenModal('#' + $('#modal_visible').val())) : "cargar documentos" == go_after_conf ? ($("#verificar").hide(), OpenModal('#processing'), location.href = '/carga_documentos/paso1', ScrollUp("#form-modificar-solicitud")) : $("#to-solicitud-one").trigger("click")) : ($("#verifica_error").html("Tu código no es valido.").slideDown("slow"), $("#checa-validar").addClass("disabled-btn"), $("#checa-validar").removeClass("enabled-btn"), $("#aprovado").hide(), $("#conf_code").focus(), $("#valida-text").hide(), $("#denegado").show())
                }).fail(function() {})
            })
        }
        return {
            start: e
        }
    }(),
    ResendCode = function() {
        function e() {
            $("#resend_code_link").on("click", function(e) {
                e.preventDefault(), $.ajax({
                    method: "POST",
                    url: "/resend_code",
                    data: $("#form-verificar").serialize(),
                    beforeSend: function() {
                        OpenModal("#loading")
                    }
                }).done(function(e) {
                    CloseModal("#loading"), res = $.parseJSON(e), "OK" == res.response ? ($("#notificacion-verifica").html("Código Reenviado por SMS"), $("#notificacion-verifica").removeClass("hidden")) : ($("#error-message").html("Error reenviando el código."), $("#error-message").removeClass("hidden"))
                }).fail(function() {})
            })
        }
        return {
            start: e
        }
    }(),
    ToSolicitudOne = function() {
        function e() {
            $("#to-solicitud-one").on("click", function(e) {
                e.preventDefault();
                var o = !1,
                    i = "";
                0 === $("#conf_code").val().length && (o = !0, i = "Debes ingresar el Código"), o ? $("#notificacion-verifica").html(i).slideDown("slow") : $("#notificacion-verifica").hide(), o || ($("#verificar").hide(), $("#solicitud").toggle("slide", {
                    direction: "left"
                }, 1e3), $("#denegado").hide(), $("#aprovado").hide(), $("#valida-text").show(), $("#r-codigo").val(""), $("#pagination").slideDown(), $("#pag").text("1 de 4"), $("#notificacion-verifica").hide(), ScrollUp("#form-modificar-solicitud"))
            })
        }
        return {
            start: e
        }
    }(),
    ToSolicitudTwo = function() {
        function e() {
            $("#select_colonia").on("change", function(e) {
                var colonia = $(this).find('option:selected').text();
                $('#solic_colonia').val(colonia);
            });

            $("#to-solicitud-two").on("click", function(e) {
                e.preventDefault();
                var campos_faltan = '';
                if (0 === $("#select_colonia").val() || null == $("#select_colonia").val()) {
                    var campos_faltan = 'El campo Colonia es obligatorio';
                }
                var o = ($(this), !1),
                    i = "",
                    a = !0;
                $("#dom-error").hide(), (0 !== $("#select_colonia").val() && null != $("#select_colonia").val()) && 0 !== $("#solic_dom_calle").val().length && 0 !== $("#solic_dom_num_ext").val().length && 0 !== $("#solic_dom_cp").val().length && 0 !== $("#solic_dom_deleg").val().length && 0 !== $("#solic_dom_estado").val().length || (o = !0, i = "Todos los campos son obligatorios" + '</br>' + campos_faltan), 0 == $("#select_colonia").val() && (a = !1), a || (o = !0, i = "Todos los campos son obligatorios" + '</br>' + campos_faltan), $("#solic_dom_calle").val().length < 3 && (o = !0, $("#dom-error").html("Mínimo 3 caracteres").slideDown("slow")), $("#solic_dom_cp").val().length < 5 && (o = !0, $("#cp-error").html("Son 5 dígitos").slideDown("slow")), o ? $("#notificacion-solicitud-1").html(i).slideDown("slow") : $("#notificacion-solicitud-1").hide(), o || $.ajax({
                    method: "POST",
                    url: "/reg_domicilio",
                    data: $("#form-solicitud").serialize(),
                    beforeSend: function() {
                        OpenModal("#loading")
                    }
                }).done(function(res) {
                    CloseModal("#loading"), "OK" == res.response && ($("#prospect_dp_id").val(res.prospect_id), $("#solic_dp_id").val(res.solic_id), $("#solicitud").hide(), $("#solicitud-two").toggle("slide", {
                        direction: "right"
                    }, 1e3), $("#pag").text("2 de 4"), $("#solic_dom_calle").val($("#solic_dom_calle").val().toUpperCase()), $("#solic_dom_num_ext").val($("#solic_dom_num_ext").val().toUpperCase()), $("#solic_dom_num_int").val($("#solic_dom_num_int").val().toUpperCase()), $("#solic_colonia").val($("#solic_colonia").val().toUpperCase()), $("#solic_dom_deleg").val($("#solic_dom_deleg").val().toUpperCase()), $("#solic_dom_estado").val($("#solic_dom_estado").val().toUpperCase()), $("#solic_dom_ciudad").val($("#solic_dom_ciudad").val().toUpperCase()), ScrollUp("#form-modificar-solicitud"))
                }).fail(function() {})
            })
        }
        return {
            start: e
        }
    }(),
    ReturnToSolicitudOne = function() {
        function e() {
            $("#return-solicitud").on("click", function(e) {
                e.preventDefault(), $("#solicitud-two").hide(), $("#solicitud").toggle("slide", {
                    direction: "left"
                }, 1e3), $("#pag").text("1 de 4"), ScrollUp("#form-modificar-solicitud")
            })
        }
        return {
            start: e
        }
    }(),
    ToSolicitudThree = function() {
        function e() {

            $("#solic_lugar_nac_estado").on("change", function(e) {
                var estado = $(this).find('option:selected').text();
                $('#solic_lugar_nac_estado').val(estado);
            });

            $("#solic_lugar_nac_ciudad").on("change", function(e) {
                var ciudad = $(this).find('option:selected').text();
                $('#solic_lugar_nac_ciudad').val(ciudad);
            });

            $("#to-solicitud3").on("click", function(e) {
                e.preventDefault(), $("#solic_rfc").val($("#solic_rfc").val().toUpperCase());
                var o = ($(this), !1),
                    i = !1;
                $("#tel-casa-error").hide();
                moment.locale('es');
                var date = $('#fecha_nacimiento').combodate('getValue', 'YYYY-MM-DD');
                var m = moment(date);
                var a = "";

                var valid_rfc = validateRFC();
                 if (valid_rfc['success'] == false) {
                     // En lugar de mostrar el error del RFC, se almacena en una
                     // variable para forma
                    $('#error_rfc').val(valid_rfc['msj']);
                } else {
                    $('#error_rfc').val('');
                    $("#rfc-error").html("");
                }
                // Haciendo que la validación del RFC sea true para que salte
                // esta validación
                valid_rfc['success'] = true;

                var valid_curp = [];
                valid_curp['success'] = true;

                if ($("#solic_curp").val().length >= 1) {
                    valid_curp = validateCURP();
                    if (valid_curp['success'] == false) {
                        $("#curp-error").html(valid_curp['msj']).slideDown("slow");
                    } else {
                        $("#curp-error").html("");
                    }
                }

                var campos_faltan = '';
                if  (0 === $("#solic_sexo").val().length) {
                    campos_faltan = campos_faltan + 'El campo Sexo es obligatorio. <br>';
                }

                if  (m.isValid() == false) {
                    if ($('.year').val() == '' || $('.year').val() == null) {
                        campos_faltan = campos_faltan + 'Selecciona el Año en el campo Fecha de Nacimiento. <br>';
                    }
                    if ($('.month').val() == '' || $('.month').val() == null) {
                        campos_faltan = campos_faltan + 'Selecciona el Mes en el campo Fecha de Nacimiento. <br>';
                    }
                    if ($('.day').val() == '' || $('.day').val() == null) {
                        campos_faltan = campos_faltan + 'Selecciona el Día en el campo Fecha de Nacimiento. <br>';
                    }
                }

                if  (0 == $("#solic_lugar_nac_estado").val() || $("#solic_lugar_nac_estado").val() == null) {
                    campos_faltan = campos_faltan + 'Selecciona el Estado en el campo Lugar de nacimiento. <br>';
                }

                if  (0 == $("#solic_lugar_nac_ciudad").val() || $("#solic_lugar_nac_ciudad").val() == null) {
                    campos_faltan = campos_faltan + 'Selecciona la Ciudad en el campo Lugar de nacimiento. <br>';
                }

                if  ($("#solic_telefono_casa").val().length < 10) {
                    campos_faltan = campos_faltan + 'El campo Teléfono de casa debe contener 10 dígitos. <br>';
                }

                if  (0 === $("#solic_estado_civil").val().length) {
                    campos_faltan = campos_faltan + 'El campo Estado civl es obligatorio. <br>';
                }

                if  (0 === $("#solic_nivel_estudios").val().length) {
                    campos_faltan = campos_faltan + 'El campo Nivel de estudios es obligatorio. <br>';
                }

                /* if  ($("#solic_rfc").val().length < 10) {
                    campos_faltan = campos_faltan + 'El campo RFC es obligatorio. <br>';
                } else if  (valid_rfc['success'] == false) {
                    campos_faltan = campos_faltan + valid_rfc['msj'] + '. <br>';
                } */

                (0 === $("#solic_sexo").val().length || m.isValid() == false || valid_rfc['success'] == false || valid_curp['success'] == false || 0 == $("#solic_lugar_nac_estado").val() || 0 == $("#solic_lugar_nac_ciudad").val() || campos_faltan != '' || 0 === $("#solic_telefono_casa").val().length || 0 === $("#solic_estado_civil").val().length || 0 === $("#solic_nivel_estudios").val().length ) && (o = !0, a = campos_faltan), $("#solic_telefono_casa").val().length < 10 && (o = !0, $("#tel-casa-error").html("Mínimo 10 dígitos").slideDown("slow")), o ? $("#notificacion-solicitud-2").html(a).slideDown("slow") : $("#notificacion-solicitud-2").hide(), o || i || $.ajax({
                    method: "POST",
                    url: "/datos_personales",
                    data: $("#form-solicitud2").serialize(),
                    beforeSend: function() {
                        OpenModal("#loading")
                    }
                }).done(function(res) {
                    CloseModal("#loading"), "OK" == res.response && ($("#prospect_cc_id").val(res.prospect_id), $("#solic_cc_id").val(res.solic_id), $("#solicitud-two").hide(), $("#solicitud-three").toggle("slide", {
                        direction: "right"
                    }, 1e3), $("#pag").text("3 de 4"), $("#solic_sexo").val($("#solic_sexo").val().toUpperCase()), $("#solic_lugar_nac_estado").val($("#solic_lugar_nac_estado").val().toUpperCase()), $("#solic_lugar_nac_ciudad").val($("#solic_lugar_nac_ciudad").val().toUpperCase()), $("#solic_estado_civil").val($("#solic_estado_civil").val().toUpperCase()), $("#solic_nivel_estudios").val($("#solic_nivel_estudios").val().toUpperCase()), $("#solic_rfc").val($("#solic_rfc").val().toUpperCase()), $("#solic_curp").val($("#solic_curp").val().toUpperCase()), ScrollUp("#form-modificar-solicitud"))
                }).fail(function() {})
            })
        }
        return {
            start: e
        }
    }(),
    ReturnToSolicitudTwo = function() {
        function e() {
            $("#return-solicitud2").on("click", function(e) {
                e.preventDefault(), $("#solicitud-three").hide(), $("#solicitud-two").toggle("slide", {
                    direction: "left"
                }, 1e3), $("#pag").text("2 de 4"), ScrollUp("#form-modificar-solicitud")
            })
        }
        return {
            start: e
        }
    }(),
    ToSolicitudFour = function() {
        function e() {
            $("#to-solicitud4").on("click", function(e) {
                e.preventDefault();
                var o = !1,
                    i = "";
                var selected_hipotecario = $("input[name='solic_credito_hipotecario']:checked");
                var selected_automotriz = $("input[name='solic_credito_automotriz']:checked");
                var selected_bancario = $("input[name='solic_credito_tdc']:checked");

                selected_hipotecario.length == 0 ? ( $('#hipotecario-error').html('Debes selecionar una respuesta') ) : ( $('#hipotecario-error').html('') );
                selected_automotriz.length == 0 ? ( $('#automotriz-error').html('Debes selecionar una respuesta') ) : ( $('#automotriz-error').html('') );
                selected_bancario.length == 0 ? ( $('#bancario-error').html('Debes selecionar una respuesta') ) : ( $('#bancario-error').html('') );

                if (selected_hipotecario.length == 0 || selected_automotriz.length == 0 || selected_bancario.length == 0) {
                    $('.msg-error').css('min-height', '16px');
                    $('#notificacion-solicitud-3').html('Debes seleccionar una respuesta a todas las preguntas');
                    return 0;
                }
                if (selected_bancario.val() == 'on' && $("#solic_num_tdc").val().length < 4) {
                    $('.msg-error').css('min-height', '16px');
                    $("#tdc-error").html("Este campo es obligatorio");
                    $('#notificacion-solicitud-3').html('Debes ingresar los últimos 4 dígitos de tu tarjeta');
                    return 0;
                }

                $('.msg-error').css('min-height', '0px');
                $('#notificacion-solicitud-3').html('');
                $("#tdc-error").html();

                var clientId = 0;
                ga(function(tracker) {
                    clientId = tracker.get('clientId');
                });

                $("#solic_credito_tdcS").is(":checked") && $("#solic_num_tdc").val().length < 4 ? ($("#tdc-error").html("Este campo es obligatorio").slideDown("slow"), o = !0) : $("#tdc-error").html(""), $("input[name=solic_autorizar]:checked").length || (i = "Debes autorizar la consulta de antecedentes crediticios.", o = !0), o ? $("#notificacion-solicitud-3").html(i).slideDown("slow") : $("#notificacion-solicitud-3").hide(), o || $.ajax({
                    method: "POST",
                    url: "/cuentas_de_credito",
                    data: $("#form-solicitud3").serialize(),
                    beforeSend: function() {
                        OpenModal("#loading");
                        dataLayer.push({'event': 'AutorizaCB', 'userId': $('#prospect_cc_id').val(), 'solicId': $('#solic_cc_id').val(), 'clientId': clientId})
                    }
                }).done(function(e) {
                    res = $.parseJSON(e);
                    if (res.lead == 'askrobin') {
                        sendPixelAskRobin('2', res.lead_id, res.prospect_id);
                    }
                    CloseModal("#loading"), res = $.parseJSON(e), "OK" == res.response && ($("#prospect_bc_id").val(res.prospect_id), $("#solic_bc_id").val(res.solic_id), sendBCRequest())
                }).fail(function() {})
            })
        }
        return {
            start: e
        }
    }(),
    EndRegistro = function() {
        function e() {
            $("#end-registro").on("click", function(e) {
                e.preventDefault();
                var o = ($(this), !1);
                $("#tel-empleo-error").hide();
                var i = "";

                var campos_faltan = '';
                if  ($("#solic_tel_empleo").val().length < 10) {
                    campos_faltan = campos_faltan + 'El campo Teléfono del empleo actual debe contener 10 dígitos. <br>';
                } else {
                    campos_faltan = '';
                }
                var clientId = 0;
                ga(function(tracker) {
                	clientId = tracker.get('clientId');
                });

                0 !== $("#solic_empresa").val().length && 0 !== $("#solic_ocupacion").val().length && 0 !== $("#solic_princp_fuente_ingr").val().length && 0 !== $("#solic_ingreso_mensual").val().length && 0 !== $("#solic_antiguedad_empleo").val().length && 0 !== $("#solic_tel_empleo").val().length && 0 !== $("#solic_residencia").val().length && 0 !== $("#solic_antiguedad_domicilio").val().length && 0 !== $("#solic_gastos_mensuales").val().length && 0 !== $("#solic_num_dependientes").val().length || (o = !0, i =  "Todos los campos son obligatorios"), $("#solic_tel_empleo").val().length < 10 && (o = !0, $("#tel-empleo-error").html("Mínimo 10 dígitos").slideDown("slow")), o ? $("#notificacion-solicitud-4").html(campos_faltan + i).slideDown("slow") : $("#notificacion-solicitud-4").hide(), o || ($("#solic_ocupacion").val($("#solic_ocupacion").val().toUpperCase()), $("#solic_princp_fuente_ingr").val($("#solic_princp_fuente_ingr").val().toUpperCase()), $("#solic_antiguedad_empleo").val($("#solic_antiguedad_empleo").val().toUpperCase()), $("#solic_residencia").val($("#solic_residencia").val().toUpperCase()), $("#solic_antiguedad_domicilio").val($("#solic_antiguedad_domicilio").val().toUpperCase()), $("#solic_num_dependientes").val($("#solic_num_dependientes").val().toUpperCase()), $.ajax({
                    method: "POST",
                    url: "/datos_adicionales",
                    data: $("#form-solicitud4").serialize(),
                    beforeSend: function() {
                        OpenModal("#loading")
                    }
                }).done(function(e) {
                    CloseModal("#loading"), res = e,
                    "Elegible" == res.stat && ($('#prospect_hawk_id').val(res.prospect_id), $('#solic_hawk_id').val(res.solic_id), $('#es_elegible_hawk').val(res.elegible), dataLayer.push({'event': 'Finalizan4', 'userId': res.prospect_id, 'solicId': res.solic_id, 'clientId': clientId}), sendReportRequest()),
                    "No Elegible" == res.stat && (dataLayer.push({'event': 'Finalizan4', 'userId': res.prospect_id, 'solicId': res.solic_id, 'clientId': clientId}), OpenModal("#gracias")),
                    "Datos Adicionales OK" == res.stat && ($("#prospect_alp_id").val(res.prospect_id), $("#solic_alp_id").val(res.solic_id), dataLayer.push({'event': 'Finalizan4', 'userId': res.prospect_id, 'solicId': res.solic_id, 'clientId': clientId}), sendALPRequest())
                }).fail(function() {}))
            })
        }
        return {
            start: e
        }
    }();
$(function() {
    Forms.start(), PathView.start(), ToRegisterSlide.start(), ReturnToSimulador.start(), ToValidateCode.start(), ValidateCode.start(), ResendCode.start(), ToSolicitudOne.start(), ToSolicitudTwo.start(), ReturnToSolicitudOne.start(), ToSolicitudThree.start(), ReturnToSolicitudTwo.start(), ToSolicitudFour.start(), EndRegistro.start()
});
