var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*! jQuery v3.2.1 -ajax,-ajax/jsonp,-ajax/load,-ajax/parseXML,-ajax/script,-ajax/var/location,-ajax/var/nonce,-ajax/var/rquery,-ajax/xhr,-manipulation/_evalUrl,-event/ajax,-effects,-effects/Tween,-effects/animatedSelector | (c) JS Foundation and other contributors | jquery.org/license */
!function (a, b) {
	"use strict";
	"object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && "object" == _typeof(module.exports) ? module.exports = a.document ? b(a, !0) : function (a) {
		if (!a.document) throw new Error("jQuery requires a window with a document");return b(a);
	} : b(a);
}("undefined" != typeof window ? window : this, function (a, b) {
	"use strict";
	var c = [],
	    d = a.document,
	    e = Object.getPrototypeOf,
	    f = c.slice,
	    g = c.concat,
	    h = c.push,
	    i = c.indexOf,
	    j = {},
	    k = j.toString,
	    l = j.hasOwnProperty,
	    m = l.toString,
	    n = m.call(Object),
	    o = {};function p(a, b) {
		b = b || d;var c = b.createElement("script");c.text = a, b.head.appendChild(c).parentNode.removeChild(c);
	}var q = "3.2.1 -ajax,-ajax/jsonp,-ajax/load,-ajax/parseXML,-ajax/script,-ajax/var/location,-ajax/var/nonce,-ajax/var/rquery,-ajax/xhr,-manipulation/_evalUrl,-event/ajax,-effects,-effects/Tween,-effects/animatedSelector",
	    r = function r(a, b) {
		return new r.fn.init(a, b);
	},
	    s = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
	    t = /^-ms-/,
	    u = /-([a-z])/g,
	    v = function v(a, b) {
		return b.toUpperCase();
	};r.fn = r.prototype = { jquery: q, constructor: r, length: 0, toArray: function toArray() {
			return f.call(this);
		}, get: function get(a) {
			return null == a ? f.call(this) : a < 0 ? this[a + this.length] : this[a];
		}, pushStack: function pushStack(a) {
			var b = r.merge(this.constructor(), a);return b.prevObject = this, b;
		}, each: function each(a) {
			return r.each(this, a);
		}, map: function map(a) {
			return this.pushStack(r.map(this, function (b, c) {
				return a.call(b, c, b);
			}));
		}, slice: function slice() {
			return this.pushStack(f.apply(this, arguments));
		}, first: function first() {
			return this.eq(0);
		}, last: function last() {
			return this.eq(-1);
		}, eq: function eq(a) {
			var b = this.length,
			    c = +a + (a < 0 ? b : 0);return this.pushStack(c >= 0 && c < b ? [this[c]] : []);
		}, end: function end() {
			return this.prevObject || this.constructor();
		}, push: h, sort: c.sort, splice: c.splice }, r.extend = r.fn.extend = function () {
		var a,
		    b,
		    c,
		    d,
		    e,
		    f,
		    g = arguments[0] || {},
		    h = 1,
		    i = arguments.length,
		    j = !1;for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == (typeof g === "undefined" ? "undefined" : _typeof(g)) || r.isFunction(g) || (g = {}), h === i && (g = this, h--); h < i; h++) {
			if (null != (a = arguments[h])) for (b in a) {
				c = g[b], d = a[b], g !== d && (j && d && (r.isPlainObject(d) || (e = Array.isArray(d))) ? (e ? (e = !1, f = c && Array.isArray(c) ? c : []) : f = c && r.isPlainObject(c) ? c : {}, g[b] = r.extend(j, f, d)) : void 0 !== d && (g[b] = d));
			}
		}return g;
	}, r.extend({ expando: "jQuery" + (q + Math.random()).replace(/\D/g, ""), isReady: !0, error: function error(a) {
			throw new Error(a);
		}, noop: function noop() {}, isFunction: function isFunction(a) {
			return "function" === r.type(a);
		}, isWindow: function isWindow(a) {
			return null != a && a === a.window;
		}, isNumeric: function isNumeric(a) {
			var b = r.type(a);return ("number" === b || "string" === b) && !isNaN(a - parseFloat(a));
		}, isPlainObject: function isPlainObject(a) {
			var b, c;return !(!a || "[object Object]" !== k.call(a)) && (!(b = e(a)) || (c = l.call(b, "constructor") && b.constructor, "function" == typeof c && m.call(c) === n));
		}, isEmptyObject: function isEmptyObject(a) {
			var b;for (b in a) {
				return !1;
			}return !0;
		}, type: function type(a) {
			return null == a ? a + "" : "object" == (typeof a === "undefined" ? "undefined" : _typeof(a)) || "function" == typeof a ? j[k.call(a)] || "object" : typeof a === "undefined" ? "undefined" : _typeof(a);
		}, globalEval: function globalEval(a) {
			p(a);
		}, camelCase: function camelCase(a) {
			return a.replace(t, "ms-").replace(u, v);
		}, each: function each(a, b) {
			var c,
			    d = 0;if (w(a)) {
				for (c = a.length; d < c; d++) {
					if (b.call(a[d], d, a[d]) === !1) break;
				}
			} else for (d in a) {
				if (b.call(a[d], d, a[d]) === !1) break;
			}return a;
		}, trim: function trim(a) {
			return null == a ? "" : (a + "").replace(s, "");
		}, makeArray: function makeArray(a, b) {
			var c = b || [];return null != a && (w(Object(a)) ? r.merge(c, "string" == typeof a ? [a] : a) : h.call(c, a)), c;
		}, inArray: function inArray(a, b, c) {
			return null == b ? -1 : i.call(b, a, c);
		}, merge: function merge(a, b) {
			for (var c = +b.length, d = 0, e = a.length; d < c; d++) {
				a[e++] = b[d];
			}return a.length = e, a;
		}, grep: function grep(a, b, c) {
			for (var d, e = [], f = 0, g = a.length, h = !c; f < g; f++) {
				d = !b(a[f], f), d !== h && e.push(a[f]);
			}return e;
		}, map: function map(a, b, c) {
			var d,
			    e,
			    f = 0,
			    h = [];if (w(a)) for (d = a.length; f < d; f++) {
				e = b(a[f], f, c), null != e && h.push(e);
			} else for (f in a) {
				e = b(a[f], f, c), null != e && h.push(e);
			}return g.apply([], h);
		}, guid: 1, proxy: function proxy(a, b) {
			var c, d, e;if ("string" == typeof b && (c = a[b], b = a, a = c), r.isFunction(a)) return d = f.call(arguments, 2), e = function e() {
				return a.apply(b || this, d.concat(f.call(arguments)));
			}, e.guid = a.guid = a.guid || r.guid++, e;
		}, now: Date.now, support: o }), "function" == typeof Symbol && (r.fn[Symbol.iterator] = c[Symbol.iterator]), r.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (a, b) {
		j["[object " + b + "]"] = b.toLowerCase();
	});function w(a) {
		var b = !!a && "length" in a && a.length,
		    c = r.type(a);return "function" !== c && !r.isWindow(a) && ("array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a);
	}var x = function (a) {
		var b,
		    c,
		    d,
		    e,
		    f,
		    g,
		    h,
		    i,
		    j,
		    k,
		    l,
		    m,
		    n,
		    o,
		    p,
		    q,
		    r,
		    s,
		    t,
		    u = "sizzle" + 1 * new Date(),
		    v = a.document,
		    w = 0,
		    x = 0,
		    y = ha(),
		    z = ha(),
		    A = ha(),
		    B = function B(a, b) {
			return a === b && (l = !0), 0;
		},
		    C = {}.hasOwnProperty,
		    D = [],
		    E = D.pop,
		    F = D.push,
		    G = D.push,
		    H = D.slice,
		    I = function I(a, b) {
			for (var c = 0, d = a.length; c < d; c++) {
				if (a[c] === b) return c;
			}return -1;
		},
		    J = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
		    K = "[\\x20\\t\\r\\n\\f]",
		    L = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
		    M = "\\[" + K + "*(" + L + ")(?:" + K + "*([*^$|!~]?=)" + K + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + L + "))|)" + K + "*\\]",
		    N = ":(" + L + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + M + ")*)|.*)\\)|)",
		    O = new RegExp(K + "+", "g"),
		    P = new RegExp("^" + K + "+|((?:^|[^\\\\])(?:\\\\.)*)" + K + "+$", "g"),
		    Q = new RegExp("^" + K + "*," + K + "*"),
		    R = new RegExp("^" + K + "*([>+~]|" + K + ")" + K + "*"),
		    S = new RegExp("=" + K + "*([^\\]'\"]*?)" + K + "*\\]", "g"),
		    T = new RegExp(N),
		    U = new RegExp("^" + L + "$"),
		    V = { ID: new RegExp("^#(" + L + ")"), CLASS: new RegExp("^\\.(" + L + ")"), TAG: new RegExp("^(" + L + "|[*])"), ATTR: new RegExp("^" + M), PSEUDO: new RegExp("^" + N), CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + K + "*(even|odd|(([+-]|)(\\d*)n|)" + K + "*(?:([+-]|)" + K + "*(\\d+)|))" + K + "*\\)|)", "i"), bool: new RegExp("^(?:" + J + ")$", "i"), needsContext: new RegExp("^" + K + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + K + "*((?:-\\d)?\\d*)" + K + "*\\)|)(?=[^-]|$)", "i") },
		    W = /^(?:input|select|textarea|button)$/i,
		    X = /^h\d$/i,
		    Y = /^[^{]+\{\s*\[native \w/,
		    Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
		    $ = /[+~]/,
		    _ = new RegExp("\\\\([\\da-f]{1,6}" + K + "?|(" + K + ")|.)", "ig"),
		    aa = function aa(a, b, c) {
			var d = "0x" + b - 65536;return d !== d || c ? b : d < 0 ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320);
		},
		    ba = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
		    ca = function ca(a, b) {
			return b ? "\0" === a ? "\uFFFD" : a.slice(0, -1) + "\\" + a.charCodeAt(a.length - 1).toString(16) + " " : "\\" + a;
		},
		    da = function da() {
			m();
		},
		    ea = ta(function (a) {
			return a.disabled === !0 && ("form" in a || "label" in a);
		}, { dir: "parentNode", next: "legend" });try {
			G.apply(D = H.call(v.childNodes), v.childNodes), D[v.childNodes.length].nodeType;
		} catch (fa) {
			G = { apply: D.length ? function (a, b) {
					F.apply(a, H.call(b));
				} : function (a, b) {
					var c = a.length,
					    d = 0;while (a[c++] = b[d++]) {}a.length = c - 1;
				} };
		}function ga(a, b, d, e) {
			var f,
			    h,
			    j,
			    k,
			    l,
			    o,
			    r,
			    s = b && b.ownerDocument,
			    w = b ? b.nodeType : 9;if (d = d || [], "string" != typeof a || !a || 1 !== w && 9 !== w && 11 !== w) return d;if (!e && ((b ? b.ownerDocument || b : v) !== n && m(b), b = b || n, p)) {
				if (11 !== w && (l = Z.exec(a))) if (f = l[1]) {
					if (9 === w) {
						if (!(j = b.getElementById(f))) return d;if (j.id === f) return d.push(j), d;
					} else if (s && (j = s.getElementById(f)) && t(b, j) && j.id === f) return d.push(j), d;
				} else {
					if (l[2]) return G.apply(d, b.getElementsByTagName(a)), d;if ((f = l[3]) && c.getElementsByClassName && b.getElementsByClassName) return G.apply(d, b.getElementsByClassName(f)), d;
				}if (c.qsa && !A[a + " "] && (!q || !q.test(a))) {
					if (1 !== w) s = b, r = a;else if ("object" !== b.nodeName.toLowerCase()) {
						(k = b.getAttribute("id")) ? k = k.replace(ba, ca) : b.setAttribute("id", k = u), o = g(a), h = o.length;while (h--) {
							o[h] = "#" + k + " " + sa(o[h]);
						}r = o.join(","), s = $.test(a) && qa(b.parentNode) || b;
					}if (r) try {
						return G.apply(d, s.querySelectorAll(r)), d;
					} catch (x) {} finally {
						k === u && b.removeAttribute("id");
					}
				}
			}return i(a.replace(P, "$1"), b, d, e);
		}function ha() {
			var a = [];function b(c, e) {
				return a.push(c + " ") > d.cacheLength && delete b[a.shift()], b[c + " "] = e;
			}return b;
		}function ia(a) {
			return a[u] = !0, a;
		}function ja(a) {
			var b = n.createElement("fieldset");try {
				return !!a(b);
			} catch (c) {
				return !1;
			} finally {
				b.parentNode && b.parentNode.removeChild(b), b = null;
			}
		}function ka(a, b) {
			var c = a.split("|"),
			    e = c.length;while (e--) {
				d.attrHandle[c[e]] = b;
			}
		}function la(a, b) {
			var c = b && a,
			    d = c && 1 === a.nodeType && 1 === b.nodeType && a.sourceIndex - b.sourceIndex;if (d) return d;if (c) while (c = c.nextSibling) {
				if (c === b) return -1;
			}return a ? 1 : -1;
		}function ma(a) {
			return function (b) {
				var c = b.nodeName.toLowerCase();return "input" === c && b.type === a;
			};
		}function na(a) {
			return function (b) {
				var c = b.nodeName.toLowerCase();return ("input" === c || "button" === c) && b.type === a;
			};
		}function oa(a) {
			return function (b) {
				return "form" in b ? b.parentNode && b.disabled === !1 ? "label" in b ? "label" in b.parentNode ? b.parentNode.disabled === a : b.disabled === a : b.isDisabled === a || b.isDisabled !== !a && ea(b) === a : b.disabled === a : "label" in b && b.disabled === a;
			};
		}function pa(a) {
			return ia(function (b) {
				return b = +b, ia(function (c, d) {
					var e,
					    f = a([], c.length, b),
					    g = f.length;while (g--) {
						c[e = f[g]] && (c[e] = !(d[e] = c[e]));
					}
				});
			});
		}function qa(a) {
			return a && "undefined" != typeof a.getElementsByTagName && a;
		}c = ga.support = {}, f = ga.isXML = function (a) {
			var b = a && (a.ownerDocument || a).documentElement;return !!b && "HTML" !== b.nodeName;
		}, m = ga.setDocument = function (a) {
			var b,
			    e,
			    g = a ? a.ownerDocument || a : v;return g !== n && 9 === g.nodeType && g.documentElement ? (n = g, o = n.documentElement, p = !f(n), v !== n && (e = n.defaultView) && e.top !== e && (e.addEventListener ? e.addEventListener("unload", da, !1) : e.attachEvent && e.attachEvent("onunload", da)), c.attributes = ja(function (a) {
				return a.className = "i", !a.getAttribute("className");
			}), c.getElementsByTagName = ja(function (a) {
				return a.appendChild(n.createComment("")), !a.getElementsByTagName("*").length;
			}), c.getElementsByClassName = Y.test(n.getElementsByClassName), c.getById = ja(function (a) {
				return o.appendChild(a).id = u, !n.getElementsByName || !n.getElementsByName(u).length;
			}), c.getById ? (d.filter.ID = function (a) {
				var b = a.replace(_, aa);return function (a) {
					return a.getAttribute("id") === b;
				};
			}, d.find.ID = function (a, b) {
				if ("undefined" != typeof b.getElementById && p) {
					var c = b.getElementById(a);return c ? [c] : [];
				}
			}) : (d.filter.ID = function (a) {
				var b = a.replace(_, aa);return function (a) {
					var c = "undefined" != typeof a.getAttributeNode && a.getAttributeNode("id");return c && c.value === b;
				};
			}, d.find.ID = function (a, b) {
				if ("undefined" != typeof b.getElementById && p) {
					var c,
					    d,
					    e,
					    f = b.getElementById(a);if (f) {
						if (c = f.getAttributeNode("id"), c && c.value === a) return [f];e = b.getElementsByName(a), d = 0;while (f = e[d++]) {
							if (c = f.getAttributeNode("id"), c && c.value === a) return [f];
						}
					}return [];
				}
			}), d.find.TAG = c.getElementsByTagName ? function (a, b) {
				return "undefined" != typeof b.getElementsByTagName ? b.getElementsByTagName(a) : c.qsa ? b.querySelectorAll(a) : void 0;
			} : function (a, b) {
				var c,
				    d = [],
				    e = 0,
				    f = b.getElementsByTagName(a);if ("*" === a) {
					while (c = f[e++]) {
						1 === c.nodeType && d.push(c);
					}return d;
				}return f;
			}, d.find.CLASS = c.getElementsByClassName && function (a, b) {
				if ("undefined" != typeof b.getElementsByClassName && p) return b.getElementsByClassName(a);
			}, r = [], q = [], (c.qsa = Y.test(n.querySelectorAll)) && (ja(function (a) {
				o.appendChild(a).innerHTML = "<a id='" + u + "'></a><select id='" + u + "-\r\\' msallowcapture=''><option selected=''></option></select>", a.querySelectorAll("[msallowcapture^='']").length && q.push("[*^$]=" + K + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || q.push("\\[" + K + "*(?:value|" + J + ")"), a.querySelectorAll("[id~=" + u + "-]").length || q.push("~="), a.querySelectorAll(":checked").length || q.push(":checked"), a.querySelectorAll("a#" + u + "+*").length || q.push(".#.+[+~]");
			}), ja(function (a) {
				a.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";var b = n.createElement("input");b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && q.push("name" + K + "*[*^$|!~]?="), 2 !== a.querySelectorAll(":enabled").length && q.push(":enabled", ":disabled"), o.appendChild(a).disabled = !0, 2 !== a.querySelectorAll(":disabled").length && q.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), q.push(",.*:");
			})), (c.matchesSelector = Y.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) && ja(function (a) {
				c.disconnectedMatch = s.call(a, "*"), s.call(a, "[s!='']:x"), r.push("!=", N);
			}), q = q.length && new RegExp(q.join("|")), r = r.length && new RegExp(r.join("|")), b = Y.test(o.compareDocumentPosition), t = b || Y.test(o.contains) ? function (a, b) {
				var c = 9 === a.nodeType ? a.documentElement : a,
				    d = b && b.parentNode;return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)));
			} : function (a, b) {
				if (b) while (b = b.parentNode) {
					if (b === a) return !0;
				}return !1;
			}, B = b ? function (a, b) {
				if (a === b) return l = !0, 0;var d = !a.compareDocumentPosition - !b.compareDocumentPosition;return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !c.sortDetached && b.compareDocumentPosition(a) === d ? a === n || a.ownerDocument === v && t(v, a) ? -1 : b === n || b.ownerDocument === v && t(v, b) ? 1 : k ? I(k, a) - I(k, b) : 0 : 4 & d ? -1 : 1);
			} : function (a, b) {
				if (a === b) return l = !0, 0;var c,
				    d = 0,
				    e = a.parentNode,
				    f = b.parentNode,
				    g = [a],
				    h = [b];if (!e || !f) return a === n ? -1 : b === n ? 1 : e ? -1 : f ? 1 : k ? I(k, a) - I(k, b) : 0;if (e === f) return la(a, b);c = a;while (c = c.parentNode) {
					g.unshift(c);
				}c = b;while (c = c.parentNode) {
					h.unshift(c);
				}while (g[d] === h[d]) {
					d++;
				}return d ? la(g[d], h[d]) : g[d] === v ? -1 : h[d] === v ? 1 : 0;
			}, n) : n;
		}, ga.matches = function (a, b) {
			return ga(a, null, null, b);
		}, ga.matchesSelector = function (a, b) {
			if ((a.ownerDocument || a) !== n && m(a), b = b.replace(S, "='$1']"), c.matchesSelector && p && !A[b + " "] && (!r || !r.test(b)) && (!q || !q.test(b))) try {
				var d = s.call(a, b);if (d || c.disconnectedMatch || a.document && 11 !== a.document.nodeType) return d;
			} catch (e) {}return ga(b, n, null, [a]).length > 0;
		}, ga.contains = function (a, b) {
			return (a.ownerDocument || a) !== n && m(a), t(a, b);
		}, ga.attr = function (a, b) {
			(a.ownerDocument || a) !== n && m(a);var e = d.attrHandle[b.toLowerCase()],
			    f = e && C.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0;return void 0 !== f ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null;
		}, ga.escape = function (a) {
			return (a + "").replace(ba, ca);
		}, ga.error = function (a) {
			throw new Error("Syntax error, unrecognized expression: " + a);
		}, ga.uniqueSort = function (a) {
			var b,
			    d = [],
			    e = 0,
			    f = 0;if (l = !c.detectDuplicates, k = !c.sortStable && a.slice(0), a.sort(B), l) {
				while (b = a[f++]) {
					b === a[f] && (e = d.push(f));
				}while (e--) {
					a.splice(d[e], 1);
				}
			}return k = null, a;
		}, e = ga.getText = function (a) {
			var b,
			    c = "",
			    d = 0,
			    f = a.nodeType;if (f) {
				if (1 === f || 9 === f || 11 === f) {
					if ("string" == typeof a.textContent) return a.textContent;for (a = a.firstChild; a; a = a.nextSibling) {
						c += e(a);
					}
				} else if (3 === f || 4 === f) return a.nodeValue;
			} else while (b = a[d++]) {
				c += e(b);
			}return c;
		}, d = ga.selectors = { cacheLength: 50, createPseudo: ia, match: V, attrHandle: {}, find: {}, relative: { ">": { dir: "parentNode", first: !0 }, " ": { dir: "parentNode" }, "+": { dir: "previousSibling", first: !0 }, "~": { dir: "previousSibling" } }, preFilter: { ATTR: function ATTR(a) {
					return a[1] = a[1].replace(_, aa), a[3] = (a[3] || a[4] || a[5] || "").replace(_, aa), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4);
				}, CHILD: function CHILD(a) {
					return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || ga.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && ga.error(a[0]), a;
				}, PSEUDO: function PSEUDO(a) {
					var b,
					    c = !a[6] && a[2];return V.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && T.test(c) && (b = g(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3));
				} }, filter: { TAG: function TAG(a) {
					var b = a.replace(_, aa).toLowerCase();return "*" === a ? function () {
						return !0;
					} : function (a) {
						return a.nodeName && a.nodeName.toLowerCase() === b;
					};
				}, CLASS: function CLASS(a) {
					var b = y[a + " "];return b || (b = new RegExp("(^|" + K + ")" + a + "(" + K + "|$)")) && y(a, function (a) {
						return b.test("string" == typeof a.className && a.className || "undefined" != typeof a.getAttribute && a.getAttribute("class") || "");
					});
				}, ATTR: function ATTR(a, b, c) {
					return function (d) {
						var e = ga.attr(d, a);return null == e ? "!=" === b : !b || (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf(c) : "*=" === b ? c && e.indexOf(c) > -1 : "$=" === b ? c && e.slice(-c.length) === c : "~=" === b ? (" " + e.replace(O, " ") + " ").indexOf(c) > -1 : "|=" === b && (e === c || e.slice(0, c.length + 1) === c + "-"));
					};
				}, CHILD: function CHILD(a, b, c, d, e) {
					var f = "nth" !== a.slice(0, 3),
					    g = "last" !== a.slice(-4),
					    h = "of-type" === b;return 1 === d && 0 === e ? function (a) {
						return !!a.parentNode;
					} : function (b, c, i) {
						var j,
						    k,
						    l,
						    m,
						    n,
						    o,
						    p = f !== g ? "nextSibling" : "previousSibling",
						    q = b.parentNode,
						    r = h && b.nodeName.toLowerCase(),
						    s = !i && !h,
						    t = !1;if (q) {
							if (f) {
								while (p) {
									m = b;while (m = m[p]) {
										if (h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) return !1;
									}o = p = "only" === a && !o && "nextSibling";
								}return !0;
							}if (o = [g ? q.firstChild : q.lastChild], g && s) {
								m = q, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n && j[2], m = n && q.childNodes[n];while (m = ++n && m && m[p] || (t = n = 0) || o.pop()) {
									if (1 === m.nodeType && ++t && m === b) {
										k[a] = [w, n, t];break;
									}
								}
							} else if (s && (m = b, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n), t === !1) while (m = ++n && m && m[p] || (t = n = 0) || o.pop()) {
								if ((h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) && ++t && (s && (l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), k[a] = [w, t]), m === b)) break;
							}return t -= e, t === d || t % d === 0 && t / d >= 0;
						}
					};
				}, PSEUDO: function PSEUDO(a, b) {
					var c,
					    e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || ga.error("unsupported pseudo: " + a);return e[u] ? e(b) : e.length > 1 ? (c = [a, a, "", b], d.setFilters.hasOwnProperty(a.toLowerCase()) ? ia(function (a, c) {
						var d,
						    f = e(a, b),
						    g = f.length;while (g--) {
							d = I(a, f[g]), a[d] = !(c[d] = f[g]);
						}
					}) : function (a) {
						return e(a, 0, c);
					}) : e;
				} }, pseudos: { not: ia(function (a) {
					var b = [],
					    c = [],
					    d = h(a.replace(P, "$1"));return d[u] ? ia(function (a, b, c, e) {
						var f,
						    g = d(a, null, e, []),
						    h = a.length;while (h--) {
							(f = g[h]) && (a[h] = !(b[h] = f));
						}
					}) : function (a, e, f) {
						return b[0] = a, d(b, null, f, c), b[0] = null, !c.pop();
					};
				}), has: ia(function (a) {
					return function (b) {
						return ga(a, b).length > 0;
					};
				}), contains: ia(function (a) {
					return a = a.replace(_, aa), function (b) {
						return (b.textContent || b.innerText || e(b)).indexOf(a) > -1;
					};
				}), lang: ia(function (a) {
					return U.test(a || "") || ga.error("unsupported lang: " + a), a = a.replace(_, aa).toLowerCase(), function (b) {
						var c;do {
							if (c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-");
						} while ((b = b.parentNode) && 1 === b.nodeType);return !1;
					};
				}), target: function target(b) {
					var c = a.location && a.location.hash;return c && c.slice(1) === b.id;
				}, root: function root(a) {
					return a === o;
				}, focus: function focus(a) {
					return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex);
				}, enabled: oa(!1), disabled: oa(!0), checked: function checked(a) {
					var b = a.nodeName.toLowerCase();return "input" === b && !!a.checked || "option" === b && !!a.selected;
				}, selected: function selected(a) {
					return a.parentNode && a.parentNode.selectedIndex, a.selected === !0;
				}, empty: function empty(a) {
					for (a = a.firstChild; a; a = a.nextSibling) {
						if (a.nodeType < 6) return !1;
					}return !0;
				}, parent: function parent(a) {
					return !d.pseudos.empty(a);
				}, header: function header(a) {
					return X.test(a.nodeName);
				}, input: function input(a) {
					return W.test(a.nodeName);
				}, button: function button(a) {
					var b = a.nodeName.toLowerCase();return "input" === b && "button" === a.type || "button" === b;
				}, text: function text(a) {
					var b;return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase());
				}, first: pa(function () {
					return [0];
				}), last: pa(function (a, b) {
					return [b - 1];
				}), eq: pa(function (a, b, c) {
					return [c < 0 ? c + b : c];
				}), even: pa(function (a, b) {
					for (var c = 0; c < b; c += 2) {
						a.push(c);
					}return a;
				}), odd: pa(function (a, b) {
					for (var c = 1; c < b; c += 2) {
						a.push(c);
					}return a;
				}), lt: pa(function (a, b, c) {
					for (var d = c < 0 ? c + b : c; --d >= 0;) {
						a.push(d);
					}return a;
				}), gt: pa(function (a, b, c) {
					for (var d = c < 0 ? c + b : c; ++d < b;) {
						a.push(d);
					}return a;
				}) } }, d.pseudos.nth = d.pseudos.eq;for (b in { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }) {
			d.pseudos[b] = ma(b);
		}for (b in { submit: !0, reset: !0 }) {
			d.pseudos[b] = na(b);
		}function ra() {}ra.prototype = d.filters = d.pseudos, d.setFilters = new ra(), g = ga.tokenize = function (a, b) {
			var c,
			    e,
			    f,
			    g,
			    h,
			    i,
			    j,
			    k = z[a + " "];if (k) return b ? 0 : k.slice(0);h = a, i = [], j = d.preFilter;while (h) {
				c && !(e = Q.exec(h)) || (e && (h = h.slice(e[0].length) || h), i.push(f = [])), c = !1, (e = R.exec(h)) && (c = e.shift(), f.push({ value: c, type: e[0].replace(P, " ") }), h = h.slice(c.length));for (g in d.filter) {
					!(e = V[g].exec(h)) || j[g] && !(e = j[g](e)) || (c = e.shift(), f.push({ value: c, type: g, matches: e }), h = h.slice(c.length));
				}if (!c) break;
			}return b ? h.length : h ? ga.error(a) : z(a, i).slice(0);
		};function sa(a) {
			for (var b = 0, c = a.length, d = ""; b < c; b++) {
				d += a[b].value;
			}return d;
		}function ta(a, b, c) {
			var d = b.dir,
			    e = b.next,
			    f = e || d,
			    g = c && "parentNode" === f,
			    h = x++;return b.first ? function (b, c, e) {
				while (b = b[d]) {
					if (1 === b.nodeType || g) return a(b, c, e);
				}return !1;
			} : function (b, c, i) {
				var j,
				    k,
				    l,
				    m = [w, h];if (i) {
					while (b = b[d]) {
						if ((1 === b.nodeType || g) && a(b, c, i)) return !0;
					}
				} else while (b = b[d]) {
					if (1 === b.nodeType || g) if (l = b[u] || (b[u] = {}), k = l[b.uniqueID] || (l[b.uniqueID] = {}), e && e === b.nodeName.toLowerCase()) b = b[d] || b;else {
						if ((j = k[f]) && j[0] === w && j[1] === h) return m[2] = j[2];if (k[f] = m, m[2] = a(b, c, i)) return !0;
					}
				}return !1;
			};
		}function ua(a) {
			return a.length > 1 ? function (b, c, d) {
				var e = a.length;while (e--) {
					if (!a[e](b, c, d)) return !1;
				}return !0;
			} : a[0];
		}function va(a, b, c) {
			for (var d = 0, e = b.length; d < e; d++) {
				ga(a, b[d], c);
			}return c;
		}function wa(a, b, c, d, e) {
			for (var f, g = [], h = 0, i = a.length, j = null != b; h < i; h++) {
				(f = a[h]) && (c && !c(f, d, e) || (g.push(f), j && b.push(h)));
			}return g;
		}function xa(a, b, c, d, e, f) {
			return d && !d[u] && (d = xa(d)), e && !e[u] && (e = xa(e, f)), ia(function (f, g, h, i) {
				var j,
				    k,
				    l,
				    m = [],
				    n = [],
				    o = g.length,
				    p = f || va(b || "*", h.nodeType ? [h] : h, []),
				    q = !a || !f && b ? p : wa(p, m, a, h, i),
				    r = c ? e || (f ? a : o || d) ? [] : g : q;if (c && c(q, r, h, i), d) {
					j = wa(r, n), d(j, [], h, i), k = j.length;while (k--) {
						(l = j[k]) && (r[n[k]] = !(q[n[k]] = l));
					}
				}if (f) {
					if (e || a) {
						if (e) {
							j = [], k = r.length;while (k--) {
								(l = r[k]) && j.push(q[k] = l);
							}e(null, r = [], j, i);
						}k = r.length;while (k--) {
							(l = r[k]) && (j = e ? I(f, l) : m[k]) > -1 && (f[j] = !(g[j] = l));
						}
					}
				} else r = wa(r === g ? r.splice(o, r.length) : r), e ? e(null, g, r, i) : G.apply(g, r);
			});
		}function ya(a) {
			for (var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[" "], i = g ? 1 : 0, k = ta(function (a) {
				return a === b;
			}, h, !0), l = ta(function (a) {
				return I(b, a) > -1;
			}, h, !0), m = [function (a, c, d) {
				var e = !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));return b = null, e;
			}]; i < f; i++) {
				if (c = d.relative[a[i].type]) m = [ta(ua(m), c)];else {
					if (c = d.filter[a[i].type].apply(null, a[i].matches), c[u]) {
						for (e = ++i; e < f; e++) {
							if (d.relative[a[e].type]) break;
						}return xa(i > 1 && ua(m), i > 1 && sa(a.slice(0, i - 1).concat({ value: " " === a[i - 2].type ? "*" : "" })).replace(P, "$1"), c, i < e && ya(a.slice(i, e)), e < f && ya(a = a.slice(e)), e < f && sa(a));
					}m.push(c);
				}
			}return ua(m);
		}function za(a, b) {
			var c = b.length > 0,
			    e = a.length > 0,
			    f = function f(_f, g, h, i, k) {
				var l,
				    o,
				    q,
				    r = 0,
				    s = "0",
				    t = _f && [],
				    u = [],
				    v = j,
				    x = _f || e && d.find.TAG("*", k),
				    y = w += null == v ? 1 : Math.random() || .1,
				    z = x.length;for (k && (j = g === n || g || k); s !== z && null != (l = x[s]); s++) {
					if (e && l) {
						o = 0, g || l.ownerDocument === n || (m(l), h = !p);while (q = a[o++]) {
							if (q(l, g || n, h)) {
								i.push(l);break;
							}
						}k && (w = y);
					}c && ((l = !q && l) && r--, _f && t.push(l));
				}if (r += s, c && s !== r) {
					o = 0;while (q = b[o++]) {
						q(t, u, g, h);
					}if (_f) {
						if (r > 0) while (s--) {
							t[s] || u[s] || (u[s] = E.call(i));
						}u = wa(u);
					}G.apply(i, u), k && !_f && u.length > 0 && r + b.length > 1 && ga.uniqueSort(i);
				}return k && (w = y, j = v), t;
			};return c ? ia(f) : f;
		}return h = ga.compile = function (a, b) {
			var c,
			    d = [],
			    e = [],
			    f = A[a + " "];if (!f) {
				b || (b = g(a)), c = b.length;while (c--) {
					f = ya(b[c]), f[u] ? d.push(f) : e.push(f);
				}f = A(a, za(e, d)), f.selector = a;
			}return f;
		}, i = ga.select = function (a, b, c, e) {
			var f,
			    i,
			    j,
			    k,
			    l,
			    m = "function" == typeof a && a,
			    n = !e && g(a = m.selector || a);if (c = c || [], 1 === n.length) {
				if (i = n[0] = n[0].slice(0), i.length > 2 && "ID" === (j = i[0]).type && 9 === b.nodeType && p && d.relative[i[1].type]) {
					if (b = (d.find.ID(j.matches[0].replace(_, aa), b) || [])[0], !b) return c;m && (b = b.parentNode), a = a.slice(i.shift().value.length);
				}f = V.needsContext.test(a) ? 0 : i.length;while (f--) {
					if (j = i[f], d.relative[k = j.type]) break;if ((l = d.find[k]) && (e = l(j.matches[0].replace(_, aa), $.test(i[0].type) && qa(b.parentNode) || b))) {
						if (i.splice(f, 1), a = e.length && sa(i), !a) return G.apply(c, e), c;break;
					}
				}
			}return (m || h(a, n))(e, b, !p, c, !b || $.test(a) && qa(b.parentNode) || b), c;
		}, c.sortStable = u.split("").sort(B).join("") === u, c.detectDuplicates = !!l, m(), c.sortDetached = ja(function (a) {
			return 1 & a.compareDocumentPosition(n.createElement("fieldset"));
		}), ja(function (a) {
			return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href");
		}) || ka("type|href|height|width", function (a, b, c) {
			if (!c) return a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2);
		}), c.attributes && ja(function (a) {
			return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value");
		}) || ka("value", function (a, b, c) {
			if (!c && "input" === a.nodeName.toLowerCase()) return a.defaultValue;
		}), ja(function (a) {
			return null == a.getAttribute("disabled");
		}) || ka(J, function (a, b, c) {
			var d;if (!c) return a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null;
		}), ga;
	}(a);r.find = x, r.expr = x.selectors, r.expr[":"] = r.expr.pseudos, r.uniqueSort = r.unique = x.uniqueSort, r.text = x.getText, r.isXMLDoc = x.isXML, r.contains = x.contains, r.escapeSelector = x.escape;var y = function y(a, b, c) {
		var d = [],
		    e = void 0 !== c;while ((a = a[b]) && 9 !== a.nodeType) {
			if (1 === a.nodeType) {
				if (e && r(a).is(c)) break;d.push(a);
			}
		}return d;
	},
	    z = function z(a, b) {
		for (var c = []; a; a = a.nextSibling) {
			1 === a.nodeType && a !== b && c.push(a);
		}return c;
	},
	    A = r.expr.match.needsContext;function B(a, b) {
		return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase();
	}var C = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
	    D = /^.[^:#\[\.,]*$/;function E(a, b, c) {
		return r.isFunction(b) ? r.grep(a, function (a, d) {
			return !!b.call(a, d, a) !== c;
		}) : b.nodeType ? r.grep(a, function (a) {
			return a === b !== c;
		}) : "string" != typeof b ? r.grep(a, function (a) {
			return i.call(b, a) > -1 !== c;
		}) : D.test(b) ? r.filter(b, a, c) : (b = r.filter(b, a), r.grep(a, function (a) {
			return i.call(b, a) > -1 !== c && 1 === a.nodeType;
		}));
	}r.filter = function (a, b, c) {
		var d = b[0];return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? r.find.matchesSelector(d, a) ? [d] : [] : r.find.matches(a, r.grep(b, function (a) {
			return 1 === a.nodeType;
		}));
	}, r.fn.extend({ find: function find(a) {
			var b,
			    c,
			    d = this.length,
			    e = this;if ("string" != typeof a) return this.pushStack(r(a).filter(function () {
				for (b = 0; b < d; b++) {
					if (r.contains(e[b], this)) return !0;
				}
			}));for (c = this.pushStack([]), b = 0; b < d; b++) {
				r.find(a, e[b], c);
			}return d > 1 ? r.uniqueSort(c) : c;
		}, filter: function filter(a) {
			return this.pushStack(E(this, a || [], !1));
		}, not: function not(a) {
			return this.pushStack(E(this, a || [], !0));
		}, is: function is(a) {
			return !!E(this, "string" == typeof a && A.test(a) ? r(a) : a || [], !1).length;
		} });var F,
	    G = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
	    H = r.fn.init = function (a, b, c) {
		var e, f;if (!a) return this;if (c = c || F, "string" == typeof a) {
			if (e = "<" === a[0] && ">" === a[a.length - 1] && a.length >= 3 ? [null, a, null] : G.exec(a), !e || !e[1] && b) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);if (e[1]) {
				if (b = b instanceof r ? b[0] : b, r.merge(this, r.parseHTML(e[1], b && b.nodeType ? b.ownerDocument || b : d, !0)), C.test(e[1]) && r.isPlainObject(b)) for (e in b) {
					r.isFunction(this[e]) ? this[e](b[e]) : this.attr(e, b[e]);
				}return this;
			}return f = d.getElementById(e[2]), f && (this[0] = f, this.length = 1), this;
		}return a.nodeType ? (this[0] = a, this.length = 1, this) : r.isFunction(a) ? void 0 !== c.ready ? c.ready(a) : a(r) : r.makeArray(a, this);
	};H.prototype = r.fn, F = r(d);var I = /^(?:parents|prev(?:Until|All))/,
	    J = { children: !0, contents: !0, next: !0, prev: !0 };r.fn.extend({ has: function has(a) {
			var b = r(a, this),
			    c = b.length;return this.filter(function () {
				for (var a = 0; a < c; a++) {
					if (r.contains(this, b[a])) return !0;
				}
			});
		}, closest: function closest(a, b) {
			var c,
			    d = 0,
			    e = this.length,
			    f = [],
			    g = "string" != typeof a && r(a);if (!A.test(a)) for (; d < e; d++) {
				for (c = this[d]; c && c !== b; c = c.parentNode) {
					if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && r.find.matchesSelector(c, a))) {
						f.push(c);break;
					}
				}
			}return this.pushStack(f.length > 1 ? r.uniqueSort(f) : f);
		}, index: function index(a) {
			return a ? "string" == typeof a ? i.call(r(a), this[0]) : i.call(this, a.jquery ? a[0] : a) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
		}, add: function add(a, b) {
			return this.pushStack(r.uniqueSort(r.merge(this.get(), r(a, b))));
		}, addBack: function addBack(a) {
			return this.add(null == a ? this.prevObject : this.prevObject.filter(a));
		} });function K(a, b) {
		while ((a = a[b]) && 1 !== a.nodeType) {}return a;
	}r.each({ parent: function parent(a) {
			var b = a.parentNode;return b && 11 !== b.nodeType ? b : null;
		}, parents: function parents(a) {
			return y(a, "parentNode");
		}, parentsUntil: function parentsUntil(a, b, c) {
			return y(a, "parentNode", c);
		}, next: function next(a) {
			return K(a, "nextSibling");
		}, prev: function prev(a) {
			return K(a, "previousSibling");
		}, nextAll: function nextAll(a) {
			return y(a, "nextSibling");
		}, prevAll: function prevAll(a) {
			return y(a, "previousSibling");
		}, nextUntil: function nextUntil(a, b, c) {
			return y(a, "nextSibling", c);
		}, prevUntil: function prevUntil(a, b, c) {
			return y(a, "previousSibling", c);
		}, siblings: function siblings(a) {
			return z((a.parentNode || {}).firstChild, a);
		}, children: function children(a) {
			return z(a.firstChild);
		}, contents: function contents(a) {
			return B(a, "iframe") ? a.contentDocument : (B(a, "template") && (a = a.content || a), r.merge([], a.childNodes));
		} }, function (a, b) {
		r.fn[a] = function (c, d) {
			var e = r.map(this, b, c);return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = r.filter(d, e)), this.length > 1 && (J[a] || r.uniqueSort(e), I.test(a) && e.reverse()), this.pushStack(e);
		};
	});var L = /[^\x20\t\r\n\f]+/g;function M(a) {
		var b = {};return r.each(a.match(L) || [], function (a, c) {
			b[c] = !0;
		}), b;
	}r.Callbacks = function (a) {
		a = "string" == typeof a ? M(a) : r.extend({}, a);var b,
		    c,
		    d,
		    e,
		    f = [],
		    g = [],
		    h = -1,
		    i = function i() {
			for (e = e || a.once, d = b = !0; g.length; h = -1) {
				c = g.shift();while (++h < f.length) {
					f[h].apply(c[0], c[1]) === !1 && a.stopOnFalse && (h = f.length, c = !1);
				}
			}a.memory || (c = !1), b = !1, e && (f = c ? [] : "");
		},
		    j = { add: function add() {
				return f && (c && !b && (h = f.length - 1, g.push(c)), function d(b) {
					r.each(b, function (b, c) {
						r.isFunction(c) ? a.unique && j.has(c) || f.push(c) : c && c.length && "string" !== r.type(c) && d(c);
					});
				}(arguments), c && !b && i()), this;
			}, remove: function remove() {
				return r.each(arguments, function (a, b) {
					var c;while ((c = r.inArray(b, f, c)) > -1) {
						f.splice(c, 1), c <= h && h--;
					}
				}), this;
			}, has: function has(a) {
				return a ? r.inArray(a, f) > -1 : f.length > 0;
			}, empty: function empty() {
				return f && (f = []), this;
			}, disable: function disable() {
				return e = g = [], f = c = "", this;
			}, disabled: function disabled() {
				return !f;
			}, lock: function lock() {
				return e = g = [], c || b || (f = c = ""), this;
			}, locked: function locked() {
				return !!e;
			}, fireWith: function fireWith(a, c) {
				return e || (c = c || [], c = [a, c.slice ? c.slice() : c], g.push(c), b || i()), this;
			}, fire: function fire() {
				return j.fireWith(this, arguments), this;
			}, fired: function fired() {
				return !!d;
			} };return j;
	};function N(a) {
		return a;
	}function O(a) {
		throw a;
	}function P(a, b, c, d) {
		var e;try {
			a && r.isFunction(e = a.promise) ? e.call(a).done(b).fail(c) : a && r.isFunction(e = a.then) ? e.call(a, b, c) : b.apply(void 0, [a].slice(d));
		} catch (a) {
			c.apply(void 0, [a]);
		}
	}r.extend({ Deferred: function Deferred(b) {
			var c = [["notify", "progress", r.Callbacks("memory"), r.Callbacks("memory"), 2], ["resolve", "done", r.Callbacks("once memory"), r.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", r.Callbacks("once memory"), r.Callbacks("once memory"), 1, "rejected"]],
			    d = "pending",
			    e = { state: function state() {
					return d;
				}, always: function always() {
					return f.done(arguments).fail(arguments), this;
				}, "catch": function _catch(a) {
					return e.then(null, a);
				}, pipe: function pipe() {
					var a = arguments;return r.Deferred(function (b) {
						r.each(c, function (c, d) {
							var e = r.isFunction(a[d[4]]) && a[d[4]];f[d[1]](function () {
								var a = e && e.apply(this, arguments);a && r.isFunction(a.promise) ? a.promise().progress(b.notify).done(b.resolve).fail(b.reject) : b[d[0] + "With"](this, e ? [a] : arguments);
							});
						}), a = null;
					}).promise();
				}, then: function then(b, d, e) {
					var f = 0;function g(b, c, d, e) {
						return function () {
							var h = this,
							    i = arguments,
							    j = function j() {
								var a, j;if (!(b < f)) {
									if (a = d.apply(h, i), a === c.promise()) throw new TypeError("Thenable self-resolution");j = a && ("object" == (typeof a === "undefined" ? "undefined" : _typeof(a)) || "function" == typeof a) && a.then, r.isFunction(j) ? e ? j.call(a, g(f, c, N, e), g(f, c, O, e)) : (f++, j.call(a, g(f, c, N, e), g(f, c, O, e), g(f, c, N, c.notifyWith))) : (d !== N && (h = void 0, i = [a]), (e || c.resolveWith)(h, i));
								}
							},
							    k = e ? j : function () {
								try {
									j();
								} catch (a) {
									r.Deferred.exceptionHook && r.Deferred.exceptionHook(a, k.stackTrace), b + 1 >= f && (d !== O && (h = void 0, i = [a]), c.rejectWith(h, i));
								}
							};b ? k() : (r.Deferred.getStackHook && (k.stackTrace = r.Deferred.getStackHook()), a.setTimeout(k));
						};
					}return r.Deferred(function (a) {
						c[0][3].add(g(0, a, r.isFunction(e) ? e : N, a.notifyWith)), c[1][3].add(g(0, a, r.isFunction(b) ? b : N)), c[2][3].add(g(0, a, r.isFunction(d) ? d : O));
					}).promise();
				}, promise: function promise(a) {
					return null != a ? r.extend(a, e) : e;
				} },
			    f = {};return r.each(c, function (a, b) {
				var g = b[2],
				    h = b[5];e[b[1]] = g.add, h && g.add(function () {
					d = h;
				}, c[3 - a][2].disable, c[0][2].lock), g.add(b[3].fire), f[b[0]] = function () {
					return f[b[0] + "With"](this === f ? void 0 : this, arguments), this;
				}, f[b[0] + "With"] = g.fireWith;
			}), e.promise(f), b && b.call(f, f), f;
		}, when: function when(a) {
			var b = arguments.length,
			    c = b,
			    d = Array(c),
			    e = f.call(arguments),
			    g = r.Deferred(),
			    h = function h(a) {
				return function (c) {
					d[a] = this, e[a] = arguments.length > 1 ? f.call(arguments) : c, --b || g.resolveWith(d, e);
				};
			};if (b <= 1 && (P(a, g.done(h(c)).resolve, g.reject, !b), "pending" === g.state() || r.isFunction(e[c] && e[c].then))) return g.then();while (c--) {
				P(e[c], h(c), g.reject);
			}return g.promise();
		} });var Q = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;r.Deferred.exceptionHook = function (b, c) {
		a.console && a.console.warn && b && Q.test(b.name) && a.console.warn("jQuery.Deferred exception: " + b.message, b.stack, c);
	}, r.readyException = function (b) {
		a.setTimeout(function () {
			throw b;
		});
	};var R = r.Deferred();r.fn.ready = function (a) {
		return R.then(a)["catch"](function (a) {
			r.readyException(a);
		}), this;
	}, r.extend({ isReady: !1, readyWait: 1, ready: function ready(a) {
			(a === !0 ? --r.readyWait : r.isReady) || (r.isReady = !0, a !== !0 && --r.readyWait > 0 || R.resolveWith(d, [r]));
		} }), r.ready.then = R.then;function S() {
		d.removeEventListener("DOMContentLoaded", S), a.removeEventListener("load", S), r.ready();
	}"complete" === d.readyState || "loading" !== d.readyState && !d.documentElement.doScroll ? a.setTimeout(r.ready) : (d.addEventListener("DOMContentLoaded", S), a.addEventListener("load", S));var T = function T(a, b, c, d, e, f, g) {
		var h = 0,
		    i = a.length,
		    j = null == c;if ("object" === r.type(c)) {
			e = !0;for (h in c) {
				T(a, b, h, c[h], !0, f, g);
			}
		} else if (void 0 !== d && (e = !0, r.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function b(a, _b, c) {
			return j.call(r(a), c);
		})), b)) for (; h < i; h++) {
			b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
		}return e ? a : j ? b.call(a) : i ? b(a[0], c) : f;
	},
	    U = function U(a) {
		return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType;
	};function V() {
		this.expando = r.expando + V.uid++;
	}V.uid = 1, V.prototype = { cache: function cache(a) {
			var b = a[this.expando];return b || (b = {}, U(a) && (a.nodeType ? a[this.expando] = b : Object.defineProperty(a, this.expando, { value: b, configurable: !0 }))), b;
		}, set: function set(a, b, c) {
			var d,
			    e = this.cache(a);if ("string" == typeof b) e[r.camelCase(b)] = c;else for (d in b) {
				e[r.camelCase(d)] = b[d];
			}return e;
		}, get: function get(a, b) {
			return void 0 === b ? this.cache(a) : a[this.expando] && a[this.expando][r.camelCase(b)];
		}, access: function access(a, b, c) {
			return void 0 === b || b && "string" == typeof b && void 0 === c ? this.get(a, b) : (this.set(a, b, c), void 0 !== c ? c : b);
		}, remove: function remove(a, b) {
			var c,
			    d = a[this.expando];if (void 0 !== d) {
				if (void 0 !== b) {
					Array.isArray(b) ? b = b.map(r.camelCase) : (b = r.camelCase(b), b = b in d ? [b] : b.match(L) || []), c = b.length;while (c--) {
						delete d[b[c]];
					}
				}(void 0 === b || r.isEmptyObject(d)) && (a.nodeType ? a[this.expando] = void 0 : delete a[this.expando]);
			}
		}, hasData: function hasData(a) {
			var b = a[this.expando];return void 0 !== b && !r.isEmptyObject(b);
		} };var W = new V(),
	    X = new V(),
	    Y = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	    Z = /[A-Z]/g;function $(a) {
		return "true" === a || "false" !== a && ("null" === a ? null : a === +a + "" ? +a : Y.test(a) ? JSON.parse(a) : a);
	}function _(a, b, c) {
		var d;if (void 0 === c && 1 === a.nodeType) if (d = "data-" + b.replace(Z, "-$&").toLowerCase(), c = a.getAttribute(d), "string" == typeof c) {
			try {
				c = $(c);
			} catch (e) {}X.set(a, b, c);
		} else c = void 0;return c;
	}r.extend({ hasData: function hasData(a) {
			return X.hasData(a) || W.hasData(a);
		}, data: function data(a, b, c) {
			return X.access(a, b, c);
		}, removeData: function removeData(a, b) {
			X.remove(a, b);
		}, _data: function _data(a, b, c) {
			return W.access(a, b, c);
		}, _removeData: function _removeData(a, b) {
			W.remove(a, b);
		} }), r.fn.extend({ data: function data(a, b) {
			var c,
			    d,
			    e,
			    f = this[0],
			    g = f && f.attributes;if (void 0 === a) {
				if (this.length && (e = X.get(f), 1 === f.nodeType && !W.get(f, "hasDataAttrs"))) {
					c = g.length;while (c--) {
						g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = r.camelCase(d.slice(5)), _(f, d, e[d])));
					}W.set(f, "hasDataAttrs", !0);
				}return e;
			}return "object" == (typeof a === "undefined" ? "undefined" : _typeof(a)) ? this.each(function () {
				X.set(this, a);
			}) : T(this, function (b) {
				var c;if (f && void 0 === b) {
					if (c = X.get(f, a), void 0 !== c) return c;if (c = _(f, a), void 0 !== c) return c;
				} else this.each(function () {
					X.set(this, a, b);
				});
			}, null, b, arguments.length > 1, null, !0);
		}, removeData: function removeData(a) {
			return this.each(function () {
				X.remove(this, a);
			});
		} }), r.extend({ queue: function queue(a, b, c) {
			var d;if (a) return b = (b || "fx") + "queue", d = W.get(a, b), c && (!d || Array.isArray(c) ? d = W.access(a, b, r.makeArray(c)) : d.push(c)), d || [];
		}, dequeue: function dequeue(a, b) {
			b = b || "fx";var c = r.queue(a, b),
			    d = c.length,
			    e = c.shift(),
			    f = r._queueHooks(a, b),
			    g = function g() {
				r.dequeue(a, b);
			};"inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire();
		}, _queueHooks: function _queueHooks(a, b) {
			var c = b + "queueHooks";return W.get(a, c) || W.access(a, c, { empty: r.Callbacks("once memory").add(function () {
					W.remove(a, [b + "queue", c]);
				}) });
		} }), r.fn.extend({ queue: function queue(a, b) {
			var c = 2;return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? r.queue(this[0], a) : void 0 === b ? this : this.each(function () {
				var c = r.queue(this, a, b);r._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && r.dequeue(this, a);
			});
		}, dequeue: function dequeue(a) {
			return this.each(function () {
				r.dequeue(this, a);
			});
		}, clearQueue: function clearQueue(a) {
			return this.queue(a || "fx", []);
		}, promise: function promise(a, b) {
			var c,
			    d = 1,
			    e = r.Deferred(),
			    f = this,
			    g = this.length,
			    h = function h() {
				--d || e.resolveWith(f, [f]);
			};"string" != typeof a && (b = a, a = void 0), a = a || "fx";while (g--) {
				c = W.get(f[g], a + "queueHooks"), c && c.empty && (d++, c.empty.add(h));
			}return h(), e.promise(b);
		} });var aa = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
	    ba = new RegExp("^(?:([+-])=|)(" + aa + ")([a-z%]*)$", "i"),
	    ca = ["Top", "Right", "Bottom", "Left"],
	    da = function da(a, b) {
		return a = b || a, "none" === a.style.display || "" === a.style.display && r.contains(a.ownerDocument, a) && "none" === r.css(a, "display");
	},
	    ea = function ea(a, b, c, d) {
		var e,
		    f,
		    g = {};for (f in b) {
			g[f] = a.style[f], a.style[f] = b[f];
		}e = c.apply(a, d || []);for (f in b) {
			a.style[f] = g[f];
		}return e;
	};function fa(a, b, c, d) {
		var e,
		    f = 1,
		    g = 20,
		    h = d ? function () {
			return d.cur();
		} : function () {
			return r.css(a, b, "");
		},
		    i = h(),
		    j = c && c[3] || (r.cssNumber[b] ? "" : "px"),
		    k = (r.cssNumber[b] || "px" !== j && +i) && ba.exec(r.css(a, b));if (k && k[3] !== j) {
			j = j || k[3], c = c || [], k = +i || 1;do {
				f = f || ".5", k /= f, r.style(a, b, k + j);
			} while (f !== (f = h() / i) && 1 !== f && --g);
		}return c && (k = +k || +i || 0, e = c[1] ? k + (c[1] + 1) * c[2] : +c[2], d && (d.unit = j, d.start = k, d.end = e)), e;
	}var ga = {};function ha(a) {
		var b,
		    c = a.ownerDocument,
		    d = a.nodeName,
		    e = ga[d];return e ? e : (b = c.body.appendChild(c.createElement(d)), e = r.css(b, "display"), b.parentNode.removeChild(b), "none" === e && (e = "block"), ga[d] = e, e);
	}function ia(a, b) {
		for (var c, d, e = [], f = 0, g = a.length; f < g; f++) {
			d = a[f], d.style && (c = d.style.display, b ? ("none" === c && (e[f] = W.get(d, "display") || null, e[f] || (d.style.display = "")), "" === d.style.display && da(d) && (e[f] = ha(d))) : "none" !== c && (e[f] = "none", W.set(d, "display", c)));
		}for (f = 0; f < g; f++) {
			null != e[f] && (a[f].style.display = e[f]);
		}return a;
	}r.fn.extend({ show: function show() {
			return ia(this, !0);
		}, hide: function hide() {
			return ia(this);
		}, toggle: function toggle(a) {
			return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function () {
				da(this) ? r(this).show() : r(this).hide();
			});
		} });var ja = /^(?:checkbox|radio)$/i,
	    ka = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
	    la = /^$|\/(?:java|ecma)script/i,
	    ma = { option: [1, "<select multiple='multiple'>", "</select>"], thead: [1, "<table>", "</table>"], col: [2, "<table><colgroup>", "</colgroup></table>"], tr: [2, "<table><tbody>", "</tbody></table>"], td: [3, "<table><tbody><tr>", "</tr></tbody></table>"], _default: [0, "", ""] };ma.optgroup = ma.option, ma.tbody = ma.tfoot = ma.colgroup = ma.caption = ma.thead, ma.th = ma.td;function na(a, b) {
		var c;return c = "undefined" != typeof a.getElementsByTagName ? a.getElementsByTagName(b || "*") : "undefined" != typeof a.querySelectorAll ? a.querySelectorAll(b || "*") : [], void 0 === b || b && B(a, b) ? r.merge([a], c) : c;
	}function oa(a, b) {
		for (var c = 0, d = a.length; c < d; c++) {
			W.set(a[c], "globalEval", !b || W.get(b[c], "globalEval"));
		}
	}var pa = /<|&#?\w+;/;function qa(a, b, c, d, e) {
		for (var f, g, h, i, j, k, l = b.createDocumentFragment(), m = [], n = 0, o = a.length; n < o; n++) {
			if (f = a[n], f || 0 === f) if ("object" === r.type(f)) r.merge(m, f.nodeType ? [f] : f);else if (pa.test(f)) {
				g = g || l.appendChild(b.createElement("div")), h = (ka.exec(f) || ["", ""])[1].toLowerCase(), i = ma[h] || ma._default, g.innerHTML = i[1] + r.htmlPrefilter(f) + i[2], k = i[0];while (k--) {
					g = g.lastChild;
				}r.merge(m, g.childNodes), g = l.firstChild, g.textContent = "";
			} else m.push(b.createTextNode(f));
		}l.textContent = "", n = 0;while (f = m[n++]) {
			if (d && r.inArray(f, d) > -1) e && e.push(f);else if (j = r.contains(f.ownerDocument, f), g = na(l.appendChild(f), "script"), j && oa(g), c) {
				k = 0;while (f = g[k++]) {
					la.test(f.type || "") && c.push(f);
				}
			}
		}return l;
	}!function () {
		var a = d.createDocumentFragment(),
		    b = a.appendChild(d.createElement("div")),
		    c = d.createElement("input");c.setAttribute("type", "radio"), c.setAttribute("checked", "checked"), c.setAttribute("name", "t"), b.appendChild(c), o.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked, b.innerHTML = "<textarea>x</textarea>", o.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue;
	}();var ra = d.documentElement,
	    sa = /^key/,
	    ta = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
	    ua = /^([^.]*)(?:\.(.+)|)/;function va() {
		return !0;
	}function wa() {
		return !1;
	}function xa() {
		try {
			return d.activeElement;
		} catch (a) {}
	}function ya(a, b, c, d, e, f) {
		var g, h;if ("object" == (typeof b === "undefined" ? "undefined" : _typeof(b))) {
			"string" != typeof c && (d = d || c, c = void 0);for (h in b) {
				ya(a, h, c, d, b[h], f);
			}return a;
		}if (null == d && null == e ? (e = c, d = c = void 0) : null == e && ("string" == typeof c ? (e = d, d = void 0) : (e = d, d = c, c = void 0)), e === !1) e = wa;else if (!e) return a;return 1 === f && (g = e, e = function e(a) {
			return r().off(a), g.apply(this, arguments);
		}, e.guid = g.guid || (g.guid = r.guid++)), a.each(function () {
			r.event.add(this, b, e, d, c);
		});
	}r.event = { global: {}, add: function add(a, b, c, d, e) {
			var f,
			    g,
			    h,
			    i,
			    j,
			    k,
			    l,
			    m,
			    n,
			    o,
			    p,
			    q = W.get(a);if (q) {
				c.handler && (f = c, c = f.handler, e = f.selector), e && r.find.matchesSelector(ra, e), c.guid || (c.guid = r.guid++), (i = q.events) || (i = q.events = {}), (g = q.handle) || (g = q.handle = function (b) {
					return "undefined" != typeof r && r.event.triggered !== b.type ? r.event.dispatch.apply(a, arguments) : void 0;
				}), b = (b || "").match(L) || [""], j = b.length;while (j--) {
					h = ua.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n && (l = r.event.special[n] || {}, n = (e ? l.delegateType : l.bindType) || n, l = r.event.special[n] || {}, k = r.extend({ type: n, origType: p, data: d, handler: c, guid: c.guid, selector: e, needsContext: e && r.expr.match.needsContext.test(e), namespace: o.join(".") }, f), (m = i[n]) || (m = i[n] = [], m.delegateCount = 0, l.setup && l.setup.call(a, d, o, g) !== !1 || a.addEventListener && a.addEventListener(n, g)), l.add && (l.add.call(a, k), k.handler.guid || (k.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, k) : m.push(k), r.event.global[n] = !0);
				}
			}
		}, remove: function remove(a, b, c, d, e) {
			var f,
			    g,
			    h,
			    i,
			    j,
			    k,
			    l,
			    m,
			    n,
			    o,
			    p,
			    q = W.hasData(a) && W.get(a);if (q && (i = q.events)) {
				b = (b || "").match(L) || [""], j = b.length;while (j--) {
					if (h = ua.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n) {
						l = r.event.special[n] || {}, n = (d ? l.delegateType : l.bindType) || n, m = i[n] || [], h = h[2] && new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)"), g = f = m.length;while (f--) {
							k = m[f], !e && p !== k.origType || c && c.guid !== k.guid || h && !h.test(k.namespace) || d && d !== k.selector && ("**" !== d || !k.selector) || (m.splice(f, 1), k.selector && m.delegateCount--, l.remove && l.remove.call(a, k));
						}g && !m.length && (l.teardown && l.teardown.call(a, o, q.handle) !== !1 || r.removeEvent(a, n, q.handle), delete i[n]);
					} else for (n in i) {
						r.event.remove(a, n + b[j], c, d, !0);
					}
				}r.isEmptyObject(i) && W.remove(a, "handle events");
			}
		}, dispatch: function dispatch(a) {
			var b = r.event.fix(a),
			    c,
			    d,
			    e,
			    f,
			    g,
			    h,
			    i = new Array(arguments.length),
			    j = (W.get(this, "events") || {})[b.type] || [],
			    k = r.event.special[b.type] || {};for (i[0] = b, c = 1; c < arguments.length; c++) {
				i[c] = arguments[c];
			}if (b.delegateTarget = this, !k.preDispatch || k.preDispatch.call(this, b) !== !1) {
				h = r.event.handlers.call(this, b, j), c = 0;while ((f = h[c++]) && !b.isPropagationStopped()) {
					b.currentTarget = f.elem, d = 0;while ((g = f.handlers[d++]) && !b.isImmediatePropagationStopped()) {
						b.rnamespace && !b.rnamespace.test(g.namespace) || (b.handleObj = g, b.data = g.data, e = ((r.event.special[g.origType] || {}).handle || g.handler).apply(f.elem, i), void 0 !== e && (b.result = e) === !1 && (b.preventDefault(), b.stopPropagation()));
					}
				}return k.postDispatch && k.postDispatch.call(this, b), b.result;
			}
		}, handlers: function handlers(a, b) {
			var c,
			    d,
			    e,
			    f,
			    g,
			    h = [],
			    i = b.delegateCount,
			    j = a.target;if (i && j.nodeType && !("click" === a.type && a.button >= 1)) for (; j !== this; j = j.parentNode || this) {
				if (1 === j.nodeType && ("click" !== a.type || j.disabled !== !0)) {
					for (f = [], g = {}, c = 0; c < i; c++) {
						d = b[c], e = d.selector + " ", void 0 === g[e] && (g[e] = d.needsContext ? r(e, this).index(j) > -1 : r.find(e, this, null, [j]).length), g[e] && f.push(d);
					}f.length && h.push({ elem: j, handlers: f });
				}
			}return j = this, i < b.length && h.push({ elem: j, handlers: b.slice(i) }), h;
		}, addProp: function addProp(a, b) {
			Object.defineProperty(r.Event.prototype, a, { enumerable: !0, configurable: !0, get: r.isFunction(b) ? function () {
					if (this.originalEvent) return b(this.originalEvent);
				} : function () {
					if (this.originalEvent) return this.originalEvent[a];
				}, set: function set(b) {
					Object.defineProperty(this, a, { enumerable: !0, configurable: !0, writable: !0, value: b });
				} });
		}, fix: function fix(a) {
			return a[r.expando] ? a : new r.Event(a);
		}, special: { load: { noBubble: !0 }, focus: { trigger: function trigger() {
					if (this !== xa() && this.focus) return this.focus(), !1;
				}, delegateType: "focusin" }, blur: { trigger: function trigger() {
					if (this === xa() && this.blur) return this.blur(), !1;
				}, delegateType: "focusout" }, click: { trigger: function trigger() {
					if ("checkbox" === this.type && this.click && B(this, "input")) return this.click(), !1;
				}, _default: function _default(a) {
					return B(a.target, "a");
				} }, beforeunload: { postDispatch: function postDispatch(a) {
					void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result);
				} } } }, r.removeEvent = function (a, b, c) {
		a.removeEventListener && a.removeEventListener(b, c);
	}, r.Event = function (a, b) {
		return this instanceof r.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? va : wa, this.target = a.target && 3 === a.target.nodeType ? a.target.parentNode : a.target, this.currentTarget = a.currentTarget, this.relatedTarget = a.relatedTarget) : this.type = a, b && r.extend(this, b), this.timeStamp = a && a.timeStamp || r.now(), void (this[r.expando] = !0)) : new r.Event(a, b);
	}, r.Event.prototype = { constructor: r.Event, isDefaultPrevented: wa, isPropagationStopped: wa, isImmediatePropagationStopped: wa, isSimulated: !1, preventDefault: function preventDefault() {
			var a = this.originalEvent;this.isDefaultPrevented = va, a && !this.isSimulated && a.preventDefault();
		}, stopPropagation: function stopPropagation() {
			var a = this.originalEvent;this.isPropagationStopped = va, a && !this.isSimulated && a.stopPropagation();
		}, stopImmediatePropagation: function stopImmediatePropagation() {
			var a = this.originalEvent;this.isImmediatePropagationStopped = va, a && !this.isSimulated && a.stopImmediatePropagation(), this.stopPropagation();
		} }, r.each({ altKey: !0, bubbles: !0, cancelable: !0, changedTouches: !0, ctrlKey: !0, detail: !0, eventPhase: !0, metaKey: !0, pageX: !0, pageY: !0, shiftKey: !0, view: !0, "char": !0, charCode: !0, key: !0, keyCode: !0, button: !0, buttons: !0, clientX: !0, clientY: !0, offsetX: !0, offsetY: !0, pointerId: !0, pointerType: !0, screenX: !0, screenY: !0, targetTouches: !0, toElement: !0, touches: !0, which: function which(a) {
			var b = a.button;return null == a.which && sa.test(a.type) ? null != a.charCode ? a.charCode : a.keyCode : !a.which && void 0 !== b && ta.test(a.type) ? 1 & b ? 1 : 2 & b ? 3 : 4 & b ? 2 : 0 : a.which;
		} }, r.event.addProp), r.each({ mouseenter: "mouseover", mouseleave: "mouseout", pointerenter: "pointerover", pointerleave: "pointerout" }, function (a, b) {
		r.event.special[a] = { delegateType: b, bindType: b, handle: function handle(a) {
				var c,
				    d = this,
				    e = a.relatedTarget,
				    f = a.handleObj;return e && (e === d || r.contains(d, e)) || (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c;
			} };
	}), r.fn.extend({ on: function on(a, b, c, d) {
			return ya(this, a, b, c, d);
		}, one: function one(a, b, c, d) {
			return ya(this, a, b, c, d, 1);
		}, off: function off(a, b, c) {
			var d, e;if (a && a.preventDefault && a.handleObj) return d = a.handleObj, r(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;if ("object" == (typeof a === "undefined" ? "undefined" : _typeof(a))) {
				for (e in a) {
					this.off(e, b, a[e]);
				}return this;
			}return b !== !1 && "function" != typeof b || (c = b, b = void 0), c === !1 && (c = wa), this.each(function () {
				r.event.remove(this, a, c, b);
			});
		} });var za = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
	    Aa = /<script|<style|<link/i,
	    Ba = /checked\s*(?:[^=]|=\s*.checked.)/i,
	    Ca = /^true\/(.*)/,
	    Da = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;function Ea(a, b) {
		return B(a, "table") && B(11 !== b.nodeType ? b : b.firstChild, "tr") ? r(">tbody", a)[0] || a : a;
	}function Fa(a) {
		return a.type = (null !== a.getAttribute("type")) + "/" + a.type, a;
	}function Ga(a) {
		var b = Ca.exec(a.type);return b ? a.type = b[1] : a.removeAttribute("type"), a;
	}function Ha(a, b) {
		var c, d, e, f, g, h, i, j;if (1 === b.nodeType) {
			if (W.hasData(a) && (f = W.access(a), g = W.set(b, f), j = f.events)) {
				delete g.handle, g.events = {};for (e in j) {
					for (c = 0, d = j[e].length; c < d; c++) {
						r.event.add(b, e, j[e][c]);
					}
				}
			}X.hasData(a) && (h = X.access(a), i = r.extend({}, h), X.set(b, i));
		}
	}function Ia(a, b) {
		var c = b.nodeName.toLowerCase();"input" === c && ja.test(a.type) ? b.checked = a.checked : "input" !== c && "textarea" !== c || (b.defaultValue = a.defaultValue);
	}function Ja(a, b, c, d) {
		b = g.apply([], b);var e,
		    f,
		    h,
		    i,
		    j,
		    k,
		    l = 0,
		    m = a.length,
		    n = m - 1,
		    q = b[0],
		    s = r.isFunction(q);if (s || m > 1 && "string" == typeof q && !o.checkClone && Ba.test(q)) return a.each(function (e) {
			var f = a.eq(e);s && (b[0] = q.call(this, e, f.html())), Ja(f, b, c, d);
		});if (m && (e = qa(b, a[0].ownerDocument, !1, a, d), f = e.firstChild, 1 === e.childNodes.length && (e = f), f || d)) {
			for (h = r.map(na(e, "script"), Fa), i = h.length; l < m; l++) {
				j = e, l !== n && (j = r.clone(j, !0, !0), i && r.merge(h, na(j, "script"))), c.call(a[l], j, l);
			}if (i) for (k = h[h.length - 1].ownerDocument, r.map(h, Ga), l = 0; l < i; l++) {
				j = h[l], la.test(j.type || "") && !W.access(j, "globalEval") && r.contains(k, j) && (j.src ? r._evalUrl && r._evalUrl(j.src) : p(j.textContent.replace(Da, ""), k));
			}
		}return a;
	}function Ka(a, b, c) {
		for (var d, e = b ? r.filter(b, a) : a, f = 0; null != (d = e[f]); f++) {
			c || 1 !== d.nodeType || r.cleanData(na(d)), d.parentNode && (c && r.contains(d.ownerDocument, d) && oa(na(d, "script")), d.parentNode.removeChild(d));
		}return a;
	}r.extend({ htmlPrefilter: function htmlPrefilter(a) {
			return a.replace(za, "<$1></$2>");
		}, clone: function clone(a, b, c) {
			var d,
			    e,
			    f,
			    g,
			    h = a.cloneNode(!0),
			    i = r.contains(a.ownerDocument, a);if (!(o.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || r.isXMLDoc(a))) for (g = na(h), f = na(a), d = 0, e = f.length; d < e; d++) {
				Ia(f[d], g[d]);
			}if (b) if (c) for (f = f || na(a), g = g || na(h), d = 0, e = f.length; d < e; d++) {
				Ha(f[d], g[d]);
			} else Ha(a, h);return g = na(h, "script"), g.length > 0 && oa(g, !i && na(a, "script")), h;
		}, cleanData: function cleanData(a) {
			for (var b, c, d, e = r.event.special, f = 0; void 0 !== (c = a[f]); f++) {
				if (U(c)) {
					if (b = c[W.expando]) {
						if (b.events) for (d in b.events) {
							e[d] ? r.event.remove(c, d) : r.removeEvent(c, d, b.handle);
						}c[W.expando] = void 0;
					}c[X.expando] && (c[X.expando] = void 0);
				}
			}
		} }), r.fn.extend({ detach: function detach(a) {
			return Ka(this, a, !0);
		}, remove: function remove(a) {
			return Ka(this, a);
		}, text: function text(a) {
			return T(this, function (a) {
				return void 0 === a ? r.text(this) : this.empty().each(function () {
					1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = a);
				});
			}, null, a, arguments.length);
		}, append: function append() {
			return Ja(this, arguments, function (a) {
				if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
					var b = Ea(this, a);b.appendChild(a);
				}
			});
		}, prepend: function prepend() {
			return Ja(this, arguments, function (a) {
				if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
					var b = Ea(this, a);b.insertBefore(a, b.firstChild);
				}
			});
		}, before: function before() {
			return Ja(this, arguments, function (a) {
				this.parentNode && this.parentNode.insertBefore(a, this);
			});
		}, after: function after() {
			return Ja(this, arguments, function (a) {
				this.parentNode && this.parentNode.insertBefore(a, this.nextSibling);
			});
		}, empty: function empty() {
			for (var a, b = 0; null != (a = this[b]); b++) {
				1 === a.nodeType && (r.cleanData(na(a, !1)), a.textContent = "");
			}return this;
		}, clone: function clone(a, b) {
			return a = null != a && a, b = null == b ? a : b, this.map(function () {
				return r.clone(this, a, b);
			});
		}, html: function html(a) {
			return T(this, function (a) {
				var b = this[0] || {},
				    c = 0,
				    d = this.length;if (void 0 === a && 1 === b.nodeType) return b.innerHTML;if ("string" == typeof a && !Aa.test(a) && !ma[(ka.exec(a) || ["", ""])[1].toLowerCase()]) {
					a = r.htmlPrefilter(a);try {
						for (; c < d; c++) {
							b = this[c] || {}, 1 === b.nodeType && (r.cleanData(na(b, !1)), b.innerHTML = a);
						}b = 0;
					} catch (e) {}
				}b && this.empty().append(a);
			}, null, a, arguments.length);
		}, replaceWith: function replaceWith() {
			var a = [];return Ja(this, arguments, function (b) {
				var c = this.parentNode;r.inArray(this, a) < 0 && (r.cleanData(na(this)), c && c.replaceChild(b, this));
			}, a);
		} }), r.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function (a, b) {
		r.fn[a] = function (a) {
			for (var c, d = [], e = r(a), f = e.length - 1, g = 0; g <= f; g++) {
				c = g === f ? this : this.clone(!0), r(e[g])[b](c), h.apply(d, c.get());
			}return this.pushStack(d);
		};
	});var La = /^margin/,
	    Ma = new RegExp("^(" + aa + ")(?!px)[a-z%]+$", "i"),
	    Na = function Na(b) {
		var c = b.ownerDocument.defaultView;return c && c.opener || (c = a), c.getComputedStyle(b);
	};!function () {
		function b() {
			if (i) {
				i.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", i.innerHTML = "", ra.appendChild(h);var b = a.getComputedStyle(i);c = "1%" !== b.top, g = "2px" === b.marginLeft, e = "4px" === b.width, i.style.marginRight = "50%", f = "4px" === b.marginRight, ra.removeChild(h), i = null;
			}
		}var c,
		    e,
		    f,
		    g,
		    h = d.createElement("div"),
		    i = d.createElement("div");i.style && (i.style.backgroundClip = "content-box", i.cloneNode(!0).style.backgroundClip = "", o.clearCloneStyle = "content-box" === i.style.backgroundClip, h.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", h.appendChild(i), r.extend(o, { pixelPosition: function pixelPosition() {
				return b(), c;
			}, boxSizingReliable: function boxSizingReliable() {
				return b(), e;
			}, pixelMarginRight: function pixelMarginRight() {
				return b(), f;
			}, reliableMarginLeft: function reliableMarginLeft() {
				return b(), g;
			} }));
	}();function Oa(a, b, c) {
		var d,
		    e,
		    f,
		    g,
		    h = a.style;return c = c || Na(a), c && (g = c.getPropertyValue(b) || c[b], "" !== g || r.contains(a.ownerDocument, a) || (g = r.style(a, b)), !o.pixelMarginRight() && Ma.test(g) && La.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f)), void 0 !== g ? g + "" : g;
	}function Pa(a, b) {
		return { get: function get() {
				return a() ? void delete this.get : (this.get = b).apply(this, arguments);
			} };
	}var Qa = /^(none|table(?!-c[ea]).+)/,
	    Ra = /^--/,
	    Sa = { position: "absolute", visibility: "hidden", display: "block" },
	    Ta = { letterSpacing: "0", fontWeight: "400" },
	    Ua = ["Webkit", "Moz", "ms"],
	    Va = d.createElement("div").style;function Wa(a) {
		if (a in Va) return a;var b = a[0].toUpperCase() + a.slice(1),
		    c = Ua.length;while (c--) {
			if (a = Ua[c] + b, a in Va) return a;
		}
	}function Xa(a) {
		var b = r.cssProps[a];return b || (b = r.cssProps[a] = Wa(a) || a), b;
	}function Ya(a, b, c) {
		var d = ba.exec(b);return d ? Math.max(0, d[2] - (c || 0)) + (d[3] || "px") : b;
	}function Za(a, b, c, d, e) {
		var f,
		    g = 0;for (f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0; f < 4; f += 2) {
			"margin" === c && (g += r.css(a, c + ca[f], !0, e)), d ? ("content" === c && (g -= r.css(a, "padding" + ca[f], !0, e)), "margin" !== c && (g -= r.css(a, "border" + ca[f] + "Width", !0, e))) : (g += r.css(a, "padding" + ca[f], !0, e), "padding" !== c && (g += r.css(a, "border" + ca[f] + "Width", !0, e)));
		}return g;
	}function $a(a, b, c) {
		var d,
		    e = Na(a),
		    f = Oa(a, b, e),
		    g = "border-box" === r.css(a, "boxSizing", !1, e);return Ma.test(f) ? f : (d = g && (o.boxSizingReliable() || f === a.style[b]), "auto" === f && (f = a["offset" + b[0].toUpperCase() + b.slice(1)]), f = parseFloat(f) || 0, f + Za(a, b, c || (g ? "border" : "content"), d, e) + "px");
	}r.extend({ cssHooks: { opacity: { get: function get(a, b) {
					if (b) {
						var c = Oa(a, "opacity");return "" === c ? "1" : c;
					}
				} } }, cssNumber: { animationIterationCount: !0, columnCount: !0, fillOpacity: !0, flexGrow: !0, flexShrink: !0, fontWeight: !0, lineHeight: !0, opacity: !0, order: !0, orphans: !0, widows: !0, zIndex: !0, zoom: !0 }, cssProps: { "float": "cssFloat" }, style: function style(a, b, c, d) {
			if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
				var e,
				    f,
				    g,
				    h = r.camelCase(b),
				    i = Ra.test(b),
				    j = a.style;return i || (b = Xa(h)), g = r.cssHooks[b] || r.cssHooks[h], void 0 === c ? g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : j[b] : (f = typeof c === "undefined" ? "undefined" : _typeof(c), "string" === f && (e = ba.exec(c)) && e[1] && (c = fa(a, b, e), f = "number"), null != c && c === c && ("number" === f && (c += e && e[3] || (r.cssNumber[h] ? "" : "px")), o.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (j[b] = "inherit"), g && "set" in g && void 0 === (c = g.set(a, c, d)) || (i ? j.setProperty(b, c) : j[b] = c)), void 0);
			}
		}, css: function css(a, b, c, d) {
			var e,
			    f,
			    g,
			    h = r.camelCase(b),
			    i = Ra.test(b);return i || (b = Xa(h)), g = r.cssHooks[b] || r.cssHooks[h], g && "get" in g && (e = g.get(a, !0, c)), void 0 === e && (e = Oa(a, b, d)), "normal" === e && b in Ta && (e = Ta[b]), "" === c || c ? (f = parseFloat(e), c === !0 || isFinite(f) ? f || 0 : e) : e;
		} }), r.each(["height", "width"], function (a, b) {
		r.cssHooks[b] = { get: function get(a, c, d) {
				if (c) return !Qa.test(r.css(a, "display")) || a.getClientRects().length && a.getBoundingClientRect().width ? $a(a, b, d) : ea(a, Sa, function () {
					return $a(a, b, d);
				});
			}, set: function set(a, c, d) {
				var e,
				    f = d && Na(a),
				    g = d && Za(a, b, d, "border-box" === r.css(a, "boxSizing", !1, f), f);return g && (e = ba.exec(c)) && "px" !== (e[3] || "px") && (a.style[b] = c, c = r.css(a, b)), Ya(a, c, g);
			} };
	}), r.cssHooks.marginLeft = Pa(o.reliableMarginLeft, function (a, b) {
		if (b) return (parseFloat(Oa(a, "marginLeft")) || a.getBoundingClientRect().left - ea(a, { marginLeft: 0 }, function () {
			return a.getBoundingClientRect().left;
		})) + "px";
	}), r.each({ margin: "", padding: "", border: "Width" }, function (a, b) {
		r.cssHooks[a + b] = { expand: function expand(c) {
				for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; d < 4; d++) {
					e[a + ca[d] + b] = f[d] || f[d - 2] || f[0];
				}return e;
			} }, La.test(a) || (r.cssHooks[a + b].set = Ya);
	}), r.fn.extend({ css: function css(a, b) {
			return T(this, function (a, b, c) {
				var d,
				    e,
				    f = {},
				    g = 0;if (Array.isArray(b)) {
					for (d = Na(a), e = b.length; g < e; g++) {
						f[b[g]] = r.css(a, b[g], !1, d);
					}return f;
				}return void 0 !== c ? r.style(a, b, c) : r.css(a, b);
			}, a, b, arguments.length > 1);
		} }), r.fn.delay = function (b, c) {
		return b = r.fx ? r.fx.speeds[b] || b : b, c = c || "fx", this.queue(c, function (c, d) {
			var e = a.setTimeout(c, b);d.stop = function () {
				a.clearTimeout(e);
			};
		});
	}, function () {
		var a = d.createElement("input"),
		    b = d.createElement("select"),
		    c = b.appendChild(d.createElement("option"));a.type = "checkbox", o.checkOn = "" !== a.value, o.optSelected = c.selected, a = d.createElement("input"), a.value = "t", a.type = "radio", o.radioValue = "t" === a.value;
	}();var _a,
	    ab = r.expr.attrHandle;r.fn.extend({ attr: function attr(a, b) {
			return T(this, r.attr, a, b, arguments.length > 1);
		}, removeAttr: function removeAttr(a) {
			return this.each(function () {
				r.removeAttr(this, a);
			});
		} }), r.extend({ attr: function attr(a, b, c) {
			var d,
			    e,
			    f = a.nodeType;if (3 !== f && 8 !== f && 2 !== f) return "undefined" == typeof a.getAttribute ? r.prop(a, b, c) : (1 === f && r.isXMLDoc(a) || (e = r.attrHooks[b.toLowerCase()] || (r.expr.match.bool.test(b) ? _a : void 0)), void 0 !== c ? null === c ? void r.removeAttr(a, b) : e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a.setAttribute(b, c + ""), c) : e && "get" in e && null !== (d = e.get(a, b)) ? d : (d = r.find.attr(a, b), null == d ? void 0 : d));
		}, attrHooks: { type: { set: function set(a, b) {
					if (!o.radioValue && "radio" === b && B(a, "input")) {
						var c = a.value;return a.setAttribute("type", b), c && (a.value = c), b;
					}
				} } }, removeAttr: function removeAttr(a, b) {
			var c,
			    d = 0,
			    e = b && b.match(L);if (e && 1 === a.nodeType) while (c = e[d++]) {
				a.removeAttribute(c);
			}
		} }), _a = { set: function set(a, b, c) {
			return b === !1 ? r.removeAttr(a, c) : a.setAttribute(c, c), c;
		} }, r.each(r.expr.match.bool.source.match(/\w+/g), function (a, b) {
		var c = ab[b] || r.find.attr;ab[b] = function (a, b, d) {
			var e,
			    f,
			    g = b.toLowerCase();return d || (f = ab[g], ab[g] = e, e = null != c(a, b, d) ? g : null, ab[g] = f), e;
		};
	});var bb = /^(?:input|select|textarea|button)$/i,
	    cb = /^(?:a|area)$/i;r.fn.extend({ prop: function prop(a, b) {
			return T(this, r.prop, a, b, arguments.length > 1);
		}, removeProp: function removeProp(a) {
			return this.each(function () {
				delete this[r.propFix[a] || a];
			});
		} }), r.extend({ prop: function prop(a, b, c) {
			var d,
			    e,
			    f = a.nodeType;if (3 !== f && 8 !== f && 2 !== f) return 1 === f && r.isXMLDoc(a) || (b = r.propFix[b] || b, e = r.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b];
		}, propHooks: { tabIndex: { get: function get(a) {
					var b = r.find.attr(a, "tabindex");return b ? parseInt(b, 10) : bb.test(a.nodeName) || cb.test(a.nodeName) && a.href ? 0 : -1;
				} } }, propFix: { "for": "htmlFor", "class": "className" } }), o.optSelected || (r.propHooks.selected = { get: function get(a) {
			var b = a.parentNode;return b && b.parentNode && b.parentNode.selectedIndex, null;
		}, set: function set(a) {
			var b = a.parentNode;b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex);
		} }), r.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
		r.propFix[this.toLowerCase()] = this;
	});function db(a) {
		var b = a.match(L) || [];return b.join(" ");
	}function eb(a) {
		return a.getAttribute && a.getAttribute("class") || "";
	}r.fn.extend({ addClass: function addClass(a) {
			var b,
			    c,
			    d,
			    e,
			    f,
			    g,
			    h,
			    i = 0;if (r.isFunction(a)) return this.each(function (b) {
				r(this).addClass(a.call(this, b, eb(this)));
			});if ("string" == typeof a && a) {
				b = a.match(L) || [];while (c = this[i++]) {
					if (e = eb(c), d = 1 === c.nodeType && " " + db(e) + " ") {
						g = 0;while (f = b[g++]) {
							d.indexOf(" " + f + " ") < 0 && (d += f + " ");
						}h = db(d), e !== h && c.setAttribute("class", h);
					}
				}
			}return this;
		}, removeClass: function removeClass(a) {
			var b,
			    c,
			    d,
			    e,
			    f,
			    g,
			    h,
			    i = 0;if (r.isFunction(a)) return this.each(function (b) {
				r(this).removeClass(a.call(this, b, eb(this)));
			});if (!arguments.length) return this.attr("class", "");if ("string" == typeof a && a) {
				b = a.match(L) || [];while (c = this[i++]) {
					if (e = eb(c), d = 1 === c.nodeType && " " + db(e) + " ") {
						g = 0;while (f = b[g++]) {
							while (d.indexOf(" " + f + " ") > -1) {
								d = d.replace(" " + f + " ", " ");
							}
						}h = db(d), e !== h && c.setAttribute("class", h);
					}
				}
			}return this;
		}, toggleClass: function toggleClass(a, b) {
			var c = typeof a === "undefined" ? "undefined" : _typeof(a);return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : r.isFunction(a) ? this.each(function (c) {
				r(this).toggleClass(a.call(this, c, eb(this), b), b);
			}) : this.each(function () {
				var b, d, e, f;if ("string" === c) {
					d = 0, e = r(this), f = a.match(L) || [];while (b = f[d++]) {
						e.hasClass(b) ? e.removeClass(b) : e.addClass(b);
					}
				} else void 0 !== a && "boolean" !== c || (b = eb(this), b && W.set(this, "__className__", b), this.setAttribute && this.setAttribute("class", b || a === !1 ? "" : W.get(this, "__className__") || ""));
			});
		}, hasClass: function hasClass(a) {
			var b,
			    c,
			    d = 0;b = " " + a + " ";while (c = this[d++]) {
				if (1 === c.nodeType && (" " + db(eb(c)) + " ").indexOf(b) > -1) return !0;
			}return !1;
		} });var fb = /\r/g;r.fn.extend({ val: function val(a) {
			var b,
			    c,
			    d,
			    e = this[0];{
				if (arguments.length) return d = r.isFunction(a), this.each(function (c) {
					var e;1 === this.nodeType && (e = d ? a.call(this, c, r(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : Array.isArray(e) && (e = r.map(e, function (a) {
						return null == a ? "" : a + "";
					})), b = r.valHooks[this.type] || r.valHooks[this.nodeName.toLowerCase()], b && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e));
				});if (e) return b = r.valHooks[e.type] || r.valHooks[e.nodeName.toLowerCase()], b && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, "string" == typeof c ? c.replace(fb, "") : null == c ? "" : c);
			}
		} }), r.extend({ valHooks: { option: { get: function get(a) {
					var b = r.find.attr(a, "value");return null != b ? b : db(r.text(a));
				} }, select: { get: function get(a) {
					var b,
					    c,
					    d,
					    e = a.options,
					    f = a.selectedIndex,
					    g = "select-one" === a.type,
					    h = g ? null : [],
					    i = g ? f + 1 : e.length;for (d = f < 0 ? i : g ? f : 0; d < i; d++) {
						if (c = e[d], (c.selected || d === f) && !c.disabled && (!c.parentNode.disabled || !B(c.parentNode, "optgroup"))) {
							if (b = r(c).val(), g) return b;h.push(b);
						}
					}return h;
				}, set: function set(a, b) {
					var c,
					    d,
					    e = a.options,
					    f = r.makeArray(b),
					    g = e.length;while (g--) {
						d = e[g], (d.selected = r.inArray(r.valHooks.option.get(d), f) > -1) && (c = !0);
					}return c || (a.selectedIndex = -1), f;
				} } } }), r.each(["radio", "checkbox"], function () {
		r.valHooks[this] = { set: function set(a, b) {
				if (Array.isArray(b)) return a.checked = r.inArray(r(a).val(), b) > -1;
			} }, o.checkOn || (r.valHooks[this].get = function (a) {
			return null === a.getAttribute("value") ? "on" : a.value;
		});
	});var gb = /^(?:focusinfocus|focusoutblur)$/;r.extend(r.event, { trigger: function trigger(b, c, e, f) {
			var g,
			    h,
			    i,
			    j,
			    k,
			    m,
			    n,
			    o = [e || d],
			    p = l.call(b, "type") ? b.type : b,
			    q = l.call(b, "namespace") ? b.namespace.split(".") : [];if (h = i = e = e || d, 3 !== e.nodeType && 8 !== e.nodeType && !gb.test(p + r.event.triggered) && (p.indexOf(".") > -1 && (q = p.split("."), p = q.shift(), q.sort()), k = p.indexOf(":") < 0 && "on" + p, b = b[r.expando] ? b : new r.Event(p, "object" == (typeof b === "undefined" ? "undefined" : _typeof(b)) && b), b.isTrigger = f ? 2 : 3, b.namespace = q.join("."), b.rnamespace = b.namespace ? new RegExp("(^|\\.)" + q.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, b.result = void 0, b.target || (b.target = e), c = null == c ? [b] : r.makeArray(c, [b]), n = r.event.special[p] || {}, f || !n.trigger || n.trigger.apply(e, c) !== !1)) {
				if (!f && !n.noBubble && !r.isWindow(e)) {
					for (j = n.delegateType || p, gb.test(j + p) || (h = h.parentNode); h; h = h.parentNode) {
						o.push(h), i = h;
					}i === (e.ownerDocument || d) && o.push(i.defaultView || i.parentWindow || a);
				}g = 0;while ((h = o[g++]) && !b.isPropagationStopped()) {
					b.type = g > 1 ? j : n.bindType || p, m = (W.get(h, "events") || {})[b.type] && W.get(h, "handle"), m && m.apply(h, c), m = k && h[k], m && m.apply && U(h) && (b.result = m.apply(h, c), b.result === !1 && b.preventDefault());
				}return b.type = p, f || b.isDefaultPrevented() || n._default && n._default.apply(o.pop(), c) !== !1 || !U(e) || k && r.isFunction(e[p]) && !r.isWindow(e) && (i = e[k], i && (e[k] = null), r.event.triggered = p, e[p](), r.event.triggered = void 0, i && (e[k] = i)), b.result;
			}
		}, simulate: function simulate(a, b, c) {
			var d = r.extend(new r.Event(), c, { type: a, isSimulated: !0 });r.event.trigger(d, null, b);
		} }), r.fn.extend({ trigger: function trigger(a, b) {
			return this.each(function () {
				r.event.trigger(a, b, this);
			});
		}, triggerHandler: function triggerHandler(a, b) {
			var c = this[0];if (c) return r.event.trigger(a, b, c, !0);
		} }), r.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (a, b) {
		r.fn[b] = function (a, c) {
			return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b);
		};
	}), r.fn.extend({ hover: function hover(a, b) {
			return this.mouseenter(a).mouseleave(b || a);
		} }), o.focusin = "onfocusin" in a, o.focusin || r.each({ focus: "focusin", blur: "focusout" }, function (a, b) {
		var c = function c(a) {
			r.event.simulate(b, a.target, r.event.fix(a));
		};r.event.special[b] = { setup: function setup() {
				var d = this.ownerDocument || this,
				    e = W.access(d, b);e || d.addEventListener(a, c, !0), W.access(d, b, (e || 0) + 1);
			}, teardown: function teardown() {
				var d = this.ownerDocument || this,
				    e = W.access(d, b) - 1;e ? W.access(d, b, e) : (d.removeEventListener(a, c, !0), W.remove(d, b));
			} };
	});var hb = /\[\]$/,
	    ib = /\r?\n/g,
	    jb = /^(?:submit|button|image|reset|file)$/i,
	    kb = /^(?:input|select|textarea|keygen)/i;function lb(a, b, c, d) {
		var e;if (Array.isArray(b)) r.each(b, function (b, e) {
			c || hb.test(a) ? d(a, e) : lb(a + "[" + ("object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && null != e ? b : "") + "]", e, c, d);
		});else if (c || "object" !== r.type(b)) d(a, b);else for (e in b) {
			lb(a + "[" + e + "]", b[e], c, d);
		}
	}r.param = function (a, b) {
		var c,
		    d = [],
		    e = function e(a, b) {
			var c = r.isFunction(b) ? b() : b;d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(null == c ? "" : c);
		};if (Array.isArray(a) || a.jquery && !r.isPlainObject(a)) r.each(a, function () {
			e(this.name, this.value);
		});else for (c in a) {
			lb(c, a[c], b, e);
		}return d.join("&");
	}, r.fn.extend({ serialize: function serialize() {
			return r.param(this.serializeArray());
		}, serializeArray: function serializeArray() {
			return this.map(function () {
				var a = r.prop(this, "elements");return a ? r.makeArray(a) : this;
			}).filter(function () {
				var a = this.type;return this.name && !r(this).is(":disabled") && kb.test(this.nodeName) && !jb.test(a) && (this.checked || !ja.test(a));
			}).map(function (a, b) {
				var c = r(this).val();return null == c ? null : Array.isArray(c) ? r.map(c, function (a) {
					return { name: b.name, value: a.replace(ib, "\r\n") };
				}) : { name: b.name, value: c.replace(ib, "\r\n") };
			}).get();
		} }), r.fn.extend({ wrapAll: function wrapAll(a) {
			var b;return this[0] && (r.isFunction(a) && (a = a.call(this[0])), b = r(a, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && b.insertBefore(this[0]), b.map(function () {
				var a = this;while (a.firstElementChild) {
					a = a.firstElementChild;
				}return a;
			}).append(this)), this;
		}, wrapInner: function wrapInner(a) {
			return r.isFunction(a) ? this.each(function (b) {
				r(this).wrapInner(a.call(this, b));
			}) : this.each(function () {
				var b = r(this),
				    c = b.contents();c.length ? c.wrapAll(a) : b.append(a);
			});
		}, wrap: function wrap(a) {
			var b = r.isFunction(a);return this.each(function (c) {
				r(this).wrapAll(b ? a.call(this, c) : a);
			});
		}, unwrap: function unwrap(a) {
			return this.parent(a).not("body").each(function () {
				r(this).replaceWith(this.childNodes);
			}), this;
		} }), r.expr.pseudos.hidden = function (a) {
		return !r.expr.pseudos.visible(a);
	}, r.expr.pseudos.visible = function (a) {
		return !!(a.offsetWidth || a.offsetHeight || a.getClientRects().length);
	}, o.createHTMLDocument = function () {
		var a = d.implementation.createHTMLDocument("").body;return a.innerHTML = "<form></form><form></form>", 2 === a.childNodes.length;
	}(), r.parseHTML = function (a, b, c) {
		if ("string" != typeof a) return [];"boolean" == typeof b && (c = b, b = !1);var e, f, g;return b || (o.createHTMLDocument ? (b = d.implementation.createHTMLDocument(""), e = b.createElement("base"), e.href = d.location.href, b.head.appendChild(e)) : b = d), f = C.exec(a), g = !c && [], f ? [b.createElement(f[1])] : (f = qa([a], b, g), g && g.length && r(g).remove(), r.merge([], f.childNodes));
	}, r.offset = { setOffset: function setOffset(a, b, c) {
			var d,
			    e,
			    f,
			    g,
			    h,
			    i,
			    j,
			    k = r.css(a, "position"),
			    l = r(a),
			    m = {};"static" === k && (a.style.position = "relative"), h = l.offset(), f = r.css(a, "top"), i = r.css(a, "left"), j = ("absolute" === k || "fixed" === k) && (f + i).indexOf("auto") > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), r.isFunction(b) && (b = b.call(a, c, r.extend({}, h))), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using" in b ? b.using.call(a, m) : l.css(m);
		} }, r.fn.extend({ offset: function offset(a) {
			if (arguments.length) return void 0 === a ? this : this.each(function (b) {
				r.offset.setOffset(this, a, b);
			});var b,
			    c,
			    d,
			    e,
			    f = this[0];if (f) return f.getClientRects().length ? (d = f.getBoundingClientRect(), b = f.ownerDocument, c = b.documentElement, e = b.defaultView, { top: d.top + e.pageYOffset - c.clientTop, left: d.left + e.pageXOffset - c.clientLeft }) : { top: 0, left: 0 };
		}, position: function position() {
			if (this[0]) {
				var a,
				    b,
				    c = this[0],
				    d = { top: 0, left: 0 };return "fixed" === r.css(c, "position") ? b = c.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), B(a[0], "html") || (d = a.offset()), d = { top: d.top + r.css(a[0], "borderTopWidth", !0), left: d.left + r.css(a[0], "borderLeftWidth", !0) }), { top: b.top - d.top - r.css(c, "marginTop", !0), left: b.left - d.left - r.css(c, "marginLeft", !0) };
			}
		}, offsetParent: function offsetParent() {
			return this.map(function () {
				var a = this.offsetParent;while (a && "static" === r.css(a, "position")) {
					a = a.offsetParent;
				}return a || ra;
			});
		} }), r.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function (a, b) {
		var c = "pageYOffset" === b;r.fn[a] = function (d) {
			return T(this, function (a, d, e) {
				var f;return r.isWindow(a) ? f = a : 9 === a.nodeType && (f = a.defaultView), void 0 === e ? f ? f[b] : a[d] : void (f ? f.scrollTo(c ? f.pageXOffset : e, c ? e : f.pageYOffset) : a[d] = e);
			}, a, d, arguments.length);
		};
	}), r.each(["top", "left"], function (a, b) {
		r.cssHooks[b] = Pa(o.pixelPosition, function (a, c) {
			if (c) return c = Oa(a, b), Ma.test(c) ? r(a).position()[b] + "px" : c;
		});
	}), r.each({ Height: "height", Width: "width" }, function (a, b) {
		r.each({ padding: "inner" + a, content: b, "": "outer" + a }, function (c, d) {
			r.fn[d] = function (e, f) {
				var g = arguments.length && (c || "boolean" != typeof e),
				    h = c || (e === !0 || f === !0 ? "margin" : "border");return T(this, function (b, c, e) {
					var f;return r.isWindow(b) ? 0 === d.indexOf("outer") ? b["inner" + a] : b.document.documentElement["client" + a] : 9 === b.nodeType ? (f = b.documentElement, Math.max(b.body["scroll" + a], f["scroll" + a], b.body["offset" + a], f["offset" + a], f["client" + a])) : void 0 === e ? r.css(b, c, h) : r.style(b, c, e, h);
				}, b, g ? e : void 0, g);
			};
		});
	}), r.fn.extend({ bind: function bind(a, b, c) {
			return this.on(a, null, b, c);
		}, unbind: function unbind(a, b) {
			return this.off(a, null, b);
		}, delegate: function delegate(a, b, c, d) {
			return this.on(b, a, c, d);
		}, undelegate: function undelegate(a, b, c) {
			return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c);
		} }), r.holdReady = function (a) {
		a ? r.readyWait++ : r.ready(!0);
	}, r.isArray = Array.isArray, r.parseJSON = JSON.parse, r.nodeName = B, "function" == typeof define && define.amd && define("jquery", [], function () {
		return r;
	});var mb = a.jQuery,
	    nb = a.$;return r.noConflict = function (b) {
		return a.$ === r && (a.$ = nb), b && a.jQuery === r && (a.jQuery = mb), r;
	}, b || (a.jQuery = a.$ = r), r;
});

/*
 Copyright (C) Federico Zivolo 2017
 Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */(function (e, t) {
	'object' == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && 'undefined' != typeof module ? module.exports = t() : 'function' == typeof define && define.amd ? define(t) : e.Popper = t();
})(this, function () {
	'use strict';
	function e(e) {
		return e && '[object Function]' === {}.toString.call(e);
	}function t(e, t) {
		if (1 !== e.nodeType) return [];var o = getComputedStyle(e, null);return t ? o[t] : o;
	}function o(e) {
		return 'HTML' === e.nodeName ? e : e.parentNode || e.host;
	}function n(e) {
		if (!e) return document.body;switch (e.nodeName) {case 'HTML':case 'BODY':
				return e.ownerDocument.body;case '#document':
				return e.body;}var i = t(e),
		    r = i.overflow,
		    p = i.overflowX,
		    s = i.overflowY;return (/(auto|scroll)/.test(r + s + p) ? e : n(o(e))
		);
	}function r(e) {
		var o = e && e.offsetParent,
		    i = o && o.nodeName;return i && 'BODY' !== i && 'HTML' !== i ? -1 !== ['TD', 'TABLE'].indexOf(o.nodeName) && 'static' === t(o, 'position') ? r(o) : o : e ? e.ownerDocument.documentElement : document.documentElement;
	}function p(e) {
		var t = e.nodeName;return 'BODY' !== t && ('HTML' === t || r(e.firstElementChild) === e);
	}function s(e) {
		return null === e.parentNode ? e : s(e.parentNode);
	}function d(e, t) {
		if (!e || !e.nodeType || !t || !t.nodeType) return document.documentElement;var o = e.compareDocumentPosition(t) & Node.DOCUMENT_POSITION_FOLLOWING,
		    i = o ? e : t,
		    n = o ? t : e,
		    a = document.createRange();a.setStart(i, 0), a.setEnd(n, 0);var l = a.commonAncestorContainer;if (e !== l && t !== l || i.contains(n)) return p(l) ? l : r(l);var f = s(e);return f.host ? d(f.host, t) : d(e, s(t).host);
	}function a(e) {
		var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : 'top',
		    o = 'top' === t ? 'scrollTop' : 'scrollLeft',
		    i = e.nodeName;if ('BODY' === i || 'HTML' === i) {
			var n = e.ownerDocument.documentElement,
			    r = e.ownerDocument.scrollingElement || n;return r[o];
		}return e[o];
	}function l(e, t) {
		var o = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
		    i = a(t, 'top'),
		    n = a(t, 'left'),
		    r = o ? -1 : 1;return e.top += i * r, e.bottom += i * r, e.left += n * r, e.right += n * r, e;
	}function f(e, t) {
		var o = 'x' === t ? 'Left' : 'Top',
		    i = 'Left' == o ? 'Right' : 'Bottom';return parseFloat(e['border' + o + 'Width'], 10) + parseFloat(e['border' + i + 'Width'], 10);
	}function m(e, t, o, i) {
		return J(t['offset' + e], t['scroll' + e], o['client' + e], o['offset' + e], o['scroll' + e], ie() ? o['offset' + e] + i['margin' + ('Height' === e ? 'Top' : 'Left')] + i['margin' + ('Height' === e ? 'Bottom' : 'Right')] : 0);
	}function h() {
		var e = document.body,
		    t = document.documentElement,
		    o = ie() && getComputedStyle(t);return { height: m('Height', e, t, o), width: m('Width', e, t, o) };
	}function c(e) {
		return se({}, e, { right: e.left + e.width, bottom: e.top + e.height });
	}function g(e) {
		var o = {};if (ie()) try {
			o = e.getBoundingClientRect();var i = a(e, 'top'),
			    n = a(e, 'left');o.top += i, o.left += n, o.bottom += i, o.right += n;
		} catch (e) {} else o = e.getBoundingClientRect();var r = { left: o.left, top: o.top, width: o.right - o.left, height: o.bottom - o.top },
		    p = 'HTML' === e.nodeName ? h() : {},
		    s = p.width || e.clientWidth || r.right - r.left,
		    d = p.height || e.clientHeight || r.bottom - r.top,
		    l = e.offsetWidth - s,
		    m = e.offsetHeight - d;if (l || m) {
			var g = t(e);l -= f(g, 'x'), m -= f(g, 'y'), r.width -= l, r.height -= m;
		}return c(r);
	}function u(e, o) {
		var i = ie(),
		    r = 'HTML' === o.nodeName,
		    p = g(e),
		    s = g(o),
		    d = n(e),
		    a = t(o),
		    f = parseFloat(a.borderTopWidth, 10),
		    m = parseFloat(a.borderLeftWidth, 10),
		    h = c({ top: p.top - s.top - f, left: p.left - s.left - m, width: p.width, height: p.height });if (h.marginTop = 0, h.marginLeft = 0, !i && r) {
			var u = parseFloat(a.marginTop, 10),
			    b = parseFloat(a.marginLeft, 10);h.top -= f - u, h.bottom -= f - u, h.left -= m - b, h.right -= m - b, h.marginTop = u, h.marginLeft = b;
		}return (i ? o.contains(d) : o === d && 'BODY' !== d.nodeName) && (h = l(h, o)), h;
	}function b(e) {
		var t = e.ownerDocument.documentElement,
		    o = u(e, t),
		    i = J(t.clientWidth, window.innerWidth || 0),
		    n = J(t.clientHeight, window.innerHeight || 0),
		    r = a(t),
		    p = a(t, 'left'),
		    s = { top: r - o.top + o.marginTop, left: p - o.left + o.marginLeft, width: i, height: n };return c(s);
	}function w(e) {
		var i = e.nodeName;return 'BODY' === i || 'HTML' === i ? !1 : 'fixed' === t(e, 'position') || w(o(e));
	}function y(e, t, i, r) {
		var p = { top: 0, left: 0 },
		    s = d(e, t);if ('viewport' === r) p = b(s);else {
			var a;'scrollParent' === r ? (a = n(o(t)), 'BODY' === a.nodeName && (a = e.ownerDocument.documentElement)) : 'window' === r ? a = e.ownerDocument.documentElement : a = r;var l = u(a, s);if ('HTML' === a.nodeName && !w(s)) {
				var f = h(),
				    m = f.height,
				    c = f.width;p.top += l.top - l.marginTop, p.bottom = m + l.top, p.left += l.left - l.marginLeft, p.right = c + l.left;
			} else p = l;
		}return p.left += i, p.top += i, p.right -= i, p.bottom -= i, p;
	}function E(e) {
		var t = e.width,
		    o = e.height;return t * o;
	}function v(e, t, o, i, n) {
		var r = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;if (-1 === e.indexOf('auto')) return e;var p = y(o, i, r, n),
		    s = { top: { width: p.width, height: t.top - p.top }, right: { width: p.right - t.right, height: p.height }, bottom: { width: p.width, height: p.bottom - t.bottom }, left: { width: t.left - p.left, height: p.height } },
		    d = Object.keys(s).map(function (e) {
			return se({ key: e }, s[e], { area: E(s[e]) });
		}).sort(function (e, t) {
			return t.area - e.area;
		}),
		    a = d.filter(function (e) {
			var t = e.width,
			    i = e.height;return t >= o.clientWidth && i >= o.clientHeight;
		}),
		    l = 0 < a.length ? a[0].key : d[0].key,
		    f = e.split('-')[1];return l + (f ? '-' + f : '');
	}function O(e, t, o) {
		var i = d(t, o);return u(o, i);
	}function L(e) {
		var t = getComputedStyle(e),
		    o = parseFloat(t.marginTop) + parseFloat(t.marginBottom),
		    i = parseFloat(t.marginLeft) + parseFloat(t.marginRight),
		    n = { width: e.offsetWidth + i, height: e.offsetHeight + o };return n;
	}function x(e) {
		var t = { left: 'right', right: 'left', bottom: 'top', top: 'bottom' };return e.replace(/left|right|bottom|top/g, function (e) {
			return t[e];
		});
	}function S(e, t, o) {
		o = o.split('-')[0];var i = L(e),
		    n = { width: i.width, height: i.height },
		    r = -1 !== ['right', 'left'].indexOf(o),
		    p = r ? 'top' : 'left',
		    s = r ? 'left' : 'top',
		    d = r ? 'height' : 'width',
		    a = r ? 'width' : 'height';return n[p] = t[p] + t[d] / 2 - i[d] / 2, n[s] = o === s ? t[s] - i[a] : t[x(s)], n;
	}function T(e, t) {
		return Array.prototype.find ? e.find(t) : e.filter(t)[0];
	}function D(e, t, o) {
		if (Array.prototype.findIndex) return e.findIndex(function (e) {
			return e[t] === o;
		});var i = T(e, function (e) {
			return e[t] === o;
		});return e.indexOf(i);
	}function C(t, o, i) {
		var n = void 0 === i ? t : t.slice(0, D(t, 'name', i));return n.forEach(function (t) {
			t['function'] && console.warn('`modifier.function` is deprecated, use `modifier.fn`!');var i = t['function'] || t.fn;t.enabled && e(i) && (o.offsets.popper = c(o.offsets.popper), o.offsets.reference = c(o.offsets.reference), o = i(o, t));
		}), o;
	}function N() {
		if (!this.state.isDestroyed) {
			var e = { instance: this, styles: {}, arrowStyles: {}, attributes: {}, flipped: !1, offsets: {} };e.offsets.reference = O(this.state, this.popper, this.reference), e.placement = v(this.options.placement, e.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), e.originalPlacement = e.placement, e.offsets.popper = S(this.popper, e.offsets.reference, e.placement), e.offsets.popper.position = 'absolute', e = C(this.modifiers, e), this.state.isCreated ? this.options.onUpdate(e) : (this.state.isCreated = !0, this.options.onCreate(e));
		}
	}function k(e, t) {
		return e.some(function (e) {
			var o = e.name,
			    i = e.enabled;return i && o === t;
		});
	}function W(e) {
		for (var t = [!1, 'ms', 'Webkit', 'Moz', 'O'], o = e.charAt(0).toUpperCase() + e.slice(1), n = 0; n < t.length - 1; n++) {
			var i = t[n],
			    r = i ? '' + i + o : e;if ('undefined' != typeof document.body.style[r]) return r;
		}return null;
	}function P() {
		return this.state.isDestroyed = !0, k(this.modifiers, 'applyStyle') && (this.popper.removeAttribute('x-placement'), this.popper.style.left = '', this.popper.style.position = '', this.popper.style.top = '', this.popper.style[W('transform')] = ''), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this;
	}function B(e) {
		var t = e.ownerDocument;return t ? t.defaultView : window;
	}function H(e, t, o, i) {
		var r = 'BODY' === e.nodeName,
		    p = r ? e.ownerDocument.defaultView : e;p.addEventListener(t, o, { passive: !0 }), r || H(n(p.parentNode), t, o, i), i.push(p);
	}function A(e, t, o, i) {
		o.updateBound = i, B(e).addEventListener('resize', o.updateBound, { passive: !0 });var r = n(e);return H(r, 'scroll', o.updateBound, o.scrollParents), o.scrollElement = r, o.eventsEnabled = !0, o;
	}function I() {
		this.state.eventsEnabled || (this.state = A(this.reference, this.options, this.state, this.scheduleUpdate));
	}function M(e, t) {
		return B(e).removeEventListener('resize', t.updateBound), t.scrollParents.forEach(function (e) {
			e.removeEventListener('scroll', t.updateBound);
		}), t.updateBound = null, t.scrollParents = [], t.scrollElement = null, t.eventsEnabled = !1, t;
	}function R() {
		this.state.eventsEnabled && (cancelAnimationFrame(this.scheduleUpdate), this.state = M(this.reference, this.state));
	}function U(e) {
		return '' !== e && !isNaN(parseFloat(e)) && isFinite(e);
	}function Y(e, t) {
		Object.keys(t).forEach(function (o) {
			var i = '';-1 !== ['width', 'height', 'top', 'right', 'bottom', 'left'].indexOf(o) && U(t[o]) && (i = 'px'), e.style[o] = t[o] + i;
		});
	}function j(e, t) {
		Object.keys(t).forEach(function (o) {
			var i = t[o];!1 === i ? e.removeAttribute(o) : e.setAttribute(o, t[o]);
		});
	}function F(e, t, o) {
		var i = T(e, function (e) {
			var o = e.name;return o === t;
		}),
		    n = !!i && e.some(function (e) {
			return e.name === o && e.enabled && e.order < i.order;
		});if (!n) {
			var r = '`' + t + '`';console.warn('`' + o + '`' + ' modifier is required by ' + r + ' modifier in order to work, be sure to include it before ' + r + '!');
		}return n;
	}function K(e) {
		return 'end' === e ? 'start' : 'start' === e ? 'end' : e;
	}function q(e) {
		var t = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
		    o = ae.indexOf(e),
		    i = ae.slice(o + 1).concat(ae.slice(0, o));return t ? i.reverse() : i;
	}function V(e, t, o, i) {
		var n = e.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
		    r = +n[1],
		    p = n[2];if (!r) return e;if (0 === p.indexOf('%')) {
			var s;switch (p) {case '%p':
					s = o;break;case '%':case '%r':default:
					s = i;}var d = c(s);return d[t] / 100 * r;
		}if ('vh' === p || 'vw' === p) {
			var a;return a = 'vh' === p ? J(document.documentElement.clientHeight, window.innerHeight || 0) : J(document.documentElement.clientWidth, window.innerWidth || 0), a / 100 * r;
		}return r;
	}function z(e, t, o, i) {
		var n = [0, 0],
		    r = -1 !== ['right', 'left'].indexOf(i),
		    p = e.split(/(\+|\-)/).map(function (e) {
			return e.trim();
		}),
		    s = p.indexOf(T(p, function (e) {
			return -1 !== e.search(/,|\s/);
		}));p[s] && -1 === p[s].indexOf(',') && console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');var d = /\s*,\s*|\s+/,
		    a = -1 === s ? [p] : [p.slice(0, s).concat([p[s].split(d)[0]]), [p[s].split(d)[1]].concat(p.slice(s + 1))];return a = a.map(function (e, i) {
			var n = (1 === i ? !r : r) ? 'height' : 'width',
			    p = !1;return e.reduce(function (e, t) {
				return '' === e[e.length - 1] && -1 !== ['+', '-'].indexOf(t) ? (e[e.length - 1] = t, p = !0, e) : p ? (e[e.length - 1] += t, p = !1, e) : e.concat(t);
			}, []).map(function (e) {
				return V(e, n, t, o);
			});
		}), a.forEach(function (e, t) {
			e.forEach(function (o, i) {
				U(o) && (n[t] += o * ('-' === e[i - 1] ? -1 : 1));
			});
		}), n;
	}function G(e, t) {
		var o,
		    i = t.offset,
		    n = e.placement,
		    r = e.offsets,
		    p = r.popper,
		    s = r.reference,
		    d = n.split('-')[0];return o = U(+i) ? [+i, 0] : z(i, p, s, d), 'left' === d ? (p.top += o[0], p.left -= o[1]) : 'right' === d ? (p.top += o[0], p.left += o[1]) : 'top' === d ? (p.left += o[0], p.top -= o[1]) : 'bottom' === d && (p.left += o[0], p.top += o[1]), e.popper = p, e;
	}for (var _ = Math.min, X = Math.floor, J = Math.max, Q = 'undefined' != typeof window && 'undefined' != typeof document, Z = ['Edge', 'Trident', 'Firefox'], $ = 0, ee = 0; ee < Z.length; ee += 1) {
		if (Q && 0 <= navigator.userAgent.indexOf(Z[ee])) {
			$ = 1;break;
		}
	}var i,
	    te = Q && window.Promise,
	    oe = te ? function (e) {
		var t = !1;return function () {
			t || (t = !0, window.Promise.resolve().then(function () {
				t = !1, e();
			}));
		};
	} : function (e) {
		var t = !1;return function () {
			t || (t = !0, setTimeout(function () {
				t = !1, e();
			}, $));
		};
	},
	    ie = function ie() {
		return void 0 == i && (i = -1 !== navigator.appVersion.indexOf('MSIE 10')), i;
	},
	    ne = function ne(e, t) {
		if (!(e instanceof t)) throw new TypeError('Cannot call a class as a function');
	},
	    re = function () {
		function e(e, t) {
			for (var o, n = 0; n < t.length; n++) {
				o = t[n], o.enumerable = o.enumerable || !1, o.configurable = !0, 'value' in o && (o.writable = !0), Object.defineProperty(e, o.key, o);
			}
		}return function (t, o, i) {
			return o && e(t.prototype, o), i && e(t, i), t;
		};
	}(),
	    pe = function pe(e, t, o) {
		return t in e ? Object.defineProperty(e, t, { value: o, enumerable: !0, configurable: !0, writable: !0 }) : e[t] = o, e;
	},
	    se = Object.assign || function (e) {
		for (var t, o = 1; o < arguments.length; o++) {
			for (var i in t = arguments[o], t) {
				Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
			}
		}return e;
	},
	    de = ['auto-start', 'auto', 'auto-end', 'top-start', 'top', 'top-end', 'right-start', 'right', 'right-end', 'bottom-end', 'bottom', 'bottom-start', 'left-end', 'left', 'left-start'],
	    ae = de.slice(3),
	    le = { FLIP: 'flip', CLOCKWISE: 'clockwise', COUNTERCLOCKWISE: 'counterclockwise' },
	    fe = function () {
		function t(o, i) {
			var n = this,
			    r = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};ne(this, t), this.scheduleUpdate = function () {
				return requestAnimationFrame(n.update);
			}, this.update = oe(this.update.bind(this)), this.options = se({}, t.Defaults, r), this.state = { isDestroyed: !1, isCreated: !1, scrollParents: [] }, this.reference = o && o.jquery ? o[0] : o, this.popper = i && i.jquery ? i[0] : i, this.options.modifiers = {}, Object.keys(se({}, t.Defaults.modifiers, r.modifiers)).forEach(function (e) {
				n.options.modifiers[e] = se({}, t.Defaults.modifiers[e] || {}, r.modifiers ? r.modifiers[e] : {});
			}), this.modifiers = Object.keys(this.options.modifiers).map(function (e) {
				return se({ name: e }, n.options.modifiers[e]);
			}).sort(function (e, t) {
				return e.order - t.order;
			}), this.modifiers.forEach(function (t) {
				t.enabled && e(t.onLoad) && t.onLoad(n.reference, n.popper, n.options, t, n.state);
			}), this.update();var p = this.options.eventsEnabled;p && this.enableEventListeners(), this.state.eventsEnabled = p;
		}return re(t, [{ key: 'update', value: function value() {
				return N.call(this);
			} }, { key: 'destroy', value: function value() {
				return P.call(this);
			} }, { key: 'enableEventListeners', value: function value() {
				return I.call(this);
			} }, { key: 'disableEventListeners', value: function value() {
				return R.call(this);
			} }]), t;
	}();return fe.Utils = ('undefined' == typeof window ? global : window).PopperUtils, fe.placements = de, fe.Defaults = { placement: 'bottom', eventsEnabled: !0, removeOnDestroy: !1, onCreate: function onCreate() {}, onUpdate: function onUpdate() {}, modifiers: { shift: { order: 100, enabled: !0, fn: function fn(e) {
					var t = e.placement,
					    o = t.split('-')[0],
					    i = t.split('-')[1];if (i) {
						var n = e.offsets,
						    r = n.reference,
						    p = n.popper,
						    s = -1 !== ['bottom', 'top'].indexOf(o),
						    d = s ? 'left' : 'top',
						    a = s ? 'width' : 'height',
						    l = { start: pe({}, d, r[d]), end: pe({}, d, r[d] + r[a] - p[a]) };e.offsets.popper = se({}, p, l[i]);
					}return e;
				} }, offset: { order: 200, enabled: !0, fn: G, offset: 0 }, preventOverflow: { order: 300, enabled: !0, fn: function fn(e, t) {
					var o = t.boundariesElement || r(e.instance.popper);e.instance.reference === o && (o = r(o));var i = y(e.instance.popper, e.instance.reference, t.padding, o);t.boundaries = i;var n = t.priority,
					    p = e.offsets.popper,
					    s = { primary: function primary(e) {
							var o = p[e];return p[e] < i[e] && !t.escapeWithReference && (o = J(p[e], i[e])), pe({}, e, o);
						}, secondary: function secondary(e) {
							var o = 'right' === e ? 'left' : 'top',
							    n = p[o];return p[e] > i[e] && !t.escapeWithReference && (n = _(p[o], i[e] - ('right' === e ? p.width : p.height))), pe({}, o, n);
						} };return n.forEach(function (e) {
						var t = -1 === ['left', 'top'].indexOf(e) ? 'secondary' : 'primary';p = se({}, p, s[t](e));
					}), e.offsets.popper = p, e;
				}, priority: ['left', 'right', 'top', 'bottom'], padding: 5, boundariesElement: 'scrollParent' }, keepTogether: { order: 400, enabled: !0, fn: function fn(e) {
					var t = e.offsets,
					    o = t.popper,
					    i = t.reference,
					    n = e.placement.split('-')[0],
					    r = X,
					    p = -1 !== ['top', 'bottom'].indexOf(n),
					    s = p ? 'right' : 'bottom',
					    d = p ? 'left' : 'top',
					    a = p ? 'width' : 'height';return o[s] < r(i[d]) && (e.offsets.popper[d] = r(i[d]) - o[a]), o[d] > r(i[s]) && (e.offsets.popper[d] = r(i[s])), e;
				} }, arrow: { order: 500, enabled: !0, fn: function fn(e, o) {
					var i;if (!F(e.instance.modifiers, 'arrow', 'keepTogether')) return e;var n = o.element;if ('string' == typeof n) {
						if (n = e.instance.popper.querySelector(n), !n) return e;
					} else if (!e.instance.popper.contains(n)) return console.warn('WARNING: `arrow.element` must be child of its popper element!'), e;var r = e.placement.split('-')[0],
					    p = e.offsets,
					    s = p.popper,
					    d = p.reference,
					    a = -1 !== ['left', 'right'].indexOf(r),
					    l = a ? 'height' : 'width',
					    f = a ? 'Top' : 'Left',
					    m = f.toLowerCase(),
					    h = a ? 'left' : 'top',
					    g = a ? 'bottom' : 'right',
					    u = L(n)[l];d[g] - u < s[m] && (e.offsets.popper[m] -= s[m] - (d[g] - u)), d[m] + u > s[g] && (e.offsets.popper[m] += d[m] + u - s[g]), e.offsets.popper = c(e.offsets.popper);var b = d[m] + d[l] / 2 - u / 2,
					    w = t(e.instance.popper),
					    y = parseFloat(w['margin' + f], 10),
					    E = parseFloat(w['border' + f + 'Width'], 10),
					    v = b - e.offsets.popper[m] - y - E;return v = J(_(s[l] - u, v), 0), e.arrowElement = n, e.offsets.arrow = (i = {}, pe(i, m, Math.round(v)), pe(i, h, ''), i), e;
				}, element: '[x-arrow]' }, flip: { order: 600, enabled: !0, fn: function fn(e, t) {
					if (k(e.instance.modifiers, 'inner')) return e;if (e.flipped && e.placement === e.originalPlacement) return e;var o = y(e.instance.popper, e.instance.reference, t.padding, t.boundariesElement),
					    i = e.placement.split('-')[0],
					    n = x(i),
					    r = e.placement.split('-')[1] || '',
					    p = [];switch (t.behavior) {case le.FLIP:
							p = [i, n];break;case le.CLOCKWISE:
							p = q(i);break;case le.COUNTERCLOCKWISE:
							p = q(i, !0);break;default:
							p = t.behavior;}return p.forEach(function (s, d) {
						if (i !== s || p.length === d + 1) return e;i = e.placement.split('-')[0], n = x(i);var a = e.offsets.popper,
						    l = e.offsets.reference,
						    f = X,
						    m = 'left' === i && f(a.right) > f(l.left) || 'right' === i && f(a.left) < f(l.right) || 'top' === i && f(a.bottom) > f(l.top) || 'bottom' === i && f(a.top) < f(l.bottom),
						    h = f(a.left) < f(o.left),
						    c = f(a.right) > f(o.right),
						    g = f(a.top) < f(o.top),
						    u = f(a.bottom) > f(o.bottom),
						    b = 'left' === i && h || 'right' === i && c || 'top' === i && g || 'bottom' === i && u,
						    w = -1 !== ['top', 'bottom'].indexOf(i),
						    y = !!t.flipVariations && (w && 'start' === r && h || w && 'end' === r && c || !w && 'start' === r && g || !w && 'end' === r && u);(m || b || y) && (e.flipped = !0, (m || b) && (i = p[d + 1]), y && (r = K(r)), e.placement = i + (r ? '-' + r : ''), e.offsets.popper = se({}, e.offsets.popper, S(e.instance.popper, e.offsets.reference, e.placement)), e = C(e.instance.modifiers, e, 'flip'));
					}), e;
				}, behavior: 'flip', padding: 5, boundariesElement: 'viewport' }, inner: { order: 700, enabled: !1, fn: function fn(e) {
					var t = e.placement,
					    o = t.split('-')[0],
					    i = e.offsets,
					    n = i.popper,
					    r = i.reference,
					    p = -1 !== ['left', 'right'].indexOf(o),
					    s = -1 === ['top', 'left'].indexOf(o);return n[p ? 'left' : 'top'] = r[o] - (s ? n[p ? 'width' : 'height'] : 0), e.placement = x(t), e.offsets.popper = c(n), e;
				} }, hide: { order: 800, enabled: !0, fn: function fn(e) {
					if (!F(e.instance.modifiers, 'hide', 'preventOverflow')) return e;var t = e.offsets.reference,
					    o = T(e.instance.modifiers, function (e) {
						return 'preventOverflow' === e.name;
					}).boundaries;if (t.bottom < o.top || t.left > o.right || t.top > o.bottom || t.right < o.left) {
						if (!0 === e.hide) return e;e.hide = !0, e.attributes['x-out-of-boundaries'] = '';
					} else {
						if (!1 === e.hide) return e;e.hide = !1, e.attributes['x-out-of-boundaries'] = !1;
					}return e;
				} }, computeStyle: { order: 850, enabled: !0, fn: function fn(e, t) {
					var o = t.x,
					    i = t.y,
					    n = e.offsets.popper,
					    p = T(e.instance.modifiers, function (e) {
						return 'applyStyle' === e.name;
					}).gpuAcceleration;void 0 !== p && console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');var s,
					    d,
					    a = void 0 === p ? t.gpuAcceleration : p,
					    l = r(e.instance.popper),
					    f = g(l),
					    m = { position: n.position },
					    h = { left: X(n.left), top: X(n.top), bottom: X(n.bottom), right: X(n.right) },
					    c = 'bottom' === o ? 'top' : 'bottom',
					    u = 'right' === i ? 'left' : 'right',
					    b = W('transform');if (d = 'bottom' == c ? -f.height + h.bottom : h.top, s = 'right' == u ? -f.width + h.right : h.left, a && b) m[b] = 'translate3d(' + s + 'px, ' + d + 'px, 0)', m[c] = 0, m[u] = 0, m.willChange = 'transform';else {
						var w = 'bottom' == c ? -1 : 1,
						    y = 'right' == u ? -1 : 1;m[c] = d * w, m[u] = s * y, m.willChange = c + ', ' + u;
					}var E = { "x-placement": e.placement };return e.attributes = se({}, E, e.attributes), e.styles = se({}, m, e.styles), e.arrowStyles = se({}, e.offsets.arrow, e.arrowStyles), e;
				}, gpuAcceleration: !0, x: 'bottom', y: 'right' }, applyStyle: { order: 900, enabled: !0, fn: function fn(e) {
					return Y(e.instance.popper, e.styles), j(e.instance.popper, e.attributes), e.arrowElement && Object.keys(e.arrowStyles).length && Y(e.arrowElement, e.arrowStyles), e;
				}, onLoad: function onLoad(e, t, o, i, n) {
					var r = O(n, t, e),
					    p = v(o.placement, r, t, e, o.modifiers.flip.boundariesElement, o.modifiers.flip.padding);return t.setAttribute('x-placement', p), Y(t, { position: 'absolute' }), o;
				}, gpuAcceleration: void 0 } } }, fe;
});
//# sourceMappingURL=popper.min.js.map

/*!
  * Bootstrap v4.0.0 (https://getbootstrap.com)
  * Copyright 2011-2018 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  */
!function (t, e) {
	"object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? e(exports, require("jquery"), require("popper.js")) : "function" == typeof define && define.amd ? define(["exports", "jquery", "popper.js"], e) : e(t.bootstrap = {}, t.jQuery, t.Popper);
}(this, function (t, e, n) {
	"use strict";
	function i(t, e) {
		for (var n = 0; n < e.length; n++) {
			var i = e[n];i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
		}
	}function s(t, e, n) {
		return e && i(t.prototype, e), n && i(t, n), t;
	}function r() {
		return (r = Object.assign || function (t) {
			for (var e = 1; e < arguments.length; e++) {
				var n = arguments[e];for (var i in n) {
					Object.prototype.hasOwnProperty.call(n, i) && (t[i] = n[i]);
				}
			}return t;
		}).apply(this, arguments);
	}e = e && e.hasOwnProperty("default") ? e.default : e, n = n && n.hasOwnProperty("default") ? n.default : n;var o,
	    a,
	    l,
	    h,
	    c,
	    u,
	    f,
	    d,
	    _,
	    g,
	    p,
	    m,
	    v,
	    E,
	    T,
	    y,
	    C,
	    I,
	    A,
	    b,
	    D,
	    S,
	    w,
	    N,
	    O,
	    k,
	    P = function (t) {
		var e = !1;function n(e) {
			var n = this,
			    s = !1;return t(this).one(i.TRANSITION_END, function () {
				s = !0;
			}), setTimeout(function () {
				s || i.triggerTransitionEnd(n);
			}, e), this;
		}var i = { TRANSITION_END: "bsTransitionEnd", getUID: function getUID(t) {
				do {
					t += ~~(1e6 * Math.random());
				} while (document.getElementById(t));return t;
			}, getSelectorFromElement: function getSelectorFromElement(e) {
				var n,
				    i = e.getAttribute("data-target");i && "#" !== i || (i = e.getAttribute("href") || ""), "#" === i.charAt(0) && (n = i, i = n = "function" == typeof t.escapeSelector ? t.escapeSelector(n).substr(1) : n.replace(/(:|\.|\[|\]|,|=|@)/g, "\\$1"));try {
					return t(document).find(i).length > 0 ? i : null;
				} catch (t) {
					return null;
				}
			}, reflow: function reflow(t) {
				return t.offsetHeight;
			}, triggerTransitionEnd: function triggerTransitionEnd(n) {
				t(n).trigger(e.end);
			}, supportsTransitionEnd: function supportsTransitionEnd() {
				return Boolean(e);
			}, isElement: function isElement(t) {
				return (t[0] || t).nodeType;
			}, typeCheckConfig: function typeCheckConfig(t, e, n) {
				for (var s in n) {
					if (Object.prototype.hasOwnProperty.call(n, s)) {
						var r = n[s],
						    o = e[s],
						    a = o && i.isElement(o) ? "element" : (l = o, {}.toString.call(l).match(/\s([a-zA-Z]+)/)[1].toLowerCase());if (!new RegExp(r).test(a)) throw new Error(t.toUpperCase() + ': Option "' + s + '" provided type "' + a + '" but expected type "' + r + '".');
					}
				}var l;
			} };return e = ("undefined" == typeof window || !window.QUnit) && { end: "transitionend" }, t.fn.emulateTransitionEnd = n, i.supportsTransitionEnd() && (t.event.special[i.TRANSITION_END] = { bindType: e.end, delegateType: e.end, handle: function handle(e) {
				if (t(e.target).is(this)) return e.handleObj.handler.apply(this, arguments);
			} }), i;
	}(e),
	    L = (a = "alert", h = "." + (l = "bs.alert"), c = (o = e).fn[a], u = { CLOSE: "close" + h, CLOSED: "closed" + h, CLICK_DATA_API: "click" + h + ".data-api" }, f = "alert", d = "fade", _ = "show", g = function () {
		function t(t) {
			this._element = t;
		}var e = t.prototype;return e.close = function (t) {
			t = t || this._element;var e = this._getRootElement(t);this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e);
		}, e.dispose = function () {
			o.removeData(this._element, l), this._element = null;
		}, e._getRootElement = function (t) {
			var e = P.getSelectorFromElement(t),
			    n = !1;return e && (n = o(e)[0]), n || (n = o(t).closest("." + f)[0]), n;
		}, e._triggerCloseEvent = function (t) {
			var e = o.Event(u.CLOSE);return o(t).trigger(e), e;
		}, e._removeElement = function (t) {
			var e = this;o(t).removeClass(_), P.supportsTransitionEnd() && o(t).hasClass(d) ? o(t).one(P.TRANSITION_END, function (n) {
				return e._destroyElement(t, n);
			}).emulateTransitionEnd(150) : this._destroyElement(t);
		}, e._destroyElement = function (t) {
			o(t).detach().trigger(u.CLOSED).remove();
		}, t._jQueryInterface = function (e) {
			return this.each(function () {
				var n = o(this),
				    i = n.data(l);i || (i = new t(this), n.data(l, i)), "close" === e && i[e](this);
			});
		}, t._handleDismiss = function (t) {
			return function (e) {
				e && e.preventDefault(), t.close(this);
			};
		}, s(t, null, [{ key: "VERSION", get: function get() {
				return "4.0.0";
			} }]), t;
	}(), o(document).on(u.CLICK_DATA_API, '[data-dismiss="alert"]', g._handleDismiss(new g())), o.fn[a] = g._jQueryInterface, o.fn[a].Constructor = g, o.fn[a].noConflict = function () {
		return o.fn[a] = c, g._jQueryInterface;
	}, g),
	    R = (m = "button", E = "." + (v = "bs.button"), T = ".data-api", y = (p = e).fn[m], C = "active", I = "btn", A = "focus", b = '[data-toggle^="button"]', D = '[data-toggle="buttons"]', S = "input", w = ".active", N = ".btn", O = { CLICK_DATA_API: "click" + E + T, FOCUS_BLUR_DATA_API: "focus" + E + T + " blur" + E + T }, k = function () {
		function t(t) {
			this._element = t;
		}var e = t.prototype;return e.toggle = function () {
			var t = !0,
			    e = !0,
			    n = p(this._element).closest(D)[0];if (n) {
				var i = p(this._element).find(S)[0];if (i) {
					if ("radio" === i.type) if (i.checked && p(this._element).hasClass(C)) t = !1;else {
						var s = p(n).find(w)[0];s && p(s).removeClass(C);
					}if (t) {
						if (i.hasAttribute("disabled") || n.hasAttribute("disabled") || i.classList.contains("disabled") || n.classList.contains("disabled")) return;i.checked = !p(this._element).hasClass(C), p(i).trigger("change");
					}i.focus(), e = !1;
				}
			}e && this._element.setAttribute("aria-pressed", !p(this._element).hasClass(C)), t && p(this._element).toggleClass(C);
		}, e.dispose = function () {
			p.removeData(this._element, v), this._element = null;
		}, t._jQueryInterface = function (e) {
			return this.each(function () {
				var n = p(this).data(v);n || (n = new t(this), p(this).data(v, n)), "toggle" === e && n[e]();
			});
		}, s(t, null, [{ key: "VERSION", get: function get() {
				return "4.0.0";
			} }]), t;
	}(), p(document).on(O.CLICK_DATA_API, b, function (t) {
		t.preventDefault();var e = t.target;p(e).hasClass(I) || (e = p(e).closest(N)), k._jQueryInterface.call(p(e), "toggle");
	}).on(O.FOCUS_BLUR_DATA_API, b, function (t) {
		var e = p(t.target).closest(N)[0];p(e).toggleClass(A, /^focus(in)?$/.test(t.type));
	}), p.fn[m] = k._jQueryInterface, p.fn[m].Constructor = k, p.fn[m].noConflict = function () {
		return p.fn[m] = y, k._jQueryInterface;
	}, k),
	    j = function (t) {
		var e = "carousel",
		    n = "bs.carousel",
		    i = "." + n,
		    o = t.fn[e],
		    a = { interval: 5e3, keyboard: !0, slide: !1, pause: "hover", wrap: !0 },
		    l = { interval: "(number|boolean)", keyboard: "boolean", slide: "(boolean|string)", pause: "(string|boolean)", wrap: "boolean" },
		    h = "next",
		    c = "prev",
		    u = "left",
		    f = "right",
		    d = { SLIDE: "slide" + i, SLID: "slid" + i, KEYDOWN: "keydown" + i, MOUSEENTER: "mouseenter" + i, MOUSELEAVE: "mouseleave" + i, TOUCHEND: "touchend" + i, LOAD_DATA_API: "load" + i + ".data-api", CLICK_DATA_API: "click" + i + ".data-api" },
		    _ = "carousel",
		    g = "active",
		    p = "slide",
		    m = "carousel-item-right",
		    v = "carousel-item-left",
		    E = "carousel-item-next",
		    T = "carousel-item-prev",
		    y = { ACTIVE: ".active", ACTIVE_ITEM: ".active.carousel-item", ITEM: ".carousel-item", NEXT_PREV: ".carousel-item-next, .carousel-item-prev", INDICATORS: ".carousel-indicators", DATA_SLIDE: "[data-slide], [data-slide-to]", DATA_RIDE: '[data-ride="carousel"]' },
		    C = function () {
			function o(e, n) {
				this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this._config = this._getConfig(n), this._element = t(e)[0], this._indicatorsElement = t(this._element).find(y.INDICATORS)[0], this._addEventListeners();
			}var C = o.prototype;return C.next = function () {
				this._isSliding || this._slide(h);
			}, C.nextWhenVisible = function () {
				!document.hidden && t(this._element).is(":visible") && "hidden" !== t(this._element).css("visibility") && this.next();
			}, C.prev = function () {
				this._isSliding || this._slide(c);
			}, C.pause = function (e) {
				e || (this._isPaused = !0), t(this._element).find(y.NEXT_PREV)[0] && P.supportsTransitionEnd() && (P.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null;
			}, C.cycle = function (t) {
				t || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval));
			}, C.to = function (e) {
				var n = this;this._activeElement = t(this._element).find(y.ACTIVE_ITEM)[0];var i = this._getItemIndex(this._activeElement);if (!(e > this._items.length - 1 || e < 0)) if (this._isSliding) t(this._element).one(d.SLID, function () {
					return n.to(e);
				});else {
					if (i === e) return this.pause(), void this.cycle();var s = e > i ? h : c;this._slide(s, this._items[e]);
				}
			}, C.dispose = function () {
				t(this._element).off(i), t.removeData(this._element, n), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null;
			}, C._getConfig = function (t) {
				return t = r({}, a, t), P.typeCheckConfig(e, t, l), t;
			}, C._addEventListeners = function () {
				var e = this;this._config.keyboard && t(this._element).on(d.KEYDOWN, function (t) {
					return e._keydown(t);
				}), "hover" === this._config.pause && (t(this._element).on(d.MOUSEENTER, function (t) {
					return e.pause(t);
				}).on(d.MOUSELEAVE, function (t) {
					return e.cycle(t);
				}), "ontouchstart" in document.documentElement && t(this._element).on(d.TOUCHEND, function () {
					e.pause(), e.touchTimeout && clearTimeout(e.touchTimeout), e.touchTimeout = setTimeout(function (t) {
						return e.cycle(t);
					}, 500 + e._config.interval);
				}));
			}, C._keydown = function (t) {
				if (!/input|textarea/i.test(t.target.tagName)) switch (t.which) {case 37:
						t.preventDefault(), this.prev();break;case 39:
						t.preventDefault(), this.next();}
			}, C._getItemIndex = function (e) {
				return this._items = t.makeArray(t(e).parent().find(y.ITEM)), this._items.indexOf(e);
			}, C._getItemByDirection = function (t, e) {
				var n = t === h,
				    i = t === c,
				    s = this._getItemIndex(e),
				    r = this._items.length - 1;if ((i && 0 === s || n && s === r) && !this._config.wrap) return e;var o = (s + (t === c ? -1 : 1)) % this._items.length;return -1 === o ? this._items[this._items.length - 1] : this._items[o];
			}, C._triggerSlideEvent = function (e, n) {
				var i = this._getItemIndex(e),
				    s = this._getItemIndex(t(this._element).find(y.ACTIVE_ITEM)[0]),
				    r = t.Event(d.SLIDE, { relatedTarget: e, direction: n, from: s, to: i });return t(this._element).trigger(r), r;
			}, C._setActiveIndicatorElement = function (e) {
				if (this._indicatorsElement) {
					t(this._indicatorsElement).find(y.ACTIVE).removeClass(g);var n = this._indicatorsElement.children[this._getItemIndex(e)];n && t(n).addClass(g);
				}
			}, C._slide = function (e, n) {
				var i,
				    s,
				    r,
				    o = this,
				    a = t(this._element).find(y.ACTIVE_ITEM)[0],
				    l = this._getItemIndex(a),
				    c = n || a && this._getItemByDirection(e, a),
				    _ = this._getItemIndex(c),
				    C = Boolean(this._interval);if (e === h ? (i = v, s = E, r = u) : (i = m, s = T, r = f), c && t(c).hasClass(g)) this._isSliding = !1;else if (!this._triggerSlideEvent(c, r).isDefaultPrevented() && a && c) {
					this._isSliding = !0, C && this.pause(), this._setActiveIndicatorElement(c);var I = t.Event(d.SLID, { relatedTarget: c, direction: r, from: l, to: _ });P.supportsTransitionEnd() && t(this._element).hasClass(p) ? (t(c).addClass(s), P.reflow(c), t(a).addClass(i), t(c).addClass(i), t(a).one(P.TRANSITION_END, function () {
						t(c).removeClass(i + " " + s).addClass(g), t(a).removeClass(g + " " + s + " " + i), o._isSliding = !1, setTimeout(function () {
							return t(o._element).trigger(I);
						}, 0);
					}).emulateTransitionEnd(600)) : (t(a).removeClass(g), t(c).addClass(g), this._isSliding = !1, t(this._element).trigger(I)), C && this.cycle();
				}
			}, o._jQueryInterface = function (e) {
				return this.each(function () {
					var i = t(this).data(n),
					    s = r({}, a, t(this).data());"object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && (s = r({}, s, e));var l = "string" == typeof e ? e : s.slide;if (i || (i = new o(this, s), t(this).data(n, i)), "number" == typeof e) i.to(e);else if ("string" == typeof l) {
						if ("undefined" == typeof i[l]) throw new TypeError('No method named "' + l + '"');i[l]();
					} else s.interval && (i.pause(), i.cycle());
				});
			}, o._dataApiClickHandler = function (e) {
				var i = P.getSelectorFromElement(this);if (i) {
					var s = t(i)[0];if (s && t(s).hasClass(_)) {
						var a = r({}, t(s).data(), t(this).data()),
						    l = this.getAttribute("data-slide-to");l && (a.interval = !1), o._jQueryInterface.call(t(s), a), l && t(s).data(n).to(l), e.preventDefault();
					}
				}
			}, s(o, null, [{ key: "VERSION", get: function get() {
					return "4.0.0";
				} }, { key: "Default", get: function get() {
					return a;
				} }]), o;
		}();return t(document).on(d.CLICK_DATA_API, y.DATA_SLIDE, C._dataApiClickHandler), t(window).on(d.LOAD_DATA_API, function () {
			t(y.DATA_RIDE).each(function () {
				var e = t(this);C._jQueryInterface.call(e, e.data());
			});
		}), t.fn[e] = C._jQueryInterface, t.fn[e].Constructor = C, t.fn[e].noConflict = function () {
			return t.fn[e] = o, C._jQueryInterface;
		}, C;
	}(e),
	    H = function (t) {
		var e = "collapse",
		    n = "bs.collapse",
		    i = "." + n,
		    o = t.fn[e],
		    a = { toggle: !0, parent: "" },
		    l = { toggle: "boolean", parent: "(string|element)" },
		    h = { SHOW: "show" + i, SHOWN: "shown" + i, HIDE: "hide" + i, HIDDEN: "hidden" + i, CLICK_DATA_API: "click" + i + ".data-api" },
		    c = "show",
		    u = "collapse",
		    f = "collapsing",
		    d = "collapsed",
		    _ = "width",
		    g = "height",
		    p = { ACTIVES: ".show, .collapsing", DATA_TOGGLE: '[data-toggle="collapse"]' },
		    m = function () {
			function i(e, n) {
				this._isTransitioning = !1, this._element = e, this._config = this._getConfig(n), this._triggerArray = t.makeArray(t('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'));for (var i = t(p.DATA_TOGGLE), s = 0; s < i.length; s++) {
					var r = i[s],
					    o = P.getSelectorFromElement(r);null !== o && t(o).filter(e).length > 0 && (this._selector = o, this._triggerArray.push(r));
				}this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle();
			}var o = i.prototype;return o.toggle = function () {
				t(this._element).hasClass(c) ? this.hide() : this.show();
			}, o.show = function () {
				var e,
				    s,
				    r = this;if (!this._isTransitioning && !t(this._element).hasClass(c) && (this._parent && 0 === (e = t.makeArray(t(this._parent).find(p.ACTIVES).filter('[data-parent="' + this._config.parent + '"]'))).length && (e = null), !(e && (s = t(e).not(this._selector).data(n)) && s._isTransitioning))) {
					var o = t.Event(h.SHOW);if (t(this._element).trigger(o), !o.isDefaultPrevented()) {
						e && (i._jQueryInterface.call(t(e).not(this._selector), "hide"), s || t(e).data(n, null));var a = this._getDimension();t(this._element).removeClass(u).addClass(f), this._element.style[a] = 0, this._triggerArray.length > 0 && t(this._triggerArray).removeClass(d).attr("aria-expanded", !0), this.setTransitioning(!0);var l = function l() {
							t(r._element).removeClass(f).addClass(u).addClass(c), r._element.style[a] = "", r.setTransitioning(!1), t(r._element).trigger(h.SHOWN);
						};if (P.supportsTransitionEnd()) {
							var _ = "scroll" + (a[0].toUpperCase() + a.slice(1));t(this._element).one(P.TRANSITION_END, l).emulateTransitionEnd(600), this._element.style[a] = this._element[_] + "px";
						} else l();
					}
				}
			}, o.hide = function () {
				var e = this;if (!this._isTransitioning && t(this._element).hasClass(c)) {
					var n = t.Event(h.HIDE);if (t(this._element).trigger(n), !n.isDefaultPrevented()) {
						var i = this._getDimension();if (this._element.style[i] = this._element.getBoundingClientRect()[i] + "px", P.reflow(this._element), t(this._element).addClass(f).removeClass(u).removeClass(c), this._triggerArray.length > 0) for (var s = 0; s < this._triggerArray.length; s++) {
							var r = this._triggerArray[s],
							    o = P.getSelectorFromElement(r);if (null !== o) t(o).hasClass(c) || t(r).addClass(d).attr("aria-expanded", !1);
						}this.setTransitioning(!0);var a = function a() {
							e.setTransitioning(!1), t(e._element).removeClass(f).addClass(u).trigger(h.HIDDEN);
						};this._element.style[i] = "", P.supportsTransitionEnd() ? t(this._element).one(P.TRANSITION_END, a).emulateTransitionEnd(600) : a();
					}
				}
			}, o.setTransitioning = function (t) {
				this._isTransitioning = t;
			}, o.dispose = function () {
				t.removeData(this._element, n), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null;
			}, o._getConfig = function (t) {
				return (t = r({}, a, t)).toggle = Boolean(t.toggle), P.typeCheckConfig(e, t, l), t;
			}, o._getDimension = function () {
				return t(this._element).hasClass(_) ? _ : g;
			}, o._getParent = function () {
				var e = this,
				    n = null;P.isElement(this._config.parent) ? (n = this._config.parent, "undefined" != typeof this._config.parent.jquery && (n = this._config.parent[0])) : n = t(this._config.parent)[0];var s = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]';return t(n).find(s).each(function (t, n) {
					e._addAriaAndCollapsedClass(i._getTargetFromElement(n), [n]);
				}), n;
			}, o._addAriaAndCollapsedClass = function (e, n) {
				if (e) {
					var i = t(e).hasClass(c);n.length > 0 && t(n).toggleClass(d, !i).attr("aria-expanded", i);
				}
			}, i._getTargetFromElement = function (e) {
				var n = P.getSelectorFromElement(e);return n ? t(n)[0] : null;
			}, i._jQueryInterface = function (e) {
				return this.each(function () {
					var s = t(this),
					    o = s.data(n),
					    l = r({}, a, s.data(), "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e);if (!o && l.toggle && /show|hide/.test(e) && (l.toggle = !1), o || (o = new i(this, l), s.data(n, o)), "string" == typeof e) {
						if ("undefined" == typeof o[e]) throw new TypeError('No method named "' + e + '"');o[e]();
					}
				});
			}, s(i, null, [{ key: "VERSION", get: function get() {
					return "4.0.0";
				} }, { key: "Default", get: function get() {
					return a;
				} }]), i;
		}();return t(document).on(h.CLICK_DATA_API, p.DATA_TOGGLE, function (e) {
			"A" === e.currentTarget.tagName && e.preventDefault();var i = t(this),
			    s = P.getSelectorFromElement(this);t(s).each(function () {
				var e = t(this),
				    s = e.data(n) ? "toggle" : i.data();m._jQueryInterface.call(e, s);
			});
		}), t.fn[e] = m._jQueryInterface, t.fn[e].Constructor = m, t.fn[e].noConflict = function () {
			return t.fn[e] = o, m._jQueryInterface;
		}, m;
	}(e),
	    W = function (t) {
		var e = "dropdown",
		    i = "bs.dropdown",
		    o = "." + i,
		    a = ".data-api",
		    l = t.fn[e],
		    h = new RegExp("38|40|27"),
		    c = { HIDE: "hide" + o, HIDDEN: "hidden" + o, SHOW: "show" + o, SHOWN: "shown" + o, CLICK: "click" + o, CLICK_DATA_API: "click" + o + a, KEYDOWN_DATA_API: "keydown" + o + a, KEYUP_DATA_API: "keyup" + o + a },
		    u = "disabled",
		    f = "show",
		    d = "dropup",
		    _ = "dropright",
		    g = "dropleft",
		    p = "dropdown-menu-right",
		    m = "dropdown-menu-left",
		    v = "position-static",
		    E = '[data-toggle="dropdown"]',
		    T = ".dropdown form",
		    y = ".dropdown-menu",
		    C = ".navbar-nav",
		    I = ".dropdown-menu .dropdown-item:not(.disabled)",
		    A = "top-start",
		    b = "top-end",
		    D = "bottom-start",
		    S = "bottom-end",
		    w = "right-start",
		    N = "left-start",
		    O = { offset: 0, flip: !0, boundary: "scrollParent" },
		    k = { offset: "(number|string|function)", flip: "boolean", boundary: "(string|element)" },
		    L = function () {
			function a(t, e) {
				this._element = t, this._popper = null, this._config = this._getConfig(e), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners();
			}var l = a.prototype;return l.toggle = function () {
				if (!this._element.disabled && !t(this._element).hasClass(u)) {
					var e = a._getParentFromElement(this._element),
					    i = t(this._menu).hasClass(f);if (a._clearMenus(), !i) {
						var s = { relatedTarget: this._element },
						    r = t.Event(c.SHOW, s);if (t(e).trigger(r), !r.isDefaultPrevented()) {
							if (!this._inNavbar) {
								if ("undefined" == typeof n) throw new TypeError("Bootstrap dropdown require Popper.js (https://popper.js.org)");var o = this._element;t(e).hasClass(d) && (t(this._menu).hasClass(m) || t(this._menu).hasClass(p)) && (o = e), "scrollParent" !== this._config.boundary && t(e).addClass(v), this._popper = new n(o, this._menu, this._getPopperConfig());
							}"ontouchstart" in document.documentElement && 0 === t(e).closest(C).length && t("body").children().on("mouseover", null, t.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), t(this._menu).toggleClass(f), t(e).toggleClass(f).trigger(t.Event(c.SHOWN, s));
						}
					}
				}
			}, l.dispose = function () {
				t.removeData(this._element, i), t(this._element).off(o), this._element = null, this._menu = null, null !== this._popper && (this._popper.destroy(), this._popper = null);
			}, l.update = function () {
				this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate();
			}, l._addEventListeners = function () {
				var e = this;t(this._element).on(c.CLICK, function (t) {
					t.preventDefault(), t.stopPropagation(), e.toggle();
				});
			}, l._getConfig = function (n) {
				return n = r({}, this.constructor.Default, t(this._element).data(), n), P.typeCheckConfig(e, n, this.constructor.DefaultType), n;
			}, l._getMenuElement = function () {
				if (!this._menu) {
					var e = a._getParentFromElement(this._element);this._menu = t(e).find(y)[0];
				}return this._menu;
			}, l._getPlacement = function () {
				var e = t(this._element).parent(),
				    n = D;return e.hasClass(d) ? (n = A, t(this._menu).hasClass(p) && (n = b)) : e.hasClass(_) ? n = w : e.hasClass(g) ? n = N : t(this._menu).hasClass(p) && (n = S), n;
			}, l._detectNavbar = function () {
				return t(this._element).closest(".navbar").length > 0;
			}, l._getPopperConfig = function () {
				var t = this,
				    e = {};return "function" == typeof this._config.offset ? e.fn = function (e) {
					return e.offsets = r({}, e.offsets, t._config.offset(e.offsets) || {}), e;
				} : e.offset = this._config.offset, { placement: this._getPlacement(), modifiers: { offset: e, flip: { enabled: this._config.flip }, preventOverflow: { boundariesElement: this._config.boundary } } };
			}, a._jQueryInterface = function (e) {
				return this.each(function () {
					var n = t(this).data(i);if (n || (n = new a(this, "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) ? e : null), t(this).data(i, n)), "string" == typeof e) {
						if ("undefined" == typeof n[e]) throw new TypeError('No method named "' + e + '"');n[e]();
					}
				});
			}, a._clearMenus = function (e) {
				if (!e || 3 !== e.which && ("keyup" !== e.type || 9 === e.which)) for (var n = t.makeArray(t(E)), s = 0; s < n.length; s++) {
					var r = a._getParentFromElement(n[s]),
					    o = t(n[s]).data(i),
					    l = { relatedTarget: n[s] };if (o) {
						var h = o._menu;if (t(r).hasClass(f) && !(e && ("click" === e.type && /input|textarea/i.test(e.target.tagName) || "keyup" === e.type && 9 === e.which) && t.contains(r, e.target))) {
							var u = t.Event(c.HIDE, l);t(r).trigger(u), u.isDefaultPrevented() || ("ontouchstart" in document.documentElement && t("body").children().off("mouseover", null, t.noop), n[s].setAttribute("aria-expanded", "false"), t(h).removeClass(f), t(r).removeClass(f).trigger(t.Event(c.HIDDEN, l)));
						}
					}
				}
			}, a._getParentFromElement = function (e) {
				var n,
				    i = P.getSelectorFromElement(e);return i && (n = t(i)[0]), n || e.parentNode;
			}, a._dataApiKeydownHandler = function (e) {
				if ((/input|textarea/i.test(e.target.tagName) ? !(32 === e.which || 27 !== e.which && (40 !== e.which && 38 !== e.which || t(e.target).closest(y).length)) : h.test(e.which)) && (e.preventDefault(), e.stopPropagation(), !this.disabled && !t(this).hasClass(u))) {
					var n = a._getParentFromElement(this),
					    i = t(n).hasClass(f);if ((i || 27 === e.which && 32 === e.which) && (!i || 27 !== e.which && 32 !== e.which)) {
						var s = t(n).find(I).get();if (0 !== s.length) {
							var r = s.indexOf(e.target);38 === e.which && r > 0 && r--, 40 === e.which && r < s.length - 1 && r++, r < 0 && (r = 0), s[r].focus();
						}
					} else {
						if (27 === e.which) {
							var o = t(n).find(E)[0];t(o).trigger("focus");
						}t(this).trigger("click");
					}
				}
			}, s(a, null, [{ key: "VERSION", get: function get() {
					return "4.0.0";
				} }, { key: "Default", get: function get() {
					return O;
				} }, { key: "DefaultType", get: function get() {
					return k;
				} }]), a;
		}();return t(document).on(c.KEYDOWN_DATA_API, E, L._dataApiKeydownHandler).on(c.KEYDOWN_DATA_API, y, L._dataApiKeydownHandler).on(c.CLICK_DATA_API + " " + c.KEYUP_DATA_API, L._clearMenus).on(c.CLICK_DATA_API, E, function (e) {
			e.preventDefault(), e.stopPropagation(), L._jQueryInterface.call(t(this), "toggle");
		}).on(c.CLICK_DATA_API, T, function (t) {
			t.stopPropagation();
		}), t.fn[e] = L._jQueryInterface, t.fn[e].Constructor = L, t.fn[e].noConflict = function () {
			return t.fn[e] = l, L._jQueryInterface;
		}, L;
	}(e),
	    M = function (t) {
		var e = "modal",
		    n = "bs.modal",
		    i = "." + n,
		    o = t.fn.modal,
		    a = { backdrop: !0, keyboard: !0, focus: !0, show: !0 },
		    l = { backdrop: "(boolean|string)", keyboard: "boolean", focus: "boolean", show: "boolean" },
		    h = { HIDE: "hide" + i, HIDDEN: "hidden" + i, SHOW: "show" + i, SHOWN: "shown" + i, FOCUSIN: "focusin" + i, RESIZE: "resize" + i, CLICK_DISMISS: "click.dismiss" + i, KEYDOWN_DISMISS: "keydown.dismiss" + i, MOUSEUP_DISMISS: "mouseup.dismiss" + i, MOUSEDOWN_DISMISS: "mousedown.dismiss" + i, CLICK_DATA_API: "click" + i + ".data-api" },
		    c = "modal-scrollbar-measure",
		    u = "modal-backdrop",
		    f = "modal-open",
		    d = "fade",
		    _ = "show",
		    g = { DIALOG: ".modal-dialog", DATA_TOGGLE: '[data-toggle="modal"]', DATA_DISMISS: '[data-dismiss="modal"]', FIXED_CONTENT: ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top", STICKY_CONTENT: ".sticky-top", NAVBAR_TOGGLER: ".navbar-toggler" },
		    p = function () {
			function o(e, n) {
				this._config = this._getConfig(n), this._element = e, this._dialog = t(e).find(g.DIALOG)[0], this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._originalBodyPadding = 0, this._scrollbarWidth = 0;
			}var p = o.prototype;return p.toggle = function (t) {
				return this._isShown ? this.hide() : this.show(t);
			}, p.show = function (e) {
				var n = this;if (!this._isTransitioning && !this._isShown) {
					P.supportsTransitionEnd() && t(this._element).hasClass(d) && (this._isTransitioning = !0);var i = t.Event(h.SHOW, { relatedTarget: e });t(this._element).trigger(i), this._isShown || i.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), t(document.body).addClass(f), this._setEscapeEvent(), this._setResizeEvent(), t(this._element).on(h.CLICK_DISMISS, g.DATA_DISMISS, function (t) {
						return n.hide(t);
					}), t(this._dialog).on(h.MOUSEDOWN_DISMISS, function () {
						t(n._element).one(h.MOUSEUP_DISMISS, function (e) {
							t(e.target).is(n._element) && (n._ignoreBackdropClick = !0);
						});
					}), this._showBackdrop(function () {
						return n._showElement(e);
					}));
				}
			}, p.hide = function (e) {
				var n = this;if (e && e.preventDefault(), !this._isTransitioning && this._isShown) {
					var i = t.Event(h.HIDE);if (t(this._element).trigger(i), this._isShown && !i.isDefaultPrevented()) {
						this._isShown = !1;var s = P.supportsTransitionEnd() && t(this._element).hasClass(d);s && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), t(document).off(h.FOCUSIN), t(this._element).removeClass(_), t(this._element).off(h.CLICK_DISMISS), t(this._dialog).off(h.MOUSEDOWN_DISMISS), s ? t(this._element).one(P.TRANSITION_END, function (t) {
							return n._hideModal(t);
						}).emulateTransitionEnd(300) : this._hideModal();
					}
				}
			}, p.dispose = function () {
				t.removeData(this._element, n), t(window, document, this._element, this._backdrop).off(i), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._scrollbarWidth = null;
			}, p.handleUpdate = function () {
				this._adjustDialog();
			}, p._getConfig = function (t) {
				return t = r({}, a, t), P.typeCheckConfig(e, t, l), t;
			}, p._showElement = function (e) {
				var n = this,
				    i = P.supportsTransitionEnd() && t(this._element).hasClass(d);this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.scrollTop = 0, i && P.reflow(this._element), t(this._element).addClass(_), this._config.focus && this._enforceFocus();var s = t.Event(h.SHOWN, { relatedTarget: e }),
				    r = function r() {
					n._config.focus && n._element.focus(), n._isTransitioning = !1, t(n._element).trigger(s);
				};i ? t(this._dialog).one(P.TRANSITION_END, r).emulateTransitionEnd(300) : r();
			}, p._enforceFocus = function () {
				var e = this;t(document).off(h.FOCUSIN).on(h.FOCUSIN, function (n) {
					document !== n.target && e._element !== n.target && 0 === t(e._element).has(n.target).length && e._element.focus();
				});
			}, p._setEscapeEvent = function () {
				var e = this;this._isShown && this._config.keyboard ? t(this._element).on(h.KEYDOWN_DISMISS, function (t) {
					27 === t.which && (t.preventDefault(), e.hide());
				}) : this._isShown || t(this._element).off(h.KEYDOWN_DISMISS);
			}, p._setResizeEvent = function () {
				var e = this;this._isShown ? t(window).on(h.RESIZE, function (t) {
					return e.handleUpdate(t);
				}) : t(window).off(h.RESIZE);
			}, p._hideModal = function () {
				var e = this;this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._isTransitioning = !1, this._showBackdrop(function () {
					t(document.body).removeClass(f), e._resetAdjustments(), e._resetScrollbar(), t(e._element).trigger(h.HIDDEN);
				});
			}, p._removeBackdrop = function () {
				this._backdrop && (t(this._backdrop).remove(), this._backdrop = null);
			}, p._showBackdrop = function (e) {
				var n = this,
				    i = t(this._element).hasClass(d) ? d : "";if (this._isShown && this._config.backdrop) {
					var s = P.supportsTransitionEnd() && i;if (this._backdrop = document.createElement("div"), this._backdrop.className = u, i && t(this._backdrop).addClass(i), t(this._backdrop).appendTo(document.body), t(this._element).on(h.CLICK_DISMISS, function (t) {
						n._ignoreBackdropClick ? n._ignoreBackdropClick = !1 : t.target === t.currentTarget && ("static" === n._config.backdrop ? n._element.focus() : n.hide());
					}), s && P.reflow(this._backdrop), t(this._backdrop).addClass(_), !e) return;if (!s) return void e();t(this._backdrop).one(P.TRANSITION_END, e).emulateTransitionEnd(150);
				} else if (!this._isShown && this._backdrop) {
					t(this._backdrop).removeClass(_);var r = function r() {
						n._removeBackdrop(), e && e();
					};P.supportsTransitionEnd() && t(this._element).hasClass(d) ? t(this._backdrop).one(P.TRANSITION_END, r).emulateTransitionEnd(150) : r();
				} else e && e();
			}, p._adjustDialog = function () {
				var t = this._element.scrollHeight > document.documentElement.clientHeight;!this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px");
			}, p._resetAdjustments = function () {
				this._element.style.paddingLeft = "", this._element.style.paddingRight = "";
			}, p._checkScrollbar = function () {
				var t = document.body.getBoundingClientRect();this._isBodyOverflowing = t.left + t.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth();
			}, p._setScrollbar = function () {
				var e = this;if (this._isBodyOverflowing) {
					t(g.FIXED_CONTENT).each(function (n, i) {
						var s = t(i)[0].style.paddingRight,
						    r = t(i).css("padding-right");t(i).data("padding-right", s).css("padding-right", parseFloat(r) + e._scrollbarWidth + "px");
					}), t(g.STICKY_CONTENT).each(function (n, i) {
						var s = t(i)[0].style.marginRight,
						    r = t(i).css("margin-right");t(i).data("margin-right", s).css("margin-right", parseFloat(r) - e._scrollbarWidth + "px");
					}), t(g.NAVBAR_TOGGLER).each(function (n, i) {
						var s = t(i)[0].style.marginRight,
						    r = t(i).css("margin-right");t(i).data("margin-right", s).css("margin-right", parseFloat(r) + e._scrollbarWidth + "px");
					});var n = document.body.style.paddingRight,
					    i = t("body").css("padding-right");t("body").data("padding-right", n).css("padding-right", parseFloat(i) + this._scrollbarWidth + "px");
				}
			}, p._resetScrollbar = function () {
				t(g.FIXED_CONTENT).each(function (e, n) {
					var i = t(n).data("padding-right");"undefined" != typeof i && t(n).css("padding-right", i).removeData("padding-right");
				}), t(g.STICKY_CONTENT + ", " + g.NAVBAR_TOGGLER).each(function (e, n) {
					var i = t(n).data("margin-right");"undefined" != typeof i && t(n).css("margin-right", i).removeData("margin-right");
				});var e = t("body").data("padding-right");"undefined" != typeof e && t("body").css("padding-right", e).removeData("padding-right");
			}, p._getScrollbarWidth = function () {
				var t = document.createElement("div");t.className = c, document.body.appendChild(t);var e = t.getBoundingClientRect().width - t.clientWidth;return document.body.removeChild(t), e;
			}, o._jQueryInterface = function (e, i) {
				return this.each(function () {
					var s = t(this).data(n),
					    a = r({}, o.Default, t(this).data(), "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e);if (s || (s = new o(this, a), t(this).data(n, s)), "string" == typeof e) {
						if ("undefined" == typeof s[e]) throw new TypeError('No method named "' + e + '"');s[e](i);
					} else a.show && s.show(i);
				});
			}, s(o, null, [{ key: "VERSION", get: function get() {
					return "4.0.0";
				} }, { key: "Default", get: function get() {
					return a;
				} }]), o;
		}();return t(document).on(h.CLICK_DATA_API, g.DATA_TOGGLE, function (e) {
			var i,
			    s = this,
			    o = P.getSelectorFromElement(this);o && (i = t(o)[0]);var a = t(i).data(n) ? "toggle" : r({}, t(i).data(), t(this).data());"A" !== this.tagName && "AREA" !== this.tagName || e.preventDefault();var l = t(i).one(h.SHOW, function (e) {
				e.isDefaultPrevented() || l.one(h.HIDDEN, function () {
					t(s).is(":visible") && s.focus();
				});
			});p._jQueryInterface.call(t(i), a, this);
		}), t.fn.modal = p._jQueryInterface, t.fn.modal.Constructor = p, t.fn.modal.noConflict = function () {
			return t.fn.modal = o, p._jQueryInterface;
		}, p;
	}(e),
	    U = function (t) {
		var e = "tooltip",
		    i = "bs.tooltip",
		    o = "." + i,
		    a = t.fn[e],
		    l = new RegExp("(^|\\s)bs-tooltip\\S+", "g"),
		    h = { animation: "boolean", template: "string", title: "(string|element|function)", trigger: "string", delay: "(number|object)", html: "boolean", selector: "(string|boolean)", placement: "(string|function)", offset: "(number|string)", container: "(string|element|boolean)", fallbackPlacement: "(string|array)", boundary: "(string|element)" },
		    c = { AUTO: "auto", TOP: "top", RIGHT: "right", BOTTOM: "bottom", LEFT: "left" },
		    u = { animation: !0, template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>', trigger: "hover focus", title: "", delay: 0, html: !1, selector: !1, placement: "top", offset: 0, container: !1, fallbackPlacement: "flip", boundary: "scrollParent" },
		    f = "show",
		    d = "out",
		    _ = { HIDE: "hide" + o, HIDDEN: "hidden" + o, SHOW: "show" + o, SHOWN: "shown" + o, INSERTED: "inserted" + o, CLICK: "click" + o, FOCUSIN: "focusin" + o, FOCUSOUT: "focusout" + o, MOUSEENTER: "mouseenter" + o, MOUSELEAVE: "mouseleave" + o },
		    g = "fade",
		    p = "show",
		    m = ".tooltip-inner",
		    v = ".arrow",
		    E = "hover",
		    T = "focus",
		    y = "click",
		    C = "manual",
		    I = function () {
			function a(t, e) {
				if ("undefined" == typeof n) throw new TypeError("Bootstrap tooltips require Popper.js (https://popper.js.org)");this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = t, this.config = this._getConfig(e), this.tip = null, this._setListeners();
			}var I = a.prototype;return I.enable = function () {
				this._isEnabled = !0;
			}, I.disable = function () {
				this._isEnabled = !1;
			}, I.toggleEnabled = function () {
				this._isEnabled = !this._isEnabled;
			}, I.toggle = function (e) {
				if (this._isEnabled) if (e) {
					var n = this.constructor.DATA_KEY,
					    i = t(e.currentTarget).data(n);i || (i = new this.constructor(e.currentTarget, this._getDelegateConfig()), t(e.currentTarget).data(n, i)), i._activeTrigger.click = !i._activeTrigger.click, i._isWithActiveTrigger() ? i._enter(null, i) : i._leave(null, i);
				} else {
					if (t(this.getTipElement()).hasClass(p)) return void this._leave(null, this);this._enter(null, this);
				}
			}, I.dispose = function () {
				clearTimeout(this._timeout), t.removeData(this.element, this.constructor.DATA_KEY), t(this.element).off(this.constructor.EVENT_KEY), t(this.element).closest(".modal").off("hide.bs.modal"), this.tip && t(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, this._activeTrigger = null, null !== this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null;
			}, I.show = function () {
				var e = this;if ("none" === t(this.element).css("display")) throw new Error("Please use show on visible elements");var i = t.Event(this.constructor.Event.SHOW);if (this.isWithContent() && this._isEnabled) {
					t(this.element).trigger(i);var s = t.contains(this.element.ownerDocument.documentElement, this.element);if (i.isDefaultPrevented() || !s) return;var r = this.getTipElement(),
					    o = P.getUID(this.constructor.NAME);r.setAttribute("id", o), this.element.setAttribute("aria-describedby", o), this.setContent(), this.config.animation && t(r).addClass(g);var l = "function" == typeof this.config.placement ? this.config.placement.call(this, r, this.element) : this.config.placement,
					    h = this._getAttachment(l);this.addAttachmentClass(h);var c = !1 === this.config.container ? document.body : t(this.config.container);t(r).data(this.constructor.DATA_KEY, this), t.contains(this.element.ownerDocument.documentElement, this.tip) || t(r).appendTo(c), t(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new n(this.element, r, { placement: h, modifiers: { offset: { offset: this.config.offset }, flip: { behavior: this.config.fallbackPlacement }, arrow: { element: v }, preventOverflow: { boundariesElement: this.config.boundary } }, onCreate: function onCreate(t) {
							t.originalPlacement !== t.placement && e._handlePopperPlacementChange(t);
						}, onUpdate: function onUpdate(t) {
							e._handlePopperPlacementChange(t);
						} }), t(r).addClass(p), "ontouchstart" in document.documentElement && t("body").children().on("mouseover", null, t.noop);var u = function u() {
						e.config.animation && e._fixTransition();var n = e._hoverState;e._hoverState = null, t(e.element).trigger(e.constructor.Event.SHOWN), n === d && e._leave(null, e);
					};P.supportsTransitionEnd() && t(this.tip).hasClass(g) ? t(this.tip).one(P.TRANSITION_END, u).emulateTransitionEnd(a._TRANSITION_DURATION) : u();
				}
			}, I.hide = function (e) {
				var n = this,
				    i = this.getTipElement(),
				    s = t.Event(this.constructor.Event.HIDE),
				    r = function r() {
					n._hoverState !== f && i.parentNode && i.parentNode.removeChild(i), n._cleanTipClass(), n.element.removeAttribute("aria-describedby"), t(n.element).trigger(n.constructor.Event.HIDDEN), null !== n._popper && n._popper.destroy(), e && e();
				};t(this.element).trigger(s), s.isDefaultPrevented() || (t(i).removeClass(p), "ontouchstart" in document.documentElement && t("body").children().off("mouseover", null, t.noop), this._activeTrigger[y] = !1, this._activeTrigger[T] = !1, this._activeTrigger[E] = !1, P.supportsTransitionEnd() && t(this.tip).hasClass(g) ? t(i).one(P.TRANSITION_END, r).emulateTransitionEnd(150) : r(), this._hoverState = "");
			}, I.update = function () {
				null !== this._popper && this._popper.scheduleUpdate();
			}, I.isWithContent = function () {
				return Boolean(this.getTitle());
			}, I.addAttachmentClass = function (e) {
				t(this.getTipElement()).addClass("bs-tooltip-" + e);
			}, I.getTipElement = function () {
				return this.tip = this.tip || t(this.config.template)[0], this.tip;
			}, I.setContent = function () {
				var e = t(this.getTipElement());this.setElementContent(e.find(m), this.getTitle()), e.removeClass(g + " " + p);
			}, I.setElementContent = function (e, n) {
				var i = this.config.html;"object" == (typeof n === "undefined" ? "undefined" : _typeof(n)) && (n.nodeType || n.jquery) ? i ? t(n).parent().is(e) || e.empty().append(n) : e.text(t(n).text()) : e[i ? "html" : "text"](n);
			}, I.getTitle = function () {
				var t = this.element.getAttribute("data-original-title");return t || (t = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), t;
			}, I._getAttachment = function (t) {
				return c[t.toUpperCase()];
			}, I._setListeners = function () {
				var e = this;this.config.trigger.split(" ").forEach(function (n) {
					if ("click" === n) t(e.element).on(e.constructor.Event.CLICK, e.config.selector, function (t) {
						return e.toggle(t);
					});else if (n !== C) {
						var i = n === E ? e.constructor.Event.MOUSEENTER : e.constructor.Event.FOCUSIN,
						    s = n === E ? e.constructor.Event.MOUSELEAVE : e.constructor.Event.FOCUSOUT;t(e.element).on(i, e.config.selector, function (t) {
							return e._enter(t);
						}).on(s, e.config.selector, function (t) {
							return e._leave(t);
						});
					}t(e.element).closest(".modal").on("hide.bs.modal", function () {
						return e.hide();
					});
				}), this.config.selector ? this.config = r({}, this.config, { trigger: "manual", selector: "" }) : this._fixTitle();
			}, I._fixTitle = function () {
				var t = _typeof(this.element.getAttribute("data-original-title"));(this.element.getAttribute("title") || "string" !== t) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""));
			}, I._enter = function (e, n) {
				var i = this.constructor.DATA_KEY;(n = n || t(e.currentTarget).data(i)) || (n = new this.constructor(e.currentTarget, this._getDelegateConfig()), t(e.currentTarget).data(i, n)), e && (n._activeTrigger["focusin" === e.type ? T : E] = !0), t(n.getTipElement()).hasClass(p) || n._hoverState === f ? n._hoverState = f : (clearTimeout(n._timeout), n._hoverState = f, n.config.delay && n.config.delay.show ? n._timeout = setTimeout(function () {
					n._hoverState === f && n.show();
				}, n.config.delay.show) : n.show());
			}, I._leave = function (e, n) {
				var i = this.constructor.DATA_KEY;(n = n || t(e.currentTarget).data(i)) || (n = new this.constructor(e.currentTarget, this._getDelegateConfig()), t(e.currentTarget).data(i, n)), e && (n._activeTrigger["focusout" === e.type ? T : E] = !1), n._isWithActiveTrigger() || (clearTimeout(n._timeout), n._hoverState = d, n.config.delay && n.config.delay.hide ? n._timeout = setTimeout(function () {
					n._hoverState === d && n.hide();
				}, n.config.delay.hide) : n.hide());
			}, I._isWithActiveTrigger = function () {
				for (var t in this._activeTrigger) {
					if (this._activeTrigger[t]) return !0;
				}return !1;
			}, I._getConfig = function (n) {
				return "number" == typeof (n = r({}, this.constructor.Default, t(this.element).data(), n)).delay && (n.delay = { show: n.delay, hide: n.delay }), "number" == typeof n.title && (n.title = n.title.toString()), "number" == typeof n.content && (n.content = n.content.toString()), P.typeCheckConfig(e, n, this.constructor.DefaultType), n;
			}, I._getDelegateConfig = function () {
				var t = {};if (this.config) for (var e in this.config) {
					this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
				}return t;
			}, I._cleanTipClass = function () {
				var e = t(this.getTipElement()),
				    n = e.attr("class").match(l);null !== n && n.length > 0 && e.removeClass(n.join(""));
			}, I._handlePopperPlacementChange = function (t) {
				this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(t.placement));
			}, I._fixTransition = function () {
				var e = this.getTipElement(),
				    n = this.config.animation;null === e.getAttribute("x-placement") && (t(e).removeClass(g), this.config.animation = !1, this.hide(), this.show(), this.config.animation = n);
			}, a._jQueryInterface = function (e) {
				return this.each(function () {
					var n = t(this).data(i),
					    s = "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e;if ((n || !/dispose|hide/.test(e)) && (n || (n = new a(this, s), t(this).data(i, n)), "string" == typeof e)) {
						if ("undefined" == typeof n[e]) throw new TypeError('No method named "' + e + '"');n[e]();
					}
				});
			}, s(a, null, [{ key: "VERSION", get: function get() {
					return "4.0.0";
				} }, { key: "Default", get: function get() {
					return u;
				} }, { key: "NAME", get: function get() {
					return e;
				} }, { key: "DATA_KEY", get: function get() {
					return i;
				} }, { key: "Event", get: function get() {
					return _;
				} }, { key: "EVENT_KEY", get: function get() {
					return o;
				} }, { key: "DefaultType", get: function get() {
					return h;
				} }]), a;
		}();return t.fn[e] = I._jQueryInterface, t.fn[e].Constructor = I, t.fn[e].noConflict = function () {
			return t.fn[e] = a, I._jQueryInterface;
		}, I;
	}(e),
	    x = function (t) {
		var e = "popover",
		    n = "bs.popover",
		    i = "." + n,
		    o = t.fn[e],
		    a = new RegExp("(^|\\s)bs-popover\\S+", "g"),
		    l = r({}, U.Default, { placement: "right", trigger: "click", content: "", template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>' }),
		    h = r({}, U.DefaultType, { content: "(string|element|function)" }),
		    c = "fade",
		    u = "show",
		    f = ".popover-header",
		    d = ".popover-body",
		    _ = { HIDE: "hide" + i, HIDDEN: "hidden" + i, SHOW: "show" + i, SHOWN: "shown" + i, INSERTED: "inserted" + i, CLICK: "click" + i, FOCUSIN: "focusin" + i, FOCUSOUT: "focusout" + i, MOUSEENTER: "mouseenter" + i, MOUSELEAVE: "mouseleave" + i },
		    g = function (r) {
			var o, g;function p() {
				return r.apply(this, arguments) || this;
			}g = r, (o = p).prototype = Object.create(g.prototype), o.prototype.constructor = o, o.__proto__ = g;var m = p.prototype;return m.isWithContent = function () {
				return this.getTitle() || this._getContent();
			}, m.addAttachmentClass = function (e) {
				t(this.getTipElement()).addClass("bs-popover-" + e);
			}, m.getTipElement = function () {
				return this.tip = this.tip || t(this.config.template)[0], this.tip;
			}, m.setContent = function () {
				var e = t(this.getTipElement());this.setElementContent(e.find(f), this.getTitle());var n = this._getContent();"function" == typeof n && (n = n.call(this.element)), this.setElementContent(e.find(d), n), e.removeClass(c + " " + u);
			}, m._getContent = function () {
				return this.element.getAttribute("data-content") || this.config.content;
			}, m._cleanTipClass = function () {
				var e = t(this.getTipElement()),
				    n = e.attr("class").match(a);null !== n && n.length > 0 && e.removeClass(n.join(""));
			}, p._jQueryInterface = function (e) {
				return this.each(function () {
					var i = t(this).data(n),
					    s = "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) ? e : null;if ((i || !/destroy|hide/.test(e)) && (i || (i = new p(this, s), t(this).data(n, i)), "string" == typeof e)) {
						if ("undefined" == typeof i[e]) throw new TypeError('No method named "' + e + '"');i[e]();
					}
				});
			}, s(p, null, [{ key: "VERSION", get: function get() {
					return "4.0.0";
				} }, { key: "Default", get: function get() {
					return l;
				} }, { key: "NAME", get: function get() {
					return e;
				} }, { key: "DATA_KEY", get: function get() {
					return n;
				} }, { key: "Event", get: function get() {
					return _;
				} }, { key: "EVENT_KEY", get: function get() {
					return i;
				} }, { key: "DefaultType", get: function get() {
					return h;
				} }]), p;
		}(U);return t.fn[e] = g._jQueryInterface, t.fn[e].Constructor = g, t.fn[e].noConflict = function () {
			return t.fn[e] = o, g._jQueryInterface;
		}, g;
	}(e),
	    K = function (t) {
		var e = "scrollspy",
		    n = "bs.scrollspy",
		    i = "." + n,
		    o = t.fn[e],
		    a = { offset: 10, method: "auto", target: "" },
		    l = { offset: "number", method: "string", target: "(string|element)" },
		    h = { ACTIVATE: "activate" + i, SCROLL: "scroll" + i, LOAD_DATA_API: "load" + i + ".data-api" },
		    c = "dropdown-item",
		    u = "active",
		    f = { DATA_SPY: '[data-spy="scroll"]', ACTIVE: ".active", NAV_LIST_GROUP: ".nav, .list-group", NAV_LINKS: ".nav-link", NAV_ITEMS: ".nav-item", LIST_ITEMS: ".list-group-item", DROPDOWN: ".dropdown", DROPDOWN_ITEMS: ".dropdown-item", DROPDOWN_TOGGLE: ".dropdown-toggle" },
		    d = "offset",
		    _ = "position",
		    g = function () {
			function o(e, n) {
				var i = this;this._element = e, this._scrollElement = "BODY" === e.tagName ? window : e, this._config = this._getConfig(n), this._selector = this._config.target + " " + f.NAV_LINKS + "," + this._config.target + " " + f.LIST_ITEMS + "," + this._config.target + " " + f.DROPDOWN_ITEMS, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, t(this._scrollElement).on(h.SCROLL, function (t) {
					return i._process(t);
				}), this.refresh(), this._process();
			}var g = o.prototype;return g.refresh = function () {
				var e = this,
				    n = this._scrollElement === this._scrollElement.window ? d : _,
				    i = "auto" === this._config.method ? n : this._config.method,
				    s = i === _ ? this._getScrollTop() : 0;this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), t.makeArray(t(this._selector)).map(function (e) {
					var n,
					    r = P.getSelectorFromElement(e);if (r && (n = t(r)[0]), n) {
						var o = n.getBoundingClientRect();if (o.width || o.height) return [t(n)[i]().top + s, r];
					}return null;
				}).filter(function (t) {
					return t;
				}).sort(function (t, e) {
					return t[0] - e[0];
				}).forEach(function (t) {
					e._offsets.push(t[0]), e._targets.push(t[1]);
				});
			}, g.dispose = function () {
				t.removeData(this._element, n), t(this._scrollElement).off(i), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null;
			}, g._getConfig = function (n) {
				if ("string" != typeof (n = r({}, a, n)).target) {
					var i = t(n.target).attr("id");i || (i = P.getUID(e), t(n.target).attr("id", i)), n.target = "#" + i;
				}return P.typeCheckConfig(e, n, l), n;
			}, g._getScrollTop = function () {
				return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
			}, g._getScrollHeight = function () {
				return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
			}, g._getOffsetHeight = function () {
				return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
			}, g._process = function () {
				var t = this._getScrollTop() + this._config.offset,
				    e = this._getScrollHeight(),
				    n = this._config.offset + e - this._getOffsetHeight();if (this._scrollHeight !== e && this.refresh(), t >= n) {
					var i = this._targets[this._targets.length - 1];this._activeTarget !== i && this._activate(i);
				} else {
					if (this._activeTarget && t < this._offsets[0] && this._offsets[0] > 0) return this._activeTarget = null, void this._clear();for (var s = this._offsets.length; s--;) {
						this._activeTarget !== this._targets[s] && t >= this._offsets[s] && ("undefined" == typeof this._offsets[s + 1] || t < this._offsets[s + 1]) && this._activate(this._targets[s]);
					}
				}
			}, g._activate = function (e) {
				this._activeTarget = e, this._clear();var n = this._selector.split(",");n = n.map(function (t) {
					return t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]';
				});var i = t(n.join(","));i.hasClass(c) ? (i.closest(f.DROPDOWN).find(f.DROPDOWN_TOGGLE).addClass(u), i.addClass(u)) : (i.addClass(u), i.parents(f.NAV_LIST_GROUP).prev(f.NAV_LINKS + ", " + f.LIST_ITEMS).addClass(u), i.parents(f.NAV_LIST_GROUP).prev(f.NAV_ITEMS).children(f.NAV_LINKS).addClass(u)), t(this._scrollElement).trigger(h.ACTIVATE, { relatedTarget: e });
			}, g._clear = function () {
				t(this._selector).filter(f.ACTIVE).removeClass(u);
			}, o._jQueryInterface = function (e) {
				return this.each(function () {
					var i = t(this).data(n);if (i || (i = new o(this, "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e), t(this).data(n, i)), "string" == typeof e) {
						if ("undefined" == typeof i[e]) throw new TypeError('No method named "' + e + '"');i[e]();
					}
				});
			}, s(o, null, [{ key: "VERSION", get: function get() {
					return "4.0.0";
				} }, { key: "Default", get: function get() {
					return a;
				} }]), o;
		}();return t(window).on(h.LOAD_DATA_API, function () {
			for (var e = t.makeArray(t(f.DATA_SPY)), n = e.length; n--;) {
				var i = t(e[n]);g._jQueryInterface.call(i, i.data());
			}
		}), t.fn[e] = g._jQueryInterface, t.fn[e].Constructor = g, t.fn[e].noConflict = function () {
			return t.fn[e] = o, g._jQueryInterface;
		}, g;
	}(e),
	    V = function (t) {
		var e = "bs.tab",
		    n = "." + e,
		    i = t.fn.tab,
		    r = { HIDE: "hide" + n, HIDDEN: "hidden" + n, SHOW: "show" + n, SHOWN: "shown" + n, CLICK_DATA_API: "click.bs.tab.data-api" },
		    o = "dropdown-menu",
		    a = "active",
		    l = "disabled",
		    h = "fade",
		    c = "show",
		    u = ".dropdown",
		    f = ".nav, .list-group",
		    d = ".active",
		    _ = "> li > .active",
		    g = '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
		    p = ".dropdown-toggle",
		    m = "> .dropdown-menu .active",
		    v = function () {
			function n(t) {
				this._element = t;
			}var i = n.prototype;return i.show = function () {
				var e = this;if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && t(this._element).hasClass(a) || t(this._element).hasClass(l))) {
					var n,
					    i,
					    s = t(this._element).closest(f)[0],
					    o = P.getSelectorFromElement(this._element);if (s) {
						var h = "UL" === s.nodeName ? _ : d;i = (i = t.makeArray(t(s).find(h)))[i.length - 1];
					}var c = t.Event(r.HIDE, { relatedTarget: this._element }),
					    u = t.Event(r.SHOW, { relatedTarget: i });if (i && t(i).trigger(c), t(this._element).trigger(u), !u.isDefaultPrevented() && !c.isDefaultPrevented()) {
						o && (n = t(o)[0]), this._activate(this._element, s);var g = function g() {
							var n = t.Event(r.HIDDEN, { relatedTarget: e._element }),
							    s = t.Event(r.SHOWN, { relatedTarget: i });t(i).trigger(n), t(e._element).trigger(s);
						};n ? this._activate(n, n.parentNode, g) : g();
					}
				}
			}, i.dispose = function () {
				t.removeData(this._element, e), this._element = null;
			}, i._activate = function (e, n, i) {
				var s = this,
				    r = ("UL" === n.nodeName ? t(n).find(_) : t(n).children(d))[0],
				    o = i && P.supportsTransitionEnd() && r && t(r).hasClass(h),
				    a = function a() {
					return s._transitionComplete(e, r, i);
				};r && o ? t(r).one(P.TRANSITION_END, a).emulateTransitionEnd(150) : a();
			}, i._transitionComplete = function (e, n, i) {
				if (n) {
					t(n).removeClass(c + " " + a);var s = t(n.parentNode).find(m)[0];s && t(s).removeClass(a), "tab" === n.getAttribute("role") && n.setAttribute("aria-selected", !1);
				}if (t(e).addClass(a), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !0), P.reflow(e), t(e).addClass(c), e.parentNode && t(e.parentNode).hasClass(o)) {
					var r = t(e).closest(u)[0];r && t(r).find(p).addClass(a), e.setAttribute("aria-expanded", !0);
				}i && i();
			}, n._jQueryInterface = function (i) {
				return this.each(function () {
					var s = t(this),
					    r = s.data(e);if (r || (r = new n(this), s.data(e, r)), "string" == typeof i) {
						if ("undefined" == typeof r[i]) throw new TypeError('No method named "' + i + '"');r[i]();
					}
				});
			}, s(n, null, [{ key: "VERSION", get: function get() {
					return "4.0.0";
				} }]), n;
		}();return t(document).on(r.CLICK_DATA_API, g, function (e) {
			e.preventDefault(), v._jQueryInterface.call(t(this), "show");
		}), t.fn.tab = v._jQueryInterface, t.fn.tab.Constructor = v, t.fn.tab.noConflict = function () {
			return t.fn.tab = i, v._jQueryInterface;
		}, v;
	}(e);!function (t) {
		if ("undefined" == typeof t) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");var e = t.fn.jquery.split(" ")[0].split(".");if (e[0] < 2 && e[1] < 9 || 1 === e[0] && 9 === e[1] && e[2] < 1 || e[0] >= 4) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0");
	}(e), t.Util = P, t.Alert = L, t.Button = R, t.Carousel = j, t.Collapse = H, t.Dropdown = W, t.Modal = M, t.Popover = x, t.Scrollspy = K, t.Tab = V, t.Tooltip = U, Object.defineProperty(t, "__esModule", { value: !0 });
});
//# sourceMappingURL=bootstrap.min.js.map
/* axios v0.19.0 | (c) 2019 by Matt Zabriskie */
!function (e, t) {
	"object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) ? module.exports = t() : "function" == typeof define && define.amd ? define([], t) : "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? exports.axios = t() : e.axios = t();
}(this, function () {
	return function (e) {
		function t(r) {
			if (n[r]) return n[r].exports;var o = n[r] = { exports: {}, id: r, loaded: !1 };return e[r].call(o.exports, o, o.exports, t), o.loaded = !0, o.exports;
		}var n = {};return t.m = e, t.c = n, t.p = "", t(0);
	}([function (e, t, n) {
		e.exports = n(1);
	}, function (e, t, n) {
		"use strict";
		function r(e) {
			var t = new i(e),
			    n = s(i.prototype.request, t);return o.extend(n, i.prototype, t), o.extend(n, t), n;
		}var o = n(2),
		    s = n(3),
		    i = n(5),
		    a = n(22),
		    u = n(11),
		    c = r(u);c.Axios = i, c.create = function (e) {
			return r(a(c.defaults, e));
		}, c.Cancel = n(23), c.CancelToken = n(24), c.isCancel = n(10), c.all = function (e) {
			return Promise.all(e);
		}, c.spread = n(25), e.exports = c, e.exports.default = c;
	}, function (e, t, n) {
		"use strict";
		function r(e) {
			return "[object Array]" === j.call(e);
		}function o(e) {
			return "[object ArrayBuffer]" === j.call(e);
		}function s(e) {
			return "undefined" != typeof FormData && e instanceof FormData;
		}function i(e) {
			var t;return t = "undefined" != typeof ArrayBuffer && ArrayBuffer.isView ? ArrayBuffer.isView(e) : e && e.buffer && e.buffer instanceof ArrayBuffer;
		}function a(e) {
			return "string" == typeof e;
		}function u(e) {
			return "number" == typeof e;
		}function c(e) {
			return "undefined" == typeof e;
		}function f(e) {
			return null !== e && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e));
		}function p(e) {
			return "[object Date]" === j.call(e);
		}function d(e) {
			return "[object File]" === j.call(e);
		}function l(e) {
			return "[object Blob]" === j.call(e);
		}function h(e) {
			return "[object Function]" === j.call(e);
		}function m(e) {
			return f(e) && h(e.pipe);
		}function y(e) {
			return "undefined" != typeof URLSearchParams && e instanceof URLSearchParams;
		}function g(e) {
			return e.replace(/^\s*/, "").replace(/\s*$/, "");
		}function x() {
			return ("undefined" == typeof navigator || "ReactNative" !== navigator.product && "NativeScript" !== navigator.product && "NS" !== navigator.product) && "undefined" != typeof window && "undefined" != typeof document;
		}function v(e, t) {
			if (null !== e && "undefined" != typeof e) if ("object" != (typeof e === "undefined" ? "undefined" : _typeof(e)) && (e = [e]), r(e)) for (var n = 0, o = e.length; n < o; n++) {
				t.call(null, e[n], n, e);
			} else for (var s in e) {
				Object.prototype.hasOwnProperty.call(e, s) && t.call(null, e[s], s, e);
			}
		}function w() {
			function e(e, n) {
				"object" == _typeof(t[n]) && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) ? t[n] = w(t[n], e) : t[n] = e;
			}for (var t = {}, n = 0, r = arguments.length; n < r; n++) {
				v(arguments[n], e);
			}return t;
		}function b() {
			function e(e, n) {
				"object" == _typeof(t[n]) && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) ? t[n] = b(t[n], e) : "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) ? t[n] = b({}, e) : t[n] = e;
			}for (var t = {}, n = 0, r = arguments.length; n < r; n++) {
				v(arguments[n], e);
			}return t;
		}function E(e, t, n) {
			return v(t, function (t, r) {
				n && "function" == typeof t ? e[r] = S(t, n) : e[r] = t;
			}), e;
		}var S = n(3),
		    R = n(4),
		    j = Object.prototype.toString;e.exports = { isArray: r, isArrayBuffer: o, isBuffer: R, isFormData: s, isArrayBufferView: i, isString: a, isNumber: u, isObject: f, isUndefined: c, isDate: p, isFile: d, isBlob: l, isFunction: h, isStream: m, isURLSearchParams: y, isStandardBrowserEnv: x, forEach: v, merge: w, deepMerge: b, extend: E, trim: g };
	}, function (e, t) {
		"use strict";
		e.exports = function (e, t) {
			return function () {
				for (var n = new Array(arguments.length), r = 0; r < n.length; r++) {
					n[r] = arguments[r];
				}return e.apply(t, n);
			};
		};
	}, function (e, t) {
		/*!
  * Determine if an object is a Buffer
  *
  * @author   Feross Aboukhadijeh <https://feross.org>
  * @license  MIT
  */
		e.exports = function (e) {
			return null != e && null != e.constructor && "function" == typeof e.constructor.isBuffer && e.constructor.isBuffer(e);
		};
	}, function (e, t, n) {
		"use strict";
		function r(e) {
			this.defaults = e, this.interceptors = { request: new i(), response: new i() };
		}var o = n(2),
		    s = n(6),
		    i = n(7),
		    a = n(8),
		    u = n(22);r.prototype.request = function (e) {
			"string" == typeof e ? (e = arguments[1] || {}, e.url = arguments[0]) : e = e || {}, e = u(this.defaults, e), e.method = e.method ? e.method.toLowerCase() : "get";var t = [a, void 0],
			    n = Promise.resolve(e);for (this.interceptors.request.forEach(function (e) {
				t.unshift(e.fulfilled, e.rejected);
			}), this.interceptors.response.forEach(function (e) {
				t.push(e.fulfilled, e.rejected);
			}); t.length;) {
				n = n.then(t.shift(), t.shift());
			}return n;
		}, r.prototype.getUri = function (e) {
			return e = u(this.defaults, e), s(e.url, e.params, e.paramsSerializer).replace(/^\?/, "");
		}, o.forEach(["delete", "get", "head", "options"], function (e) {
			r.prototype[e] = function (t, n) {
				return this.request(o.merge(n || {}, { method: e, url: t }));
			};
		}), o.forEach(["post", "put", "patch"], function (e) {
			r.prototype[e] = function (t, n, r) {
				return this.request(o.merge(r || {}, { method: e, url: t, data: n }));
			};
		}), e.exports = r;
	}, function (e, t, n) {
		"use strict";
		function r(e) {
			return encodeURIComponent(e).replace(/%40/gi, "@").replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "+").replace(/%5B/gi, "[").replace(/%5D/gi, "]");
		}var o = n(2);e.exports = function (e, t, n) {
			if (!t) return e;var s;if (n) s = n(t);else if (o.isURLSearchParams(t)) s = t.toString();else {
				var i = [];o.forEach(t, function (e, t) {
					null !== e && "undefined" != typeof e && (o.isArray(e) ? t += "[]" : e = [e], o.forEach(e, function (e) {
						o.isDate(e) ? e = e.toISOString() : o.isObject(e) && (e = JSON.stringify(e)), i.push(r(t) + "=" + r(e));
					}));
				}), s = i.join("&");
			}if (s) {
				var a = e.indexOf("#");a !== -1 && (e = e.slice(0, a)), e += (e.indexOf("?") === -1 ? "?" : "&") + s;
			}return e;
		};
	}, function (e, t, n) {
		"use strict";
		function r() {
			this.handlers = [];
		}var o = n(2);r.prototype.use = function (e, t) {
			return this.handlers.push({ fulfilled: e, rejected: t }), this.handlers.length - 1;
		}, r.prototype.eject = function (e) {
			this.handlers[e] && (this.handlers[e] = null);
		}, r.prototype.forEach = function (e) {
			o.forEach(this.handlers, function (t) {
				null !== t && e(t);
			});
		}, e.exports = r;
	}, function (e, t, n) {
		"use strict";
		function r(e) {
			e.cancelToken && e.cancelToken.throwIfRequested();
		}var o = n(2),
		    s = n(9),
		    i = n(10),
		    a = n(11),
		    u = n(20),
		    c = n(21);e.exports = function (e) {
			r(e), e.baseURL && !u(e.url) && (e.url = c(e.baseURL, e.url)), e.headers = e.headers || {}, e.data = s(e.data, e.headers, e.transformRequest), e.headers = o.merge(e.headers.common || {}, e.headers[e.method] || {}, e.headers || {}), o.forEach(["delete", "get", "head", "post", "put", "patch", "common"], function (t) {
				delete e.headers[t];
			});var t = e.adapter || a.adapter;return t(e).then(function (t) {
				return r(e), t.data = s(t.data, t.headers, e.transformResponse), t;
			}, function (t) {
				return i(t) || (r(e), t && t.response && (t.response.data = s(t.response.data, t.response.headers, e.transformResponse))), Promise.reject(t);
			});
		};
	}, function (e, t, n) {
		"use strict";
		var r = n(2);e.exports = function (e, t, n) {
			return r.forEach(n, function (n) {
				e = n(e, t);
			}), e;
		};
	}, function (e, t) {
		"use strict";
		e.exports = function (e) {
			return !(!e || !e.__CANCEL__);
		};
	}, function (e, t, n) {
		"use strict";
		function r(e, t) {
			!s.isUndefined(e) && s.isUndefined(e["Content-Type"]) && (e["Content-Type"] = t);
		}function o() {
			var e;return "undefined" != typeof process && "[object process]" === Object.prototype.toString.call(process) ? e = n(13) : "undefined" != typeof XMLHttpRequest && (e = n(13)), e;
		}var s = n(2),
		    i = n(12),
		    a = { "Content-Type": "application/x-www-form-urlencoded" },
		    u = { adapter: o(), transformRequest: [function (e, t) {
				return i(t, "Accept"), i(t, "Content-Type"), s.isFormData(e) || s.isArrayBuffer(e) || s.isBuffer(e) || s.isStream(e) || s.isFile(e) || s.isBlob(e) ? e : s.isArrayBufferView(e) ? e.buffer : s.isURLSearchParams(e) ? (r(t, "application/x-www-form-urlencoded;charset=utf-8"), e.toString()) : s.isObject(e) ? (r(t, "application/json;charset=utf-8"), JSON.stringify(e)) : e;
			}], transformResponse: [function (e) {
				if ("string" == typeof e) try {
					e = JSON.parse(e);
				} catch (e) {}return e;
			}], timeout: 0, xsrfCookieName: "XSRF-TOKEN", xsrfHeaderName: "X-XSRF-TOKEN", maxContentLength: -1, validateStatus: function validateStatus(e) {
				return e >= 200 && e < 300;
			} };u.headers = { common: { Accept: "application/json, text/plain, */*" } }, s.forEach(["delete", "get", "head"], function (e) {
			u.headers[e] = {};
		}), s.forEach(["post", "put", "patch"], function (e) {
			u.headers[e] = s.merge(a);
		}), e.exports = u;
	}, function (e, t, n) {
		"use strict";
		var r = n(2);e.exports = function (e, t) {
			r.forEach(e, function (n, r) {
				r !== t && r.toUpperCase() === t.toUpperCase() && (e[t] = n, delete e[r]);
			});
		};
	}, function (e, t, n) {
		"use strict";
		var r = n(2),
		    o = n(14),
		    s = n(6),
		    i = n(17),
		    a = n(18),
		    u = n(15);e.exports = function (e) {
			return new Promise(function (t, c) {
				var f = e.data,
				    p = e.headers;r.isFormData(f) && delete p["Content-Type"];var d = new XMLHttpRequest();if (e.auth) {
					var l = e.auth.username || "",
					    h = e.auth.password || "";p.Authorization = "Basic " + btoa(l + ":" + h);
				}if (d.open(e.method.toUpperCase(), s(e.url, e.params, e.paramsSerializer), !0), d.timeout = e.timeout, d.onreadystatechange = function () {
					if (d && 4 === d.readyState && (0 !== d.status || d.responseURL && 0 === d.responseURL.indexOf("file:"))) {
						var n = "getAllResponseHeaders" in d ? i(d.getAllResponseHeaders()) : null,
						    r = e.responseType && "text" !== e.responseType ? d.response : d.responseText,
						    s = { data: r, status: d.status, statusText: d.statusText, headers: n, config: e, request: d };o(t, c, s), d = null;
					}
				}, d.onabort = function () {
					d && (c(u("Request aborted", e, "ECONNABORTED", d)), d = null);
				}, d.onerror = function () {
					c(u("Network Error", e, null, d)), d = null;
				}, d.ontimeout = function () {
					c(u("timeout of " + e.timeout + "ms exceeded", e, "ECONNABORTED", d)), d = null;
				}, r.isStandardBrowserEnv()) {
					var m = n(19),
					    y = (e.withCredentials || a(e.url)) && e.xsrfCookieName ? m.read(e.xsrfCookieName) : void 0;y && (p[e.xsrfHeaderName] = y);
				}if ("setRequestHeader" in d && r.forEach(p, function (e, t) {
					"undefined" == typeof f && "content-type" === t.toLowerCase() ? delete p[t] : d.setRequestHeader(t, e);
				}), e.withCredentials && (d.withCredentials = !0), e.responseType) try {
					d.responseType = e.responseType;
				} catch (t) {
					if ("json" !== e.responseType) throw t;
				}"function" == typeof e.onDownloadProgress && d.addEventListener("progress", e.onDownloadProgress), "function" == typeof e.onUploadProgress && d.upload && d.upload.addEventListener("progress", e.onUploadProgress), e.cancelToken && e.cancelToken.promise.then(function (e) {
					d && (d.abort(), c(e), d = null);
				}), void 0 === f && (f = null), d.send(f);
			});
		};
	}, function (e, t, n) {
		"use strict";
		var r = n(15);e.exports = function (e, t, n) {
			var o = n.config.validateStatus;!o || o(n.status) ? e(n) : t(r("Request failed with status code " + n.status, n.config, null, n.request, n));
		};
	}, function (e, t, n) {
		"use strict";
		var r = n(16);e.exports = function (e, t, n, o, s) {
			var i = new Error(e);return r(i, t, n, o, s);
		};
	}, function (e, t) {
		"use strict";
		e.exports = function (e, t, n, r, o) {
			return e.config = t, n && (e.code = n), e.request = r, e.response = o, e.isAxiosError = !0, e.toJSON = function () {
				return { message: this.message, name: this.name, description: this.description, number: this.number, fileName: this.fileName, lineNumber: this.lineNumber, columnNumber: this.columnNumber, stack: this.stack, config: this.config, code: this.code };
			}, e;
		};
	}, function (e, t, n) {
		"use strict";
		var r = n(2),
		    o = ["age", "authorization", "content-length", "content-type", "etag", "expires", "from", "host", "if-modified-since", "if-unmodified-since", "last-modified", "location", "max-forwards", "proxy-authorization", "referer", "retry-after", "user-agent"];e.exports = function (e) {
			var t,
			    n,
			    s,
			    i = {};return e ? (r.forEach(e.split("\n"), function (e) {
				if (s = e.indexOf(":"), t = r.trim(e.substr(0, s)).toLowerCase(), n = r.trim(e.substr(s + 1)), t) {
					if (i[t] && o.indexOf(t) >= 0) return;"set-cookie" === t ? i[t] = (i[t] ? i[t] : []).concat([n]) : i[t] = i[t] ? i[t] + ", " + n : n;
				}
			}), i) : i;
		};
	}, function (e, t, n) {
		"use strict";
		var r = n(2);e.exports = r.isStandardBrowserEnv() ? function () {
			function e(e) {
				var t = e;return n && (o.setAttribute("href", t), t = o.href), o.setAttribute("href", t), { href: o.href, protocol: o.protocol ? o.protocol.replace(/:$/, "") : "", host: o.host, search: o.search ? o.search.replace(/^\?/, "") : "", hash: o.hash ? o.hash.replace(/^#/, "") : "", hostname: o.hostname, port: o.port, pathname: "/" === o.pathname.charAt(0) ? o.pathname : "/" + o.pathname };
			}var t,
			    n = /(msie|trident)/i.test(navigator.userAgent),
			    o = document.createElement("a");return t = e(window.location.href), function (n) {
				var o = r.isString(n) ? e(n) : n;return o.protocol === t.protocol && o.host === t.host;
			};
		}() : function () {
			return function () {
				return !0;
			};
		}();
	}, function (e, t, n) {
		"use strict";
		var r = n(2);e.exports = r.isStandardBrowserEnv() ? function () {
			return { write: function write(e, t, n, o, s, i) {
					var a = [];a.push(e + "=" + encodeURIComponent(t)), r.isNumber(n) && a.push("expires=" + new Date(n).toGMTString()), r.isString(o) && a.push("path=" + o), r.isString(s) && a.push("domain=" + s), i === !0 && a.push("secure"), document.cookie = a.join("; ");
				}, read: function read(e) {
					var t = document.cookie.match(new RegExp("(^|;\\s*)(" + e + ")=([^;]*)"));return t ? decodeURIComponent(t[3]) : null;
				}, remove: function remove(e) {
					this.write(e, "", Date.now() - 864e5);
				} };
		}() : function () {
			return { write: function write() {}, read: function read() {
					return null;
				}, remove: function remove() {} };
		}();
	}, function (e, t) {
		"use strict";
		e.exports = function (e) {
			return (/^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(e)
			);
		};
	}, function (e, t) {
		"use strict";
		e.exports = function (e, t) {
			return t ? e.replace(/\/+$/, "") + "/" + t.replace(/^\/+/, "") : e;
		};
	}, function (e, t, n) {
		"use strict";
		var r = n(2);e.exports = function (e, t) {
			t = t || {};var n = {};return r.forEach(["url", "method", "params", "data"], function (e) {
				"undefined" != typeof t[e] && (n[e] = t[e]);
			}), r.forEach(["headers", "auth", "proxy"], function (o) {
				r.isObject(t[o]) ? n[o] = r.deepMerge(e[o], t[o]) : "undefined" != typeof t[o] ? n[o] = t[o] : r.isObject(e[o]) ? n[o] = r.deepMerge(e[o]) : "undefined" != typeof e[o] && (n[o] = e[o]);
			}), r.forEach(["baseURL", "transformRequest", "transformResponse", "paramsSerializer", "timeout", "withCredentials", "adapter", "responseType", "xsrfCookieName", "xsrfHeaderName", "onUploadProgress", "onDownloadProgress", "maxContentLength", "validateStatus", "maxRedirects", "httpAgent", "httpsAgent", "cancelToken", "socketPath"], function (r) {
				"undefined" != typeof t[r] ? n[r] = t[r] : "undefined" != typeof e[r] && (n[r] = e[r]);
			}), n;
		};
	}, function (e, t) {
		"use strict";
		function n(e) {
			this.message = e;
		}n.prototype.toString = function () {
			return "Cancel" + (this.message ? ": " + this.message : "");
		}, n.prototype.__CANCEL__ = !0, e.exports = n;
	}, function (e, t, n) {
		"use strict";
		function r(e) {
			if ("function" != typeof e) throw new TypeError("executor must be a function.");var t;this.promise = new Promise(function (e) {
				t = e;
			});var n = this;e(function (e) {
				n.reason || (n.reason = new o(e), t(n.reason));
			});
		}var o = n(23);r.prototype.throwIfRequested = function () {
			if (this.reason) throw this.reason;
		}, r.source = function () {
			var e,
			    t = new r(function (t) {
				e = t;
			});return { token: t, cancel: e };
		}, e.exports = r;
	}, function (e, t) {
		"use strict";
		e.exports = function (e) {
			return function (t) {
				return e.apply(null, t);
			};
		};
	}]);
});
//# sourceMappingURL=axios.min.map
/*! Magnific Popup - v1.1.0 - 2016-02-20
* http://dimsemenov.com/plugins/magnific-popup/
* Copyright (c) 2016 Dmitry Semenov; */
;(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module. 
		define(['jquery'], factory);
	} else if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object') {
		// Node/CommonJS 
		factory(require('jquery'));
	} else {
		// Browser globals 
		factory(window.jQuery || window.Zepto);
	}
})(function ($) {

	/*>>core*/
	/**
  * 
  * Magnific Popup Core JS file
  * 
  */

	/**
  * Private static constants
  */
	var CLOSE_EVENT = 'Close',
	    BEFORE_CLOSE_EVENT = 'BeforeClose',
	    AFTER_CLOSE_EVENT = 'AfterClose',
	    BEFORE_APPEND_EVENT = 'BeforeAppend',
	    MARKUP_PARSE_EVENT = 'MarkupParse',
	    OPEN_EVENT = 'Open',
	    CHANGE_EVENT = 'Change',
	    NS = 'mfp',
	    EVENT_NS = '.' + NS,
	    READY_CLASS = 'mfp-ready',
	    REMOVING_CLASS = 'mfp-removing',
	    PREVENT_CLOSE_CLASS = 'mfp-prevent-close';

	/**
  * Private vars 
  */
	/*jshint -W079 */
	var mfp,
	    // As we have only one instance of MagnificPopup object, we define it locally to not to use 'this'
	MagnificPopup = function MagnificPopup() {},
	    _isJQ = !!window.jQuery,
	    _prevStatus,
	    _window = $(window),
	    _document,
	    _prevContentType,
	    _wrapClasses,
	    _currPopupType;

	/**
  * Private functions
  */
	var _mfpOn = function _mfpOn(name, f) {
		mfp.ev.on(NS + name + EVENT_NS, f);
	},
	    _getEl = function _getEl(className, appendTo, html, raw) {
		var el = document.createElement('div');
		el.className = 'mfp-' + className;
		if (html) {
			el.innerHTML = html;
		}
		if (!raw) {
			el = $(el);
			if (appendTo) {
				el.appendTo(appendTo);
			}
		} else if (appendTo) {
			appendTo.appendChild(el);
		}
		return el;
	},
	    _mfpTrigger = function _mfpTrigger(e, data) {
		mfp.ev.triggerHandler(NS + e, data);

		if (mfp.st.callbacks) {
			// converts "mfpEventName" to "eventName" callback and triggers it if it's present
			e = e.charAt(0).toLowerCase() + e.slice(1);
			if (mfp.st.callbacks[e]) {
				mfp.st.callbacks[e].apply(mfp, $.isArray(data) ? data : [data]);
			}
		}
	},
	    _getCloseBtn = function _getCloseBtn(type) {
		if (type !== _currPopupType || !mfp.currTemplate.closeBtn) {
			mfp.currTemplate.closeBtn = $(mfp.st.closeMarkup.replace('%title%', mfp.st.tClose));
			_currPopupType = type;
		}
		return mfp.currTemplate.closeBtn;
	},

	// Initialize Magnific Popup only when called at least once
	_checkInstance = function _checkInstance() {
		if (!$.magnificPopup.instance) {
			/*jshint -W020 */
			mfp = new MagnificPopup();
			mfp.init();
			$.magnificPopup.instance = mfp;
		}
	},

	// CSS transition detection, http://stackoverflow.com/questions/7264899/detect-css-transitions-using-javascript-and-without-modernizr
	supportsTransitions = function supportsTransitions() {
		var s = document.createElement('p').style,
		    // 's' for style. better to create an element if body yet to exist
		v = ['ms', 'O', 'Moz', 'Webkit']; // 'v' for vendor

		if (s['transition'] !== undefined) {
			return true;
		}

		while (v.length) {
			if (v.pop() + 'Transition' in s) {
				return true;
			}
		}

		return false;
	};

	/**
  * Public functions
  */
	MagnificPopup.prototype = {

		constructor: MagnificPopup,

		/**
   * Initializes Magnific Popup plugin. 
   * This function is triggered only once when $.fn.magnificPopup or $.magnificPopup is executed
   */
		init: function init() {
			var appVersion = navigator.appVersion;
			mfp.isLowIE = mfp.isIE8 = document.all && !document.addEventListener;
			mfp.isAndroid = /android/gi.test(appVersion);
			mfp.isIOS = /iphone|ipad|ipod/gi.test(appVersion);
			mfp.supportsTransition = supportsTransitions();

			// We disable fixed positioned lightbox on devices that don't handle it nicely.
			// If you know a better way of detecting this - let me know.
			mfp.probablyMobile = mfp.isAndroid || mfp.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent);
			_document = $(document);

			mfp.popupsCache = {};
		},

		/**
   * Opens popup
   * @param  data [description]
   */
		open: function open(data) {

			var i;

			if (data.isObj === false) {
				// convert jQuery collection to array to avoid conflicts later
				mfp.items = data.items.toArray();

				mfp.index = 0;
				var items = data.items,
				    item;
				for (i = 0; i < items.length; i++) {
					item = items[i];
					if (item.parsed) {
						item = item.el[0];
					}
					if (item === data.el[0]) {
						mfp.index = i;
						break;
					}
				}
			} else {
				mfp.items = $.isArray(data.items) ? data.items : [data.items];
				mfp.index = data.index || 0;
			}

			// if popup is already opened - we just update the content
			if (mfp.isOpen) {
				mfp.updateItemHTML();
				return;
			}

			mfp.types = [];
			_wrapClasses = '';
			if (data.mainEl && data.mainEl.length) {
				mfp.ev = data.mainEl.eq(0);
			} else {
				mfp.ev = _document;
			}

			if (data.key) {
				if (!mfp.popupsCache[data.key]) {
					mfp.popupsCache[data.key] = {};
				}
				mfp.currTemplate = mfp.popupsCache[data.key];
			} else {
				mfp.currTemplate = {};
			}

			mfp.st = $.extend(true, {}, $.magnificPopup.defaults, data);
			mfp.fixedContentPos = mfp.st.fixedContentPos === 'auto' ? !mfp.probablyMobile : mfp.st.fixedContentPos;

			if (mfp.st.modal) {
				mfp.st.closeOnContentClick = false;
				mfp.st.closeOnBgClick = false;
				mfp.st.showCloseBtn = false;
				mfp.st.enableEscapeKey = false;
			}

			// Building markup
			// main containers are created only once
			if (!mfp.bgOverlay) {

				// Dark overlay
				mfp.bgOverlay = _getEl('bg').on('click' + EVENT_NS, function () {
					mfp.close();
				});

				mfp.wrap = _getEl('wrap').attr('tabindex', -1).on('click' + EVENT_NS, function (e) {
					if (mfp._checkIfClose(e.target)) {
						mfp.close();
					}
				});

				mfp.container = _getEl('container', mfp.wrap);
			}

			mfp.contentContainer = _getEl('content');
			if (mfp.st.preloader) {
				mfp.preloader = _getEl('preloader', mfp.container, mfp.st.tLoading);
			}

			// Initializing modules
			var modules = $.magnificPopup.modules;
			for (i = 0; i < modules.length; i++) {
				var n = modules[i];
				n = n.charAt(0).toUpperCase() + n.slice(1);
				mfp['init' + n].call(mfp);
			}
			_mfpTrigger('BeforeOpen');

			if (mfp.st.showCloseBtn) {
				// Close button
				if (!mfp.st.closeBtnInside) {
					mfp.wrap.append(_getCloseBtn());
				} else {
					_mfpOn(MARKUP_PARSE_EVENT, function (e, template, values, item) {
						values.close_replaceWith = _getCloseBtn(item.type);
					});
					_wrapClasses += ' mfp-close-btn-in';
				}
			}

			if (mfp.st.alignTop) {
				_wrapClasses += ' mfp-align-top';
			}

			if (mfp.fixedContentPos) {
				mfp.wrap.css({
					overflow: mfp.st.overflowY,
					overflowX: 'hidden',
					overflowY: mfp.st.overflowY
				});
			} else {
				mfp.wrap.css({
					top: _window.scrollTop(),
					position: 'absolute'
				});
			}
			if (mfp.st.fixedBgPos === false || mfp.st.fixedBgPos === 'auto' && !mfp.fixedContentPos) {
				mfp.bgOverlay.css({
					height: _document.height(),
					position: 'absolute'
				});
			}

			if (mfp.st.enableEscapeKey) {
				// Close on ESC key
				_document.on('keyup' + EVENT_NS, function (e) {
					if (e.keyCode === 27) {
						mfp.close();
					}
				});
			}

			_window.on('resize' + EVENT_NS, function () {
				mfp.updateSize();
			});

			if (!mfp.st.closeOnContentClick) {
				_wrapClasses += ' mfp-auto-cursor';
			}

			if (_wrapClasses) mfp.wrap.addClass(_wrapClasses);

			// this triggers recalculation of layout, so we get it once to not to trigger twice
			var windowHeight = mfp.wH = _window.height();

			var windowStyles = {};

			if (mfp.fixedContentPos) {
				if (mfp._hasScrollBar(windowHeight)) {
					var s = mfp._getScrollbarSize();
					if (s) {
						windowStyles.marginRight = s;
					}
				}
			}

			if (mfp.fixedContentPos) {
				if (!mfp.isIE7) {
					windowStyles.overflow = 'hidden';
				} else {
					// ie7 double-scroll bug
					$('body, html').css('overflow', 'hidden');
				}
			}

			var classesToadd = mfp.st.mainClass;
			if (mfp.isIE7) {
				classesToadd += ' mfp-ie7';
			}
			if (classesToadd) {
				mfp._addClassToMFP(classesToadd);
			}

			// add content
			mfp.updateItemHTML();

			_mfpTrigger('BuildControls');

			// remove scrollbar, add margin e.t.c
			$('html').css(windowStyles);

			// add everything to DOM
			mfp.bgOverlay.add(mfp.wrap).prependTo(mfp.st.prependTo || $(document.body));

			// Save last focused element
			mfp._lastFocusedEl = document.activeElement;

			// Wait for next cycle to allow CSS transition
			setTimeout(function () {

				if (mfp.content) {
					mfp._addClassToMFP(READY_CLASS);
					mfp._setFocus();
				} else {
					// if content is not defined (not loaded e.t.c) we add class only for BG
					mfp.bgOverlay.addClass(READY_CLASS);
				}

				// Trap the focus in popup
				_document.on('focusin' + EVENT_NS, mfp._onFocusIn);
			}, 16);

			mfp.isOpen = true;
			mfp.updateSize(windowHeight);
			_mfpTrigger(OPEN_EVENT);

			return data;
		},

		/**
   * Closes the popup
   */
		close: function close() {
			if (!mfp.isOpen) return;
			_mfpTrigger(BEFORE_CLOSE_EVENT);

			mfp.isOpen = false;
			// for CSS3 animation
			if (mfp.st.removalDelay && !mfp.isLowIE && mfp.supportsTransition) {
				mfp._addClassToMFP(REMOVING_CLASS);
				setTimeout(function () {
					mfp._close();
				}, mfp.st.removalDelay);
			} else {
				mfp._close();
			}
		},

		/**
   * Helper for close() function
   */
		_close: function _close() {
			_mfpTrigger(CLOSE_EVENT);

			var classesToRemove = REMOVING_CLASS + ' ' + READY_CLASS + ' ';

			mfp.bgOverlay.detach();
			mfp.wrap.detach();
			mfp.container.empty();

			if (mfp.st.mainClass) {
				classesToRemove += mfp.st.mainClass + ' ';
			}

			mfp._removeClassFromMFP(classesToRemove);

			if (mfp.fixedContentPos) {
				var windowStyles = { marginRight: '' };
				if (mfp.isIE7) {
					$('body, html').css('overflow', '');
				} else {
					windowStyles.overflow = '';
				}
				$('html').css(windowStyles);
			}

			_document.off('keyup' + EVENT_NS + ' focusin' + EVENT_NS);
			mfp.ev.off(EVENT_NS);

			// clean up DOM elements that aren't removed
			mfp.wrap.attr('class', 'mfp-wrap').removeAttr('style');
			mfp.bgOverlay.attr('class', 'mfp-bg');
			mfp.container.attr('class', 'mfp-container');

			// remove close button from target element
			if (mfp.st.showCloseBtn && (!mfp.st.closeBtnInside || mfp.currTemplate[mfp.currItem.type] === true)) {
				if (mfp.currTemplate.closeBtn) mfp.currTemplate.closeBtn.detach();
			}

			if (mfp.st.autoFocusLast && mfp._lastFocusedEl) {
				$(mfp._lastFocusedEl).focus(); // put tab focus back
			}
			mfp.currItem = null;
			mfp.content = null;
			mfp.currTemplate = null;
			mfp.prevHeight = 0;

			_mfpTrigger(AFTER_CLOSE_EVENT);
		},

		updateSize: function updateSize(winHeight) {

			if (mfp.isIOS) {
				// fixes iOS nav bars https://github.com/dimsemenov/Magnific-Popup/issues/2
				var zoomLevel = document.documentElement.clientWidth / window.innerWidth;
				var height = window.innerHeight * zoomLevel;
				mfp.wrap.css('height', height);
				mfp.wH = height;
			} else {
				mfp.wH = winHeight || _window.height();
			}
			// Fixes #84: popup incorrectly positioned with position:relative on body
			if (!mfp.fixedContentPos) {
				mfp.wrap.css('height', mfp.wH);
			}

			_mfpTrigger('Resize');
		},

		/**
   * Set content of popup based on current index
   */
		updateItemHTML: function updateItemHTML() {
			var item = mfp.items[mfp.index];

			// Detach and perform modifications
			mfp.contentContainer.detach();

			if (mfp.content) mfp.content.detach();

			if (!item.parsed) {
				item = mfp.parseEl(mfp.index);
			}

			var type = item.type;

			_mfpTrigger('BeforeChange', [mfp.currItem ? mfp.currItem.type : '', type]);
			// BeforeChange event works like so:
			// _mfpOn('BeforeChange', function(e, prevType, newType) { });

			mfp.currItem = item;

			if (!mfp.currTemplate[type]) {
				var markup = mfp.st[type] ? mfp.st[type].markup : false;

				// allows to modify markup
				_mfpTrigger('FirstMarkupParse', markup);

				if (markup) {
					mfp.currTemplate[type] = $(markup);
				} else {
					// if there is no markup found we just define that template is parsed
					mfp.currTemplate[type] = true;
				}
			}

			if (_prevContentType && _prevContentType !== item.type) {
				mfp.container.removeClass('mfp-' + _prevContentType + '-holder');
			}

			var newContent = mfp['get' + type.charAt(0).toUpperCase() + type.slice(1)](item, mfp.currTemplate[type]);
			mfp.appendContent(newContent, type);

			item.preloaded = true;

			_mfpTrigger(CHANGE_EVENT, item);
			_prevContentType = item.type;

			// Append container back after its content changed
			mfp.container.prepend(mfp.contentContainer);

			_mfpTrigger('AfterChange');
		},

		/**
   * Set HTML content of popup
   */
		appendContent: function appendContent(newContent, type) {
			mfp.content = newContent;

			if (newContent) {
				if (mfp.st.showCloseBtn && mfp.st.closeBtnInside && mfp.currTemplate[type] === true) {
					// if there is no markup, we just append close button element inside
					if (!mfp.content.find('.mfp-close').length) {
						mfp.content.append(_getCloseBtn());
					}
				} else {
					mfp.content = newContent;
				}
			} else {
				mfp.content = '';
			}

			_mfpTrigger(BEFORE_APPEND_EVENT);
			mfp.container.addClass('mfp-' + type + '-holder');

			mfp.contentContainer.append(mfp.content);
		},

		/**
   * Creates Magnific Popup data object based on given data
   * @param  {int} index Index of item to parse
   */
		parseEl: function parseEl(index) {
			var item = mfp.items[index],
			    type;

			if (item.tagName) {
				item = { el: $(item) };
			} else {
				type = item.type;
				item = { data: item, src: item.src };
			}

			if (item.el) {
				var types = mfp.types;

				// check for 'mfp-TYPE' class
				for (var i = 0; i < types.length; i++) {
					if (item.el.hasClass('mfp-' + types[i])) {
						type = types[i];
						break;
					}
				}

				item.src = item.el.attr('data-mfp-src');
				if (!item.src) {
					item.src = item.el.attr('href');
				}
			}

			item.type = type || mfp.st.type || 'inline';
			item.index = index;
			item.parsed = true;
			mfp.items[index] = item;
			_mfpTrigger('ElementParse', item);

			return mfp.items[index];
		},

		/**
   * Initializes single popup or a group of popups
   */
		addGroup: function addGroup(el, options) {
			var eHandler = function eHandler(e) {
				e.mfpEl = this;
				mfp._openClick(e, el, options);
			};

			if (!options) {
				options = {};
			}

			var eName = 'click.magnificPopup';
			options.mainEl = el;

			if (options.items) {
				options.isObj = true;
				el.off(eName).on(eName, eHandler);
			} else {
				options.isObj = false;
				if (options.delegate) {
					el.off(eName).on(eName, options.delegate, eHandler);
				} else {
					options.items = el;
					el.off(eName).on(eName, eHandler);
				}
			}
		},
		_openClick: function _openClick(e, el, options) {
			var midClick = options.midClick !== undefined ? options.midClick : $.magnificPopup.defaults.midClick;

			if (!midClick && (e.which === 2 || e.ctrlKey || e.metaKey || e.altKey || e.shiftKey)) {
				return;
			}

			var disableOn = options.disableOn !== undefined ? options.disableOn : $.magnificPopup.defaults.disableOn;

			if (disableOn) {
				if ($.isFunction(disableOn)) {
					if (!disableOn.call(mfp)) {
						return true;
					}
				} else {
					// else it's number
					if (_window.width() < disableOn) {
						return true;
					}
				}
			}

			if (e.type) {
				e.preventDefault();

				// This will prevent popup from closing if element is inside and popup is already opened
				if (mfp.isOpen) {
					e.stopPropagation();
				}
			}

			options.el = $(e.mfpEl);
			if (options.delegate) {
				options.items = el.find(options.delegate);
			}
			mfp.open(options);
		},

		/**
   * Updates text on preloader
   */
		updateStatus: function updateStatus(status, text) {

			if (mfp.preloader) {
				if (_prevStatus !== status) {
					mfp.container.removeClass('mfp-s-' + _prevStatus);
				}

				if (!text && status === 'loading') {
					text = mfp.st.tLoading;
				}

				var data = {
					status: status,
					text: text
				};
				// allows to modify status
				_mfpTrigger('UpdateStatus', data);

				status = data.status;
				text = data.text;

				mfp.preloader.html(text);

				mfp.preloader.find('a').on('click', function (e) {
					e.stopImmediatePropagation();
				});

				mfp.container.addClass('mfp-s-' + status);
				_prevStatus = status;
			}
		},

		/*
  	"Private" helpers that aren't private at all
   */
		// Check to close popup or not
		// "target" is an element that was clicked
		_checkIfClose: function _checkIfClose(target) {

			if ($(target).hasClass(PREVENT_CLOSE_CLASS)) {
				return;
			}

			var closeOnContent = mfp.st.closeOnContentClick;
			var closeOnBg = mfp.st.closeOnBgClick;

			if (closeOnContent && closeOnBg) {
				return true;
			} else {

				// We close the popup if click is on close button or on preloader. Or if there is no content.
				if (!mfp.content || $(target).hasClass('mfp-close') || mfp.preloader && target === mfp.preloader[0]) {
					return true;
				}

				// if click is outside the content
				if (target !== mfp.content[0] && !$.contains(mfp.content[0], target)) {
					if (closeOnBg) {
						// last check, if the clicked element is in DOM, (in case it's removed onclick)
						if ($.contains(document, target)) {
							return true;
						}
					}
				} else if (closeOnContent) {
					return true;
				}
			}
			return false;
		},
		_addClassToMFP: function _addClassToMFP(cName) {
			mfp.bgOverlay.addClass(cName);
			mfp.wrap.addClass(cName);
		},
		_removeClassFromMFP: function _removeClassFromMFP(cName) {
			this.bgOverlay.removeClass(cName);
			mfp.wrap.removeClass(cName);
		},
		_hasScrollBar: function _hasScrollBar(winHeight) {
			return (mfp.isIE7 ? _document.height() : document.body.scrollHeight) > (winHeight || _window.height());
		},
		_setFocus: function _setFocus() {
			(mfp.st.focus ? mfp.content.find(mfp.st.focus).eq(0) : mfp.wrap).focus();
		},
		_onFocusIn: function _onFocusIn(e) {
			if (e.target !== mfp.wrap[0] && !$.contains(mfp.wrap[0], e.target)) {
				mfp._setFocus();
				return false;
			}
		},
		_parseMarkup: function _parseMarkup(template, values, item) {
			var arr;
			if (item.data) {
				values = $.extend(item.data, values);
			}
			_mfpTrigger(MARKUP_PARSE_EVENT, [template, values, item]);

			$.each(values, function (key, value) {
				if (value === undefined || value === false) {
					return true;
				}
				arr = key.split('_');
				if (arr.length > 1) {
					var el = template.find(EVENT_NS + '-' + arr[0]);

					if (el.length > 0) {
						var attr = arr[1];
						if (attr === 'replaceWith') {
							if (el[0] !== value[0]) {
								el.replaceWith(value);
							}
						} else if (attr === 'img') {
							if (el.is('img')) {
								el.attr('src', value);
							} else {
								el.replaceWith($('<img>').attr('src', value).attr('class', el.attr('class')));
							}
						} else {
							el.attr(arr[1], value);
						}
					}
				} else {
					template.find(EVENT_NS + '-' + key).html(value);
				}
			});
		},

		_getScrollbarSize: function _getScrollbarSize() {
			// thx David
			if (mfp.scrollbarSize === undefined) {
				var scrollDiv = document.createElement("div");
				scrollDiv.style.cssText = 'width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;';
				document.body.appendChild(scrollDiv);
				mfp.scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
				document.body.removeChild(scrollDiv);
			}
			return mfp.scrollbarSize;
		}

	}; /* MagnificPopup core prototype end */

	/**
  * Public static functions
  */
	$.magnificPopup = {
		instance: null,
		proto: MagnificPopup.prototype,
		modules: [],

		open: function open(options, index) {
			_checkInstance();

			if (!options) {
				options = {};
			} else {
				options = $.extend(true, {}, options);
			}

			options.isObj = true;
			options.index = index || 0;
			return this.instance.open(options);
		},

		close: function close() {
			return $.magnificPopup.instance && $.magnificPopup.instance.close();
		},

		registerModule: function registerModule(name, module) {
			if (module.options) {
				$.magnificPopup.defaults[name] = module.options;
			}
			$.extend(this.proto, module.proto);
			this.modules.push(name);
		},

		defaults: {

			// Info about options is in docs:
			// http://dimsemenov.com/plugins/magnific-popup/documentation.html#options

			disableOn: 0,

			key: null,

			midClick: false,

			mainClass: '',

			preloader: true,

			focus: '', // CSS selector of input to focus after popup is opened

			closeOnContentClick: false,

			closeOnBgClick: true,

			closeBtnInside: true,

			showCloseBtn: true,

			enableEscapeKey: true,

			modal: false,

			alignTop: false,

			removalDelay: 0,

			prependTo: null,

			fixedContentPos: 'auto',

			fixedBgPos: 'auto',

			overflowY: 'auto',

			closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',

			tClose: 'Close (Esc)',

			tLoading: 'Loading...',

			autoFocusLast: true

		}
	};

	$.fn.magnificPopup = function (options) {
		_checkInstance();

		var jqEl = $(this);

		// We call some API method of first param is a string
		if (typeof options === "string") {

			if (options === 'open') {
				var items,
				    itemOpts = _isJQ ? jqEl.data('magnificPopup') : jqEl[0].magnificPopup,
				    index = parseInt(arguments[1], 10) || 0;

				if (itemOpts.items) {
					items = itemOpts.items[index];
				} else {
					items = jqEl;
					if (itemOpts.delegate) {
						items = items.find(itemOpts.delegate);
					}
					items = items.eq(index);
				}
				mfp._openClick({ mfpEl: items }, jqEl, itemOpts);
			} else {
				if (mfp.isOpen) mfp[options].apply(mfp, Array.prototype.slice.call(arguments, 1));
			}
		} else {
			// clone options obj
			options = $.extend(true, {}, options);

			/*
    * As Zepto doesn't support .data() method for objects
    * and it works only in normal browsers
    * we assign "options" object directly to the DOM element. FTW!
    */
			if (_isJQ) {
				jqEl.data('magnificPopup', options);
			} else {
				jqEl[0].magnificPopup = options;
			}

			mfp.addGroup(jqEl, options);
		}
		return jqEl;
	};

	/*>>core*/

	/*>>inline*/

	var INLINE_NS = 'inline',
	    _hiddenClass,
	    _inlinePlaceholder,
	    _lastInlineElement,
	    _putInlineElementsBack = function _putInlineElementsBack() {
		if (_lastInlineElement) {
			_inlinePlaceholder.after(_lastInlineElement.addClass(_hiddenClass)).detach();
			_lastInlineElement = null;
		}
	};

	$.magnificPopup.registerModule(INLINE_NS, {
		options: {
			hiddenClass: 'hide', // will be appended with `mfp-` prefix
			markup: '',
			tNotFound: 'Content not found'
		},
		proto: {

			initInline: function initInline() {
				mfp.types.push(INLINE_NS);

				_mfpOn(CLOSE_EVENT + '.' + INLINE_NS, function () {
					_putInlineElementsBack();
				});
			},

			getInline: function getInline(item, template) {

				_putInlineElementsBack();

				if (item.src) {
					var inlineSt = mfp.st.inline,
					    el = $(item.src);

					if (el.length) {

						// If target element has parent - we replace it with placeholder and put it back after popup is closed
						var parent = el[0].parentNode;
						if (parent && parent.tagName) {
							if (!_inlinePlaceholder) {
								_hiddenClass = inlineSt.hiddenClass;
								_inlinePlaceholder = _getEl(_hiddenClass);
								_hiddenClass = 'mfp-' + _hiddenClass;
							}
							// replace target inline element with placeholder
							_lastInlineElement = el.after(_inlinePlaceholder).detach().removeClass(_hiddenClass);
						}

						mfp.updateStatus('ready');
					} else {
						mfp.updateStatus('error', inlineSt.tNotFound);
						el = $('<div>');
					}

					item.inlineElement = el;
					return el;
				}

				mfp.updateStatus('ready');
				mfp._parseMarkup(template, {}, item);
				return template;
			}
		}
	});

	/*>>inline*/

	/*>>ajax*/
	var AJAX_NS = 'ajax',
	    _ajaxCur,
	    _removeAjaxCursor = function _removeAjaxCursor() {
		if (_ajaxCur) {
			$(document.body).removeClass(_ajaxCur);
		}
	},
	    _destroyAjaxRequest = function _destroyAjaxRequest() {
		_removeAjaxCursor();
		if (mfp.req) {
			mfp.req.abort();
		}
	};

	$.magnificPopup.registerModule(AJAX_NS, {

		options: {
			settings: null,
			cursor: 'mfp-ajax-cur',
			tError: '<a href="%url%">The content</a> could not be loaded.'
		},

		proto: {
			initAjax: function initAjax() {
				mfp.types.push(AJAX_NS);
				_ajaxCur = mfp.st.ajax.cursor;

				_mfpOn(CLOSE_EVENT + '.' + AJAX_NS, _destroyAjaxRequest);
				_mfpOn('BeforeChange.' + AJAX_NS, _destroyAjaxRequest);
			},
			getAjax: function getAjax(item) {

				if (_ajaxCur) {
					$(document.body).addClass(_ajaxCur);
				}

				mfp.updateStatus('loading');

				var opts = $.extend({
					url: item.src,
					success: function success(data, textStatus, jqXHR) {
						var temp = {
							data: data,
							xhr: jqXHR
						};

						_mfpTrigger('ParseAjax', temp);

						mfp.appendContent($(temp.data), AJAX_NS);

						item.finished = true;

						_removeAjaxCursor();

						mfp._setFocus();

						setTimeout(function () {
							mfp.wrap.addClass(READY_CLASS);
						}, 16);

						mfp.updateStatus('ready');

						_mfpTrigger('AjaxContentAdded');
					},
					error: function error() {
						_removeAjaxCursor();
						item.finished = item.loadError = true;
						mfp.updateStatus('error', mfp.st.ajax.tError.replace('%url%', item.src));
					}
				}, mfp.st.ajax.settings);

				mfp.req = $.ajax(opts);

				return '';
			}
		}
	});

	/*>>ajax*/

	/*>>image*/
	var _imgInterval,
	    _getTitle = function _getTitle(item) {
		if (item.data && item.data.title !== undefined) return item.data.title;

		var src = mfp.st.image.titleSrc;

		if (src) {
			if ($.isFunction(src)) {
				return src.call(mfp, item);
			} else if (item.el) {
				return item.el.attr(src) || '';
			}
		}
		return '';
	};

	$.magnificPopup.registerModule('image', {

		options: {
			markup: '<div class="mfp-figure">' + '<div class="mfp-close"></div>' + '<figure>' + '<div class="mfp-img"></div>' + '<figcaption>' + '<div class="mfp-bottom-bar">' + '<div class="mfp-title"></div>' + '<div class="mfp-counter"></div>' + '</div>' + '</figcaption>' + '</figure>' + '</div>',
			cursor: 'mfp-zoom-out-cur',
			titleSrc: 'title',
			verticalFit: true,
			tError: '<a href="%url%">The image</a> could not be loaded.'
		},

		proto: {
			initImage: function initImage() {
				var imgSt = mfp.st.image,
				    ns = '.image';

				mfp.types.push('image');

				_mfpOn(OPEN_EVENT + ns, function () {
					if (mfp.currItem.type === 'image' && imgSt.cursor) {
						$(document.body).addClass(imgSt.cursor);
					}
				});

				_mfpOn(CLOSE_EVENT + ns, function () {
					if (imgSt.cursor) {
						$(document.body).removeClass(imgSt.cursor);
					}
					_window.off('resize' + EVENT_NS);
				});

				_mfpOn('Resize' + ns, mfp.resizeImage);
				if (mfp.isLowIE) {
					_mfpOn('AfterChange', mfp.resizeImage);
				}
			},
			resizeImage: function resizeImage() {
				var item = mfp.currItem;
				if (!item || !item.img) return;

				if (mfp.st.image.verticalFit) {
					var decr = 0;
					// fix box-sizing in ie7/8
					if (mfp.isLowIE) {
						decr = parseInt(item.img.css('padding-top'), 10) + parseInt(item.img.css('padding-bottom'), 10);
					}
					item.img.css('max-height', mfp.wH - decr);
				}
			},
			_onImageHasSize: function _onImageHasSize(item) {
				if (item.img) {

					item.hasSize = true;

					if (_imgInterval) {
						clearInterval(_imgInterval);
					}

					item.isCheckingImgSize = false;

					_mfpTrigger('ImageHasSize', item);

					if (item.imgHidden) {
						if (mfp.content) mfp.content.removeClass('mfp-loading');

						item.imgHidden = false;
					}
				}
			},

			/**
    * Function that loops until the image has size to display elements that rely on it asap
    */
			findImageSize: function findImageSize(item) {

				var counter = 0,
				    img = item.img[0],
				    mfpSetInterval = function mfpSetInterval(delay) {

					if (_imgInterval) {
						clearInterval(_imgInterval);
					}
					// decelerating interval that checks for size of an image
					_imgInterval = setInterval(function () {
						if (img.naturalWidth > 0) {
							mfp._onImageHasSize(item);
							return;
						}

						if (counter > 200) {
							clearInterval(_imgInterval);
						}

						counter++;
						if (counter === 3) {
							mfpSetInterval(10);
						} else if (counter === 40) {
							mfpSetInterval(50);
						} else if (counter === 100) {
							mfpSetInterval(500);
						}
					}, delay);
				};

				mfpSetInterval(1);
			},

			getImage: function getImage(item, template) {

				var guard = 0,


				// image load complete handler
				onLoadComplete = function onLoadComplete() {
					if (item) {
						if (item.img[0].complete) {
							item.img.off('.mfploader');

							if (item === mfp.currItem) {
								mfp._onImageHasSize(item);

								mfp.updateStatus('ready');
							}

							item.hasSize = true;
							item.loaded = true;

							_mfpTrigger('ImageLoadComplete');
						} else {
							// if image complete check fails 200 times (20 sec), we assume that there was an error.
							guard++;
							if (guard < 200) {
								setTimeout(onLoadComplete, 100);
							} else {
								onLoadError();
							}
						}
					}
				},


				// image error handler
				onLoadError = function onLoadError() {
					if (item) {
						item.img.off('.mfploader');
						if (item === mfp.currItem) {
							mfp._onImageHasSize(item);
							mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src));
						}

						item.hasSize = true;
						item.loaded = true;
						item.loadError = true;
					}
				},
				    imgSt = mfp.st.image;

				var el = template.find('.mfp-img');
				if (el.length) {
					var img = document.createElement('img');
					img.className = 'mfp-img';
					if (item.el && item.el.find('img').length) {
						img.alt = item.el.find('img').attr('alt');
					}
					item.img = $(img).on('load.mfploader', onLoadComplete).on('error.mfploader', onLoadError);
					img.src = item.src;

					// without clone() "error" event is not firing when IMG is replaced by new IMG
					// TODO: find a way to avoid such cloning
					if (el.is('img')) {
						item.img = item.img.clone();
					}

					img = item.img[0];
					if (img.naturalWidth > 0) {
						item.hasSize = true;
					} else if (!img.width) {
						item.hasSize = false;
					}
				}

				mfp._parseMarkup(template, {
					title: _getTitle(item),
					img_replaceWith: item.img
				}, item);

				mfp.resizeImage();

				if (item.hasSize) {
					if (_imgInterval) clearInterval(_imgInterval);

					if (item.loadError) {
						template.addClass('mfp-loading');
						mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src));
					} else {
						template.removeClass('mfp-loading');
						mfp.updateStatus('ready');
					}
					return template;
				}

				mfp.updateStatus('loading');
				item.loading = true;

				if (!item.hasSize) {
					item.imgHidden = true;
					template.addClass('mfp-loading');
					mfp.findImageSize(item);
				}

				return template;
			}
		}
	});

	/*>>image*/

	/*>>zoom*/
	var hasMozTransform,
	    getHasMozTransform = function getHasMozTransform() {
		if (hasMozTransform === undefined) {
			hasMozTransform = document.createElement('p').style.MozTransform !== undefined;
		}
		return hasMozTransform;
	};

	$.magnificPopup.registerModule('zoom', {

		options: {
			enabled: false,
			easing: 'ease-in-out',
			duration: 300,
			opener: function opener(element) {
				return element.is('img') ? element : element.find('img');
			}
		},

		proto: {

			initZoom: function initZoom() {
				var zoomSt = mfp.st.zoom,
				    ns = '.zoom',
				    image;

				if (!zoomSt.enabled || !mfp.supportsTransition) {
					return;
				}

				var duration = zoomSt.duration,
				    getElToAnimate = function getElToAnimate(image) {
					var newImg = image.clone().removeAttr('style').removeAttr('class').addClass('mfp-animated-image'),
					    transition = 'all ' + zoomSt.duration / 1000 + 's ' + zoomSt.easing,
					    cssObj = {
						position: 'fixed',
						zIndex: 9999,
						left: 0,
						top: 0,
						'-webkit-backface-visibility': 'hidden'
					},
					    t = 'transition';

					cssObj['-webkit-' + t] = cssObj['-moz-' + t] = cssObj['-o-' + t] = cssObj[t] = transition;

					newImg.css(cssObj);
					return newImg;
				},
				    showMainContent = function showMainContent() {
					mfp.content.css('visibility', 'visible');
				},
				    openTimeout,
				    animatedImg;

				_mfpOn('BuildControls' + ns, function () {
					if (mfp._allowZoom()) {

						clearTimeout(openTimeout);
						mfp.content.css('visibility', 'hidden');

						// Basically, all code below does is clones existing image, puts in on top of the current one and animated it

						image = mfp._getItemToZoom();

						if (!image) {
							showMainContent();
							return;
						}

						animatedImg = getElToAnimate(image);

						animatedImg.css(mfp._getOffset());

						mfp.wrap.append(animatedImg);

						openTimeout = setTimeout(function () {
							animatedImg.css(mfp._getOffset(true));
							openTimeout = setTimeout(function () {

								showMainContent();

								setTimeout(function () {
									animatedImg.remove();
									image = animatedImg = null;
									_mfpTrigger('ZoomAnimationEnded');
								}, 16); // avoid blink when switching images
							}, duration); // this timeout equals animation duration
						}, 16); // by adding this timeout we avoid short glitch at the beginning of animation


						// Lots of timeouts...
					}
				});
				_mfpOn(BEFORE_CLOSE_EVENT + ns, function () {
					if (mfp._allowZoom()) {

						clearTimeout(openTimeout);

						mfp.st.removalDelay = duration;

						if (!image) {
							image = mfp._getItemToZoom();
							if (!image) {
								return;
							}
							animatedImg = getElToAnimate(image);
						}

						animatedImg.css(mfp._getOffset(true));
						mfp.wrap.append(animatedImg);
						mfp.content.css('visibility', 'hidden');

						setTimeout(function () {
							animatedImg.css(mfp._getOffset());
						}, 16);
					}
				});

				_mfpOn(CLOSE_EVENT + ns, function () {
					if (mfp._allowZoom()) {
						showMainContent();
						if (animatedImg) {
							animatedImg.remove();
						}
						image = null;
					}
				});
			},

			_allowZoom: function _allowZoom() {
				return mfp.currItem.type === 'image';
			},

			_getItemToZoom: function _getItemToZoom() {
				if (mfp.currItem.hasSize) {
					return mfp.currItem.img;
				} else {
					return false;
				}
			},

			// Get element postion relative to viewport
			_getOffset: function _getOffset(isLarge) {
				var el;
				if (isLarge) {
					el = mfp.currItem.img;
				} else {
					el = mfp.st.zoom.opener(mfp.currItem.el || mfp.currItem);
				}

				var offset = el.offset();
				var paddingTop = parseInt(el.css('padding-top'), 10);
				var paddingBottom = parseInt(el.css('padding-bottom'), 10);
				offset.top -= $(window).scrollTop() - paddingTop;

				/*
    		Animating left + top + width/height looks glitchy in Firefox, but perfect in Chrome. And vice-versa.
    		 */
				var obj = {
					width: el.width(),
					// fix Zepto height+padding issue
					height: (_isJQ ? el.innerHeight() : el[0].offsetHeight) - paddingBottom - paddingTop
				};

				// I hate to do this, but there is no another option
				if (getHasMozTransform()) {
					obj['-moz-transform'] = obj['transform'] = 'translate(' + offset.left + 'px,' + offset.top + 'px)';
				} else {
					obj.left = offset.left;
					obj.top = offset.top;
				}
				return obj;
			}

		}
	});

	/*>>zoom*/

	/*>>iframe*/

	var IFRAME_NS = 'iframe',
	    _emptyPage = '//about:blank',
	    _fixIframeBugs = function _fixIframeBugs(isShowing) {
		if (mfp.currTemplate[IFRAME_NS]) {
			var el = mfp.currTemplate[IFRAME_NS].find('iframe');
			if (el.length) {
				// reset src after the popup is closed to avoid "video keeps playing after popup is closed" bug
				if (!isShowing) {
					el[0].src = _emptyPage;
				}

				// IE8 black screen bug fix
				if (mfp.isIE8) {
					el.css('display', isShowing ? 'block' : 'none');
				}
			}
		}
	};

	$.magnificPopup.registerModule(IFRAME_NS, {

		options: {
			markup: '<div class="mfp-iframe-scaler">' + '<div class="mfp-close"></div>' + '<iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe>' + '</div>',

			srcAction: 'iframe_src',

			// we don't care and support only one default type of URL by default
			patterns: {
				youtube: {
					index: 'youtube.com',
					id: 'v=',
					src: '//www.youtube.com/embed/%id%?autoplay=1'
				},
				vimeo: {
					index: 'vimeo.com/',
					id: '/',
					src: '//player.vimeo.com/video/%id%?autoplay=1'
				},
				gmaps: {
					index: '//maps.google.',
					src: '%id%&output=embed'
				}
			}
		},

		proto: {
			initIframe: function initIframe() {
				mfp.types.push(IFRAME_NS);

				_mfpOn('BeforeChange', function (e, prevType, newType) {
					if (prevType !== newType) {
						if (prevType === IFRAME_NS) {
							_fixIframeBugs(); // iframe if removed
						} else if (newType === IFRAME_NS) {
							_fixIframeBugs(true); // iframe is showing
						}
					} // else {
					// iframe source is switched, don't do anything
					//}
				});

				_mfpOn(CLOSE_EVENT + '.' + IFRAME_NS, function () {
					_fixIframeBugs();
				});
			},

			getIframe: function getIframe(item, template) {
				var embedSrc = item.src;
				var iframeSt = mfp.st.iframe;

				$.each(iframeSt.patterns, function () {
					if (embedSrc.indexOf(this.index) > -1) {
						if (this.id) {
							if (typeof this.id === 'string') {
								embedSrc = embedSrc.substr(embedSrc.lastIndexOf(this.id) + this.id.length, embedSrc.length);
							} else {
								embedSrc = this.id.call(this, embedSrc);
							}
						}
						embedSrc = this.src.replace('%id%', embedSrc);
						return false; // break;
					}
				});

				var dataObj = {};
				if (iframeSt.srcAction) {
					dataObj[iframeSt.srcAction] = embedSrc;
				}
				mfp._parseMarkup(template, dataObj, item);

				mfp.updateStatus('ready');

				return template;
			}
		}
	});

	/*>>iframe*/

	/*>>gallery*/
	/**
  * Get looped index depending on number of slides
  */
	var _getLoopedId = function _getLoopedId(index) {
		var numSlides = mfp.items.length;
		if (index > numSlides - 1) {
			return index - numSlides;
		} else if (index < 0) {
			return numSlides + index;
		}
		return index;
	},
	    _replaceCurrTotal = function _replaceCurrTotal(text, curr, total) {
		return text.replace(/%curr%/gi, curr + 1).replace(/%total%/gi, total);
	};

	$.magnificPopup.registerModule('gallery', {

		options: {
			enabled: false,
			arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
			preload: [0, 2],
			navigateByImgClick: true,
			arrows: true,

			tPrev: 'Previous (Left arrow key)',
			tNext: 'Next (Right arrow key)',
			tCounter: '%curr% of %total%'
		},

		proto: {
			initGallery: function initGallery() {

				var gSt = mfp.st.gallery,
				    ns = '.mfp-gallery';

				mfp.direction = true; // true - next, false - prev

				if (!gSt || !gSt.enabled) return false;

				_wrapClasses += ' mfp-gallery';

				_mfpOn(OPEN_EVENT + ns, function () {

					if (gSt.navigateByImgClick) {
						mfp.wrap.on('click' + ns, '.mfp-img', function () {
							if (mfp.items.length > 1) {
								mfp.next();
								return false;
							}
						});
					}

					_document.on('keydown' + ns, function (e) {
						if (e.keyCode === 37) {
							mfp.prev();
						} else if (e.keyCode === 39) {
							mfp.next();
						}
					});
				});

				_mfpOn('UpdateStatus' + ns, function (e, data) {
					if (data.text) {
						data.text = _replaceCurrTotal(data.text, mfp.currItem.index, mfp.items.length);
					}
				});

				_mfpOn(MARKUP_PARSE_EVENT + ns, function (e, element, values, item) {
					var l = mfp.items.length;
					values.counter = l > 1 ? _replaceCurrTotal(gSt.tCounter, item.index, l) : '';
				});

				_mfpOn('BuildControls' + ns, function () {
					if (mfp.items.length > 1 && gSt.arrows && !mfp.arrowLeft) {
						var markup = gSt.arrowMarkup,
						    arrowLeft = mfp.arrowLeft = $(markup.replace(/%title%/gi, gSt.tPrev).replace(/%dir%/gi, 'left')).addClass(PREVENT_CLOSE_CLASS),
						    arrowRight = mfp.arrowRight = $(markup.replace(/%title%/gi, gSt.tNext).replace(/%dir%/gi, 'right')).addClass(PREVENT_CLOSE_CLASS);

						arrowLeft.click(function () {
							mfp.prev();
						});
						arrowRight.click(function () {
							mfp.next();
						});

						mfp.container.append(arrowLeft.add(arrowRight));
					}
				});

				_mfpOn(CHANGE_EVENT + ns, function () {
					if (mfp._preloadTimeout) clearTimeout(mfp._preloadTimeout);

					mfp._preloadTimeout = setTimeout(function () {
						mfp.preloadNearbyImages();
						mfp._preloadTimeout = null;
					}, 16);
				});

				_mfpOn(CLOSE_EVENT + ns, function () {
					_document.off(ns);
					mfp.wrap.off('click' + ns);
					mfp.arrowRight = mfp.arrowLeft = null;
				});
			},
			next: function next() {
				mfp.direction = true;
				mfp.index = _getLoopedId(mfp.index + 1);
				mfp.updateItemHTML();
			},
			prev: function prev() {
				mfp.direction = false;
				mfp.index = _getLoopedId(mfp.index - 1);
				mfp.updateItemHTML();
			},
			goTo: function goTo(newIndex) {
				mfp.direction = newIndex >= mfp.index;
				mfp.index = newIndex;
				mfp.updateItemHTML();
			},
			preloadNearbyImages: function preloadNearbyImages() {
				var p = mfp.st.gallery.preload,
				    preloadBefore = Math.min(p[0], mfp.items.length),
				    preloadAfter = Math.min(p[1], mfp.items.length),
				    i;

				for (i = 1; i <= (mfp.direction ? preloadAfter : preloadBefore); i++) {
					mfp._preloadItem(mfp.index + i);
				}
				for (i = 1; i <= (mfp.direction ? preloadBefore : preloadAfter); i++) {
					mfp._preloadItem(mfp.index - i);
				}
			},
			_preloadItem: function _preloadItem(index) {
				index = _getLoopedId(index);

				if (mfp.items[index].preloaded) {
					return;
				}

				var item = mfp.items[index];
				if (!item.parsed) {
					item = mfp.parseEl(index);
				}

				_mfpTrigger('LazyLoad', item);

				if (item.type === 'image') {
					item.img = $('<img class="mfp-img" />').on('load.mfploader', function () {
						item.hasSize = true;
					}).on('error.mfploader', function () {
						item.hasSize = true;
						item.loadError = true;
						_mfpTrigger('LazyLoadError', item);
					}).attr('src', item.src);
				}

				item.preloaded = true;
			}
		}
	});

	/*>>gallery*/

	/*>>retina*/

	var RETINA_NS = 'retina';

	$.magnificPopup.registerModule(RETINA_NS, {
		options: {
			replaceSrc: function replaceSrc(item) {
				return item.src.replace(/\.\w+$/, function (m) {
					return '@2x' + m;
				});
			},
			ratio: 1 // Function or number.  Set to 1 to disable.
		},
		proto: {
			initRetina: function initRetina() {
				if (window.devicePixelRatio > 1) {

					var st = mfp.st.retina,
					    ratio = st.ratio;

					ratio = !isNaN(ratio) ? ratio : ratio();

					if (ratio > 1) {
						_mfpOn('ImageHasSize' + '.' + RETINA_NS, function (e, item) {
							item.img.css({
								'max-width': item.img[0].naturalWidth / ratio,
								'width': '100%'
							});
						});
						_mfpOn('ElementParse' + '.' + RETINA_NS, function (e, item) {
							item.src = st.replaceSrc(item, ratio);
						});
					}
				}
			}
		}
	});

	/*>>retina*/
	_checkInstance();
});
/*! Magnific Popup - v1.1.0 - 2016-02-20
* http://dimsemenov.com/plugins/magnific-popup/
* Copyright (c) 2016 Dmitry Semenov; */
;(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module. 
		define(['jquery'], factory);
	} else if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object') {
		// Node/CommonJS 
		factory(require('jquery'));
	} else {
		// Browser globals 
		factory(window.jQuery || window.Zepto);
	}
})(function ($) {

	/*>>core*/
	/**
  * 
  * Magnific Popup Core JS file
  * 
  */

	/**
  * Private static constants
  */
	var CLOSE_EVENT = 'Close',
	    BEFORE_CLOSE_EVENT = 'BeforeClose',
	    AFTER_CLOSE_EVENT = 'AfterClose',
	    BEFORE_APPEND_EVENT = 'BeforeAppend',
	    MARKUP_PARSE_EVENT = 'MarkupParse',
	    OPEN_EVENT = 'Open',
	    CHANGE_EVENT = 'Change',
	    NS = 'mfp',
	    EVENT_NS = '.' + NS,
	    READY_CLASS = 'mfp-ready',
	    REMOVING_CLASS = 'mfp-removing',
	    PREVENT_CLOSE_CLASS = 'mfp-prevent-close';

	/**
  * Private vars 
  */
	/*jshint -W079 */
	var mfp,
	    // As we have only one instance of MagnificPopup object, we define it locally to not to use 'this'
	MagnificPopup = function MagnificPopup() {},
	    _isJQ = !!window.jQuery,
	    _prevStatus,
	    _window = $(window),
	    _document,
	    _prevContentType,
	    _wrapClasses,
	    _currPopupType;

	/**
  * Private functions
  */
	var _mfpOn = function _mfpOn(name, f) {
		mfp.ev.on(NS + name + EVENT_NS, f);
	},
	    _getEl = function _getEl(className, appendTo, html, raw) {
		var el = document.createElement('div');
		el.className = 'mfp-' + className;
		if (html) {
			el.innerHTML = html;
		}
		if (!raw) {
			el = $(el);
			if (appendTo) {
				el.appendTo(appendTo);
			}
		} else if (appendTo) {
			appendTo.appendChild(el);
		}
		return el;
	},
	    _mfpTrigger = function _mfpTrigger(e, data) {
		mfp.ev.triggerHandler(NS + e, data);

		if (mfp.st.callbacks) {
			// converts "mfpEventName" to "eventName" callback and triggers it if it's present
			e = e.charAt(0).toLowerCase() + e.slice(1);
			if (mfp.st.callbacks[e]) {
				mfp.st.callbacks[e].apply(mfp, $.isArray(data) ? data : [data]);
			}
		}
	},
	    _getCloseBtn = function _getCloseBtn(type) {
		if (type !== _currPopupType || !mfp.currTemplate.closeBtn) {
			mfp.currTemplate.closeBtn = $(mfp.st.closeMarkup.replace('%title%', mfp.st.tClose));
			_currPopupType = type;
		}
		return mfp.currTemplate.closeBtn;
	},

	// Initialize Magnific Popup only when called at least once
	_checkInstance = function _checkInstance() {
		if (!$.magnificPopup.instance) {
			/*jshint -W020 */
			mfp = new MagnificPopup();
			mfp.init();
			$.magnificPopup.instance = mfp;
		}
	},

	// CSS transition detection, http://stackoverflow.com/questions/7264899/detect-css-transitions-using-javascript-and-without-modernizr
	supportsTransitions = function supportsTransitions() {
		var s = document.createElement('p').style,
		    // 's' for style. better to create an element if body yet to exist
		v = ['ms', 'O', 'Moz', 'Webkit']; // 'v' for vendor

		if (s['transition'] !== undefined) {
			return true;
		}

		while (v.length) {
			if (v.pop() + 'Transition' in s) {
				return true;
			}
		}

		return false;
	};

	/**
  * Public functions
  */
	MagnificPopup.prototype = {

		constructor: MagnificPopup,

		/**
   * Initializes Magnific Popup plugin. 
   * This function is triggered only once when $.fn.magnificPopup or $.magnificPopup is executed
   */
		init: function init() {
			var appVersion = navigator.appVersion;
			mfp.isLowIE = mfp.isIE8 = document.all && !document.addEventListener;
			mfp.isAndroid = /android/gi.test(appVersion);
			mfp.isIOS = /iphone|ipad|ipod/gi.test(appVersion);
			mfp.supportsTransition = supportsTransitions();

			// We disable fixed positioned lightbox on devices that don't handle it nicely.
			// If you know a better way of detecting this - let me know.
			mfp.probablyMobile = mfp.isAndroid || mfp.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent);
			_document = $(document);

			mfp.popupsCache = {};
		},

		/**
   * Opens popup
   * @param  data [description]
   */
		open: function open(data) {

			var i;

			if (data.isObj === false) {
				// convert jQuery collection to array to avoid conflicts later
				mfp.items = data.items.toArray();

				mfp.index = 0;
				var items = data.items,
				    item;
				for (i = 0; i < items.length; i++) {
					item = items[i];
					if (item.parsed) {
						item = item.el[0];
					}
					if (item === data.el[0]) {
						mfp.index = i;
						break;
					}
				}
			} else {
				mfp.items = $.isArray(data.items) ? data.items : [data.items];
				mfp.index = data.index || 0;
			}

			// if popup is already opened - we just update the content
			if (mfp.isOpen) {
				mfp.updateItemHTML();
				return;
			}

			mfp.types = [];
			_wrapClasses = '';
			if (data.mainEl && data.mainEl.length) {
				mfp.ev = data.mainEl.eq(0);
			} else {
				mfp.ev = _document;
			}

			if (data.key) {
				if (!mfp.popupsCache[data.key]) {
					mfp.popupsCache[data.key] = {};
				}
				mfp.currTemplate = mfp.popupsCache[data.key];
			} else {
				mfp.currTemplate = {};
			}

			mfp.st = $.extend(true, {}, $.magnificPopup.defaults, data);
			mfp.fixedContentPos = mfp.st.fixedContentPos === 'auto' ? !mfp.probablyMobile : mfp.st.fixedContentPos;

			if (mfp.st.modal) {
				mfp.st.closeOnContentClick = false;
				mfp.st.closeOnBgClick = false;
				mfp.st.showCloseBtn = false;
				mfp.st.enableEscapeKey = false;
			}

			// Building markup
			// main containers are created only once
			if (!mfp.bgOverlay) {

				// Dark overlay
				mfp.bgOverlay = _getEl('bg').on('click' + EVENT_NS, function () {
					mfp.close();
				});

				mfp.wrap = _getEl('wrap').attr('tabindex', -1).on('click' + EVENT_NS, function (e) {
					if (mfp._checkIfClose(e.target)) {
						mfp.close();
					}
				});

				mfp.container = _getEl('container', mfp.wrap);
			}

			mfp.contentContainer = _getEl('content');
			if (mfp.st.preloader) {
				mfp.preloader = _getEl('preloader', mfp.container, mfp.st.tLoading);
			}

			// Initializing modules
			var modules = $.magnificPopup.modules;
			for (i = 0; i < modules.length; i++) {
				var n = modules[i];
				n = n.charAt(0).toUpperCase() + n.slice(1);
				mfp['init' + n].call(mfp);
			}
			_mfpTrigger('BeforeOpen');

			if (mfp.st.showCloseBtn) {
				// Close button
				if (!mfp.st.closeBtnInside) {
					mfp.wrap.append(_getCloseBtn());
				} else {
					_mfpOn(MARKUP_PARSE_EVENT, function (e, template, values, item) {
						values.close_replaceWith = _getCloseBtn(item.type);
					});
					_wrapClasses += ' mfp-close-btn-in';
				}
			}

			if (mfp.st.alignTop) {
				_wrapClasses += ' mfp-align-top';
			}

			if (mfp.fixedContentPos) {
				mfp.wrap.css({
					overflow: mfp.st.overflowY,
					overflowX: 'hidden',
					overflowY: mfp.st.overflowY
				});
			} else {
				mfp.wrap.css({
					top: _window.scrollTop(),
					position: 'absolute'
				});
			}
			if (mfp.st.fixedBgPos === false || mfp.st.fixedBgPos === 'auto' && !mfp.fixedContentPos) {
				mfp.bgOverlay.css({
					height: _document.height(),
					position: 'absolute'
				});
			}

			if (mfp.st.enableEscapeKey) {
				// Close on ESC key
				_document.on('keyup' + EVENT_NS, function (e) {
					if (e.keyCode === 27) {
						mfp.close();
					}
				});
			}

			_window.on('resize' + EVENT_NS, function () {
				mfp.updateSize();
			});

			if (!mfp.st.closeOnContentClick) {
				_wrapClasses += ' mfp-auto-cursor';
			}

			if (_wrapClasses) mfp.wrap.addClass(_wrapClasses);

			// this triggers recalculation of layout, so we get it once to not to trigger twice
			var windowHeight = mfp.wH = _window.height();

			var windowStyles = {};

			if (mfp.fixedContentPos) {
				if (mfp._hasScrollBar(windowHeight)) {
					var s = mfp._getScrollbarSize();
					if (s) {
						windowStyles.marginRight = s;
					}
				}
			}

			if (mfp.fixedContentPos) {
				if (!mfp.isIE7) {
					windowStyles.overflow = 'hidden';
				} else {
					// ie7 double-scroll bug
					$('body, html').css('overflow', 'hidden');
				}
			}

			var classesToadd = mfp.st.mainClass;
			if (mfp.isIE7) {
				classesToadd += ' mfp-ie7';
			}
			if (classesToadd) {
				mfp._addClassToMFP(classesToadd);
			}

			// add content
			mfp.updateItemHTML();

			_mfpTrigger('BuildControls');

			// remove scrollbar, add margin e.t.c
			$('html').css(windowStyles);

			// add everything to DOM
			mfp.bgOverlay.add(mfp.wrap).prependTo(mfp.st.prependTo || $(document.body));

			// Save last focused element
			mfp._lastFocusedEl = document.activeElement;

			// Wait for next cycle to allow CSS transition
			setTimeout(function () {

				if (mfp.content) {
					mfp._addClassToMFP(READY_CLASS);
					mfp._setFocus();
				} else {
					// if content is not defined (not loaded e.t.c) we add class only for BG
					mfp.bgOverlay.addClass(READY_CLASS);
				}

				// Trap the focus in popup
				_document.on('focusin' + EVENT_NS, mfp._onFocusIn);
			}, 16);

			mfp.isOpen = true;
			mfp.updateSize(windowHeight);
			_mfpTrigger(OPEN_EVENT);

			return data;
		},

		/**
   * Closes the popup
   */
		close: function close() {
			if (!mfp.isOpen) return;
			_mfpTrigger(BEFORE_CLOSE_EVENT);

			mfp.isOpen = false;
			// for CSS3 animation
			if (mfp.st.removalDelay && !mfp.isLowIE && mfp.supportsTransition) {
				mfp._addClassToMFP(REMOVING_CLASS);
				setTimeout(function () {
					mfp._close();
				}, mfp.st.removalDelay);
			} else {
				mfp._close();
			}
		},

		/**
   * Helper for close() function
   */
		_close: function _close() {
			_mfpTrigger(CLOSE_EVENT);

			var classesToRemove = REMOVING_CLASS + ' ' + READY_CLASS + ' ';

			mfp.bgOverlay.detach();
			mfp.wrap.detach();
			mfp.container.empty();

			if (mfp.st.mainClass) {
				classesToRemove += mfp.st.mainClass + ' ';
			}

			mfp._removeClassFromMFP(classesToRemove);

			if (mfp.fixedContentPos) {
				var windowStyles = { marginRight: '' };
				if (mfp.isIE7) {
					$('body, html').css('overflow', '');
				} else {
					windowStyles.overflow = '';
				}
				$('html').css(windowStyles);
			}

			_document.off('keyup' + EVENT_NS + ' focusin' + EVENT_NS);
			mfp.ev.off(EVENT_NS);

			// clean up DOM elements that aren't removed
			mfp.wrap.attr('class', 'mfp-wrap').removeAttr('style');
			mfp.bgOverlay.attr('class', 'mfp-bg');
			mfp.container.attr('class', 'mfp-container');

			// remove close button from target element
			if (mfp.st.showCloseBtn && (!mfp.st.closeBtnInside || mfp.currTemplate[mfp.currItem.type] === true)) {
				if (mfp.currTemplate.closeBtn) mfp.currTemplate.closeBtn.detach();
			}

			if (mfp.st.autoFocusLast && mfp._lastFocusedEl) {
				$(mfp._lastFocusedEl).focus(); // put tab focus back
			}
			mfp.currItem = null;
			mfp.content = null;
			mfp.currTemplate = null;
			mfp.prevHeight = 0;

			_mfpTrigger(AFTER_CLOSE_EVENT);
		},

		updateSize: function updateSize(winHeight) {

			if (mfp.isIOS) {
				// fixes iOS nav bars https://github.com/dimsemenov/Magnific-Popup/issues/2
				var zoomLevel = document.documentElement.clientWidth / window.innerWidth;
				var height = window.innerHeight * zoomLevel;
				mfp.wrap.css('height', height);
				mfp.wH = height;
			} else {
				mfp.wH = winHeight || _window.height();
			}
			// Fixes #84: popup incorrectly positioned with position:relative on body
			if (!mfp.fixedContentPos) {
				mfp.wrap.css('height', mfp.wH);
			}

			_mfpTrigger('Resize');
		},

		/**
   * Set content of popup based on current index
   */
		updateItemHTML: function updateItemHTML() {
			var item = mfp.items[mfp.index];

			// Detach and perform modifications
			mfp.contentContainer.detach();

			if (mfp.content) mfp.content.detach();

			if (!item.parsed) {
				item = mfp.parseEl(mfp.index);
			}

			var type = item.type;

			_mfpTrigger('BeforeChange', [mfp.currItem ? mfp.currItem.type : '', type]);
			// BeforeChange event works like so:
			// _mfpOn('BeforeChange', function(e, prevType, newType) { });

			mfp.currItem = item;

			if (!mfp.currTemplate[type]) {
				var markup = mfp.st[type] ? mfp.st[type].markup : false;

				// allows to modify markup
				_mfpTrigger('FirstMarkupParse', markup);

				if (markup) {
					mfp.currTemplate[type] = $(markup);
				} else {
					// if there is no markup found we just define that template is parsed
					mfp.currTemplate[type] = true;
				}
			}

			if (_prevContentType && _prevContentType !== item.type) {
				mfp.container.removeClass('mfp-' + _prevContentType + '-holder');
			}

			var newContent = mfp['get' + type.charAt(0).toUpperCase() + type.slice(1)](item, mfp.currTemplate[type]);
			mfp.appendContent(newContent, type);

			item.preloaded = true;

			_mfpTrigger(CHANGE_EVENT, item);
			_prevContentType = item.type;

			// Append container back after its content changed
			mfp.container.prepend(mfp.contentContainer);

			_mfpTrigger('AfterChange');
		},

		/**
   * Set HTML content of popup
   */
		appendContent: function appendContent(newContent, type) {
			mfp.content = newContent;

			if (newContent) {
				if (mfp.st.showCloseBtn && mfp.st.closeBtnInside && mfp.currTemplate[type] === true) {
					// if there is no markup, we just append close button element inside
					if (!mfp.content.find('.mfp-close').length) {
						mfp.content.append(_getCloseBtn());
					}
				} else {
					mfp.content = newContent;
				}
			} else {
				mfp.content = '';
			}

			_mfpTrigger(BEFORE_APPEND_EVENT);
			mfp.container.addClass('mfp-' + type + '-holder');

			mfp.contentContainer.append(mfp.content);
		},

		/**
   * Creates Magnific Popup data object based on given data
   * @param  {int} index Index of item to parse
   */
		parseEl: function parseEl(index) {
			var item = mfp.items[index],
			    type;

			if (item.tagName) {
				item = { el: $(item) };
			} else {
				type = item.type;
				item = { data: item, src: item.src };
			}

			if (item.el) {
				var types = mfp.types;

				// check for 'mfp-TYPE' class
				for (var i = 0; i < types.length; i++) {
					if (item.el.hasClass('mfp-' + types[i])) {
						type = types[i];
						break;
					}
				}

				item.src = item.el.attr('data-mfp-src');
				if (!item.src) {
					item.src = item.el.attr('href');
				}
			}

			item.type = type || mfp.st.type || 'inline';
			item.index = index;
			item.parsed = true;
			mfp.items[index] = item;
			_mfpTrigger('ElementParse', item);

			return mfp.items[index];
		},

		/**
   * Initializes single popup or a group of popups
   */
		addGroup: function addGroup(el, options) {
			var eHandler = function eHandler(e) {
				e.mfpEl = this;
				mfp._openClick(e, el, options);
			};

			if (!options) {
				options = {};
			}

			var eName = 'click.magnificPopup';
			options.mainEl = el;

			if (options.items) {
				options.isObj = true;
				el.off(eName).on(eName, eHandler);
			} else {
				options.isObj = false;
				if (options.delegate) {
					el.off(eName).on(eName, options.delegate, eHandler);
				} else {
					options.items = el;
					el.off(eName).on(eName, eHandler);
				}
			}
		},
		_openClick: function _openClick(e, el, options) {
			var midClick = options.midClick !== undefined ? options.midClick : $.magnificPopup.defaults.midClick;

			if (!midClick && (e.which === 2 || e.ctrlKey || e.metaKey || e.altKey || e.shiftKey)) {
				return;
			}

			var disableOn = options.disableOn !== undefined ? options.disableOn : $.magnificPopup.defaults.disableOn;

			if (disableOn) {
				if ($.isFunction(disableOn)) {
					if (!disableOn.call(mfp)) {
						return true;
					}
				} else {
					// else it's number
					if (_window.width() < disableOn) {
						return true;
					}
				}
			}

			if (e.type) {
				e.preventDefault();

				// This will prevent popup from closing if element is inside and popup is already opened
				if (mfp.isOpen) {
					e.stopPropagation();
				}
			}

			options.el = $(e.mfpEl);
			if (options.delegate) {
				options.items = el.find(options.delegate);
			}
			mfp.open(options);
		},

		/**
   * Updates text on preloader
   */
		updateStatus: function updateStatus(status, text) {

			if (mfp.preloader) {
				if (_prevStatus !== status) {
					mfp.container.removeClass('mfp-s-' + _prevStatus);
				}

				if (!text && status === 'loading') {
					text = mfp.st.tLoading;
				}

				var data = {
					status: status,
					text: text
				};
				// allows to modify status
				_mfpTrigger('UpdateStatus', data);

				status = data.status;
				text = data.text;

				mfp.preloader.html(text);

				mfp.preloader.find('a').on('click', function (e) {
					e.stopImmediatePropagation();
				});

				mfp.container.addClass('mfp-s-' + status);
				_prevStatus = status;
			}
		},

		/*
  	"Private" helpers that aren't private at all
   */
		// Check to close popup or not
		// "target" is an element that was clicked
		_checkIfClose: function _checkIfClose(target) {

			if ($(target).hasClass(PREVENT_CLOSE_CLASS)) {
				return;
			}

			var closeOnContent = mfp.st.closeOnContentClick;
			var closeOnBg = mfp.st.closeOnBgClick;

			if (closeOnContent && closeOnBg) {
				return true;
			} else {

				// We close the popup if click is on close button or on preloader. Or if there is no content.
				if (!mfp.content || $(target).hasClass('mfp-close') || mfp.preloader && target === mfp.preloader[0]) {
					return true;
				}

				// if click is outside the content
				if (target !== mfp.content[0] && !$.contains(mfp.content[0], target)) {
					if (closeOnBg) {
						// last check, if the clicked element is in DOM, (in case it's removed onclick)
						if ($.contains(document, target)) {
							return true;
						}
					}
				} else if (closeOnContent) {
					return true;
				}
			}
			return false;
		},
		_addClassToMFP: function _addClassToMFP(cName) {
			mfp.bgOverlay.addClass(cName);
			mfp.wrap.addClass(cName);
		},
		_removeClassFromMFP: function _removeClassFromMFP(cName) {
			this.bgOverlay.removeClass(cName);
			mfp.wrap.removeClass(cName);
		},
		_hasScrollBar: function _hasScrollBar(winHeight) {
			return (mfp.isIE7 ? _document.height() : document.body.scrollHeight) > (winHeight || _window.height());
		},
		_setFocus: function _setFocus() {
			(mfp.st.focus ? mfp.content.find(mfp.st.focus).eq(0) : mfp.wrap).focus();
		},
		_onFocusIn: function _onFocusIn(e) {
			if (e.target !== mfp.wrap[0] && !$.contains(mfp.wrap[0], e.target)) {
				mfp._setFocus();
				return false;
			}
		},
		_parseMarkup: function _parseMarkup(template, values, item) {
			var arr;
			if (item.data) {
				values = $.extend(item.data, values);
			}
			_mfpTrigger(MARKUP_PARSE_EVENT, [template, values, item]);

			$.each(values, function (key, value) {
				if (value === undefined || value === false) {
					return true;
				}
				arr = key.split('_');
				if (arr.length > 1) {
					var el = template.find(EVENT_NS + '-' + arr[0]);

					if (el.length > 0) {
						var attr = arr[1];
						if (attr === 'replaceWith') {
							if (el[0] !== value[0]) {
								el.replaceWith(value);
							}
						} else if (attr === 'img') {
							if (el.is('img')) {
								el.attr('src', value);
							} else {
								el.replaceWith($('<img>').attr('src', value).attr('class', el.attr('class')));
							}
						} else {
							el.attr(arr[1], value);
						}
					}
				} else {
					template.find(EVENT_NS + '-' + key).html(value);
				}
			});
		},

		_getScrollbarSize: function _getScrollbarSize() {
			// thx David
			if (mfp.scrollbarSize === undefined) {
				var scrollDiv = document.createElement("div");
				scrollDiv.style.cssText = 'width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;';
				document.body.appendChild(scrollDiv);
				mfp.scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
				document.body.removeChild(scrollDiv);
			}
			return mfp.scrollbarSize;
		}

	}; /* MagnificPopup core prototype end */

	/**
  * Public static functions
  */
	$.magnificPopup = {
		instance: null,
		proto: MagnificPopup.prototype,
		modules: [],

		open: function open(options, index) {
			_checkInstance();

			if (!options) {
				options = {};
			} else {
				options = $.extend(true, {}, options);
			}

			options.isObj = true;
			options.index = index || 0;
			return this.instance.open(options);
		},

		close: function close() {
			return $.magnificPopup.instance && $.magnificPopup.instance.close();
		},

		registerModule: function registerModule(name, module) {
			if (module.options) {
				$.magnificPopup.defaults[name] = module.options;
			}
			$.extend(this.proto, module.proto);
			this.modules.push(name);
		},

		defaults: {

			// Info about options is in docs:
			// http://dimsemenov.com/plugins/magnific-popup/documentation.html#options

			disableOn: 0,

			key: null,

			midClick: false,

			mainClass: '',

			preloader: true,

			focus: '', // CSS selector of input to focus after popup is opened

			closeOnContentClick: false,

			closeOnBgClick: true,

			closeBtnInside: true,

			showCloseBtn: true,

			enableEscapeKey: true,

			modal: false,

			alignTop: false,

			removalDelay: 0,

			prependTo: null,

			fixedContentPos: 'auto',

			fixedBgPos: 'auto',

			overflowY: 'auto',

			closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',

			tClose: 'Close (Esc)',

			tLoading: 'Loading...',

			autoFocusLast: true

		}
	};

	$.fn.magnificPopup = function (options) {
		_checkInstance();

		var jqEl = $(this);

		// We call some API method of first param is a string
		if (typeof options === "string") {

			if (options === 'open') {
				var items,
				    itemOpts = _isJQ ? jqEl.data('magnificPopup') : jqEl[0].magnificPopup,
				    index = parseInt(arguments[1], 10) || 0;

				if (itemOpts.items) {
					items = itemOpts.items[index];
				} else {
					items = jqEl;
					if (itemOpts.delegate) {
						items = items.find(itemOpts.delegate);
					}
					items = items.eq(index);
				}
				mfp._openClick({ mfpEl: items }, jqEl, itemOpts);
			} else {
				if (mfp.isOpen) mfp[options].apply(mfp, Array.prototype.slice.call(arguments, 1));
			}
		} else {
			// clone options obj
			options = $.extend(true, {}, options);

			/*
    * As Zepto doesn't support .data() method for objects
    * and it works only in normal browsers
    * we assign "options" object directly to the DOM element. FTW!
    */
			if (_isJQ) {
				jqEl.data('magnificPopup', options);
			} else {
				jqEl[0].magnificPopup = options;
			}

			mfp.addGroup(jqEl, options);
		}
		return jqEl;
	};

	/*>>core*/

	/*>>inline*/

	var INLINE_NS = 'inline',
	    _hiddenClass,
	    _inlinePlaceholder,
	    _lastInlineElement,
	    _putInlineElementsBack = function _putInlineElementsBack() {
		if (_lastInlineElement) {
			_inlinePlaceholder.after(_lastInlineElement.addClass(_hiddenClass)).detach();
			_lastInlineElement = null;
		}
	};

	$.magnificPopup.registerModule(INLINE_NS, {
		options: {
			hiddenClass: 'hide', // will be appended with `mfp-` prefix
			markup: '',
			tNotFound: 'Content not found'
		},
		proto: {

			initInline: function initInline() {
				mfp.types.push(INLINE_NS);

				_mfpOn(CLOSE_EVENT + '.' + INLINE_NS, function () {
					_putInlineElementsBack();
				});
			},

			getInline: function getInline(item, template) {

				_putInlineElementsBack();

				if (item.src) {
					var inlineSt = mfp.st.inline,
					    el = $(item.src);

					if (el.length) {

						// If target element has parent - we replace it with placeholder and put it back after popup is closed
						var parent = el[0].parentNode;
						if (parent && parent.tagName) {
							if (!_inlinePlaceholder) {
								_hiddenClass = inlineSt.hiddenClass;
								_inlinePlaceholder = _getEl(_hiddenClass);
								_hiddenClass = 'mfp-' + _hiddenClass;
							}
							// replace target inline element with placeholder
							_lastInlineElement = el.after(_inlinePlaceholder).detach().removeClass(_hiddenClass);
						}

						mfp.updateStatus('ready');
					} else {
						mfp.updateStatus('error', inlineSt.tNotFound);
						el = $('<div>');
					}

					item.inlineElement = el;
					return el;
				}

				mfp.updateStatus('ready');
				mfp._parseMarkup(template, {}, item);
				return template;
			}
		}
	});

	/*>>inline*/

	/*>>ajax*/
	var AJAX_NS = 'ajax',
	    _ajaxCur,
	    _removeAjaxCursor = function _removeAjaxCursor() {
		if (_ajaxCur) {
			$(document.body).removeClass(_ajaxCur);
		}
	},
	    _destroyAjaxRequest = function _destroyAjaxRequest() {
		_removeAjaxCursor();
		if (mfp.req) {
			mfp.req.abort();
		}
	};

	$.magnificPopup.registerModule(AJAX_NS, {

		options: {
			settings: null,
			cursor: 'mfp-ajax-cur',
			tError: '<a href="%url%">The content</a> could not be loaded.'
		},

		proto: {
			initAjax: function initAjax() {
				mfp.types.push(AJAX_NS);
				_ajaxCur = mfp.st.ajax.cursor;

				_mfpOn(CLOSE_EVENT + '.' + AJAX_NS, _destroyAjaxRequest);
				_mfpOn('BeforeChange.' + AJAX_NS, _destroyAjaxRequest);
			},
			getAjax: function getAjax(item) {

				if (_ajaxCur) {
					$(document.body).addClass(_ajaxCur);
				}

				mfp.updateStatus('loading');

				var opts = $.extend({
					url: item.src,
					success: function success(data, textStatus, jqXHR) {
						var temp = {
							data: data,
							xhr: jqXHR
						};

						_mfpTrigger('ParseAjax', temp);

						mfp.appendContent($(temp.data), AJAX_NS);

						item.finished = true;

						_removeAjaxCursor();

						mfp._setFocus();

						setTimeout(function () {
							mfp.wrap.addClass(READY_CLASS);
						}, 16);

						mfp.updateStatus('ready');

						_mfpTrigger('AjaxContentAdded');
					},
					error: function error() {
						_removeAjaxCursor();
						item.finished = item.loadError = true;
						mfp.updateStatus('error', mfp.st.ajax.tError.replace('%url%', item.src));
					}
				}, mfp.st.ajax.settings);

				mfp.req = $.ajax(opts);

				return '';
			}
		}
	});

	/*>>ajax*/

	/*>>image*/
	var _imgInterval,
	    _getTitle = function _getTitle(item) {
		if (item.data && item.data.title !== undefined) return item.data.title;

		var src = mfp.st.image.titleSrc;

		if (src) {
			if ($.isFunction(src)) {
				return src.call(mfp, item);
			} else if (item.el) {
				return item.el.attr(src) || '';
			}
		}
		return '';
	};

	$.magnificPopup.registerModule('image', {

		options: {
			markup: '<div class="mfp-figure">' + '<div class="mfp-close"></div>' + '<figure>' + '<div class="mfp-img"></div>' + '<figcaption>' + '<div class="mfp-bottom-bar">' + '<div class="mfp-title"></div>' + '<div class="mfp-counter"></div>' + '</div>' + '</figcaption>' + '</figure>' + '</div>',
			cursor: 'mfp-zoom-out-cur',
			titleSrc: 'title',
			verticalFit: true,
			tError: '<a href="%url%">The image</a> could not be loaded.'
		},

		proto: {
			initImage: function initImage() {
				var imgSt = mfp.st.image,
				    ns = '.image';

				mfp.types.push('image');

				_mfpOn(OPEN_EVENT + ns, function () {
					if (mfp.currItem.type === 'image' && imgSt.cursor) {
						$(document.body).addClass(imgSt.cursor);
					}
				});

				_mfpOn(CLOSE_EVENT + ns, function () {
					if (imgSt.cursor) {
						$(document.body).removeClass(imgSt.cursor);
					}
					_window.off('resize' + EVENT_NS);
				});

				_mfpOn('Resize' + ns, mfp.resizeImage);
				if (mfp.isLowIE) {
					_mfpOn('AfterChange', mfp.resizeImage);
				}
			},
			resizeImage: function resizeImage() {
				var item = mfp.currItem;
				if (!item || !item.img) return;

				if (mfp.st.image.verticalFit) {
					var decr = 0;
					// fix box-sizing in ie7/8
					if (mfp.isLowIE) {
						decr = parseInt(item.img.css('padding-top'), 10) + parseInt(item.img.css('padding-bottom'), 10);
					}
					item.img.css('max-height', mfp.wH - decr);
				}
			},
			_onImageHasSize: function _onImageHasSize(item) {
				if (item.img) {

					item.hasSize = true;

					if (_imgInterval) {
						clearInterval(_imgInterval);
					}

					item.isCheckingImgSize = false;

					_mfpTrigger('ImageHasSize', item);

					if (item.imgHidden) {
						if (mfp.content) mfp.content.removeClass('mfp-loading');

						item.imgHidden = false;
					}
				}
			},

			/**
    * Function that loops until the image has size to display elements that rely on it asap
    */
			findImageSize: function findImageSize(item) {

				var counter = 0,
				    img = item.img[0],
				    mfpSetInterval = function mfpSetInterval(delay) {

					if (_imgInterval) {
						clearInterval(_imgInterval);
					}
					// decelerating interval that checks for size of an image
					_imgInterval = setInterval(function () {
						if (img.naturalWidth > 0) {
							mfp._onImageHasSize(item);
							return;
						}

						if (counter > 200) {
							clearInterval(_imgInterval);
						}

						counter++;
						if (counter === 3) {
							mfpSetInterval(10);
						} else if (counter === 40) {
							mfpSetInterval(50);
						} else if (counter === 100) {
							mfpSetInterval(500);
						}
					}, delay);
				};

				mfpSetInterval(1);
			},

			getImage: function getImage(item, template) {

				var guard = 0,


				// image load complete handler
				onLoadComplete = function onLoadComplete() {
					if (item) {
						if (item.img[0].complete) {
							item.img.off('.mfploader');

							if (item === mfp.currItem) {
								mfp._onImageHasSize(item);

								mfp.updateStatus('ready');
							}

							item.hasSize = true;
							item.loaded = true;

							_mfpTrigger('ImageLoadComplete');
						} else {
							// if image complete check fails 200 times (20 sec), we assume that there was an error.
							guard++;
							if (guard < 200) {
								setTimeout(onLoadComplete, 100);
							} else {
								onLoadError();
							}
						}
					}
				},


				// image error handler
				onLoadError = function onLoadError() {
					if (item) {
						item.img.off('.mfploader');
						if (item === mfp.currItem) {
							mfp._onImageHasSize(item);
							mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src));
						}

						item.hasSize = true;
						item.loaded = true;
						item.loadError = true;
					}
				},
				    imgSt = mfp.st.image;

				var el = template.find('.mfp-img');
				if (el.length) {
					var img = document.createElement('img');
					img.className = 'mfp-img';
					if (item.el && item.el.find('img').length) {
						img.alt = item.el.find('img').attr('alt');
					}
					item.img = $(img).on('load.mfploader', onLoadComplete).on('error.mfploader', onLoadError);
					img.src = item.src;

					// without clone() "error" event is not firing when IMG is replaced by new IMG
					// TODO: find a way to avoid such cloning
					if (el.is('img')) {
						item.img = item.img.clone();
					}

					img = item.img[0];
					if (img.naturalWidth > 0) {
						item.hasSize = true;
					} else if (!img.width) {
						item.hasSize = false;
					}
				}

				mfp._parseMarkup(template, {
					title: _getTitle(item),
					img_replaceWith: item.img
				}, item);

				mfp.resizeImage();

				if (item.hasSize) {
					if (_imgInterval) clearInterval(_imgInterval);

					if (item.loadError) {
						template.addClass('mfp-loading');
						mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src));
					} else {
						template.removeClass('mfp-loading');
						mfp.updateStatus('ready');
					}
					return template;
				}

				mfp.updateStatus('loading');
				item.loading = true;

				if (!item.hasSize) {
					item.imgHidden = true;
					template.addClass('mfp-loading');
					mfp.findImageSize(item);
				}

				return template;
			}
		}
	});

	/*>>image*/

	/*>>zoom*/
	var hasMozTransform,
	    getHasMozTransform = function getHasMozTransform() {
		if (hasMozTransform === undefined) {
			hasMozTransform = document.createElement('p').style.MozTransform !== undefined;
		}
		return hasMozTransform;
	};

	$.magnificPopup.registerModule('zoom', {

		options: {
			enabled: false,
			easing: 'ease-in-out',
			duration: 300,
			opener: function opener(element) {
				return element.is('img') ? element : element.find('img');
			}
		},

		proto: {

			initZoom: function initZoom() {
				var zoomSt = mfp.st.zoom,
				    ns = '.zoom',
				    image;

				if (!zoomSt.enabled || !mfp.supportsTransition) {
					return;
				}

				var duration = zoomSt.duration,
				    getElToAnimate = function getElToAnimate(image) {
					var newImg = image.clone().removeAttr('style').removeAttr('class').addClass('mfp-animated-image'),
					    transition = 'all ' + zoomSt.duration / 1000 + 's ' + zoomSt.easing,
					    cssObj = {
						position: 'fixed',
						zIndex: 9999,
						left: 0,
						top: 0,
						'-webkit-backface-visibility': 'hidden'
					},
					    t = 'transition';

					cssObj['-webkit-' + t] = cssObj['-moz-' + t] = cssObj['-o-' + t] = cssObj[t] = transition;

					newImg.css(cssObj);
					return newImg;
				},
				    showMainContent = function showMainContent() {
					mfp.content.css('visibility', 'visible');
				},
				    openTimeout,
				    animatedImg;

				_mfpOn('BuildControls' + ns, function () {
					if (mfp._allowZoom()) {

						clearTimeout(openTimeout);
						mfp.content.css('visibility', 'hidden');

						// Basically, all code below does is clones existing image, puts in on top of the current one and animated it

						image = mfp._getItemToZoom();

						if (!image) {
							showMainContent();
							return;
						}

						animatedImg = getElToAnimate(image);

						animatedImg.css(mfp._getOffset());

						mfp.wrap.append(animatedImg);

						openTimeout = setTimeout(function () {
							animatedImg.css(mfp._getOffset(true));
							openTimeout = setTimeout(function () {

								showMainContent();

								setTimeout(function () {
									animatedImg.remove();
									image = animatedImg = null;
									_mfpTrigger('ZoomAnimationEnded');
								}, 16); // avoid blink when switching images
							}, duration); // this timeout equals animation duration
						}, 16); // by adding this timeout we avoid short glitch at the beginning of animation


						// Lots of timeouts...
					}
				});
				_mfpOn(BEFORE_CLOSE_EVENT + ns, function () {
					if (mfp._allowZoom()) {

						clearTimeout(openTimeout);

						mfp.st.removalDelay = duration;

						if (!image) {
							image = mfp._getItemToZoom();
							if (!image) {
								return;
							}
							animatedImg = getElToAnimate(image);
						}

						animatedImg.css(mfp._getOffset(true));
						mfp.wrap.append(animatedImg);
						mfp.content.css('visibility', 'hidden');

						setTimeout(function () {
							animatedImg.css(mfp._getOffset());
						}, 16);
					}
				});

				_mfpOn(CLOSE_EVENT + ns, function () {
					if (mfp._allowZoom()) {
						showMainContent();
						if (animatedImg) {
							animatedImg.remove();
						}
						image = null;
					}
				});
			},

			_allowZoom: function _allowZoom() {
				return mfp.currItem.type === 'image';
			},

			_getItemToZoom: function _getItemToZoom() {
				if (mfp.currItem.hasSize) {
					return mfp.currItem.img;
				} else {
					return false;
				}
			},

			// Get element postion relative to viewport
			_getOffset: function _getOffset(isLarge) {
				var el;
				if (isLarge) {
					el = mfp.currItem.img;
				} else {
					el = mfp.st.zoom.opener(mfp.currItem.el || mfp.currItem);
				}

				var offset = el.offset();
				var paddingTop = parseInt(el.css('padding-top'), 10);
				var paddingBottom = parseInt(el.css('padding-bottom'), 10);
				offset.top -= $(window).scrollTop() - paddingTop;

				/*
    		Animating left + top + width/height looks glitchy in Firefox, but perfect in Chrome. And vice-versa.
    		 */
				var obj = {
					width: el.width(),
					// fix Zepto height+padding issue
					height: (_isJQ ? el.innerHeight() : el[0].offsetHeight) - paddingBottom - paddingTop
				};

				// I hate to do this, but there is no another option
				if (getHasMozTransform()) {
					obj['-moz-transform'] = obj['transform'] = 'translate(' + offset.left + 'px,' + offset.top + 'px)';
				} else {
					obj.left = offset.left;
					obj.top = offset.top;
				}
				return obj;
			}

		}
	});

	/*>>zoom*/

	/*>>iframe*/

	var IFRAME_NS = 'iframe',
	    _emptyPage = '//about:blank',
	    _fixIframeBugs = function _fixIframeBugs(isShowing) {
		if (mfp.currTemplate[IFRAME_NS]) {
			var el = mfp.currTemplate[IFRAME_NS].find('iframe');
			if (el.length) {
				// reset src after the popup is closed to avoid "video keeps playing after popup is closed" bug
				if (!isShowing) {
					el[0].src = _emptyPage;
				}

				// IE8 black screen bug fix
				if (mfp.isIE8) {
					el.css('display', isShowing ? 'block' : 'none');
				}
			}
		}
	};

	$.magnificPopup.registerModule(IFRAME_NS, {

		options: {
			markup: '<div class="mfp-iframe-scaler">' + '<div class="mfp-close"></div>' + '<iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe>' + '</div>',

			srcAction: 'iframe_src',

			// we don't care and support only one default type of URL by default
			patterns: {
				youtube: {
					index: 'youtube.com',
					id: 'v=',
					src: '//www.youtube.com/embed/%id%?autoplay=1'
				},
				vimeo: {
					index: 'vimeo.com/',
					id: '/',
					src: '//player.vimeo.com/video/%id%?autoplay=1'
				},
				gmaps: {
					index: '//maps.google.',
					src: '%id%&output=embed'
				}
			}
		},

		proto: {
			initIframe: function initIframe() {
				mfp.types.push(IFRAME_NS);

				_mfpOn('BeforeChange', function (e, prevType, newType) {
					if (prevType !== newType) {
						if (prevType === IFRAME_NS) {
							_fixIframeBugs(); // iframe if removed
						} else if (newType === IFRAME_NS) {
							_fixIframeBugs(true); // iframe is showing
						}
					} // else {
					// iframe source is switched, don't do anything
					//}
				});

				_mfpOn(CLOSE_EVENT + '.' + IFRAME_NS, function () {
					_fixIframeBugs();
				});
			},

			getIframe: function getIframe(item, template) {
				var embedSrc = item.src;
				var iframeSt = mfp.st.iframe;

				$.each(iframeSt.patterns, function () {
					if (embedSrc.indexOf(this.index) > -1) {
						if (this.id) {
							if (typeof this.id === 'string') {
								embedSrc = embedSrc.substr(embedSrc.lastIndexOf(this.id) + this.id.length, embedSrc.length);
							} else {
								embedSrc = this.id.call(this, embedSrc);
							}
						}
						embedSrc = this.src.replace('%id%', embedSrc);
						return false; // break;
					}
				});

				var dataObj = {};
				if (iframeSt.srcAction) {
					dataObj[iframeSt.srcAction] = embedSrc;
				}
				mfp._parseMarkup(template, dataObj, item);

				mfp.updateStatus('ready');

				return template;
			}
		}
	});

	/*>>iframe*/

	/*>>gallery*/
	/**
  * Get looped index depending on number of slides
  */
	var _getLoopedId = function _getLoopedId(index) {
		var numSlides = mfp.items.length;
		if (index > numSlides - 1) {
			return index - numSlides;
		} else if (index < 0) {
			return numSlides + index;
		}
		return index;
	},
	    _replaceCurrTotal = function _replaceCurrTotal(text, curr, total) {
		return text.replace(/%curr%/gi, curr + 1).replace(/%total%/gi, total);
	};

	$.magnificPopup.registerModule('gallery', {

		options: {
			enabled: false,
			arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
			preload: [0, 2],
			navigateByImgClick: true,
			arrows: true,

			tPrev: 'Previous (Left arrow key)',
			tNext: 'Next (Right arrow key)',
			tCounter: '%curr% of %total%'
		},

		proto: {
			initGallery: function initGallery() {

				var gSt = mfp.st.gallery,
				    ns = '.mfp-gallery';

				mfp.direction = true; // true - next, false - prev

				if (!gSt || !gSt.enabled) return false;

				_wrapClasses += ' mfp-gallery';

				_mfpOn(OPEN_EVENT + ns, function () {

					if (gSt.navigateByImgClick) {
						mfp.wrap.on('click' + ns, '.mfp-img', function () {
							if (mfp.items.length > 1) {
								mfp.next();
								return false;
							}
						});
					}

					_document.on('keydown' + ns, function (e) {
						if (e.keyCode === 37) {
							mfp.prev();
						} else if (e.keyCode === 39) {
							mfp.next();
						}
					});
				});

				_mfpOn('UpdateStatus' + ns, function (e, data) {
					if (data.text) {
						data.text = _replaceCurrTotal(data.text, mfp.currItem.index, mfp.items.length);
					}
				});

				_mfpOn(MARKUP_PARSE_EVENT + ns, function (e, element, values, item) {
					var l = mfp.items.length;
					values.counter = l > 1 ? _replaceCurrTotal(gSt.tCounter, item.index, l) : '';
				});

				_mfpOn('BuildControls' + ns, function () {
					if (mfp.items.length > 1 && gSt.arrows && !mfp.arrowLeft) {
						var markup = gSt.arrowMarkup,
						    arrowLeft = mfp.arrowLeft = $(markup.replace(/%title%/gi, gSt.tPrev).replace(/%dir%/gi, 'left')).addClass(PREVENT_CLOSE_CLASS),
						    arrowRight = mfp.arrowRight = $(markup.replace(/%title%/gi, gSt.tNext).replace(/%dir%/gi, 'right')).addClass(PREVENT_CLOSE_CLASS);

						arrowLeft.click(function () {
							mfp.prev();
						});
						arrowRight.click(function () {
							mfp.next();
						});

						mfp.container.append(arrowLeft.add(arrowRight));
					}
				});

				_mfpOn(CHANGE_EVENT + ns, function () {
					if (mfp._preloadTimeout) clearTimeout(mfp._preloadTimeout);

					mfp._preloadTimeout = setTimeout(function () {
						mfp.preloadNearbyImages();
						mfp._preloadTimeout = null;
					}, 16);
				});

				_mfpOn(CLOSE_EVENT + ns, function () {
					_document.off(ns);
					mfp.wrap.off('click' + ns);
					mfp.arrowRight = mfp.arrowLeft = null;
				});
			},
			next: function next() {
				mfp.direction = true;
				mfp.index = _getLoopedId(mfp.index + 1);
				mfp.updateItemHTML();
			},
			prev: function prev() {
				mfp.direction = false;
				mfp.index = _getLoopedId(mfp.index - 1);
				mfp.updateItemHTML();
			},
			goTo: function goTo(newIndex) {
				mfp.direction = newIndex >= mfp.index;
				mfp.index = newIndex;
				mfp.updateItemHTML();
			},
			preloadNearbyImages: function preloadNearbyImages() {
				var p = mfp.st.gallery.preload,
				    preloadBefore = Math.min(p[0], mfp.items.length),
				    preloadAfter = Math.min(p[1], mfp.items.length),
				    i;

				for (i = 1; i <= (mfp.direction ? preloadAfter : preloadBefore); i++) {
					mfp._preloadItem(mfp.index + i);
				}
				for (i = 1; i <= (mfp.direction ? preloadBefore : preloadAfter); i++) {
					mfp._preloadItem(mfp.index - i);
				}
			},
			_preloadItem: function _preloadItem(index) {
				index = _getLoopedId(index);

				if (mfp.items[index].preloaded) {
					return;
				}

				var item = mfp.items[index];
				if (!item.parsed) {
					item = mfp.parseEl(index);
				}

				_mfpTrigger('LazyLoad', item);

				if (item.type === 'image') {
					item.img = $('<img class="mfp-img" />').on('load.mfploader', function () {
						item.hasSize = true;
					}).on('error.mfploader', function () {
						item.hasSize = true;
						item.loadError = true;
						_mfpTrigger('LazyLoadError', item);
					}).attr('src', item.src);
				}

				item.preloaded = true;
			}
		}
	});

	/*>>gallery*/

	/*>>retina*/

	var RETINA_NS = 'retina';

	$.magnificPopup.registerModule(RETINA_NS, {
		options: {
			replaceSrc: function replaceSrc(item) {
				return item.src.replace(/\.\w+$/, function (m) {
					return '@2x' + m;
				});
			},
			ratio: 1 // Function or number.  Set to 1 to disable.
		},
		proto: {
			initRetina: function initRetina() {
				if (window.devicePixelRatio > 1) {

					var st = mfp.st.retina,
					    ratio = st.ratio;

					ratio = !isNaN(ratio) ? ratio : ratio();

					if (ratio > 1) {
						_mfpOn('ImageHasSize' + '.' + RETINA_NS, function (e, item) {
							item.img.css({
								'max-width': item.img[0].naturalWidth / ratio,
								'width': '100%'
							});
						});
						_mfpOn('ElementParse' + '.' + RETINA_NS, function (e, item) {
							item.src = st.replaceSrc(item, ratio);
						});
					}
				}
			}
		}
	});

	/*>>retina*/
	_checkInstance();
});
!function (e, t) {
	"object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : e.Sweetalert2 = t();
}(this, function () {
	"use strict";
	function q(e) {
		return (q = "function" == typeof Symbol && "symbol" == _typeof(Symbol.iterator) ? function (e) {
			return typeof e === "undefined" ? "undefined" : _typeof(e);
		} : function (e) {
			return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e === "undefined" ? "undefined" : _typeof(e);
		})(e);
	}function a(e, t) {
		if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
	}function o(e, t) {
		for (var n = 0; n < t.length; n++) {
			var o = t[n];o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, o.key, o);
		}
	}function i(e, t, n) {
		return t && o(e.prototype, t), n && o(e, n), e;
	}function r() {
		return (r = Object.assign || function (e) {
			for (var t = 1; t < arguments.length; t++) {
				var n = arguments[t];for (var o in n) {
					Object.prototype.hasOwnProperty.call(n, o) && (e[o] = n[o]);
				}
			}return e;
		}).apply(this, arguments);
	}function s(e, t) {
		if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");e.prototype = Object.create(t && t.prototype, { constructor: { value: e, writable: !0, configurable: !0 } }), t && u(e, t);
	}function c(e) {
		return (c = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
			return e.__proto__ || Object.getPrototypeOf(e);
		})(e);
	}function u(e, t) {
		return (u = Object.setPrototypeOf || function (e, t) {
			return e.__proto__ = t, e;
		})(e, t);
	}function l(e, t, n) {
		return (l = function () {
			if ("undefined" == typeof Reflect || !Reflect.construct) return !1;if (Reflect.construct.sham) return !1;if ("function" == typeof Proxy) return !0;try {
				return Date.prototype.toString.call(Reflect.construct(Date, [], function () {})), !0;
			} catch (e) {
				return !1;
			}
		}() ? Reflect.construct : function (e, t, n) {
			var o = [null];o.push.apply(o, t);var i = new (Function.bind.apply(e, o))();return n && u(i, n.prototype), i;
		}).apply(null, arguments);
	}function d(e, t) {
		return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? function (e) {
			if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return e;
		}(e) : t;
	}function p(e, t, n) {
		return (p = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, n) {
			var o = function (e, t) {
				for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = c(e));) {}return e;
			}(e, t);if (o) {
				var i = Object.getOwnPropertyDescriptor(o, t);return i.get ? i.get.call(n) : i.value;
			}
		})(e, t, n || e);
	}var t = "SweetAlert2:",
	    f = function f(e) {
		return Array.prototype.slice.call(e);
	},
	    R = function R(e) {
		console.warn("".concat(t, " ").concat(e));
	},
	    I = function I(e) {
		console.error("".concat(t, " ").concat(e));
	},
	    n = [],
	    m = function m(e) {
		-1 === n.indexOf(e) && (n.push(e), R(e));
	},
	    H = function H(e) {
		return "function" == typeof e ? e() : e;
	},
	    D = function D(e) {
		return e && Promise.resolve(e) === e;
	},
	    e = Object.freeze({ cancel: "cancel", backdrop: "overlay", close: "close", esc: "esc", timer: "timer" }),
	    h = function h(e) {
		var t = {};for (var n in e) {
			t[e[n]] = "swal2-" + e[n];
		}return t;
	},
	    _ = h(["container", "shown", "height-auto", "iosfix", "popup", "modal", "no-backdrop", "toast", "toast-shown", "toast-column", "fade", "show", "hide", "noanimation", "close", "title", "header", "content", "actions", "confirm", "cancel", "footer", "icon", "icon-text", "image", "input", "file", "range", "select", "radio", "checkbox", "label", "textarea", "inputerror", "validation-message", "progresssteps", "activeprogressstep", "progresscircle", "progressline", "loading", "styled", "top", "top-start", "top-end", "top-left", "top-right", "center", "center-start", "center-end", "center-left", "center-right", "bottom", "bottom-start", "bottom-end", "bottom-left", "bottom-right", "grow-row", "grow-column", "grow-fullscreen", "rtl"]),
	    g = h(["success", "warning", "info", "question", "error"]),
	    b = { previousBodyPadding: null },
	    v = function v(e, t) {
		return e.classList.contains(t);
	},
	    N = function N(e) {
		if (e.focus(), "file" !== e.type) {
			var t = e.value;e.value = "", e.value = t;
		}
	},
	    y = function y(e, t, n) {
		e && t && ("string" == typeof t && (t = t.split(/\s+/).filter(Boolean)), t.forEach(function (t) {
			e.forEach ? e.forEach(function (e) {
				n ? e.classList.add(t) : e.classList.remove(t);
			}) : n ? e.classList.add(t) : e.classList.remove(t);
		}));
	},
	    z = function z(e, t) {
		y(e, t, !0);
	},
	    W = function W(e, t) {
		y(e, t, !1);
	},
	    U = function U(e, t) {
		for (var n = 0; n < e.childNodes.length; n++) {
			if (v(e.childNodes[n], t)) return e.childNodes[n];
		}
	},
	    K = function K(e) {
		e.style.opacity = "", e.style.display = e.id === _.content ? "block" : "flex";
	},
	    F = function F(e) {
		e.style.opacity = "", e.style.display = "none";
	},
	    Z = function Z(e) {
		return e && (e.offsetWidth || e.offsetHeight || e.getClientRects().length);
	},
	    w = function w() {
		return document.body.querySelector("." + _.container);
	},
	    C = function C(e) {
		var t = w();return t ? t.querySelector("." + e) : null;
	},
	    k = function k() {
		return C(_.popup);
	},
	    x = function x() {
		var e = k();return f(e.querySelectorAll("." + _.icon));
	},
	    A = function A() {
		return C(_.title);
	},
	    B = function B() {
		return C(_.content);
	},
	    S = function S() {
		return C(_.image);
	},
	    P = function P() {
		return C(_.progresssteps);
	},
	    E = function E() {
		return C(_["validation-message"]);
	},
	    L = function L() {
		return C(_.confirm);
	},
	    O = function O() {
		return C(_.cancel);
	},
	    Q = function Q() {
		return C(_.actions);
	},
	    Y = function Y() {
		return C(_.footer);
	},
	    $ = function $() {
		return C(_.close);
	},
	    J = function J() {
		var e = f(k().querySelectorAll('[tabindex]:not([tabindex="-1"]):not([tabindex="0"])')).sort(function (e, t) {
			return e = parseInt(e.getAttribute("tabindex")), (t = parseInt(t.getAttribute("tabindex"))) < e ? 1 : e < t ? -1 : 0;
		}),
		    t = f(k().querySelectorAll('a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable], audio[controls], video[controls]')).filter(function (e) {
			return "-1" !== e.getAttribute("tabindex");
		});return function (e) {
			for (var t = [], n = 0; n < e.length; n++) {
				-1 === t.indexOf(e[n]) && t.push(e[n]);
			}return t;
		}(e.concat(t)).filter(function (e) {
			return Z(e);
		});
	},
	    T = function T() {
		return !M() && !document.body.classList.contains(_["no-backdrop"]);
	},
	    M = function M() {
		return document.body.classList.contains(_["toast-shown"]);
	},
	    j = function j() {
		return "undefined" == typeof window || "undefined" == typeof document;
	},
	    V = '\n <div aria-labelledby="'.concat(_.title, '" aria-describedby="').concat(_.content, '" class="').concat(_.popup, '" tabindex="-1">\n   <div class="').concat(_.header, '">\n     <ul class="').concat(_.progresssteps, '"></ul>\n     <div class="').concat(_.icon, " ").concat(g.error, '">\n       <span class="swal2-x-mark"><span class="swal2-x-mark-line-left"></span><span class="swal2-x-mark-line-right"></span></span>\n     </div>\n     <div class="').concat(_.icon, " ").concat(g.question, '">\n       <span class="').concat(_["icon-text"], '">?</span>\n      </div>\n     <div class="').concat(_.icon, " ").concat(g.warning, '">\n       <span class="').concat(_["icon-text"], '">!</span>\n      </div>\n     <div class="').concat(_.icon, " ").concat(g.info, '">\n       <span class="').concat(_["icon-text"], '">i</span>\n      </div>\n     <div class="').concat(_.icon, " ").concat(g.success, '">\n       <div class="swal2-success-circular-line-left"></div>\n       <span class="swal2-success-line-tip"></span> <span class="swal2-success-line-long"></span>\n       <div class="swal2-success-ring"></div> <div class="swal2-success-fix"></div>\n       <div class="swal2-success-circular-line-right"></div>\n     </div>\n     <img class="').concat(_.image, '" />\n     <h2 class="').concat(_.title, '" id="').concat(_.title, '"></h2>\n     <button type="button" class="').concat(_.close, '">×</button>\n   </div>\n   <div class="').concat(_.content, '">\n     <div id="').concat(_.content, '"></div>\n     <input class="').concat(_.input, '" />\n     <input type="file" class="').concat(_.file, '" />\n     <div class="').concat(_.range, '">\n       <input type="range" />\n       <output></output>\n     </div>\n     <select class="').concat(_.select, '"></select>\n     <div class="').concat(_.radio, '"></div>\n     <label for="').concat(_.checkbox, '" class="').concat(_.checkbox, '">\n       <input type="checkbox" />\n       <span class="').concat(_.label, '"></span>\n     </label>\n     <textarea class="').concat(_.textarea, '"></textarea>\n     <div class="').concat(_["validation-message"], '" id="').concat(_["validation-message"], '"></div>\n   </div>\n   <div class="').concat(_.actions, '">\n     <button type="button" class="').concat(_.confirm, '">OK</button>\n     <button type="button" class="').concat(_.cancel, '">Cancel</button>\n   </div>\n   <div class="').concat(_.footer, '">\n   </div>\n </div>\n').replace(/(^|\n)\s*/g, ""),
	    X = function X(e) {
		var t = w();if (t && (t.parentNode.removeChild(t), W([document.documentElement, document.body], [_["no-backdrop"], _["toast-shown"], _["has-column"]])), !j()) {
			var n = document.createElement("div");n.className = _.container, n.innerHTML = V;var o = "string" == typeof e.target ? document.querySelector(e.target) : e.target;o.appendChild(n);var i,
			    r = k(),
			    a = B(),
			    s = U(a, _.input),
			    c = U(a, _.file),
			    u = a.querySelector(".".concat(_.range, " input")),
			    l = a.querySelector(".".concat(_.range, " output")),
			    d = U(a, _.select),
			    p = a.querySelector(".".concat(_.checkbox, " input")),
			    f = U(a, _.textarea);r.setAttribute("role", e.toast ? "alert" : "dialog"), r.setAttribute("aria-live", e.toast ? "polite" : "assertive"), e.toast || r.setAttribute("aria-modal", "true"), "rtl" === window.getComputedStyle(o).direction && z(w(), _.rtl);var m = function m(e) {
				De.isVisible() && i !== e.target.value && De.resetValidationMessage(), i = e.target.value;
			};return s.oninput = m, c.onchange = m, d.onchange = m, p.onchange = m, f.oninput = m, u.oninput = function (e) {
				m(e), l.value = u.value;
			}, u.onchange = function (e) {
				m(e), u.nextSibling.value = u.value;
			}, r;
		}I("SweetAlert2 requires document to initialize");
	},
	    G = function G(e, t) {
		if (!e) return F(t);if (e instanceof HTMLElement) t.appendChild(e);else if ("object" === q(e)) {
			if (t.innerHTML = "", 0 in e) for (var n = 0; n in e; n++) {
				t.appendChild(e[n].cloneNode(!0));
			} else t.appendChild(e.cloneNode(!0));
		} else e && (t.innerHTML = e);K(t);
	},
	    ee = function () {
		if (j()) return !1;var e = document.createElement("div"),
		    t = { WebkitAnimation: "webkitAnimationEnd", OAnimation: "oAnimationEnd oanimationend", animation: "animationend" };for (var n in t) {
			if (t.hasOwnProperty(n) && void 0 !== e.style[n]) return t[n];
		}return !1;
	}(),
	    te = function te(e) {
		var t = Q(),
		    n = L(),
		    o = O();if (e.showConfirmButton || e.showCancelButton ? K(t) : F(t), e.showCancelButton ? o.style.display = "inline-block" : F(o), e.showConfirmButton ? n.style.removeProperty("display") : F(n), n.innerHTML = e.confirmButtonText, o.innerHTML = e.cancelButtonText, n.setAttribute("aria-label", e.confirmButtonAriaLabel), o.setAttribute("aria-label", e.cancelButtonAriaLabel), n.className = _.confirm, z(n, e.confirmButtonClass), o.className = _.cancel, z(o, e.cancelButtonClass), e.buttonsStyling) {
			z([n, o], _.styled), e.confirmButtonColor && (n.style.backgroundColor = e.confirmButtonColor), e.cancelButtonColor && (o.style.backgroundColor = e.cancelButtonColor);var i = window.getComputedStyle(n).getPropertyValue("background-color");n.style.borderLeftColor = i, n.style.borderRightColor = i;
		} else W([n, o], _.styled), n.style.backgroundColor = n.style.borderLeftColor = n.style.borderRightColor = "", o.style.backgroundColor = o.style.borderLeftColor = o.style.borderRightColor = "";
	},
	    ne = function ne(e) {
		var t = B().querySelector("#" + _.content);e.html ? G(e.html, t) : e.text ? (t.textContent = e.text, K(t)) : F(t);
	},
	    oe = function oe(e) {
		for (var t = x(), n = 0; n < t.length; n++) {
			F(t[n]);
		}if (e.type) if (-1 !== Object.keys(g).indexOf(e.type)) {
			var o = De.getPopup().querySelector(".".concat(_.icon, ".").concat(g[e.type]));K(o), e.animation && z(o, "swal2-animate-".concat(e.type, "-icon"));
		} else I('Unknown type! Expected "success", "error", "warning", "info" or "question", got "'.concat(e.type, '"'));
	},
	    ie = function ie(e) {
		var t = S();e.imageUrl ? (t.setAttribute("src", e.imageUrl), t.setAttribute("alt", e.imageAlt), K(t), e.imageWidth ? t.setAttribute("width", e.imageWidth) : t.removeAttribute("width"), e.imageHeight ? t.setAttribute("height", e.imageHeight) : t.removeAttribute("height"), t.className = _.image, e.imageClass && z(t, e.imageClass)) : F(t);
	},
	    re = function re(i) {
		var r = P(),
		    a = parseInt(null === i.currentProgressStep ? De.getQueueStep() : i.currentProgressStep, 10);i.progressSteps && i.progressSteps.length ? (K(r), r.innerHTML = "", a >= i.progressSteps.length && R("Invalid currentProgressStep parameter, it should be less than progressSteps.length (currentProgressStep like JS arrays starts from 0)"), i.progressSteps.forEach(function (e, t) {
			var n = document.createElement("li");if (z(n, _.progresscircle), n.innerHTML = e, t === a && z(n, _.activeprogressstep), r.appendChild(n), t !== i.progressSteps.length - 1) {
				var o = document.createElement("li");z(o, _.progressline), i.progressStepsDistance && (o.style.width = i.progressStepsDistance), r.appendChild(o);
			}
		})) : F(r);
	},
	    ae = function ae(e) {
		var t = A();e.titleText ? t.innerText = e.titleText : e.title && ("string" == typeof e.title && (e.title = e.title.split("\n").join("<br />")), G(e.title, t));
	},
	    se = function se() {
		null === b.previousBodyPadding && document.body.scrollHeight > window.innerHeight && (b.previousBodyPadding = parseInt(window.getComputedStyle(document.body).getPropertyValue("padding-right")), document.body.style.paddingRight = b.previousBodyPadding + function () {
			if ("ontouchstart" in window || navigator.msMaxTouchPoints) return 0;var e = document.createElement("div");e.style.width = "50px", e.style.height = "50px", e.style.overflow = "scroll", document.body.appendChild(e);var t = e.offsetWidth - e.clientWidth;return document.body.removeChild(e), t;
		}() + "px");
	},
	    ce = function ce() {
		return !!window.MSInputMethodContext && !!document.documentMode;
	},
	    ue = function ue() {
		var e = w(),
		    t = k();e.style.removeProperty("align-items"), t.offsetTop < 0 && (e.style.alignItems = "flex-start");
	},
	    le = {},
	    de = function de(e, t) {
		var n = w(),
		    o = k();if (o) {
			null !== e && "function" == typeof e && e(o), W(o, _.show), z(o, _.hide);var i = function i() {
				M() ? pe(t) : (new Promise(function (e) {
					var t = window.scrollX,
					    n = window.scrollY;le.restoreFocusTimeout = setTimeout(function () {
						le.previousActiveElement && le.previousActiveElement.focus ? (le.previousActiveElement.focus(), le.previousActiveElement = null) : document.body && document.body.focus(), e();
					}, 100), void 0 !== t && void 0 !== n && window.scrollTo(t, n);
				}).then(function () {
					return pe(t);
				}), le.keydownTarget.removeEventListener("keydown", le.keydownHandler, { capture: le.keydownListenerCapture }), le.keydownHandlerAdded = !1), n.parentNode && n.parentNode.removeChild(n), W([document.documentElement, document.body], [_.shown, _["height-auto"], _["no-backdrop"], _["toast-shown"], _["toast-column"]]), T() && (null !== b.previousBodyPadding && (document.body.style.paddingRight = b.previousBodyPadding, b.previousBodyPadding = null), function () {
					if (v(document.body, _.iosfix)) {
						var e = parseInt(document.body.style.top, 10);W(document.body, _.iosfix), document.body.style.top = "", document.body.scrollTop = -1 * e;
					}
				}(), "undefined" != typeof window && ce() && window.removeEventListener("resize", ue), f(document.body.children).forEach(function (e) {
					e.hasAttribute("data-previous-aria-hidden") ? (e.setAttribute("aria-hidden", e.getAttribute("data-previous-aria-hidden")), e.removeAttribute("data-previous-aria-hidden")) : e.removeAttribute("aria-hidden");
				}));
			};ee && !v(o, _.noanimation) ? o.addEventListener(ee, function e() {
				o.removeEventListener(ee, e), v(o, _.hide) && i();
			}) : i();
		}
	},
	    pe = function pe(e) {
		null !== e && "function" == typeof e && setTimeout(function () {
			e();
		});
	};function fe(e) {
		var t = function e() {
			for (var t = arguments.length, n = new Array(t), o = 0; o < t; o++) {
				n[o] = arguments[o];
			}if (!(this instanceof e)) return l(e, n);Object.getPrototypeOf(e).apply(this, n);
		};return t.prototype = r(Object.create(e.prototype), { constructor: t }), "function" == typeof Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e, t;
	}var me = { title: "", titleText: "", text: "", html: "", footer: "", type: null, toast: !1, customClass: "", customContainerClass: "", target: "body", backdrop: !0, animation: !0, heightAuto: !0, allowOutsideClick: !0, allowEscapeKey: !0, allowEnterKey: !0, stopKeydownPropagation: !0, keydownListenerCapture: !1, showConfirmButton: !0, showCancelButton: !1, preConfirm: null, confirmButtonText: "OK", confirmButtonAriaLabel: "", confirmButtonColor: null, confirmButtonClass: null, cancelButtonText: "Cancel", cancelButtonAriaLabel: "", cancelButtonColor: null, cancelButtonClass: null, buttonsStyling: !0, reverseButtons: !1, focusConfirm: !0, focusCancel: !1, showCloseButton: !1, closeButtonAriaLabel: "Close this dialog", showLoaderOnConfirm: !1, imageUrl: null, imageWidth: null, imageHeight: null, imageAlt: "", imageClass: null, timer: null, width: null, padding: null, background: null, input: null, inputPlaceholder: "", inputValue: "", inputOptions: {}, inputAutoTrim: !0, inputClass: null, inputAttributes: {}, inputValidator: null, validationMessage: null, grow: !1, position: "center", progressSteps: [], currentProgressStep: null, progressStepsDistance: null, onBeforeOpen: null, onAfterClose: null, onOpen: null, onClose: null, useRejections: !1, expectRejections: !1 },
	    he = ["useRejections", "expectRejections", "extraParams"],
	    ge = ["allowOutsideClick", "allowEnterKey", "backdrop", "focusConfirm", "focusCancel", "heightAuto", "keydownListenerCapture"],
	    be = function be(e) {
		return me.hasOwnProperty(e) || "extraParams" === e;
	},
	    ve = function ve(e) {
		return -1 !== he.indexOf(e);
	},
	    ye = function ye(e) {
		for (var t in e) {
			be(t) || R('Unknown parameter "'.concat(t, '"')), e.toast && -1 !== ge.indexOf(t) && R('The parameter "'.concat(t, '" is incompatible with toasts')), ve(t) && m('The parameter "'.concat(t, '" is deprecated and will be removed in the next major release.'));
		}
	},
	    we = '"setDefaults" & "resetDefaults" methods are deprecated in favor of "mixin" method and will be removed in the next major release. For new projects, use "mixin". For past projects already using "setDefaults", support will be provided through an additional package.',
	    Ce = {};var ke = [],
	    xe = function xe() {
		var e = k();e || De(""), e = k();var t = Q(),
		    n = L(),
		    o = O();K(t), K(n), z([e, t], _.loading), n.disabled = !0, o.disabled = !0, e.setAttribute("data-loading", !0), e.setAttribute("aria-busy", !0), e.focus();
	},
	    Ae = Object.freeze({ isValidParameter: be, isDeprecatedParameter: ve, argsToParams: function argsToParams(n) {
			var o = {};switch (q(n[0])) {case "object":
					r(o, n[0]);break;default:
					["title", "html", "type"].forEach(function (e, t) {
						switch (q(n[t])) {case "string":
								o[e] = n[t];break;case "undefined":
								break;default:
								I("Unexpected type of ".concat(e, '! Expected "string", got ').concat(q(n[t])));}
					});}return o;
		}, adaptInputValidator: function adaptInputValidator(n) {
			return function (e, t) {
				return n.call(this, e, t).then(function () {}, function (e) {
					return e;
				});
			};
		}, close: de, closePopup: de, closeModal: de, closeToast: de, isVisible: function isVisible() {
			return !!k();
		}, clickConfirm: function clickConfirm() {
			return L().click();
		}, clickCancel: function clickCancel() {
			return O().click();
		}, getContainer: w, getPopup: k, getTitle: A, getContent: B, getImage: S, getIcons: x, getCloseButton: $, getButtonsWrapper: function getButtonsWrapper() {
			return m("swal.getButtonsWrapper() is deprecated and will be removed in the next major release, use swal.getActions() instead"), C(_.actions);
		}, getActions: Q, getConfirmButton: L, getCancelButton: O, getFooter: Y, getFocusableElements: J, getValidationMessage: E, isLoading: function isLoading() {
			return k().hasAttribute("data-loading");
		}, fire: function fire() {
			for (var e = arguments.length, t = new Array(e), n = 0; n < e; n++) {
				t[n] = arguments[n];
			}return l(this, t);
		}, mixin: function mixin(n) {
			return fe(function (e) {
				function t() {
					return a(this, t), d(this, c(t).apply(this, arguments));
				}return s(t, e), i(t, [{ key: "_main", value: function value(e) {
						return p(c(t.prototype), "_main", this).call(this, r({}, n, e));
					} }]), t;
			}(this));
		}, queue: function queue(e) {
			var r = this;ke = e;var a = function a() {
				ke = [], document.body.removeAttribute("data-swal2-queue-step");
			},
			    s = [];return new Promise(function (i) {
				!function t(n, o) {
					n < ke.length ? (document.body.setAttribute("data-swal2-queue-step", n), r(ke[n]).then(function (e) {
						void 0 !== e.value ? (s.push(e.value), t(n + 1, o)) : (a(), i({ dismiss: e.dismiss }));
					})) : (a(), i({ value: s }));
				}(0);
			});
		}, getQueueStep: function getQueueStep() {
			return document.body.getAttribute("data-swal2-queue-step");
		}, insertQueueStep: function insertQueueStep(e, t) {
			return t && t < ke.length ? ke.splice(t, 0, e) : ke.push(e);
		}, deleteQueueStep: function deleteQueueStep(e) {
			void 0 !== ke[e] && ke.splice(e, 1);
		}, showLoading: xe, enableLoading: xe, getTimerLeft: function getTimerLeft() {
			return le.timeout && le.timeout.getTimerLeft();
		}, stopTimer: function stopTimer() {
			return le.timeout && le.timeout.stop();
		}, resumeTimer: function resumeTimer() {
			return le.timeout && le.timeout.start();
		}, toggleTimer: function toggleTimer() {
			var e = le.timeout;return e && (e.running ? e.stop() : e.start());
		}, increaseTimer: function increaseTimer(e) {
			return le.timeout && le.timeout.increase(e);
		}, isTimerRunning: function isTimerRunning() {
			return le.timeout && le.timeout.isRunning();
		} }),
	    Be = "function" == typeof Symbol ? Symbol : function () {
		var t = 0;function e(e) {
			return "__" + e + "_" + Math.floor(1e9 * Math.random()) + "_" + ++t + "__";
		}return e.iterator = e("Symbol.iterator"), e;
	}(),
	    Se = "function" == typeof WeakMap ? WeakMap : function (n, o, t) {
		function e() {
			o(this, n, { value: Be("WeakMap") });
		}return e.prototype = { delete: function _delete(e) {
				delete e[this[n]];
			}, get: function get(e) {
				return e[this[n]];
			}, has: function has(e) {
				return t.call(e, this[n]);
			}, set: function set(e, t) {
				o(e, this[n], { configurable: !0, value: t });
			} }, e;
	}(Be("WeakMap"), Object.defineProperty, {}.hasOwnProperty),
	    Pe = { promise: new Se(), innerParams: new Se(), domCache: new Se() };function Ee() {
		var e = Pe.innerParams.get(this),
		    t = Pe.domCache.get(this);e.showConfirmButton || (F(t.confirmButton), e.showCancelButton || F(t.actions)), W([t.popup, t.actions], _.loading), t.popup.removeAttribute("aria-busy"), t.popup.removeAttribute("data-loading"), t.confirmButton.disabled = !1, t.cancelButton.disabled = !1;
	}function Le(e) {
		var t = Pe.domCache.get(this);t.validationMessage.innerHTML = e;var n = window.getComputedStyle(t.popup);t.validationMessage.style.marginLeft = "-".concat(n.getPropertyValue("padding-left")), t.validationMessage.style.marginRight = "-".concat(n.getPropertyValue("padding-right")), K(t.validationMessage);var o = this.getInput();o && (o.setAttribute("aria-invalid", !0), o.setAttribute("aria-describedBy", _["validation-message"]), N(o), z(o, _.inputerror));
	}function Oe() {
		var e = Pe.domCache.get(this);e.validationMessage && F(e.validationMessage);var t = this.getInput();t && (t.removeAttribute("aria-invalid"), t.removeAttribute("aria-describedBy"), W(t, _.inputerror));
	}var Te = function e(t, n) {
		a(this, e);var o,
		    i,
		    r = n;this.running = !1, this.start = function () {
			return this.running || (this.running = !0, i = new Date(), o = setTimeout(t, r)), r;
		}, this.stop = function () {
			return this.running && (this.running = !1, clearTimeout(o), r -= new Date() - i), r;
		}, this.increase = function (e) {
			var t = this.running;return t && this.stop(), r += e, t && this.start(), r;
		}, this.getTimerLeft = function () {
			return this.running && (this.stop(), this.start()), r;
		}, this.isRunning = function () {
			return this.running;
		}, this.start();
	},
	    Me = { email: function email(e, t) {
			return (/^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9-]{2,24}$/.test(e) ? Promise.resolve() : Promise.reject(t && t.validationMessage ? t.validationMessage : "Invalid email address")
			);
		}, url: function url(e, t) {
			return (/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,63}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)$/.test(e) ? Promise.resolve() : Promise.reject(t && t.validationMessage ? t.validationMessage : "Invalid URL")
			);
		} };var je = function je(e) {
		var t = w(),
		    n = k();null !== e.onBeforeOpen && "function" == typeof e.onBeforeOpen && e.onBeforeOpen(n), e.animation ? (z(n, _.show), z(t, _.fade), W(n, _.hide)) : W(n, _.fade), K(n), t.style.overflowY = "hidden", ee && !v(n, _.noanimation) ? n.addEventListener(ee, function e() {
			n.removeEventListener(ee, e), t.style.overflowY = "auto";
		}) : t.style.overflowY = "auto", z([document.documentElement, document.body, t], _.shown), e.heightAuto && e.backdrop && !e.toast && z([document.documentElement, document.body], _["height-auto"]), T() && (se(), function () {
			if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream && !v(document.body, _.iosfix)) {
				var e = document.body.scrollTop;document.body.style.top = -1 * e + "px", z(document.body, _.iosfix);
			}
		}(), "undefined" != typeof window && ce() && (ue(), window.addEventListener("resize", ue)), f(document.body.children).forEach(function (e) {
			e === w() || function (e, t) {
				if ("function" == typeof e.contains) return e.contains(t);
			}(e, w()) || (e.hasAttribute("aria-hidden") && e.setAttribute("data-previous-aria-hidden", e.getAttribute("aria-hidden")), e.setAttribute("aria-hidden", "true"));
		}), setTimeout(function () {
			t.scrollTop = 0;
		})), M() || le.previousActiveElement || (le.previousActiveElement = document.activeElement), null !== e.onOpen && "function" == typeof e.onOpen && setTimeout(function () {
			e.onOpen(n);
		});
	};var Ve,
	    qe = Object.freeze({ hideLoading: Ee, disableLoading: Ee, getInput: function getInput(e) {
			var t = Pe.innerParams.get(this),
			    n = Pe.domCache.get(this);if (!(e = e || t.input)) return null;switch (e) {case "select":case "textarea":case "file":
					return U(n.content, _[e]);case "checkbox":
					return n.popup.querySelector(".".concat(_.checkbox, " input"));case "radio":
					return n.popup.querySelector(".".concat(_.radio, " input:checked")) || n.popup.querySelector(".".concat(_.radio, " input:first-child"));case "range":
					return n.popup.querySelector(".".concat(_.range, " input"));default:
					return U(n.content, _.input);}
		}, enableButtons: function enableButtons() {
			var e = Pe.domCache.get(this);e.confirmButton.disabled = !1, e.cancelButton.disabled = !1;
		}, disableButtons: function disableButtons() {
			var e = Pe.domCache.get(this);e.confirmButton.disabled = !0, e.cancelButton.disabled = !0;
		}, enableConfirmButton: function enableConfirmButton() {
			Pe.domCache.get(this).confirmButton.disabled = !1;
		}, disableConfirmButton: function disableConfirmButton() {
			Pe.domCache.get(this).confirmButton.disabled = !0;
		}, enableInput: function enableInput() {
			var e = this.getInput();if (!e) return !1;if ("radio" === e.type) for (var t = e.parentNode.parentNode.querySelectorAll("input"), n = 0; n < t.length; n++) {
				t[n].disabled = !1;
			} else e.disabled = !1;
		}, disableInput: function disableInput() {
			var e = this.getInput();if (!e) return !1;if (e && "radio" === e.type) for (var t = e.parentNode.parentNode.querySelectorAll("input"), n = 0; n < t.length; n++) {
				t[n].disabled = !0;
			} else e.disabled = !0;
		}, showValidationMessage: Le, resetValidationMessage: Oe, resetValidationError: function resetValidationError() {
			m("Swal.resetValidationError() is deprecated and will be removed in the next major release, use Swal.resetValidationMessage() instead"), Oe.bind(this)();
		}, showValidationError: function showValidationError(e) {
			m("Swal.showValidationError() is deprecated and will be removed in the next major release, use Swal.showValidationMessage() instead"), Le.bind(this)(e);
		}, getProgressSteps: function getProgressSteps() {
			return Pe.innerParams.get(this).progressSteps;
		}, setProgressSteps: function setProgressSteps(e) {
			var t = r({}, Pe.innerParams.get(this), { progressSteps: e });Pe.innerParams.set(this, t), re(t);
		}, showProgressSteps: function showProgressSteps() {
			var e = Pe.domCache.get(this);K(e.progressSteps);
		}, hideProgressSteps: function hideProgressSteps() {
			var e = Pe.domCache.get(this);F(e.progressSteps);
		}, _main: function _main(e) {
			var T = this;ye(e);var M = r({}, me, e);!function (t) {
				var e;t.inputValidator || Object.keys(Me).forEach(function (e) {
					t.input === e && (t.inputValidator = t.expectRejections ? Me[e] : De.adaptInputValidator(Me[e]));
				}), t.validationMessage && ("object" !== q(t.extraParams) && (t.extraParams = {}), t.extraParams.validationMessage = t.validationMessage), (!t.target || "string" == typeof t.target && !document.querySelector(t.target) || "string" != typeof t.target && !t.target.appendChild) && (R('Target parameter is not valid, defaulting to "body"'), t.target = "body"), "function" == typeof t.animation && (t.animation = t.animation.call());var n = k(),
				    o = "string" == typeof t.target ? document.querySelector(t.target) : t.target;e = n && o && n.parentNode !== o.parentNode ? X(t) : n || X(t), t.width && (e.style.width = "number" == typeof t.width ? t.width + "px" : t.width), t.padding && (e.style.padding = "number" == typeof t.padding ? t.padding + "px" : t.padding), t.background && (e.style.background = t.background);for (var i = window.getComputedStyle(e).getPropertyValue("background-color"), r = e.querySelectorAll("[class^=swal2-success-circular-line], .swal2-success-fix"), a = 0; a < r.length; a++) {
					r[a].style.backgroundColor = i;
				}var s = w(),
				    c = $(),
				    u = Y();if (ae(t), ne(t), "string" == typeof t.backdrop ? w().style.background = t.backdrop : t.backdrop || z([document.documentElement, document.body], _["no-backdrop"]), !t.backdrop && t.allowOutsideClick && R('"allowOutsideClick" parameter requires `backdrop` parameter to be set to `true`'), t.position in _ ? z(s, _[t.position]) : (R('The "position" parameter is not valid, defaulting to "center"'), z(s, _.center)), t.grow && "string" == typeof t.grow) {
					var l = "grow-" + t.grow;l in _ && z(s, _[l]);
				}t.showCloseButton ? (c.setAttribute("aria-label", t.closeButtonAriaLabel), K(c)) : F(c), e.className = _.popup, t.toast ? (z([document.documentElement, document.body], _["toast-shown"]), z(e, _.toast)) : z(e, _.modal), t.customClass && z(e, t.customClass), t.customContainerClass && z(s, t.customContainerClass), re(t), oe(t), ie(t), te(t), G(t.footer, u), !0 === t.animation ? W(e, _.noanimation) : z(e, _.noanimation), t.showLoaderOnConfirm && !t.preConfirm && R("showLoaderOnConfirm is set to true, but preConfirm is not defined.\nshowLoaderOnConfirm should be used together with preConfirm, see usage example:\nhttps://sweetalert2.github.io/#ajax-request");
			}(M), Object.freeze(M), Pe.innerParams.set(this, M), le.timeout && (le.timeout.stop(), delete le.timeout), clearTimeout(le.restoreFocusTimeout);var j = { popup: k(), container: w(), content: B(), actions: Q(), confirmButton: L(), cancelButton: O(), closeButton: $(), validationMessage: E(), progressSteps: P() };Pe.domCache.set(this, j);var V = this.constructor;return new Promise(function (t, n) {
				var o = function o(e) {
					V.closePopup(M.onClose, M.onAfterClose), M.useRejections ? t(e) : t({ value: e });
				},
				    c = function c(e) {
					V.closePopup(M.onClose, M.onAfterClose), M.useRejections ? n(e) : t({ dismiss: e });
				},
				    u = function u(e) {
					V.closePopup(M.onClose, M.onAfterClose), n(e);
				};M.timer && (le.timeout = new Te(function () {
					c("timer"), delete le.timeout;
				}, M.timer)), M.input && setTimeout(function () {
					var e = T.getInput();e && N(e);
				}, 0);for (var l = function l(t) {
					if (M.showLoaderOnConfirm && V.showLoading(), M.preConfirm) {
						T.resetValidationMessage();var e = Promise.resolve().then(function () {
							return M.preConfirm(t, M.extraParams);
						});M.expectRejections ? e.then(function (e) {
							return o(e || t);
						}, function (e) {
							T.hideLoading(), e && T.showValidationMessage(e);
						}) : e.then(function (e) {
							Z(j.validationMessage) || !1 === e ? T.hideLoading() : o(e || t);
						}, function (e) {
							return u(e);
						});
					} else o(t);
				}, e = function e(_e) {
					var t = _e.target,
					    n = j.confirmButton,
					    o = j.cancelButton,
					    i = n && (n === t || n.contains(t)),
					    r = o && (o === t || o.contains(t));switch (_e.type) {case "click":
							if (i && V.isVisible()) {
								if (T.disableButtons(), M.input) {
									var a = function () {
										var e = T.getInput();if (!e) return null;switch (M.input) {case "checkbox":
												return e.checked ? 1 : 0;case "radio":
												return e.checked ? e.value : null;case "file":
												return e.files.length ? e.files[0] : null;default:
												return M.inputAutoTrim ? e.value.trim() : e.value;}
									}();if (M.inputValidator) {
										T.disableInput();var s = Promise.resolve().then(function () {
											return M.inputValidator(a, M.extraParams);
										});M.expectRejections ? s.then(function () {
											T.enableButtons(), T.enableInput(), l(a);
										}, function (e) {
											T.enableButtons(), T.enableInput(), e && T.showValidationMessage(e);
										}) : s.then(function (e) {
											T.enableButtons(), T.enableInput(), e ? T.showValidationMessage(e) : l(a);
										}, function (e) {
											return u(e);
										});
									} else T.getInput().checkValidity() ? l(a) : (T.enableButtons(), T.showValidationMessage(M.validationMessage));
								} else l(!0);
							} else r && V.isVisible() && (T.disableButtons(), c(V.DismissReason.cancel));}
				}, i = j.popup.querySelectorAll("button"), r = 0; r < i.length; r++) {
					i[r].onclick = e, i[r].onmouseover = e, i[r].onmouseout = e, i[r].onmousedown = e;
				}if (j.closeButton.onclick = function () {
					c(V.DismissReason.close);
				}, M.toast) j.popup.onclick = function () {
					M.showConfirmButton || M.showCancelButton || M.showCloseButton || M.input || c(V.DismissReason.close);
				};else {
					var a = !1;j.popup.onmousedown = function () {
						j.container.onmouseup = function (e) {
							j.container.onmouseup = void 0, e.target === j.container && (a = !0);
						};
					}, j.container.onmousedown = function () {
						j.popup.onmouseup = function (e) {
							j.popup.onmouseup = void 0, (e.target === j.popup || j.popup.contains(e.target)) && (a = !0);
						};
					}, j.container.onclick = function (e) {
						a ? a = !1 : e.target === j.container && H(M.allowOutsideClick) && c(V.DismissReason.backdrop);
					};
				}M.reverseButtons ? j.confirmButton.parentNode.insertBefore(j.cancelButton, j.confirmButton) : j.confirmButton.parentNode.insertBefore(j.confirmButton, j.cancelButton);var s = function s(e, t) {
					for (var n = J(M.focusCancel), o = 0; o < n.length; o++) {
						return (e += t) === n.length ? e = 0 : -1 === e && (e = n.length - 1), n[e].focus();
					}j.popup.focus();
				};le.keydownHandlerAdded && (le.keydownTarget.removeEventListener("keydown", le.keydownHandler, { capture: le.keydownListenerCapture }), le.keydownHandlerAdded = !1), M.toast || (le.keydownHandler = function (e) {
					return function (e, t) {
						if (t.stopKeydownPropagation && e.stopPropagation(), "Enter" !== e.key || e.isComposing) {
							if ("Tab" === e.key) {
								for (var n = e.target, o = J(t.focusCancel), i = -1, r = 0; r < o.length; r++) {
									if (n === o[r]) {
										i = r;break;
									}
								}e.shiftKey ? s(i, -1) : s(i, 1), e.stopPropagation(), e.preventDefault();
							} else -1 !== ["ArrowLeft", "ArrowRight", "ArrowUp", "ArrowDown", "Left", "Right", "Up", "Down"].indexOf(e.key) ? document.activeElement === j.confirmButton && Z(j.cancelButton) ? j.cancelButton.focus() : document.activeElement === j.cancelButton && Z(j.confirmButton) && j.confirmButton.focus() : "Escape" !== e.key && "Esc" !== e.key || !0 !== H(t.allowEscapeKey) || (e.preventDefault(), c(V.DismissReason.esc));
						} else if (e.target && T.getInput() && e.target.outerHTML === T.getInput().outerHTML) {
							if (-1 !== ["textarea", "file"].indexOf(t.input)) return;V.clickConfirm(), e.preventDefault();
						}
					}(e, M);
				}, le.keydownTarget = M.keydownListenerCapture ? window : j.popup, le.keydownListenerCapture = M.keydownListenerCapture, le.keydownTarget.addEventListener("keydown", le.keydownHandler, { capture: le.keydownListenerCapture }), le.keydownHandlerAdded = !0), T.enableButtons(), T.hideLoading(), T.resetValidationMessage(), M.toast && (M.input || M.footer || M.showCloseButton) ? z(document.body, _["toast-column"]) : W(document.body, _["toast-column"]);for (var d, p, f = ["input", "file", "range", "select", "radio", "checkbox", "textarea"], m = function m(e) {
					e.placeholder && !M.inputPlaceholder || (e.placeholder = M.inputPlaceholder);
				}, h = 0; h < f.length; h++) {
					var g = _[f[h]],
					    b = U(j.content, g);if (d = T.getInput(f[h])) {
						for (var v in d.attributes) {
							if (d.attributes.hasOwnProperty(v)) {
								var y = d.attributes[v].name;"type" !== y && "value" !== y && d.removeAttribute(y);
							}
						}for (var w in M.inputAttributes) {
							"range" === f[h] && "placeholder" === w || d.setAttribute(w, M.inputAttributes[w]);
						}
					}b.className = g, M.inputClass && z(b, M.inputClass), F(b);
				}switch (M.input) {case "text":case "email":case "password":case "number":case "tel":case "url":
						d = U(j.content, _.input), "string" == typeof M.inputValue || "number" == typeof M.inputValue ? d.value = M.inputValue : D(M.inputValue) || R('Unexpected type of inputValue! Expected "string", "number" or "Promise", got "'.concat(q(M.inputValue), '"')), m(d), d.type = M.input, K(d);break;case "file":
						m(d = U(j.content, _.file)), d.type = M.input, K(d);break;case "range":
						var C = U(j.content, _.range),
						    k = C.querySelector("input"),
						    x = C.querySelector("output");k.value = M.inputValue, k.type = M.input, x.value = M.inputValue, K(C);break;case "select":
						var A = U(j.content, _.select);if (A.innerHTML = "", M.inputPlaceholder) {
							var B = document.createElement("option");B.innerHTML = M.inputPlaceholder, B.value = "", B.disabled = !0, B.selected = !0, A.appendChild(B);
						}p = function p(e) {
							e.forEach(function (e) {
								var t = e[0],
								    n = e[1],
								    o = document.createElement("option");o.value = t, o.innerHTML = n, M.inputValue.toString() === t.toString() && (o.selected = !0), A.appendChild(o);
							}), K(A), A.focus();
						};break;case "radio":
						var S = U(j.content, _.radio);S.innerHTML = "", p = function p(e) {
							e.forEach(function (e) {
								var t = e[0],
								    n = e[1],
								    o = document.createElement("input"),
								    i = document.createElement("label");o.type = "radio", o.name = _.radio, o.value = t, M.inputValue.toString() === t.toString() && (o.checked = !0);var r = document.createElement("span");r.innerHTML = n, r.className = _.label, i.appendChild(o), i.appendChild(r), S.appendChild(i);
							}), K(S);var t = S.querySelectorAll("input");t.length && t[0].focus();
						};break;case "checkbox":
						var P = U(j.content, _.checkbox),
						    E = T.getInput("checkbox");E.type = "checkbox", E.value = 1, E.id = _.checkbox, E.checked = Boolean(M.inputValue), P.querySelector("span").innerHTML = M.inputPlaceholder, K(P);break;case "textarea":
						var L = U(j.content, _.textarea);L.value = M.inputValue, m(L), K(L);break;case null:
						break;default:
						I('Unexpected type of input! Expected "text", "email", "password", "number", "tel", "select", "radio", "checkbox", "textarea", "file" or "url", got "'.concat(M.input, '"'));}if ("select" === M.input || "radio" === M.input) {
					var O = function O(e) {
						return p((t = e, n = [], "undefined" != typeof Map && t instanceof Map ? t.forEach(function (e, t) {
							n.push([t, e]);
						}) : Object.keys(t).forEach(function (e) {
							n.push([e, t[e]]);
						}), n));var t, n;
					};D(M.inputOptions) ? (V.showLoading(), M.inputOptions.then(function (e) {
						T.hideLoading(), O(e);
					})) : "object" === q(M.inputOptions) ? O(M.inputOptions) : I("Unexpected type of inputOptions! Expected object, Map or Promise, got ".concat(q(M.inputOptions)));
				} else -1 !== ["text", "email", "number", "tel", "textarea"].indexOf(M.input) && D(M.inputValue) && (V.showLoading(), F(d), M.inputValue.then(function (e) {
					d.value = "number" === M.input ? parseFloat(e) || 0 : e + "", K(d), d.focus(), T.hideLoading();
				}).catch(function (e) {
					I("Error in inputValue promise: " + e), d.value = "", K(d), d.focus(), T.hideLoading();
				}));je(M), M.toast || (H(M.allowEnterKey) ? M.focusCancel && Z(j.cancelButton) ? j.cancelButton.focus() : M.focusConfirm && Z(j.confirmButton) ? j.confirmButton.focus() : s(-1, 1) : document.activeElement && "function" == typeof document.activeElement.blur && document.activeElement.blur()), j.container.scrollTop = 0;
			});
		} });function Re() {
		if ("undefined" != typeof window) {
			"undefined" == typeof Promise && I("This package requires a Promise library, please include a shim to enable it in this browser (See: https://github.com/sweetalert2/sweetalert2/wiki/Migration-from-SweetAlert-to-SweetAlert2#1-ie-support)"), Ve = this;for (var e = arguments.length, t = new Array(e), n = 0; n < e; n++) {
				t[n] = arguments[n];
			}var o = Object.freeze(this.constructor.argsToParams(t));Object.defineProperties(this, { params: { value: o, writable: !1, enumerable: !0 } });var i = this._main(this.params);Pe.promise.set(this, i);
		}
	}Re.prototype.then = function (e, t) {
		return Pe.promise.get(this).then(e, t);
	}, Re.prototype.catch = function (e) {
		return Pe.promise.get(this).catch(e);
	}, Re.prototype.finally = function (e) {
		return Pe.promise.get(this).finally(e);
	}, r(Re.prototype, qe), r(Re, Ae), Object.keys(qe).forEach(function (t) {
		Re[t] = function () {
			var e;if (Ve) return (e = Ve)[t].apply(e, arguments);
		};
	}), Re.DismissReason = e, Re.noop = function () {};var Ie,
	    He,
	    De = fe((Ie = Re, He = function (e) {
		function t() {
			return a(this, t), d(this, c(t).apply(this, arguments));
		}return s(t, Ie), i(t, [{ key: "_main", value: function value(e) {
				return p(c(t.prototype), "_main", this).call(this, r({}, Ce, e));
			} }], [{ key: "setDefaults", value: function value(t) {
				if (m(we), !t || "object" !== q(t)) throw new TypeError("SweetAlert2: The argument for setDefaults() is required and has to be a object");ye(t), Object.keys(t).forEach(function (e) {
					Ie.isValidParameter(e) && (Ce[e] = t[e]);
				});
			} }, { key: "resetDefaults", value: function value() {
				m(we), Ce = {};
			} }]), t;
	}(), "undefined" != typeof window && "object" === q(window._swalDefaults) && He.setDefaults(window._swalDefaults), He));return De.default = De;
}), "undefined" != typeof window && window.Sweetalert2 && (window.Sweetalert2.version = "7.33.1", window.swal = window.sweetAlert = window.Swal = window.SweetAlert = window.Sweetalert2);
"undefined" != typeof document && function (e, t) {
	var n = e.createElement("style");if (e.getElementsByTagName("head")[0].appendChild(n), n.styleSheet) n.styleSheet.disabled || (n.styleSheet.cssText = t);else try {
		n.innerHTML = t;
	} catch (e) {
		n.innerText = t;
	}
}(document, "@-webkit-keyframes swal2-show{0%{-webkit-transform:scale(.7);transform:scale(.7)}45%{-webkit-transform:scale(1.05);transform:scale(1.05)}80%{-webkit-transform:scale(.95);transform:scale(.95)}100%{-webkit-transform:scale(1);transform:scale(1)}}@keyframes swal2-show{0%{-webkit-transform:scale(.7);transform:scale(.7)}45%{-webkit-transform:scale(1.05);transform:scale(1.05)}80%{-webkit-transform:scale(.95);transform:scale(.95)}100%{-webkit-transform:scale(1);transform:scale(1)}}@-webkit-keyframes swal2-hide{0%{-webkit-transform:scale(1);transform:scale(1);opacity:1}100%{-webkit-transform:scale(.5);transform:scale(.5);opacity:0}}@keyframes swal2-hide{0%{-webkit-transform:scale(1);transform:scale(1);opacity:1}100%{-webkit-transform:scale(.5);transform:scale(.5);opacity:0}}@-webkit-keyframes swal2-animate-success-line-tip{0%{top:1.1875em;left:.0625em;width:0}54%{top:1.0625em;left:.125em;width:0}70%{top:2.1875em;left:-.375em;width:3.125em}84%{top:3em;left:1.3125em;width:1.0625em}100%{top:2.8125em;left:.875em;width:1.5625em}}@keyframes swal2-animate-success-line-tip{0%{top:1.1875em;left:.0625em;width:0}54%{top:1.0625em;left:.125em;width:0}70%{top:2.1875em;left:-.375em;width:3.125em}84%{top:3em;left:1.3125em;width:1.0625em}100%{top:2.8125em;left:.875em;width:1.5625em}}@-webkit-keyframes swal2-animate-success-line-long{0%{top:3.375em;right:2.875em;width:0}65%{top:3.375em;right:2.875em;width:0}84%{top:2.1875em;right:0;width:3.4375em}100%{top:2.375em;right:.5em;width:2.9375em}}@keyframes swal2-animate-success-line-long{0%{top:3.375em;right:2.875em;width:0}65%{top:3.375em;right:2.875em;width:0}84%{top:2.1875em;right:0;width:3.4375em}100%{top:2.375em;right:.5em;width:2.9375em}}@-webkit-keyframes swal2-rotate-success-circular-line{0%{-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}5%{-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}12%{-webkit-transform:rotate(-405deg);transform:rotate(-405deg)}100%{-webkit-transform:rotate(-405deg);transform:rotate(-405deg)}}@keyframes swal2-rotate-success-circular-line{0%{-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}5%{-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}12%{-webkit-transform:rotate(-405deg);transform:rotate(-405deg)}100%{-webkit-transform:rotate(-405deg);transform:rotate(-405deg)}}@-webkit-keyframes swal2-animate-error-x-mark{0%{margin-top:1.625em;-webkit-transform:scale(.4);transform:scale(.4);opacity:0}50%{margin-top:1.625em;-webkit-transform:scale(.4);transform:scale(.4);opacity:0}80%{margin-top:-.375em;-webkit-transform:scale(1.15);transform:scale(1.15)}100%{margin-top:0;-webkit-transform:scale(1);transform:scale(1);opacity:1}}@keyframes swal2-animate-error-x-mark{0%{margin-top:1.625em;-webkit-transform:scale(.4);transform:scale(.4);opacity:0}50%{margin-top:1.625em;-webkit-transform:scale(.4);transform:scale(.4);opacity:0}80%{margin-top:-.375em;-webkit-transform:scale(1.15);transform:scale(1.15)}100%{margin-top:0;-webkit-transform:scale(1);transform:scale(1);opacity:1}}@-webkit-keyframes swal2-animate-error-icon{0%{-webkit-transform:rotateX(100deg);transform:rotateX(100deg);opacity:0}100%{-webkit-transform:rotateX(0);transform:rotateX(0);opacity:1}}@keyframes swal2-animate-error-icon{0%{-webkit-transform:rotateX(100deg);transform:rotateX(100deg);opacity:0}100%{-webkit-transform:rotateX(0);transform:rotateX(0);opacity:1}}body.swal2-toast-shown .swal2-container{background-color:transparent}body.swal2-toast-shown .swal2-container.swal2-shown{background-color:transparent}body.swal2-toast-shown .swal2-container.swal2-top{top:0;right:auto;bottom:auto;left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%)}body.swal2-toast-shown .swal2-container.swal2-top-end,body.swal2-toast-shown .swal2-container.swal2-top-right{top:0;right:0;bottom:auto;left:auto}body.swal2-toast-shown .swal2-container.swal2-top-left,body.swal2-toast-shown .swal2-container.swal2-top-start{top:0;right:auto;bottom:auto;left:0}body.swal2-toast-shown .swal2-container.swal2-center-left,body.swal2-toast-shown .swal2-container.swal2-center-start{top:50%;right:auto;bottom:auto;left:0;-webkit-transform:translateY(-50%);transform:translateY(-50%)}body.swal2-toast-shown .swal2-container.swal2-center{top:50%;right:auto;bottom:auto;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}body.swal2-toast-shown .swal2-container.swal2-center-end,body.swal2-toast-shown .swal2-container.swal2-center-right{top:50%;right:0;bottom:auto;left:auto;-webkit-transform:translateY(-50%);transform:translateY(-50%)}body.swal2-toast-shown .swal2-container.swal2-bottom-left,body.swal2-toast-shown .swal2-container.swal2-bottom-start{top:auto;right:auto;bottom:0;left:0}body.swal2-toast-shown .swal2-container.swal2-bottom{top:auto;right:auto;bottom:0;left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%)}body.swal2-toast-shown .swal2-container.swal2-bottom-end,body.swal2-toast-shown .swal2-container.swal2-bottom-right{top:auto;right:0;bottom:0;left:auto}body.swal2-toast-column .swal2-toast{flex-direction:column;align-items:stretch}body.swal2-toast-column .swal2-toast .swal2-actions{flex:1;align-self:stretch;height:2.2em;margin-top:.3125em}body.swal2-toast-column .swal2-toast .swal2-loading{justify-content:center}body.swal2-toast-column .swal2-toast .swal2-input{height:2em;margin:.3125em auto;font-size:1em}body.swal2-toast-column .swal2-toast .swal2-validation-message{font-size:1em}.swal2-popup.swal2-toast{flex-direction:row;align-items:center;width:auto;padding:.625em;box-shadow:0 0 .625em #d9d9d9;overflow-y:hidden}.swal2-popup.swal2-toast .swal2-header{flex-direction:row}.swal2-popup.swal2-toast .swal2-title{flex-grow:1;justify-content:flex-start;margin:0 .6em;font-size:1em}.swal2-popup.swal2-toast .swal2-footer{margin:.5em 0 0;padding:.5em 0 0;font-size:.8em}.swal2-popup.swal2-toast .swal2-close{position:initial;width:.8em;height:.8em;line-height:.8}.swal2-popup.swal2-toast .swal2-content{justify-content:flex-start;font-size:1em}.swal2-popup.swal2-toast .swal2-icon{width:2em;min-width:2em;height:2em;margin:0}.swal2-popup.swal2-toast .swal2-icon-text{font-size:2em;font-weight:700;line-height:1em}.swal2-popup.swal2-toast .swal2-icon.swal2-success .swal2-success-ring{width:2em;height:2em}.swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line]{top:.875em;width:1.375em}.swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=left]{left:.3125em}.swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=right]{right:.3125em}.swal2-popup.swal2-toast .swal2-actions{height:auto;margin:0 .3125em}.swal2-popup.swal2-toast .swal2-styled{margin:0 .3125em;padding:.3125em .625em;font-size:1em}.swal2-popup.swal2-toast .swal2-styled:focus{box-shadow:0 0 0 .0625em #fff,0 0 0 .125em rgba(50,100,150,.4)}.swal2-popup.swal2-toast .swal2-success{border-color:#a5dc86}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line]{position:absolute;width:2em;height:2.8125em;-webkit-transform:rotate(45deg);transform:rotate(45deg);border-radius:50%}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line][class$=left]{top:-.25em;left:-.9375em;-webkit-transform:rotate(-45deg);transform:rotate(-45deg);-webkit-transform-origin:2em 2em;transform-origin:2em 2em;border-radius:4em 0 0 4em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line][class$=right]{top:-.25em;left:.9375em;-webkit-transform-origin:0 2em;transform-origin:0 2em;border-radius:0 4em 4em 0}.swal2-popup.swal2-toast .swal2-success .swal2-success-ring{width:2em;height:2em}.swal2-popup.swal2-toast .swal2-success .swal2-success-fix{top:0;left:.4375em;width:.4375em;height:2.6875em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line]{height:.3125em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line][class$=tip]{top:1.125em;left:.1875em;width:.75em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line][class$=long]{top:.9375em;right:.1875em;width:1.375em}.swal2-popup.swal2-toast.swal2-show{-webkit-animation:showSweetToast .5s;animation:showSweetToast .5s}.swal2-popup.swal2-toast.swal2-hide{-webkit-animation:hideSweetToast .2s forwards;animation:hideSweetToast .2s forwards}.swal2-popup.swal2-toast .swal2-animate-success-icon .swal2-success-line-tip{-webkit-animation:animate-toast-success-tip .75s;animation:animate-toast-success-tip .75s}.swal2-popup.swal2-toast .swal2-animate-success-icon .swal2-success-line-long{-webkit-animation:animate-toast-success-long .75s;animation:animate-toast-success-long .75s}@-webkit-keyframes showSweetToast{0%{-webkit-transform:translateY(-.625em) rotateZ(2deg);transform:translateY(-.625em) rotateZ(2deg);opacity:0}33%{-webkit-transform:translateY(0) rotateZ(-2deg);transform:translateY(0) rotateZ(-2deg);opacity:.5}66%{-webkit-transform:translateY(.3125em) rotateZ(2deg);transform:translateY(.3125em) rotateZ(2deg);opacity:.7}100%{-webkit-transform:translateY(0) rotateZ(0);transform:translateY(0) rotateZ(0);opacity:1}}@keyframes showSweetToast{0%{-webkit-transform:translateY(-.625em) rotateZ(2deg);transform:translateY(-.625em) rotateZ(2deg);opacity:0}33%{-webkit-transform:translateY(0) rotateZ(-2deg);transform:translateY(0) rotateZ(-2deg);opacity:.5}66%{-webkit-transform:translateY(.3125em) rotateZ(2deg);transform:translateY(.3125em) rotateZ(2deg);opacity:.7}100%{-webkit-transform:translateY(0) rotateZ(0);transform:translateY(0) rotateZ(0);opacity:1}}@-webkit-keyframes hideSweetToast{0%{opacity:1}33%{opacity:.5}100%{-webkit-transform:rotateZ(1deg);transform:rotateZ(1deg);opacity:0}}@keyframes hideSweetToast{0%{opacity:1}33%{opacity:.5}100%{-webkit-transform:rotateZ(1deg);transform:rotateZ(1deg);opacity:0}}@-webkit-keyframes animate-toast-success-tip{0%{top:.5625em;left:.0625em;width:0}54%{top:.125em;left:.125em;width:0}70%{top:.625em;left:-.25em;width:1.625em}84%{top:1.0625em;left:.75em;width:.5em}100%{top:1.125em;left:.1875em;width:.75em}}@keyframes animate-toast-success-tip{0%{top:.5625em;left:.0625em;width:0}54%{top:.125em;left:.125em;width:0}70%{top:.625em;left:-.25em;width:1.625em}84%{top:1.0625em;left:.75em;width:.5em}100%{top:1.125em;left:.1875em;width:.75em}}@-webkit-keyframes animate-toast-success-long{0%{top:1.625em;right:1.375em;width:0}65%{top:1.25em;right:.9375em;width:0}84%{top:.9375em;right:0;width:1.125em}100%{top:.9375em;right:.1875em;width:1.375em}}@keyframes animate-toast-success-long{0%{top:1.625em;right:1.375em;width:0}65%{top:1.25em;right:.9375em;width:0}84%{top:.9375em;right:0;width:1.125em}100%{top:.9375em;right:.1875em;width:1.375em}}body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown){overflow:hidden}body.swal2-height-auto{height:auto!important}body.swal2-no-backdrop .swal2-shown{top:auto;right:auto;bottom:auto;left:auto;background-color:transparent}body.swal2-no-backdrop .swal2-shown>.swal2-modal{box-shadow:0 0 10px rgba(0,0,0,.4)}body.swal2-no-backdrop .swal2-shown.swal2-top{top:0;left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%)}body.swal2-no-backdrop .swal2-shown.swal2-top-left,body.swal2-no-backdrop .swal2-shown.swal2-top-start{top:0;left:0}body.swal2-no-backdrop .swal2-shown.swal2-top-end,body.swal2-no-backdrop .swal2-shown.swal2-top-right{top:0;right:0}body.swal2-no-backdrop .swal2-shown.swal2-center{top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}body.swal2-no-backdrop .swal2-shown.swal2-center-left,body.swal2-no-backdrop .swal2-shown.swal2-center-start{top:50%;left:0;-webkit-transform:translateY(-50%);transform:translateY(-50%)}body.swal2-no-backdrop .swal2-shown.swal2-center-end,body.swal2-no-backdrop .swal2-shown.swal2-center-right{top:50%;right:0;-webkit-transform:translateY(-50%);transform:translateY(-50%)}body.swal2-no-backdrop .swal2-shown.swal2-bottom{bottom:0;left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%)}body.swal2-no-backdrop .swal2-shown.swal2-bottom-left,body.swal2-no-backdrop .swal2-shown.swal2-bottom-start{bottom:0;left:0}body.swal2-no-backdrop .swal2-shown.swal2-bottom-end,body.swal2-no-backdrop .swal2-shown.swal2-bottom-right{right:0;bottom:0}.swal2-container{display:flex;position:fixed;top:0;right:0;bottom:0;left:0;flex-direction:row;align-items:center;justify-content:center;padding:10px;background-color:transparent;z-index:1060;overflow-x:hidden;-webkit-overflow-scrolling:touch}.swal2-container.swal2-top{align-items:flex-start}.swal2-container.swal2-top-left,.swal2-container.swal2-top-start{align-items:flex-start;justify-content:flex-start}.swal2-container.swal2-top-end,.swal2-container.swal2-top-right{align-items:flex-start;justify-content:flex-end}.swal2-container.swal2-center{align-items:center}.swal2-container.swal2-center-left,.swal2-container.swal2-center-start{align-items:center;justify-content:flex-start}.swal2-container.swal2-center-end,.swal2-container.swal2-center-right{align-items:center;justify-content:flex-end}.swal2-container.swal2-bottom{align-items:flex-end}.swal2-container.swal2-bottom-left,.swal2-container.swal2-bottom-start{align-items:flex-end;justify-content:flex-start}.swal2-container.swal2-bottom-end,.swal2-container.swal2-bottom-right{align-items:flex-end;justify-content:flex-end}.swal2-container.swal2-grow-fullscreen>.swal2-modal{display:flex!important;flex:1;align-self:stretch;justify-content:center}.swal2-container.swal2-grow-row>.swal2-modal{display:flex!important;flex:1;align-content:center;justify-content:center}.swal2-container.swal2-grow-column{flex:1;flex-direction:column}.swal2-container.swal2-grow-column.swal2-bottom,.swal2-container.swal2-grow-column.swal2-center,.swal2-container.swal2-grow-column.swal2-top{align-items:center}.swal2-container.swal2-grow-column.swal2-bottom-left,.swal2-container.swal2-grow-column.swal2-bottom-start,.swal2-container.swal2-grow-column.swal2-center-left,.swal2-container.swal2-grow-column.swal2-center-start,.swal2-container.swal2-grow-column.swal2-top-left,.swal2-container.swal2-grow-column.swal2-top-start{align-items:flex-start}.swal2-container.swal2-grow-column.swal2-bottom-end,.swal2-container.swal2-grow-column.swal2-bottom-right,.swal2-container.swal2-grow-column.swal2-center-end,.swal2-container.swal2-grow-column.swal2-center-right,.swal2-container.swal2-grow-column.swal2-top-end,.swal2-container.swal2-grow-column.swal2-top-right{align-items:flex-end}.swal2-container.swal2-grow-column>.swal2-modal{display:flex!important;flex:1;align-content:center;justify-content:center}.swal2-container:not(.swal2-top):not(.swal2-top-start):not(.swal2-top-end):not(.swal2-top-left):not(.swal2-top-right):not(.swal2-center-start):not(.swal2-center-end):not(.swal2-center-left):not(.swal2-center-right):not(.swal2-bottom):not(.swal2-bottom-start):not(.swal2-bottom-end):not(.swal2-bottom-left):not(.swal2-bottom-right):not(.swal2-grow-fullscreen)>.swal2-modal{margin:auto}@media all and (-ms-high-contrast:none),(-ms-high-contrast:active){.swal2-container .swal2-modal{margin:0!important}}.swal2-container.swal2-fade{transition:background-color .1s}.swal2-container.swal2-shown{background-color:rgba(0,0,0,.4)}.swal2-popup{display:none;position:relative;flex-direction:column;justify-content:center;width:32em;max-width:100%;padding:1.25em;border-radius:.3125em;background:#fff;font-family:inherit;font-size:1rem;box-sizing:border-box}.swal2-popup:focus{outline:0}.swal2-popup.swal2-loading{overflow-y:hidden}.swal2-popup .swal2-header{display:flex;flex-direction:column;align-items:center}.swal2-popup .swal2-title{display:block;position:relative;max-width:100%;margin:0 0 .4em;padding:0;color:#595959;font-size:1.875em;font-weight:600;text-align:center;text-transform:none;word-wrap:break-word}.swal2-popup .swal2-actions{flex-wrap:wrap;align-items:center;justify-content:center;margin:1.25em auto 0;z-index:1}.swal2-popup .swal2-actions:not(.swal2-loading) .swal2-styled[disabled]{opacity:.4}.swal2-popup .swal2-actions:not(.swal2-loading) .swal2-styled:hover{background-image:linear-gradient(rgba(0,0,0,.1),rgba(0,0,0,.1))}.swal2-popup .swal2-actions:not(.swal2-loading) .swal2-styled:active{background-image:linear-gradient(rgba(0,0,0,.2),rgba(0,0,0,.2))}.swal2-popup .swal2-actions.swal2-loading .swal2-styled.swal2-confirm{width:2.5em;height:2.5em;margin:.46875em;padding:0;border:.25em solid transparent;border-radius:100%;border-color:transparent;background-color:transparent!important;color:transparent;cursor:default;box-sizing:border-box;-webkit-animation:swal2-rotate-loading 1.5s linear 0s infinite normal;animation:swal2-rotate-loading 1.5s linear 0s infinite normal;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.swal2-popup .swal2-actions.swal2-loading .swal2-styled.swal2-cancel{margin-right:30px;margin-left:30px}.swal2-popup .swal2-actions.swal2-loading :not(.swal2-styled).swal2-confirm::after{display:inline-block;width:15px;height:15px;margin-left:5px;border:3px solid #999;border-radius:50%;border-right-color:transparent;box-shadow:1px 1px 1px #fff;content:'';-webkit-animation:swal2-rotate-loading 1.5s linear 0s infinite normal;animation:swal2-rotate-loading 1.5s linear 0s infinite normal}.swal2-popup .swal2-styled{margin:.3125em;padding:.625em 2em;font-weight:500;box-shadow:none}.swal2-popup .swal2-styled:not([disabled]){cursor:pointer}.swal2-popup .swal2-styled.swal2-confirm{border:0;border-radius:.25em;background:initial;background-color:#3085d6;color:#fff;font-size:1.0625em}.swal2-popup .swal2-styled.swal2-cancel{border:0;border-radius:.25em;background:initial;background-color:#aaa;color:#fff;font-size:1.0625em}.swal2-popup .swal2-styled:focus{outline:0;box-shadow:0 0 0 2px #fff,0 0 0 4px rgba(50,100,150,.4)}.swal2-popup .swal2-styled::-moz-focus-inner{border:0}.swal2-popup .swal2-footer{justify-content:center;margin:1.25em 0 0;padding:1em 0 0;border-top:1px solid #eee;color:#545454;font-size:1em}.swal2-popup .swal2-image{max-width:100%;margin:1.25em auto}.swal2-popup .swal2-close{position:absolute;top:0;right:0;justify-content:center;width:1.2em;height:1.2em;padding:0;transition:color .1s ease-out;border:none;border-radius:0;outline:initial;background:0 0;color:#ccc;font-family:serif;font-size:2.5em;line-height:1.2;cursor:pointer;overflow:hidden}.swal2-popup .swal2-close:hover{-webkit-transform:none;transform:none;color:#f27474}.swal2-popup>.swal2-checkbox,.swal2-popup>.swal2-file,.swal2-popup>.swal2-input,.swal2-popup>.swal2-radio,.swal2-popup>.swal2-select,.swal2-popup>.swal2-textarea{display:none}.swal2-popup .swal2-content{justify-content:center;margin:0;padding:0;color:#545454;font-size:1.125em;font-weight:300;line-height:normal;z-index:1;word-wrap:break-word}.swal2-popup #swal2-content{text-align:center}.swal2-popup .swal2-checkbox,.swal2-popup .swal2-file,.swal2-popup .swal2-input,.swal2-popup .swal2-radio,.swal2-popup .swal2-select,.swal2-popup .swal2-textarea{margin:1em auto}.swal2-popup .swal2-file,.swal2-popup .swal2-input,.swal2-popup .swal2-textarea{width:100%;transition:border-color .3s,box-shadow .3s;border:1px solid #d9d9d9;border-radius:.1875em;font-size:1.125em;box-shadow:inset 0 1px 1px rgba(0,0,0,.06);box-sizing:border-box}.swal2-popup .swal2-file.swal2-inputerror,.swal2-popup .swal2-input.swal2-inputerror,.swal2-popup .swal2-textarea.swal2-inputerror{border-color:#f27474!important;box-shadow:0 0 2px #f27474!important}.swal2-popup .swal2-file:focus,.swal2-popup .swal2-input:focus,.swal2-popup .swal2-textarea:focus{border:1px solid #b4dbed;outline:0;box-shadow:0 0 3px #c4e6f5}.swal2-popup .swal2-file::-webkit-input-placeholder,.swal2-popup .swal2-input::-webkit-input-placeholder,.swal2-popup .swal2-textarea::-webkit-input-placeholder{color:#ccc}.swal2-popup .swal2-file:-ms-input-placeholder,.swal2-popup .swal2-input:-ms-input-placeholder,.swal2-popup .swal2-textarea:-ms-input-placeholder{color:#ccc}.swal2-popup .swal2-file::-ms-input-placeholder,.swal2-popup .swal2-input::-ms-input-placeholder,.swal2-popup .swal2-textarea::-ms-input-placeholder{color:#ccc}.swal2-popup .swal2-file::placeholder,.swal2-popup .swal2-input::placeholder,.swal2-popup .swal2-textarea::placeholder{color:#ccc}.swal2-popup .swal2-range input{width:80%}.swal2-popup .swal2-range output{width:20%;font-weight:600;text-align:center}.swal2-popup .swal2-range input,.swal2-popup .swal2-range output{height:2.625em;margin:1em auto;padding:0;font-size:1.125em;line-height:2.625em}.swal2-popup .swal2-input{height:2.625em;padding:0 .75em}.swal2-popup .swal2-input[type=number]{max-width:10em}.swal2-popup .swal2-file{font-size:1.125em}.swal2-popup .swal2-textarea{height:6.75em;padding:.75em}.swal2-popup .swal2-select{min-width:50%;max-width:100%;padding:.375em .625em;color:#545454;font-size:1.125em}.swal2-popup .swal2-checkbox,.swal2-popup .swal2-radio{align-items:center;justify-content:center}.swal2-popup .swal2-checkbox label,.swal2-popup .swal2-radio label{margin:0 .6em;font-size:1.125em}.swal2-popup .swal2-checkbox input,.swal2-popup .swal2-radio input{margin:0 .4em}.swal2-popup .swal2-validation-message{display:none;align-items:center;justify-content:center;padding:.625em;background:#f0f0f0;color:#666;font-size:1em;font-weight:300;overflow:hidden}.swal2-popup .swal2-validation-message::before{display:inline-block;width:1.5em;min-width:1.5em;height:1.5em;margin:0 .625em;border-radius:50%;background-color:#f27474;color:#fff;font-weight:600;line-height:1.5em;text-align:center;content:'!';zoom:normal}@supports (-ms-accelerator:true){.swal2-range input{width:100%!important}.swal2-range output{display:none}}@media all and (-ms-high-contrast:none),(-ms-high-contrast:active){.swal2-range input{width:100%!important}.swal2-range output{display:none}}@-moz-document url-prefix(){.swal2-close:focus{outline:2px solid rgba(50,100,150,.4)}}.swal2-icon{position:relative;justify-content:center;width:5em;height:5em;margin:1.25em auto 1.875em;border:.25em solid transparent;border-radius:50%;line-height:5em;cursor:default;box-sizing:content-box;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;zoom:normal}.swal2-icon-text{font-size:3.75em}.swal2-icon.swal2-error{border-color:#f27474}.swal2-icon.swal2-error .swal2-x-mark{position:relative;flex-grow:1}.swal2-icon.swal2-error [class^=swal2-x-mark-line]{display:block;position:absolute;top:2.3125em;width:2.9375em;height:.3125em;border-radius:.125em;background-color:#f27474}.swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=left]{left:1.0625em;-webkit-transform:rotate(45deg);transform:rotate(45deg)}.swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=right]{right:1em;-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}.swal2-icon.swal2-warning{border-color:#facea8;color:#f8bb86}.swal2-icon.swal2-info{border-color:#9de0f6;color:#3fc3ee}.swal2-icon.swal2-question{border-color:#c9dae1;color:#87adbd}.swal2-icon.swal2-success{border-color:#a5dc86}.swal2-icon.swal2-success [class^=swal2-success-circular-line]{position:absolute;width:3.75em;height:7.5em;-webkit-transform:rotate(45deg);transform:rotate(45deg);border-radius:50%}.swal2-icon.swal2-success [class^=swal2-success-circular-line][class$=left]{top:-.4375em;left:-2.0635em;-webkit-transform:rotate(-45deg);transform:rotate(-45deg);-webkit-transform-origin:3.75em 3.75em;transform-origin:3.75em 3.75em;border-radius:7.5em 0 0 7.5em}.swal2-icon.swal2-success [class^=swal2-success-circular-line][class$=right]{top:-.6875em;left:1.875em;-webkit-transform:rotate(-45deg);transform:rotate(-45deg);-webkit-transform-origin:0 3.75em;transform-origin:0 3.75em;border-radius:0 7.5em 7.5em 0}.swal2-icon.swal2-success .swal2-success-ring{position:absolute;top:-.25em;left:-.25em;width:100%;height:100%;border:.25em solid rgba(165,220,134,.3);border-radius:50%;z-index:2;box-sizing:content-box}.swal2-icon.swal2-success .swal2-success-fix{position:absolute;top:.5em;left:1.625em;width:.4375em;height:5.625em;-webkit-transform:rotate(-45deg);transform:rotate(-45deg);z-index:1}.swal2-icon.swal2-success [class^=swal2-success-line]{display:block;position:absolute;height:.3125em;border-radius:.125em;background-color:#a5dc86;z-index:2}.swal2-icon.swal2-success [class^=swal2-success-line][class$=tip]{top:2.875em;left:.875em;width:1.5625em;-webkit-transform:rotate(45deg);transform:rotate(45deg)}.swal2-icon.swal2-success [class^=swal2-success-line][class$=long]{top:2.375em;right:.5em;width:2.9375em;-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}.swal2-progresssteps{align-items:center;margin:0 0 1.25em;padding:0;font-weight:600}.swal2-progresssteps li{display:inline-block;position:relative}.swal2-progresssteps .swal2-progresscircle{width:2em;height:2em;border-radius:2em;background:#3085d6;color:#fff;line-height:2em;text-align:center;z-index:20}.swal2-progresssteps .swal2-progresscircle:first-child{margin-left:0}.swal2-progresssteps .swal2-progresscircle:last-child{margin-right:0}.swal2-progresssteps .swal2-progresscircle.swal2-activeprogressstep{background:#3085d6}.swal2-progresssteps .swal2-progresscircle.swal2-activeprogressstep~.swal2-progresscircle{background:#add8e6}.swal2-progresssteps .swal2-progresscircle.swal2-activeprogressstep~.swal2-progressline{background:#add8e6}.swal2-progresssteps .swal2-progressline{width:2.5em;height:.4em;margin:0 -1px;background:#3085d6;z-index:10}[class^=swal2]{-webkit-tap-highlight-color:transparent}.swal2-show{-webkit-animation:swal2-show .3s;animation:swal2-show .3s}.swal2-show.swal2-noanimation{-webkit-animation:none;animation:none}.swal2-hide{-webkit-animation:swal2-hide .15s forwards;animation:swal2-hide .15s forwards}.swal2-hide.swal2-noanimation{-webkit-animation:none;animation:none}.swal2-rtl .swal2-close{right:auto;left:0}.swal2-animate-success-icon .swal2-success-line-tip{-webkit-animation:swal2-animate-success-line-tip .75s;animation:swal2-animate-success-line-tip .75s}.swal2-animate-success-icon .swal2-success-line-long{-webkit-animation:swal2-animate-success-line-long .75s;animation:swal2-animate-success-line-long .75s}.swal2-animate-success-icon .swal2-success-circular-line-right{-webkit-animation:swal2-rotate-success-circular-line 4.25s ease-in;animation:swal2-rotate-success-circular-line 4.25s ease-in}.swal2-animate-error-icon{-webkit-animation:swal2-animate-error-icon .5s;animation:swal2-animate-error-icon .5s}.swal2-animate-error-icon .swal2-x-mark{-webkit-animation:swal2-animate-error-x-mark .5s;animation:swal2-animate-error-x-mark .5s}@-webkit-keyframes swal2-rotate-loading{0%{-webkit-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes swal2-rotate-loading{0%{-webkit-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@media print{body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown){overflow-y:scroll!important}body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown)>[aria-hidden=true]{display:none}body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown) .swal2-container{position:initial!important}}");
function iniciarProceso() {
	var OPTIONS_VIDEO = {
		modeCapture: IcarSDK.MODE_CAPTURE.DOCUMENT_IDCARD,
		callBackFeedBackFunction: onFeedBackCallBack,
		flip_video: false
	};
	IcarSDK.video.initialize(videoInput, OPTIONS_VIDEO);
	IcarSDK.video.getNumberOfCameras(onNumCamsReceivedCallback);

	var OPTIONS_DOCUMENTS = {
		type_document: IcarSDK.TYPE_DOC.IDCARD,
		MAX_ATTEMPTS: -1,
		fontText: "bold 50px Arial",
		colorText: "#c91f28",
		multiplicationFactorBlurring: 2.5,
		MSG_PLACE_DOC_INSIDE: "COLOCA LA IDENTIFICACIÓN\nDENTRO",
		MSG_DOC_OK: "PERMANECE QUIETO",
		MSG_DARK_IMAGE: "IMAGEN OSCURA",
		MSG_BLURRED_IMAGE: "IMAGEN BORROSA",
		MSG_TRY_AGAIN: "INTENTA ENFOCAR EL DOCUMENTO\nACERCANDOLO E INTENTA DE NUEVO"
	};
	IcarSDK.documentCapture.create(videoInput, requestFrameCallback, onDocumentResultCallback, OPTIONS_DOCUMENTS);
	setTimeout(function () {}, 2000);
	setTimeout(function () {
		$('#cargando').hide();
		$('#videoDiv').show();
	}, 2000);
}

function iniciarProcesoSelfie() {
	$('#cargando').show();
	setTimeout(function () {}, 2000);
	var OPTIONS_VIDEO = {
		modeCapture: IcarSDK.MODE_CAPTURE.FACE,
		callBackFeedBackFunction: onFeedBackCallBackFace,
		flip_video: false
	};
	IcarSDK.video.initialize(videoInput, OPTIONS_VIDEO);
	IcarSDK.video.getNumberOfCameras(onNumCamsReceivedCallback);

	var OPTIONS_FACE_CAPTURE = {
		fontText: "bold 25px Arial",
		colorText: "#c91f28",
		MIN_INTEROCULAR_DISTANCE: 0.4,
		MAX_INTEROCULAR_DISTANCE: 0.13,
		NUMBER_SEC_WAITING_STRING_PROCESS: 3,
		MSG_PLACE_FACE_INSIDE: "COLOCA EL ROSTRO DENTRO",
		MSG_STAY_STILL_AND_OPEN_EYES: "PERMANECE QUIETO Y ABRE LOS OJOS",
		MSG_NO_EYES_DETECTED: "OJOS NO DETECTADOS",
		MSG_COME_CLOSER: "POR FAVOR, ACERCATE",
		MSG_GO_BACK: "POR FAVOR, ALEJATE",
		MSG_BLURRED_IMAGE: "IMAGEN BORROSA",
		MSG_STAY_STILL: "POR FAVOR, PERMANECE QUIETO",
		MSG_PLACE_HEAD_CENTERED: "POR FAVOR, COLOCA EL ROSTRO CENTRADO Y HACIA EL FRENTE",
		MSG_TAKE_OFF_GLASSES: "SI USAS LENTES, POR FAVOR RETIRATELOS",
		MSG_BLINK_EYES: "POR FAVOR, PARPADEA LENTAMENTE",
		MSG_ABORTED: "PROCESO INTERRUMPIDO",
		MSG_STARTING_PROCESS: "INICIANDO\n\nEN %n SEGUNDOS\n\nABRE LOS OJOS Y\nSIGUE LAS INSTRUCCIONES",
		MSG_RESTARTING_PROCESS: "REINICIANDO \n\nEN %n SEGUNDOS\n\nABRE LOS OJOS Y\nSIGUE LAS INSTRUCCIONES",
		MSG_WRONG_POS_DEVICE: "COLOCA EL DISPOSITIVO DE FRENTE"
	};
	IcarSDK.faceCapture.create(videoInput, requestFrameCallback, onFaceCaptureResultCallback, OPTIONS_FACE_CAPTURE);
	setTimeout(function () {}, 2000);
	setTimeout(function () {
		$('#cargando').hide();
		$('#videoDiv').show();
	}, 2000);
}

function onNumCamsReceivedCallback(numberCams) {
	if (numberCams > 1) {}
}

function askForChangeCameraFunction() {
	IcarSDK.documentCapture.stop(changeCameraFunction);
	hideButtons_stopMode();
}

function changeCameraFunction() {
	IcarSDK.video.changeCamera();
}

function repetirPasoIne() {
	$('#cargando').show();
	setTimeout(function () {}, 2000);
	iniciarProceso();
	setTimeout(function () {
		$('#resultado').hide();
		$('#tomar-foto').show();
	}, 2000);
}

function repetirPasoSelfie() {
	$('#cargando').show();
	setTimeout(function () {}, 2000);
	iniciarProcesoSelfie();
	setTimeout(function () {
		$('#resultado').hide();
		$('#tomar-foto').show();
	}, 2000);
}

// function to do the request of the frame to the video
//'onFrameReceivedCallback' must to be the next parameters:
// - imageData: frame of the video
// - hashCode: hashCode of the image
function requestFrameCallback(onFrameReceivedCallback) {
	IcarSDK.video.requestFrame(onFrameReceivedCallback);
}

// Return function that the process will use when it will finish
// params:
//  - answerResult: property when is stored the result of the process:
//				IcarSDK.ResultProcess.OK
//				IcarSDK.ResultProcess.FAIL
//              IcarSDK.ResultProcess.ATTEMPTS_EXCEEDED
//  - modeCapture: mode of the capture:
//              IcarSDK.CaptureMode.MANUAL_TRIGGER
//              IcarSDK.CaptureMode.AUTO_TRIGGER
//  - imageResult: property where is stored the image captured
//  - hashCode: property that contains the hashcode generated from the image
//              result and the private key.
function onDocumentResultCallback(answerResult, modeCapture, imageResult, hashCode) {

	if (answerResult == IcarSDK.ResultProcess.OK) {
		IcarSDK.video.cleanUp();
		Swal.fire({
			type: 'success',
			title: '¡Documento capturado!',
			showConfirmButton: false,
			timer: 1500
		});

		var newCanvas = document.createElement('canvas');
		newCanvas.width = imageResult.width;
		newCanvas.height = imageResult.height;
		newCanvas.getContext("2d").putImageData(imageResult, 0, 0);

		var canvasResult = document.getElementById("resultImage");
		canvasResult.className = imageResult.className;
		canvasResult.width = imageResult.width;
		canvasResult.height = imageResult.height;
		var context = canvasResult.getContext('2d');
		context.clearRect(0, 0, canvasResult.width, canvasResult.height);
		context.scale($('#videoInput').width / Math.max(newCanvas.width, newCanvas.height), $('#videoInput').width / Math.max(newCanvas.width, newCanvas.height));
		context.drawImage(newCanvas, 0, 0);

		$('#resultado').show();
		$('#tomar-foto').hide();
	} else if (answerResult == IcarSDK.ResultProcess.ATTEMPTS_EXCEEDED) {
		alert("Number attempts exceeded!!\nPlease, try again.");
	} else {
		alert("OHPSSS!!!, Process stopped");
	}
}

// Return function that the process will use when it will finish
// params:
//  - answerResult: property when is stored the result of the process:
//				IcarSDK.ResultProcess.OK
//				IcarSDK.ResultProcess.FAIL
//              IcarSDK.ResultProcess.ATTEMPTS_EXCEEDED
//  - modeCapture: mode of the capture:
//              IcarSDK.CaptureMode.MANUAL_TRIGGER
//              IcarSDK.CaptureMode.AUTO_TRIGGER
//  - imageResult: property where is stored the image captured
//  - hashCode: property that contains the hashcode generated from the image
//              result and the private key.
function onFaceCaptureResultCallback(answerResult, modeCapture, imageResult, hashCode) {

	if (answerResult == IcarSDK.ResultProcess.OK) {
		IcarSDK.video.cleanUp();
		Swal.fire({
			type: 'success',
			title: '¡Imagen capturada!',
			showConfirmButton: false,
			timer: 1500
		});

		var newCanvas = document.createElement('canvas');
		newCanvas.width = imageResult.width;
		newCanvas.height = imageResult.height;
		newCanvas.getContext("2d").putImageData(imageResult, 0, 0);

		var canvasResult = document.getElementById("resultImage");
		canvasResult.className = imageResult.className;
		canvasResult.width = imageResult.width;
		canvasResult.height = imageResult.height;
		var context = canvasResult.getContext('2d');
		context.clearRect(0, 0, canvasResult.width, canvasResult.height);
		context.scale($('#videoInput').width / Math.max(newCanvas.width, newCanvas.height), $('#videoInput').width / Math.max(newCanvas.width, newCanvas.height));
		context.drawImage(newCanvas, 0, 0);

		$('#resultado').show();
		$('#tomar-foto').hide();
	} else if (answerResult == IcarSDK.ResultProcess.ATTEMPTS_EXCEEDED) {
		alert("Number attempts exceeded!!\nPlease, try again.");
	} else {
		alert("OHPSSS!!!, Process stopped");
	}
}

// FUNCTION: onFeedBackCallBack
// Callback function to be called if there is any problem
// executing the webSDK
// params:
//  - resultFeedBack: property where is indicated the result of
//           the execution. The possible values are:
//              IcarSDK.RESULT_VIDEO.OK
//              IcarSDK.RESULT_VIDEO.NO_CAMERA_DEVICES
//				IcarSDK.RESULT_VIDEO.NO_CAMERA_PERMISSION
//				IcarSDK.RESULT_VIDEO.UNAVAILABLE_CAMERA
//				IcarSDK.RESULT_VIDEO.UNSUPPORTED_BROWSER
//				IcarSDK.RESULT_VIDEO.UNKNOWN_ERROR
function onFeedBackCallBack(resultFeedBack) {
	switch (resultFeedBack) {
		case IcarSDK.RESULT_VIDEO.OK:
			// Video iniciado correctamente
			startFunction();
			break;
		case IcarSDK.RESULT_VIDEO.NO_CAMERA_DEVICES:
			alert("Cámara no detectada");
			break;
		case IcarSDK.RESULT_VIDEO.NO_CAMERA_PERMISSION:
			alert("No se concedieron permisos para usar la cámara");
			break;
		case IcarSDK.RESULT_VIDEO.UNAVAILABLE_CAMERA:
			alert("La cámara esta siendo usada en otro proceso");
			break;
		case IcarSDK.RESULT_VIDEO.UNSUPPORTED_BROWSER:
			alert("El navegador no es compatible");
			break;
		case IcarSDK.RESULT_VIDEO.UNKNOWN_ERROR:
			alert("Error desconocido al iniciar la cámara");
			break;
	}
}

function onFeedBackCallBackFace(resultFeedBack) {
	switch (resultFeedBack) {
		case IcarSDK.RESULT_VIDEO.OK:
			// Video iniciado correctamente
			startFunctionFace();
			break;
		case IcarSDK.RESULT_VIDEO.NO_CAMERA_DEVICES:
			alert("Cámara no detectada");
			break;
		case IcarSDK.RESULT_VIDEO.NO_CAMERA_PERMISSION:
			alert("No se concedieron permisos para usar la cámara");
			break;
		case IcarSDK.RESULT_VIDEO.UNAVAILABLE_CAMERA:
			alert("La cámara esta siendo usada en otro proceso");
			break;
		case IcarSDK.RESULT_VIDEO.UNSUPPORTED_BROWSER:
			alert("El navegador no es compatible");
			break;
		case IcarSDK.RESULT_VIDEO.UNKNOWN_ERROR:
			alert("Error desconocido al iniciar la cámara");
			break;
	}
}

function startFunction() {
	IcarSDK.documentCapture.start();
}

function startFunctionFace() {
	IcarSDK.faceCapture.start();
}

function stopFunctionFace() {
	IcarSDK.faceCapture.stop();
}

function stopFunction() {
	IcarSDK.documentCapture.stop();
}

function saveImage(tipoDocumento, detalleDocumento) {

	var texto = 'Guardando imagen...';
	Swal.fire({
		html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
		customClass: 'modalLoading',
		showCancelButton: false,
		showConfirmButton: false,
		allowOutsideClick: false
	});

	var canvas = document.getElementById('resultImage');
	var dataURL = canvas.toDataURL();
	axios.post('/carga_documentos/subirDocumento', {
		imgBase64: dataURL,
		tipo_documento: tipoDocumento,
		detalle_documento: detalleDocumento
	}).then(function (response) {

		if (response.status == 200) {
			if (response.data.termina_proceso == true) {

				Swal.fire({
					html: response.data.modal,
					showConfirmButton: false,
					allowOutsideClick: false,
					customClass: 'modalstatus'
				});
			} else {
				var paso = response.data.paso;
				location.href = '/carga_documentos/paso' + paso;
			}
		}
	}).catch(function (err) {

		Swal.fire({
			title: "Ooops.. Surgió un problema",
			text: error.response.status + ': ' + error.response.responseText,
			type: 'error',
			showConfirmButton: true,
			customClass: 'modalError',
			allowOutsideClick: false
		});
	});
}

function ayuda(tip) {
	Swal.fire({
		title: 'Algunos consejos',
		html: '<img src="/images/webapp/' + tip + '.jpg" style="max-width: 300px;">',
		showConfirmButton: true,
		confirmButtonText: 'Entendido',
		customClass: 'tipsWebapp'
	});
}

$(document).on('click', "#terminaFlujo", function () {
	location.href = '/';
});