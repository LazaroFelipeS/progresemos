/*
function guardarCuestionarioDinamico() {

    $('.labelDinamico').css('color', '#b7b7b7');
    $('.lerror').text('');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        data: $("#formCuestionarioDinamico").serialize(),
        url: "/saveCuestionarioDinamico",
        dataType: "json",
        beforeSend: function() {
            //OpenModal("#processing");
       }
    }).done(function(resultado) {

        if (resultado.success == true) {

            Swal.fire({
                html: resultado.modal,
                showConfirmButton: false,
                allowOutsideClick: false,
            });

        } else {
            // CloseModal("#processing");
            swal({
                title: "Ooops.. Surgió un problema",
                text: resultado.message,
                showConfirmButton: true
           });
        }

    }).fail(function(resultado, textStatus, errorThrow) {
        $("html, body").animate({ scrollTop: $("#cuestionarioDinamico").offset().top }, 1000);
        // CloseModal("#processing");
        if (resultado.status == 422 && errorThrow == 'Unprocessable Entity') {
            $.each(resultado.responseJSON, function(index, error) {
                $('#label_' + index).css('color', 'red');
                $('#lerror_' + index).text(error);
            });
        } else {
            swal({
                title: "Ooops.. Surgió un problema",
                text: resultado.status + ': ' + errorThrow,
                showConfirmButton: true
           });
        }

    });

}*/

function guardarCuestionarioDinamico() {

    $('.labelDinamico').css('color', '#b7b7b7');
    $('.help').text('');

    var texto = 'Guardando respuestas...';
    Swal.fire({
       html: '<center><img src="/images/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false
   });

    var datos = $("#formCuestionarioDinamico").serialize();
    axios.post('/saveCuestionarioDinamico', datos)
    .then(function (resultado) {

        if (resultado.data.success == true) {

            Swal.fire({
                html: resultado.data.modal,
                showConfirmButton: false,
                allowOutsideClick: false,
                customClass: 'modaloferta'
            });

        } else {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: resultado.data.message,
                showConfirmButton: true
           });
        }

    })
    .catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        if (error.response.status == 422) {
            $.each(error.response.data, function(index, error) {
                $('#label_' + index).css('color', 'red');
                $('#lerror_' + index).text(error);
            });
            swal.close();
        } else {
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                showConfirmButton: true,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });
       }

    });

}

function campoVisible(valor, div_id, respuetas) {

    respuestas = respuetas.split('|');
    div_id = div_id.split('|');

    $.each(respuestas, function(index, respuesta) {

        if (respuesta == valor) {
            $('#div_' + div_id[index]).show();
        } else {
            $('#div_' + div_id[index]).hide();
        }

    });

}


function campoVisible(valor, div_id, respuetas) {

    respuestas = respuetas.split('|');
    div_id = div_id.split('|');

    $.each(respuestas, function(index, respuesta) {

        if (respuesta == valor) {
            $('#div_' + div_id[index]).show();
        } else {
            $('#div_' + div_id[index]).hide();
        }

    });

}
