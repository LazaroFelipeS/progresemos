<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Carbon\Carbon;
use App\Producto;


Auth::routes();

Route::group(['middleware' => ['web']], function () {

    Route::get('/', 'ProductoController@obtenerVista')->name('index');
    Route::get('/getForm', 'RegisterController@getForm');
    Route::get('/enviarInvitacion/{uuid}', 'ClienteEstrellaController@indexInvitacion');
    Route::post('/validaciones', 'RegisterController@validaciones');
    Route::get('/solicitud/cuestionarioDinamico','RegisterController@getCuestionario');

    Route::get('/registro', 'ProductoController@obtenerVista');
   
    Route::get('/aviso-privacidad', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('aviso-privacidad', ['configuracion' => $configuracion]);
    });

    Route::get('/derechos-arco', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('derechos-arco', ['configuracion' => $configuracion]);
    });

    Route::get('/contratos', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('contratos', ['configuracion' => $configuracion]);
    });

    //=========== RESTAURA CONTRASEÑA =============
    Route::get('/password-restore', function () {
        $logo = Cookie::get('logo');
        if (!isset($logo)) {
            $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
            $configuracion['logo'] = $configuracion['logo'];
        } else {
            $configuracion['logo'] = $logo;
        }
        return view('password-restore', ['configuracion' => $configuracion]);
    });
    Route::post('/email-password-restore', 'RestorePasswordController@emailPasswordRestore');
    Route::post('/temporary-password', 'RestorePasswordController@validateTemporaryPassword');
    Route::post('/password-update', 'RestorePasswordController@passwordUpdate');

    Route::get('/login', function () {
        return redirect('auth/login');
    });

    Route::get('/api/geo/{cp}', 'SolicitudController@apiGeoCP');
    Route::get('/api/cp/{cp}', 'SolicitudController@apiGeoCP');
    Route::get('/api/ciudades/{estado}', 'SolicitudController@apiCiudades');

    // RUTAS REGISTRO
    Route::post('/solicitud/registro', 'RegisterController@registroProspecto');
    Route::post('/solicitud/registro/solicitud', 'RegisterController@registroSolicitud');
    Route::post('/solicitud/registro/ldap', 'RegisterController@registroLDAP');
    Route::post('/solicitud/registro/sms', 'RegisterController@envioSMS');
    Route::get('/solicitud/validacion_usuario', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('solicitud.validacion_usuario', ['configuracion' => $configuracion]);
    });
    Route::get('/solicitud/datos_domicilio', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('solicitud.datos_domicilio', ['configuracion' => $configuracion]);
    }); 
    Route::get('/solicitud/datos_personales', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('solicitud.datos_personales', ['configuracion' => $configuracion]);
    });
    Route::get('/solicitud/datos_buro', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('solicitud.datos_buro', ['configuracion' => $configuracion]);
    });
    Route::get('/solicitud/datos_adicionales', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('solicitud.datos_adicionales', ['configuracion' => $configuracion]);
    });
    Route::get('/solicitud/datos_ingreso', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('solicitud.datos_ingreso', ['configuracion' => $configuracion]);
    });
    /* Route::get('/solicitud/cuestionario', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('solicitud.cuestionario_dinamico', ['configuracion' => $configuracion]);
    }); */
    Route::get('/solicitud/decision', function () {
        $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion');
        return view('solicitud.decision', ['configuracion' => $configuracion]);
    });


    Route::post('/solicitud/conf_sms_code', 'RegisterController@confSMSCode');
    Route::post('/solicitud/resend_code', 'RegisterController@resendSMSCode');
    Route::post('/solicitud/reg_domicilio', 'RegisterController@regDomicilio');
    Route::post('/solicitud/domicilio/buscar_colonias', 'RegisterController@buscarColonias');
    Route::post('/solicitud/datos_personales/buscar_ciudades', 'RegisterController@buscarCiudades');
    Route::post('/solicitud/datos_personales', 'RegisterController@datosPersonales');
    Route::post('/solicitud/cuentas_de_credito', 'RegisterController@cuentasDeCredito');
    Route::post('/solicitud/correccionBC', 'RegisterController@correccionBC');
    Route::post('/solicitud/datos_adicionales', 'RegisterController@datosAdicionales');
    Route::post('/solicitud/datos_empleo', 'RegisterController@datosEmpleo');

    //LDAP LOGIN ROUTES
    Route::post('/ldap/register/', 'SessionController@Register_LDAP');
    Route::post('/login/prospecto', 'SessionController@loginLDAP');

    //SOLICITUD ROUTES
    Route::post('/solicitud_bc_score', 'BuroCreditoController@primeraLlamada');
    Route::post('/solicitud_hawk', 'BuroCreditoController@segundaLlamada');
    Route::post('/solicitud_fico_alp', 'ALPController@solicitudFicoAlp');

    //============== PRIVATE ROUTES - REQUIRE AUTHENTICATION =======================
    //admin home
    Route::get('/panel', 'BackOfficeController@panelDashboard')->middleware('auth')->name('panel');
    Route::post('/panel', 'BackOfficeController@panelDashboard')->middleware('auth');
    // Detalle del prospecto
    Route::get('/panel/prospecto/{prospecto_id}', 'BackOfficeV2Controller@detalleProspecto')->middleware('auth');
    Route::get('/panel/prospecto/getDetalleProspecto/{prospecto_id}', 'BackOfficeV2Controller@getDetalleProspecto')->middleware('auth');
    // Detalle de solicitud
    Route::get('/panel/prospecto/solicitud/{solicitud_id}', 'BackOfficeV2Controller@detalleSolicitud')->middleware('auth');
    Route::get('/panel/prospecto/getDetalleSolicitud/{solicitud_id}', 'BackOfficeV2Controller@getDetalleSolicitud')->middleware('auth');
    //prospect detail
    Route::get('/panelva/prospecto/{prospect_id}', 'BackOfficeController@panelProspectDetail')->middleware('auth');

    //download report csv for a single solicitud
    Route::get('/panel/report-csv/{prospect_id}/{solic_id}', 'BackOfficeController@downloadCsvReport')->middleware('auth');
    //download complete report for all solicitudes for a given month of year
    Route::get('/panel/report-completo-csv/', 'BackOfficeController@toViewFullCsvReport')->middleware('auth');
    Route::post('/panel/report-completo-csv/', 'BackOfficeController@postToViewFullCsvReport')->middleware('auth');
    Route::get('/panel/report-completo-csv/{month}/{year}', 'BackOfficeController@viewCsvReportFull')->middleware('auth');
    Route::post('/panel/report-completo-csv/{month}/{year}', 'BackOfficeController@viewCsvReportFull')->middleware('auth');

    //encrypt any non encrypted values (sensetive fields only) in the Prospects Table
    Route::get('/panel/verify-encrypted/prospects', 'BackOfficeController@cleanEncryptProspects')->middleware('auth');
    //encrypt any non encrypted values (sensetive fields only) in the Solicitations Table
    Route::get('/panel/verify-encrypted/solics', 'BackOfficeController@cleanEncryptSolics')->middleware('auth');

    // //test buro report
    // Route::get('/panel/buro-reporte-prueba', ['middleware' => 'auth', function(){
    //     return view('crm.buro.buro_reporte_test');
    // }]);
    //test registro
    Route::get('/panel/registro-prueba', 'BackOfficeController@panelRegistroPrueba')->middleware('auth');
    //test calixta sms sending
    Route::get('/panel/calixta-prueba', 'BackOfficeController@panelCalixtaPrueba')->middleware('auth');

    //show register error log
    Route::get('/panel/registro-logs', 'BackOfficeController@showRegErrorLogs')->middleware('auth');

    //update the codigos postales table
    Route::get('/panel/cp-console', 'BackOfficeController@codigosPostalesConsole')->middleware('auth');
    Route::post('/panel/cp-get-state', 'BackOfficeController@codigosPostalesGetState')->middleware('auth');
    Route::post('/panel/cp-update-verify', 'BackOfficeController@codigosPostalesUpdateVerify')->middleware('auth');

    //============== AUTHENTICATION ROUTES =========================================
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@login');
    Route::get('logout', 'Auth\AuthController@logout');

    //=============== DEBUG ROUTES  (COMMENT WHEN NOT IN USE) ============
    // Route::post('/debug/full_report_parse', 'BackOfficeController@debugFullReportParse');
    Route::get('/panel/show-fico-xml/{solic_id}', 'BackOfficeController@showFicoXml')->middleware('auth');

    //=============== ANALYTICS ============
    Route::get('analytics', 'AnalyticsController@getDataAnalytics')->middleware('auth');

    //=============== ALTA CLIENTES T24 ============
    Route::get('alta-clientes', 'T24Controller@altaClientes')->middleware('auth');
    Route::post('carga-layout', 'T24Controller@cargaLayout')->middleware('auth');
    Route::post('alta-cliente-t24', 'T24Controller@altaCliente_T24')->middleware('auth');
    Route::post('alta-solicitud-t24', 'T24Controller@altaSolicitud_T24')->middleware('auth');
    Route::post('ligar-usuario-ldap', 'T24Controller@ligarUsuarioLDAP')->middleware('auth');
    Route::post('enviar-email', 'T24Controller@enviarEmail')->middleware('auth');
    Route::get('search/autocomplete', 'T24Controller@autoComplete');
    Route::get('alta-clientes/update_lista', 'T24Controller@updateLista');

    //============= CODIGOS POSTALES =================
    Route::get('/panel/cobertura', 'BackOfficeController@coberturaCodigos')->middleware('auth');
    Route::get('/panel/buscar_cobertura', 'BackOfficeController@buscarCobertura')->middleware('auth');
    Route::get('/decode-domicilios', 'BackOfficeController@decodeDomicilios')->middleware('auth');

    //============= CUESTIONARIO DINAMICO =================
    Route::get('/solicitud/cuestionario_dinamico', 'SolicitudApiController@cuestionarioDinamico');
    Route::post('/saveCuestionarioDinamico', 'CuestionarioDinamicoController@save');

    //============= CUESTIONARIO FIJO =================
    Route::post('/saveCuestionarioFijo', 'CuestionarioFijoController@save');

    //============= CALENDARIO CITAS =================
    Route::get('/calendario-citas', 'CuestionarioFijoController@calendarioCitas')->middleware('auth');
    Route::get('/obtener-citas', 'CuestionarioFijoController@obtenerCitas')->middleware('auth');

    //============ OFERTA ============================
    Route::post('/solicitud/status_oferta', 'SolicitudController@statusOferta');
    Route::post('/solicitud/status_oferta_doble', 'SolicitudController@statusOfertaDoble');
    Route::post('/solicitud/rechazar_oferta', 'SolicitudController@rechazarOferta');
    Route::get('/solicitud/datosOferta', 'SolicitudApiController@getDatosOferta');

    Route::get('/retry-job/{id}', function($id) {

        $retryJob = Artisan::call('queue:retry', ['id' => [$id]]);

    });

    //=========== PRODUCTOS =============
    Route::get('/productos', 'ProductoController@index')->name('productos')->middleware('auth');
    Route::get('/productos/nuevo', 'ProductoController@nuevo')->name('productos.nuevo')->middleware('auth');
    Route::post('/productos/save', 'ProductoController@save')->middleware('auth');
    Route::get('/productos/{id}', 'ProductoController@editar')->middleware('auth');
    Route::post('/productos/{id}', 'ProductoController@update')->middleware('auth');
    Route::get('/productos/{id}/cobertura', 'ProductoController@cobertura')->middleware('auth');
    Route::post('/productos/{id}/cobertura', 'ProductoController@updateCobertura')->middleware('auth');
    Route::post('/productos/plazos/agregar', 'ProductoController@savePlazos')->middleware('auth');
    Route::post('/productos/finalidad/agregar', 'ProductoController@saveFinalidad')->middleware('auth');
    Route::post('/productos/ocupacion/agregar', 'ProductoController@saveOcupacion')->middleware('auth');

    Route::get('/landing/{producto}', function ($alias) {

        Cookie::queue(Cookie::forget('checa_calificas'));
        $producto = Producto::where('alias', $alias)->get();

        if (count($producto) == 1) {

            $vista = $producto[0]->vista;
            if ($vista == null && $producto[0]->tipo == 'Nómina') {
                $vista = 'producto.landing.generica_nomina';
            } elseif ($vista == null && $producto[0]->tipo != 'Nómina') {
                $vista = 'producto.landing.generica';
            }

            if ($producto[0]->vigencia_hasta != null) {

                $hoy = Carbon::today();
                $vigencia_hasta = Carbon::createFromFormat('Y-m-d H:i:s', $producto[0]->vigencia_hasta. '00:00:00');
                $vigencia = $hoy->diffInDays($vigencia_hasta, false);

                if ($vigencia < 0) {

                    Cookie::queue(Cookie::forget('producto'));
                    Cookie::queue(Cookie::forget('logo'));
                    return redirect()->route('index');

                } else {

                    Cookie::queue(Cookie::forget('referencia'));
                    $response = new Illuminate\Http\Response(view($vista)->with('producto', $producto[0]));
                    $lifetime = Carbon::now()->addYear()->diffInMinutes();
                    $cookie = Cookie::make('producto', $alias, $lifetime);
                    return $response->cookie($cookie);

                }

            } else {

                Cookie::queue(Cookie::forget('referencia'));
                $response = new Illuminate\Http\Response(view($vista)->with('producto', $producto[0]));
                $lifetime = Carbon::now()->addYear()->diffInMinutes();
                $cookie = Cookie::make('producto', $alias, $lifetime);
                return $response->cookie($cookie);

            }

        } else {
            abort(404);
        }

    });

    Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
    Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

    Route::get('/panel/usuarios', 'UsersController@index')->name('usuarios');
    Route::get('/panel/usuarios/perfiles', 'RolesController@index')->name('perfiles');

    Route::post('/panel/usuarios/save/', 'UsersController@save');
    Route::get('/panel/usuarios/{id}', 'UsersController@editar');
    Route::post('/panel/usuarios/{id}', 'UsersController@actualizar');

    Route::post('/panel/usuarios/perfiles/save/', 'RolesController@save');
    Route::get('/panel/usuarios/perfiles/{id}', 'RolesController@editar');
    Route::post('/panel/usuarios/perfiles/{id}', 'RolesController@actualizar');
    Route::post('/solicitudExpress', 'RegisterController@solicitudExpress');

    //=============== CONVENIOS ===============
    Route::get('/captura_solicitud/{producto}', function($producto) {
        return view('auth.login_captura', ['producto' => $producto]);
    });
    Route::post('/login/captura', 'Auth\LoginController@loginCaptura');
    Route::get('/logout/captura/{producto}', 'Auth\LoginController@logoutCaptura');
    Route::get('/panel/producto/captura/{producto}', function($alias) {

        $producto = Producto::where('alias', $alias)->get();

        if (count($producto) == 1) {

            $configuracion = App::call('App\Http\Controllers\ProductoController@obtenerConfiguracion', ['id_producto' => $producto[0]->id]);
            if ($producto[0]->vigencia_hasta != null) {

                $hoy = Carbon::today();
                $vigencia_hasta = Carbon::createFromFormat('Y-m-d H:i:s', $producto[0]->vigencia_hasta. '00:00:00');
                $vigencia = $hoy->diffInDays($vigencia_hasta, false);

                if ($vigencia < 0) {

                    return redirect()->route('index');

                } else {

                    return view('producto.landing.'.$alias.'-captura', ['configuracion' => $configuracion]);

                }

            } else {

                return view('producto.landing.'.$alias.'-captura', ['configuracion' => $configuracion]);

            }

        } else {
            abort(404);
        }

    })->middleware('auth:captura_convenio');

    Route::get('/descargaDatosBuro', 'BackOfficeController@descargaDatosBuro')->middleware('auth');

    //=============== CLIENTE ESTRELLA ===============
    Route::get('/panel/clienteEstrella', 'ClienteEstrellaController@index')->name('clienteEstrella')->middleware('auth');
    Route::post('/panel/uploadClienteEstrella', 'ClienteEstrellaController@uploadClienteEstrella');
    Route::get('/panel/listaClienteEstrella', 'ClienteEstrellaController@listaClienteEstrella');
    
    Route::get('/getDatosUsuario', 'ClienteEstrellaController@getDatosUsuario');

    //=============== REPORTE DE EFICIENCIA ===============
    Route::get('/datosReporteEficiencia',  'ReporteDiarioController@datosReporteEficiencia')->middleware('auth');
    Route::get('/descargaReporte/{file}',  'ReporteDiarioController@descargaReporte')->middleware('auth');

    //=============== WEB APP ==============
    Route::get('/carga_documentos/paso1',  'WebAppController@capturaIdentificacionFront')->middleware('auth:prospecto');
    Route::get('/carga_documentos/paso2',  'WebAppController@capturaIdentificacionBack')->middleware('auth:prospecto');
    Route::get('/carga_documentos/paso3',  'WebAppController@capturaSelfie')->middleware('auth:prospecto');
    Route::post('/carga_documentos/subirDocumento',  'WebAppController@subirDocumento')->middleware('auth:prospecto');
    Route::get('/carga_documentos/subirDocumentoRepositorio',  'WebAppController@subirDocumentoRepositorio');
    Route::post('/carga_documentos/procesaFaceMatch', 'WebAppController@procesaFaceMatch')->middleware('auth');;
    Route::get('/prospecto/logout', 'Auth\AuthProspectoController@logout');
    Route::get('/token', function () {

        $customClaims = ['jti' => env('JWT_JTI')];
        $payload = JWTFactory::make($customClaims);
        $token = JWTAuth::encode($payload);

        return $token;

    })->middleware('auth');

});
