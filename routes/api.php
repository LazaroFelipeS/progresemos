<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {

    return $request->user();
});

Route::middleware('auth:api')->post('/cuentas_de_credito', 'RegisterController@cuentasDeCredito');
Route::middleware('auth:api')->post('/solicitud_bc_score', 'BuroCreditoController@primeraLlamada');
Route::middleware('auth:api')->post('/solicitud_hawk', 'BuroCreditoController@segundaLlamada');
Route::middleware('auth:api')->post('/solicitud_fico_alp', 'ALPController@solicitudFicoAlp');
Route::middleware('auth:api')->post('/solicitud_elegible', 'SolicitudApiController@solicitudElegible');
Route::middleware('auth:api')->get('/cuestionario_dinamico', 'SolicitudApiController@cuestionarioDinamico');
Route::middleware('auth:api')->post('/guarda_respuestas', 'CuestionarioDinamicoController@save');
Route::middleware('auth:api')->get('/datosOferta', 'SolicitudApiController@getDatosOferta');
Route::middleware('auth:api')->post('/statusOferta', 'SolicitudApiController@statusOferta');
Route::middleware('auth:api')->post('/cuestionarioFijo', 'CuestionarioFijoController@save');

Route::middleware('auth:api')->get('/datosSolicitud', 'SolicitudApiController@datosSolicitud');
